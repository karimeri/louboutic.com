<?php
namespace Rollback\Export\Console\Command;

use Exception;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Io\File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Export
 */
class Export extends Command
{
    /**
     * @var bool
     */
    private $debug = false;

    /**
     * @var string
     */
    private $goliveDate;
    /**
     * @var array
     */
    private $results = array();
    /**
     * @var array
     */
    private $ids = array(
        'customer' => array(),
        'order' => array(),
        'wishlist' => array(),
    );
    /**
     * @var array
     */
    protected $exports = array(
        'customer' => array(
            'filename' => 'customer_entity.csv',
        ),
        'customer_address' => array(
            'filename' => 'customer_address.csv',
        ),
        'newsletter_subscriber' => array(
            'filename' => 'newsletter_subscriber.csv',
        ),
        'gift_message' => array(
            'filename' => 'gift_message.csv',
        ),
        'order' => array(
            'filename' => 'sales_order.csv',
        ),
        'order_item' => array(
            'filename' => 'sales_order_item.csv',
        ),
        'order_address' => array(
            'filename' => 'sales_order_address.csv',
        ),
        'order_payment' => array(
            'filename' => 'sales_order_payment.csv',
        ),
        'order_invoice' => array(
            'filename' => 'sales_order_invoice.csv',
        ),
        'order_status' => array(
            'filename' => 'sales_order_status.csv',
        ),
        'wishlist' => array(
            'filename' => 'wishlist.csv',
        ),
        'wishlist_item' => array(
            'filename' => 'wishlist_item.csv',
        ),
        'back_in_stock' => array(
            'filename' => 'product_alert_stock.csv',
        ),
    );

    /**
     * @var DirectoryList
     */
    protected $directoryList;
    /**
     * @var ResourceConnection
     */
    private $resource;
    /**
     * @var File
     */
    protected $ioFile;

    /**
     * @param string|null $name
     * @param DirectoryList $directoryList
     * @param ResourceConnection $resource
     * @param File $ioFile
     */
    public function __construct(
        DirectoryList $directoryList,
        ResourceConnection $resource,
        File $ioFile
    ) {
        parent::__construct();
        $this->directoryList = $directoryList;
        $this->resource = $resource;
        $this->ioFile = $ioFile;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('rollback:export')
            ->setDescription('Export datas for Rollback');

        $this->addArgument(
            'date',
            InputArgument::OPTIONAL,
            'GoLive Date',
            '2020-10-13'
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->goliveDate = $input->getArgument('date');
        $this->output = $output;
        $this->launchExport();
    }

    /**
     * Run export
     * @return int|void
     */
    public function launchExport()
    {
        $this->log(PHP_EOL . PHP_EOL);
        $begin = microtime(true);

        ini_set('memory_limit', '-1');

        $this->log('=====================================================================================');
        $this->log('Script started');
        $this->log('=====================================================================================');
        $this->log("GoLive Date : " . $this->goliveDate);
        $this->log('=====================================================================================');
        try {
            $this->initDatas();
            foreach ($this->exports as $type => $config) {
                $start = microtime(true);

                $result = $this->export($type, $config['filename']);

                $time = microtime(true) - $start;
                $this->log(sprintf("Export %-25s : %s entries exported - %.3f sec %s", $type, $result['count'], $time, ($result['cache']) ? '- from cache' : ''));
            }
            $time = microtime(true) - $begin;
            $this->log('=====================================================================================');
            $this->log(sprintf("Total time : %.3f sec", $time));
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }
        $this->log(PHP_EOL . PHP_EOL);
    }

    /**
     * Pre-calculate ids before executing all requests
     * @throws Exception
     */
    protected function initDatas()
    {
        $this->log('Initialize scope datas :');
        $list = array(
            'customer' => 'entity_id',
            'order' => 'entity_id',
            'wishlist' => 'wishlist_id',
        );
        foreach ($list as $type => $entityId) {
            $query = $this->getRequest($type);
            if ($query != '') {
                if ($this->debug) $this->log($query);
                $this->results[$type] = $this->executeQuery($query);
                foreach ($this->results[$type] as $row) {
                    $this->ids[$type][] = $row[$entityId];
                }
            }
        }
        foreach ($this->ids as $type => $datas) {
            $this->log(sprintf(" - %-10s : %s ids", $type, count($datas)));
        }
    }

    /**
     * Export datas for defined type and write it on filename
     * @param $type
     * @param $filename
     * @return array
     * @throws Exception
     */
    protected function export($type, $filename)
    {
        $fromCache = true;
        $query = $this->getRequest($type);
        if ($query != '') {
            if (!isset($this->results[$type])) {
                $fromCache = false;
                $this->results[$type] = $this->executeQuery($query);
            }
            $this->write($filename, $this->results[$type]);
        }
        return array(
            'cache' => $fromCache,
            'count' => count($this->results[$type]),
        );
    }

    /**
     * Write result on export filename
     * @param $filename
     * @param $result
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function write($filename, $result)
    {
        $hasHeaders = false;
        $fp = fopen($this->getFilepath($filename), 'w');
        foreach ($result as $datas) {
            if (!$hasHeaders) {
                fputcsv($fp, array_keys($datas));
                $hasHeaders = true;
            }
            fputcsv($fp, $datas);
        }
        fclose($fp);
    }

    /**
     * Get request for defined $type
     * @param $type
     * @return string
     * @throws Exception
     */
    protected function getRequest($type)
    {
        $customerIds = join(', ', $this->ids['customer']);
        $orderIds = join(', ', $this->ids['order']);
        $wishlistIds = join(', ', $this->ids['wishlist']);
        switch ($type) {
            case 'customer' :
                return "
SELECT
    cus.entity_id, -- save for correspondance
    cus.website_id, -- mapping
    cus.email,
    cus.group_id, -- mapping
    cus.increment_id,
    cus.store_id, -- mapping
    cus.created_at,
    cus.updated_at,
    cus.is_active,
    -- disable_auto_group_change,
    cus.created_in, -- mapping
    cus.prefix,
    cus.firstname,
    cus.middlename,
    cus.lastname,
    cus.suffix,
    cus.dob,
    cus.password_hash, -- to clean ? => not used
    cus.rp_token,
    cus.rp_token_created_at,
    cus.default_billing, -- correspondance
    cus.default_shipping, -- correspondance
    cus.taxvat, -- mapping
    cus.confirmation,
    cus.gender -- mapping
    -- failures_num,
    -- first_failure,
    -- lock_expires
FROM customer_entity cus
LEFT JOIN newsletter_subscriber ns ON ns.customer_id = cus.entity_id
LEFT JOIN sales_order so ON so.customer_id = cus.entity_id
LEFT JOIN customer_address_entity ad ON ad.parent_id = cus.entity_id
LEFT JOIN wishlist wl ON wl.customer_id = cus.entity_id
LEFT JOIN wishlist_item wli ON wli.wishlist_id = wl.wishlist_id
WHERE
    cus.updated_at > '{$this->goliveDate}'
    OR ad.updated_at > '{$this->goliveDate}'
    OR ns.change_status_at > '{$this->goliveDate}'
    OR so.created_at > '{$this->goliveDate}'
    OR wl.updated_at > '{$this->goliveDate}'
    OR wli.added_at > '{$this->goliveDate}'
GROUP BY cus.entity_id;";
                break;

            case 'customer_address' :
                if ($customerIds !== '') {
                    return "
SELECT
    entity_id, -- save for correspondance
    parent_id AS customer_id, -- correspondance
    created_at,
    updated_at,
    is_active,
    city,
    company,
    country_id,
    fax,
    firstname,
    lastname,
    middlename,
    postcode,
    prefix,
    region,
    region_id,
    street,
    suffix,
    telephone,
    vat_id, -- to check
    vat_is_valid, -- to check
    vat_request_date, -- to check
    vat_request_id, -- to check
    vat_request_success -- to check
FROM customer_address_entity WHERE parent_id IN ({$customerIds});";
                } else {
                    return '';
                }
                break;

            case 'newsletter_subscriber' :
                return "
SELECT
    store_id, -- correspondance
    change_status_at,
    customer_id, -- correspondance
    subscriber_email,
    subscriber_status, -- mapping
    subscriber_confirm_code,
    -- subscriber_collection_gender
    'all (commerce & beauty)' AS subscription_type
FROM newsletter_subscriber WHERE change_status_at > '{$this->goliveDate};';";
                break;

            case 'gift_message' :
                if ($orderIds !== '') {
                    return "
SELECT *
FROM gift_message
WHERE gift_message_id IN (
    SELECT gift_message_id FROM sales_order WHERE entity_id IN ({$orderIds})
    -- UNION SELECT gift_message_id FROM sales_order_item WHERE order_id IN ({$orderIds})
);";
                } else {
                    return '';
                }
                break;

            case 'order' :
                return "
SELECT
    entity_id, -- save for correspondance
	state, -- mapping
	status, -- mapping
	coupon_code,
	protect_code,
	shipping_description,
	is_virtual,
	store_id, -- mapping
	customer_id, -- correspondance
	base_discount_amount,
	base_discount_canceled,
	base_discount_invoiced,
	base_discount_refunded,
	base_grand_total,
	base_shipping_amount,
	base_shipping_canceled,
	base_shipping_invoiced,
	base_shipping_refunded,
	base_shipping_tax_amount,
	base_shipping_tax_refunded,
	base_subtotal,
	base_subtotal_canceled,
	base_subtotal_invoiced,
	base_subtotal_refunded,
	base_tax_amount,
	base_tax_canceled,
	base_tax_invoiced,
	base_tax_refunded,
	base_to_global_rate,
	base_to_order_rate,
	base_total_canceled,
	base_total_invoiced,
	base_total_invoiced_cost,
	base_total_offline_refunded,
	base_total_online_refunded,
	base_total_paid,
	base_total_qty_ordered,
	base_total_refunded,
	discount_amount,
	discount_canceled,
	discount_invoiced,
	discount_refunded,
	grand_total,
	shipping_amount,
	shipping_canceled,
	shipping_invoiced,
	shipping_refunded,
	shipping_tax_amount,
	shipping_tax_refunded,
	store_to_base_rate,
	store_to_order_rate,
	subtotal,
	subtotal_canceled,
	subtotal_invoiced,
	subtotal_refunded,
	tax_amount,
	tax_canceled,
	tax_invoiced,
	tax_refunded,
	total_canceled,
	total_invoiced,
	total_offline_refunded,
	total_online_refunded,
	total_paid,
	total_qty_ordered,
	total_refunded,
	can_ship_partially,
	can_ship_partially_item,
	customer_is_guest,
	customer_note_notify,
	billing_address_id, -- correspondance
	customer_group_id, -- mapping
	edit_increment,
	email_sent,
	-- send_email,
	forced_shipment_with_invoice,
	payment_auth_expiration,
	-- quote_address_id, -- quote not imported
	-- quote_id, -- quote not imported
	shipping_address_id, -- correspondance
	adjustment_negative,
	adjustment_positive,
	base_adjustment_negative,
	base_adjustment_positive,
	base_shipping_discount_amount,
	base_subtotal_incl_tax,
	base_total_due,
	payment_authorization_amount,
	shipping_discount_amount,
	subtotal_incl_tax,
	total_due,
	weight,
	customer_dob,
	increment_id, -- to verify
	-- applied_rule_ids, -- not used
	base_currency_code,
	customer_email,
	customer_firstname,
	customer_lastname,
	customer_middlename,
	customer_prefix,
	customer_suffix,
	customer_taxvat,
	discount_description,
	ext_customer_id, -- to check
	ext_order_id, -- to check
	global_currency_code,
	hold_before_state,
	hold_before_status,
	order_currency_code,
	original_increment_id, -- to check
	relation_child_id, -- to check
	relation_child_real_id, -- to check
	relation_parent_id, -- to check
	relation_parent_real_id, -- to check
	remote_ip,
	shipping_method,
	store_currency_code,
	store_name,
	x_forwarded_for,
	customer_note,
	created_at,
	updated_at,
	total_item_count,
	customer_gender, -- mapping
	-- discount_tax_compensation_amount,
	-- base_discount_tax_compensation_amount,
	-- shipping_discount_tax_compensation_amount,
	-- base_shipping_discount_tax_compensation_amnt,
	-- discount_tax_compensation_invoiced,
	-- base_discount_tax_compensation_invoiced,
	-- discount_tax_compensation_refunded,
	-- base_discount_tax_compensation_refunded,
	shipping_incl_tax,
	base_shipping_incl_tax,
	-- coupon_rule_name,
	base_customer_balance_amount,
	customer_balance_amount,
	base_customer_balance_invoiced,
	customer_balance_invoiced,
	base_customer_balance_refunded,
	customer_balance_refunded,
	bs_customer_bal_total_refunded,
	customer_bal_total_refunded,
	gift_cards,
	base_gift_cards_amount,
	gift_cards_amount,
	base_gift_cards_invoiced,
	gift_cards_invoiced,
	base_gift_cards_refunded,
	gift_cards_refunded,
	gift_message_id,
	gw_id,
	gw_allow_gift_receipt,
	gw_add_card,
	gw_base_price,
	gw_price,
	gw_items_base_price,
	gw_items_price,
	gw_card_base_price,
	gw_card_price,
	gw_base_tax_amount,
	gw_tax_amount,
	gw_items_base_tax_amount,
	gw_items_tax_amount,
	gw_card_base_tax_amount,
	gw_card_tax_amount,
	-- gw_base_price_incl_tax,
	-- gw_price_incl_tax,
	-- gw_items_base_price_incl_tax,
	-- gw_items_price_incl_tax,
	-- gw_card_base_price_incl_tax,
	-- gw_card_price_incl_tax,
	gw_base_price_invoiced,
	gw_price_invoiced,
	gw_items_base_price_invoiced,
	gw_items_price_invoiced,
	gw_card_base_price_invoiced,
	gw_card_price_invoiced,
	gw_base_tax_amount_invoiced,
	gw_tax_amount_invoiced,
	gw_items_base_tax_invoiced,
	gw_items_tax_invoiced,
	gw_card_base_tax_invoiced,
	gw_card_tax_invoiced,
	gw_base_price_refunded,
	gw_price_refunded,
	gw_items_base_price_refunded,
	gw_items_price_refunded,
	gw_card_base_price_refunded,
	gw_card_price_refunded,
	gw_base_tax_amount_refunded,
	gw_tax_amount_refunded,
	gw_items_base_tax_refunded,
	gw_items_tax_refunded,
	gw_card_base_tax_refunded,
	gw_card_tax_refunded,
	paypal_ipn_customer_notified,
	reward_points_balance,
	base_reward_currency_amount,
	reward_currency_amount,
	base_rwrd_crrncy_amt_invoiced,
	rwrd_currency_amount_invoiced,
	base_rwrd_crrncy_amnt_refnded,
	rwrd_crrncy_amnt_refunded,
	reward_points_balance_refund
	-- adyen_resulturl_event_code,
	-- adyen_notification_event_code,
	-- adyen_notification_event_code_success,
	-- customer_exported,
	-- address_exported,
	-- source_code,
	-- rma_id, -- rma not imported
	-- metadata,
	-- metatype,
	-- items_to_invoice,
	-- wms_id,
	-- is_fraud,
	-- transaction_id, -- transaction not imported
	-- creditmemo_id,
	-- retry_full_auto,
	-- send_full_auth
FROM sales_order WHERE created_at > '{$this->goliveDate}';";
                break;

            case 'order_address' :
                if ($orderIds !== '') {
                    return "
SELECT
    entity_id, -- save for correspondance
    parent_id, -- correspondance order_id
    customer_address_id, -- correspondance
    -- quote_address_id, -- quote not imported
    region_id,
    customer_id, -- correspondance
    fax,
    region,
    postcode,
    lastname,
    street,
    city,
    email,
    telephone, -- to check
    country_id,
    firstname,
    address_type,
    prefix,
    middlename,
    suffix,
    company,
    -- vat_id,
    -- vat_is_valid,
    -- vat_request_id,
    -- vat_request_date,
    -- vat_request_success,
    giftregistry_item_id,
    IFNULL(contact_phone, '') AS contact_telephone -- to check
FROM sales_order_address WHERE parent_id IN ({$orderIds});";
                } else {
                    return '';
                }
                break;

            case 'order_item' :
                if ($orderIds !== '') {
                    return "
SELECT
    item_id, -- correspondance in live
    order_id,  -- correspondance
    parent_item_id, -- correspondance in live
    -- quote_item_id, -- quote not imported
    store_id, -- mapping
    created_at,
    updated_at,
    product_id, -- correspondance
    product_type,
    product_options,
    weight,
    is_virtual,
    sku,
    name,
    description,
    -- applied_rule_ids,
    additional_data,
    is_qty_decimal,
    no_discount,
    qty_backordered,
    qty_canceled,
    qty_invoiced,
    qty_ordered,
    qty_refunded,
    qty_shipped,
    base_cost,
    price,
    base_price,
    original_price,
    base_original_price,
    tax_percent,
    tax_amount,
    base_tax_amount,
    tax_invoiced,
    base_tax_invoiced,
    discount_percent,
    discount_amount,
    base_discount_amount,
    discount_invoiced,
    base_discount_invoiced,
    amount_refunded,
    base_amount_refunded,
    row_total,
    base_row_total,
    row_invoiced,
    base_row_invoiced,
    row_weight,
    base_tax_before_discount,
    tax_before_discount,
    ext_order_item_id, -- to check
    locked_do_invoice,
    locked_do_ship,
    price_incl_tax,
    base_price_incl_tax,
    row_total_incl_tax,
    base_row_total_incl_tax,
    -- discount_tax_compensation_amount,
    -- base_discount_tax_compensation_amount,
    -- discount_tax_compensation_invoiced,
    -- base_discount_tax_compensation_invoiced,
    -- discount_tax_compensation_refunded,
    -- base_discount_tax_compensation_refunded,
    tax_canceled,
    -- discount_tax_compensation_canceled,
    tax_refunded,
    -- base_tax_refunded,
    -- discount_refunded,
    -- base_discount_refunded,
    free_shipping,
    qty_returned,
    gift_message_id,
    gift_message_available,
    -- weee_tax_applied,
    -- weee_tax_applied_amount,
    -- weee_tax_applied_row_amount,
    -- weee_tax_disposition,
    -- weee_tax_row_disposition,
    -- base_weee_tax_applied_amount,
    -- base_weee_tax_applied_row_amnt,
    -- base_weee_tax_disposition,
    -- base_weee_tax_row_disposition,
    gw_id,
    gw_base_price,
    gw_price,
    gw_base_tax_amount,
    gw_tax_amount,
    gw_base_price_invoiced,
    gw_price_invoiced,
    gw_base_tax_amount_invoiced,
    gw_tax_amount_invoiced,
    gw_base_price_refunded,
    gw_price_refunded,
    gw_base_tax_amount_refunded,
    gw_tax_amount_refunded,
    event_id, -- to check
    giftregistry_item_id, -- to check
    is_preorder
FROM sales_order_item WHERE order_id IN ({$orderIds})
ORDER BY order_id, parent_item_id ASC;";
                } else {
                    return '';
                }
                break;

            case 'order_invoice' :
                if ($orderIds !== '') {
                    return "
SELECT
    entity_id,
    store_id, -- mapping
    base_grand_total,
    shipping_tax_amount,
    tax_amount,
    base_tax_amount,
    store_to_order_rate,
    base_shipping_tax_amount,
    base_discount_amount,
    base_to_order_rate,
    grand_total,
    shipping_amount,
    subtotal_incl_tax,
    base_subtotal_incl_tax,
    store_to_base_rate,
    base_shipping_amount,
    total_qty,
    base_to_global_rate,
    subtotal,
    base_subtotal,
    discount_amount,
    billing_address_id, -- correspondance
    is_used_for_refund,
    order_id, -- correspondance
    email_sent,
    -- send_email,
    can_void_flag,
    state, -- to check
    shipping_address_id,
    store_currency_code,
    transaction_id,
    order_currency_code,
    base_currency_code,
    global_currency_code,
    increment_id,
    created_at,
    updated_at,
    -- discount_tax_compensation_amount,
    -- base_discount_tax_compensation_amount,
    -- shipping_discount_tax_compensation_amount,
    -- base_shipping_discount_tax_compensation_amnt,
    shipping_incl_tax,
    base_shipping_incl_tax,
    base_total_refunded,
    -- discount_description,
    -- customer_note,
    -- customer_note_notify,
    base_customer_balance_amount,
    customer_balance_amount,
    base_gift_cards_amount,
    gift_cards_amount,
    gw_base_price,
    gw_price,
    gw_items_base_price,
    gw_items_price,
    gw_card_base_price,
    gw_card_price,
    gw_base_tax_amount,
    gw_tax_amount,
    gw_items_base_tax_amount,
    gw_items_tax_amount,
    gw_card_base_tax_amount,
    gw_card_tax_amount,
    base_reward_currency_amount,
    reward_currency_amount,
    reward_points_balance
    -- exported,
    -- rma_id, -- rma not imported
    -- payment_exported,
    -- customer_exported,
    -- address_exported
FROM sales_invoice WHERE order_id IN ({$orderIds});";
                } else {
                    return '';
                }
                break;

            case 'order_payment' :
                if ($orderIds !== '') {
                return "
SELECT
	parent_id, -- correspondance order_id
	base_shipping_captured,
	shipping_captured,
	amount_refunded,
	base_amount_paid,
	amount_canceled,
	base_amount_authorized,
	base_amount_paid_online,
	base_amount_refunded_online,
	base_shipping_amount,
	shipping_amount,
	amount_paid,
	amount_authorized,
	base_amount_ordered,
	base_shipping_refunded,
	shipping_refunded,
	base_amount_refunded,
	amount_ordered,
	base_amount_canceled,
	-- quote_payment_id, -- quote not imported
	additional_data,
	cc_exp_month,
	cc_ss_start_year,
	echeck_bank_name,
	method, -- mapping
	cc_debug_request_body,
	cc_secure_verify,
	protection_eligibility,
	cc_approval,
	cc_last_4 AS cc_last4,
	cc_status_description,
	echeck_type,
	cc_debug_response_serialized,
	cc_ss_start_month,
	echeck_account_type,
	-- last_trans_id, -- transaction not imported
	cc_cid_status,
	cc_owner,
	cc_type,
	po_number,
	cc_exp_year,
	cc_status,
	echeck_routing_number,
	account_status,
	anet_trans_method,
	cc_debug_response_body,
	cc_ss_issue,
	echeck_account_name,
	cc_avs_status,
	cc_number_enc,
	-- cc_trans_id, -- transaction not imported
	address_status,
	additional_information
	-- adyen_psp_reference
FROM sales_order_payment WHERE parent_id IN ({$orderIds});";
                } else {
                    return '';
                }
                break;

            case 'order_status' :
                if ($orderIds !== '') {
                return "
SELECT
	parent_id, -- correspondance order_id
    is_customer_notified,
    is_visible_on_front,
    comment,
    status, -- mapping
    created_at,
    entity_name
FROM sales_order_status_history
WHERE parent_id IN ({$orderIds});";
                } else {
                    return '';
                }
                break;

            case 'wishlist' :
                if ($customerIds !== '') {
                    $customerIds = join(', ', $this->ids['customer']);
                    return "
SELECT
    wishlist_id,
    customer_id,
    shared,
    sharing_code,
    updated_at
FROM wishlist
WHERE customer_id IN (
    SELECT entity_id FROM customer_entity WHERE entity_id IN ({$customerIds})
);";
                } else {
                    return '';
                }
                break;

            case 'wishlist_item' :
                if ($wishlistIds !== '') {
                    return "
SELECT
    wishlist_id AS wishlist_id_m2,
    product_id,
    store_id,
    added_at,
    description,
    qty,
    cpe.sku AS product_sku
FROM wishlist_item wi
LEFT JOIN catalog_product_entity cpe ON cpe.entity_id = wi.product_id
WHERE wishlist_id IN ({$wishlistIds});";
                } else {
                    return '';
                }
                break;
            case 'back_in_stock' :
                return "
SELECT
    *
FROM product_alert_stock
WHERE add_date > '{$this->goliveDate}';";
                break;

            default :
                throw new Exception('Error request type : ' . $type);
            }
    }

    /** ============================================================================================================ **/
    /** ================================================== UTILS =================================================== **/
    /** ============================================================================================================ **/
    /**
     * Execute query
     * @param $query
     * @return Zend_Db_Pdo_Statement
     * @throws Zend_Db_Adapter_Exception
     */
    protected function executeQuery($query)
    {
        //$this->log($query);
        //$this->log("================================");
        return $this->getConnexion()->fetchAll($query);
    }

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function getConnexion()
    {
        return $this->resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

    /**
     * @param string $message
     */
    protected function log($message)
    {
        $this->output->writeln($message);
    }

    /**
     * @param $filename
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function getFilepath($filename)
    {
        $dir = $this->directoryList->getPath('var').'/export/';
        if (!is_dir($dir)) {
            $this->ioFile->mkdir($dir, 0775);
        }
        return $dir . $filename;
    }

}