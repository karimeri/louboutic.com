<?php
namespace Yasumi\Provider;

use DateTime;
use DateTimeZone;
use Yasumi\Holiday;

/**
 * Class Singapore
 * Provider for all holidays in Singapore.
 * @package Yasumi\Provider
 * @author  Synolia <contact@synolia.com>
 */
class Singapore extends AbstractProvider
{
    use CommonHolidays, ChristianHolidays;

    /**
     * Code to identify this Holiday Provider. Typically this is the ISO3166 code corresponding to the respective
     * country or sub-region.
     */
    const ID = 'SG';

    /**
     * Initialize holidays for Singapore.
     *
     * @throws \InvalidArgumentException
     * @throws \Yasumi\Exception\UnknownLocaleException
     * @throws \Exception
     */
    public function initialize()
    {
        $this->timezone = 'Asia/Singapore';

        // Add common holidays
        $this->addHoliday($this->newYearsDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->internationalWorkersDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->christmasDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->goodFriday($this->year, $this->timezone, $this->locale));

        /**
         * Singapore National Day.
         *
         * Singapore National Day (also known Merdeka Day) is celebrated on 9 August each year. It commemorates the
         * independence from Britain.
         */
        $this->addHoliday(new Holiday('nationalDay', [
            'en_US' => 'National Day',
        ], new DateTime("$this->year-8-9", new DateTimeZone($this->timezone)), $this->locale));
    }
}
