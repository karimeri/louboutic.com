<?php
namespace Yasumi\Provider;

use DateTime;
use DateTimeZone;
use Yasumi\Holiday;

/**
 * Class Taiwan
 * Provider for all holidays in Taiwan.
 * @package Yasumi\Provider
 * @author  Synolia <contact@synolia.com>
 */
class Taiwan extends AbstractProvider
{
    use CommonHolidays, ChristianHolidays;

    /**
     * Code to identify this Holiday Provider. Typically this is the ISO3166 code corresponding to the respective
     * country or sub-region.
     */
    const ID = 'TW';

    /**
     * Initialize holidays for Taiwan.
     *
     * @throws \InvalidArgumentException
     * @throws \Yasumi\Exception\UnknownLocaleException
     * @throws \Exception
     */
    public function initialize()
    {
        $this->timezone = 'Asia/Taipei';

        // Add common holidays
        $this->addHoliday($this->newYearsDay($this->year, $this->timezone, $this->locale)); // Also Republic Day
        $this->addHoliday($this->internationalWorkersDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->christmasDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->goodFriday($this->year, $this->timezone, $this->locale));

        /**
         * Peace Memorial Day.
         *
         * Peace Memorial Day is celebrated on 28 February each year. It commemorates the February 28 Incident in 1947:
         * it was an anti-government uprising.
         */
        $this->addHoliday(new Holiday('peaceMemorialDay', [
            'en_US' => 'Peace Memorial Day',
        ], new DateTime("$this->year-2-28", new DateTimeZone($this->timezone)), $this->locale));

        /**
         * Taiwan National Day.
         *
         * Taiwan National Day (also known Double Tenth) is celebrated on 10 October each year. It marks the start
         * of the Wuchang Uprising of 10 October 1911.
         */
        $this->addHoliday(new Holiday('nationalDay', [
            'en_US' => 'National Day',
        ], new DateTime("$this->year-10-10", new DateTimeZone($this->timezone)), $this->locale));
    }
}
