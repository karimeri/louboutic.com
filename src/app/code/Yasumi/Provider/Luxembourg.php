<?php
namespace Yasumi\Provider;

use DateTime;
use DateTimeZone;
use Yasumi\Holiday;

/**
 * Class Luxembourg
 * Provider for all holidays in Luxembourg.
 * Source : http://www.luxembourg.public.lu/en/travailler/jours-feries-legaux/
 * @package Yasumi\Provider
 * @author  Synolia <contact@synolia.com>
 */
class Luxembourg extends AbstractProvider
{
    use CommonHolidays, ChristianHolidays;

    /**
     * Code to identify this Holiday Provider. Typically this is the ISO3166 code corresponding to the respective
     * country or sub-region.
     */
    const ID = 'LU';

    /**
     * Initialize holidays for Luxembourg.
     *
     * @throws \InvalidArgumentException
     * @throws \Yasumi\Exception\UnknownLocaleException
     * @throws \Exception
     */
    public function initialize()
    {
        $this->timezone = 'Europe/Luxembourg';

        // Add common holidays
        $this->addHoliday($this->newYearsDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->internationalWorkersDay($this->year, $this->timezone, $this->locale));

        // Add Christian holidays
        $this->addHoliday($this->easterMonday($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->ascensionDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->pentecostMonday($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->assumptionOfMary($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->allSaintsDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->christmasDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->secondChristmasDay($this->year, $this->timezone, $this->locale));

        /**
         * Luxembourg National Day.
         *
         * Luxembourg National Day is celebrated on 23 June each year.
         * It's seem to depend the true birthday of actual Grand Dukes Actually not.
         */
        $this->addHoliday(new Holiday('nationalDay', [
            'fr_FR' => 'Jour de la célébration de l\'anniversaire du Grand-Duc',
            'en_US' => 'Grand Dukes Official Birthday',
        ], new DateTime("$this->year-6-23", new DateTimeZone($this->timezone)), $this->locale));
    }
}
