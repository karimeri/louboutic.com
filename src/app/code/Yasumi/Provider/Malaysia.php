<?php
namespace Yasumi\Provider;

use DateTime;
use DateTimeZone;
use Yasumi\Holiday;

/**
 * Class Malaysia
 * Provider for all holidays in Malaysia.
 * @package Yasumi\Provider
 * @author  Synolia <contact@synolia.com>
 */
class Malaysia extends AbstractProvider
{
    use CommonHolidays, ChristianHolidays;

    /**
     * Code to identify this Holiday Provider. Typically this is the ISO3166 code corresponding to the respective
     * country or sub-region.
     */
    const ID = 'MY';

    /**
     * Initialize holidays for Malaysia.
     *
     * @throws \InvalidArgumentException
     * @throws \Yasumi\Exception\UnknownLocaleException
     * @throws \Exception
     */
    public function initialize()
    {
        $this->timezone = 'Asia/Kuala_Lumpur';

        // Add common holidays
        $this->addHoliday($this->newYearsDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->internationalWorkersDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->christmasDay($this->year, $this->timezone, $this->locale));

        /**
         * Malaysia National Day.
         *
         * Malaysia National Day (also known Merdeka Day) is celebrated on 31 August each year. It commemorates the
         * independence of the Federation of Malaya from British colonial rule in 1957.
         */
        $this->addHoliday(new Holiday('nationalDay', [
            'en_US' => 'Independence Day',
        ], new DateTime("$this->year-8-31", new DateTimeZone($this->timezone)), $this->locale));

        /**
         * Malaysia Day.
         *
         * Malaysia Day (also known Hari Malaysia) is celebrated on 16 September each year.
         */
        $this->addHoliday(new Holiday('malaysiaDay', [
            'en_US' => 'Malaysia Day',
        ], new DateTime("$this->year-9-16", new DateTimeZone($this->timezone)), $this->locale));
    }
}
