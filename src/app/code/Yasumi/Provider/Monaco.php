<?php
namespace Yasumi\Provider;

use DateTime;
use DateTimeZone;
use Yasumi\Holiday;

/**
 * Class Monaco
 * Provider for all holidays in Monaco.
 * Source : http://en.service-public-particuliers.gouv.mc/Employment/Employees/Leave-and-sickness/Public-Holidays2
 * @package Yasumi\Provider
 * @author  Synolia <contact@synolia.com>
 */
class Monaco extends AbstractProvider
{
    use CommonHolidays, ChristianHolidays;

    /**
     * Code to identify this Holiday Provider. Typically this is the ISO3166 code corresponding to the respective
     * country or sub-region.
     */
    const ID = 'MC';

    /**
     * Initialize holidays for Monaco.
     *
     * @throws \InvalidArgumentException
     * @throws \Yasumi\Exception\UnknownLocaleException
     * @throws \Exception
     */
    public function initialize()
    {
        $this->timezone = 'Europe/Monaco';

        // Add common holidays
        $this->addHoliday($this->newYearsDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->internationalWorkersDay($this->year, $this->timezone, $this->locale));

        // Add Christian holidays
        $this->addHoliday($this->easterMonday($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->ascensionDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->pentecostMonday($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->corpusChristi($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->assumptionOfMary($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->allSaintsDay($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->immaculateConception($this->year, $this->timezone, $this->locale));
        $this->addHoliday($this->christmasDay($this->year, $this->timezone, $this->locale));

        /**
         * Saint Devote Day
         *
         * Saint Devote Day is celebrated on 27 January each year.
         */
        $this->addHoliday(new Holiday('saintDevoteDay', [
            'fr_FR' => 'Sainte Dévote',
            'en_US' => 'Saint Dévote\'s Day',
        ], new DateTime("$this->year-1-27", new DateTimeZone($this->timezone)), $this->locale));

        /**
         * Monaco National Day.
         *
         * Monaco National Day is celebrated on 19 November each year and depends of the reigning Prince.
         */
        $nationalDate = $this->getNationalDay();

        $this->addHoliday(new Holiday('nationalDay', [
            'fr_FR' => 'La Fête du Prince',
            'en_US' => 'H.S.H. the Sovereign Prince\'s Day',
        ], $nationalDate, $this->locale));
    }

    /**
     * @override
     * @param        $year
     * @param        $timezone
     * @param        $locale
     * @param string $type
     * @return Holiday
     */
    public function newYearsDay($year, $timezone, $locale, $type = Holiday::TYPE_OFFICIAL)
    {
        $newYearsDate = $this->getTrueHoliday(1, 1);

        return new Holiday('newYearsDay', [], $newYearsDate, $locale, $type);
    }

    /**
     * @override
     * @param        $year
     * @param        $timezone
     * @param        $locale
     * @param string $type
     * @return Holiday
     */
    public function internationalWorkersDay($year, $timezone, $locale, $type = Holiday::TYPE_OFFICIAL)
    {
        $newYearsDate = $this->getTrueHoliday(5, 1);

        return new Holiday('internationalWorkersDay', [], $newYearsDate, $locale, $type);
    }

    /**
     * @override
     * @param        $year
     * @param        $timezone
     * @param        $locale
     * @param string $type
     * @return Holiday
     */
    public function assumptionOfMary($year, $timezone, $locale, $type = Holiday::TYPE_OFFICIAL)
    {
        $newYearsDate = $this->getTrueHoliday(8, 15);

        return new Holiday('assumptionOfMary', [], $newYearsDate, $locale, $type);
    }

    /**
     * @override
     * @param        $year
     * @param        $timezone
     * @param        $locale
     * @param string $type
     * @return Holiday
     */
    public function allSaintsDay($year, $timezone, $locale, $type = Holiday::TYPE_OFFICIAL)
    {
        $newYearsDate = $this->getTrueHoliday(11, 1);

        return new Holiday('allSaintsDay', [], $newYearsDate, $locale, $type);
    }

    /**
     * @override
     * @param        $year
     * @param        $timezone
     * @param        $locale
     * @param string $type
     * @return Holiday
     */
    public function christmasDay($year, $timezone, $locale, $type = Holiday::TYPE_OFFICIAL)
    {
        $newYearsDate = $this->getTrueHoliday(12, 25);

        return new Holiday('christmasDay', [], $newYearsDate, $locale, $type);
    }

    /**
     * @return DateTime
     */
    public function getNationalDay()
    {
        return $this->getTrueHoliday(11, 19);
    }

    /**
     * @param $month
     * @param $day
     * @return DateTime
     */
    private function getTrueHoliday($month, $day)
    {
        $date = new DateTime("$this->year-$month-$day", new DateTimeZone($this->timezone));

        if ($date->format('w') === '0') {
            $day++;
        }

        return new DateTime("$this->year-$month-$day", new DateTimeZone($this->timezone));
    }
}
