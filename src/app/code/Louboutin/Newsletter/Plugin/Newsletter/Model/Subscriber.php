<?php

namespace Louboutin\Newsletter\Plugin\Newsletter\Model;

use Magento\Store\Model\ScopeInterface;

/**
 * Class Subscriber
 * @package Louboutin\Newsletter\Plugin\Newsletter\Model
 */
class Subscriber
{
    const XML_PATH_NEWSLETTER_SUBSCRIPTION_DISABLE_SUCCESS_EMAIL = 'newsletter/subscription/disable_success_email';

    const XML_PATH_NEWSLETTER_SUBSCRIPTION_DISABLE_UN_EMAIL = 'newsletter/subscription/disable_un_email';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Subscriber constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\Newsletter\Model\Subscriber $subject
     */
    public function beforeSendConfirmationSuccessEmail(\Magento\Newsletter\Model\Subscriber $subject) // @codingStandardsIgnoreLine
    {
        if ($this->scopeConfig->isSetFlag(
            self::XML_PATH_NEWSLETTER_SUBSCRIPTION_DISABLE_SUCCESS_EMAIL,
            ScopeInterface::SCOPE_STORES
        )) {
            $subject->setImportMode(true);
        }
    }

    /**
     * @param \Magento\Newsletter\Model\Subscriber $subject
     */
    public function beforeSendUnsubscriptionEmail(\Magento\Newsletter\Model\Subscriber $subject) // @codingStandardsIgnoreLine
    {
        if ($this->scopeConfig->isSetFlag(
            self::XML_PATH_NEWSLETTER_SUBSCRIPTION_DISABLE_UN_EMAIL,
            ScopeInterface::SCOPE_STORES
        )) {
            $subject->setImportMode(true);
        }
    }
}
