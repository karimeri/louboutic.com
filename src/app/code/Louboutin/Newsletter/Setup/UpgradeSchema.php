<?php

namespace Louboutin\Newsletter\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Louboutin\Newsletter\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE_NEWSLETTER_SUBSCRIBER = 'newsletter_subscriber';

    const COLUMN_COLLECTION_GENDER = 'subscriber_collection_gender';

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->addCollectionGenderColumn($installer);
        }

        $installer->endSetup();
    }

    /**
     * @param SchemaSetupInterface $installer
     */
    private function addCollectionGenderColumn(SchemaSetupInterface $installer)
    {
        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_NEWSLETTER_SUBSCRIBER),
            self::COLUMN_COLLECTION_GENDER,
            [
                'type'     => Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => false,
                'comment'  => 'Collection Gender (1: Men, 2: Women)',
                'default'  => 2
            ]
        );
    }
}
