<?php

namespace Louboutin\Security\Api;

use Louboutin\Security\Api\Data\RuleInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface RuleRepositoryInterface
 * @package Louboutin\Security\Api
 */
interface RuleRepositoryInterface
{
    /**
     * @param RuleInterface $rule
     * @return RuleInterface
     */
    public function save(RuleInterface $rule);

    /**
     * @param int $ruleId
     * @return RuleInterface
     */
    public function getById($ruleId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Louboutin\Security\Api\Data\RuleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param RuleInterface $rule
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(RuleInterface $rule);

    /**
     * @param int $ruleId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($ruleId);
}
