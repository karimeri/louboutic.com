<?php

namespace Louboutin\Security\Api;

use Louboutin\Security\Api\Data\ConfigInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ConfigRepositoryInterface
 * @package Louboutin\Security\Api
 */
interface ConfigRepositoryInterface
{
    /**
     * @param ConfigInterface $config
     * @return ConfigInterface
     */
    public function save(ConfigInterface $config);

    /**
     * @param int $topicId
     * @return ConfigInterface
     */
    public function getById($topicId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Louboutin\Security\Api\Data\ConfigSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param ConfigInterface $config
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ConfigInterface $config);

    /**
     * @param int $configId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($configId);
}
