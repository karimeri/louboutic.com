<?php

namespace Louboutin\Security\Api;

use Louboutin\Security\Api\Data\RoleInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface RoleRepositoryInterface
 * @package Louboutin\Security\Api
 */
interface RoleRepositoryInterface
{
    /**
     * @param RoleInterface $role
     * @return RoleInterface
     */
    public function save(RoleInterface $role);

    /**
     * @param int $roleId
     * @return RoleInterface
     */
    public function getById($roleId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Louboutin\Security\Api\Data\RoleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param RoleInterface $role
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(RoleInterface $role);

    /**
     * @param int $roleId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($roleId);
}
