<?php

namespace Louboutin\Security\Api\Data;

/**
 * Interface RuleInterface
 * @package Louboutin\Security\Api\Data
 */
interface RuleInterface
{
    const RULE_ID       = 'rule_id';
    const ROLE_ID       = 'role_id';
    const RESOURCE_ID   = 'resource_id';
    const PRIVILEGES    = 'privileges';
    const PERMISSION    = 'permission';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $id
     * @return RuleInterface
     */
    public function setId($id);

    /**
     * @return int|null
     */
    public function getRoleId();

    /**
     * @param int $roleId
     * @return RuleInterface
     */
    public function setRoleId($roleId);

    /**
     * @return string|null
     */
    public function getResourceId();

    /**
     * @param string $resourceId
     * @return RuleInterface
     */
    public function setResourceId($resourceId);

    /**
     * @return string|null
     */
    public function getPrivileges();

    /**
     * @param string $privileges
     * @return RuleInterface
     */
    public function setPrivileges($privileges);

    /**
     * @return string|null
     */
    public function getPermission();

    /**
     * @param string $permission
     * @return RuleInterface
     */
    public function setPermission($permission);
}
