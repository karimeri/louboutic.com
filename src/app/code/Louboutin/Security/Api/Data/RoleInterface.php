<?php

namespace Louboutin\Security\Api\Data;

/**
 * Interface RoleInterface
 * @package Louboutin\Security\Api\Data
 */
interface RoleInterface
{
    const ROLE_ID           = 'role_id';
    const PARENT_ID         = 'parent_id';
    const TREE_LEVEL        = 'tree_level';
    const SORT_ORDER        = 'sort_order';
    const ROLE_TYPE         = 'role_type';
    const USER_ID           = 'user_id';
    const USER_TYPE         = 'user_type';
    const ROLE_NAME         = 'role_name';
    const GWS_IS_ALL        = 'gws_is_all';
    const GWS_WEBSITES      = 'gws_websites';
    const GWS_STORE_GROUPS  = 'gws_store_groups';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $id
     * @return RoleInterface
     */
    public function setId($id);

    /**
     * @return int|null
     */
    public function getParentId();

    /**
     * @param int $parentId
     * @return RoleInterface
     */
    public function setParentId($parentId);

    /**
     * @return int|null
     */
    public function getTreeLevel();

    /**
     * @param int $treeLevel
     * @return RoleInterface
     */
    public function setTreeLevel($treeLevel);

    /**
     * @return int|null
     */
    public function getSortOrder();

    /**
     * @param int $sortOrder
     * @return RoleInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * @return string|null
     */
    public function getRoleType();

    /**
     * @param string $roleType
     * @return RoleInterface
     */
    public function setRoleType($roleType);

    /**
     * @return int|null
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return RoleInterface
     */
    public function setUserId($userId);

    /**
     * @return string|null
     */
    public function getUserType();

    /**
     * @param string $userType
     * @return RoleInterface
     */
    public function setUserType($userType);

    /**
     * @return string|null
     */
    public function getRoleName();

    /**
     * @param string $roleName
     * @return RoleInterface
     */
    public function setRoleName($roleName);

    /**
     * @return int|null
     */
    public function getGwsIsAll();

    /**
     * @param int $gwsIsAll
     * @return RoleInterface
     */
    public function setGwsIsAll($gwsIsAll);

    /**
     * @return string|null
     */
    public function getGwsWebsites();

    /**
     * @param string $gwsWebsites
     * @return RoleInterface
     */
    public function setGwsWebsites($gwsWebsites);

    /**
     * @return string|null
     */
    public function getGwsStoreGroups();

    /**
     * @param string $gwsStoreGroups
     * @return RoleInterface
     */
    public function setGwsStoreGroups($gwsStoreGroups);
}
