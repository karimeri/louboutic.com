<?php

namespace Louboutin\Security\Api\Data;

/**
 * Interface ConfigInterface
 * @package Louboutin\Security\Api\Data
 */
interface ConfigInterface
{
    const CONFIG_ID = 'config_id';
    const SCOPE     = 'scope';
    const SCOPE_ID  = 'scope_id';
    const PATH      = 'path';
    const VALUE     = 'value';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $id
     * @return ConfigInterface
     */
    public function setId($id);

    /**
     * @return string|null
     */
    public function getScope();

    /**
     * @param string $scope
     * @return ConfigInterface
     */
    public function setScope($scope);

    /**
     * @return int|null
     */
    public function getScopeId();

    /**
     * @param int $scopeId
     * @return ConfigInterface
     */
    public function setScopeId($scopeId);

    /**
     * @return string|null
     */
    public function getPath();

    /**
     * @param string $path
     * @return ConfigInterface
     */
    public function setPath($path);

    /**
     * @return string|null
     */
    public function getValue();

    /**
     * @param string $value
     * @return ConfigInterface
     */
    public function setValue($value);
}
