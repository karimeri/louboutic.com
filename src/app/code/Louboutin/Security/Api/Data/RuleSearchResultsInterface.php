<?php

namespace Louboutin\Security\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface RuleSearchResultsInterface
 * @package Louboutin\Security\Api\Data
 */
interface RuleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Louboutin\Security\Api\Data\RuleInterface[]
     */
    public function getItems();

    /**
     * @param \Louboutin\Security\Api\Data\RuleInterface[] $items
     * @return RuleSearchResultsInterface
     */
    public function setItems(array $items);
}
