<?php

namespace Louboutin\Security\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface RoleSearchResultsInterface
 * @package Louboutin\Security\Api\Data
 */
interface RoleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Louboutin\Security\Api\Data\RoleInterface[]
     */
    public function getItems();

    /**
     * @param \Louboutin\Security\Api\Data\RoleInterface[] $items
     * @return RoleSearchResultsInterface
     */
    public function setItems(array $items);
}
