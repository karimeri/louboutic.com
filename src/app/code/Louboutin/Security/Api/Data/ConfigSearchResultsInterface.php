<?php

namespace Louboutin\Security\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Class ConfigSearchResultsInterface
 * @package Louboutin\Security\Api\Data
 */
interface ConfigSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Louboutin\Security\Api\Data\ConfigInterface[]
     */
    public function getItems();

    /**
     * @param \Louboutin\Security\Api\Data\ConfigInterface[] $items
     * @return ConfigSearchResultsInterface
     */
    public function setItems(array $items);
}
