<?php

namespace Louboutin\Security\Setup;

use Louboutin\Security\Api\Data\ConfigInterface;
use Louboutin\Security\Model\ResourceModel\Config;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Louboutin\Security\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->createConfigCheckTable($setup);

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function createConfigCheckTable(SchemaSetupInterface $setup)
    {
        if (!$setup->getConnection()->isTableExists($setup->getTable(Config::TABLE_NAME))) {
            $tableConfig = $setup->getConnection()->newTable($setup->getTable(Config::TABLE_NAME));

            $tableConfig->addColumn(
                ConfigInterface::CONFIG_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Config Id'
            )->addColumn(
                ConfigInterface::SCOPE,
                Table::TYPE_TEXT,
                8,
                ['nullable' => false, 'default' => 'default'],
                'Config Scope'
            )->addColumn(
                ConfigInterface::SCOPE_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => '0'],
                'Config Scope Id'
            )->addColumn(
                ConfigInterface::PATH,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => 'general'],
                'Config Path'
            )->addColumn(
                ConfigInterface::VALUE,
                Table::TYPE_TEXT,
                '64k',
                [],
                'Config Value'
            )->addIndex(
                $setup->getIdxName(
                    Config::TABLE_NAME,
                    [ConfigInterface::SCOPE, ConfigInterface::SCOPE_ID, ConfigInterface::PATH],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                [ConfigInterface::SCOPE, ConfigInterface::SCOPE_ID, ConfigInterface::PATH],
                ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
            )->setComment(
                'Louboutin Security Config Data'
            );

            $setup->getConnection()->createTable($tableConfig);
        }
    }
}
