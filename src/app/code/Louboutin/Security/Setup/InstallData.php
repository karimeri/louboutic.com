<?php

namespace Louboutin\Security\Setup;

use Louboutin\Security\Model\ConfigManager;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 * @package Louboutin\Security\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var \Louboutin\Security\Model\ConfigManager
     */
    protected $configManager;

    /**
     * InstallData constructor.
     * @param \Louboutin\Security\Model\ConfigManager $configManager
     */
    public function __construct(
        ConfigManager $configManager
    ) {
        $this->configManager = $configManager;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->configManager->updateReferenceValues();
    }
}
