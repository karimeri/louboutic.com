<?php

namespace Louboutin\Security\Setup;

use Louboutin\Security\Api\Data\RoleInterface;
use Louboutin\Security\Api\Data\RuleInterface;
use Louboutin\Security\Model\ResourceModel\Role;
use Louboutin\Security\Model\ResourceModel\Rule;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Louboutin\Security\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->createRoleCheckTable($setup);
            $this->createRuleCheckTable($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function createRoleCheckTable(SchemaSetupInterface $setup)
    {
        if (!$setup->getConnection()->isTableExists($setup->getTable(Role::TABLE_NAME))) {
            $tableRole = $setup->getConnection()->newTable($setup->getTable(Role::TABLE_NAME));

            $tableRole->addColumn(
                RoleInterface::ROLE_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Role ID'
            )->addColumn(
                RoleInterface::PARENT_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Parent Role ID'
            )->addColumn(
                RoleInterface::TREE_LEVEL,
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Role Tree Level'
            )->addColumn(
                RoleInterface::SORT_ORDER,
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Role Sort Order'
            )->addColumn(
                RoleInterface::ROLE_TYPE,
                Table::TYPE_TEXT,
                1,
                ['nullable' => false, 'default' => '0'],
                'Role Type'
            )->addColumn(
                RoleInterface::USER_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'User ID'
            )->addColumn(
                RoleInterface::USER_TYPE,
                Table::TYPE_TEXT,
                16,
                ['nullable' => true, 'default' => null],
                'User Type'
            )->addColumn(
                RoleInterface::ROLE_NAME,
                Table::TYPE_TEXT,
                50,
                ['nullable' => true, 'default' => null],
                'Role Name'
            )->addColumn(
                RoleInterface::GWS_IS_ALL,
                Table::TYPE_SMALLINT,
                1,
                ['nullable' => false, 'default' => '1'],
                'Yes/No Flag'
            )->addColumn(
                RoleInterface::GWS_WEBSITES,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'Comma-separated Website Ids'
            )->addColumn(
                RoleInterface::GWS_STORE_GROUPS,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'Comma-separated Store Groups Ids'
            )->addIndex(
                $setup->getIdxName(Role::TABLE_NAME, [RoleInterface::PARENT_ID, RoleInterface::SORT_ORDER]),
                [RoleInterface::PARENT_ID, RoleInterface::SORT_ORDER]
            )->addIndex(
                $setup->getIdxName(Role::TABLE_NAME, [RoleInterface::TREE_LEVEL]),
                [RoleInterface::TREE_LEVEL]
            )->setComment(
                'Louboutin Security Admin Role Table'
            );

            $setup->getConnection()->createTable($tableRole);
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    private function createRuleCheckTable(SchemaSetupInterface $setup)
    {
        if (!$setup->getConnection()->isTableExists($setup->getTable(Rule::TABLE_NAME))) {
            $tableRule = $setup->getConnection()->newTable($setup->getTable(Rule::TABLE_NAME));

            $tableRule->addColumn(
                RuleInterface::RULE_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Rule ID'
            )->addColumn(
                RuleInterface::ROLE_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Role ID'
            )->addColumn(
                RuleInterface::RESOURCE_ID,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'Resource ID'
            )->addColumn(
                RuleInterface::PRIVILEGES,
                Table::TYPE_TEXT,
                20,
                ['nullable' => true],
                'Privileges'
            )->addColumn(
                RuleInterface::PERMISSION,
                Table::TYPE_TEXT,
                10,
                [],
                'Permission'
            )->addIndex(
                $setup->getIdxName(Rule::TABLE_NAME, [RuleInterface::RESOURCE_ID, RuleInterface::ROLE_ID]),
                [RuleInterface::RESOURCE_ID, RuleInterface::ROLE_ID]
            )->addIndex(
                $setup->getIdxName(Rule::TABLE_NAME, [RuleInterface::ROLE_ID, RuleInterface::RESOURCE_ID]),
                [RuleInterface::ROLE_ID, RuleInterface::RESOURCE_ID]
            )->setComment(
                'Louboutin Security Admin Rule Table'
            );

            $setup->getConnection()->createTable($tableRule);
        }
    }
}
