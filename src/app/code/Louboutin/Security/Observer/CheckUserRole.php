<?php

namespace Louboutin\Security\Observer;

use Louboutin\Security\Helper\Alerting as AlertingHelper;
use Magento\Authorization\Model\ResourceModel\Role\CollectionFactory as RoleCollectionFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Project\Core\Manager\EnvironmentManager;
use Psr\Log\LoggerInterface;

/**
 * Class CheckUserRole
 * @package Louboutin\Security\Observer
 */
class CheckUserRole implements ObserverInterface
{
    /**
     * @var \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory
     */
    protected $userRoleCollectionFactory;

    /**
     * @var \Louboutin\Security\Helper\Alerting
     */
    protected $alertingHelper;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * CheckUserRole constructor.
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRoleCollectionFactory
     * @param \Louboutin\Security\Helper\Alerting $alertingHelper
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        RoleCollectionFactory $userRoleCollectionFactory,
        AlertingHelper $alertingHelper,
        EnvironmentManager $environmentManager,
        TimezoneInterface $timezone,
        LoggerInterface $logger
    ) {
        $this->userRoleCollectionFactory = $userRoleCollectionFactory;
        $this->alertingHelper = $alertingHelper;
        $this->environmentManager = $environmentManager;
        $this->timezone = $timezone;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            if ($this->alertingHelper->isEnabled()) {
                /* @var $user \Magento\User\Model\User */
                $user = $observer->getEvent()->getObject();
                $previousRoleId = $user->getRole()->getOrigData('role_id');
                $newRoleId = $observer->getDataObject()->getRoleId();

                // send an alert mail if user changes his role
                if ($previousRoleId && $previousRoleId != $newRoleId) {
                    /** @var \Magento\Authorization\Model\ResourceModel\Role\Collection $userRoleCollection */
                    $userRoleCollection = $this->userRoleCollectionFactory->create();
                    /** @var \Magento\Authorization\Model\Role $newUserRole */
                    $newUserRole = $userRoleCollection
                        ->addFieldToFilter('role_id', $newRoleId)
                        ->getFirstItem(); // phpcs:ignore
                    $newRoleName = $newUserRole->getRoleName();
                    $previousRoleName = $user->getRole()->getOrigData('role_name');

                    $templateVars = [
                        'environment' => $this->environmentManager->getEnvironment(),
                        'username' => $user->getName(),
                        'firstName' => $user->getFirstName(),
                        'lastName' => $user->getLastName(),
                        'previousRole' => $previousRoleName,
                        'newRole' => $newRoleName,
                        'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
                    ];
                    $alert =  $this->alertingHelper;
                    $this->alertingHelper
                        ->alert(
                            $alert::XML_PATH_USER_ROLE_CHANGE_ALERTING_EMAIL_TEMPLATE,
                            $templateVars
                        );
                }
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
