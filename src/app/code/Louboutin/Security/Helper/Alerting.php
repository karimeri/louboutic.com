<?php

namespace Louboutin\Security\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Alerting
 * @package Louboutin\Security\Helper
 */
class Alerting extends AbstractHelper
{
    const XML_PATH_ALERTING_ENABLED
        = 'louboutin_security/alerting/enabled';
    const XML_PATH_ALERTING_EMAIL_SENDER
        = 'louboutin_security/alerting/email_sender';
    const XML_PATH_ALERTING_EMAIL_RECIPIENT
        = 'louboutin_security/alerting/email_recipient';
    const XML_PATH_ALERTING_EMAIL_COPY
        = 'louboutin_security/alerting/email_copy';
    const XML_PATH_ALERTING_EMAIL_COPY_METHOD
        = 'louboutin_security/alerting/copy_method';
    const XML_PATH_CONFIG_CHANGE_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/config_change_email_template';
    const XML_PATH_USER_ROLE_CHANGE_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/user_role_change_email_template';
    const XML_PATH_NEW_USER_ROLE_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/new_user_role_email_template';
    const XML_PATH_DELETED_USER_ROLE_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/deleted_user_role_email_template';
    const XML_PATH_USER_ROLE_SCOPE_CHANGE_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/user_role_scope_change_email_template';
    const XML_PATH_GRANTED_USER_RULES_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/granted_user_rules_email_template';
    const XML_PATH_REVOKED_USER_RULES_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_security/alerting/revoked_user_rules_email_template';

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * Alerting constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        StateInterface $inlineTranslation
    ) {
        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ALERTING_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
    }

    /**
     * @param string $templatePath
     * @param array $templateVars
     * @return $this
     */
    public function alert($templatePath, $templateVars)
    {
        try {
            $this->inlineTranslation->suspend();

            $storeId = $this->storeManager->getStore()->getId();

            $sender = $this->scopeConfig->getValue(
                self::XML_PATH_ALERTING_EMAIL_SENDER,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                $storeId
            );
            $sendFrom = [
                'email' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/email',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $storeId
                ),
                'name' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/name',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $storeId
                ),
            ];

            $copyTo = $this->getEmails(self::XML_PATH_ALERTING_EMAIL_COPY, $storeId);
            $copyMethod = $this->scopeConfig->getValue(
                self::XML_PATH_ALERTING_EMAIL_COPY_METHOD,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                $storeId
            );
            $bcc = [];
            if ($copyTo && $copyMethod == 'bcc') {
                $bcc = $copyTo;
            }

            $sendTo = [
                [
                    'email' => $this->scopeConfig->getValue(
                        self::XML_PATH_ALERTING_EMAIL_RECIPIENT,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                        $storeId
                    ),
                    'name' => null,
                ]
            ];
            if ($copyTo && $copyMethod == 'copy') {
                foreach ($copyTo as $email) {
                    $sendTo[] = ['email' => $email, 'name' => null];
                }
            }

            $template = $this->getTemplate($templatePath, $storeId);
            foreach ($sendTo as $recipient) {
                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                        'store' => Store::DEFAULT_STORE_ID
                    ]
                )->setTemplateVars(
                    $templateVars
                )->setReplyTo(
                    $sendFrom['email'],
                    $sendFrom['name']
                )->setFrom(
                    $sendFrom
                )->addTo(
                    $recipient['email'],
                    $recipient['name']
                )->addBcc(
                    $bcc
                )->getTransport();

                $transport->sendMessage();
            }

            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }
        return $this;
    }

    /**
     * @param string $configPath
     * @param null|string|bool|int|Store $storeId
     * @return array|false
     */
    protected function getEmails($configPath, $storeId)
    {
        $data = $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

    /**
     * @param string $path
     * @param int $storeId
     * @return string
     */
    protected function getTemplate($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
    }
}
