<?php

namespace Louboutin\Security\Console\Command;

use Louboutin\Security\Model\ConfigManager;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckConfigValues
 * @package Louboutin\Security\Console\Command
 */
class CheckConfigValues extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Louboutin\Security\Model\ConfigManager
     */
    protected $configManager;

    /**
     * CheckConfigValues constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Louboutin\Security\Model\ConfigManager $configManager
     * @param null $name
     */
    public function __construct(
        State $state,
        ConfigManager $configManager,
        $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->configManager = $configManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:security:check-config-values')->setDescription('Check config values');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $output->writeln("<info>Check config values ...</info>");

            $this->configManager->checkConfigValues();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
