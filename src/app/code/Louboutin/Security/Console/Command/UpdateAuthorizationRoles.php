<?php

namespace Louboutin\Security\Console\Command;

use Louboutin\Security\Model\AuthorizationManager;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateAuthorizationRoles
 * @package Louboutin\Security\Console\Command
 */
class UpdateAuthorizationRoles extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Louboutin\Security\Model\AuthorizationManager
     */
    protected $authorizationManager;

    /**
     * UpdateAuthorizationRoles constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Louboutin\Security\Model\AuthorizationManager $authorizationManager
     * @param string|null $name
     */
    public function __construct(
        State $state,
        AuthorizationManager $authorizationManager,
        string $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->authorizationManager = $authorizationManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:security:update-authorization-roles')->setDescription('Update authorization roles');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $output->writeln("<info>Update authorization roles ...</info>");

            $this->authorizationManager->updateAuthorizationRoles();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
