<?php

namespace Louboutin\Security\Console\Command;

use Louboutin\Security\Model\AuthorizationManager;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckAuthorizationRoles
 * @package Louboutin\Security\Console\Command
 */
class CheckAuthorizationRoles extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Louboutin\Security\Model\AuthorizationManager
     */
    protected $authorizationManager;

    /**
     * CheckAuthorizationRoles constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Louboutin\Security\Model\AuthorizationManager $authorizationManager
     * @param string|null $name
     */
    public function __construct(
        State $state,
        AuthorizationManager $authorizationManager,
        string $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->authorizationManager = $authorizationManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:security:check-authorization-roles')->setDescription('Check authorization roles');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $output->writeln("<info>Check authorization roles ...</info>");

            $this->authorizationManager->checkAuthorizationRoles();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
