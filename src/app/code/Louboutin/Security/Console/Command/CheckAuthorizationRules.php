<?php

namespace Louboutin\Security\Console\Command;

use Louboutin\Security\Model\AuthorizationManager;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckAuthorizationRules
 * @package Louboutin\Security\Console\Command
 */
class CheckAuthorizationRules extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Louboutin\Security\Model\AuthorizationManager
     */
    protected $authorizationManager;

    /**
     * CheckAuthorizationRules constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Louboutin\Security\Model\AuthorizationManager $authorizationManager
     * @param string|null $name
     */
    public function __construct(
        State $state,
        AuthorizationManager $authorizationManager,
        string $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->authorizationManager = $authorizationManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:security:check-authorization-rules')->setDescription('Check authorization rules');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $output->writeln("<info>Check authorization rules ...</info>");

            $this->authorizationManager->checkAuthorizationRules();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
