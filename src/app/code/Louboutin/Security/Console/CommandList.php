<?php

namespace Louboutin\Security\Console;

use Magento\Framework\Console\CommandListInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class CommandList
 * @package Louboutin\Security\Console
 */
class CommandList implements CommandListInterface
{
    /**
     * Object Manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Gets list of command classes
     *
     * @return string[]
     */
    protected function getCommandsClasses()
    {
        return [
            'Louboutin\Security\Console\Command\UpdateConfigValues',
            'Louboutin\Security\Console\Command\CheckConfigValues',
            'Louboutin\Security\Console\Command\UpdateAuthorizationRoles',
            'Louboutin\Security\Console\Command\CheckAuthorizationRoles',
            'Louboutin\Security\Console\Command\UpdateAuthorizationRules',
            'Louboutin\Security\Console\Command\CheckAuthorizationRules',
        ];
    }

    /**
     * @return array|\Symfony\Component\Console\Command\Command[]
     * @throws \Exception
     */
    public function getCommands()
    {
        $commands = [];
        foreach ($this->getCommandsClasses() as $class) {
            if (class_exists($class)) {
                $commands[] = $this->objectManager->get($class);
            } else {
                throw new \Exception('Class ' . $class . ' does not exist');
            }
        }
        return $commands;
    }
}
