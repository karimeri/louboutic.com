<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\ConfigRepositoryInterface;
use Louboutin\Security\Api\Data\ConfigInterface;
use Louboutin\Security\Api\Data\ConfigInterfaceFactory;
use Louboutin\Security\Helper\Alerting as AlertingHelper;
use Louboutin\Security\Model\Logger\Handler\Reporting as ReportingHandler;
use Louboutin\Security\Model\ResourceModel\Config as ConfigResource;
use Louboutin\Security\Model\ResourceModel\Config\CollectionFactory as ConfigCollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\Dir\Reader as ModuleDirReader;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Xml\Parser as XmlParser;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Manager\EnvironmentManager;
use Psr\Log\LoggerInterface;

/**
 * Class ConfigManager
 * @package Louboutin\Security\Model
 */
class ConfigManager
{
    private static $referenceConfigFile = 'louboutin_config_data.xml';

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    protected $moduleDirReader;

    /**
     * @var \Magento\Framework\Xml\Parser
     */
    protected $xmlParser;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Louboutin\Security\Api\Data\ConfigInterfaceFactory
     */
    protected $configInterfaceFactory;

    /**
     * @var \Louboutin\Security\Api\ConfigRepositoryInterface
     */
    protected $configRepository;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Config\CollectionFactory
     */
    protected $configCollectionFactory;

    /**
     * @var \Louboutin\Security\Helper\Alerting
     */
    protected $alertingHelper;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Config
     */
    protected $resource;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Louboutin\Security\Model\Logger\Handler\Reporting
     */
    protected $reportingHandler;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * ConfigManager constructor.
     * @param \Magento\Framework\Module\Dir\Reader $moduleDirReader
     * @param \Magento\Framework\Xml\Parser $xmlParser
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Louboutin\Security\Api\Data\ConfigInterfaceFactory $configInterfaceFactory
     * @param \Louboutin\Security\Api\ConfigRepositoryInterface $configRepository
     * @param \Louboutin\Security\Model\ResourceModel\Config\CollectionFactory $collectionFactory
     * @param \Louboutin\Security\Helper\Alerting $alertingHelper
     * @param \Louboutin\Security\Model\ResourceModel\Config $resource
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Louboutin\Security\Model\Logger\Handler\Reporting $reportingHandler
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        ModuleDirReader $moduleDirReader,
        XmlParser $xmlParser,
        ScopeConfigInterface $scopeConfig,
        ConfigInterfaceFactory $configInterfaceFactory,
        ConfigRepositoryInterface $configRepository,
        ConfigCollectionFactory $collectionFactory,
        AlertingHelper $alertingHelper,
        ConfigResource $resource,
        EnvironmentManager $environmentManager,
        ReportingHandler $reportingHandler,
        StoreManagerInterface $storeManager,
        TimezoneInterface $timezone,
        LoggerInterface $logger
    ) {
        $this->moduleDirReader = $moduleDirReader;
        $this->xmlParser = $xmlParser;
        $this->scopeConfig = $scopeConfig;
        $this->configInterfaceFactory = $configInterfaceFactory;
        $this->configRepository = $configRepository;
        $this->configCollectionFactory = $collectionFactory;
        $this->alertingHelper = $alertingHelper;
        $this->resource = $resource;
        $this->environmentManager = $environmentManager;
        $this->reportingHandler = $reportingHandler;
        $this->storeManager = $storeManager;
        $this->timezone = $timezone;
        $this->logger = $logger;
    }

    public function updateReferenceValues()
    {
        $connection = $this->resource->getConnection();
        $connection->truncateTable(ConfigResource::TABLE_NAME);

        $configs = $this->getFileReferenceConfigs();
        /** @var \DOMElement $config */
        foreach ($configs as $config) {
            $scope = $config->getElementsByTagName('scope')->item(0)->nodeValue;
            $scopeCode = $config->getElementsByTagName('scope_id')->item(0)->nodeValue;
            $path = $config->getElementsByTagName('path')->item(0)->nodeValue;

            $scopeId = $this->getScopeId($scope, $scopeCode);

            if (!is_null($this->scopeConfig->getValue($path, $scope, $scopeId))) {
                $savedValue = $this->scopeConfig->getValue($path, $scope, $scopeId);

                /** @var ConfigInterface $referencedConfig */
                $referencedConfig = $this->configInterfaceFactory->create();
                $referencedConfig->setScope($scope);
                $referencedConfig->setScopeId($scopeId);
                $referencedConfig->setPath($path);
                $referencedConfig->setValue($savedValue);

                // phpcs:ignore Ecg.Performance.Loop.ModelLSD
                $this->configRepository->save($referencedConfig);
            }
        }
    }

    public function checkConfigValues()
    {
        $configs = $this->getFileReferenceConfigs();
        /** @var \DOMElement $config */
        foreach ($configs as $config) {
            $scope = $config->getElementsByTagName('scope')->item(0)->nodeValue;
            $scopeCode = $config->getElementsByTagName('scope_id')->item(0)->nodeValue;
            $path = $config->getElementsByTagName('path')->item(0)->nodeValue;

            $scopeId = $this->getScopeId($scope, $scopeCode);

            if (!is_null($this->scopeConfig->getValue($path, $scope, $scopeId))) {
                $savedValue = $this->scopeConfig->getValue($path, $scope, $scopeId);

                /** @var ConfigInterface $referencedConfig */
                $referencedConfig = $this->configCollectionFactory->create()
                    ->addFieldToFilter(ConfigInterface::SCOPE, $scope)
                    ->addFieldToFilter(ConfigInterface::SCOPE_ID, $scopeId)
                    ->addFieldToFilter(ConfigInterface::PATH, $path)
                    ->getFirstItem(); // phpcs:ignore

                $referencedValue = $referencedConfig->getValue();

                if ($savedValue != $referencedValue) {
                    $templateVars = [
                        'environment' => $this->environmentManager->getEnvironment(),
                        'scope' => $scope,
                        'scopeId' => $scopeId,
                        'path' => $path,
                        'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s'),
                        'reportId' => $this->reportingHandler->getReportId()
                    ];
                    // Send alert mail
                    $this->alertingHelper
                        ->alert($this->alertingHelper::XML_PATH_CONFIG_CHANGE_ALERTING_EMAIL_TEMPLATE, $templateVars);

                    // Add report file in var/loubisec
                    $this->logger->alert('A config value have been changed.');
                    $this->logger->alert('Scope : ' . $scope);
                    $this->logger->alert('Scope Id : ' . $scopeId);
                    $this->logger->alert('Path : ' . $path);
                    $this->logger->alert('Reference value : ' . $referencedValue);
                    $this->logger->alert('New value : ' . $savedValue);
                }
            }
        }
    }

    /**
     * @return \DOMNodeList
     */
    private function getFileReferenceConfigs()
    {
        $moduleDir = $this->moduleDirReader->getModuleDir('etc', 'Louboutin_Security');
        $filePath = $moduleDir . '/'. self::$referenceConfigFile;

        $dom = $this->xmlParser->load($filePath)->getDom();
        $configs = $dom->getElementsByTagName('config');

        return $configs;
    }

    /**
     * @param $scope
     * @param $scopeCode
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getScopeId($scope, $scopeCode)
    {
        if ($scope == ScopeInterface::SCOPE_WEBSITES) {
            $scopeId = $this->storeManager->getWebsite($scopeCode)->getId();
        } elseif ($scope == ScopeInterface::SCOPE_STORES) {
            $scopeId = $this->storeManager->getStore($scopeCode)->getId();
        } else {
            $scopeId = $scopeCode;
        }
        return $scopeId;
    }
}
