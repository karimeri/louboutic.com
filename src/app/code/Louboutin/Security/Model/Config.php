<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\Data\ConfigInterface;
use Louboutin\Security\Model\ResourceModel\Config as ConfigResourceModel;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Config
 * @package Louboutin\Security\Model
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Config extends AbstractModel implements ConfigInterface, ExtensibleDataInterface
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(ConfigResourceModel::class);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::CONFIG_ID);
    }

    /**
     * @param int $id
     * @return ConfigInterface
     */
    public function setId($id)
    {
        return $this->setData(self::CONFIG_ID, $id);
    }

    /**
     * @return string|null
     */
    public function getScope()
    {
        return $this->getData(self::SCOPE);
    }

    /**
     * @param string $scope
     * @return ConfigInterface
     */
    public function setScope($scope)
    {
        return $this->setData(self::SCOPE, $scope);
    }

    /**
     * @return int|null
     */
    public function getScopeId()
    {
        return $this->getData(self::SCOPE_ID);
    }

    /**
     * @param int $scopeId
     * @return ConfigInterface
     */
    public function setScopeId($scopeId)
    {
        return $this->setData(self::SCOPE_ID, $scopeId);
    }

    /**
     * @return string|null
     */
    public function getPath()
    {
        return $this->getData(self::PATH);
    }

    /**
     * @param string $path
     * @return ConfigInterface
     */
    public function setPath($path)
    {
        return $this->setData(self::PATH, $path);
    }

    /**
     * @return string|null
     */
    public function getValue()
    {
        return $this->getData(self::VALUE);
    }

    /**
     * @param string $value
     * @return ConfigInterface
     */
    public function setValue($value)
    {
        return $this->setData(self::VALUE, $value);
    }
}
