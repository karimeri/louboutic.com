<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Helper\Alerting as AlertingHelper;
use Louboutin\Security\Model\ResourceModel\Role as RoleResource;
use Louboutin\Security\Model\ResourceModel\Rule as RuleResource;
use Louboutin\Security\Model\ResourceModel\Role\Collection as ReferenceRoleCollection;
use Louboutin\Security\Model\ResourceModel\Role\CollectionFactory as ReferenceRoleCollectionFactory;
use Louboutin\Security\Model\ResourceModel\Rule\Collection as ReferenceRuleCollection;
use Louboutin\Security\Model\ResourceModel\Rule\CollectionFactory as ReferenceRuleCollectionFactory;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\Role;
use Magento\Authorization\Model\Rules;
use Magento\Authorization\Model\ResourceModel\Role\Collection as RoleCollection;
use Magento\Authorization\Model\ResourceModel\Role\CollectionFactory as RoleCollectionFactory;
use Magento\Authorization\Model\ResourceModel\Rules\Collection as RulesCollection;
use Magento\Authorization\Model\ResourceModel\Rules\CollectionFactory as RulesCollectionFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class AuthorizationManager
 * @package Louboutin\Security\Model
 */
class AuthorizationManager
{
    /**
     * @var \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory
     */
    protected $roleCollectionFactory;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Role
     */
    protected $roleResource;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Role\CollectionFactory
     */
    protected $referenceRoleCollectionFactory;

    /**
     * @var \Magento\Authorization\Model\ResourceModel\Rules\CollectionFactory
     */
    protected $rulesCollectionFactory;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Rule
     */
    protected $ruleResource;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $referenceRuleCollectionFactory;

    /**
     * @var \Louboutin\Security\Helper\Alerting
     */
    protected $alertingHelper;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * AuthorizationManager constructor.
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $roleCollectionFactory
     * @param \Louboutin\Security\Model\ResourceModel\Role $roleResource
     * @param \Louboutin\Security\Model\ResourceModel\Role\CollectionFactory $referenceRoleCollectionFactory
     * @param \Magento\Authorization\Model\ResourceModel\Rules\CollectionFactory $rulesCollectionFactory
     * @param \Louboutin\Security\Model\ResourceModel\Rule $ruleResource
     * @param \Louboutin\Security\Model\ResourceModel\Rule\CollectionFactory $referenceRuleCollectionFactory
     * @param \Louboutin\Security\Helper\Alerting $alertingHelper
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
    public function __construct(
        RoleCollectionFactory $roleCollectionFactory,
        RoleResource $roleResource,
        ReferenceRoleCollectionFactory $referenceRoleCollectionFactory,
        RulesCollectionFactory $rulesCollectionFactory,
        RuleResource $ruleResource,
        ReferenceRuleCollectionFactory $referenceRuleCollectionFactory,
        AlertingHelper $alertingHelper,
        EnvironmentManager $environmentManager,
        StoreManagerInterface $storeManager,
        TimezoneInterface $timezone
    ) {
        $this->roleCollectionFactory = $roleCollectionFactory;
        $this->roleResource = $roleResource;
        $this->referenceRoleCollectionFactory = $referenceRoleCollectionFactory;
        $this->rulesCollectionFactory = $rulesCollectionFactory;
        $this->ruleResource = $ruleResource;
        $this->referenceRuleCollectionFactory = $referenceRuleCollectionFactory;
        $this->alertingHelper = $alertingHelper;
        $this->environmentManager = $environmentManager;
        $this->storeManager = $storeManager;
        $this->timezone = $timezone;
    }

    public function updateAuthorizationRoles()
    {
        $connection = $this->roleResource->getConnection();
        $connection->truncateTable(RoleResource::TABLE_NAME);

        $collection = $this->getRoleCollection();

        /** @var Role $role */
        foreach ($collection->getItems() as $role) {
            $connection->insert(
                $connection->getTableName(RoleResource::TABLE_NAME),
                $role->getData()
            );
        }
        $this->updateAuthorizationRules();
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkAuthorizationRoles()
    {
        $collection = $this->getRoleCollection();
        $roleIds = $collection->getAllIds();

        /** @var ReferenceRoleCollection $referenceCollection */
        $referenceCollection = $this->referenceRoleCollectionFactory->create();
        $referenceRoleIds = $referenceCollection->getAllIds();

        // New roles
        $newRoleIds = array_diff($roleIds, $referenceRoleIds);
        foreach ($newRoleIds as $newRoleId) {
            /** @var Role $newRole */
            $newRole = $collection->getItemById($newRoleId);
            $permissions = $this->getAuthorizationRules($newRoleId);

            $templateVars = [
                'environment' => $this->environmentManager->getEnvironment(),
                'newRole' => $newRole->getRoleName(),
                'permissions' => $permissions,
                'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
            ];
            // Send alert mail
            $this->alertingHelper
                ->alert($this->alertingHelper::XML_PATH_NEW_USER_ROLE_ALERTING_EMAIL_TEMPLATE, $templateVars);
        }

        // Deleted roles
        $deletedRoleIds = array_diff($referenceRoleIds, $roleIds);
        foreach ($deletedRoleIds as $deletedRoleId) {
            /** @var Role $deletedRole */
            $deletedRole = $referenceCollection->getItemById($deletedRoleId);

            $templateVars = [
                'environment' => $this->environmentManager->getEnvironment(),
                'deletedRole' => $deletedRole->getRoleName(),
                'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
            ];
            // Send alert mail
            $this->alertingHelper
                ->alert($this->alertingHelper::XML_PATH_DELETED_USER_ROLE_ALERTING_EMAIL_TEMPLATE, $templateVars);
        }

        // Scope change
        foreach ($referenceCollection->getItems() as $referenceRole) {
            $roleId = $referenceRole->getData('role_id');
            /** @var Role $role */
            $role = $collection->getItemById($roleId);
            if ($role && ($role->getData('gws_websites') != $referenceRole->getData('gws_websites'))) {
                $referenceWebsitesIds = explode(',', $referenceRole->getData('gws_websites'));
                $referenceWebsites = $this->getWebsitesNames($referenceWebsitesIds);
                $newWebsitesIds = explode(',', $role->getData('gws_websites'));
                $newWebsites = $this->getWebsitesNames($newWebsitesIds);

                $templateVars = [
                    'environment' => $this->environmentManager->getEnvironment(),
                    'role' => $referenceRole->getData('role_name'),
                    'referenceWebsites' => $referenceWebsites,
                    'newWebsites' => $newWebsites,
                    'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
                ];

                // Send alert mail
                $this->alertingHelper
                    ->alert(
                        $this->alertingHelper::XML_PATH_USER_ROLE_SCOPE_CHANGE_ALERTING_EMAIL_TEMPLATE,
                        $templateVars
                    );
            }
        }
    }

    /**
     * @param int $roleId
     */
    public function updateAuthorizationRules($roleId = null)
    {
        $connection = $this->ruleResource->getConnection();

        $collection = $this->getRulesCollection();

        if ($roleId) {
            $collection->addFieldToFilter('role_id', $roleId);
            $connection->delete(RuleResource::TABLE_NAME, ['role_id=?' => $roleId]);
        } else {
            $connection->truncateTable(RuleResource::TABLE_NAME);
        }

        /** @var Rules $rule */
        foreach ($collection->getItems() as $rule) {
            $connection->insert(
                $connection->getTableName(RuleResource::TABLE_NAME),
                $rule->getData()
            );
        }
    }

    public function checkAuthorizationRules()
    {
        /** @var ReferenceRoleCollection $referenceRoleCollection */
        $referenceRoleCollection = $this->referenceRoleCollectionFactory->create();
        $referenceRoleIds = $referenceRoleCollection->getAllIds();
        foreach ($referenceRoleIds as $referenceRoleId) {
            // Granted permissions
            $allowedRules = $this->getAuthorizationRules($referenceRoleId);
            $allowedReferenceRules = $this->getReferenceAuthorizationRules($referenceRoleId);
            $grantedPermissions = array_diff($allowedRules, $allowedReferenceRules);
            if ($grantedPermissions) {
                $templateVars = [
                    'environment' => $this->environmentManager->getEnvironment(),
                    'role' => $referenceRoleCollection->getItemById($referenceRoleId)->getData('role_name'),
                    'grantedPermissions' => $grantedPermissions,
                    'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
                ];
                // Send alert mail
                $this->alertingHelper
                    ->alert($this->alertingHelper::XML_PATH_GRANTED_USER_RULES_ALERTING_EMAIL_TEMPLATE, $templateVars);
            }

            // Revoked permissions
            $deniedRules = $this->getAuthorizationRules($referenceRoleId, 'deny');
            $deniedReferenceRules = $this->getReferenceAuthorizationRules($referenceRoleId, 'deny');
            $revokedPermissions = array_diff($deniedRules, $deniedReferenceRules);
            if ($revokedPermissions) {
                $templateVars = [
                    'environment' => $this->environmentManager->getEnvironment(),
                    'role' => $referenceRoleCollection->getItemById($referenceRoleId)->getData('role_name'),
                    'revokedPermissions' => $revokedPermissions,
                    'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
                ];
                // Send alert mail
                $this->alertingHelper
                    ->alert($this->alertingHelper::XML_PATH_REVOKED_USER_RULES_ALERTING_EMAIL_TEMPLATE, $templateVars);
            }
        }
    }

    /**
     * @return RoleCollection
     */
    private function getRoleCollection()
    {
        /** @var RoleCollection $collection */
        $collection = $this->roleCollectionFactory->create();
        $collection->addFieldToFilter('role_type', RoleGroup::ROLE_TYPE);

        return $collection;
    }

    /**
     * @return RulesCollection
     */
    private function getRulesCollection()
    {
        /** @var RulesCollection $collection */
        $collection = $this->rulesCollectionFactory->create();

        return $collection;
    }

    /**
     * @return ReferenceRuleCollection
     */
    private function getReferenceRulesCollection()
    {
        /** @var ReferenceRuleCollection $collection */
        $collection = $this->referenceRuleCollectionFactory->create();

        return $collection;
    }

    /**
     * @param int $roleId
     * @param string $permission
     * @return array
     */
    protected function getAuthorizationRules($roleId, $permission = 'allow')
    {
        $collection = $this->getRulesCollection();
        $items = $collection
            ->addFieldToFilter('role_id', $roleId)
            ->addFieldToFilter('permission', $permission)
            ->addFieldToSelect('resource_id')
            ->getItems();

        $data = [];
        foreach ($items as $item) {
            $data[] = $item->getData('resource_id');
        }

        return $data;
    }

    /**
     * @param int $roleId
     * @param string $permission
     * @return array
     */
    protected function getReferenceAuthorizationRules($roleId, $permission = 'allow')
    {
        $collection = $this->getReferenceRulesCollection();
        $items = $collection
            ->addFieldToFilter('role_id', $roleId)
            ->addFieldToFilter('permission', $permission)
            ->addFieldToSelect('resource_id')
            ->getItems();

        $data = [];
        foreach ($items as $item) {
            $data[] = $item->getData('resource_id');
        }

        return $data;
    }

    /**
     * @param array $websiteIds
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getWebsitesNames($websiteIds)
    {
        $data = [];
        foreach ($websiteIds as $websiteId) {
            $data[] = $this->storeManager->getWebsite($websiteId)->getName();
        }
        return $data;
    }
}
