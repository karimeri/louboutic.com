<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\ConfigRepositoryInterface;
use Louboutin\Security\Api\Data\ConfigInterface;
use Louboutin\Security\Api\Data\ConfigInterfaceFactory;
use Louboutin\Security\Api\Data\ConfigSearchResultsInterfaceFactory;
use Louboutin\Security\Model\ResourceModel\Config as ConfigResource;
use Louboutin\Security\Model\ResourceModel\Config\Collection as ConfigCollection;
use Louboutin\Security\Model\ResourceModel\Config\CollectionFactory as ConfigCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Class ConfigRepository
 * @package Louboutin\Security\Model
 */
class ConfigRepository implements ConfigRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Config
     */
    protected $resource;

    /**
     * @var \Louboutin\Security\Api\Data\ConfigInterfaceFactory
     */
    protected $configInterfaceFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var \Louboutin\Security\Api\Data\ConfigSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Config\CollectionFactory
     */
    protected $configCollectionFactory;

    /**
     * ConfigRepository constructor.
     * @param \Louboutin\Security\Model\ResourceModel\Config $resource
     * @param \Louboutin\Security\Api\Data\ConfigInterfaceFactory $configInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Louboutin\Security\Api\Data\ConfigSearchResultsInterfaceFactory $configSearchResultsInterfaceFactory
     * @param \Louboutin\Security\Model\ResourceModel\Config\CollectionFactory $configCollectionFactory
     */
    public function __construct(
        ConfigResource $resource,
        ConfigInterfaceFactory $configInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        ConfigSearchResultsInterfaceFactory $configSearchResultsInterfaceFactory,
        ConfigCollectionFactory $configCollectionFactory
    ) {
        $this->resource = $resource;
        $this->configInterfaceFactory = $configInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->searchResultsFactory = $configSearchResultsInterfaceFactory;
        $this->configCollectionFactory = $configCollectionFactory;
    }

    /**
     * @param ConfigInterface $config
     * @return ConfigInterface|\Magento\Framework\Model\AbstractModel
     * @throws CouldNotSaveException
     */
    public function save(ConfigInterface $config)
    {
        /** @var ConfigInterface|\Magento\Framework\Model\AbstractModel $config */
        try {
            $this->resource->save($config);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the config: %1',
                $exception->getMessage()
            ));
        }
        return $config;
    }

    /**
     * @param int $configId
     * @return ConfigInterface|mixed
     * @throws NoSuchEntityException
     */
    public function getById($configId)
    {
        if (!isset($this->instances[$configId])) {
            /** @var \Louboutin\Security\Api\Data\ConfigInterface|\Magento\Framework\Model\AbstractModel $config */
            $config = $this->configInterfaceFactory->create();
            $this->resource->load($config, $configId);
            if (!$config->getId()) {
                throw new NoSuchEntityException(__('Requested config doesn\'t exist'));
            }
            $this->instances[$configId] = $config;
        }
        return $this->instances[$configId];
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Louboutin\Security\Api\Data\ConfigSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Louboutin\Security\Api\Data\ConfigSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Louboutin\Security\Model\ResourceModel\Config\Collection $collection */
        $collection = $this->configCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'config_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Louboutin\Security\Api\Data\ConfigInterface[] $configs */
        $configs = [];
        /** @var \Louboutin\Security\Model\Config $config */
        foreach ($collection as $config) {
            /** @var \Louboutin\Security\Api\Data\ConfigInterface $configDataObject */
            $configDataObject = $this->configInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($configDataObject, $config->getData(), ConfigInterface::class);
            $configs[] = $configDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($configs);
    }

    /**
     * @param ConfigInterface $config
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ConfigInterface $config)
    {
        /** @var \Louboutin\Security\Api\Data\ConfigInterface|\Magento\Framework\Model\AbstractModel $config */
        $id = $config->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($config);
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove config %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * @param int $configId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($configId)
    {
        $config = $this->getById($configId);
        return $this->delete($config);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param ConfigCollection $collection
     * @return $this
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, ConfigCollection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
