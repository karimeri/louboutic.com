<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\Data\RuleInterface;
use Louboutin\Security\Api\Data\RuleInterfaceFactory;
use Louboutin\Security\Api\Data\RuleSearchResultsInterfaceFactory;
use Louboutin\Security\Api\RuleRepositoryInterface;
use Louboutin\Security\Model\ResourceModel\Rule as RuleResource;
use Louboutin\Security\Model\ResourceModel\Rule\Collection as RuleCollection;
use Louboutin\Security\Model\ResourceModel\Rule\CollectionFactory as RuleCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Class RuleRepository
 * @package Louboutin\Security\Model
 */
class RuleRepository implements RuleRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Rule
     */
    protected $resource;

    /**
     * @var \Louboutin\Security\Api\Data\RuleInterfaceFactory
     */
    protected $ruleInterfaceFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var \Louboutin\Security\Api\Data\RuleSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $ruleCollectionFactory;

    /**
     * RuleRepository constructor.
     * @param \Louboutin\Security\Model\ResourceModel\Rule $resource
     * @param \Louboutin\Security\Api\Data\RuleInterfaceFactory $ruleInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Louboutin\Security\Api\Data\RuleSearchResultsInterfaceFactory $ruleSearchResultsInterfaceFactory
     * @param \Louboutin\Security\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory
     */
    public function __construct(
        RuleResource $resource,
        RuleInterfaceFactory $ruleInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        RuleSearchResultsInterfaceFactory $ruleSearchResultsInterfaceFactory,
        RuleCollectionFactory $ruleCollectionFactory
    ) {
        $this->resource = $resource;
        $this->ruleInterfaceFactory = $ruleInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->searchResultsFactory = $ruleSearchResultsInterfaceFactory;
        $this->ruleCollectionFactory = $ruleCollectionFactory;
    }

    /**
     * @param RuleInterface $rule
     * @return RuleInterface
     * @throws CouldNotSaveException
     */
    public function save(RuleInterface $rule)
    {
        /** @var RuleInterface|\Magento\Framework\Model\AbstractModel $rule */
        try {
            $this->resource->save($rule);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the rule: %1',
                $exception->getMessage()
            ));
        }
        return $rule;
    }

    /**
     * @param int $ruleId
     * @return RuleInterface|mixed
     * @throws NoSuchEntityException
     */
    public function getById($ruleId)
    {
        if (!isset($this->instances[$ruleId])) {
            /** @var \Louboutin\Security\Api\Data\RuleInterface|\Magento\Framework\Model\AbstractModel $rule */
            $rule = $this->ruleInterfaceFactory->create();
            $this->resource->load($rule, $ruleId);
            if (!$rule->getId()) {
                throw new NoSuchEntityException(__('Requested rule doesn\'t exist'));
            }
            $this->instances[$ruleId] = $rule;
        }
        return $this->instances[$ruleId];
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Louboutin\Security\Api\Data\RuleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Louboutin\Security\Api\Data\RuleSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Louboutin\Security\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->ruleCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'rule_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Louboutin\Security\Api\Data\RuleInterface[] $rules */
        $rules = [];
        /** @var \Louboutin\Security\Model\Rule $rule */
        foreach ($collection as $rule) {
            /** @var \Louboutin\Security\Api\Data\RuleInterface $ruleDataObject */
            $ruleDataObject = $this->ruleInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($ruleDataObject, $rule->getData(), RuleInterface::class);
            $rules[] = $ruleDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($rules);
    }

    /**
     * @param RuleInterface $rule
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(RuleInterface $rule)
    {
        /** @var \Louboutin\Security\Api\Data\RuleInterface|\Magento\Framework\Model\AbstractModel $rule */
        $id = $rule->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($rule);
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove rule %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * @param int $ruleId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($ruleId)
    {
        $rule = $this->getById($ruleId);
        return $this->delete($rule);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param RuleCollection $collection
     * @return $this
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, RuleCollection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
