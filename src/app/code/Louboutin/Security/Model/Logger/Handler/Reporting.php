<?php

namespace Louboutin\Security\Model\Logger\Handler;

use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

/**
 * Class Reporting
 * @package Louboutin\Security\Model\Logger\Handler
 */
class Reporting extends Base
{
    private static $reportPath = '/var/loubisec/';

    protected $loggerType = Logger::ALERT;

    /**
     * @var string
     */
    protected $reportId;

    /**
     * Reporting constructor.
     * @param \Magento\Framework\Filesystem\DriverInterface $filesystem
     * @param null $filePath
     * @param null $fileName
     */
    public function __construct(
        DriverInterface $filesystem,
        $filePath = null,
        $fileName = null
    ) {
        $fileName = self::$reportPath . $this->setReportId();
        parent::__construct($filesystem, $filePath, $fileName);
    }

    /**
     * @return string
     */
    public function setReportId()
    {
        return $this->reportId = strval(abs(intval(microtime(true) * rand(100, 1000))));
    }

    /**
     * @return string
     */
    public function getReportId()
    {
        return $this->reportId;
    }
}
