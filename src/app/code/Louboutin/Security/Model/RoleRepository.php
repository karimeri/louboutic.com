<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\Data\RoleInterface;
use Louboutin\Security\Api\Data\RoleInterfaceFactory;
use Louboutin\Security\Api\Data\RoleSearchResultsInterfaceFactory;
use Louboutin\Security\Api\RoleRepositoryInterface;
use Louboutin\Security\Model\ResourceModel\Role as RoleResource;
use Louboutin\Security\Model\ResourceModel\Role\Collection as RoleCollection;
use Louboutin\Security\Model\ResourceModel\Role\CollectionFactory as RoleCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Class RoleRepository
 * @package Louboutin\Security\Model
 */
class RoleRepository implements RoleRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Role
     */
    protected $resource;

    /**
     * @var \Louboutin\Security\Api\Data\RoleInterfaceFactory
     */
    protected $roleInterfaceFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var \Louboutin\Security\Api\Data\RoleSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Louboutin\Security\Model\ResourceModel\Role\CollectionFactory
     */
    protected $roleCollectionFactory;

    /**
     * RoleRepository constructor.
     * @param \Louboutin\Security\Model\ResourceModel\Role $resource
     * @param \Louboutin\Security\Api\Data\RoleInterfaceFactory $roleInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Louboutin\Security\Api\Data\RoleSearchResultsInterfaceFactory $roleSearchResultsInterfaceFactory
     * @param \Louboutin\Security\Model\ResourceModel\Role\CollectionFactory $roleCollectionFactory
     */
    public function __construct(
        RoleResource $resource,
        RoleInterfaceFactory $roleInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        RoleSearchResultsInterfaceFactory $roleSearchResultsInterfaceFactory,
        RoleCollectionFactory $roleCollectionFactory
    ) {
        $this->resource = $resource;
        $this->roleInterfaceFactory = $roleInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->searchResultsFactory = $roleSearchResultsInterfaceFactory;
        $this->roleCollectionFactory = $roleCollectionFactory;
    }

    /**
     * @param RoleInterface $role
     * @return RoleInterface
     * @throws CouldNotSaveException
     */
    public function save(RoleInterface $role)
    {
        /** @var RoleInterface|\Magento\Framework\Model\AbstractModel $role */
        try {
            $this->resource->save($role);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the role: %1',
                $exception->getMessage()
            ));
        }
        return $role;
    }

    /**
     * @param int $roleId
     * @return RoleInterface|mixed
     * @throws NoSuchEntityException
     */
    public function getById($roleId)
    {
        if (!isset($this->instances[$roleId])) {
            /** @var \Louboutin\Security\Api\Data\RoleInterface|\Magento\Framework\Model\AbstractModel $role */
            $role = $this->roleInterfaceFactory->create();
            $this->resource->load($role, $roleId);
            if (!$role->getId()) {
                throw new NoSuchEntityException(__('Requested role doesn\'t exist'));
            }
            $this->instances[$roleId] = $role;
        }
        return $this->instances[$roleId];
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Louboutin\Security\Api\Data\RoleSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Louboutin\Security\Api\Data\RoleSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Louboutin\Security\Model\ResourceModel\Role\Collection $collection */
        $collection = $this->roleCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'role_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Louboutin\Security\Api\Data\RoleInterface[] $roles */
        $roles = [];
        /** @var \Louboutin\Security\Model\Role $role */
        foreach ($collection as $role) {
            /** @var \Louboutin\Security\Api\Data\RoleInterface $roleDataObject */
            $roleDataObject = $this->roleInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($roleDataObject, $role->getData(), RoleInterface::class);
            $roles[] = $roleDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($roles);
    }

    /**
     * @param RoleInterface $role
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(RoleInterface $role)
    {
        /** @var \Louboutin\Security\Api\Data\RoleInterface|\Magento\Framework\Model\AbstractModel $role */
        $id = $role->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($role);
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove role %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * @param int $roleId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($roleId)
    {
        $role = $this->getById($roleId);
        return $this->delete($role);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param RoleCollection $collection
     * @return $this
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, RoleCollection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
