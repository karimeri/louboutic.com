<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\Data\RuleInterface;
use Louboutin\Security\Model\ResourceModel\Rule as RuleResourceModel;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Rule
 * @package Louboutin\Security\Model
 */
class Rule extends AbstractModel implements RuleInterface, ExtensibleDataInterface
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(RuleResourceModel::class);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::RULE_ID);
    }

    /**
     * @param int $id
     * @return RuleInterface
     */
    public function setId($id)
    {
        return $this->setData(self::RULE_ID, $id);
    }

    /**
     * @return int|null
     */
    public function getRoleId()
    {
        return $this->getData(self::ROLE_ID);
    }

    /**
     * @param int $roleId
     * @return RuleInterface
     */
    public function setRoleId($roleId)
    {
        return $this->setData(self::ROLE_ID, $roleId);
    }

    /**
     * @return string|null
     */
    public function getResourceId()
    {
        return $this->getData(self::RESOURCE_ID);
    }

    /**
     * @param string $resourceId
     * @return RuleInterface
     */
    public function setResourceId($resourceId)
    {
        return $this->setData(self::RESOURCE_ID, $resourceId);
    }

    /**
     * @return string|null
     */
    public function getPrivileges()
    {
        return $this->getData(self::PRIVILEGES);
    }

    /**
     * @param string $privileges
     * @return RuleInterface
     */
    public function setPrivileges($privileges)
    {
        return $this->setData(self::PRIVILEGES, $privileges);
    }

    /**
     * @return string|null
     */
    public function getPermission()
    {
        return $this->getData(self::PERMISSION);
    }

    /**
     * @param string $permission
     * @return RuleInterface
     */
    public function setPermission($permission)
    {
        return $this->setData(self::PERMISSION, $permission);
    }
}
