<?php

namespace Louboutin\Security\Model\ResourceModel\Role;

use Louboutin\Security\Model\Role;
use Louboutin\Security\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Louboutin\Security\Model\ResourceModel\Role
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(Role::class, RoleResourceModel::class);
    }
}
