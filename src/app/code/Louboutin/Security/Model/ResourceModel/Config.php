<?php

namespace Louboutin\Security\Model\ResourceModel;

use Louboutin\Security\Api\Data\ConfigInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Config
 * @package Louboutin\Security\Model\ResourceModel
 */
class Config extends AbstractDb
{
    const TABLE_NAME = 'louboutin_config_data';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, ConfigInterface::CONFIG_ID);
    }
}
