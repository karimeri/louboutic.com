<?php

namespace Louboutin\Security\Model\ResourceModel;

use Louboutin\Security\Api\Data\RuleInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Rule
 * @package Louboutin\Security\Model\ResourceModel
 */
class Rule extends AbstractDb
{
    const TABLE_NAME = 'louboutin_authorization_rule';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, RuleInterface::RULE_ID);
    }
}
