<?php

namespace Louboutin\Security\Model\ResourceModel;

use Louboutin\Security\Api\Data\RoleInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Role
 * @package Louboutin\Security\Model\ResourceModel
 */
class Role extends AbstractDb
{
    const TABLE_NAME = 'louboutin_authorization_role';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, RoleInterface::ROLE_ID);
    }
}
