<?php

namespace Louboutin\Security\Model\ResourceModel\Config;

use Louboutin\Security\Model\Config;
use Louboutin\Security\Model\ResourceModel\Config as ConfigResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Louboutin\Security\Model\ResourceModel\Config
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(Config::class, ConfigResourceModel::class);
    }
}
