<?php

namespace Louboutin\Security\Model\ResourceModel\Rule;

use Louboutin\Security\Model\Rule;
use Louboutin\Security\Model\ResourceModel\Rule as RuleResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Louboutin\Security\Model\ResourceModel\Rule
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(Rule::class, RuleResourceModel::class);
    }
}
