<?php

namespace Louboutin\Security\Model;

use Louboutin\Security\Api\Data\RoleInterface;
use Louboutin\Security\Model\ResourceModel\Role as RoleResourceModel;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Role
 * @package Louboutin\Security\Model
 */
class Role extends AbstractModel implements RoleInterface, ExtensibleDataInterface
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(RoleResourceModel::class);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ROLE_ID);
    }

    /**
     * @param int $id
     * @return RoleInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ROLE_ID, $id);
    }

    /**
     * @return int|null
     */
    public function getParentId()
    {
        return $this->getData(self::PARENT_ID);
    }

    /**
     * @param int $parentId
     * @return RoleInterface
     */
    public function setParentId($parentId)
    {
        return $this->setData(self::PARENT_ID, $parentId);
    }

    /**
     * @return int|null
     */
    public function getTreeLevel()
    {
        return $this->getData(self::TREE_LEVEL);
    }

    /**
     * @param int $treeLevel
     * @return RoleInterface
     */
    public function setTreeLevel($treeLevel)
    {
        return $this->setData(self::TREE_LEVEL, $treeLevel);
    }

    /**
     * @return int|null
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * @param int $sortOrder
     * @return RoleInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * @return string|null
     */
    public function getRoleType()
    {
        return $this->getData(self::ROLE_TYPE);
    }

    /**
     * @param string $roleType
     * @return RoleInterface
     */
    public function setRoleType($roleType)
    {
        return $this->setData(self::ROLE_TYPE, $roleType);
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->getData(self::USER_ID);
    }

    /**
     * @param int $userId
     * @return RoleInterface
     */
    public function setUserId($userId)
    {
        return $this->setData(self::USER_ID, $userId);
    }

    /**
     * @return string|null
     */
    public function getUserType()
    {
        return $this->getData(self::USER_TYPE);
    }

    /**
     * @param string $userType
     * @return RoleInterface
     */
    public function setUserType($userType)
    {
        return $this->setData(self::USER_TYPE, $userType);
    }

    /**
     * @return string|null
     */
    public function getRoleName()
    {
        return $this->getData(self::ROLE_NAME);
    }

    /**
     * @param string $roleName
     * @return RoleInterface
     */
    public function setRoleName($roleName)
    {
        return $this->setData(self::ROLE_NAME, $roleName);
    }

    /**
     * @return int|null
     */
    public function getGwsIsAll()
    {
        return $this->getData(self::GWS_IS_ALL);
    }

    /**
     * @param int $gwsIsAll
     * @return RoleInterface
     */
    public function setGwsIsAll($gwsIsAll)
    {
        return $this->setData(self::GWS_IS_ALL, $gwsIsAll);
    }

    /**
     * @return string|null
     */
    public function getGwsWebsites()
    {
        return $this->getData(self::GWS_WEBSITES);
    }

    /**
     * @param string $gwsWebsites
     * @return RoleInterface
     */
    public function setGwsWebsites($gwsWebsites)
    {
        return $this->setData(self::GWS_WEBSITES, $gwsWebsites);
    }

    /**
     * @return string|null
     */
    public function getGwsStoreGroups()
    {
        return $this->getData(self::GWS_STORE_GROUPS);
    }

    /**
     * @param string $gwsStoreGroups
     * @return RoleInterface
     */
    public function setGwsStoreGroups($gwsStoreGroups)
    {
        return $this->setData(self::GWS_STORE_GROUPS, $gwsStoreGroups);
    }
}
