# Louboutin_Security

This module is used to send an alert if a critical value have been changed or if roles/rules have been changed.

### Configuration

In **Stores > Configuration > Christian Louboutin > Security > Alerting**, you can set email recipients and email templates.

### Commands
```sh
$ bin/magento louboutin:security:check-config-values
```
* Compare referential config values and magento config values.
* If values do not match, an alert email will be sent and a report file will be created.
```sh
$ bin/magento louboutin:security:update-config-values
```
* Update referential config values with magento values.
```sh
$ bin/magento louboutin:security:check-authorization-roles
```
* Compare referential roles with magento roles.
* If a role have been created/deleted, an alert email will be sent.
* If a role scope have been changed, an alert email will be sent.
```sh
$ bin/magento louboutin:security:update-authorization-roles
```
* Update referential roles *and referential rules (permissions)* with magento roles/rules.
```sh
$ bin/magento louboutin:security:check-authorization-rules
```
* Compare referential rules (permissions) for each role with magento permissions.
* If permissions have been granted/revoked, an alert email will be sent.
```sh
$ bin/magento louboutin:security:update-authorization-rules (--role_id=10)
```
* Update referential rules (permissions) with magento rules.
* There is an option (--role_id) to update rules for a specific role.