<?php

namespace Louboutin\Varnish\Controller\Adminhtml\Flush;


use Louboutin\Varnish\Model\VarnishScheduleRepository as VarnishScheduleRepo;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Louboutin\Varnish\Model\VarnishScheduleRepository;
use Louboutin\Varnish\Model\PurgeCache;

class FlushCategoryAll extends Action
{

    /**
     * @var VarnishScheduleRepository
     */
    private $varnishRepository;

    /**
     * @var PurgeCache
     */
    private $purgeCache;

    /**
     * @var VarnishScheduleRepo
     */
    private $varnishScheduleRepository;

    public function __construct(Context $context, VarnishScheduleRepository $varnishRepository, PurgeCache $purgeCache, VarnishScheduleRepo $varnishScheduleRepository)
    {
        parent::__construct($context);
        $this->varnishRepository = $varnishRepository;
        $this->purgeCache = $purgeCache;
        $this->varnishScheduleRepository = $varnishScheduleRepository;

    }

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Backend::flush_cache_categories';



    public function execute()
    {

        $bareTags = array();
        $tagString = '';


        $tags = $this->varnishRepository->loadVarnishScheduleToClean('cat');
        $tagsToUpdate = $tags;

        if ( ! $tags->count()  ) {
            $this->messageManager->addSuccessMessage(__("All categories are already flushed"));
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('adminhtml/cache/index');
        }

        foreach ($tags as $tag) {
            $bareTags[] =  $tag->getTag();
            $tagString .= $tag->getTag() . '|';
        }
        $tags = [];
        $pattern = "((^|,)%s(,|$))";
        foreach ($bareTags as $tag) {
            $tags[] = sprintf($pattern, $tag);
        }
        if (!empty($tags)) {
            if ($this->purgeCache->sendPurgeRequest(implode('|', array_unique($tags)))) {
                foreach ($tagsToUpdate as $tag) {
                    if ($tag->getId() > 1) {
                        $tag->setStatus('flushed');
                        $tag->save();
                    }
                }
                $this->messageManager->addSuccessMessage(__("You flushed the cache Categories => " . $tagString));
            } else {
                $this->messageManager->addErrorMessage(__("A technical problem when trying to flush CATEGORIES. Try later please."));
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('adminhtml/cache/index');
    }

}
