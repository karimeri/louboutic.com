<?php
namespace Louboutin\Varnish\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Debug
 * @package Louboutin\Varnish\Helper
 */
class Debug extends AbstractHelper
{
    const XML_PATH_DEBUG_ENABLED
        = 'louboutin_varnish/varnish/debug';


    /**
     * Debug constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
    }

    /**
     * Check if Varnish Debug is Enabled 
     * @return bool
     */
    public function isDebugEnabled()
    {
        $storeId = $this->storeManager->getStore()->getId();
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_DEBUG_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
    }

}
