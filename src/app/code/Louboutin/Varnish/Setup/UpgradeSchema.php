<?php

namespace Louboutin\Varnish\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package WeltPixel\Backend\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /**
             * Create table 'weltpixel_license'
             */
            $table = $installer->getConnection()->newTable(
                $installer->getTable('varnish_schedule_crontab')
            )->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )->addColumn(
                'tag',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'tag Name'
            )->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false
                ]
            )->addColumn(
                'flushed_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => true
                ]
            )->addColumn(
                'flush_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ]
            )->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ]
            )->addColumn(
                'flushed_by',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true
                ]
            )->addColumn(
                'nb_request',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false
                ]
            )->setComment(
                'Varnish Schedule'
            );

            $installer->getConnection()->createTable($table);

        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $installer->getConnection()->addIndex(
                $setup->getTable('varnish_schedule_crontab'),
                $setup->getIdxName('varnish_schedule_crontab', ['tag']),
                ['tag']
            );
        }

        $installer->endSetup();
    }
}
