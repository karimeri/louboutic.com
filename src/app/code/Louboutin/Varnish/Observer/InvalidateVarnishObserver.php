<?php

namespace Louboutin\Varnish\Observer;

use Louboutin\Varnish\Model\VarnishSchedule as VarnishScheduleModel;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule as VarnishScheduleResource;
use Magento\Framework\App\Config\ScopeConfigInterface;
use  Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\App\Cache\Tag\Resolver;
use Louboutin\Varnish\Model\VarnishScheduleRepository as VarnishScheduleRepo;
use Louboutin\Varnish\Model\PurgeCache;


class InvalidateVarnishObserver extends \Magento\CacheInvalidate\Observer\InvalidateVarnishObserver
{

    /**
     * Application config object
     *
     * @var ScopeConfigInterface
     */
    protected $config;

    /**
     * @var PurgeCache
     */
    protected $purgeCache;

    /**
     * Invalidation tags resolver
     *
     * @var \Magento\Framework\App\Cache\Tag\Resolver
     */
    private $tagResolver;

    /**
     * @var VarnishScheduleResource
     */
    private $varnishScheduleResource;

    /**
     * @var DateTimeFactory
     */
    private $dateFactory;

    /**
     * @var VarnishScheduleRepo
     */
    private $varnishScheduleRepository;

    /**
     * InvalidateVarnishObserver constructor.
     * @param \Magento\PageCache\Model\Config $config
     * @param PurgeCache $purgeCache
     * @param Resolver $tagResolver
     * @param VarnishScheduleResource $varnishScheduleResource
     * @param DateTimeFactory $dateFactory
     * @param VarnishScheduleRepo $varnishScheduleRepository
     */
    public function __construct(
        \Magento\PageCache\Model\Config $config,
        PurgeCache $purgeCache,
        Resolver $tagResolver,
        VarnishScheduleResource $varnishScheduleResource,
        DateTimeFactory $dateFactory,
        VarnishScheduleRepo $varnishScheduleRepository
    ) {
        parent::__construct($config,$purgeCache,$tagResolver);
        $this->config = $config;
        $this->tagResolver = $tagResolver;
        $this->varnishScheduleResource = $varnishScheduleResource;
        $this->dateFactory = $dateFactory;
        $this->varnishScheduleRepository = $varnishScheduleRepository;
        $this->purgeCache = $purgeCache;

    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $object = $observer->getEvent()->getObject();

        if (!is_object($object)) {
            return;
        }

        $dateTime = $this->dateFactory->create();
        if ((int)$this->config->getType() === \Magento\PageCache\Model\Config::VARNISH && $this->config->isEnabled()) {
            $bareTags = $this->tagResolver->getTags($object);
            $productTags = [];
            foreach ($bareTags as $tag) {

                if (!empty($tag) ) {
                    if (preg_match("#^cat_p#", $tag)) {
                        // product tags to flushed now
                        $productTags[] = $tag;
                    } else {
                        //check if tag existe
                        $exist = false;
                        $tagSchedule = $this->varnishScheduleRepository->getScheduleByTag($tag);
                        foreach ($tagSchedule as $tagBdd) {
                            $tagBdd->setStatus('pending');
                            $tagBdd->setScheduledAt($dateTime->gmtDate());
                            $tagBdd->setNbRequest($tagBdd->getNbRequest() + 1);
                            $tagBdd->save();
                            $exist = true;
                        }

                        if (!$exist) {
                            $data = [
                                VarnishScheduleModel::TAG => $tag,
                                VarnishScheduleModel::FLUSH_TYPE => 'auto',
                                VarnishScheduleModel::STATUS => 'pending',
                                VarnishScheduleModel::CREATED_AT => $dateTime->gmtDate(),
                                VarnishScheduleModel::SCHEDULED_AT => $dateTime->gmtDate(),
                                VarnishScheduleModel::NB_REQUEST => 1
                            ];

                            $this->varnishScheduleResource->insertOnDuplicate($data);
                        }
                    }
                }
            }

            //flush product tags
            $tags = [];
            $pattern = '((^|,)%s(,|$))';
            foreach ($productTags as $tag) {
                $tags[] = sprintf($pattern, $tag);
            }
            if (!empty($tags)) {
                $this->purgeCache->sendPurgeRequest(array_unique($tags));
            }
        }
    }

    /**
     * @deprecated 100.1.2
     * @return \Magento\Framework\App\Cache\Tag\Resolver
     */
    private function getTagResolver()
    {
        if ($this->tagResolver === null) {
            $this->tagResolver = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Framework\App\Cache\Tag\Resolver::class);
        }
        return $this->tagResolver;
    }


}
