<?php
namespace Louboutin\Varnish\Model\ResourceModel\VarnishSchedule;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Louboutin\Varnish\Model\VarnishSchedule;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule as VarnishScheduleResource;

/**
 * Class Collection
 * @package Project\Security\Model\ResourceModel\LoginFailure
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(VarnishSchedule::class, VarnishScheduleResource::class);
    }
}
