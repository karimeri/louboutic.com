<?php
namespace Louboutin\Varnish\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Louboutin\Varnish\Model\VarnishSchedule as VarnishScheduleModel;

/**
 * Class VarnishSchedule
 * @package Louboutin\Varnish\Model\ResourceModel
 */
class VarnishSchedule extends AbstractDb
{
    const TABLE_NAME = 'varnish_schedule_crontab';
    const SCHEDULE_STATUS = 'pending';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected  function _construct()
    {
        $this->_init($this::TABLE_NAME, 'id');
    }


    public function insertOnDuplicate(array $data, array $fields = []) {
        $this->getConnection()->insertOnDuplicate($this->getMainTable(), $data, $fields);
    }


    /*public function getByTag($tag)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable($this->getMainTable()))
            ->where(VarnishScheduleModel::TAG . ' = ?', $tag)
            ->limit(1);

        return $this->getConnection()->fetchAll($select);
    }*/


   /* public function getLikeTag($type)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable($this->getMainTable()))
            ->where(VarnishScheduleModel::TAG . ' LIKE ?',  $type . '%');
           // ->where(VarnishScheduleModel::STATUS . ' = ?', $this::SCHEDULE_STATUS);

        return $this->getConnection()->fetchAll($select);
    }*/


}
