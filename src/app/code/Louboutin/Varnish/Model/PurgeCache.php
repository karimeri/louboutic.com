<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Louboutin\Varnish\Model;

use Louboutin\Varnish\Model\VarnishSchedule as VarnishScheduleModel;
use Magento\CacheInvalidate\Model\SocketFactory;
use Magento\Framework\Cache\InvalidateLogger;
use Magento\PageCache\Model\Cache\Server;
use Laminas\Http\Client\Adapter\Socket;
use Laminas\Uri\Uri;
use Magento\Framework\Filesystem\DirectoryList;
use Synolia\Logger\Model\LoggerFactory;
use Louboutin\Varnish\Helper\Debug as DebugHelper;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule as VarnishScheduleResource;
use  Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Louboutin\Varnish\Model\VarnishScheduleRepository;
/**
 * Invalidate external HTTP cache(s) based on tag pattern
 */
class PurgeCache extends \Magento\CacheInvalidate\Model\PurgeCache
{
    const HEADER_X_MAGENTO_TAGS_PATTERN = 'X-Magento-Tags-Pattern';
    const LOG_DIRECTORY = 'varnish';
    const LOG_FILE = 'louboutin_varnish_debug.log';

    /**
     * @var Server
     */
    protected $cacheServer;

    /**
     * @var SocketFactory
     */
    protected $socketAdapterFactory;

    /**
     * @var InvalidateLogger
     */
    private $logger;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $loggerFile;

    /**
     * @var VarnishScheduleRepository
     */
    protected $varnishRepository;

    /**
     * Batch size of the purge request.
     *
     * Based on default Varnish 4 http_req_hdr_len size minus a 512 bytes margin for method,
     * header name, line feeds etc.
     *
     * @see https://varnish-cache.org/docs/4.1/reference/varnishd.html
     *
     * @var int
     */
    private $maxHeaderSize;

    /**
     * @var \Louboutin\Varnish\Helper\Debug
     */
    protected $varnishDebugHelper;

    /**
     * @var VarnishScheduleResource
     */
    protected $varnishScheduleResource;

    /**
     * @var DateTimeFactory
     */
    private $dateFactory;


    /**
     * PurgeCache constructor.
     * @param \Magento\PageCache\Model\Cache\Server $cacheServer
     * @param \Magento\CacheInvalidate\Model\SocketFactory $socketAdapterFactory
     * @param InvalidateLogger $logger
     * @param DirectoryList $directoryList
     * @param LoggerFactory $loggerFactory
     * @param DebugHelper $varsnishDebugHelper
     * @param VarnishScheduleResource $varnishScheduleResource
     * @param DateTimeFactory $dateFactory
     * @param \Louboutin\Varnish\Model\VarnishScheduleRepository $varnishRepository
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        \Magento\PageCache\Model\Cache\Server $cacheServer,
        \Magento\CacheInvalidate\Model\SocketFactory $socketAdapterFactory,
        InvalidateLogger $logger,
        DirectoryList $directoryList,
        LoggerFactory $loggerFactory,
        int $maxHeaderSize = 7680,
        DebugHelper $varsnishDebugHelper,
        VarnishScheduleResource $varnishScheduleResource,
        DateTimeFactory $dateFactory,
        VarnishScheduleRepository $varnishRepository
    ) {
        parent::__construct($cacheServer, $socketAdapterFactory, $logger, $maxHeaderSize);
        $this->varnishDebugHelper = $varsnishDebugHelper;
        $logPath = $directoryList->getPath('log') . DIRECTORY_SEPARATOR . $this::LOG_DIRECTORY . DIRECTORY_SEPARATOR;
        $this->maxHeaderSize = $maxHeaderSize;
        $this->loggerFile = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath . $this::LOG_FILE]
        );
        $this->varnishScheduleResource = $varnishScheduleResource;
        $this->dateFactory = $dateFactory;
        $this->varnishRepository = $varnishRepository;
    }

    private function customCleanCache($uri,$varnishIp,$formattedTagsChunk)
    {
        $strURL = "https://".$varnishIp;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return data inplace of echoing on screen
        curl_setopt($ch, CURLOPT_URL, $strURL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PURGE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Magento-Tags-Pattern: '.$formattedTagsChunk
        ));

        $rsData = curl_exec($ch);
        curl_close($ch);

        if ($this->varnishDebugHelper->isDebugEnabled()) {
            $debug = array(
                "formattedTagsChunk" => $formattedTagsChunk,
                "strURL" => $strURL,
                "ipVarnish" => $varnishIp,
                "date" => date('Y-m-d-h-i-s')
            );

            //Log Varnish in varnish/louboutin_varnish_debug.log
            $this->loggerFile->addInfo("DEBUG TEMP VARNISH CACHE CLEAN ====> " . (string)$rsData, $debug);
        }
    }


    /**
     * Send curl purge request to invalidate cache by tags pattern
     *
     * @param array|string $tags
     * @return bool Return true if successful; otherwise return false
     */
    public function sendPurgeRequest($tags)
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }

        if (in_array( '.*', $tags)) {
            $dateTime = $this->dateFactory->create();
            $data = [
                VarnishScheduleModel::TAG => '.*',
                VarnishScheduleModel::FLUSH_TYPE => 'auto',
                VarnishScheduleModel::STATUS => 'flushed',
                VarnishScheduleModel::CREATED_AT => $dateTime->gmtDate(),
                VarnishScheduleModel::SCHEDULED_AT => $dateTime->gmtDate(),
                VarnishScheduleModel::NB_REQUEST => 1
            ];

            $this->varnishScheduleResource->insertOnDuplicate($data);

            //todo clean table

            $this->varnishRepository->deleteMultiple();
        }

        $successful = true;
        $socketAdapter = $this->socketAdapterFactory->create();
        $servers = $this->cacheServer->getUris();
        $socketAdapter->setOptions(['timeout' => 10]);
        $formattedTagsChunks = $this->chunkTags($tags);
        foreach ($formattedTagsChunks as $formattedTagsChunk) {
            if (!$this->sendPurgeRequestToServers($socketAdapter, $servers, $formattedTagsChunk)) {
                $successful = false;
            }
        }

        return $successful;
    }

    /**
     * Send curl purge request to servers to invalidate cache by tags pattern
     *
     * @param Socket $socketAdapter
     * @param Uri[] $servers
     * @param string $formattedTagsChunk
     * @return bool Return true if successful; otherwise return false
     */
    private function sendPurgeRequestToServers(Socket $socketAdapter, array $servers, string $formattedTagsChunk): bool
    {
        $headers = [self::HEADER_X_MAGENTO_TAGS_PATTERN => $formattedTagsChunk];
        $unresponsiveServerError = [];
        foreach ($servers as $server) {
            $headers['Host'] = $server->getHost();
            try {
                $socketAdapter->connect($server->getHost(), $server->getPort());
                $socketAdapter->write(
                    'PURGE',
                    $server,
                    '1.1',
                    $headers
                );
                $socketAdapter->read();
                $socketAdapter->close();
            } catch (\Exception $e) {
                $unresponsiveServerError[] = "Cache host: " . $server->getHost() . ":" . $server->getPort() .
                    "resulted in error message: " . $e->getMessage();
            }
            $this->customCleanCache("/",$server->getHost(),$formattedTagsChunk);
        }
        $errorCount = count($unresponsiveServerError);

        if ($errorCount > 0) {
            $loggerMessage = implode(" ", $unresponsiveServerError);

            if ($errorCount == count($servers)) {
                $this->loggerFile->addCritical(
                    'No cache server(s) could be purged ' . $loggerMessage,
                    compact('server', 'formattedTagsChunk')
                );
                return false;
            }

            $this->loggerFile->addWarning(
                'Unresponsive cache server(s) hit' . $loggerMessage,
                compact('server', 'formattedTagsChunk')
            );
        }

        $this->loggerFile->addDebug('cache_invalidate: ', compact('servers', 'formattedTagsChunk'));
        return true;
    }

    /**
     * Split tags into batches to suit Varnish max. header size
     *
     * @param array $tags
     * @return \Generator
     */
    private function chunkTags(array $tags): \Generator
    {
        $currentBatchSize = 0;
        $formattedTagsChunk = [];
        foreach ($tags as $formattedTag) {
            // Check if (currentBatchSize + length of next tag + number of pipe delimiters) would exceed header size.
            if ($currentBatchSize + strlen($formattedTag) + count($formattedTagsChunk) > $this->maxHeaderSize) {
                yield implode('|', $formattedTagsChunk);
                $formattedTagsChunk = [];
                $currentBatchSize = 0;
            }

            $currentBatchSize += strlen($formattedTag);
            $formattedTagsChunk[] = $formattedTag;
        }
        if (!empty($formattedTagsChunk)) {
            yield implode('|', $formattedTagsChunk);
        }
    }

}
