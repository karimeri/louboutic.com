<?php
namespace Louboutin\Varnish\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Louboutin\Varnish\Model\VarnishScheduleFactory;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule as VarnishScheduleResource;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule\Collection as VarnishScheduleCollectionFactory;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule;
/**
 * Class VarnishScheduleRepository
 * @package Louboutin\Varnish\Model
 */
class VarnishScheduleRepository
{

    const CAT_TYPE = 'cat_c';
    const CMS_TYPE = 'cms_';


    /**
     * @var VarnishScheduleCollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Louboutin\Varnish\Model\VarnishSchedule
     */
    protected $varnishScheduleFactory;

    /**
     * @var VarnishScheduleResource
     */
    protected $varnishScheduleResource;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * VarnishScheduleRepository constructor.
     * @param VarnishSchedule\Collection $collectionFactory
     * @param \Louboutin\Varnish\Model\VarnishSchedule $varnishScheduleFactory
     * @param VarnishSchedule $varnishScheduleResource
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        VarnishScheduleCollectionFactory $collectionFactory,
        \Louboutin\Varnish\Model\VarnishSchedule $varnishScheduleFactory,
        VarnishScheduleResource $varnishScheduleResource,
        ResourceConnection $resourceConnection,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->varnishScheduleFactory = $varnishScheduleFactory;
        $this->varnishScheduleResource = $varnishScheduleResource;
        $this->resourceConnection = $resourceConnection;
        $this->objectManager = $objectManager;
    }


    public function getScheduleByTag($tag)
    {
        /**
         * @var \Louboutin\Varnish\Model\VarnishSchedule $$schedule
         */
        $schedule = $this->objectManager->create('Louboutin\Varnish\Model\VarnishSchedule');
        $collection = $schedule->getCollection()
            ->addFieldToFilter('tag', array('like'=> $tag))
            ->setPageSize(1);

        return $collection;

    }

    /*
    public function getByTag($tag)
    {
        $tag = $this->varnishScheduleResource->getByTag($tag);

        return $tag;
    }


    public function save(VarnishSchedule $varnishSchedule)
    {
        $this->varnishScheduleResource->save($varnishSchedule);
    }

    public function loadVarnishScheduleByTag($tag)
    {
        return $this->varnishScheduleFactory->create()->load($tag, 'tag');
    }*/

    public function loadVarnishScheduleToClean($type)
    {

        switch ($type) {
            case 'cat';
                $tag = $this::CAT_TYPE;
                break;
            case 'cms':
                $tag = $this::CMS_TYPE;
                break;
            default :
                return null;
        }

        /**
         * @var \Louboutin\Varnish\Model\VarnishSchedule $schedule
         */

        $schedule = $this->objectManager->create('Louboutin\Varnish\Model\VarnishSchedule');
        $collection = $schedule->getCollection();
        $collection->addFieldToFilter('tag', array('like' => $tag . '%'))
                   ->addFieldToFilter('status', 'pending');


        return $collection;
    }

    /*


    public function loadVarnishScheduleByTag($tag)
    {

        $post = $this->varnishScheduleFactory->create();
        $collection = $post->getCollection();
        $collection->addFieldToFilter('tag', array('like' => $tag))
            ->addFieldToFilter('status', 'pending');

             // @codingStandardsIgnoreLine
        $collection->setPageSize(1);


        return $collection;
    }

*/
    public function deleteMultiple()
    {
        $where = [
            'tag NOT IN (?)' => '.*'
        ];

        //$this->resourceConnection->getConnection()
        $this->resourceConnection->getConnection()->delete(VarnishSchedule::TABLE_NAME, $where);
    }
}
