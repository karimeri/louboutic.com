<?php
namespace Louboutin\Varnish\Model;

use Magento\Framework\Model\AbstractModel;
use Louboutin\Varnish\Model\ResourceModel\VarnishSchedule as VarnishScheduleResource;

/**
 * Class VarnishSchedule
 * @package Louboutin\Varnish\Model
 */
class VarnishSchedule extends AbstractModel
{

    /**
     * tag field name
     */
    const TAG = 'tag';

    /**
     * created_at field name
     */
    const CREATED_AT = 'created_at';

    /**
     * scheduled_at count field name
     */
    const SCHEDULED_AT = 'flushed_at';

    /**
     * flush_type date field name
     */
    const FLUSH_TYPE = 'flush_type';

    /**
     * status date field name
     */
    const STATUS = 'status';

    /**
     * flushed_by date field name
     */
    const FLUSHED_BY = 'flushed_by';

    /**
     * nb_request date field name
     */
    const NB_REQUEST = 'nb_request';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(VarnishScheduleResource::class);
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return (string) $this->getData(self::TAG);
    }

    /**
     * @param string $tag
     * @return VarnishSchedule
     */
    public function setTag($tag)
    {
        $this->setData(self::TAG, $tag);
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @param $date
     * @return $this
     */
    public function setCreatedAt($date)
    {
        $this->setData(self::CREATED_AT, $date);
        return $this;
    }

    /**
     * @return int
     */
    public function getScheduledAt()
    {
        return (int) $this->getData(self::SCHEDULED_AT);
    }

    /**
     * @param $scheduled_at
     * @return $this
     */
    public function setScheduledAt($scheduled_at)
    {
        $this->setData(self::SCHEDULED_AT, $scheduled_at);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getFlushType()
    {
        return $this->getData(self::FLUSH_TYPE);
    }

    /**
     * @param $type
     * @return $this
     */
    public function setFlushType($type)
    {
        $this->setData(self::FLUSH_TYPE, $type);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getFlushedBy()
    {
        return $this->getData(self::FLUSHED_BY);
    }

    /**
     * @param $user
     * @return $this
     */
    public function setFlushedBy($user)
    {
        $this->setData(self::FLUSHED_BY, $user);
        return $this;
    }

    /**
     * @return array|mixed|null
     */
    public function getNbRequest()
    {
        return $this->getData(self::NB_REQUEST);
    }

    /**
     * @param $nb
     * @return $this
     */
    public function setNbRequest($nb)
    {
        $this->setData(self::NB_REQUEST, $nb);
        return $this;
    }

}
