<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Louboutin\Varnish\Block;

/**
 * @api
 * @since 100.0.2
 */
class Cache extends \Magento\Backend\Block\Cache
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'cache';
        $this->_headerText = __('Cache Storage Management');//nadia
        parent::_construct();
        $this->buttonList->remove('add');

        if ($this->_authorization->isAllowed('Magento_Backend::flush_magento_cache')) {
            $this->buttonList->add(
                'flush_magento',
                [
                    'label' => __('Flush Magento Cache'),
                    'onclick' => 'setLocation(\'' . $this->getFlushSystemUrl() . '\')',
                    'class' => 'primary flush-cache-magento'
                ]
            );
        }

        if ($this->_authorization->isAllowed('Magento_Backend::flush_cache_storage')) {
            $message = __('The cache storage may contain additional data. Are you sure that you want to flush it?');
            $this->buttonList->add(
                'flush_system',
                [
                    'label' => __('Flush Cache Storage'),
                    'onclick' => 'confirmSetLocation(\'' . $message . '\', \'' . $this->getFlushStorageUrl() . '\')',
                    'class' => 'flush-cache-storage'
                ]
            );
        }

        if ($this->_authorization->isAllowed('Magento_Backend::flush_cache_categories')) {
            $this->buttonList->add(
                'flush_category',
                [
                    'label' => __('Flush Categories Cache'),
                    'onclick' => 'setLocation(\'' . $this->getFlushCategoriesUrl() . '\')',
                    'class' => 'primary flush-categories-magento'
                ]
            );
        }

         if ($this->_authorization->isAllowed('Magento_Backend::flush_cache_cms')) {
        $this->buttonList->add(
            'flush_cms_content',
            [
                'label' => __('Flush CMS Cache'),
                'onclick' => 'setLocation(\'' . $this->getFlushCmsUrl() . '\')',
                'class' => 'primary flush-cms-magento'
            ]
        );
       }
    }

    /**
     * Get url for clean cache categories
     *
     * @return string
     */
    public function getFlushCategoriesUrl()
    {

        return $this->getUrl('adminhtml/flush/flushCategoryAll', ['_secure' => true]);
    }

    /**
     * Get url for clean cache cms
     *
     * @return string
     */
    public function getFlushCmsUrl()
    {
        return $this->getUrl('adminhtml/flush/flushCmsAll', ['_secure' => true]);
    }


}
