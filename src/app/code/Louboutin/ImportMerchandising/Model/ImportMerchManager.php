<?php

namespace Louboutin\ImportMerchandising\Model;

use Magento\Catalog\Model\CategoryProductLink;
use Synolia\ImportExport\Model\Import\Catalog\CategoryPathManagement;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Pimgento\Entities\Model\ResourceModel\Entities as PimEntites;

/**
 * Class ConfigManager
 * @package Louboutin\ImportMerchandising\Model
 */
class ImportMerchManager
{
    const COLUMN_ADMIN_PATH             = 'admin_path';
    const COLUMN_SKU                    = 'sku';
    const COLUMN_POSITION               = 'position';

    const COLUMN_ENTITY_ID              = 'entity_id';
    const COLUMN_CATEGORY_ID            = 'category_id';
    const COLUMN_ROW_NUM                = 'row_num';
    const COLUMN_PATH                   = 'path';
    const COLUMN_STORE_ID               = 'store_id';
    const ADD_ADMIN_PATH                = 'add_admin_path';

    const ERROR_ADMIN_PATH_NOT_FOUND    = 'adminPathNotFound';
    const ERROR_ADMIN_PATH_IS_EMPTY     = 'adminPathEmpty';
    const ERROR_STORE_IS_EMPTY          = 'storeEmpty';
    const ERROR_STORE_NOT_FOUND         = 'storeNotFound';
    const ERROR_NAME_IS_EMPTY           = 'nameEmpty';

    const ENTITY_TYPE_CODE              = 'merchandising';

    /**
     * @var string
     */
    protected $masterAttributeCode = 'admin_path';

    /** @var int */
    protected $lastPosition = 9999;

    /**
     * @var array
     */
    protected $_permanentAttributes = [
        self::COLUMN_ADMIN_PATH,
        self::COLUMN_SKU
    ];

    /**
     * @var array
     */
    protected $mappingFields = [
        self::COLUMN_ADMIN_PATH => self::COLUMN_PATH
    ];


    const BEHAVIOR_ADD_UPDATE = 'add_update';

    const BEHAVIOR_REPLACE = 'replace';

    /**
     * @var array
     */
    protected $_availableBehaviors = [
        self::BEHAVIOR_ADD_UPDATE,
        self::BEHAVIOR_REPLACE
    ];

    /** @var  \Magento\Store\Model\ResourceModel\Store\Collection */
    protected $storeCollectionFactory;

    /** @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory */
    protected $categoryCollectionFactory;

    /** @var \Synolia\ImportExport\Model\Catalog\CategoryRepository */
    protected $categoryRepository;

    /** @var \Magento\Catalog\Model\CategoryFactory */
    protected $categoryFactory;

    /** @var array */
    protected $categoryPaths = [];
    /** @var int */
    protected $rowNum = 0;

    /** @var \Magento\Framework\DB\Adapter\AdapterInterface */
    protected $connection;


    /** @var FilterManager */
    protected $filterManager;

    /** @var CategoryLinkManagement */
    protected $categoryLinkManagement;

    /** @var CategoryLinkRepository */
    protected $categoryLinkRepository;

    /** @var ProductRepository */
    protected $productRepository;

    /** @var  CategoryPathManagement */
    protected $categoryPathManagement;

    /** @var CategoryProductLinkFactory */
    protected $categoryProductLinkFactory;

    /** @var \Magento\Store\Model\App\Emulation */
    protected $emulation;


    /** @var \Magento\Framework\ObjectManagerInterface*/
    protected $_objectManager = null;

    /** @var array */
    protected $categoryLink = [];

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;


    /**
     * Merchandising constructor.
     * @param \Synolia\ImportExport\Model\Catalog\CategoryRepository $categoryRepository
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Catalog\Model\CategoryLinkManagement $categoryLinkManagement
     * @param \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository
     * @param \Magento\Catalog\Model\CategoryProductLinkFactory $categoryProductLinkFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Synolia\ImportExport\Model\Import\Catalog\CategoryPathManagementFactory $categoryPathManagementFactory
     * @param \Magento\Store\Model\App\Emulation $emulation
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param Filesystem $filesystem
     * @param LoggerInterface $logger
     */
    public function __construct(
        \Synolia\ImportExport\Model\Catalog\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Catalog\Model\CategoryLinkManagement $categoryLinkManagement,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository,
        \Magento\Catalog\Model\CategoryProductLinkFactory $categoryProductLinkFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Synolia\ImportExport\Model\Import\Catalog\CategoryPathManagementFactory $categoryPathManagementFactory,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Filesystem $filesystem,
        LoggerInterface $logger
    ) {
        $this->categoryPathManagement     = $categoryPathManagementFactory->create();
        $this->categoryRepository         = $categoryRepository;
        $this->categoryProductLinkFactory = $categoryProductLinkFactory;
        $this->categoryFactory            = $categoryFactory;
        $this->filterManager              = $filterManager;
        $this->categoryLinkManagement     = $categoryLinkManagement;
        $this->categoryLinkRepository     = $categoryLinkRepository;
        $this->productRepository          = $productRepository;
        $this->emulation                  = $emulation;
        $this->_objectManager             = $objectManager;
        $this->_filesystem                = $filesystem;
        $this->logger                     = $logger;
    }

    public function importMerchCsv()
    {

        $rowNum = 0;
        $entitiesToCreate = [];
        $entitiesToUpdate = [];

        $fileName = 'product_catogery_postion.csv';
        $directory = $this->_filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $pathFile = $directory->getAbsolutePath($fileName);
        //var_dump($pathFile);
        try {

            if ($directory->isExist($pathFile)) {

                $header = false;
                $rowDatFormatted = null;
                $file = $directory->openFile($directory->getRelativePath($fileName), 'r');
                while (($rowData = $file->readCsv(0,';')) !== false) {

                        //formatted ad build array csv
                        if(!$header)
                        {
                            $header = $rowData;
                        }
                        else
                        {
                            $rowDatFormatted = array_combine($header, $rowData);
                        }

                        if(isset($rowDatFormatted) && !is_null($rowDatFormatted))
                        {
                            $processedData = $this->_prepareData($rowDatFormatted);
                            $processedData[self::COLUMN_ROW_NUM] = $rowNum;

                            $categories[$processedData[self::COLUMN_CATEGORY_ID]] = $this->getCategoryLinkProducts(
                                $processedData[self::COLUMN_CATEGORY_ID]
                            );

                            $entitiesToCreate[$processedData[self::COLUMN_CATEGORY_ID]][$processedData[self::COLUMN_SKU]] = $processedData;


                        }

                }
                //var_dump('LINK TO CREATE');
                //var_dump($entitiesToCreate);
                if (!empty($entitiesToCreate)) {
                    $this->deleteMerchandisingEntities($entitiesToCreate);
                    $this->addMerchandisingEntities($entitiesToCreate);
                }
                return true;
            }
            else
            {
               echo 'No file product_catogery_postion.csv found in '.$pathFile;
            }
        }
        catch (\Exception $exception) {
            throw new \Exception(sprintf($exception->getMessage(), $exception->getCode()));
        }


    }

    /**
     * @param $rowData
     * @return array
     */
    public function _prepareData($rowData)
    {
        $finalArray = [];
        foreach ($rowData as $key => $value) {
            if ($key == self::COLUMN_ADMIN_PATH) {
                //REWRITE FOR PIMGENTO ENTITES
                $arrayCat = array_filter(explode("/", $value));
                $pathCategory = end($arrayCat);
                $finalArray[self::ADD_ADMIN_PATH] = $pathCategory;
            }

            if (array_key_exists($key, $this->mappingFields)) {
                $finalArray[$this->mappingFields[$key]] = $value;
                continue;
            }

            $finalArray[$key] = $value;
        }

        $categoryId = $this->getCategIdFromPimEntities(
            $finalArray[self::ADD_ADMIN_PATH]
        );
        $finalArray[self::COLUMN_CATEGORY_ID] = $categoryId;

        return $finalArray;
    }


    /**
     * @param $pimCategCode
     * @return string
     */
    public function getCategIdFromPimEntities($pimCategCode)
    {
        try{

            $pimgentoCurrentCateg = $this->_objectManager->create('\Pimgento\Entities\Model\Entities')->load($pimCategCode, 'code');

            if (is_null($pimgentoCurrentCateg->getData())) {
                $this->addRowError(self::ERROR_ADMIN_PATH_NOT_FOUND, $pimCategCode);
                throw new \Exception(sprintf(self::ERROR_ADMIN_PATH_NOT_FOUND.' Row: '.$pimCategCode));
            }
        }
        catch (\Exception $exception) {
            throw new \Exception(sprintf($exception->getMessage(), $exception->getCode()));
        }

        return $pimgentoCurrentCateg->getEntityId();
    }

    /**
     * @param $categoryId
     * @return array|mixed
     */
    public function getCategoryLinkProducts($categoryId)
    {
        if (array_key_exists($categoryId, $this->categoryLink)) {
            return $this->categoryLink[$categoryId];
        }

        $categoryLinks = [];
        try {
            $productLinks = $this->categoryRepository->getAssignedProducts($categoryId);
        } catch (\Exception $excetpion) {
            return $this->categoryLink[$categoryId] = [];
        }

        /** @var CategoryProductLink $categoryLink */
        foreach ($productLinks as $productLink) {
            $categoryLinks[$productLink->getSku()] = [
                self::COLUMN_POSITION => $productLink->getPosition()
            ];
        }

        $this->categoryLink[$categoryId] = $categoryLinks;

        return $this->categoryLink[$categoryId];
    }

    /**
     * @param $entitiesToCreate
     */
    public function addMerchandisingEntities($entitiesToCreate)
    {
        //var_dump("ADD Entities");
        //var_dump($entitiesToCreate);

        foreach ($entitiesToCreate as $category => $productLinks) {
            $this->addCategoryProductLink($category, $productLinks);
        }
    }

    /**
     * @param $entitiesToUpdate
     */
    public function updateMerchandisingEntities($entitiesToUpdate)
    {
        foreach ($entitiesToUpdate as $category => $productLinks) {
            $productLinks = $this->updateCategoryProductLink($category, $productLinks);

            if (!empty($productLinks)) {
                $this->addCategoryProductLink($category, $productLinks);
            }
        }
    }

    /**
     * @param $category
     * @param $productLinks
     * @return mixed
     */
    public function updateCategoryProductLink($category, $productLinks)
    {
        $categoryProductLink = $this->categoryRepository->getAssignedProducts($category);

        $storeId = $this->categoryRepository->getAffectedStoreId($category);
        $this->emulation->startEnvironmentEmulation($storeId);

        foreach ($categoryProductLink as $productLink) {

            if ($productLink->getPosition() == $this->getLastPosition()
                && !array_key_exists($productLink->getSku(), $productLinks)
            ) {
                continue;
            }

            $productLink->setPosition($this->getLastPosition());

            if (array_key_exists($productLink->getSku(), $productLinks)) {
                $productLink->setPosition($productLinks[$productLink->getSku()][self::COLUMN_POSITION]);
                unset($productLinks[$productLink->getSku()]);
            }

            try {
                $this->categoryLinkRepository->save($productLink);
            } catch (\Exception $exception) {
                throw new \Exception(sprintf($exception->getMessage().' Row: '.$productLink[self::COLUMN_ROW_NUM], $exception->getCode()));
            }
        }

        $this->emulation->stopEnvironmentEmulation();

        return $productLinks;
    }

    /**
     * @param $entitiesToDelete
     */
    public function deleteMerchandisingEntities($entitiesToDelete)
    {
        //var_dump("DELETE Entities");
        //var_dump($entitiesToDelete);

        foreach ($entitiesToDelete as $category => $productLink) {
            $categoryProductLinks = $this->categoryRepository->getAssignedProducts($category);
            $storeId              = $this->categoryRepository->getAffectedStoreId($category);
            $this->emulation->startEnvironmentEmulation($storeId);

            //var_dump("Product Link");
            //var_dump($categoryProductLinks);
            if (!empty($categoryProductLinks)) {
                foreach ($categoryProductLinks as $categoryProductLink) {
                    if (array_key_exists($categoryProductLink->getSku(), $productLink)) {
                        continue;
                    }
                    $this->categoryLinkRepository->delete($categoryProductLink);
                }
            }

            $this->emulation->stopEnvironmentEmulation();
        }
    }

    /**
     * @param $category
     * @param $productLinks
     */
    public function addCategoryProductLink($category, $productLinks)
    {
        $storeId = $this->categoryRepository->getAffectedStoreId($category);
        $this->emulation->startEnvironmentEmulation($storeId);

        foreach ($productLinks as $productLink) {
            /** @var CategoryProductLink $categoryProductLink */
            $categoryProductLink = $this->categoryProductLinkFactory->create();
            $categoryProductLink->setSku($productLink[self::COLUMN_SKU]);
            $categoryProductLink->setPosition($productLink[self::COLUMN_POSITION]);
            $categoryProductLink->setCategoryId($category);

            try {
                $this->categoryLinkRepository->save($categoryProductLink);
            } catch (\Exception $exception) {
                throw new \Exception(sprintf($exception->getMessage().' Row: '.$productLink[self::COLUMN_ROW_NUM], $exception->getCode()));
            }
        }

        $this->emulation->stopEnvironmentEmulation();
    }

    /**
     * @return int
     */
    public function getLastPosition()
    {
        return $this->lastPosition;
    }


}
