<?php

namespace Louboutin\ImportMerchandising\Console\Command;

use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Louboutin\ImportMerchandising\Model\ImportMerchManager;

/**
 * Class ImportMerch
 * @package Louboutin\ImportMerchandising\Console\Command
 */
class ImportMerch extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Louboutin\ImportMerchandising\Model\ImportMerchManager
     */
    protected $merchManager;

    /**
     * ImportMerch constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Louboutin\ImportMerchandising\Model\ImportMerchManager $importMerchManager
     */
    public function __construct(
        State $state,
        ImportMerchManager $importMerchManager,
        $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->merchManager = $importMerchManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:importmerchandising:import-merch')->setDescription('Import merch categories/products via csv');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if (!$this->state->getAreaCode()) {
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
            }

            $output->writeln("<info>Import Merch from CSV ...</info>");

            $this->merchManager->importMerchCsv();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
