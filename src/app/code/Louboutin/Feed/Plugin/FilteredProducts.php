<?php

namespace Louboutin\Feed\Plugin;

use Mirasvit\Feed\Export\Resolver\GeneralResolver;
use Magento\Catalog\Model\Product\Attribute\Source\Status;

class FilteredProducts
{

    /**
     * @param \Mirasvit\Feed\Export\Resolver\GeneralResolver $subject
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function afterGetProducts(GeneralResolver $subject, $collection)
    {
        $collection->addFieldToFilter('status', Status::STATUS_ENABLED)
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addFieldToFilter('price', ['gt' => 0]);

        if ($collection->isLoaded()) {
            $collection->clear()
                ->load();
        }

        return $collection;
    }
}