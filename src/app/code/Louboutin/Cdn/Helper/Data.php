<?php

namespace Louboutin\Cdn\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class Data
 * @package Louboutin\Cdn\Helper
 */
class Data extends AbstractHelper
{
    const XML_PATH_CDN_CLIENT_ID        = 'louboutin_cdn/config/client_id';
    const XML_PATH_CDN_CLIENT_SECRET    = 'louboutin_cdn/config/client_secret';
    const XML_PATH_CDN_TENANT_ID        = 'louboutin_cdn/config/tenant_id';
    const XML_PATH_CDN_SUBSCRIPTION_ID  = 'louboutin_cdn/config/subscription_id';
    const XML_PATH_CDN_RESOURCE_GROUP   = 'louboutin_cdn/config/resource_group';
    const XML_PATH_CDN_NAME             = 'louboutin_cdn/config/name';

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Data constructor.
     * @param EncryptorInterface $encryptor
     * @param Context $context
     */
    public function __construct(
        EncryptorInterface $encryptor,
        Context $context
    ) {
        $this->encryptor = $encryptor;
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getCdnClientId() {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_CDN_CLIENT_ID
        );
    }

    /**
     * @return string
     */
    public function getCdnClientSecret() {
        $password = $this->scopeConfig->getValue(
            self::XML_PATH_CDN_CLIENT_SECRET
        );

        return $this->encryptor->decrypt($password);
    }

    /**
     * @return string
     */
    public function getCdnTenantId() {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_CDN_TENANT_ID
        );
    }

    /**
     * @return string
     */
    public function getCdnSubscriptionId() {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_CDN_SUBSCRIPTION_ID
        );
    }

    /**
     * @return string
     */
    public function getCdnResourceGroup() {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_CDN_RESOURCE_GROUP
        );
    }

    /**
     * @return string
     */
    public function getCdnName() {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_CDN_NAME
        );
    }
}
