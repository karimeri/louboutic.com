<?php

namespace Louboutin\Cdn\Console\Command;

use Louboutin\Cdn\Model\PurgeManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Purge
 * @package Louboutin\Cdn\Console\Command
 */
class Purge extends Command
{
    const INPUT_PATH = 'path';

    /**
     * @var PurgeManager
     */
    protected $purgeManager;

    /**
     * Purge constructor.
     * @param PurgeManager $purgeManager
     * @param string|null $name
     */
    public function __construct(
        PurgeManager $purgeManager,
        string $name = null
    ) {
        $this->purgeManager = $purgeManager;
        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:cdn:purge')->setDescription('Purge CDN path');
        $this->addArgument(self::INPUT_PATH, InputArgument::REQUIRED);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln("<info>Purge CDN path ...</info>");

            $path = $input->getArgument(self::INPUT_PATH);
            $this->purgeManager->purge($path);

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
