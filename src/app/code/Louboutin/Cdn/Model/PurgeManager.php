<?php

namespace Louboutin\Cdn\Model;

use Louboutin\Cdn\Helper\Data as CdnHelper;
use Psr\Log\LoggerInterface;

/**
 * Class PurgeManager
 * @package Louboutin\Cdn\Model
 */
class PurgeManager
{
    /**
     * @var CdnHelper
     */
    protected $helper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    private $bearerToken = '';

    /**
     * PurgeManager constructor.
     * @param CdnHelper $helper
     * @param LoggerInterface $logger
     * @throws \Exception
     */
    public function __construct(
        CdnHelper $helper,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        try {
            $this->authentication();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function authentication()
    {
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://login.microsoftonline.com/" . $this->helper->getCdnTenantId() . "/oauth2/token"
        );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "grant_type=client_credentials&client_id=" . $this->helper->getCdnClientId() .
            "&client_secret=" . $this->helper->getCdnClientSecret() . "&resource=https%3A%2F%2Fmanagement.azure.com%2F"
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $serverOutput = curl_exec($ch);
        curl_close($ch);
        $jsonOutput = json_decode($serverOutput, true);
        if (array_key_exists('access_token', $jsonOutput)) {
            $this->bearerToken = $jsonOutput['access_token'];
            return true;
        } else {
            throw new \Exception('Authentication failed');
        }
    }

    /**
     * @param string $path
     * @return bool
     * @throws \Exception
     */
    public function purge(string $path)
    {
        if (empty($path)) {
            throw new \Exception('Path required');
        }

        $url = "https://management.azure.com/subscriptions/" . $this->helper->getCdnSubscriptionId() .
            "/resourceGroups/". $this->helper->getCdnResourceGroup(). "/providers/Microsoft.Network/frontdoors/" .
            $this->helper->getCdnName() . "/purge?api-version=2019-05-01";
        $ch = curl_init($url);
        $userAgent = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31';
        $requestHeaders = array();
        $requestHeaders[] = 'User-Agent: ' . $userAgent;
        $requestHeaders[] = 'Content-Type: application/json';
        $requestHeaders[] = 'Authorization: Bearer '. $this->bearerToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => '{contentPaths: ["' . $path . '"]}',
        ]);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $this->logger->debug('CDN - PATH TO PURGE: ' . $path);
        $this->logger->debug('CDN - PURGE RESULT : ' . $result);
        $this->logger->debug('CDN - PURGE RESULT CODE : ' . $httpCode);
        $jsonOutput = json_decode($result, true);
        if ($jsonOutput && array_key_exists('error', $jsonOutput)) {
            throw new \Exception($jsonOutput['error']['message']);
        } else {
            return true;
        }
    }
}
