<?php

namespace Louboutin\Sales\Controller\Adminhtml\Order\Creditmemo;

class View extends \Magento\Sales\Controller\Adminhtml\Order\Creditmemo\View
{
    /**
     * Creditmemo information page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $this->creditmemoLoader->setOrderId($this->getRequest()->getParam('order_id'));
        $this->creditmemoLoader->setCreditmemoId($this->getRequest()->getParam('creditmemo_id'));
        $this->creditmemoLoader->setCreditmemo($this->getRequest()->getParam('creditmemo'));
        $this->creditmemoLoader->setInvoiceId($this->getRequest()->getParam('invoice_id'));
        $creditmemo = $this->creditmemoLoader->load();
        if ($creditmemo) {
            $resultPage = $this->resultPageFactory->create();
            $resultPage->setActiveMenu('Magento_Sales::sales_creditmemo');
            $resultPage->getConfig()->getTitle()->prepend(__('Credit Memos'));
            $resultPage->getConfig()->getTitle()->prepend(sprintf("#%s", $creditmemo->getIncrementId()));
            $resultPage->getLayout()->getBlock('sales_creditmemo_view')
                ->updateBackButtonUrl($this->getRequest()->getParam('come_from'));
            return $resultPage;
        } else {
            $resultForward = $this->resultForwardFactory->create();
            $resultForward->forward('noroute');
            return $resultForward;
        }
    }
}