<?php

namespace Louboutin\VisualMerchandiser\Model\Sorting;

use Magento\VisualMerchandiser\Model\Sorting\SortInterface;

/**
 * Class Factory
 * @package Louboutin\VisualMerchandiser\Model\Sorting
 */
class Factory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * @param string $className
     * @param array $data
     * @return SortInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create($className, array $data = [])
    {
        $instance = $this->_objectManager->create('\Louboutin\VisualMerchandiser\Model\Sorting\\'.$className, $data);

        if (!$instance instanceof SortInterface) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('%1 doesn\'t implement SortInterface', $className)
            );
        }
        return $instance;
    }
}
