<?php

namespace Louboutin\VisualMerchandiser\Model\Sorting\Status;

use Magento\VisualMerchandiser\Model\Sorting\AttributeAbstract;

/**
 * Class EnabledDisabled
 * @package Louboutin\VisualMerchandiser\Model\Sorting\Status
 */
class EnabledDisabled extends AttributeAbstract
{
    /**
     * @return string
     */
    protected function getSortDirection()
    {
        return $this->ascOrder();
    }

    /**
     * @return string
     */
    protected function getSortField()
    {
        return 'status';
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return __("Status : Enabled - Disabled");
    }
}
