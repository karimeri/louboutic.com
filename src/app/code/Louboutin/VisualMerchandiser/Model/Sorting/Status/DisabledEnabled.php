<?php

namespace Louboutin\VisualMerchandiser\Model\Sorting\Status;

use Magento\VisualMerchandiser\Model\Sorting\AttributeAbstract;

/**
 * Class DisabledEnabled
 * @package Louboutin\VisualMerchandiser\Model\Sorting\Status
 */
class DisabledEnabled extends AttributeAbstract
{
    /**
     * @return string
     */
    protected function getSortDirection()
    {
        return $this->descOrder();
    }

    /**
     * @return string
     */
    protected function getSortField()
    {
        return 'status';
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return __("Status : Disabled - Enabled");
    }
}
