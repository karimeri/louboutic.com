<?php

namespace Louboutin\VisualMerchandiser\Model;

use Louboutin\VisualMerchandiser\Model\Sorting\Factory as CustomFactory;

/**
 * Class Sorting
 * @package Louboutin\VisualMerchandiser\Model
 */
class Sorting extends \Magento\VisualMerchandiser\Model\Sorting
{
    /**
     * @var array
     */
    protected $customSortClasses = [
        'Status\EnabledDisabled',
        'Status\DisabledEnabled'
    ];

    /**
     * @var Sorting\Factory
     */
    protected $customFactory;

    /**
     * Sorting constructor.
     * @param \Magento\VisualMerchandiser\Model\Sorting\Factory $factory
     * @param CustomFactory $customFactory
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        \Magento\VisualMerchandiser\Model\Sorting\Factory $factory,
        CustomFactory $customFactory
    ) {
        \Magento\VisualMerchandiser\Model\Sorting::__construct($factory);
        $this->customFactory = $customFactory;
        foreach ($this->customSortClasses as $className) {
            $this->sortInstances[] = $this->customFactory->create($className);
        }
    }
}
