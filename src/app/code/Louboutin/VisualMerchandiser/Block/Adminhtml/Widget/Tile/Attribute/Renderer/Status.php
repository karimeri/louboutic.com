<?php

namespace Louboutin\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer;

use Magento\Catalog\Model\Product\Attribute\Source\Status as StatusSource;

/**
 * Class Status
 * @package Louboutin\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer
 */
class Status extends \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer
{
    /**
     * @var StatusSource
     */
    protected $statusSource;

    /**
     * Status constructor.
     * @param \Magento\Framework\Escaper $escaper
     * @param StatusSource $statusSource
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Escaper $escaper,
        StatusSource $statusSource,
        array $data = []
    ) {
        parent::__construct($escaper, $data);

        $this->statusSource = $statusSource;
    }

    /**
     * @return string
     */
    public function render()
    {
        return '<span>'
            . $this->escaper->escapeHtml(
                __('Status')
                . ': '
                . $this->statusSource->getOptionText($this->getValue())
            )
            . '</span></br>';
    }
}
