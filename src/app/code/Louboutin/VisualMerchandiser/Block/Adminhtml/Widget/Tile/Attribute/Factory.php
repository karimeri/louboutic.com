<?php

namespace Louboutin\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute;

/**
 * Class Factory
 * @package Louboutin\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute
 */
class Factory extends \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Factory
{
    /**
     * Formatted as 'attribute_code' => [config]
     * @return array
     */
    protected function _prepareAttributeRenderers()
    {
        $renderers = [
            'price' => [
                'renderer' => \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer\Price::class
            ],
            'stock' => [
                'renderer' => \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer\Stock::class
            ],
            'name' => [
                'renderer' => \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer\Name::class
            ],
            'status' => [
                'renderer' => \Louboutin\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Renderer\Status::class
            ]
        ];
        return $renderers;
    }
}
