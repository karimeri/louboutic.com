<?php

namespace Louboutin\VisualMerchandiser\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\Tile;

/**
 * Class InstallData
 * @package Louboutin\VisualMerchandiser\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var ResourceConfig
     */
    private $resourceConfig;

    /**
     * InstallData constructor.
     * @param ResourceConfig $resourceConfig
     */
    public function __construct(
        ResourceConfig $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $this->resourceConfig->saveConfig(
            Tile::XML_PATH_ADDITIONAL_ATTRIBUTES,
            'name,sku,price,stock,status',
            'default',
            '0'
        );
    }
}
