<?php
namespace Veriteworks\Veritrans\Cron;

use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\Registry;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Sales\Api\OrderManagementInterface;

class CleanupOrder
{
    /**
     *
     */
    const ORDER_STATUS = 'peyment_review';

    /**
     *
     */
    const EXPIRE_LIMIT = '1800';

    /**
     * @var CollectionFactory
     */
    private $collection;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderManagementInterface
     */
    private $orderManager;

    /**
     * @var array
     */
    private $methodNames;


    /**
     * CleanupOrder constructor.
     * @param Registry $coreRegistry
     * @param CollectionFactory $salesOrderCollection
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderManagementInterface $orderManagement
     * @param array $methodNames
     */
    public function __construct(
        Registry $coreRegistry,
        CollectionFactory $salesOrderCollection,
        OrderRepositoryInterface $orderRepository,
        OrderManagementInterface $orderManagement,
        array $methodNames = []
    ) {
        $this->collection = $salesOrderCollection;
        $this->registry = $coreRegistry;
        $this->orderRepository = $orderRepository;
        $this->orderManager = $orderManagement;
        $this->methodNames = $methodNames;
    }

    /**
     * execute job. Cleanup abandoned order
     */
    public function execute()
    {
        $expire_time = strftime('%Y-%m-%d %H:%M:%S', time() - self::EXPIRE_LIMIT);


        $this->collection->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('updated_at', ['gteq' => $expire_time])
            ->addAttributeToFilter('status', self::ORDER_STATUS)
            ->load();

        $this->registry->register('isSecureArea', true, true);

        /** @var \Magento\Sales\Model\Order $order */
        foreach($this->collection as $order)
        {
            $method = $order->getPayment()->getMethod();
            if(!in_array($method, $this->methodNames)) {
                continue;
            }
            $order->setState('new');
            $this->orderManager->save();
            $this->orderManager->cancel($order);
            $this->orderRepository->delete($order);
        }

    }

}