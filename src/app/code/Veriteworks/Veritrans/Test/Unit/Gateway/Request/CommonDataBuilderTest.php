<?php
namespace Veriteworks\Veritrans\Test\Unit\Gateway\Request;

use Veriteworks\Veritrans\Gateway\Config\Cc;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Veriteworks\Veritrans\Gateway\Request\CommonDataBuilder;
use Magento\Payment\Gateway\Data\OrderAdapterInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order\Payment;
use Veriteworks\Veritrans\Helper\Data;

/**
 * Class CommonDataBuilderTest
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CommonDataBuilderTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var CommonDataBuilder
     */
    private $builder;

    /**
     * @var Config|\PHPUnit_Framework_MockObject_MockObject
     */
    private $configMock;

    /**
     * @var Payment|\PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $paymentDO;

    /**
     * @var SubjectReader|\PHPUnit_Framework_MockObject_MockObject
     */
    private $subjectReaderMock;

    /**
     * @var OrderAdapterInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $orderMock;

    /**
     * @var Data|\PHPUnit_Framework_MockObject_MockObject
     */
    private $helperMock;

    protected function setUp()
    {
        $this->paymentDO = $this->getMock(PaymentDataObjectInterface::class);

        $this->configMock = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->paymentMock = $this->getMockBuilder(Payment::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->subjectReaderMock = $this->getMockBuilder(SubjectReader::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderMock = $this->getMock(OrderAdapterInterface::class);

        $this->helperMock = $this->getMockBuilder(Data::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->builder = new CommonDataBuilder($this->configMock,
                                                $this->helperMock,
                                                $this->subjectReaderMock);
    }


    public function testBuild()
    {
        $expectedResult = [
            'params' => [
                    CommonDataBuilder::IS_DUMMY => '1',
                    CommonDataBuilder::TXN_VERSION => '2.0.0',
                    CommonDataBuilder::MERCHANT_ID => '123456789'
                ]
        ];

        $buildSubject = [
            'payment' => $this->paymentDO,
            'amount' => 10
        ];

        $this->helperMock->expects(static::once())
            ->method('getMerchantId')
            ->willReturn('123456789');
        $this->helperMock->expects(static::once())
            ->method('getTxnVersion')
            ->willReturn('2.0.0');
        $this->helperMock->expects(static::once())
            ->method('getIsTest')
            ->willReturn('1');

        static::assertEquals(
            $expectedResult,
            $this->builder->build($buildSubject)
        );
    }
}
