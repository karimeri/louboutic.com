<?php

namespace Veriteworks\Veritrans\Test\Unit\Gateway\Http;

use Veriteworks\Veritrans\Gateway\Http\TransferFactory;
use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferInterface;
use \Veriteworks\Veritrans\Helper\Data;

/**
 * Class TransferFactoryTest
 */
class TransferFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TransferFactory
     */
    private $transferFactory;

    /**
     * @var TransferFactory
     */
    private $transferMock;

    /**
     * @var TransferBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    private $transferBuilder;

    /**
     * @var Data|\PHPUnit_Framework_MockObject_MockObject
     */
    private $helperMock;

    protected function setUp()
    {
        $this->transferBuilder = $this->getMock(TransferBuilder::class);
        $this->transferMock = $this->getMock(TransferInterface::class);
        $this->helperMock = $this->getMockBuilder(Data::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->transferFactory = new TransferFactory(
            $this->transferBuilder,
            $this->helperMock
        );
    }

    public function testCreate()
    {
        $request = ['data1', 'data2'];

        $this->transferBuilder->expects($this->once())
            ->method('setBody')
            ->with($request)
            ->willReturnSelf();
        $this->transferBuilder->expects($this->once())
            ->method('setMethod')
            ->with(\Zend_Http_Client::POST)
            ->willReturnSelf();

        $this->transferBuilder->expects($this->once())
            ->method('shouldEncode')
            ->with('UTF-8')
            ->willReturnSelf();

        $this->transferBuilder->expects($this->once())
            ->method('build')
            ->willReturn($this->transferMock);

        $this->transferBuilder->expects($this->once())
            ->method('setUri')
            ->willReturnSelf();

        $this->helperMock->expects($this->once())
            ->method('getApiUrl')
            ->willReturn('https://api.veritrans.co.jp:443/');

        $this->assertEquals($this->transferMock, $this->transferFactory->create($request));
    }
}
