<?php
namespace Veriteworks\Veritrans\Test\Unit\Gateway\Validator;

use \Magento\Payment\Gateway\ConfigInterface;
use \Magento\Payment\Gateway\Validator\ResultInterfaceFactory;
use \Magento\Payment\Gateway\Validator\ResultInterface;
use \Veriteworks\Veritrans\Gateway\Validator\CurrencyValidator;

class CurrencyValidatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $configMock;

    /**
     * @var ResultInterfaceFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultInterfaceMock;

    /**
     * @var ResultInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $resultMock;

    /**
     * setup
     */
    public function setup()
    {
        $this->configMock = $this->getMockBuilder(ConfigInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultInterfaceMock = $this->getMockBuilder(ResultInterfaceFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->resultMock = $this->getMockBuilder(ResultInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }


    /**
     * @param $currency
     * @param $storeId
     * @param $expected
     *
     * @dataProvider currencyDataProvider
     */
    public function testValidate($currency, $storeId, $expected)
    {
        $this->configMock->expects(static::once())
            ->method('getValue')
            ->willReturn($currency);

        $this->resultInterfaceMock->expects(static::once())
            ->method('create')
            ->willReturn(
                $this->resultMock
            );

        $validator = new CurrencyValidator(
            $this->resultInterfaceMock,
            $this->configMock
        );

        $result = $validator->validate(['storeId' => $storeId]);

        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    public function currencyDataProvider()
    {
        $resultMock = $this->getMockBuilder(ResultInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        return [
            [
                'JPY',
                '1',
                $resultMock
            ]
        ];
    }
}
