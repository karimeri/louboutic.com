<?php
namespace Veriteworks\Veritrans\Gateway\Config;

use \Magento\Payment\Gateway\Config\Config as BaseConfig;
/**
 * Class Config
 */
class Cvs extends BaseConfig
{
    const CODE = 'veritrans_cvs';
    const KEY_ACTIVE = 'active';
    const KEY_PAY_LIMIT = 'paylimit';


    /**
     * Get Payment configuration status
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getValue(self::KEY_ACTIVE);
    }
    
    public function getPayLimit()
    {
        return $this->getValue(self::KEY_PAY_LIMIT);
    }

}
