<?php
namespace Veriteworks\Veritrans\Gateway\Config;

use \Magento\Payment\Gateway\Config\Config as BaseConfig;
/**
 * Class Config
 */
class Cc extends BaseConfig
{
    const KEY_ACTIVE = 'active';
    const KEY_CC_TYPES = 'cctypes';
    const KEY_USE_CVV = 'useccv';


    /**
     * Retrieve available credit card types
     *
     * @return array
     */
    public function getAvailableCardTypes()
    {
        $ccTypes = $this->getValue(self::KEY_CC_TYPES);

        return !empty($ccTypes) ? explode(',', $ccTypes) : [];
    }

    /**
     * Check if cvv field is enabled
     * @return boolean
     */
    public function isCvvEnabled()
    {
        return (bool) $this->getValue(self::KEY_USE_CVV);
    }

    /**
     * Get Payment configuration status
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getValue(self::KEY_ACTIVE);
    }

}
