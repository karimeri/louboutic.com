<?php
namespace Veriteworks\Veritrans\Gateway\Config;

use \Magento\Payment\Gateway\Config\Config as BaseConfig;
/**
 * Class Config
 */
class Bank extends BaseConfig
{
    const CODE = 'veritrans_bank';
    const KEY_ACTIVE = 'active';
    const KEY_PAY_LIMIT = 'paylimit';
    const KEY_CONTACTS = 'contacts';
    const KEY_CONTACTS_KANA = 'contacts_kana';


    /**
     * Get Payment configuration status
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getValue(self::KEY_ACTIVE);
    }
    
    public function getPayLimit()
    {
        return $this->getValue(self::KEY_PAY_LIMIT);
    }

    public function getContacts()
    {
        return $this->getValue(self::KEY_CONTACTS);
    }

    public function getContactsKana()
    {
        return $this->getValue(self::KEY_CONTACTS_KANA);
    }
}
