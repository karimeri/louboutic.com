<?php
namespace Veriteworks\Veritrans\Gateway\Config;

use \Magento\Payment\Gateway\Config\Config as BaseConfig;
/**
 * Class Config
 */
class Upop extends BaseConfig
{
    const CODE = 'veritrans_upop';
    const KEY_ACTIVE = 'active';

    /**
     * Get Payment configuration status
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getValue(self::KEY_ACTIVE);
    }

}
