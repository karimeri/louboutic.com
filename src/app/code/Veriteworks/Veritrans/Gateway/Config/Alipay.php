<?php
namespace Veriteworks\Veritrans\Gateway\Config;

use \Magento\Payment\Gateway\Config\Config as BaseConfig;
/**
 * Class Config
 */
class Alipay extends BaseConfig
{
    const CODE = 'veritrans_alipay';
    const KEY_ACTIVE = 'active';

    /**
     * Get Payment configuration status
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getValue(self::KEY_ACTIVE);
    }

}
