<?php
namespace Veriteworks\Veritrans\Gateway\Response;

use Veriteworks\Veritrans\Gateway\Config\Cc;
use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;

/**
 * Class CardDetailsHandler
 */
class CardDetailsHandler implements HandlerInterface
{
    const CARD_TYPE = 'cardType';

    const CARD_EXP_MONTH = 'expirationMonth';

    const CARD_EXP_YEAR = 'expirationYear';

    const CARD_LAST4 = 'last4';

    const CARD_NUMBER = 'cc_number';

    /**
     * @var Cc
     */
    private $config;

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param Cc $config
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        Cc $config,
        SubjectReader $subjectReader
    ) {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->subjectReader->readPayment($handlingSubject);
        //$transaction = $response['result'];

        /**
         * @TODO after changes in sales module should be refactored for new interfaces
         */
        $payment = $paymentDO->getPayment();
        ContextHelper::assertOrderPayment($payment);

    }


}
