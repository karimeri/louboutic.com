<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Gateway\Response;

use Veriteworks\Veritrans\Observer\AssignCcFormData;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;

/**
 * Mpi Handler
 */
class MpiHandler implements HandlerInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $chekcoutSession;

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;


    /**
     * MpiHandler constructor.
     * @param \Veriteworks\Veritrans\Gateway\Helper\SubjectReader $subjectReader
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        SubjectReader $subjectReader,
        \Magento\Checkout\Model\Session $checkoutSession,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->subjectReader = $subjectReader;
        $this->chekcoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritdoc
     */
    public function handle(array $handlingSubject, array $response)
    {
        $paymentDO = $this->subjectReader->readPayment($handlingSubject);
        /** @var array $transaction */
        $transaction = $response['result'];

        if($this->getConfig('use_3dsecure') && array_key_exists('resResponseContents', $transaction)) {
            if ($contents = $transaction['resResponseContents']) {
                $this->chekcoutSession->setContent($transaction['resResponseContents']);
            }
            /** @var Payment $orderPayment */
            $orderPayment = $paymentDO->getPayment();
            $orderPayment->setIsTransactionClosed(false);
            $orderPayment->setIsTransactionPending(true);
            $order = $orderPayment->getOrder();
            $order->setCanSendNewEmailFlag(false);
        } else {
            $this->chekcoutSession->setContent(null);
        }
    }
    /**
     * @param $key
     * @return mixed
     */
    private function getConfig($key)
    {
        $key = 'payment/veritrans_cc/' . $key;
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }
}
