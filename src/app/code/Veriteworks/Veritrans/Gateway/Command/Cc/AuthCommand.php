<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Gateway\Command\Cc;

use Magento\Framework\Exception\LocalizedException;
use Veriteworks\Veritrans\Gateway\Command\logExceptions;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Gateway\Validator\ValidatorInterface;
use \Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
//use Magento\Payment\Gateway\Command\CommandException;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Psr\Log\LoggerInterface;

/**
 * Class AuthCommand
 * @SuppressWarnings(PHPMD)
 */
class AuthCommand implements CommandInterface
{
    use logExceptions;
    /**
     * @var BuilderInterface
     */
    private $requestBuilder;

    /**
     * @var TransferFactoryInterface
     */
    private $transferFactory;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var HandlerInterface
     */
    private $handler;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var CheckoutHelper
     */
    private $checkoutHelper;

    private $key;


    /**
     * AuthCommand constructor.
     * @param \Magento\Payment\Gateway\Request\BuilderInterface $requestBuilder
     * @param \Magento\Payment\Gateway\Http\TransferFactoryInterface $transferFactory
     * @param \Magento\Payment\Gateway\Http\ClientInterface $client
     * @param \Psr\Log\LoggerInterface $logger
     * @param CheckoutHelper $checkoutHelper
     * @param \Magento\Payment\Gateway\Response\HandlerInterface|null $handler
     * @param \Magento\Payment\Gateway\Validator\ValidatorInterface|null $validator
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param string $key
     */
    public function __construct(
        BuilderInterface $requestBuilder,
        TransferFactoryInterface $transferFactory,
        ClientInterface $client,
        LoggerInterface $logger,
        CheckoutHelper $checkoutHelper,
        HandlerInterface $handler = null,
        ValidatorInterface $validator = null,
        ScopeConfigInterface $scopeConfig,
        $key = 'payment/veritrans_cc/'
    ) {
        $this->requestBuilder = $requestBuilder;
        $this->transferFactory = $transferFactory;
        $this->client = $client;
        $this->handler = $handler;
        $this->validator = $validator;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->checkoutHelper = $checkoutHelper;
        $this->key = $key;
    }

    /**
     * @inheritdoc
     */
    public function execute(array $commandSubject)
    {
        $transferO = $this->transferFactory->create(
            $this->requestBuilder->build($commandSubject)
        );

        $body = $transferO->getBody();

        $apiPath = '';

        if($this->getConfig('use_3dsecure'))
        {
            $apiPath = 'Authorize/mpi';
        } else {
            $apiPath = 'Authorize/card';
        }
        if(array_key_exists('originalOrderId', $body['params'])) {
            $apiPath = 'ReAuthorize/card';
        }

        $response = $this->client
            ->setApiPath($apiPath)
            ->placeRequest($transferO);

        if ($this->validator !== null) {
            $result = $this->validator->validate(
                array_merge($commandSubject, ['response' => $response])
            );
            if (!$result->isValid()) {
                $messages = $result->getFailsDescription();
                $this->logExceptions($messages);
                $this->checkoutHelper->sendPaymentFailedEmail(
                    $this->checkoutHelper->getQuote(),
                    $messages[0]
                );
                throw new LocalizedException(
                    __($messages[0])
                );
            }
        }

        if ($this->handler) {
            $this->handler->handle(
                $commandSubject,
                $response
            );
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    private function getConfig($key)
    {
        $key = $this->key . $key;
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }
}
