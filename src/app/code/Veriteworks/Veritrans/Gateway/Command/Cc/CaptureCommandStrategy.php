<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Gateway\Command\Cc;

use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Command\CommandPoolInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class CaptureCommand
 * @SuppressWarnings(PHPMD)
 */
class CaptureCommandStrategy implements CommandInterface
{
    const SALE = 'sale';

    const CAPTURE = 'settlement';

    /**
     * @var CommandPoolInterface
     */
    private $commandPool;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    private $key;


    /**
     * CaptureCommandStrategy constructor.
     * @param CommandPoolInterface $commandPool
     * @param ScopeConfigInterface $scopeConfig
     * @param string $key
     */
    public function __construct(
        CommandPoolInterface $commandPool,
        ScopeConfigInterface $scopeConfig,
        $key = 'payment/veritrans_cc/'
    ) {
        $this->commandPool = $commandPool;
        $this->scopeConfig = $scopeConfig;
        $this->key = $key;
    }

    /**
     * @inheritdoc
     */
    public function execute(array $commandSubject)
    {
        $mode = $this->getConfig('payment_action');

        $command = $this->getCommand($mode);
        $this->commandPool->get($command)->execute($commandSubject);
    }

    /**
     * Get execution command name
     * @param string $mode
     * @return string
     */
    private function getCommand($mode)
    {
        if ($mode == 'authorize_capture') {
            return self::SALE;
        }

        if ($mode == 'authorize') {
            return self::CAPTURE;
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    private function getConfig($key)
    {
        $key = $this->key . $key;
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }
}
