<?php
namespace Veriteworks\Veritrans\Gateway\Validator;

use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Validator\ResultInterfaceFactory;
use Magento\Payment\Gateway\Validator\ResultInterface;
use Magento\Payment\Gateway\Validator\AbstractValidator;
use \Magento\Framework\Registry;
use \Veriteworks\Veritrans\Model\Reauth;

/**
 * Class ReauthValidator
 * @package Magento\Payment\Gateway\Validator
 * @api
 */
class ReauthValidator extends AbstractValidator
{
    /**
     * @var \Magento\Payment\Gateway\ConfigInterface
     */
    private $config;

    /**
     * @var Reauth
     */
    private $reauth;

    /**
     * @var Registry
     */
    private $registry;


    /**
     * ReauthValidator constructor.
     * @param ResultInterfaceFactory $resultFactory
     * @param Reauth $reauth
     * @param Registry $coreRegistry
     * @param ConfigInterface $config
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory,
        Reauth $reauth,
        Registry $coreRegistry,
        ConfigInterface $config
    ) {
        $this->config = $config;
        $this->registry = $coreRegistry;
        $this->reauth = $reauth;
        parent::__construct($resultFactory);
    }

    /**
     * @param array $validationSubject
     * @return ResultInterface
     * @throws NotFoundException
     * @throws \Exception
     */
    public function validate(array $validationSubject)
    {
        $isValid = false;
        /** @var \Magento\Payment\Gateway\Data\PaymentDataObject $paymentData */
        $paymentData = $validationSubject['payment'];
        /** @var \Magento\Payment\Model\InfoInterface $info */
        $info = $paymentData->getPayment();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $info->getOrder();


        return $this->createResult($isValid);
    }
}
