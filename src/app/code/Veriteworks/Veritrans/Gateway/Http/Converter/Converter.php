<?php
namespace Veriteworks\Veritrans\Gateway\Http\Converter;

use Magento\Payment\Gateway\Http\ConverterInterface;

class Converter implements ConverterInterface
{

    /**
     * @param mixed $response
     * @return array
     */
    public function convert($response)
    {
        $data = [];
        $data = json_decode($response, true);

        return $data;
    }

}