<?php

namespace Veriteworks\Veritrans\Gateway\Http\Adapter;

use \Magento\Store\Model\ScopeInterface;
use \Magento\Framework\Exception\LocalizedException;

class Mc extends Veritrans
{
    /**
     * @param array $body
     * @return string
     */
    protected function generateHash(array $body)
    {
        $json = json_encode($body['params']);
        $merchantId = $this->getConfig('mc_merchant_id');
        $secretKey = $this->getConfig('mc_merchant_password');

        $hash = hash('SHA256', $merchantId . $json . $secretKey, false);
        return $hash;
    }
}