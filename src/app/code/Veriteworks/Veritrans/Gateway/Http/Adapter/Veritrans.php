<?php

namespace Veriteworks\Veritrans\Gateway\Http\Adapter;

use \Magento\Store\Model\ScopeInterface;
use \Magento\Framework\Exception\LocalizedException;

class Veritrans implements AdapterInterface
{
    /**
     * @var int
     *
     * Retry Counter
     */
    protected $retryCount = 0;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Veriteworks\Veritrans\Logger\Logger
     */
    protected $logger;

    /**
     * @var \Zend_Http_Client_Adapter_Curl
     */
    protected $adapter;

    /**
     * @var string
     */
    protected $apiPath;

    /**
     * GATEWAY URL
     */
    const GATEWAY_URL = 'https://api.veritrans.co.jp:443/';


    /**
     * Abstact constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Veriteworks\Veritrans\Logger\Logger $logger
     * @param \Zend_Http_Client_Adapter_Curl $adapter
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Veriteworks\Veritrans\Logger\Logger $logger,
        \Zend_Http_Client_Adapter_Curl $adapter,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->adapter = $adapter;
    }

    /**
     * @param $path
     */
    public function setApiPath($path)
    {
        $test = '';
        if ($this->getConfig('is_test')) {
            $test .= 'test-';
        }

        if(preg_match('/search/', $path))
        {
            $this->apiPath = self::GATEWAY_URL . $test . 'paynow-search/v2/' . $path;
        } else {
            $this->apiPath = self::GATEWAY_URL . $test . 'paynow/v2/' . $path;
        }

        return $this;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Http_Client_Exception
     */
    public function execute(array $param)
    {
        $returnValue = array();

        $config = array(
            'adapter'      => $this->adapter,
            'ssltransport' => 'tls'
        );

        $param['authHash'] = $this->generateHash($param);

        $client = new \Zend_Http_Client($this->apiPath, $config);
        $client->setHeaders('Accept: application/json');
        $client->setHeaders('Content-type: application/json; charset=utf-8');
        $client->setRawData(json_encode($param), 'text/json');

        try {
            $response = $client->request('POST');

            if ($response->isError()) {
                if ($this->retryCount < 3) {
                    $this->retryCount++;
                    $this->execute($param);
                } else {
                    $returnValue['ErrCode'] = 'network error';
                }
            } else {
                $this->log(var_export($this->apiPath, true));
                //$this->log(var_export($param, true));
                $this->log(var_export($response, true));

                $returnValue = json_decode($response->getBody(), true);
            }
        } catch (\Exception $e) {
            if ($this->retryCount < 3) {
                $this->retryCount++;
                $this->execute($param);
            } else {
                $returnValue['ErrCode'] = 'network error';
            }
        }

        return $returnValue;
    }

    /**
     * Record log
     *
     * @param string $str Log string
     */
    protected function log($str)
    {
        $this->logger->info($str);
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function getConfig($key)
    {
        $key = 'veritrans/common/' . $key;
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param array $body
     * @return string
     */
    protected function generateHash(array $body)
    {
        $json = json_encode($body['params']);
        $merchantId = $this->getConfig('merchant_id');
        $secretKey = $this->getConfig('merchant_password');

        $hash = hash('SHA256', $merchantId . $json . $secretKey, false);
        return $hash;
    }
}