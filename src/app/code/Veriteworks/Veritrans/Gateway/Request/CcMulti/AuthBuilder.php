<?php
namespace Veriteworks\Veritrans\Gateway\Request\CcMulti;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Cc;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;

/**
 * Cc Data Builder
 */
class AuthBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'mcAmount';
    const ORDER_ID = 'orderId';
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const JPO = 'jpo';
    const CARD_EXPIRE = 'cardExpire';
    const CARD_NUMBER = 'cardNumber';
    const CVV = 'securityCode';
    const CURRENCY = 'currencyUnit';
    const TOKEN = 'token';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Cc
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param Cc $config
     * @param SubjectReader $subjectReader
     */
    public function __construct(Cc $config, SubjectReader $subjectReader)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        $amount    = $this->subjectReader->readAmount($buildSubject);
        $ccNumber = $payment->getAdditionalInformation('cc_number');
        $ccCid = $payment->getAdditionalInformation('cc_cid');
        $currency = $order->getCurrencyCode();
        $token = $payment->getAdditionalInformation('token');

        if(in_array($currency, array('KRW', 'VND'))) {
            $amount = $this->formatPrice($amount);
        } else {
            $amount = sprintf('%0.2f', $amount);
        }

        $result = [
            'params' => [
                self::AMOUNT => $this->formatPrice($amount),
                self::JPO => $this->formatJpo($payment),
                self::ORDER_ID => $order->getOrderIncrementId(),
                self::WITH_CAPTURE => $this->getCaptureMode()
            ]
        ];

        if($token) {
            $token_data = [
                'params' => [
                    'payNowIdParam' => [
                        self::TOKEN => $token
                    ]
                ]
            ];
            $result = array_merge_recursive($result, $token_data);
        } else {
            $normal_data = [
                'params' => [
                    self::CARD_EXPIRE => $this->formatExpire($payment),
                    self::CARD_NUMBER => $ccNumber,
                    self::CVV => $ccCid,
                ]
            ];
            $result = array_merge_recursive($result, $normal_data);
        }

        return $result;
    }


    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return string
     */
    private function formatExpire(InfoInterface $payment)
    {
        $month = $payment->getAdditionalInformation('cc_exp_month');
        $year = $payment->getAdditionalInformation('cc_exp_year');

        return sprintf("%02d", $month) . '/' . substr($year, -2);
    }


    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return mixed|string
     */
    private function formatJpo(InfoInterface $payment)
    {
        $jpo = '10';
        return $jpo;
    }

    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }
}
