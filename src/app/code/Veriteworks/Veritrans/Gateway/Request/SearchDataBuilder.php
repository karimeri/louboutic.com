<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Cc;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;
use Veriteworks\Veritrans\Helper\Data;
use \Magento\Framework\Exception\LocalizedException;

/**
 * Search Data Builder
 */
class SearchDataBuilder implements BuilderInterface
{
    use Formatter;

    const ORDER_ID = 'orderId';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Cc
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * @var \Veriteworks\Veritrans\Helper\Data
     */
    private $helper;

    /**
     * Constructor
     *
     * @param Cc $config
     * @param \Veriteworks\Veritrans\Helper\Data $helper
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        Cc $config,
        Data $helper,
        SubjectReader $subjectReader
    ) {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
        $this->helper = $helper;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $payment->getOrder();


        if (!is_object($order)) {
            throw new LocalizedException(__('Invalid order information was provided.'));
        }

        $result = [
            'params' => [
                    'searchParameters' => [
                        'common' => [
                            self::ORDER_ID => $order->getIncrementId()
                        ]
                    ],
                    'newerFlag' => "true",
                    'containDummyFlag' => $this->helper->getIsTest()
                ]
        ];

        return $result;
    }
}
