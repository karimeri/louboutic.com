<?php
namespace Veriteworks\Veritrans\Gateway\Request\ReMulti;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Cc;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;

/**
 * Cc Data Builder
 */
class AuthBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'mcAmount';
    const ORDER_ID = 'orderId';
    const ORIGINAL_ORDER_ID = 'originalOrderId';
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const JPO = 'jpo';
    const CURRENCY = 'currencyUnit';
    const WITH_CAPTURE = 'withCapture';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Cc
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param Cc $config
     * @param SubjectReader $subjectReader
     */
    public function __construct(Cc $config, SubjectReader $subjectReader)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        $amount    = $this->subjectReader->readAmount($buildSubject);
        $currency = $order->getCurrencyCode();
        $originalOrderId = $payment->getAdditionalInformation('original_order_id');

        if(in_array($currency, array('KRW', 'VND'))) {
            $amount = $this->formatPrice($amount);
        } else {
            $amount = sprintf('%0.2f', $amount);
        }

        $result = [
            'params' => [
                self::AMOUNT => $this->formatPrice($amount),
                self::JPO => $this->formatJpo($payment),
                self::ORDER_ID => $order->getOrderIncrementId(),
                self::ORIGINAL_ORDER_ID => $originalOrderId,
                self::WITH_CAPTURE => $this->getCaptureMode()
            ]
        ];

        return $result;
    }


    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return string
     */
    private function formatExpire(InfoInterface $payment)
    {
        $month = $payment->getAdditionalInformation('cc_exp_month');
        $year = $payment->getAdditionalInformation('cc_exp_year');

        return sprintf("%02d", $month) . '/' . substr($year, -2);
    }


    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return mixed|string
     */
    private function formatJpo(InfoInterface $payment)
    {
        $jpo = '10';
        return $jpo;
    }

    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }

    private function getCaptureMode()
    {
        if($this->config->getValue('payment_action') == 'authorize') {
            return 'false';
        } else {
            return 'true';
        }
    }
}
