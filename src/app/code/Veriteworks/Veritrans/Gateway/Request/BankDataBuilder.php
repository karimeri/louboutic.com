<?php
namespace Veriteworks\Veritrans\Gateway\Request;

use \Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use \Veriteworks\Veritrans\Gateway\Config\Bank;
use \Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use \Magento\Payment\Gateway\Request\BuilderInterface;
use \Magento\Payment\Helper\Formatter;
use \Magento\Payment\Model\InfoInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Bank Data Builder
 */
class BankDataBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'amount';
    const ORDER_ID = 'orderId';
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const TEL_NO = 'telNo';
    const PAY_LIMIT = 'payLimit';
    const SERVICE_OPTION_TYPE = 'serviceOptionType';
    const FIRST_NAME = 'name2';
    const LAST_NAME = 'name1';
    const FIRST_NAME_KANA = 'kana2';
    const LAST_NAME_KANA = 'kana1';
    const PAYMENT_TYPE = 'paymentType';
    const CONTACTS = 'contents';
    const CONTACTS_KANA = 'contentsKana';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Bank
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * Constructor
     *
     * @param Bank $config
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        Bank $config,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        SubjectReader $subjectReader)
    {
        $this->config = $config;
        $this->orderFactory = $orderFactory;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        /** @var \Magento\Sales\Model\Order $order */
        $origOrder = $payment->getOrder();
        $billing   = $origOrder->getBillingAddress();
        $amount    = (int)$order->getGrandTotalAmount();;
        $lastname  = mb_substr($billing->getLastname(), 0, 9, 'utf-8');
        $firstname = mb_substr($billing->getFirstname(), 0, 9, 'utf-8');
        $lastnamekana  = mb_substr($billing->getLastnamekana(), 0, 9, 'utf-8');
        $firstnamekana = mb_substr($billing->getFirstnamekana(), 0, 9, 'utf-8');
        $bankType = $payment->getAdditionalInformation('bank_type');
        $payLimit = $this->calcPayLimit($this->config->getPayLimit());
        $contacts = $this->config->getContacts();
        $contactsKana = $this->config->getContactsKana();

        if(!$lastname) {
            $message = __('Please set lastname.');
            throw new LocalizedException($message);
        }

        if(!$firstname) {
            $message = __('Please set firstname.');
            throw new LocalizedException($message);
        }

        if(!$lastnamekana) {
            $message = __('Please set lastname kana.');
            throw new LocalizedException($message);
        }

        if(!$firstnamekana) {
            $message = __('Please set firstname kana.');
            throw new LocalizedException($message);
        }

        $this->checkName($lastname);
        $this->checkName($firstname);
        $this->checkKana($lastnamekana);
        $this->checkKana($firstnamekana);

        $result = [
            'params' => [
                self::SERVICE_OPTION_TYPE => $bankType,
                self::AMOUNT => $this->formatPrice($amount),
                self::ORDER_ID => $order->getOrderIncrementId(),
                self::LAST_NAME => $lastname,
                self::FIRST_NAME => $firstname,
                self::LAST_NAME_KANA => $lastnamekana,
                self::FIRST_NAME_KANA => $firstnamekana,
                self::TEL_NO => $this->formatTel($billing->getTelephone()),
                self::PAY_LIMIT => $payLimit,
                self::PAYMENT_TYPE => '0',
                self::CONTACTS => $contacts,
                self::CONTACTS_KANA => $contactsKana
            ]
        ];

        return $result;
    }



    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }

    /**
     * @param $tel
     * @return mixed
     */
    private function formatTel($tel)
    {
        $dashes = ["―","－", "－", "-"];

        return str_replace($dashes, "", mb_convert_kana($tel, "rn"));
    }

    /**
     * @param $limit
     * @param string $format
     * @return string
     */
    private function calcPayLimit($limit, $format='yyyyMMdd') {
        $date = new \Zend_Date();
        $date->add($limit, \Zend_Date::DAY);
        return $date->toString($format);
    }

    private function checkName($str)
    {
        $pattern1 = '/^(?:[\x00-\x7F\xA1-\xDF]|\x81[\x40-\x9E\xAD\xB8-\xBF\xC8-\xCE\xDA-\xE8\xF0-\xF7\xFC]|\x82[\x4F-\x58\x60-\x79\x81-\x9A\x9F-\xF1]|\x83[\x40-\x7E\x80-\x96\x9F-\xB6\xBF-\xD6]|\x84[\x40-\x60\x70-\x7E\x80-\x91\x9F-\xBE]|\x88[\x9F-\xFC]|[\x89-\x97][\x40-\x7E\x80-\xFC]|\x98[\x40-\x72])*$/';

        $pattern = '/^(?:';
        $pattern .= '[\x00-\x7F\xA1-\xDF]|\x81[\x52-\x56\x58\x59\x5B]';
        $pattern .= '|\x82[\x4F-\x58\x60-\x79\x81-\x9A\x9F-\xF1]|';
        $pattern .= '\x83[\x40-\x7E\x80-\x96]|[\x89-\x97\x99-\x9F\xE0-\xE9]';
        $pattern .= '[\x40-\x7E\x80-\xFC]|\x88[\x9F-\xFC]';
        $pattern .= '|\x98[\x40-\x72\x9F-\xFC]|\xEA[\x40-\x7E\x80-\xA4])*$/';

        if(!preg_match($pattern, trim(mb_convert_encoding($str, 'SJIS-win', 'utf-8')))) {
            $message = __('Your name contains invalid characters.');
            throw new LocalizedException($message);
        }
    }

    private function checkKana($str)
    {
        $pattern = '/^(?:[\x00-\x7F\xA1-\xDF]';
        $pattern .= '|\x81[\x40\x44\x5B\x5C\x5D\x69\x70\x75\x76]';
        $pattern .= '|\x82[\x4F-\x58\x60-\x79\x81-\x9A]';
        $pattern .= '|\x83[\x40-\x7E\x80-\x96])*$/';

        if(!preg_match($pattern, trim(mb_convert_encoding($str, 'SJIS-win', 'utf-8')))) {
            $message = __('Your name contains invalid characters.');
            throw new LocalizedException($message);
        }
    }
}
