<?php
namespace Veriteworks\Veritrans\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Veriteworks\Veritrans\Helper\Data;

/**
 * Class CommonDataBuilder
 */
class CommonDataBuilder implements BuilderInterface
{
    /**
     *
     */
    const MERCHANT_ID = 'merchantCcid';
    /**
     *
     */
    const TXN_VERSION = 'txnVersion';
    /**
     *
     */
    const IS_DUMMY    = 'dummyRequest';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var \Veriteworks\Veritrans\Helper\Data
     */
    private $helper;

    /**
     * CommonDataBuilder constructor.
     * @param \Veriteworks\Veritrans\Helper\Data $helper
     * @param \Veriteworks\Veritrans\Gateway\Helper\SubjectReader $subjectReader
     */
    public function __construct(
        Data $helper,
        SubjectReader $subjectReader)
    {
        $this->helper = $helper;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $result = [
            'params' => [
                    self::MERCHANT_ID => $this->helper->getMerchantId(),
                    self::TXN_VERSION => $this->helper->getTxnVersion(),
                    self::IS_DUMMY => $this->helper->getIsTest()
                ]
        ];

        return $result;
    }

}
