<?php
namespace Veriteworks\Veritrans\Gateway\Request;

use Magento\Framework\Exception\LocalizedException;
use \Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use \Veriteworks\Veritrans\Gateway\Config\Cvs;
use \Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use \Magento\Payment\Gateway\Request\BuilderInterface;
use \Magento\Payment\Helper\Formatter;
use \Magento\Payment\Model\InfoInterface;

/**
 * Cvs Data Builder
 */
class CvsDataBuilder implements BuilderInterface
{
    use Formatter;

    /**
     * amount
     */
    const AMOUNT = 'amount';
    /**
     * order id
     */
    const ORDER_ID = 'orderId';
    /**
     *
     */
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const TEL_NO = 'telNo';
    const PAY_LIMIT = 'payLimit';
    const SERVICE_OPTION_TYPE = 'serviceOptionType';
    const FIRST_NAME = 'name2';
    const LAST_NAME = 'name1';
    const PAYMENT_TYPE = 'paymentType';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Cvs
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param Cvs $config
     * @param SubjectReader $subjectReader
     */
    public function __construct(Cvs $config, SubjectReader $subjectReader)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        $billing   = $order->getBillingAddress();
        $amount    = (int)$order->getGrandTotalAmount();;
        $lastname  = mb_substr($billing->getLastname(), 0, 19, 'utf-8');
        $firstname = mb_substr($billing->getFirstname(), 0, 19, 'utf-8');
        $cvsType = $payment->getAdditionalInformation('cvs_type');
        $payLimit = $this->calcPayLimit($this->config->getPayLimit());

        $this->checkName($lastname);
        $this->checkName($firstname);

        $result = [
            'params' => [
                    self::SERVICE_OPTION_TYPE => $this->getCvsCode($cvsType),
                    self::AMOUNT => $this->formatPrice($amount),
                    self::ORDER_ID => $order->getOrderIncrementId(),
                    self::LAST_NAME => $lastname,
                    self::FIRST_NAME => $firstname,
                    self::TEL_NO => $this->formatTel($billing->getTelephone()),
                    self::PAY_LIMIT => $payLimit,
                    self::PAYMENT_TYPE => '0'
                ]
        ];

        return $result;
    }



    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }

    /**
     * @param $cvsType
     * @return string
     */
    private function getCvsCode($cvsType)
    {
        $code = "";
        switch ($cvsType) {
            case 'LA' :
            case 'SM' :
            case 'MS' :
            case 'FM' :
            case 'SU' :
            case 'CK' :
                $code = "econ";
                break;
            case 'DY' :
                $code = "other";
                break;
            case 'SE' :
                $code = "sej";
                break;
        }

        return $code;
    }

    /**
     * @param $tel
     * @return mixed
     */
    private function formatTel($tel)
    {
        $dashes = ["―","－", "－", "-"];

        return str_replace($dashes, "", mb_convert_kana($tel, "rn"));
    }

    /**
     * @param $limit
     * @param string $format
     * @return string
     */
    private function calcPayLimit($limit, $format='yyyy/MM/dd') {
        $date = new \Zend_Date();
        $date->add($limit, \Zend_Date::DAY);
        return $date->toString($format);
    }

    /**
     * @param $str
     * @throws LocalizedException
     */
    private function checkName($str)
    {
        $pattern = '/^(?:';
        $pattern .= '[\x00-\x7F\xA1-\xDF]|\x81[\x52-\x56\x58\x59\x5B]';
        $pattern .= '|\x82[\x4F-\x58\x60-\x79\x81-\x9A\x9F-\xF1]|';
        $pattern .= '\x83[\x40-\x7E\x80-\x96]|[\x89-\x97\x99-\x9F\xE0-\xE9]';
        $pattern .= '[\x40-\x7E\x80-\xFC]|\x88[\x9F-\xFC]';
        $pattern .= '|\x98[\x40-\x72\x9F-\xFC]|\xEA[\x40-\x7E\x80-\xA4])*$/';

        if(!preg_match($pattern, trim(mb_convert_encoding($str, 'SJIS-win', 'utf-8')))) {
            $message = __('Your name contains invalid characters.');
            throw new LocalizedException($message);
        }
    }
}