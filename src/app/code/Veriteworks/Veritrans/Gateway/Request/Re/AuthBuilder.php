<?php
namespace Veriteworks\Veritrans\Gateway\Request\Re;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Cc;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;

/**
 * Reauth Cc Data Builder
 */
class AuthBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'amount';
    const ORDER_ID = 'orderId';
    const ORIGINAL_ORDER_ID = 'originalOrderId';
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const JPO = 'jpo';
    const WITH_CAPTURE = 'withCapture';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Cc
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param Cc $config
     * @param SubjectReader $subjectReader
     */
    public function __construct(Cc $config, SubjectReader $subjectReader)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        $amount    = $this->subjectReader->readAmount($buildSubject);
        $originalOrderId = $payment->getAdditionalInformation('original_order_id');

        $result = [
            'params' => [
                self::AMOUNT => $this->formatPrice($amount),
                self::JPO => $this->formatJpo($payment),
                self::ORDER_ID => $order->getOrderIncrementId(),
                self::ORIGINAL_ORDER_ID => $originalOrderId,
                self::WITH_CAPTURE => $this->getCaptureMode()
            ]
        ];

        return $result;
    }

    /**
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @return mixed|string
     */
    private function formatJpo(InfoInterface $payment)
    {
        $jpo = '';

        $type = $payment->getAdditionalInformation('payment_type');
        $times = $payment->getAdditionalInformation('split_count');
        if ($type == 61) {
            $jpo = $type . 'C' . sprintf('%02d', $times);
        } elseif(!$type) {
            $jpo = '10';
        } else {
            $jpo = $type;
        }

        return $jpo;
    }

    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }

    private function getCaptureMode()
    {
        if($this->config->getValue('payment_action') == 'authorize') {
            return 'false';
        } else {
            return 'true';
        }
    }

}
