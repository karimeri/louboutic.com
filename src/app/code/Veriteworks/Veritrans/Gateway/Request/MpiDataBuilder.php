<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Gateway\Request;

use Veriteworks\Veritrans\Gateway\Config\Cc;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Helper\Formatter;
use \Magento\Store\Model\ScopeInterface;

/**
 * Mpi Data Builder
 */
class MpiDataBuilder implements BuilderInterface
{
    use Formatter;

    /**
     *
     */
    const OPTION_TYPE = 'serviceOptionType';
    /**
     *
     */
    const REDIRECTION_URI = 'redirectionUri';
    /**
     *
     */
    const HTTP_USER_AGENT = 'httpUserAgent';
    /**
     *
     */
    const HTTP_ACCEPT = 'httpAccept';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Cc
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * Constructor
     *
     * @param Cc $config
     * @param SubjectReader $subjectReader
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\UrlInterface
     */
    public function __construct(
        Cc $config,
        SubjectReader $subjectReader,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\UrlInterface $url
    ) {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
        $this->scopeConfig = $scopeConfig;
        $this->url = $url;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $incrementId = $payment->getAdditionalInformation('increment_id');

        if (!$this->getConfig('use_3dsecure')) {
            return [];
        }

        if($incrementId) {
            return [];
        }

        $result = [
            'params' => [
                self::OPTION_TYPE => $this->getConfig('service_option_type'),
                self::HTTP_USER_AGENT => $this->getUserAgent(),
                self::HTTP_ACCEPT => $this->getHttpAccept(),
                self::REDIRECTION_URI => $this->getRedirectUrl()
            ]
        ];

        return $result;
    }

    /**
     * @param $key
     * @return mixed
     */
    private function getConfig($key)
    {
        $key = 'payment/veritrans_cc/' . $key;
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    private function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * @return mixed
     */
    private function getHttpAccept()
    {
        return $_SERVER['HTTP_ACCEPT'];
    }

    /**
     * @return string
     */
    private function getRedirectUrl()
    {
        return $this->url->getUrl('veritrans/mpi/receive',
            array('_secure' => true));
    }

}
