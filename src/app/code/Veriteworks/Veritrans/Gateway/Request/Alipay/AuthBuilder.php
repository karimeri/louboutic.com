<?php
namespace Veriteworks\Veritrans\Gateway\Request\Alipay;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Alipay;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;

/**
 * Alipay Auth Builder
 */
class AuthBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'amount';
    const ORDER_ID = 'orderId';
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const WITH_CAPTURE = 'withCapture';
    const SUCCESS_URL = 'successUrl';
    const ERROR_URL = 'errorUrl';
    const COMMODITY_NAME = 'commodityName';
    const PAY_TYPE = 'payType';
    const CURRENCY = 'currency';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Alipay
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * Constructor
     *
     * @param Alipay $config
     * @param SubjectReader $subjectReader
     * @param \Magento\Framework\UrlInterface
     */
    public function __construct(Alipay $config,
                                SubjectReader $subjectReader,
                                \Magento\Framework\UrlInterface $url)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
        $this->url = $url;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        $amount    = $this->subjectReader->readAmount($buildSubject);
        $withCapture = true;
        $payType   = '0';
        $commodityName = $this->config->getValue('name');
        $currency = $order->getCurrencyCode();

        $result = [
            'params' => [
                    self::AMOUNT => $this->formatPrice($amount),
                    self::WITH_CAPTURE => $withCapture,
                    self::SUCCESS_URL => $this->getSuccessUrl(),
                    self::ERROR_URL => $this->getErrorUrl(),
                    self::COMMODITY_NAME => $commodityName,
                    self::ORDER_ID => $order->getOrderIncrementId(),
                    self::CURRENCY => $currency,
                    self::PAY_TYPE => $payType
                ]
        ];

        return $result;
    }




    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }

    /**
     * @return string
     */
    private function getSuccessUrl()
    {
        return $this->url->getUrl('veritrans/alipay/receive',
            array('_secure' => true));
    }

    /**
     * @return string
     */
    private function getErrorUrl()
    {
        return $this->url->getUrl('veritrans/alipay/error',
            array('_secure' => true));
    }
}
