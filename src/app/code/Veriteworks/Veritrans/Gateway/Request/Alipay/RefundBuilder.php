<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Gateway\Request\Alipay;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Alipay;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;
use \Magento\Framework\Exception\LocalizedException;

/**
 * Alipay Refund Data Builder
 */
class RefundBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'amount';
    const ORDER_ID = 'orderId';
    const REASON = 'reason';
    const PAYNOW_ID_PARAM = 'payNowIdParam';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Alipay
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;

    /**
     * Constructor
     *
     * @param Alipay $config
     * @param SubjectReader $subjectReader
     */
    public function __construct(Alipay $config, SubjectReader $subjectReader)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $transactionId = $payment->getCcTransId();
        $amount    = $this->subjectReader->readAmount($buildSubject);
        $reason = $this->config->getValue('reason');

        if (!$transactionId) {
            throw new LocalizedException(__('No authorization transaction to proceed capture.'));
        }

        $result = [
            'params' => [
                    self::AMOUNT => $this->formatPrice($amount),
                    self::ORDER_ID => $transactionId,
                    self::REASON => $reason
                ]
        ];

        return $result;
    }

    /**
     * @param $price
     * @return int
     */
    public function formatPrice($price)
    {
        return (int)$price;
    }
}
