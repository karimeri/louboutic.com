<?php
namespace Veriteworks\Veritrans\Gateway\Request\Upop;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Veriteworks\Veritrans\Gateway\Config\Upop;
use Veriteworks\Veritrans\Gateway\Helper\SubjectReader;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Helper\Formatter;
use Magento\Payment\Model\InfoInterface;

/**
 * Upop Auth Builder
 */
class AuthBuilder implements BuilderInterface
{
    use Formatter;

    const AMOUNT = 'amount';
    const ORDER_ID = 'orderId';
    const PAYNOW_ID_PARAM = 'payNowIdParam';
    const WITH_CAPTURE = 'withCapture';
    const TERM_URL = 'termUrl';
    const CUSTOMER_IP = 'customerIp';

    /**
     * @var \Veriteworks\Veritrans\Gateway\Config\Upop
     */
    private $config;
    /**
     * @var \Veriteworks\Veritrans\Gateway\Helper\SubjectReader
     */
    private $subjectReader;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * Constructor
     *
     * @param Upop $config
     * @param SubjectReader $subjectReader
     * @param \Magento\Framework\UrlInterface
     */
    public function __construct(Upop $config,
                                SubjectReader $subjectReader,
                                \Magento\Framework\UrlInterface $url)
    {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
        $this->url = $url;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject)
    {
        $paymentDO = $this->subjectReader->readPayment($buildSubject);
        $payment   = $paymentDO->getPayment();
        $order     = $paymentDO->getOrder();
        $amount    = $this->subjectReader->readAmount($buildSubject);
        $withCapture = 'false';
        if($this->config->getValue('payment_action') == 'authorize_capture') {
            $withCapture = 'true';
        }

        $result = [
            'params' => [
                    self::AMOUNT => $this->formatPrice($amount),
                    self::WITH_CAPTURE => $withCapture,
                    self::TERM_URL => $this->getTermUrl(),
                    self::CUSTOMER_IP => $order->getRemoteIp(),
                    self::ORDER_ID => $order->getOrderIncrementId()
                ]
        ];

        return $result;
    }




    /**
     * @param $price
     * @return int
     */
    private function formatPrice($price)
    {
        return (int)$price;
    }

    /**
     * @return string
     */
    private function getTermUrl()
    {
        return $this->url->getUrl('veritrans/upop/receive',
            array('_secure' => true));
    }
}
