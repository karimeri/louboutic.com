<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@principle-works.jp so we can send you a copy immediately.
 *
 * @category   payment
 * @package    Veriteworks_Veritrans
 * @copyright  Copyright (c) 2016 Veriteworks Inc. (http://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Veriteworks\Veritrans\Helper;

use \Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @return mixed
     */
    public function getMerchantId()
    {
        return $this->_getConfig('merchant_id');
    }

    /**
     * @return mixed
     */
    public function getMerchantPassword()
    {
        return $this->_getConfig('merchant_password');
    }

    /**
     * @return mixed
     */
    public function getTxnVersion()
    {
        return $this->_getConfig('txn_version');
    }

    /**
     * @return mixed
     */
    public function getIsTest()
    {
        return $this->_getConfig('is_test');
    }

    /**
     * @return mixed
     */
    public function getApiUrl()
    {
        return $this->_getConfig('url');
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function _getConfig($key)
    {
        $key = 'veritrans/common/' . $key;
        return $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->scopeConfig->
        getValue('payment/veritrans_cc/payment_type', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getSplitCount()
    {
        return $this->scopeConfig->
        getValue('payment/veritrans_cc/split_count', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->scopeConfig->
        getValue('payment/veritrans_cc/token', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getMcToken()
    {
        return $this->scopeConfig->
        getValue('payment/veritrans_ccmulti/token', ScopeInterface::SCOPE_STORE);
    }

}