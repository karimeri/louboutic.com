<?php
namespace Veriteworks\Veritrans\Model\Reauth;

interface ReauthInterface
{
    /**
     * @return mixed
     */
    public function hasSucceededOrder();

    /**
     * @return mixed
     */
    public function getLatestOrderIncrementId();
}