<?php
namespace Veriteworks\Veritrans\Model\Reauth;

use \Magento\Backend\Model\Session\Quote;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Sales\Model\Order;

class Admin
{
    /**
     * @var null | \Magento\Sales\Model\Order
     */
    private $latestOrder;

    const STATUS = 'processing,complete,closed,canceled';


    public function __construct(
        Quote $currentQuote,
        CustomerRepositoryInterface $customerRepository,
        CollectionFactory $orderCollectionFactory,
        $method = 'veritrans_cc'
    ) {
        $customerId = $currentQuote->getCustomerId();
        if(is_null($customerId)) {
            $this->latestOrder = null;
        } else {
            $date = new \Zend_Date();
            $date->sub('1', \Zend_Date::YEAR);

            $collection = $orderCollectionFactory->create($customerId);
            $collection->join('sales_order_payment',
                'main_table.entity_id=parent_id')
                ->addAttributeToFilter('method', $method)
                ->addAttributeToFilter('created_at',
                    array('gteq' => $date->toString('Y-m-d H:m:s')))
                ->addAttributeToFilter('status', self::STATUS)
                ->addOrder('created_at', 'desc');

            if(count($collection) > 0) {
                /** @var \Magento\Sales\Model\Order latestOrder */
                $this->latestOrder = $collection->getFirstItem();
            } else {
                $this->latestOrder = null;
            }
        }


    }

    public function hasSucceededOrder()
    {
        if($this->latestOrder == null) {
            return false;
        }

        return true;
    }

    public function getLatestOrderIncrementId()
    {
        if($this->latestOrder == null) {
            return null;
        }

        return $this->latestOrder->getIncrementId();
    }
}