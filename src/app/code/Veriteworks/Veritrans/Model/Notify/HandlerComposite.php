<?php
namespace Veriteworks\Veritrans\Model\Notify;

use \Magento\Framework\ObjectManager\TMapFactory;

class HandlerComposite implements HandlerCompositeInterface
{
    /**
     * @var array
     */
    private $handlers = [];

    /**
     * HandlerComposite constructor.
     * @param \Magento\Framework\ObjectManager\TMapFactory $tmapFactory
     * @param array $handlers
     */
    public function __construct(
        TMapFactory $tmapFactory,
        array $handlers
    ) {
        $this->handlers = $tmapFactory->create(
            [
                'array' => $handlers,
                'type' => HandlerInterface::class
            ]
        );
    }

    /**
     * @param $key
     * @return HandlerInterface
     */
    public function getHandler($key)
    {
        return $this->handlers[$key];
    }
}