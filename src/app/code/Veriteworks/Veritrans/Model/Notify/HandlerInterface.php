<?php
namespace Veriteworks\Veritrans\Model\Notify;

interface HandlerInterface
{
    /**
     * @param array $data
     * @param int $suffix
     * @return array
     */
    public function process(array $data, $suffix);
}