<?php
namespace Veriteworks\Veritrans\Model\Notify\Handlers;

use \Veriteworks\Veritrans\Model\Notify\HandlerInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Sales\Model\Order\Email\Sender\OrderSender;
use \Magento\Sales\Model\OrderFactory;
use \Magento\Sales\Model\ResourceModel\Order\Payment as OrderPaymentResource;
use \Magento\Sales\Model\Order;
use \Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use \Psr\Log\LoggerInterface;

abstract class Base implements HandlerInterface
{

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var OrderSender
     */
    protected $orderSender;
    /**
     * @var OrderFactory
     */
    protected $orderFactory;
    /**
     * @var OrderPaymentResource
     */
    protected $orderPaymentResource;
    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\InvoiceSender
     */
    protected $invoiceSender;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Base constructor.
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderSender $orderSender
     * @param OrderFactory $orderFactory
     * @param OrderPaymentResource $orderPaymentResource
     * @param InvoiceSender $invoiceSender
     * @param LoggerInterface $logger
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        OrderPaymentResource $orderPaymentResource,
        InvoiceSender $invoiceSender,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderSender = $orderSender;
        $this->orderFactory = $orderFactory;
        $this->orderPaymentResource = $orderPaymentResource;
        $this->invoiceSender = $invoiceSender;
        $this->logger = $logger;

    }

    protected function updateOrder(
        Order $order,
        array $param
    ) {

        if($order->canInvoice()) {
            $this->createInvoice($order);
        }
    }

    protected function createInvoice(Order $order)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $invoice */
        $invoice = $order->prepareInvoice();
        $invoice->register()
                ->pay();

        $order->addRelatedObject($invoice)
              ->setCustomerNoteNotify(true)
              ->setIsInProcess(true);
        $orderState = Order::STATE_PROCESSING;
        $order->addStatusHistoryComment(
            __('Order was payed successfully.'),
            $orderState
        );
        $invoice->setSendEmail(true);
        $this->orderRepository->save($order);
        $this->invoiceSender->send($invoice);
    }
}