<?php
namespace Veriteworks\Veritrans\Model\Notify\Handlers;

use \Magento\Sales\Model\Order;

class Cvs extends Base
{
    /**
     * @param array $data
     * @param int $suffix
     * @return array
     */
    public function process(array $data, $suffix)
    {
        $orderId = $data['orderId' . $suffix];
        $receiptNo = $data['receiptNo' . $suffix];
        $receiptDate = $data['receiptDate' . $suffix];
        $rcvAmount = $data['rcvAmount' . $suffix];


        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);


        if($order->getState() == Order::STATE_PROCESSING) {
            return;
        }

        $param = [
            'receiptNo' => $receiptNo,
            'receiptDate' => $receiptDate,
        ];

        if((int)$order->getBaseGrandTotal() == $rcvAmount) {
            $this->updateOrder($order, $param);
        }
    }

}