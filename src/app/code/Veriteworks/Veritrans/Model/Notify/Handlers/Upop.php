<?php
namespace Veriteworks\Veritrans\Model\Notify\Handlers;

use Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order;

class Upop extends Base
{
    /**
     * @param array $data
     * @param int $suffix
     * @return array
     * @throws LocalizedException
     */
    public function process(array $data, $suffix)
    {
        $orderId = $data['orderId' . $suffix];
        $txnType = $data['txnType' . $suffix];
        $mStatus = $data['mStatus' . $suffix];
        $traceNumber = $data['traceNumber' . $suffix];
        $traceTitle = $data['traceTitle' . $suffix];
        $settleAmount = $data['settleAmount' . $suffix];
        $settleDate = $data['settleDate' . $suffix];
        $settleRate = $data['settleRate' . $suffix];

        if($txnType != 'Authorize') {
            return;
        }

        if($mStatus == 'failure') {
            throw new LocalizedException(__('Union payment process was failed.'));
        }


        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);


        if($order->getState() == Order::STATE_PROCESSING) {
            return;
        }

        $param = [
            'traceNumber' => $traceNumber,
            'traceTitle' => $traceTitle,
            'settleAmount' => $settleAmount,
            'settleDate' => $settleDate,
            'settleRate' => $settleRate
        ];

        $this->updateOrder($order, $param);

    }

}