<?php
namespace Veriteworks\Veritrans\Model\Notify\Handlers;

use \Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order;

class Alipay extends Base
{
    /**
     * @param array $data
     * @param int $suffix
     * @return array
     * @throws LocalizedException
     */
    public function process(array $data, $suffix)
    {
        $orderId = $data['orderId' . $suffix];
        $txnType = $data['txnType' . $suffix];
        $mStatus = $data['mStatus' . $suffix];
        $vResultCode = $data['vResultCode' . $suffix];

        $resultCode = substr($vResultCode, 0, 4);
        $successCode = array('Y001');

        if($txnType == 'Refund') {
            return;
        }

        if($mStatus == 'failure') {
            throw new LocalizedException(__('Alipay payment process was failed.'));
        }


        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);

        if(in_array($resultCode, $successCode)) {
            if($order->getState() == Order::STATE_PROCESSING) {
                return;
            }

            $this->updateOrder($order, []);

        } else {
            throw new LocalizedException(__('Alipay payment process was failed.'));
        }
    }

}