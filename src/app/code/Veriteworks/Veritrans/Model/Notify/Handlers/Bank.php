<?php
namespace Veriteworks\Veritrans\Model\Notify\Handlers;

use Magento\Framework\Exception\LocalizedException;
use \Magento\Sales\Model\Order;

class Bank extends Base
{
    /**
     * @param array $data
     * @param int $suffix
     * @return array
     * @throws LocalizedException
     */
    public function process(array $data, $suffix)
    {
        $orderId = $data['orderId' . $suffix];
        $kikanNo = $data['kikanNo' . $suffix];
        $kigyoNo = $data['kigyoNo' . $suffix];
        $rcvDate = $data['rcvDate' . $suffix];
        $customerNo = $data['customerNo' . $suffix];
        $confNo = $data['confNo' . $suffix];
        $rcvAmount = $data['rcvAmount' . $suffix];


        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);


        if($order->getState() == Order::STATE_PROCESSING) {
            return;
        }

        $param = [
            'kikanNo' => $kikanNo,
            'kigyoNo' => $kigyoNo,
            'rcvDate' => $rcvDate,
            'customerNo' => $customerNo,
            'confNo'  => $confNo
        ];

        if((int)$order->getBaseGrandTotal() == $rcvAmount) {
            $this->updateOrder($order, $param);
        }
    }

}