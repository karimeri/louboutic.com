<?php
namespace Veriteworks\Veritrans\Model\Notify;

interface HandlerCompositeInterface
{
    public function getHandler($key);
}