<?php
namespace Veriteworks\Veritrans\Model\Notify;

use \Magento\Framework\App\RequestInterface;
use \Magento\Sales\Model\OrderFactory;
use Magento\Ui\Controller\Adminhtml\Index\Render\Handle;

class Processor
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    private $handlerComposite;

    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        HandlerCompositeInterface $handlerComposite,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->orderFactory = $orderFactory;
        $this->coreRegistry = $coreRegistry;
        $this->handlerComposite = $handlerComposite;
        $this->logger = $logger;
    }

    public function process(RequestInterface $request)
    {
        $num = $request->getParam('numberOfNotify');
        for ($i = 0; $i < $num; $i++) {
            $mStatus = '';
            $amount = 0;
            $suffix = sprintf("%04d", $i);
            $orderId = $request->getParam('orderId' . $suffix);

            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->orderFactory->create()->loadByIncrementId($orderId);
            $payment = $order->getPayment();

            $code = $payment->getMethod();

            /** @var HandlerInterface $handler */
            $handler = $this->handlerComposite->getHandler($code);
            $handler->process($request->getParams(), $suffix);

        }
    }
}