<?php
namespace Veriteworks\Veritrans\Model\Source;

/**
 * Class PayLimit
 * @package Veriteworks\Veritrans\Model\Source
 */
class PayLimit {
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $data = array();
        for($i =1; $i <= 60; $i++) {
            $data[] = [
                'value' => $i,
                'label' => $i . __('days after')
            ];

        }
        return $data;
    }


}
