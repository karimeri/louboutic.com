<?php
namespace Veriteworks\Veritrans\Model\Source;

class Paymenttype {

    /**
     * @return array
     */
    public function toOptionArray() {
        return array(
            array('value' => '10','label' => __('One Time')),
            array('value' => '61','label' => __('Split')),
            array('value' => '21','label' => __('Bonus')),
            array('value' => '80','label' => __('Rebo')),
        );
    }

    /**
     * @param $type
     * @return string
     */
    public function getPayType($type) {
        foreach($this->toOptionArray() as $data) {
            if($data['value'] == $type) {
                return $data['label'];
            }
        }

        return '';
    }
}
