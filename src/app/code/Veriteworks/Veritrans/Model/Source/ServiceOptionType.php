<?php
namespace Veriteworks\Veritrans\Model\Source;

class ServiceOptionType
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'mpi-complete',
                'label' => __("Don't allow non-3D secure card")
            ],
            [
                'value' => 'mpi-company',
                'label' => __('Allow non-3D secure card as processor risk')
            ],
            [
                'value' => 'mpi-merchant',
                'label' => __('Allow non-3D secure card as merchant risk')
            ]
        ];
    }
}
