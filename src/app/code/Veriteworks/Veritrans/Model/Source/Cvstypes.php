<?php
namespace Veriteworks\Veritrans\Model\Source;

/**
 * Class Cvstypes
 * @package Veriteworks\Veritrans\Model\Source
 */
class Cvstypes {
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'SE', 'label' => __('SevenEleven')],
            ['value' => 'LA', 'label' => __('Lawson')],
            ['value' => 'FM', 'label' => __('FamilyMart')],
            ['value' => 'SU', 'label' => __('Sunkus')],
            ['value' => 'CK', 'label' => __('CircleK')],
            ['value' => 'MS', 'label' => __('MiniStop')],
            ['value' => 'DY', 'label' => __('DailyYamazaki')],
            ['value' => 'SM', 'label' => __('Seico-Mart')]
                ];
    }

    /**
     * @param $cvsType
     * @return \Magento\Framework\Phrase|string
     */
    public function getCvsType($cvsType)
    {
        $code = "";
        switch ($cvsType) {
            case 'SE' :
                $code = __('SevenEleven');
                break;
            case 'LA' :
                $code = __('Lawson');
                break;
            case 'FM' :
                $code = __('FamilyMart');
                break;
            case 'SU' :
                $code = __('Sunkus');
                break;
            case 'CK' :
                $code = __('CircleK');
                break;
            case 'MS' :
                $code = __('MiniStop');
                break;
            case 'DY' :
                $code = __('DailyYamazaki');
                break;
            case 'SM' :
                $code = __('Seico-Mart');
                break;
        }

        return $code;
    }
}
