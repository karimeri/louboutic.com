<?php
namespace Veriteworks\Veritrans\Model\Source;

class Cctypes {
    /**
     * @return array
     */
    public function toOptionArray() {
        return array(
                array('value' => 'VI','label' => __('Visa')),
                array('value' => 'MC','label' => __('Master Card')),
                array('value' => 'JCB','label' => __('JCB')),
                array('value' => 'AE','label' => __('American Express')),
                array('value' => 'DN','label' => __('Diners')),
                );
    }
}
