<?php
namespace Veriteworks\Veritrans\Model\Source;

/**
 * Class Banktypes
 * @package Veriteworks\Veritrans\Model\Source
 */
class Banktypes {
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'atm', 'label' => __('ATM')],
            ['value' => 'netbank-pc', 'label' => __('NetBank')],
            ['value' => 'netbank-docomo', 'label' => __('NetBank Docomo')],
            ['value' => 'netbank-softbank', 'label' => __('NetBank Softbank')],
            ['value' => 'netbank-au', 'label' => __('NetBank au')],
        ];
    }

    public function getBankType($cvsType)
    {
        $code = "";
        switch ($cvsType) {
            case 'atm' :
                $code = __('ATM');
                break;
            case 'netbank-pc' :
                $code = __('NetBank');
                break;
            case 'netbank-docomo' :
                $code = __('NetBank Docomo');
                break;
            case 'netbank-softbank' :
                $code = __('NetBank Softbank');
                break;
            case 'netbank-au' :
                $code = __('NetBank au');
                break;
        }

        return $code;
    }
}
