<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Veriteworks\Veritrans\Model\Source;

class Authmode
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
                array(
                    'value' => 'Authorize/card',
                    'label' => __('No')
                    ),
                array(
                    'value' => 'Authorize/mpi',
                    'label' => __('Yes')
                    ),
                );
    }
}
