<?php
namespace Veriteworks\Veritrans\Model\Config;

use Magento\Payment\Model\CcGenericConfigProvider;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Payment\Model\CcConfig;
use Veriteworks\Veritrans\Gateway\Config\Cc;

/**
 * Class ReProvider
 * @package Veriteworks\Veritrans\Model\Config
 */
class ReProvider extends CcProvider
{
    const CODE = 'veritrans_re';


    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $config = parent::getConfig();
        $config = array_merge_recursive($config, [
            'payment' => [
                self::CODE => [
                    'payment_type' => $this->getAvailablePaymentType(),
                    'can_use_split' => $this->canUseSplit(),
                    'split_count' => $this->getAvailableSplitCount()
                ]
            ]
        ]);

        return $config;
    }



}