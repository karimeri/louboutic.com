<?php
namespace Veriteworks\Veritrans\Model\Config;

use Magento\Payment\Model\CcGenericConfigProvider;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Payment\Model\CcConfig;

class CcMultiProvider extends CcGenericConfigProvider
{
    const CODE = 'veritrans_ccmulti';

    /**
     * ConfigProvider constructor.
     * @param Magento\Payment\Model\CcConfig; $ccConfig
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Veriteworks\Veritrans\Helper\Data $helper
     */
    public function __construct(
        CcConfig $ccConfig,
        PaymentHelper $paymentHelper,
        \Veriteworks\Veritrans\Helper\Data $helper
    ){
        parent::__construct($ccConfig, $paymentHelper);
        $this->config = $helper;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $config = parent::getConfig();
        $config = array_merge_recursive($config, [
            'payment' => [
                self::CODE => [
                    'can_use_token' => $this->canUseToken(),
                    'token_key' => $this->getToken()
                ]
            ]
        ]);

        return $config;
    }

    /**
     * @return bool
     */
    private function canUseToken()
    {
        $token = $this->config->getMcToken();

        return ($token !== '') ? true : false;
    }

    /**
     * @return mixed
     */
    private function getToken()
    {
        return $this->config->getMcToken();
    }
}