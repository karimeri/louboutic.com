<?php
namespace Veriteworks\Veritrans\Model\Config;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;

class CvsProvider implements ConfigProviderInterface
{
    const CODE = 'veritrans_cvs';

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * @var \Veriteworks\Veritrans\Model\Source\Cvstypes
     */
    protected $cvstypes;


    /**
     * CvsProvider constructor.
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Veriteworks\Veritrans\Model\Source\Cvstypes $cvstypes
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        \Veriteworks\Veritrans\Model\Source\Cvstypes $cvstypes
    ) {
        $this->method = $paymentHelper->getMethodInstance(self::CODE);
        $this->cvstypes = $cvstypes;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];

        if ($this->method->isAvailable()) {
            $config = array_merge_recursive($config, [
                'payment' => [
                    'veritrans_cvs' => [
                        'availableTypes' => $this->getCvsAvailableTypes(),
                    ]
                ]
            ]);
        }

        return $config;
    }

    /**
     * Retrieve availables cvs types
     *
     * @param string $methodCode
     * @return array
     */
    protected function getCvsAvailableTypes()
    {
        $keys   = $this->cvstypes->toOptionArray();
        $availableTypes = $this->method->getConfigData('cvstypes');
        $configData = [];

        if ($availableTypes) {
            $availableTypes = explode(',', $availableTypes);
        }

        foreach ($keys as $entry) {
            if (in_array($entry["value"], $availableTypes)) {
                $configData[$entry["value"]] = $entry["label"];
            }
        }

        return $configData;
    }
}