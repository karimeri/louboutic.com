<?php
namespace Veriteworks\Veritrans\Model\Config;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;

class BankProvider implements ConfigProviderInterface
{
    const CODE = 'veritrans_bank';

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * @var \Veriteworks\Veritrans\Model\Source\Banktypes
     */
    protected $banktypes;


    /**
     * BankProvider constructor.
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Veriteworks\Veritrans\Model\Source\Banktypes $banktypes
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        \Veriteworks\Veritrans\Model\Source\Banktypes $banktypes
    ) {
        $this->method = $paymentHelper->getMethodInstance(self::CODE);
        $this->banktypes = $banktypes;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];

        if ($this->method->isAvailable()) {
            $config = array_merge_recursive($config, [
                'payment' => [
                    'veritrans_bank' => [
                        'availableTypes' => $this->getBankAvailableTypes(),
                    ]
                ]
            ]);
        }

        return $config;
    }

    /**
     * Retrieve availables bank types
     *
     * @param string $methodCode
     * @return array
     */
    protected function getBankAvailableTypes()
    {
        $keys   = $this->banktypes->toOptionArray();
        $availableTypes = $this->method->getConfigData('banktypes');
        $configData = [];

        if ($availableTypes) {
            $availableTypes = explode(',', $availableTypes);
        }

        foreach ($keys as $entry) {
            if (in_array($entry["value"], $availableTypes)) {
                $configData[$entry["value"]] = $entry["label"];
            }
        }

        return $configData;
    }
}