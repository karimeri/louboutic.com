<?php
namespace Veriteworks\Veritrans\Model\Config;

use Magento\Payment\Model\CcGenericConfigProvider;
use Magento\Payment\Helper\Data as PaymentHelper;
use \Veriteworks\Veritrans\Helper\Data;
use Magento\Payment\Model\CcConfig;
use \Veriteworks\Veritrans\Model\Source\Splittype;
use \Veriteworks\Veritrans\Model\Source\Paymenttype;
use \Veriteworks\Veritrans\Model\Reauth;

/**
 * Class CcProvider
 * @package Veriteworks\Veritrans\Model\Config
 */
class CcProvider extends CcGenericConfigProvider
{
    const CODE = 'veritrans_cc';

    /**
     * @var \Veriteworks\Veritrans\Model\Source\Paymenttype
     */
    private $paymenttype;

    /**
     * @var \Veriteworks\Veritrans\Model\Source\Splittype
     */
    private $splitCount;

    /**
     * @var \Veriteworks\Veritrans\Helper\Data
     */
    private $config;

    private $reauth;


    /**
     * CcProvider constructor.
     * @param CcConfig $ccConfig
     * @param PaymentHelper $paymentHelper
     * @param Data $helper
     * @param Paymenttype $paymenttype
     * @param Splittype $splitcount
     * @param Reauth $reauth
     */
    public function __construct(
        CcConfig $ccConfig,
        PaymentHelper $paymentHelper,
        Data $helper,
        Paymenttype $paymenttype,
        Splittype $splitcount,
        Reauth $reauth
    ){
        parent::__construct($ccConfig, $paymentHelper);
        $this->config = $helper;
        $this->paymenttype = $paymenttype;
        $this->splitCount = $splitcount;
        $this->reauth = $reauth;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $config = parent::getConfig();
        $config = array_merge_recursive($config, [
            'payment' => [
                self::CODE => [
                    'payment_type' => $this->getAvailablePaymentType(),
                    'can_use_split' => $this->canUseSplit(),
                    'split_count' => $this->getAvailableSplitCount(),
                    'can_use_token' => $this->canUseToken(),
                    'token_key' => $this->getToken(),
                    'increment_id' => $this->reauth->handle([])
                ]
            ]
        ]);

        return $config;
    }

    /**
     * Retrieve availables credit card types
     *
     * @param string $methodCode
     * @return array
     */
    protected function getAvailablePaymentType()
    {
        $keys   = $this->paymenttype->toOptionArray();
        $availableTypes = $this->config->getPaymentType();
        $configData = [];

        if ($availableTypes) {
            $availableTypes = explode(',', $availableTypes);
}

        foreach ($keys as $entry) {
            if (in_array($entry["value"], $availableTypes)) {
                $configData[$entry["value"]] = $entry["label"];
            }
        }

        return $configData;
    }

    /**
     * @return array
     */
    protected function getAvailableSplitCount()
    {
        $keys   = $this->splitCount->toOptionArray();
        $availableTypes = $this->config->getSplitCount();
        $configData = [];

        if ($availableTypes) {
            $availableTypes = explode(',', $availableTypes);
        }

        foreach ($keys as $entry) {
            if (in_array($entry["value"], $availableTypes)) {
                $configData[$entry["value"]] = $entry["label"];
            }
        }

        return $configData;
    }

    /**
     * @return bool
     */
    protected function canUseSplit()
    {
        $_config = explode(",", $this->config->getPaymentType());

        if(count($_config) == 1 && in_array('1', $_config)){
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function canUseToken()
    {
        $token = $this->config->getToken();

        return ($token !== '') ? true : false;
    }

    /**
     * @return mixed
     */
    private function getToken()
    {
        return $this->config->getToken();
    }

}