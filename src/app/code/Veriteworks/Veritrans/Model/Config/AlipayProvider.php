<?php
namespace Veriteworks\Veritrans\Model\Config;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;

class AlipayProvider implements ConfigProviderInterface
{
    const CODE = 'veritrans_alipay';

    /**
     * @var \Magento\Payment\Model\MethodInterface
     */
    protected $method;

    /**
     * CvsProvider constructor.
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(
        PaymentHelper $paymentHelper
    ) {
        $this->method = $paymentHelper->getMethodInstance(self::CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];

        if ($this->method->isAvailable()) {
            $config = array_merge_recursive($config, [
                'payment' => [
                    'veritrans_alipay' => []
                ]
            ]);
        }

        return $config;
    }
}