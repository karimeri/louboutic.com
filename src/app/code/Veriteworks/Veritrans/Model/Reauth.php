<?php
namespace Veriteworks\Veritrans\Model;

use \Magento\Payment\Gateway\Config\ValueHandlerInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Quote\Model\Quote;
use \Magento\Backend\Model\Session\Quote as QuoteSession;

/**
 * Class Reauth
 * @package Veriteworks\Veritrans\Model
 */
class Reauth  implements ValueHandlerInterface
{
    /**
     * @var null | \Magento\Sales\Model\Order
     */
    private $latestOrder;

    /**
     * @var string
     */
    private $method;

    /**
     * @var Quote
     */
    private $quote;

    /**
     * @var QuoteSession
     */
    private $quoteSession;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var CurrentCustomer
     */
    private $currentCustomer;

    /**
     * @var array
     */
    private $status = ['processing','complete','closed','canceled'];

    /**
     * constructor.
     * @param Quote $currentQuote
     * @param QuoteSession $quoteSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param CollectionFactory $orderCollectionFactory
     * @param string $method
     */
    public function __construct(
        Quote $currentQuote,
        QuoteSession $quoteSession,
        CurrentCustomer $currentCustomer,
        CustomerRepositoryInterface $customerRepository,
        CollectionFactory $orderCollectionFactory,
        $method = 'veritrans_cc'
    ) {
        $this->method = $method;
        $this->quote = $currentQuote;
        $this->currentCustomer = $currentCustomer;
        $this->quoteSession = $quoteSession;
        $this->customerRepository = $customerRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * @param array $subject
     * @param null $storeId
     * @return null|string
     */
    public function handle(array $subject, $storeId = null)
    {
        $customerId = null;

        if($this->currentCustomer->getCustomerId()) {
            $customerId = $this->currentCustomer->getCustomerId();
        } else {
            $customerId = $this->quoteSession->getCustomerId();
        }



        if(is_null($customerId)) {
            $this->latestOrder = null;
        } else {
            $date = new \Zend_Date();
            $date->sub('1', \Zend_Date::YEAR);

            $collection = $this->orderCollectionFactory->create($customerId);
            $collection->join('sales_order_payment',
                'main_table.entity_id=parent_id')
                ->addAttributeToFilter('method', $this->method)
                ->addAttributeToFilter('created_at',
                    array('gteq' => $date->toString('Y-M-d hh:mm:ss')))
                ->addAttributeToFilter('status', ['in' => $this->status])
                ->addOrder('created_at', 'desc');

            if(count($collection) > 0) {
                /** @var \Magento\Sales\Model\Order latestOrder */
                $this->latestOrder = $collection->getFirstItem();
            } else {
                $this->latestOrder = null;
            }
        }

        if($this->latestOrder == null) {
            return null;
        }

        return $this->latestOrder->getIncrementId();
    }

}