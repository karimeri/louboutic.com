/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/payment/additional-validators'
    ],
    function ($, Component, placeOrderAction, fullScreenLoader, additionalValidators) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Veriteworks_Veritrans/payment/bank'
            },

            initObservable: function () {
                this._super()
                    .observe([
                        'bankType'
                    ]);

                return this;
            },

            /**
             * Init component
             */
            initialize: function () {
                var self = this;

                this._super();

            },

            getCode: function() {
                return 'veritrans_bank';
            },

            /**
             * Get payment method data
             */
            getData: function () {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'bank_type': this.bankType()
                    }
                };
            },

            getBankTypes: function() {
                return window.checkoutConfig.payment.veritrans_bank.availableTypes;
            },

            /**
             * Get list of available month values
             * @returns {Object}
             */
            getBankValues: function () {
                return _.map(this.getBankTypes(), function (value, key) {
                    return {
                        'value': key,
                        'bank': value
                    };
                });
            },

            isActive: function() {
                return true;
            },

            validate: function() {
                var $form = $('#' + this.getCode() + '-form');
                return $form.validation() && $form.validation('isValid');
            },


        });
    }
);
