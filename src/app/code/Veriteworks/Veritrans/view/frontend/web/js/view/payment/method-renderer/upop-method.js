/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/payment/additional-validators',
        'mage/url'
    ],
    function ($, Component, placeOrderAction, fullScreenLoader, additionalValidators, url) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Veriteworks_Veritrans/payment/upop'
            },

            getCode: function() {
                return 'veritrans_upop';
            },

            /**
             * Init component
             */
            initialize: function () {
                var self = this;

                this._super();
                return this;
            },

            /**
             * Get data
             * @returns {Object}
             */
            getData: function () {
                return {
                    'method': this.item.method
                };
            },

            isActive: function() {
                return true;
            },

            placeOrder: function (data, event) {
                var self = this;

                if (event) {
                    event.preventDefault();
                }

                if (this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);

                    this.getPlaceOrderDeferredObject()
                        .fail(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                            }
                        ).done(
                        function () {
                            if (self.redirectAfterPlaceOrder) {
                                window.location.replace(url.build('veritrans/upop/send/'));
                            }
                        }
                    );

                    return true;
                }

                return false;
            },

            validate: function() {
                var $form = $('#' + this.getCode() + '-form');
                return $form.validation() && $form.validation('isValid');
            },


        });
    }
);
