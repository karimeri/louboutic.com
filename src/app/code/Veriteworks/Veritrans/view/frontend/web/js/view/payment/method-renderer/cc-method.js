/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Payment/js/view/payment/cc-form',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'mage/url',
        'mage/translate',
        'Magento_Ui/js/modal/alert'
    ],
    function ($, Component, placeOrderAction, fullScreenLoader, additionalValidators, ccValidator, url, $t, alert) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Veriteworks_Veritrans/payment/cc'
            },

            initObservable: function () {
                this._super().observe([
                        'creditCardType',
                        'creditCardExpYear',
                        'creditCardExpMonth',
                        'creditCardNumber',
                        'creditCardVerificationNumber',
                        'paymentType',
                        'splitCount',
                        'creditCardToken',
                        'useLastOrder'
                    ]);

                return this;
            },

            getCode: function() {
                return 'veritrans_cc';
            },

            /**
             * Get data
             * @returns {Object}
             */
            getData: function () {
                var additional;

                if(this.useLastOrder()) {
                    additional = {
                        'payment_type': this.paymentType(),
                        'split_count': this.splitCount(),
                        'increment_id': this.canUseLastOrder()
                    };
                } else if(this.canUseToken()){
                    additional = {
                        'payment_type': this.paymentType(),
                        'split_count': this.splitCount(),
                        'token': jQuery('#veritrans_cc_token').val()
                    };
                } else {
                    additional = {
                        'cc_cid': this.creditCardVerificationNumber(),
                        'cc_type': this.creditCardType(),
                        'cc_exp_year': this.creditCardExpYear(),
                        'cc_exp_month': this.creditCardExpMonth(),
                        'cc_number': this.creditCardNumber(),
                        'payment_type': this.paymentType(),
                        'split_count': this.splitCount()
                    };
                }

                return {
                    'method': this.item.method,
                    'additional_data': additional
                };
            },

            canUseSplit: function() {
                return window.checkoutConfig.payment.veritrans_cc.can_use_split;
            },

            getPaymentTypes: function() {
                return window.checkoutConfig.payment.veritrans_cc.payment_type;
            },

            canUseLastOrder: function () {
                return window.checkoutConfig.payment.veritrans_cc.increment_id;
            },
            
            canUseToken: function() {
                return window.checkoutConfig.payment.veritrans_cc.can_use_token;
            },
            
            getToken: function () {
                return window.checkoutConfig.payment.veritrans_cc.token_key;
            },

            getPaymentTypeValues: function () {
                return _.map(this.getPaymentTypes(), function (value, key) {
                    return {
                        'value': key,
                        'type': value
                    };
                });
            },

            getSplitCount: function() {
                return window.checkoutConfig.payment.veritrans_cc.split_count;
            },

            isSplit: function() {
                if(this.paymentType() == '61'){
                    return true;
                }

                return false;
            },

            getSplitCountValues: function () {
                return _.map(this.getSplitCount(), function (value, key) {
                    return {
                        'value': key,
                        'count': value
                    };
                });
            },


            isActive: function() {
                return true;
            },

            placeOrder: function (data, event) {
                var self = this;

                if (event) {
                    event.preventDefault();
                }

                if(this.useLastOrder()) {
                    this.getPlaceOrderDeferredObject()
                        .fail(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                            }
                        ).done(
                        function () {
                            if (self.redirectAfterPlaceOrder) {
                                window.location.replace(url.build('veritrans/mpi/send/'));
                            }
                        }
                    );
                } else if(this.canUseToken()) {
                    var param =
                        {
                            "card_number": this.creditCardNumber(),
                            "card_expire": (("00"+this.creditCardExpMonth()).slice(-2)+ '/' + this.creditCardExpYear().slice(-2)),
                            "security_code": this.creditCardVerificationNumber().replace(/[^\d]/g, ""),
                            "token_api_key": this.getToken(),
                            "lang": 'ja'
                        };

                    $.ajax({
                        type: 'POST',
                        contentType : 'application/json; charset=utf-8',
                        url: 'https://api.veritrans.co.jp/4gtoken',
                        accept: 'application/json',
                        data: JSON.stringify(param),
                        cache: false,

                        success: function (json) {
                            var data = eval(json);

                            if(data.status == 'success') {
                                jQuery('#veritrans_cc_cc_number').val('');
                                jQuery('#veritrans_cc_expiration').prop('selectedIndex', 0);
                                jQuery('#veritrans_cc_expiration_yr').prop('selectedIndex', 0);
                                jQuery('#veritrans_cc_cc_cid').val('');
                                jQuery('#veritrans_cc_token').val(data.token);

                                self.getPlaceOrderDeferredObject().fail(
                                    function () {
                                        self.isPlaceOrderActionAllowed(true);
                                    }
                                ).done(
                                    function () {
                                        if (self.redirectAfterPlaceOrder) {
                                            window.location.replace(url.build('veritrans/mpi/send/'));
                                        }
                                    });

                            } else {
                                alert({content: $t("Please confirm your credit card information.")});
                                self.isPlaceOrderActionAllowed(true);
                            }
                        },
                        error: function (json) {
                            alert({content: $t("Please confirm your credit card information.")});
                        }
                    });
                } else {
                    this.getPlaceOrderDeferredObject()
                        .fail(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                            }
                        ).done(
                        function () {
                            if (self.redirectAfterPlaceOrder) {
                                window.location.replace(url.build('veritrans/mpi/send/'));
                            }
                        }
                    );
                }

                return false;
            },

            validate: function() {
                var $form = $('#' + this.getCode() + '-form');
                return $form.validation() && $form.validation('isValid');
            },


        });
    }
);
