/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'veritrans_cc',
                component: 'Veriteworks_Veritrans/js/view/payment/method-renderer/cc-method'
            }

        );
        rendererList.push(
            {
                type: 'veritrans_ccmulti',
                component: 'Veriteworks_Veritrans/js/view/payment/method-renderer/ccmulti-method'
            }

        );
        rendererList.push(
            {
                type: 'veritrans_cvs',
                component: 'Veriteworks_Veritrans/js/view/payment/method-renderer/cvs-method'
            }

        );
        rendererList.push(
            {
                type: 'veritrans_bank',
                component: 'Veriteworks_Veritrans/js/view/payment/method-renderer/bank-method'
            }

        );
        rendererList.push(
            {
                type: 'veritrans_upop',
                component: 'Veriteworks_Veritrans/js/view/payment/method-renderer/upop-method'
            }

        );
        rendererList.push(
            {
                type: 'veritrans_alipay',
                component: 'Veriteworks_Veritrans/js/view/payment/method-renderer/alipay-method'
            }

        );

        /** Add view logic here if needed */
        return Component.extend({});
    }
);