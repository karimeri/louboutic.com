<?php
namespace Veriteworks\Veritrans\Block\Info;

use \Magento\Framework\View\Element\Template\Context;
use \Veriteworks\Veritrans\Model\Source\Cvstypes;

/**
 * Class Cvs
 * @package Veriteworks\Veritrans\Block\Info
 */
class Cvs extends \Magento\Payment\Block\Info
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::info/cvs.phtml';

    /**
     * @var \Veriteworks\Veritrans\Model\Source\Cvstypes
     */
    protected $_cvsTypes;

    /**
     * Cvs constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Veriteworks\Veritrans\Model\Source\Cvstypes $cvstypes
     * @param array $data
     */
    public function __construct(
                                Context $context,
                                Cvstypes $cvstypes,
                                array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_cvsTypes = $cvstypes;
    }

    /**
     * @param null $transport
     * @return \Magento\Framework\DataObject
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $transport = parent::_prepareSpecificInformation($transport);
        $additional = $this->getInfo()->getAdditionalInformation();
        $data = array();

        if(is_array($additional)) {
            if (array_key_exists('cvs_type', $additional)) {
                $source = $this->_cvsTypes;
                $data[__('CVS name')->__toString()] =
                    $source->getCvsType($additional['cvs_type']);
            }

            if (array_key_exists('receiptNo', $additional)) {
                $data[__('Receipt Number')->__toString()] = $additional['receiptNo'];
            }

            if (array_key_exists('haraikomiUrl', $additional)) {
                $label = __('Payment URL')->__toString();
                $data[$label] = __('<a href="%1" target="_blank">Go to payment page</a>',
                    $additional['haraikomiUrl']);
            }

            if (array_key_exists('PaymentTerm', $additional)) {
                $paydate = $additional['PaymentTerm'];
                $data[__('Payment limit date')->__toString()] =
                    preg_replace('/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/',
                                 '$1/$2/$3 $4:$5:$6', $paydate);
            }

        }

        return $transport->setData(array_merge($transport->getData(), $data));
    }

}