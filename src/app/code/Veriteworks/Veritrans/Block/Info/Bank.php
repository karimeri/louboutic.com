<?php
namespace Veriteworks\Veritrans\Block\Info;

use \Magento\Framework\View\Element\Template\Context;
use \Veriteworks\Veritrans\Model\Source\Banktypes;

/**
 * Class Bank
 * @package Veriteworks\Veritrans\Block\Info
 */
class Bank extends \Magento\Payment\Block\Info
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::info/bank.phtml';

    /**
     * @var \Veriteworks\Veritrans\Model\Source\Banktypes
     */
    private $bankTypes;

    /**
     * Bank constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Veriteworks\Veritrans\Model\Source\Banktypes $banktypes
     * @param array $data
     */
    public function __construct(
                                Context $context,
                                Banktypes $banktypes,
                                array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->bankTypes = $banktypes;
    }

    /**
     * @param null $transport
     * @return \Magento\Framework\DataObject
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $transport = parent::_prepareSpecificInformation($transport);
        $additional = $this->getInfo()->getAdditionalInformation();
        $data = array();

        if(is_array($additional)) {
            if (array_key_exists('shunoKikanNo', $additional)) {
                $data[__('Bank Code')->__toString()] = $additional['shunoKikanNo'];
            }

            if (array_key_exists('confirmNo', $additional)) {
                $data[__('Confirm Number')->__toString()] = $additional['confirmNo'];
            }

            if (array_key_exists('billPattern', $additional)) {
                $data[__('Bill Pattern')->__toString()] = $additional['billPattern'];
            }

            if (array_key_exists('url', $additional)) {
                $label = __('Payment URL')->__toString();
                $data[$label] = __('<a href="%1" target="_blank">Go to payment page</a>',
                    $additional['url']);
            }

            if (array_key_exists('PaymentTerm', $additional)) {
                $paydate = $additional['PaymentTerm'];
                $data[__('Payment limit date')->__toString()] =
                    preg_replace('/^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/',
                                 '$1/$2/$3 $4:$5:$6', $paydate);
            }

        }

        return $transport->setData(array_merge($transport->getData(), $data));
    }

}