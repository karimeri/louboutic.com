<?php
namespace Veriteworks\Veritrans\Block\Info;

use \Magento\Framework\View\Element\Template\Context;

/**
 * Class Alipay
 * @package Veriteworks\Veritrans\Block\Info
 */
class Alipay extends \Magento\Payment\Block\Info
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::info/alipay.phtml';

}