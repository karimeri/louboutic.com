<?php
namespace Veriteworks\Veritrans\Block\Info;

use \Magento\Framework\View\Element\Template\Context;

/**
 * Class Upop
 * @package Veriteworks\Veritrans\Block\Info
 */
class Upop extends \Magento\Payment\Block\Info
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::info/upop.phtml';

}