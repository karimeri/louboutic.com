<?php
namespace Veriteworks\Veritrans\Block\Form;

class Bank extends \Magento\Payment\Block\Form
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::form/bank.phtml';
}