<?php
namespace Veriteworks\Veritrans\Block\Form;

class Upop extends \Magento\Payment\Block\Form
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::form/upop.phtml';
}