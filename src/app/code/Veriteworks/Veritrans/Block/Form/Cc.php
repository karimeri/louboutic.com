<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@principle-works.jp so we can send you a copy immediately.
 *
 * @category   payment
 * @package    Veriteworks_Veritrans
 * @copyright  Copyright (c) 2016 Veriteworks Inc. (http://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Veriteworks\Veritrans\Block\Form;

use \Magento\Framework\View\Element\Template\Context;
use \Magento\Payment\Model\Config;
use \Veriteworks\Veritrans\Model\Reauth;

class Cc extends \Magento\Payment\Block\Form\Cc
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::form/cc.phtml';
    /**
     * @var string
     */
    protected $_path = 'payment/verite_veritrans/';

    /**
     * @var Reauth
     */
    private $reauth;


    /**
     * Cc constructor.
     * @param Context $context
     * @param Config $paymentConfig
     * @param Reauth $reauth
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Payment\Model\Config $paymentConfig,
        Reauth $reauth,
        array $data = []
    ) {
        parent::__construct($context, $paymentConfig, $data);
        $this->reauth = $reauth;
    }

    /**
     * @return mixed|null|string
     */
    public function getLatestIncrementId()
    {
        return $this->reauth->handle([]);
    }
}