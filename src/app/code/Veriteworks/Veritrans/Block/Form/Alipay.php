<?php
namespace Veriteworks\Veritrans\Block\Form;

class Alipay extends \Magento\Payment\Block\Form
{
    /**
     * @var string
     */
    protected $_template = 'Veriteworks_Veritrans::form/alipay.phtml';
}