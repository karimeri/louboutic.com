<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Controller\Upop;

use \Magento\Framework\App\Action\Action;

/**
 * Class Send
 * @package Veriteworks\Veritrans\Controller\Upop
 */
class Send extends \Magento\Framework\App\Action\Action {
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_session;

    /**
     * @var DataObjectFactory
     */
    protected $_dataObjectFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;


    /**
     * AbstractRemise constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);
        $this->_orderRepository = $orderRepository;
        $this->_scopeConfig = $scopeConfig;
        $this->_session = $checkoutSession;
        $this->_dataObjectFactory = $dataObjectFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_logger = $logger;
    }

    /**
     * redirect action
     */
    public function execute()
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->_orderRepository->get($this->_session->getLastOrderId());
        $this->_logger->debug('LastOrderId ' .$this->_session->getLastOrderId());
        $method = $order->getPayment()->getMethod();

        //check this transaction is 3D secure or not.
        if ($html = $this->_session->getContent()) {
            $this->_view->loadLayout();
            $layout = $this->_view->getLayout();

            $block = $layout->getBlock('veritrans_redirect');
            $this->_logger->debug(gettype($block));
            $this->_logger->debug($this->_request->getFullActionName());

            /** @var \Magento\Framework\DataObject $data */
            $data  = $this->_dataObjectFactory->create();

            $data->setContent($html);
            $this->_logger->debug($method);

            $this->_logger->debug(var_export($data->toArray(), true));
            $block->setUpopData($data);

            $this->_view->renderLayout();
        } else {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('checkout/onepage/success');
        }
    }
}