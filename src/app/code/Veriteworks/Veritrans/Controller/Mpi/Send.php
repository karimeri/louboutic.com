<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Controller\Mpi;

use \Magento\Framework\App\Action\Action;

/**
 * Class Send
 * @package Veriteworks\Veritrans\Controller\Mpi
 */
class Send extends \Magento\Framework\App\Action\Action {
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * @var DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;


    /**
     * AbstractRemise constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);
        $this->orderRepository = $orderRepository;
        $this->scopeConfig = $scopeConfig;
        $this->session = $checkoutSession;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->coreRegistry = $coreRegistry;
        $this->logger = $logger;
    }

    /**
     * redirect action
     */
    public function execute()
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderRepository->get($this->session->getLastOrderId());
        $this->logger->debug('LastOrderId ' .$this->session->getLastOrderId());
        $method = $order->getPayment()->getMethod();

        //check this transaction is 3D secure or not.
        if ($html = $this->session->getContent()) {
            $this->_view->loadLayout();
            $layout = $this->_view->getLayout();

            $block = $layout->getBlock('veritrans_redirect');
            $this->logger->debug(gettype($block));
            $this->logger->debug($this->_request->getFullActionName());

            /** @var \Magento\Framework\DataObject $data */
            $data  = $this->dataObjectFactory->create();

            $data->setContent($html);
            $this->logger->debug($method);

            $this->logger->debug(var_export($data->toArray(), true));
            $block->setMpiData($data);
            $this->session->setContent(null);
            $this->_view->renderLayout();
        } else {
            /** @var \Magento\Payment\Model\Method\Adapter $method */
            $method = $order->getPayment()->getMethodInstance();
            $this->coreRegistry->register('isSecureArea', true, true);

            $mode = $method->getConfigData('payment_action');

            if($mode == 'authorize_capture') {
                $invoices = $order->getInvoiceCollection();
                foreach($invoices as $invoice) {
                    $invoice->setBillingAddressId($order->getBillingAddressId());
                    $invoice->setShippingAddressId($order->getShippingAddressId());
                    $invoice->pay();
                    $invoice->save();
                }
            }

            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('checkout/onepage/success');
        }
    }
}