<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Controller\Alipay;

use \Magento\Framework\App\Action\Action;
use \Magento\Sales\Model\Order\Payment\Transaction;
use \Magento\Sales\Model\ResourceModel\Order\Payment as OrderPaymentResource;
use \Magento\Sales\Model\Order;

/**
 * Class Receive
 * @package Veriteworks\Veritrans\Controller\Alipay
 */
class Receive extends Action {
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;

    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    private $orderSender;

    /**
     * @var OrderPaymentResource
     */
    private $orderPaymentResource;


    /**
     * Receive constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param OrderPaymentResource $orderPaymentResource
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        OrderPaymentResource $orderPaymentResource,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
        $this->session = $checkoutSession;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->orderRepository = $orderRepository;
        $this->coreRegistry = $coreRegistry;
        $this->orderSender = $orderSender;
        $this->orderFactory = $orderFactory;
        $this->logger = $logger;
        $this->orderPaymentResource = $orderPaymentResource;
    }

    /**
     * redirect action
     */
    public function execute()
    {
        $orderId     = $this->getRequest()->getParam('orderId', null);
        $vResultCode = $this->getRequest()->getParam('vResultCode', null);


        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);
        $payment = $order->getPayment();
        $this->logger->debug(var_export($this->getRequest()->getParams(), true));
        $this->logger->debug('LastOrderId ' .$orderId);
        /** @var \Magento\Payment\Model\Method\Adapter $method */
        $method = $order->getPayment()->getMethodInstance();
        $this->coreRegistry->register('isSecureArea', true, true);

        $resultCode = substr($vResultCode, 0, 4);
        $successCode = array('Y001');

        if(in_array($resultCode, $successCode)) {
            $payment->setTransactionId($order->getRealOrderId() . '-capture');
            $payment->setIsTransactionClosed(false);
            $payment->setIsTransactionPending(false);
            $additionalData = unserialize($payment->getAdditionalData());
            if(is_array($additionalData)) {
                $result = array_merge(
                    $additionalData,
                    $this->getRequest()->getParams()
                );
            } else {
                $result = $this->getRequest()->getParams();
            }

            $mode = $method->getConfigData('payment_action');

            if($mode == 'authorize_capture') {
                $invoices = $order->getInvoiceCollection();
                foreach($invoices as $invoice) {
                    $invoice->setBillingAddressId($order->getBillingAddressId());
                    $invoice->setShippingAddressId($order->getShippingAddressId());
                    $invoice->pay();
                    $invoice->save();
                }
            }

            $payment->setTransactionAdditionalInfo(Transaction::RAW_DETAILS, $result);
            $this->orderPaymentResource->save($payment);

            $order->setState(Order::STATE_PROCESSING)
                ->addStatusToHistory(true,
                    __('Alipay authorization success.'),
                    false
                );

            $this->orderRepository->save($order);

            if($order->getCanSendNewEmailFlag()){
                $this->orderSender->send($order);
            }

            $this->_redirect('checkout/onepage/success');
        } else {
            $this->processError($order);
        }
    }

    /**
     * @param $order
     */
    private function processError($order)
    {
        $order->cancel();

        $this->session->restoreQuote();
        $message = __(
            'Unable to place order. Please try again later.'
        );
        $this->messageManager->addErrorMessage($message);
        $this->orderRepository->delete($order);
        $this->_redirect('checkout/cart');
    }
}