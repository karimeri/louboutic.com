<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Controller\Notify;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\Exception\LocalizedException;
use \Veriteworks\Veritrans\Model\Notify\Processor;
use \Veriteworks\Veritrans\Helper\Data;

/**
 * Class Receive
 * @package Veriteworks\Veritrans\Controller\Notify
 */
class Receive extends Action
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var Processor
     */
    private $processor;

    /**
     * @var Data
     */
    private $helper;


    /**
     * Receive constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param Processor $processor
     * @param Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        Processor $processor,
        \Veriteworks\Veritrans\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->processor = $processor;
    }

    /**
     * redirect action
     */
    public function execute()
    {
        try {
            $request = $this->getRequest();
            $this->logger->debug(var_export($this->getRequest()->getParams(), true));

            if (!($body = $request->getContent())) {
                throw new LocalizedException(__('There is no request body in this HTTP request.'));
            }
            if (!($hmac = $request->getHeader('Content-Hmac'))) {
                throw new LocalizedException(__('There is no Content-Hmac in this HTTP header.'));
            }

            if (!$this->checkHmac($body, $hmac)) {
                throw new LocalizedException(__('No match information.'));
            }

            $this->coreRegistry->register('isSecureArea', true, true);
            $this->processor->process($request);

        } catch (Exception $e) {
            $this->_response->setHttpResponseCode(400)->setBody($e->getMessage());
            return;
        }
        $this->_response->setHttpResponseCode(200)->setBody('OK');
    }


    /**
     * @param $msgBody
     * @param $hmacString
     * @return string
     */
    private function checkHmac($msgBody, $hmacString)
    {
        $delimiter = ";v=";
        $pos = strpos($hmacString, $delimiter);

        $s_pos = $pos + strlen($delimiter);
        $param_hmac = substr($hmacString, $s_pos);
        
        $merchant_secret_key = $this->helper->getMerchantPassword();
        $encryptionKeyBytes = pack('H*', $merchant_secret_key);

        $hmac = hash_hmac("sha256", $msgBody, $encryptionKeyBytes);

        return ($hmac === $param_hmac);
    }
}