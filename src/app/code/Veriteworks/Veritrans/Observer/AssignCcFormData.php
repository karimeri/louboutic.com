<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@veriteworks.co.jp so we can send you a copy immediately.
 *
 * @category
 * @package
 * @copyright  Copyright (c) $year Veriteworks Inc. (https://principle-works.jp/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Veriteworks\Veritrans\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;


class AssignCcFormData extends AbstractDataAssignObserver
{

    const CARD_NUMBER   = 'cc_number';
    const CVV           = 'cc_cid';
    const EXP_MONTH     = 'cc_exp_month';
    const EXP_YEAR      = 'cc_exp_year';
    const PAYMENT_TYPE  = 'payment_type';
    const SPLIT_COUNT   = 'split_count';
    const PAYMENT_METHOD_NONCE = 'payment_method_nonce';
    const TOKEN         = 'token';
    const INCREMENT_ID  = 'increment_id';

    /**
     * @var array
     */
    protected $additionalInformationList = [
        self::CARD_NUMBER,
        self::CVV,
        self::EXP_MONTH,
        self::EXP_YEAR,
        self::PAYMENT_TYPE,
        self::SPLIT_COUNT,
        self::TOKEN,
        self::INCREMENT_ID
    ];

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
            return;
        }

        $paymentInfo = $this->readPaymentModelArgument($observer);

        foreach ($this->additionalInformationList as $additionalInformationKey) {
            if (isset($additionalData[$additionalInformationKey])) {
                $paymentInfo->setAdditionalInformation(
                    $additionalInformationKey,
                    $additionalData[$additionalInformationKey]
                );
            }
        }
    }
}