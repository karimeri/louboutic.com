<?php

namespace Us\ProhibitedProduct\Plugin\Magento\Checkout\Model;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

class ShippingInformationManagementPlugin
{
    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * ShippingInformationManagementPlugin constructor.
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        CollectionFactory $productCollectionFactory,
        EnvironmentManager $environmentManager
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param ShippingInformationManagement $subject
     * @param                               $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws StateException
     * @throws NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $region = $addressInformation->getShippingAddress()->getRegion();
            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $this->quoteRepository->getActive($cartId);
            $quoteItems = $quote->getItems();

            $productsId = [];
            foreach ($quoteItems as $item) {
                $productsId[] = $item->getProduct()->getId();
            }

            //Forced to load the product collection in order to get the attribute ...
            $productCollection = $this->productCollectionFactory->create();
            $productCollection->addFieldToSelect('name')
                ->addAttributeToSelect('is_prohibited_in_states')
                ->addIdFilter($productsId)
                ->load();

            foreach ($productCollection as $product) {
                $listOfRegionProhibited = $product->getAttributeText('is_prohibited_in_states');

                if ((is_array($listOfRegionProhibited) && in_array($region, $listOfRegionProhibited))
                    || (!empty($listOfRegionProhibited) && $region == $listOfRegionProhibited)) {
                    throw new StateException(
                        __(
                            'The product "%s" may not be sold in your current shipping region. 
                        Please remove it from the cart.',
                            $product->getName()
                        )
                    );
                }
            }
        }
    }
}
