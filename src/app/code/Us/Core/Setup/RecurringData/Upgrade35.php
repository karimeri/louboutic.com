<?php

namespace Us\Core\Setup\RecurringData;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Store;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade35
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade35 implements UpgradeDataSetupInterface
{

    const THEME_NAME = 'Synolia/louboutin-us';

    /**
     * @var \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Theme\Model\Config
     */
    protected $config;

    /**
     * Upgrade35 constructor.
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Theme\Model\Config $config
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\Theme\Model\Config $config
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
    }

    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {

        $themes = $this->collectionFactory->create()->loadRegisteredThemes();

        foreach ($themes as $theme) {
            if ($theme->getCode() === self::THEME_NAME) {
                $this->config->assignToStore(
                    $theme,
                    [Store::DEFAULT_STORE_ID],
                    ScopeConfigInterface::SCOPE_TYPE_DEFAULT
                );
            }
        }
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Changing theme for each store in US env';
    }
}
