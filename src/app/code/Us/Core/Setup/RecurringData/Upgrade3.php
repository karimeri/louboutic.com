<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade3
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade3 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('synolia_retailer/map/map_latitude', '46.868041');
        $this->configSetup->saveConfig('synolia_retailer/map/map_longitude', '-99.927112');
        $this->configSetup->saveConfig('synolia_retailer/map/map_zoom', '3');
        $this->configSetup->saveConfig('synolia_retailer/map/map_style', 'default');
        $this->configSetup->saveConfig('synolia_retailer/map/map_unit', 'miles');
        $this->configSetup->saveConfig('synolia_retailer/map/map_distance', '2680.36');
        $this->configSetup->saveConfig('synolia_retailer/general/display_address', '1');
        $this->configSetup->saveConfig('synolia_retailer/general/display_contact_information', '1');
        $this->configSetup->saveConfig('synolia_retailer/map/map_filter', '{"_0":{"label":"woman","value":"Woman"},"_1":{"label":"man","value":"Man"},"_2":{"label":"bag","value":"Bag"},"_3":{"label":"beauty","value":"Beauty"}}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_all_retailer_title', 'Find Christian Louboutin Stores - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_all_retailer_description', 'Christian Louboutin Official Website {store} - Locate All Christian Louboutin Stores in the World');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_country_title', 'Find Christian Louboutin Stores in {country} - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_country_description', 'Discover our Christian Louboutin Stores in {country} and find all information about Opening Hours, Special Events and Products.');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_city_title', 'Find Christian Louboutin Stores in {city}, {country} - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_city_description', 'Discover our Christian Louboutin Stores in {city}, {country} and find all information about Opening Hours, Special Events and Products.');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_retailer_title', 'Find {retailer} Stores - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_retailer_description', 'Discover our {retailer} and find all information about Opening Hours, Special Events and Products.');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Retailer config US';
    }
}
