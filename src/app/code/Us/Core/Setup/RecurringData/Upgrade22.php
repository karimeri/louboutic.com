<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade22
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade22 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade22 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->taskRepository = $taskRepository;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            [
                'name' => 'linkshare_export_products',
                'active' => 1,
                'frequency' => '0 8 * * *',
                'command' => 'synolia:sync:launch linkshare_export_products',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'linkshare_export_orders',
                'active' => 1,
                'frequency' => '0 21 * * *',
                'command' => 'synolia:sync:launch linkshare_export_orders',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'linkshare_export_canceled_orders',
                'active' => 1,
                'frequency' => '15 21 * * *',
                'command' => 'synolia:sync:launch linkshare_export_canceled_orders',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ]
        ];

        foreach ($data as $task) {
            $taskModel = $this->task->setData($task);
            $this->taskRepository->save($taskModel);
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Init platform cron for linkshare';
    }
}
