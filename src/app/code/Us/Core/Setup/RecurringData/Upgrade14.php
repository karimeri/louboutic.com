<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Us\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade14
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade14 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Us\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade14 constructor.
     * @param \Us\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput,
        ConfigSetup $configSetup
    ) {
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {

        $prefixConfigFR = 'M.;Mlle;Mme.';

        foreach ($this->upgradeData->getStores() as $stores => $storesId) {
            if ($stores === 'ca_fr') {
                foreach ($storesId as $storeId) {
                    $this->consoleOutput->writeln('Saving configuration '.$prefixConfigFR.' on '
                        .$stores.' store '. $storeId);

                    $this->configSetup->saveConfig(
                        'customer/address/prefix_options',
                        $prefixConfigFR,
                        'stores',
                        $storeId
                    );
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Saving prefix configuration on ca_fr store';
    }
}
