<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade10
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade10 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade10 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('synolia_retailer/map/map_marker', 'default/point-map-small.png');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Retailer marker config US';
    }
}
