<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade8
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade8 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade8 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('tax/cart_display/subtotal', 1);
        $this->configSetup->saveConfig('louboutin_tax/minicart_display/subtotal', 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Set shopping cart tax configs for US';
    }
}
