<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Us\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class Upgrade23
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade23 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Us\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade23 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Us\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     */
    public function __construct(
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsPageIdentifiers = [
            'bikini-questa-sera' => 'Bikini Questa Sera',
            'tornade-blonde'     => 'Tornade Blonde',
            'trouble-in-heaven'  => 'Trouble In Heaven'
        ];

        foreach ($cmsPageIdentifiers as $identifier => $title) {
            $this->addCmsPages($identifier, $title);
        }
    }

    /**
     * @param $identifier
     * @param $title
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addCmsPages($identifier, $title)
    {
        foreach ($this->upgradeData->getStores() as $locale => $stores) {
            if ($locale === 'ca_en' || $locale === 'ca_fr') {
                $cmsPageContent = $this->cmsSetup->getCmsPageContent(
                    $identifier,
                    'Us_Core',
                    '',
                    '',
                    'misc/cms/pages/'.$locale
                );

                $cmsPage = [
                    'title'             => 'Christian Louboutin - ' . $title,
                    'page_layout'       => 'fullscreen',
                    'identifier'        => $identifier . '.html',
                    'content_heading'   => '',
                    'content'           => $cmsPageContent,
                    'is_active'         => 1,
                    'stores'            => $stores
                ];

                $this->consoleOutput->writeln('Saving page '.$identifier.' on '
                    .$locale.' stores ('.implode(',', $stores).') ');

                $this->cmsSetup->savePage($cmsPage, true);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS page bikini-questa-sera, tornade-blonde, trouble-in-heaven for US';
    }
}
