<?php

namespace Us\Core\Setup\RecurringData;

use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade28
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade28 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $storeCode = [
        'ot_en',
        'ca_en',
        'us_en',
    ];

    /**
     * Upgrade28 constructor.
     * @param ConfigSetup $configSetup
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManagerInterface $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        foreach ($this->storeCode as $storeCode) {
            $this->configSetup->saveConfig(
                'synolia_retailer/general/twelve_format_hour',
                1,
                ScopeInterface::SCOPE_WEBSITES,
                $this->storeManager->getStore($storeCode)->getWebsiteId()
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Website in 12 hour format';
    }
}
