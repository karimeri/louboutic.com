<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade34
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade34 implements UpgradeDataSetupInterface
{

    const TABLE_EMAIL_TEMPLATE = 'email_template';

    const COL_TEMPLATE_TEXT = 'template_text';
    const COL_TEMPLATE_CODE = 'template_code';

    const BLOCK_TRACK_PATTERN = '({{block .+email.+track.phtml.+shipment=.+order=.+}})';
    const NEW_BLOCK_TRACK = '{{block class=\'Project\\Sales\\Block\\Email\\Trackings\' area=\'frontend\'' .
    'template=\'Magento_Sales::email/shipment/track.phtml\' shipment=$shipment order=$order}}';

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * Upgrade34 constructor.
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Zend_Db_Statement_Exception
     */
    public function run(Upgrade $upgradeObject)
    {
        $connection = $this->resource->getConnection();
        $tableExist = $connection->isTableExists(self::TABLE_EMAIL_TEMPLATE);

        if (!$tableExist) {
            return;
        }

        $select = $connection->select()
            ->from(
                $connection->getTableName(self::TABLE_EMAIL_TEMPLATE),
                [self::COL_TEMPLATE_CODE]
            )
            ->where(self::COL_TEMPLATE_CODE . ' LIKE (?)', "%Shipment - confirmation email%");

        //phpcs:ignore
        $templates = $connection->query($select)->fetchAll();

        foreach ($templates as $template) {
            $select = $connection->select()
                ->from(
                    $connection->getTableName(self::TABLE_EMAIL_TEMPLATE),
                    [self::COL_TEMPLATE_TEXT, self::COL_TEMPLATE_CODE]
                )
                ->where(self::COL_TEMPLATE_CODE . ' = (?)', $template[self::COL_TEMPLATE_CODE])
                ->limit(1);

            $result = $connection->fetchOne($select);

            if (!$result) {
                return;
            }

            if (\preg_match(self::BLOCK_TRACK_PATTERN, $result)) {
                $connection->update(
                    $connection->getTableName(self::TABLE_EMAIL_TEMPLATE),
                    [
                        self::COL_TEMPLATE_TEXT => \preg_replace(
                            self::BLOCK_TRACK_PATTERN,
                            self::NEW_BLOCK_TRACK,
                            $result,
                            1
                        )
                    ],
                    [
                        self::COL_TEMPLATE_CODE . ' = (?)' => [
                            $template[self::COL_TEMPLATE_CODE]
                        ]
                    ]
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Correcting tracking list tag in email template for US';
    }
}
