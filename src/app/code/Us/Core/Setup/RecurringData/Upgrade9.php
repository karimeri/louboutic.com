<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade9
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade9 implements UpgradeDataSetupInterface
{
    /**
     * @var EavSetup
     */
    protected $eavSetup;


    /**
     * Upgrade9 constructor.
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $postcodeAttribute = [
            'type' => 'customer_address',
            'code' => 'postcode',
            'data' => [
                'validate_rules' => '',
            ],
        ];

        foreach ($postcodeAttribute['data'] as $field => $value) {
            $this->eavSetup->updateAttribute($postcodeAttribute['type'], $postcodeAttribute['code'], $field, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Set validation rule for postcode attribute on US';
    }
}
