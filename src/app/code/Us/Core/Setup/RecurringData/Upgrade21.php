<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade21
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade21 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade21 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->taskRepository = $taskRepository;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            [
                'name' => 'y2_import_prices',
                'active' => 1,
                'frequency' => '35 23 * * *',
                'command' => 'synolia:sync:launch y2_import_prices',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ]
        ];

        foreach ($data as $task) {
            $taskModel = $this->task->setData($task);
            $this->taskRepository->save($taskModel);
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Init platform cron for Y2';
    }
}
