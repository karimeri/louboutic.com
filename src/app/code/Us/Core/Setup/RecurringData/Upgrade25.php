<?php

namespace Us\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Us\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class Upgrade25
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade25 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Us\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade25 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Us\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     */
    public function __construct(
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsPageIdentifier = 'discover-lips';

        foreach ($this->upgradeData->getStores() as $locale => $stores) {
            $cmsPageContent = $this->cmsSetup->getCmsPageContent(
                $cmsPageIdentifier,
                'Us_Core',
                '',
                '',
                'misc/cms/pages/'.$locale
            );

            $cmsPage = [
                'title'             => 'Christian Louboutin - Discover Lips',
                'page_layout'       => 'fullscreen',
                'identifier'        => 'discover-lips.html',
                'content_heading'   => '',
                'content'           => $cmsPageContent,
                'is_active'         => 1,
                'stores'            => $stores
            ];

            $this->consoleOutput->writeln('Saving page '.$cmsPageIdentifier.' on '
                .$locale.' stores ('.implode(',', $stores).') ');

            $this->cmsSetup->savePage($cmsPage);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS page discover-lips for US';
    }
}
