<?php

namespace Us\Core\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\Upgrade;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class RecurringData
 * Upgrade Data script
 * @codeCoverageIgnore
 * @package Eu\Core\Setup
 * @author Synolia <contact@synolia.com>
 */
class RecurringData implements \Magento\Framework\Setup\InstallDataInterface
{
    /**
     * @var Upgrade
     */
    protected $synoliaSetup;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * RecurringData constructor.
     * @param Upgrade $synoliaSetup
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        Upgrade $synoliaSetup,
        EnvironmentManager $environmentManager
    ) {
        $this->synoliaSetup = $synoliaSetup;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if ($this->environmentManager->getEnvironment() != Environment::US) {
            return;
        }

        $setup->startSetup();
        $this->synoliaSetup->runUpgrade($this, 'RecurringData');
        $setup->endSetup();
    }
}
