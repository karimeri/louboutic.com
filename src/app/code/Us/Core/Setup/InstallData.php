<?php

namespace Us\Core\Setup;

use Magento\Framework\App\Config;
use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Registry;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepositoryFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\WebsiteRepository;
use Project\Core\Model\Environment;
use Project\Core\Setup\CoreInstallData;
use Project\Core\Setup\DirectoryListist;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Synolia\Standard\Setup\ShopSetupFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class InstallData
 * @package   Us\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData extends CoreInstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Synolia\Standard\Setup\ShopSetup
     */
    protected $shopSetup;

    /**
     * @var Magento\Store\Model\StoreRepository
     */
    protected $storeRepository;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Website
     */
    protected $websiteResourceModel;

    /**
     * @var WebsiteFactory
     */
    protected $websiteFactory;

    /**
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
     * @var Group
     */
    protected $groupResourceModel;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Store
     */
    protected $storeResourceModel;

    /**
     * InstallData constructor.
     * @param DirectoryList          $directoryList
     * @param Csv                    $csvProcessor
     * @param Reader                 $moduleReader
     * @param StoreManager           $storeManager
     * @param WriterInterface        $writerInterface
     * @param Config                 $config
     * @param ShopSetupFactory       $shopSetupFactory
     * @param StoreRepositoryFactory $storeRepositoryFactory
     * @param ConfigSetupFactory     $configSetupFactory
     * @param WebsiteRepository      $websiteRepository
     * @param Website                $websiteResourceModel
     * @param WebsiteFactory         $websiteFactory
     * @param GroupFactory           $groupFactory
     * @param Group                  $groupResourceModel
     * @param StoreFactory           $storeFactory
     * @param Store                  $storeResourceModel
     * @param Registry               $registry
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $writerInterface,
        Config $config,
        ShopSetupFactory $shopSetupFactory,
        StoreRepositoryFactory $storeRepositoryFactory,
        ConfigSetupFactory $configSetupFactory,
        WebsiteRepository $websiteRepository,
        Website $websiteResourceModel,
        WebsiteFactory $websiteFactory,
        GroupFactory $groupFactory,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        Store $storeResourceModel,
        Registry $registry
    ) {
        parent::__construct($directoryList, $csvProcessor, $moduleReader, $storeManager, $writerInterface);

        $this->config               = $config;
        $this->shopSetup            = $shopSetupFactory->create();
        $this->storeRepository      = $storeRepositoryFactory->create();
        $this->configSetupFactory   = $configSetupFactory;
        $this->websiteRepository    = $websiteRepository;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->websiteFactory       = $websiteFactory;
        $this->groupFactory         = $groupFactory;
        $this->groupResourceModel   = $groupResourceModel;
        $this->storeFactory         = $storeFactory;
        $this->storeResourceModel   = $storeResourceModel;
        $this->registry             = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        // @codingStandardsIgnoreStarts
        if (!getenv('PROJECT_ENV') || getenv('PROJECT_ENV') != Environment::US) {
            echo 'Install Core US ignore';
            // @codingStandardsIgnoreEnd
            $setup->endSetup();
            return;
        }

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $this->config->clean();
        $this->createWebsites();

        $setup->endSetup();
    }

    /**
     * Crate website Time FR and Time Int EN
     */
    public function createWebsites()
    {
        $this->configSetup->saveConfig('general/locale/code', 'en_US');

        $this->registry->register('isSecureArea', true);

        $this->shopSetup->populateWebsites([
            UpgradeData::WEBSITE_CODE_US => [
                'code' => UpgradeData::WEBSITE_CODE_US,
                'name' => 'United States',
                'is_default' => 1,
                'groups' => [
                    'United States Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_US,
                        'name' => 'United States Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_US_EN => [
                                'code' => UpgradeData::STORE_CODE_US_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_CA => [
                'code' => UpgradeData::WEBSITE_CODE_CA,
                'name' => 'Canada',
                'groups' => [
                    'Canada Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_CA,
                        'name' => 'Canada Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_CA_EN => [
                                'code' => UpgradeData::STORE_CODE_CA_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_CA_FR => [
                                'code' => UpgradeData::STORE_CODE_CA_FR,
                                'name' => 'French',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_OT => [
                'code' => UpgradeData::WEBSITE_CODE_OT,
                'name' => 'Other Countries',
                'groups' => [
                    'Other Countries Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_OT,
                        'name' => 'Other Countries Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_OT_EN => [
                                'code' => UpgradeData::STORE_CODE_OT_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $store = $this->storeFactory->create();
        $this->storeResourceModel->load($store, 'default', 'code');
        $this->storeResourceModel->delete($store);

        $storeGroup = $this->groupFactory->create();
        $this->groupResourceModel->load($storeGroup, 'main_website_store', 'code');
        $this->groupResourceModel->delete($storeGroup);

        $website = $this->websiteFactory->create();
        $this->websiteResourceModel->load($website, 'base', 'code');
        $this->websiteResourceModel->delete($website);

        $idStoreUsEn = $this->storeRepository->get(UpgradeData::STORE_CODE_US_EN)->getId();
        $idStoreCaEn = $this->storeRepository->get(UpgradeData::STORE_CODE_CA_EN)->getId();
        $idStoreCaFr = $this->storeRepository->get(UpgradeData::STORE_CODE_CA_FR)->getId();
        $idStoreOtEn = $this->storeRepository->get(UpgradeData::STORE_CODE_OT_EN)->getId();

        $stores = [
            $idStoreUsEn => 'en_US',
            $idStoreCaEn => 'en_US',
            $idStoreCaFr => 'fr_CA',
            $idStoreOtEn => 'en_US'
        ];

        foreach ($stores as $id => $locale) {
            $this->configSetup->saveConfig('general/locale/code', $locale, ScopeInterface::SCOPE_STORES, $id);
        }
    }
}
