<?php

namespace Us\Core\Setup\Upgrade;

use Us\Core\Setup\UpgradeData;

/**
 * Class Upgrade104
 * @package   Us\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade104
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Disable adyen on US
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_abstract/debug', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_abstract/paypal_capture_mode', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_apple_pay/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_boleto/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_cc/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_cc/enable_installments', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_hpp/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_oneclick/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_pay_by_mail/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_pos/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_sepa/active', 0);
    }
}
