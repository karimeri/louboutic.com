<?php
namespace Us\Core\Setup\Upgrade;

use Us\Core\Setup\UpgradeData;
use Magento\Rma\Model\Rma;

/**
 * Class Upgrade1010
 * @package Us\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1010
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        // Active RMA on storefront for all
        $upgradeDataObject->getConfigSetup()->saveConfig(Rma::XML_PATH_ENABLED, 1);
    }
}
