<?php

namespace Us\Core\Setup\Upgrade;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\File\Csv;
use Magento\Tax\Api\Data\TaxClassInterface;
use Project\Core\Setup\TaxInstallData;
use Us\Core\Setup\UpgradeData;
use Magento\Tax\Api\Data\TaxClassInterfaceFactory;
use Magento\Tax\Api\TaxClassRepositoryInterface;
use Magento\Framework\Module\Dir\Reader;

/**
 * Class Upgrade102
 * @package   Us\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade102 extends TaxInstallData
{
    const MODULE_NAME = 'Us_Core';
    const TAX_CLASS_PATH = 'data/taxClass.csv';

    /**
     * @var TaxClassInterfaceFactory
     */
    protected $taxClassDataObjectFactory;

    /**
     * @var TaxClassRepositoryInterface
     */
    protected $taxClassRepository;

    /**
     * Upgrade102 constructor.
     * @param Csv                         $csvProcessor
     * @param Reader                      $moduleReader
     * @param TaxClassInterfaceFactory    $taxClassDataObjectFactory
     * @param TaxClassRepositoryInterface $taxClassRepository
     */
    public function __construct(
        Csv $csvProcessor,
        Reader $moduleReader,
        TaxClassInterfaceFactory $taxClassDataObjectFactory,
        TaxClassRepositoryInterface $taxClassRepository
    ) {
        parent::__construct($csvProcessor, $moduleReader);

        $this->taxClassDataObjectFactory = $taxClassDataObjectFactory;
        $this->taxClassRepository        = $taxClassRepository;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->saveTaxClass();
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveTaxClass()
    {
        $csvData = $this->getDataTaxFile(self::MODULE_NAME, self::TAX_CLASS_PATH);
        $taxClassArray = $this->formatRawData($csvData);

        foreach ($taxClassArray as $taxClass) {
            $taxClassModel = $this->getTaxClassModel();
            foreach ($taxClass as $method => $value) {
                $taxClassModel->$method($value);
            }
            $this->taxClassRepository->save($taxClassModel);
        }
    }

    /**
     * @return TaxClassInterface
     */
    protected function getTaxClassModel()
    {
        return $this->taxClassDataObjectFactory->create();
    }
}
