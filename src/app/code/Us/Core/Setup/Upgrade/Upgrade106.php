<?php

namespace Us\Core\Setup\Upgrade;

use Us\Core\Setup\UpgradeData;

/**
 * Class Upgrade106
 * @package   Us\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade106
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Hide tooltips on checkout address form on US
        $upgradeDataObject->getConfigSetup()->saveConfig('synolia_standard_design/checkout/display_tooltips', 0);
    }
}
