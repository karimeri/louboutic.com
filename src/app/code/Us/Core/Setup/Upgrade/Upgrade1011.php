<?php

namespace Us\Core\Setup\Upgrade;

use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\Standard\Setup\CmsSetup;
use Us\Core\Setup\UpgradeData;

/**
 * Class Upgrade1011
 * @package Us\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1011
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade1011 constructor.
     * @param CmsSetup $cmsSetup
     * @param ConsoleOutput $consoleOutput
     */
    public function __construct(
        CmsSetup $cmsSetup,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @throws NoSuchEntityException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $cmsBlockIdentifier = 'product-contact';

        foreach ($upgradeDataObject->getStores() as $storeCode => $stores) {
            $cmsBlockContent = $this->cmsSetup->getCmsBlockContent(
                $cmsBlockIdentifier,
                'Us_Core',
                '',
                '',
                'misc/cms/blocks/'.$storeCode
            );

            $cmsBlock = [
                'title'      => 'PRODUCT > contact',
                'identifier' => $cmsBlockIdentifier,
                'content'    => $cmsBlockContent,
                'is_active'  => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->consoleOutput->writeln('Saving block '.$cmsBlockIdentifier.' on '
                .$storeCode.' stores ('.implode(',', $stores).') ');

            $this->cmsSetup->saveBlock($cmsBlock);
        }
    }
}
