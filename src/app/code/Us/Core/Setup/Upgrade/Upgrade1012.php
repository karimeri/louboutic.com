<?php

namespace Us\Core\Setup\Upgrade;

use Magento\Cms\Model\BlockRepository;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollectionFactory;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\Standard\Setup\CmsSetup;
use Us\Core\Setup\UpgradeData;

/**
 * Class Upgrade1012
 * @package Us\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade1012
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var BlockCollectionFactory
     */
    protected $blockCollectionFactory;

    /**
     * Upgrade1012 constructor.
     * @param CmsSetup               $cmsSetup
     * @param ConsoleOutput          $consoleOutput
     * @param BlockRepository        $blockRepository
     * @param BlockCollectionFactory $blockCollectionFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        ConsoleOutput $consoleOutput,
        BlockRepository $blockRepository,
        BlockCollectionFactory $blockCollectionFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->consoleOutput = $consoleOutput;
        $this->blockRepository = $blockRepository;
        $this->blockCollectionFactory = $blockCollectionFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $cmsBlockIdentifiers = [
            'menu-help',
            'footer-desktop-ecom',
            'footer-mobile-ecom',
        ];

        $allStores = $upgradeDataObject->getStores();

        foreach ($cmsBlockIdentifiers as $cmsBlockIdentifier) {
            $blockCollection = $this->blockCollectionFactory->create();

            $block = $blockCollection
                ->addFieldToFilter('identifier', $cmsBlockIdentifier)
                ->addStoreFilter(0, false)
                ->getFirstItem(); // phpcs:ignore

            if ($block->getId()) {
                // phpcs:ignore
                $this->blockRepository->delete($block);
            }

            foreach ($allStores as $storeCode => $stores) {
                $cmsBlockContent = $this->cmsSetup->getCmsBlockContentData([
                    'identifier' => $cmsBlockIdentifier,
                    'moduleName' => 'Us_Core',
                    'filePath'   => 'misc/cms/blocks/' . $storeCode,
                ]);

                $cmsBlock = [
                    'title'      => $cmsBlockIdentifier,
                    'identifier' => $cmsBlockIdentifier,
                    'content'    => $cmsBlockContent,
                    'is_active'  => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores,
                ];

                $this->consoleOutput->writeln(
                    'Saving block ' . $cmsBlockIdentifier . ' on ' . $storeCode .
                    ' stores (' . \implode(',', $stores) . ')'
                );

                $this->cmsSetup->saveBlock($cmsBlock, true);
            }
        }
    }
}
