<?php

namespace Us\Core\Setup\Upgrade;

use Us\Core\Setup\UpgradeData;

/**
 * Class Upgrade107
 *
 * @package Us\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade107
{
    const COUNTRY_AND_CURRENCY_PATH = 'data/countryAndCurrency.csv';

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $countryAndCurrencyData = $upgradeDataObject->getDataFile(UpgradeData::MODULE_NAME, self::COUNTRY_AND_CURRENCY_PATH);
        $upgradeDataObject->saveConfig($countryAndCurrencyData);
    }
}
