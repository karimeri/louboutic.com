<?php

namespace Us\Core\Setup\Upgrade;

use Us\Core\Setup\UpgradeData;

/**
 * Class Upgrade101
 * @package   Us\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade101
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $upgradeDataObject->getConfigSetup()->saveConfig('general/locale/timezone', 'America/New_York');
    }
}
