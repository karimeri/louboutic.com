<?php

namespace Us\Core\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepository;
use Project\Core\Model\Environment;
use Project\Core\Setup\CoreInstallData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class UpgradeData
 * @package   Us\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class UpgradeData extends CoreInstallData implements UpgradeDataInterface
{
    const WEBSITE_CODE_US = 'us';
    const WEBSITE_CODE_CA = 'ca';
    const WEBSITE_CODE_OT = 'ot';

    const STORE_CODE_US_EN = 'us_en';
    const STORE_CODE_CA_EN = 'ca_en';
    const STORE_CODE_CA_FR = 'ca_fr';
    const STORE_CODE_OT_EN = 'ot_en';

    const MODULE_NAME = 'Us_Core';

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * UpgradeData constructor.
     * @param DirectoryList      $directoryList
     * @param Csv                $csvProcessor
     * @param Reader             $moduleReader
     * @param StoreManager       $storeManager
     * @param WriterInterface    $writerInterface
     * @param ConsoleOutput      $output
     * @param ConfigSetupFactory $configSetupFactory
     * @param StoreRepository    $storeRepository
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $writerInterface,
        ConsoleOutput $output,
        ConfigSetupFactory $configSetupFactory,
        StoreRepository $storeRepository
    ) {
        parent::__construct($directoryList, $csvProcessor, $moduleReader, $storeManager, $writerInterface);

        $this->output             = $output;
        $this->configSetupFactory = $configSetupFactory;
        $this->objectManager      = ObjectManager::getInstance();
        $this->storeRepository    = $storeRepository;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        // @codingStandardsIgnoreStart
        if (!getenv('PROJECT_ENV') || getenv('PROJECT_ENV') != Environment::US) {
            // @codingStandardsIgnoreEnd
            $this->output->writeln('Upgrade Core US ignore');
            $setup->endSetup();
            return;
        }

        $versions = [
            '1.0.1' => '101',
            '1.0.2' => '102',
            '1.0.3' => '103',
            '1.0.4' => '104',
            '1.0.5' => '105',
            '1.0.6' => '106',
            '1.0.7' => '107',
            '1.0.9'  => '109',
            '1.0.11' => '1011',
            '1.0.12' => '1012',
            '1.0.13' => '1013',
            '1.0.14' => '1014'
        ];

        $this->output->writeln(""); // new line in console

        foreach ($versions as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing US Core setup : $version");

                $currentSetup = $this->getObjectManager()->create('Us\Core\Setup\Upgrade\Upgrade'.$fileData);
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ModuleDataSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return ConfigSetup
     */
    public function getConfigSetup()
    {
        return $this->configSetup;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStores()
    {
        return [
            self::STORE_CODE_US_EN => [
                $this->storeRepository->get(self::STORE_CODE_US_EN)->getId(),
            ],
            self::STORE_CODE_CA_EN => [
                $this->storeRepository->get(self::STORE_CODE_CA_EN)->getId()
            ],
            self::STORE_CODE_CA_FR => [
                $this->storeRepository->get(self::STORE_CODE_CA_FR)->getId()
            ],
            self::STORE_CODE_OT_EN => [
                $this->storeRepository->get(self::STORE_CODE_OT_EN)->getId()
            ]
        ];
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocale()
    {
        return [
            'en_US' => [
                $this->storeRepository->get(self::STORE_CODE_US_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CA_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_OT_EN)->getId()
            ],
            'fr_CA' => [
                $this->storeRepository->get(self::STORE_CODE_CA_FR)->getId()
            ]
        ];
    }
}
