define([
    "jquery",
    "jquery/ui",
    "underscore"
], function($) {
    "use strict";

    $.widget('synolia.livechat', {
        options: {
            url_script: '',
            url_chat: '',
            mainchatcode: '',
            chatcode1: '',
            chatcode2: '',
            linkSelector: '.livechat'
        },

        liveagent: undefined,

        /**
         * Constructor
         * @private
         */
        _create: function () {
            _.bindAll(this, '_scriptLoaded', '_linkClick', '_laqCallback');

            this._bindEvents();

            $.getScript(this.options.url_script, this._scriptLoaded);
        },

        /**
         * Bind events
         * @private
         */
        _bindEvents: function () {
            $(document).on('click', this.options.linkSelector, this._linkClick);
        },

        /**
         * Call when external script is loaded
         * @private
         */
        _scriptLoaded: function () {
            this.liveagent = window.liveagent;

            this.liveagent.init(
                this.options.url_chat,
                this.options.chatcode1,
                this.options.chatcode2
            );

            if (!window._laq) {
                window._laq = [];
            }

            window._laq.push(this._laqCallback);
        },

        /**
         * Callback function pushed in _laq array
         * @private
         */
        _laqCallback: function () {
            var chatElements = document.getElementsByClassName('livechat');
            for (var i = 0; i < chatElements.length; ++i) {
                this.liveagent.showWhenOnline(
                    this.options.mainchatcode,
                    chatElements[i]
                );
            }
        },

        /**
         * Click on "livechat" link
         * @param event
         * @private
         */
        _linkClick: function (event) {
            event.preventDefault();
            this.liveagent.startChat(this.options.mainchatcode);
        }
    });

    return $.synolia.livechat;
});
