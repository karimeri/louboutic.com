<?php
namespace Us\LiveChat\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 * @package Us\LiveChat\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const URL_LIVECHAT_SCRIPT   = 'https://c.la1-c2-ord.salesforceliveagent.com/content/g/js/40.0/deployment.js';
    const URL_LIVECHAT_CHAT     = 'https://d.la1-c2-ord.salesforceliveagent.com/chat';

    const XML_PATH_ACTIVE       = 'louboutin_chat/livechat/active';
    const XML_PATH_MAINCHATCODE = 'louboutin_chat/livechat/main_chat_code';
    const XML_PATH_CHATCODE1    = 'louboutin_chat/livechat/chat_code1';
    const XML_PATH_CHATCODE2    = 'louboutin_chat/livechat/chat_code2';

    /**
     * @param             $configName
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getActive($scopeCode = null)
    {
        return $this->getConfig($this::XML_PATH_ACTIVE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getMainChatCode($scopeCode = null)
    {
        return $this->getConfig($this::XML_PATH_MAINCHATCODE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getChatCode1($scopeCode = null)
    {
        return $this->getConfig($this::XML_PATH_CHATCODE1, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getChatCode2($scopeCode = null)
    {
        return $this->getConfig($this::XML_PATH_CHATCODE2, $scopeCode);
    }

    /**
     * @return string
     */
    public function getUrlScript()
    {
        return $this::URL_LIVECHAT_SCRIPT;
    }

    /**
     * @return string
     */
    public function getUrlChat()
    {
        return $this::URL_LIVECHAT_CHAT;
    }
}
