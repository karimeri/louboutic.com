<?php

namespace Us\MultiStock\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\ConfigSetup;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Magento\Store\Model\WebsiteRepository;
use Synolia\MultiStock\Setup\StockSetupFactory;
use Synolia\MultiStock\Setup\StockSetup;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class InstallData
 * @package Us\MultiStock\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var StockSetup
     */
    protected $stockSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * InstallData constructor.
     * @param \Synolia\Standard\Setup\ConfigSetupFactory $configSetupFactory
     * @param \Magento\Store\Model\WebsiteRepository $websiteRepository
     * @param \Synolia\MultiStock\Setup\StockSetupFactory $stockSetupFactory
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     */
    public function __construct(
        ConfigSetupFactory $configSetupFactory,
        WebsiteRepository $websiteRepository,
        StockSetupFactory $stockSetupFactory,
        EnvironmentManager $environmentManager,
        ConsoleOutput $consoleOutput
    ) {
        $this->configSetupFactory = $configSetupFactory;
        $this->websiteRepository  = $websiteRepository;
        $this->stockSetup         = $stockSetupFactory->create();
        $this->environmentManager = $environmentManager;
        $this->consoleOutput      = $consoleOutput;
    }

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        if ($this->environmentManager->getEnvironment() != Environment::US) {
            $this->configSetup->saveConfig('cataloginventory/item_options/is_multistock', 0);
            $this->consoleOutput->write('Install MultiStock US ignore');
            $setup->endSetup();
            return;
        }

        $this->consoleOutput->write('Install MultiStock US 1.0.0');
        $this->configSetup->saveConfig('cataloginventory/item_options/is_multistock', 1);
        $this->createStocks();

        $setup->endSetup();
    }

    /**
     * Create Stock Name
     */
    public function createStocks()
    {
        try {
            $websiteUs = $this->websiteRepository->get('us');
            $websiteCa = $this->websiteRepository->get('ca');
            $dataStock = [
                [
                    'website_id' => $websiteUs->getId(),
                    'stock_name' => 'B25',
                ],
                [
                    'website_id' => $websiteCa->getId(),
                    'stock_name' => 'BB6',
                ]
            ];
            $this->stockSetup->createStocks($dataStock);
        } catch (\Exception $exception) {
            return;
        }
    }
}
