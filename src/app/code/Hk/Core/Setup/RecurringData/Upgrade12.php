<?php

namespace Hk\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade12
 * @package Hk\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade12 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade12 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('salesforce/general/source_code', 'Asia');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Salesforce source code for Hk';
    }
}
