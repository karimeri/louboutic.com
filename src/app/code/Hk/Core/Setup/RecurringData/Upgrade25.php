<?php

namespace Hk\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

class Upgrade25 implements UpgradeDataSetupInterface
{

    /**
     * @var CmsSetup
     */
    protected $cmsSetup;
    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade25 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(CmsSetup $cmsSetup, SliderSetup $sliderSetup)
    {
        $this->cmsSetup    = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->addMaterialsSliderHk();
        $this->addMaterialsSliderEn();
        $this->addEmbellishmentSliderHk();
        $this->addEmbellishmentSliderEn();
        $this->addAnimatedShoesSliderHk();
        $this->addAnimatedShoesSliderEn();
    }

    /**
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addEmbellishmentSliderEn()
    {
        $slider = [
            [
                'identifier'     => 'discover-embellishment-slider-en',
                'title'          => 'DISCOVER - Embellishment slider - EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-embellishment-slide-1',
                        'title'      => 'Embroideries',
                        'image_big'  => 'embellishments1.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-2',
                        'title'      => 'Chains',
                        'image_big'  => 'embellishments3.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-3',
                        'title'      => 'Spikes Dégradé',
                        'image_big'  => 'embellishments4.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-4',
                        'title'      => 'Embellished Patch',
                        'image_big'  => 'embellishments5.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-5',
                        'title'      => 'Pik Pik',
                        'image_big'  => 'embellishments8.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-6',
                        'title'      => 'Embroideries',
                        'image_big'  => 'embellishments2.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-7',
                        'title'      => 'Strass',
                        'image_big'  => 'embellishments10.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-8',
                        'title'      => 'Spikes',
                        'image_big'  => 'embellishments9.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-9',
                        'title'      => 'Perforated Cap Toe',
                        'image_big'  => 'embellishments6.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($slider);
    }

    /**
     * @param \Eu\Core\Setup\UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addAnimatedShoesSliderEn()
    {

        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-first',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-second',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );
        $thirdSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-third',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );
        $fourthSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fourth',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );
        $fifthSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fifth',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );

        $sliders = [
            [
                'identifier'     => 'discover-animated-shoe-slider-hk-en',
                'title'          => 'DISCOVER - Animated Shoe Slider - HK EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'derby-hk-en',
                        'title'        => 'Derby',
                        'image_big'    => 'oxford-sprite-in.png',
                        'image_medium' => 'oxford-sprite-out.png',
                        'image_small'  => 'oxford.svg',
                        'content'      => $firstSlide,
                    ],
                    [
                        'identifier'   => 'loafer-hk-en',
                        'title'        => 'Loafer',
                        'image_big'    => 'loafer-sprite-in.png',
                        'image_medium' => 'loafer-sprite-out.png',
                        'image_small'  => 'loafer.svg',
                        'content'      => $secondSlide,
                    ],
                    [
                        'identifier'   => 'oxford-hk-en',
                        'title'        => 'Oxford',
                        'image_big'    => 'brogue-sprite-in.png',
                        'image_medium' => 'brogue-sprite-out.png',
                        'image_small'  => 'brogue.svg',
                        'content'      => $thirdSlide,
                    ],
                    [
                        'identifier'   => 'ankle-boot-hk-en',
                        'title'        => 'Ankle Boot',
                        'image_big'    => 'boot-sprite-in.png',
                        'image_medium' => 'boot-sprite-out.png',
                        'image_small'  => 'boot.svg',
                        'content'      => $fourthSlide,
                    ],
                    [
                        'identifier'   => 'double-monk-strap-hk-en',
                        'title'        => 'Double Monk Strap',
                        'image_big'    => 'monkStrap-sprite-in.png',
                        'image_medium' => 'monkStrap-sprite-out.png',
                        'image_small'  => 'monkstrap.svg',
                        'content'      => $fifthSlide,
                    ]
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addMaterialsSliderEn()
    {
        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-first',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-second',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/en_AU'
        );

        $sliders = [
            [
                'identifier'     => 'discover-soles-slider-hk-en',
                'title'          => 'DISCOVER - Soles slider - EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-soles-slide-1-hk-en',
                        'title'      => 'The Lug sole',
                        'content'    => $secondSlide,
                        'image_big'  => 'step3-sole-img2.png',
                    ],
                    [
                        'identifier' => 'discover-soles-slide-2-hk-en',
                        'title'      => 'The Red sole',
                        'content'    => $firstSlide,
                        'image_big'  => 'step3-sole-img1.png',
                    ],
                ],
            ],
            [
                'identifier'     => 'discover-materials-slider-hk-en',
                'title'          => 'DISCOVER - Materials slider - HK EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-materials-slide-1-hk-en',
                        'title'      => 'Calf Leather',
                        'image_big'  => 'materials1.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-2-hk-en',
                        'title'      => 'Printed Patent Leather',
                        'image_big'  => 'materials3.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-3-hk-en',
                        'title'      => 'Patent Leather',
                        'image_big'  => 'materials4.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-4-hk-en',
                        'title'      => 'Laser Cut',
                        'image_big'  => 'materials6.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-5-hk-en',
                        'title'      => 'Denim',
                        'image_big'  => 'materials7.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-6-hk-en',
                        'title'      => 'Patent Dégradé',
                        'image_big'  => 'materials8.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-7-hk-en',
                        'title'      => 'Moiré',
                        'image_big'  => 'materials9.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-8-hk-en',
                        'title'      => 'Tressé',
                        'image_big'  => 'materials10.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-9-hk-en',
                        'title'      => 'Veau Velours',
                        'image_big'  => 'materials11.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addMaterialsSliderHk()
    {
        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-first',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-second',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );

        $sliders = [
            [
                'identifier'     => 'discover-soles-slider-hk',
                'title'          => 'DISCOVER - Soles slider - HK',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-soles-slide-1-hk',
                        'title'      => '坑纹鞋底',
                        'content'    => $secondSlide,
                        'image_big'  => 'step3-sole-img2.png',
                    ],
                    [
                        'identifier' => 'discover-soles-slide-2-hk',
                        'title'      => '红鞋底',
                        'content'    => $firstSlide,
                        'image_big'  => 'step3-sole-img1.png',
                    ],
                ],
            ],
            [
                'identifier'     => 'discover-materials-slider-hk',
                'title'          => 'DISCOVER - Materials slider - HK',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-materials-slide-1-hk',
                        'title'      => '小牛皮',
                        'image_big'  => 'materials1.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-2-hk',
                        'title'      => '印花漆皮',
                        'image_big'  => 'materials3.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-3-hk',
                        'title'      => '漆皮',
                        'image_big'  => 'materials4.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-4-hk',
                        'title'      => '镭射切割皮革',
                        'image_big'  => 'materials6.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-5-hk',
                        'title'      => '牛仔布',
                        'image_big'  => 'materials7.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-6-hk',
                        'title'      => '渐变色漆皮',
                        'image_big'  => 'materials8.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-7-hk',
                        'title'      => '云纹绸',
                        'image_big'  => 'materials9.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-8-hk',
                        'title'      => '织皮',
                        'image_big'  => 'materials10.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-9-hk',
                        'title'      => '丝绒',
                        'image_big'  => 'materials11.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }

    /**
     * @param \Eu\Core\Setup\UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addAnimatedShoesSliderHk()
    {

        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-first',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-second',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );
        $thirdSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-third',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );
        $fourthSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fourth',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );
        $fifthSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fifth',
            'Hk_Core',
            '',
            '',
            'misc/cms/blocks/zh_Hans_CN'
        );

        $sliders = [
            [
                'identifier'     => 'discover-animated-shoe-slider-hk',
                'title'          => 'DISCOVER - Animated Shoe Slider - HK',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'derby-hk',
                        'title'        => '牛津鞋',
                        'image_big'    => 'oxford-sprite-in.png',
                        'image_medium' => 'oxford-sprite-out.png',
                        'image_small'  => 'oxford.svg',
                        'content'      => $firstSlide,
                    ],
                    [
                        'identifier'   => 'loafer-hk',
                        'title'        => '绅士鞋',
                        'image_big'    => 'loafer-sprite-in.png',
                        'image_medium' => 'loafer-sprite-out.png',
                        'image_small'  => 'loafer.svg',
                        'content'      => $secondSlide,
                    ],
                    [
                        'identifier'   => 'oxford-hk',
                        'title'        => '凋花鞋',
                        'image_big'    => 'brogue-sprite-in.png',
                        'image_medium' => 'brogue-sprite-out.png',
                        'image_small'  => 'brogue.svg',
                        'content'      => $thirdSlide,
                    ],
                    [
                        'identifier'   => 'ankle-boot-hk',
                        'title'        => '短靴',
                        'image_big'    => 'boot-sprite-in.png',
                        'image_medium' => 'boot-sprite-out.png',
                        'image_small'  => 'boot.svg',
                        'content'      => $fourthSlide,
                    ],
                    [
                        'identifier'   => 'double-monk-strap-hk',
                        'title'        => '搭扣皮鞋',
                        'image_big'    => 'monkStrap-sprite-in.png',
                        'image_medium' => 'monkStrap-sprite-out.png',
                        'image_small'  => 'monkstrap.svg',
                        'content'      => $fifthSlide,
                    ]
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }


    /**
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addEmbellishmentSliderHk()
    {
        $slider = [
            [
                'identifier'     => 'discover-embellishment-slider-hk',
                'title'          => 'DISCOVER - Embellishment slider - HK',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-embellishment-slide-1',
                        'title'      => '刺绣',
                        'image_big'  => 'embellishments1.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-2',
                        'title'      => '链饰',
                        'image_big'  => 'embellishments3.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-3',
                        'title'      => '渐变色窝钉',
                        'image_big'  => 'embellishments4.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-4',
                        'title'      => '装饰拼布',
                        'image_big'  => 'embellishments5.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-5',
                        'title'      => '溷合窝钉',
                        'image_big'  => 'embellishments8.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-6',
                        'title'      => '刺绣',
                        'image_big'  => 'embellishments2.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-7',
                        'title'      => '闪石',
                        'image_big'  => 'embellishments10.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-8',
                        'title'      => '窝钉',
                        'image_big'  => 'embellishments9.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-9',
                        'title'      => '通花鞋头',
                        'image_big'  => 'embellishments6.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($slider);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return "Add sliders discover";
    }
}
