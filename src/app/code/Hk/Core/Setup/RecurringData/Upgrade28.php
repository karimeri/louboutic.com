<?php

namespace Hk\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Hk\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class Upgrade28
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade28 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Hk\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade28 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Hk\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     */
    public function __construct(
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsPageIdentifier = 'discover-nails';

        foreach ($this->upgradeData->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsPageContent = $this->cmsSetup->getCmsPageContent(
                $cmsPageIdentifier,
                'Hk_Core',
                '',
                '',
                'misc/cms/pages/'.$locale
            );

            $cmsPage = [
                'title'             => 'Christian Louboutin - Discover Nails',
                'page_layout'       => 'fullscreen',
                'identifier'        => 'discover-nails.html',
                'content_heading'   => '',
                'content'           => $cmsPageContent,
                'is_active'         => 1,
                'stores'            => $stores
            ];

            $this->consoleOutput->writeln('Saving page '.$cmsPageIdentifier.' on '
                .$locale.' stores ('.implode(',', $stores).') ');

            $this->cmsSetup->savePage($cmsPage);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS page discover-nails for HK';
    }
}
