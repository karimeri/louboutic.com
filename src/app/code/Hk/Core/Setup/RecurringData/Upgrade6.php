<?php

namespace Hk\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade6
 * @package Us\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade6 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade6 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('currency/options/customsymbol', '{"AUD":"AUD","TWD":"TWD","HKD":"HKD"}');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Currency Symbol HK';
    }
}
