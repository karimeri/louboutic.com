<?php

namespace Hk\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository;
use Magento\CheckoutAgreements\Model\AgreementFactory;
use Synolia\Standard\Setup\CmsSetup;
use Hk\Core\Setup\UpgradeData;

/**
 * Class Upgrade2
 * @package Hk\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade2 implements UpgradeDataSetupInterface
{
    /**
     * @var \Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository
     */
    protected $agreementsRepository;

    /**
     * @var \Magento\CheckoutAgreements\Model\AgreementFactory
     */
    protected $agreementFactory;

    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Hk\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * Upgrade2 constructor.
     * @param \Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository $agreementsRepository
     * @param \Magento\CheckoutAgreements\Model\AgreementFactory $agreementFactory
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Hk\Core\Setup\UpgradeData $upgradeData
     */
    public function __construct(
        CheckoutAgreementsRepository $agreementsRepository,
        AgreementFactory  $agreementFactory,
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData
    ) {
        $this->agreementsRepository = $agreementsRepository;
        $this->agreementFactory = $agreementFactory;
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $messages = [];

        $dataAgreements = [
            [
                'name' => 'Terms and Conditions',
                'checkbox_text_html' => 'checkbox-terms-and-conditions',
                'is_active' => 1,
                'is_html' => 1,
                'mode' => 1
            ],
        ];

        foreach ($dataAgreements as $agreement) {
            foreach ($this->upgradeData->getStoresIndexedByLocale() as $locale => $stores) {
                foreach ($stores as $storeId) {
                    $checkboxText = $this->cmsSetup->getCmsBlockContent(
                        $agreement['checkbox_text_html'],
                        'Hk_Core',
                        '',
                        '',
                        'misc/cms/blocks/' . $locale
                    );

                    $agreementObject = $this->agreementFactory
                        ->create()
                        ->setName($agreement['name'] . ' - ' . $locale)
                        ->setContent(" ")
                        ->setCheckboxText($checkboxText)
                        ->setIsActive($agreement['is_active'])
                        ->setIsHtml($agreement['is_html'])
                        ->setMode($agreement['mode']);

                    try {
                        //@codingStandardsIgnoreLine
                        $this->agreementsRepository->save($agreementObject, $storeId);
                    } catch (\Exception $e) {
                        $messages[] = $e->getMessage();
                        continue;
                    }
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Init Terms and conditions checkbox on checkout for HK';
    }
}
