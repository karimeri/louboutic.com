<?php

namespace Hk\Core\Setup\RecurringData;

use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\WebsiteRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade8
 * @package Hk\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade8 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Magento\Store\Model\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * Upgrade7 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Magento\Store\Model\WebsiteRepository $websiteRepository
     */
    public function __construct(
        ConfigSetup $configSetup,
        WebsiteRepository $websiteRepository
    ) {
        $this->configSetup = $configSetup;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig(
            'general/country/allow',
            'HK,MO',
            ScopeInterface::SCOPE_WEBSITES,
            $this->websiteRepository->get('hk')->getId()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Update allowed countries for HK';
    }
}
