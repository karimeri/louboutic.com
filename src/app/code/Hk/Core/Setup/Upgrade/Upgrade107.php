<?php

namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;

/**
 * Class Upgrade107
 *
 * @package Hk\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade107
{
    const COUNTRY_AND_CURRENCY_PATH = 'data/countryAndCurrency.csv';

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $countryAndCurrencyData = $upgradeDataObject->getDataFile(UpgradeData::MODULE_NAME, self::COUNTRY_AND_CURRENCY_PATH);
        $upgradeDataObject->saveConfig($countryAndCurrencyData);
    }
}
