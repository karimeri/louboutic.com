<?php

namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;

/**
 * Class Upgrade101
 * @package   Hk\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade101
{
    /**
     * @param $upgradeDataObject
     */
    public function run($upgradeDataObject)
    {
        $upgradeDataObject->getConfigSetup()->saveConfig('general/locale/timezone', 'Asia/Singapore');
    }
}
