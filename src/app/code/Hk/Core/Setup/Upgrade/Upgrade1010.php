<?php
namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;
use Magento\Rma\Model\Rma;

/**
 * Class Upgrade1010
 * @package Hk\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1010
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        // Disable RMA on storefront for all
        $upgradeDataObject->getConfigSetup()->saveConfig(Rma::XML_PATH_ENABLED, 0);
    }
}
