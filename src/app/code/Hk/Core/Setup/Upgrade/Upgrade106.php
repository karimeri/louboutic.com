<?php

namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;

/**
 * Class Upgrade106
 * @package   Hk\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade106
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Hide tooltips on checkout address form on HK
        $upgradeDataObject->getConfigSetup()->saveConfig('synolia_standard_design/checkout/display_tooltips', 0);
    }
}
