<?php

namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\ResourceModel\Website;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\RestrictSale\Manager\ConfigManager;

/**
 * Class Upgrade105
 *
 * @package Hk\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade105
{
    /**
     * @var \Synolia\RestrictSale\Manager\ConfigManager
     */
    protected $configManager;
    /**
     * @var \Magento\Store\Api\WebsiteRepositoryInterface
     */
    protected $websiteRepository;
    /**
     * @var \Us\Core\Setup\Upgrade\ConsoleOutput
     */
    protected $output;
    /**
     * @var \Magento\Store\Model\ResourceModel\Website
     */
    protected $websiteResourceModel;

    /**
     * Upgrade105 constructor.
     *
     * @param \Synolia\RestrictSale\Manager\ConfigManager $configManager
     * @param \Magento\Store\Api\WebsiteRepositoryInterface $websiteRepository
     * @param \Magento\Store\Model\ResourceModel\Website $websiteResourceModel
     * @param \Symfony\Component\Console\Output\ConsoleOutput $output
     */
    public function __construct(
        ConfigManager $configManager,
        WebsiteRepositoryInterface $websiteRepository,
        Website $websiteResourceModel,
        ConsoleOutput $output
    ) {
        $this->configManager        = $configManager;
        $this->websiteRepository    = $websiteRepository;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->output               = $output;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        try {
            /** @var \Magento\Store\Model\Website $website */
            $website = $this->websiteRepository->get(UpgradeData::WEBSITE_CODE_CN);
            $website->setRestrictSale(1);
            $this->websiteResourceModel->save($website);

            $configRestrict = [
                ConfigManager::OPTION_HIDE_PRODUCT_PRICE      => '1',
                ConfigManager::OPTION_HIDE_ADD_TO_CART_BUTTON => '1',
                ConfigManager::OPTION_HIDE_SEARCH             => '1',
                ConfigManager::OPTION_RESTRICT_CATALOG        => '1',
                ConfigManager::OPTION_RESTRICT_WISHLIST       => '1',
                ConfigManager::OPTION_RESTRICT_CUSTOMER       => '1',
                ConfigManager::OPTION_RESTRICT_CHECKOUT       => '1',
            ];

            $this->configManager->updateWebsiteConfig($configRestrict, $website->getId());
        } catch (\Throwable $throwable) {
            $this->output->writeln('<error> Unable to restrict sale on website China </error>');
        }
    }
}
