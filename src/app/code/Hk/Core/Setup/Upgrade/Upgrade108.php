<?php

namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade108
 * @package Hk\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade108
{
    const XML_RECAPTCHA_BASE_PATH = 'synolia_recaptcha/general/';

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade108 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'enabled',
            '1'
        );
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'public_key',
            '6LcK800UAAAAACUCFDaI5DwgVQvG1ygkIxG-KbTi'
        );
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'private_key',
            '6LcK800UAAAAAOA6jTDrZ3EdqyQ5BzUg7uj2LQ_1'
        );
    }
}
