<?php

namespace Hk\Core\Setup\Upgrade;

use Hk\Core\Setup\UpgradeData;

/**
 * Class Upgrade102
 * @package   Hk\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade102
{
    /**
     * @param $upgradeDataObject
     */
    public function run($upgradeDataObject)
    {
        $this->saveTaxConfiguration($upgradeDataObject);
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    protected function saveTaxConfiguration(UpgradeData $upgradeDataObject)
    {
        $configData = [
            'tax/calculation/price_includes_tax' => 1,
            'tax/calculation/shipping_includes_tax' => 1,
            'tax/calculation/discount_tax' => 1,
            'tax/defaults/country' => 'CN',
            'tax/display/type' => 2,
            'tax/display/shipping' => 2,
            'tax/cart_display/price' => 2,
            'tax/cart_display/subtotal' => 2,
            'tax/cart_display/shipping' => 2,
            'tax/cart_display/gift_wrapping' => 2,
            'tax/cart_display/printed_card' => 2,
            'tax/sales_display/price' => 2,
            'tax/sales_display/subtotal' => 2,
            'tax/sales_display/shipping' => 2,
            'tax/sales_display/gift_wrapping' => 2,
            'tax/sales_display/printed_card' => 2,
            'tax/sales_display/grand_total' => 2
        ];

        foreach ($configData as $path => $value) {
            $upgradeDataObject->getConfigSetup()->saveConfig($path, $value);
        }
    }
}
