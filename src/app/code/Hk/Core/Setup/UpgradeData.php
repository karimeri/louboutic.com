<?php

namespace Hk\Core\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepository;
use Project\Core\Model\Environment;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Project\Core\Setup\CoreInstallData;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class UpgradeData
 * @package   Hk\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class UpgradeData extends CoreInstallData implements UpgradeDataInterface
{
    const VERSIONS = [
        '1.0.1' => '101',
        '1.0.2' => '102',
        '1.0.3' => '103',
        '1.0.4' => '104',
        '1.0.5' => '105',
        '1.0.6' => '106',
        '1.0.7' => '107',
        '1.0.8' => '108',
        '1.0.9' => '109',
        '1.0.10' => '1010',
        '1.0.11' => '1011',
        '1.0.12' => '1012',
    ];

    const WEBSITE_CODE_AU  = 'au';
    const WEBSITE_CODE_MY  = 'my';
    const WEBSITE_CODE_SG  = 'sg';
    const WEBSITE_CODE_TW  = 'tw';
    const WEBSITE_CODE_HK  = 'hk';
    const WEBSITE_CODE_CN  = 'cn';

    const STORE_CODE_AU_EN = 'au_en';
    const STORE_CODE_MY_EN = 'my_en';
    const STORE_CODE_MY_SC = 'my_sc';
    const STORE_CODE_SG_EN = 'sg_en';
    const STORE_CODE_SG_SC = 'sg_sc';
    const STORE_CODE_TW_TC = 'tw_tc';
    const STORE_CODE_TW_EN = 'tw_en';
    const STORE_CODE_HK_EN = 'hk_en';
    const STORE_CODE_HK_TC = 'hk_tc';
    const STORE_CODE_CN_SC = 'cn_sc';
    const STORE_CODE_CN_EN = 'cn_en';
    const STORE_CODE_CN_TC = 'cn_tc';

    const MODULE_NAME = 'Hk_Core';

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * UpgradeData constructor.
     * @param DirectoryList      $directoryList
     * @param Csv                $csvProcessor
     * @param Reader             $moduleReader
     * @param StoreManager       $storeManager
     * @param WriterInterface    $writerInterface
     * @param ConsoleOutput      $output
     * @param ConfigSetupFactory $configSetupFactory
     * @param StoreRepository $storeRepository
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $writerInterface,
        ConsoleOutput $output,
        ConfigSetupFactory $configSetupFactory,
        StoreRepository $storeRepository
    ) {
        parent::__construct($directoryList, $csvProcessor, $moduleReader, $storeManager, $writerInterface);

        $this->output             = $output;
        $this->configSetupFactory = $configSetupFactory;
        $this->objectManager      = ObjectManager::getInstance();
        $this->storeRepository    = $storeRepository;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        // @codingStandardsIgnoreStart
        if (!getenv('PROJECT_ENV') || getenv('PROJECT_ENV') != Environment::HK) {
            // @codingStandardsIgnoreEnd
            $this->output->writeln('Upgrade Core HK ignore');
            $setup->endSetup();
            return;
        }

        $this->output->writeln(""); // new line in console

        foreach ($this::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing HK Core setup : $version");

                $currentSetup = $this->getObjectManager()->create('Hk\Core\Setup\Upgrade\Upgrade'.$fileData);
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return ConfigSetup
     */
    public function getConfigSetup()
    {
        return $this->configSetup;
    }


    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocale()
    {
        return [
            'en_AU' => [
                $this->storeRepository->get(self::STORE_CODE_AU_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MY_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_SG_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_TW_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_HK_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CN_EN)->getId(),
            ],
            'zh_Hans_CN' => [
                $this->storeRepository->get(self::STORE_CODE_MY_SC)->getId(),
                $this->storeRepository->get(self::STORE_CODE_SG_SC)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CN_SC)->getId(),
            ],
            'zh_Hant_HK' => [
                $this->storeRepository->get(self::STORE_CODE_TW_TC)->getId(),
                $this->storeRepository->get(self::STORE_CODE_HK_TC)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CN_TC)->getId(),
            ]
        ];
    }
}
