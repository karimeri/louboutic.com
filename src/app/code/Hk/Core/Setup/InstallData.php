<?php

namespace Hk\Core\Setup;

use Magento\Framework\App\Config;
use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Registry;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepositoryFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\WebsiteRepository;
use Project\Core\Model\Environment;
use Project\Core\Setup\CoreInstallData;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Synolia\Standard\Setup\ShopSetupFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class InstallData
 * @package   Hk\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData extends CoreInstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Synolia\Standard\Setup\ShopSetup
     */
    protected $shopSetup;

    /**
     * @var Magento\Store\Model\StoreRepository
     */
    protected $storeRepository;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Website
     */
    protected $websiteResourceModel;

    /**
     * @var WebsiteFactory
     */
    protected $websiteFactory;

    /**
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
     * @var Group
     */
    protected $groupResourceModel;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Store
     */
    protected $storeResourceModel;

    /**
     * InstallData constructor.
     * @param DirectoryList          $directoryList
     * @param Csv                    $csvProcessor
     * @param Reader                 $moduleReader
     * @param StoreManager           $storeManager
     * @param WriterInterface        $writerInterface
     * @param Config                 $config
     * @param ShopSetupFactory       $shopSetupFactory
     * @param StoreRepositoryFactory $storeRepositoryFactory
     * @param ConfigSetupFactory     $configSetupFactory
     * @param WebsiteRepository      $websiteRepository
     * @param Website                $websiteResourceModel
     * @param WebsiteFactory         $websiteFactory
     * @param GroupFactory           $groupFactory
     * @param Group                  $groupResourceModel
     * @param StoreFactory           $storeFactory
     * @param Store                  $storeResourceModel
     * @param Registry               $registry
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $writerInterface,
        Config $config,
        ShopSetupFactory $shopSetupFactory,
        StoreRepositoryFactory $storeRepositoryFactory,
        ConfigSetupFactory $configSetupFactory,
        WebsiteRepository $websiteRepository,
        Website $websiteResourceModel,
        WebsiteFactory $websiteFactory,
        GroupFactory $groupFactory,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        Store $storeResourceModel,
        Registry $registry
    ) {
        parent::__construct($directoryList, $csvProcessor, $moduleReader, $storeManager, $writerInterface);

        $this->config               = $config;
        $this->shopSetup            = $shopSetupFactory->create();
        $this->storeRepository      = $storeRepositoryFactory->create();
        $this->configSetupFactory   = $configSetupFactory;
        $this->websiteRepository    = $websiteRepository;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->websiteFactory       = $websiteFactory;
        $this->groupFactory         = $groupFactory;
        $this->groupResourceModel   = $groupResourceModel;
        $this->storeFactory         = $storeFactory;
        $this->storeResourceModel   = $storeResourceModel;
        $this->registry             = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        // @codingStandardsIgnoreStart
        if (!getenv('PROJECT_ENV') || getenv('PROJECT_ENV') != Environment::HK) {
            echo 'Install Core HK ignore';
            // @codingStandardsIgnoreEnd
            $setup->endSetup();
            return;
        }

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $this->config->clean();
        $this->createWebsites();

        $setup->endSetup();
    }

    /**
     * Create website Time FR and Time Int EN
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function createWebsites()
    {
        $this->configSetup->saveConfig('general/locale/code', 'en_AU');

        $this->registry->register('isSecureArea', true);

        $this->shopSetup->populateWebsites([
            UpgradeData::WEBSITE_CODE_AU => [
                'code' => UpgradeData::WEBSITE_CODE_AU,
                'name' => 'Australia',
                'is_default' => 1,
                'groups' => [
                    'Australia Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_AU,
                        'name' => 'Australia Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_AU_EN => [
                                'code' => UpgradeData::STORE_CODE_AU_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_MY => [
                'code' => UpgradeData::WEBSITE_CODE_MY,
                'name' => 'Malaysia',
                'groups' => [
                    'Malaysia Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_MY,
                        'name' => 'Malaysia Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_MY_EN => [
                                'code' => UpgradeData::STORE_CODE_MY_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_MY_SC => [
                                'code' => UpgradeData::STORE_CODE_MY_SC,
                                'name' => 'Simplified chinese',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_SG => [
                'code' => UpgradeData::WEBSITE_CODE_SG,
                'name' => 'Singapore',
                'groups' => [
                    'Singapore Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_SG,
                        'name' => 'Singapore Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_SG_EN => [
                                'code' => UpgradeData::STORE_CODE_SG_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_SG_SC => [
                                'code' => UpgradeData::STORE_CODE_SG_SC,
                                'name' => 'Simplified chinese',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_TW => [
                'code' => UpgradeData::WEBSITE_CODE_TW,
                'name' => 'Taiwan',
                'groups' => [
                    'Taiwan Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_TW,
                        'name' => 'Taiwan Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_TW_EN => [
                                'code' => UpgradeData::STORE_CODE_TW_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_TW_TC => [
                                'code' => UpgradeData::STORE_CODE_TW_TC,
                                'name' => 'Traditional chinese',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_HK => [
                'code' => UpgradeData::WEBSITE_CODE_HK,
                'name' => 'Hong Kong',
                'groups' => [
                    'Hong Kong Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_HK,
                        'name' => 'Hong Kong Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_HK_EN => [
                                'code' => UpgradeData::STORE_CODE_HK_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_HK_TC => [
                                'code' => UpgradeData::STORE_CODE_HK_TC,
                                'name' => 'Traditional chinese',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_CN => [
                'code' => UpgradeData::WEBSITE_CODE_CN,
                'name' => 'China',
                'groups' => [
                    'China Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_CN,
                        'name' => 'China Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_CN_EN => [
                                'code' => UpgradeData::STORE_CODE_CN_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_CN_SC => [
                                'code' => UpgradeData::STORE_CODE_CN_SC,
                                'name' => 'Simplified chinese',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_CN_TC => [
                                'code' => UpgradeData::STORE_CODE_CN_TC,
                                'name' => 'Traditional chinese',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $store = $this->storeFactory->create();
        $this->storeResourceModel->load($store, 'default', 'code');
        $this->storeResourceModel->delete($store);

        $storeGroup = $this->groupFactory->create();
        $this->groupResourceModel->load($storeGroup, 'main_website_store', 'code');
        $this->groupResourceModel->delete($storeGroup);

        $website = $this->websiteFactory->create();
        $this->websiteResourceModel->load($website, 'base', 'code');
        $this->websiteResourceModel->delete($website);

        $idStoreAuEn = $this->storeRepository->get(UpgradeData::STORE_CODE_AU_EN)->getId();
        $idStoreMyEn = $this->storeRepository->get(UpgradeData::STORE_CODE_MY_EN)->getId();
        $idStoreMySc = $this->storeRepository->get(UpgradeData::STORE_CODE_MY_SC)->getId();
        $idStoreSgEn = $this->storeRepository->get(UpgradeData::STORE_CODE_SG_EN)->getId();
        $idStoreSgSc = $this->storeRepository->get(UpgradeData::STORE_CODE_SG_SC)->getId();
        $idStoreTwTc = $this->storeRepository->get(UpgradeData::STORE_CODE_TW_TC)->getId();
        $idStoreTwEn = $this->storeRepository->get(UpgradeData::STORE_CODE_TW_EN)->getId();
        $idStoreHkEn = $this->storeRepository->get(UpgradeData::STORE_CODE_HK_EN)->getId();
        $idStoreHkTc = $this->storeRepository->get(UpgradeData::STORE_CODE_HK_TC)->getId();
        $idStoreCnSc = $this->storeRepository->get(UpgradeData::STORE_CODE_CN_SC)->getId();
        $idStoreCnEn = $this->storeRepository->get(UpgradeData::STORE_CODE_CN_EN)->getId();
        $idStoreCnTc = $this->storeRepository->get(UpgradeData::STORE_CODE_CN_TC)->getId();

        $stores = [
            $idStoreAuEn => 'en_AU',
            $idStoreMyEn => 'en_AU',
            $idStoreMySc => 'zh_Hans_CN',
            $idStoreSgEn => 'en_AU',
            $idStoreSgSc => 'zh_Hans_CN',
            $idStoreTwTc => 'zh_Hant_HK',
            $idStoreTwEn => 'en_AU',
            $idStoreHkEn => 'en_AU',
            $idStoreHkTc => 'zh_Hant_HK',
            $idStoreCnSc => 'zh_Hans_CN',
            $idStoreCnEn => 'en_AU',
            $idStoreCnTc => 'zh_Hant_HK'
        ];

        foreach ($stores as $id => $locale) {
            $this->configSetup->saveConfig('general/locale/code', $locale, ScopeInterface::SCOPE_STORES, $id);
        }
    }
}
