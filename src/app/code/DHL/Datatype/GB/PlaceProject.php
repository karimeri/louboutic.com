<?php
/**
 * Note : Code is released under the GNU LGPL
 *
 * Please do not change the header of this file
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU
 * Lesser General Public License as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 */

namespace DHL\Datatype\GB;

use DHL\Datatype\Base;

/**
 * Class Place
 * @package DHL\Datatype\GB
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 */
class PlaceProject extends Base
{
    /**
     * @var string
     */
    protected $_xmlNodeName = 'Place';

    /**
     * Is this object a subobject
     * @var boolean
     */
    protected $_isSubobject = true;

    /**
     * Parameters of the datatype
     * @var array
     */
    protected $_params = [
        'LocationType' => [
            'type' => 'string',
            'required' => true,
            'subobject' => false,
            'comment' => 'Location Type',
        ],
        'ResidenceOrBusiness' => [
            'type' => 'ResidenceOrBusiness',
            'required' => false,
            'subobject' => false,
            'comment' => 'Identifies if a location is a business, residence, or both
                (B:Business, R:Residence, C:Business Residence)',
            'length' => '1',
            'enumeration' => 'B,R,C',
        ],
        'CompanyName' => [
            'type' => 'CompanyNameValidator',
            'required' => false,
            'subobject' => false,
            'comment' => 'Name of company / business',
            'maxLength' => '35',
        ],
        'Address1' => [
            'type' => 'string',
            'required' => true,
            'subobject' => false,
            'comment' => 'Address 1',
        ],
        'Address2' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'Address 2',
        ],
        'Address3' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'Address 3',
        ],
        'PackageLocation' => [
            'type' => 'PackageLocation',
            'required' => false,
            'subobject' => false,
            'comment' => 'Package Location',
            'maxLength' => '35',
        ],
        'City' => [
            'type' => 'City',
            'required' => false,
            'subobject' => false,
            'comment' => 'City name',
            'maxLength' => '35',
        ],
        'CountryCode' => [
            'type' => 'CountryCode',
            'required' => false,
            'subobject' => false,
            'comment' => 'ISO country codes',
            'length' => '2',
        ],
        'StateCode' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'StateCode'
        ],
        'DivisionCode' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'Division (state) code.',
            'maxLength' => '2',
            'minLength' => '2',
        ],
        'DivisionName' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'Division name.'
        ],
        'PostalCode' => [
            'type' => 'PostalCode',
            'required' => false,
            'subobject' => false,
            'comment' => 'Full postal/zip code for address',
        ],
        'RouteCode' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'Route Code.'
        ],
        'Division' => [
            'type' => 'State',
            'required' => false,
            'subobject' => false,
            'comment' => 'State',
            'maxLength' => '35',
        ],
    ];
}
