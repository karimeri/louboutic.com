<?php
/**
 * Note : Code is released under the GNU LGPL
 *
 * Please do not change the header of this file
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU
 * Lesser General Public License as published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License for more details.
 */

namespace DHL\Entity\GB;

use DHL\Entity\Base;

/**
 * Class BookPURequest
 * @package DHL\Entity\GB
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 */
class BookPURequestProject extends Base
{
    /**
     * @var bool
     */
    protected $_displaySchemaVersion = true;

    /**
     * Is this object a subobject
     * @var boolean
     */
    protected $_isSubobject = false;

    /**
     * Name of the service
     * @var string
     */
    protected $_serviceName = 'BookPURequest';

    /**
     * @var string
     * Service XSD
     */
    protected $_serviceXSD = 'BookPURequest.xsd';

    /**
     * Parameters to be send in the body
     * @var array
     */
    protected $_bodyParams = [
        'RegionCode' => [
            'type' => 'string',
            'required' => false,
            'subobject' => false,
            'comment' => 'RegionCode',
            'minLength' => '2',
            'maxLength' => '2',
            'enumeration' => 'AP,EU,AM',
        ],
        'Requestor' => [
            'type' => 'Requestor',
            'required' => false,
            'subobject' => false,
        ],
        'Place' => [
            'type' => 'PlaceProject',
            'required' => false,
            'subobject' => true,
        ],
        'Pickup' => [
            'type' => 'DHL\Datatype\GB\Pickup',
            'required' => false,
            'subobject' => false,
        ],
        'PickupContact' => [
            'type' => 'DHL\Datatype\GB\PickupContact',
            'required' => false,
            'subobject' => false,
        ],
        'ShipmentDetails' => [
            'type' => 'ShipmentDetails',
            'required' => false,
            'subobject' => true,
        ],
    ];
}
