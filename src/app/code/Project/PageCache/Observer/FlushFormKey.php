<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\PageCache\Observer;

use Magento\Customer\Model\Session;
use Magento\Framework\App\PageCache\FormKey as CookieFormKey;
use Magento\Framework\Data\Form\FormKey as DataFormKey;
use Magento\Framework\Event\ObserverInterface;

class FlushFormKey extends \Magento\PageCache\Observer\FlushFormKey
{

    /**
     * @var CookieFormKey
     */
    private $cookieFormKey;

    /**
     * @var DataFormKey
     */
    private $dataFormKey;

    /**
     * @var Session
     */
    private $customerSession;


    /**
     * @param CookieFormKey $cookieFormKey
     * @param DataFormKey $dataFormKey
     * @param Session $customerSession
     */
    public function __construct(CookieFormKey $cookieFormKey, DataFormKey $dataFormKey, Session $customerSession)
    {
        $this->cookieFormKey = $cookieFormKey;
        $this->dataFormKey = $dataFormKey;
        $this->customerSession = $customerSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $currentFormKey = $this->dataFormKey->getFormKey();
        $this->cookieFormKey->delete();
        $this->dataFormKey->set(null);
        $beforeParams = $this->customerSession->getBeforeRequestParams();
        if (isset($beforeParams['form_key']) &&  $beforeParams['form_key'] == $currentFormKey) {
            $beforeParams['form_key'] = $this->dataFormKey->getFormKey();
            $this->customerSession->setBeforeRequestParams($beforeParams);
        }
    }
}
