<?php

namespace Project\CatalogSearch\Model\ResourceModel\Fulltext\Collection;

use Magento\Framework\Data\Collection;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorage;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory;
use Magento\Framework\Api\Search\SearchResultInterface;

class SearchResultApplier extends \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection\SearchResultApplier
{

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var SearchResultInterface
     */
    private $searchResult;

    /**
     * @var TemporaryStorageFactory
     */
    private $temporaryStorageFactory;

    /**
     * @var array
     */
    private $orders;

    /**
     * @param Collection $collection
     * @param SearchResultInterface $searchResult
     * @param TemporaryStorageFactory $temporaryStorageFactory
     * @param array $orders
     */
    public function __construct(
        Collection $collection,
        SearchResultInterface $searchResult,
        TemporaryStorageFactory $temporaryStorageFactory,
        array $orders
    ) {
        $this->collection = $collection;
        $this->searchResult = $searchResult;
        $this->temporaryStorageFactory = $temporaryStorageFactory;
        $this->orders = $orders;
    }

    /**
     * Replace creating temporary table by where condition
     * @inheritdoc
     */
    public function apply()
    {
        if (isset($this->orders['relevance'])) {
            $temporaryStorage = $this->temporaryStorageFactory->create();
            $table = $temporaryStorage->storeApiDocuments($this->searchResult->getItems());
            $this->collection->getSelect()->joinInner(
                [
                    'search_result' => $table->getName(),
                ],
                'e.entity_id = search_result.' . TemporaryStorage::FIELD_ENTITY_ID,
                []
            );

            $this->collection->getSelect()->order(
                'search_result.' . TemporaryStorage::FIELD_SCORE . ' ' . $this->orders['relevance']
            );

        } else {
            $productIds = [];
            foreach ($this->searchResult->getItems() as $item) {
                $productIds[] = $item->getId();
            }
            $this->collection->getSelect()->where('e.entity_id in (?)', $productIds);
        }
    }

}
