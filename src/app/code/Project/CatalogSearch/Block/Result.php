<?php

namespace Project\CatalogSearch\Block;

class Result extends \Magento\CatalogSearch\Block\Result
{
    /**
     * Prepare layout
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _prepareLayout()
    {
        $title = $this->getSearchQueryText();
        $this->pageConfig->getTitle()->set($title);

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadcrumbs) {
            $breadcrumbs->addCrumb(
                'search',
                [
                    'label' => $title,
                    'title' => $title
                ]
            );
        }

        return $this;
    }
}
