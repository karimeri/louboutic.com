<?php

namespace Project\Addthis\Block;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Share
 * @package Project\Addthis\Block\Share
 * @author Marwen JELLOUL
 */
class Share extends Template
{
    const XML_PATH_ADDTHIS_ID   = 'project_addthis/general/addthis_id';

    /**
     * Share constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getAddthisId()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_ADDTHIS_ID,
            ScopeInterface::SCOPE_STORE
        );
    }
}
