<?php

namespace Project\Addthis\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package Project\Addthis\Helper
 */
class Data extends AbstractHelper
{
    const XML_PATH_ADDTHIS_ID = 'project_addthis/general/addthis_id';

    /**
     * @return string
     */
    public function getAddthisId()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ADDTHIS_ID,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getAddThisUrl()
    {
        return '//s7.addthis.com/js/300/addthis_widget.js#pubid=' . $this->getAddthisId();
    }
}
