<?php

namespace Project\RestrictSale\Block;

use Magento\Cms\Block\Block;
use Synolia\RestrictSale\Helper\Data as RestrictSaleHelper;

/**
 * Class AbstractFooterBlock
 *
 * @package Project\RestrictSale\Block
 * @author Synolia <contact@synolia.com>
 */
abstract class AbstractFooterBlock extends Block
{
    /**
     * @var string
     * CMS block ID for ecom
     */
    protected static $blockIdEcom;
    /**
     * @var string
     * CMS block ID for Restricted website
     */
    protected static $blockIdRestrict;

    /**
     * AbstractFooterBlock constructor.
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     * @param \Synolia\RestrictSale\Helper\Data $restrictSaleHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        RestrictSaleHelper $restrictSaleHelper,
        array $data = []
    ) {
        $blockId = static::$blockIdEcom;
        if ($restrictSaleHelper->isSaleRestricted()) {
            $blockId = static::$blockIdRestrict;
        }

        $data = \array_merge($data, ['block_id' => $blockId]);

        parent::__construct($context, $filterProvider, $storeManager, $blockFactory, $data);
    }
}
