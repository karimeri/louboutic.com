<?php

namespace Project\RestrictSale\Block;

/**
 * Class FooterMobileBlock
 *
 * @package Project\RestrictSale\Block
 * @author Synolia <contact@synolia.com>
 */
class FooterMobileBlock extends AbstractFooterBlock
{
    protected static $blockIdEcom = 'footer-mobile-ecom';
    protected static $blockIdRestrict = 'footer-mobile';
}
