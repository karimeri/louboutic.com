<?php

namespace Project\RestrictSale\Block;

/**
 * Class FooterDesktopBlock
 *
 * @package Project\RestrictSale\Block
 * @author Synolia <contact@synolia.com>
 */
class FooterDesktopBlock extends AbstractFooterBlock
{
    protected static $blockIdEcom = 'footer-desktop-ecom';
    protected static $blockIdRestrict = 'footer-desktop';
}
