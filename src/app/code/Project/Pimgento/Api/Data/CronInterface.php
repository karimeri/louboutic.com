<?php

namespace Project\Pimgento\Api\Data;

/**
 * Interface CronInterface
 * @package Project\Pimgento\Api\Data
 */
interface CronInterface
{
    const TABLE_NAME   = 'pimgento_import_cron';
    const CRON_ID      = 'cron_id';
    const IMPORT_TYPE  = 'import_type';
    const STATUS       = 'status';
    const ORDER        = 'order';
    const EXECUTION_ID = 'execution_id';
    const FILE         = 'file';
    const ZIP_FILE     = 'zip_file';
    const ACTION       = 'action';

    const STATUS_PENDING    = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_FINISHED   = 'finished';
    const STATUS_ERROR      = 'error';

    const ACTION_MASS = 'mass';

    /**
     * Get ID
     * @return int|null
     */
    public function getId();

    /**
     * Get Import Type
     * @return string
     */
    public function getImportType();

    /**
     * Get status
     * @return string
     */
    public function getStatus();

    /**
     * Get order
     * @return int
     */
    public function getOrder();

    /**
     * Get File
     * @return string
     */
    public function getFile();

    /**
     * Get Zip File
     * @return null|string
     */
    public function getZipFile();

    /**
     * Get execution ID
     * @return int
     */
    public function getExecutionId();

    /**
     * Get action
     * @return null|string
     */
    public function getAction();

    /**
     * Set ID
     * @param int $cronId
     * @return CronInterface
     */
    public function setId($cronId);

    /**
     * Set Import Type
     * @param string $importType
     * @return CronInterface
     */
    public function setImportType($importType);

    /**
     * Set status
     * @param string $status
     * @return CronInterface
     */
    public function setStatus($status);

    /**
     * Set order
     * @param string $order
     * @return CronInterface
     */
    public function setOrder($order);

    /**
     * Set File
     * @param string $file
     * @return CronInterface
     */
    public function setFile($file);

    /**
     * Set Zip File
     * @param string $zipFile
     * @return CronInterface
     */
    public function setZipFile($zipFile);

    /**
     * Set execution ID
     * @param string $executionId
     * @return CronInterface
     */
    public function setExecutionId($executionId);

    /**
     * Set action
     * @param string $action
     * @return CronInterface
     */
    public function setAction($action);
}
