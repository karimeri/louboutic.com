<?php
namespace Project\Pimgento\Api;

/**
 * Interface PimgentoInterface
 * @package Project\Pimgento\Api
 */
interface PimgentoInterface
{
    /**
     * Lunch import pimgento by type and file
     * @param string $type
     * @param string $file
     * @param string|null $zipFile
     * @param string|null $action
     * @param int $executionId
     * @return mixed
     */
    public function launch(string $type, string $file, string $zipFile = null, string $action = null, int $executionId);
}
