<?php
namespace Project\Pimgento\Api;

use Magento\Framework\Exception\CouldNotSaveException;
use Project\Pimgento\Api\Data\CronInterface;
use Project\Pimgento\Model\Cron;

/**
 * Interface CronRepositoryInterface
 * @package Project\Pimgento\Api
 */
interface CronRepositoryInterface
{
    /**
     * Save Cron
     * @param CronInterface $cron
     * @return CronInterface
     * @throws CouldNotSaveException
     */
    public function save(CronInterface $cron);

    /**
     * Load Cron
     * @param string $importType
     * @return Cron $cron
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByImportType($importType);

    /**
     * Load Cron by status
     * @param string $status
     * @return Cron $cron
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByStatus($status);
}
