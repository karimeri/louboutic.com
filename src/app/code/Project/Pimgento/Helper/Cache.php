<?php

namespace Project\Pimgento\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Cache\TypeListInterface;

/**
 * Class Cache
 * @package Project\Pimgento\Helper
 * @author Synolia <contact@synolia.com>
 */
class Cache extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $typeList;

    /**
     * @var \Project\Pimgento\Helper\Config
     */
    protected $config;

    /**
     * Cache constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $typeList
     * @param \Project\Pimgento\Helper\Config $config
     */
    public function __construct(
        Context $context,
        TypeListInterface $typeList,
        Config $config
    ) {
        parent::__construct($context);
        $this->context  = $context;
        $this->typeList = $typeList;
        $this->config   = $config;
    }

    /**
     * @param string $type
     */
    public function cleaCache($type)
    {
        $types = $this->config->getCleanCache($type);
        foreach ($types as $type) {
            $this->typeList->cleanType($type);
        }
    }
}
