<?php

namespace Project\Pimgento\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Store\Model\Store;
use Project\Pimgento\Block\Email\Report;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;

/**
 * Class Reporting
 * @package Project\Pimgento\Helper
 */
class Reporting extends AbstractHelper
{
    const XML_PATH_REPORTING_ENABLED
        = 'pimgento/reporting/enabled';
    const XML_PATH_REPORTING_EMAIL_SENDER
        = 'pimgento/reporting/email_sender';
    const XML_PATH_REPORTING_EMAIL_RECIPIENT
        = 'pimgento/reporting/email_recipient';
    const XML_PATH_REPORTING_EMAIL_COPY
        = 'pimgento/reporting/email_copy';
    const XML_PATH_REPORTING_EMAIL_COPY_METHOD
        = 'pimgento/reporting/copy_method';
    const XML_PATH_REPORTING_EMAIL_TEMPLATE
        = 'pimgento/reporting/email_template';
    const XML_PATH_REPORTING_CRON_ERROR_EMAIL_TEMPLATE
        = 'pimgento/reporting/cron_error_email_template';

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $layout;

    /**
     * Reporting constructor.
     * @param Context $context
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param LayoutInterface $layout
     * @param State $state
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        LayoutInterface $layout,
        State $state
    ) {
        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->layout = $layout;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }

    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_REPORTING_ENABLED
        );
    }

    /**
     * @return $this
     */
    public function sendReport()
    {
        try {
            $this->inlineTranslation->suspend();

            $sender = $this->scopeConfig->getValue(
                self::XML_PATH_REPORTING_EMAIL_SENDER
            );
            $sendFrom = [
                'email' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/email'
                ),
                'name' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/name'
                ),
            ];

            $copyTo = $this->getEmails(self::XML_PATH_REPORTING_EMAIL_COPY);
            $copyMethod = $this->scopeConfig->getValue(
                self::XML_PATH_REPORTING_EMAIL_COPY_METHOD
            );
            $bcc = [];
            if ($copyTo && $copyMethod == 'bcc') {
                $bcc = $copyTo;
            }

            $sendTo = [
                [
                    'email' => $this->scopeConfig->getValue(
                        self::XML_PATH_REPORTING_EMAIL_RECIPIENT
                    ),
                    'name' => null,
                ]
            ];
            if ($copyTo && $copyMethod == 'copy') {
                foreach ($copyTo as $email) {
                    $sendTo[] = ['email' => $email, 'name' => null];
                }
            }

            $reportGrid = $this->createBlock(Report::class)->toHtml();

            $template = $this->scopeConfig->getValue(
                self::XML_PATH_REPORTING_EMAIL_TEMPLATE
            );
            foreach ($sendTo as $recipient) {
                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                        'store' => Store::DEFAULT_STORE_ID
                    ]
                )->setTemplateVars(
                    [
                        'reportGrid' => $reportGrid,
                    ]
                )->setReplyTo(
                    $sendFrom['email'],
                    $sendFrom['name']
                )->setFromByScope(
                    $sendFrom
                )->addTo(
                    $recipient['email'],
                    $recipient['name']
                )->addBcc(
                    $bcc
                )->getTransport();

                $transport->sendMessage();
            }

            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }
        return $this;
    }

    /**
     * @param array $templateVars
     * @return $this
     */
    public function sendCronErrorAlert($templateVars)
    {
        try {
            $this->inlineTranslation->suspend();

            $sender = $this->scopeConfig->getValue(
                self::XML_PATH_REPORTING_EMAIL_SENDER
            );
            $sendFrom = [
                'email' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/email'
                ),
                'name' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/name'
                ),
            ];

            $copyTo = $this->getEmails(self::XML_PATH_REPORTING_EMAIL_COPY);
            $copyMethod = $this->scopeConfig->getValue(
                self::XML_PATH_REPORTING_EMAIL_COPY_METHOD
            );
            $bcc = [];
            if ($copyTo && $copyMethod == 'bcc') {
                $bcc = $copyTo;
            }

            $sendTo = [
                [
                    'email' => $this->scopeConfig->getValue(
                        self::XML_PATH_REPORTING_EMAIL_RECIPIENT
                    ),
                    'name' => null,
                ]
            ];
            if ($copyTo && $copyMethod == 'copy') {
                foreach ($copyTo as $email) {
                    $sendTo[] = ['email' => $email, 'name' => null];
                }
            }

            $template = $this->scopeConfig->getValue(
                self::XML_PATH_REPORTING_CRON_ERROR_EMAIL_TEMPLATE
            );
            foreach ($sendTo as $recipient) {
                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                        'store' => Store::DEFAULT_STORE_ID
                    ]
                )->setTemplateVars(
                    $templateVars
                )->setReplyTo(
                    $sendFrom['email'],
                    $sendFrom['name']
                )->setFromByScope(
                    $sendFrom
                )->addTo(
                    $recipient['email'],
                    $recipient['name']
                )->addBcc(
                    $bcc
                )->getTransport();

                $transport->sendMessage();
            }

            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }
        return $this;
    }

    /**
     * @param string $configPath
     * @return array|false
     */
    protected function getEmails($configPath)
    {
        $data = $this->scopeConfig->getValue(
            $configPath
        );
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

    /**
     * Create block instance
     *
     * @param string|\Magento\Framework\View\Element\AbstractBlock $block
     * @return \Magento\Framework\View\Element\AbstractBlock
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createBlock($block)
    {
        if (is_string($block)) {
            if (class_exists($block)) {
                $block = $this->layout->createBlock($block);
            }
        }
        if (!$block instanceof \Magento\Framework\View\Element\AbstractBlock) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid block type: %1', $block));
        }
        return $block;
    }
}
