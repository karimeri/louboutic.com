<?php

namespace Project\Pimgento\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\SerializerInterface;
use Pimgento\Import\Helper\Config as PimgentoHelper;

/**
 * Class Config
 * @package Project\Pimgento\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const DEFAULT_CHANNEL                                  = 'ecommerce';
    const XML_PATH_PIMGENTO_CATEGORIES_ROOT                = 'pimgento/category/categories_root';
    const XML_PATH_PIMGENTO_AKENEO_CATEGORY_MAIN           = 'pimgento/category/akeneo_main_category';
    const XML_PATH_PIMGENTO_CATEGORY_ATTR_MAPPING          = 'pimgento/category/attribute_mapping';
    const XML_PATH_PIMGENTO_MAIN_CHANNEL                   = 'pimgento/general/main_channel';
    const XML_PATH_PIMGENTO_AKENEO_FTP_TYPE                = 'pimgento/akeneo_ftp/type';
    const XML_PATH_PIMGENTO_AKENEO_FTP_HOST                = 'pimgento/akeneo_ftp/host';
    const XML_PATH_PIMGENTO_AKENEO_FTP_USERNAME            = 'pimgento/akeneo_ftp/username';
    const XML_PATH_PIMGENTO_AKENEO_FTP_PASSWORD            = 'pimgento/akeneo_ftp/password';
    const XML_PATH_PIMGENTO_AKENEO_FTP_PORT                = 'pimgento/akeneo_ftp/port';
    const XML_PATH_PIMGENTO_AKENEO_FTP_PATH                = 'pimgento/akeneo_ftp/path';
    const XML_PATH_PIMGENTO_AKENEO_API_URL                 = 'pimgento/akeneo_api/url';
    const XML_PATH_PIMGENTO_AKENEO_API_USERNAME            = 'pimgento/akeneo_api/username';
    const XML_PATH_PIMGENTO_AKENEO_API_PASSWORD            = 'pimgento/akeneo_api/password';
    const XML_PATH_PIMGENTO_AKENEO_API_CLIENT_ID           = 'pimgento/akeneo_api/client_id';
    const XML_PATH_PIMGENTO_AKENEO_API_CLIENT_SECRET       = 'pimgento/akeneo_api/client_secret';
    const XML_PATH_PIMGENTO_CUSTOM_DISABLE_RMA             = 'pimgento/custom/disable_rma_by_attribut_set';
    const XML_PATH_PIMGENTO_PRODUCT_IMAGE_PATH             = 'pimgento/image/path';
    const XML_PATH_PIMGENTO_TAX_CLASS_ID                   = 'pimgento/product/tax_class_id_by_family';
    const XML_PATH_PIMGENTO_SPECIFIC_ATTRIBUTE_TO_ACTIVATE = 'pimgento/product/specific_attribute_to_activate';
    const XML_PATH_PIMGENTO_PRODUCT_ATTRIBUTES_EXCLUDED    = 'pimgento/product/excluded_attributes';
    const XML_PATH_PIMGENTO_ATTRIBUTE_FORCE_SCOPABLE       = 'pimgento/attribute/force_scopable';
    const XML_PATH_PIMGENTO_IMAGE_ATTACH_TO_ATTRIBUT       = 'pimgento/image/attach_to_attributes';
    const XML_PATH_PIMGENTO_IMAGE_PURGE_CDN                = 'pimgento/image/purge_cdn';
    const XML_PATH_PIMGENTO_CLEAN_CACHE                    = 'pimgento/{type}/clean_cache';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var \Pimgento\Entities\Helper\Config
     */
    protected $pimgentoHelper;

    /**
     * Config constructor.
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param \Pimgento\Entities\Helper\Config $pimgentoHelper
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        PimgentoHelper $pimgentoHelper
    ) {
        parent::__construct($context);
        $this->serializer     = $serializer;
        $this->pimgentoHelper = $pimgentoHelper;
    }

    /**
     * @param $configName
     * @return mixed
     */
    public function getConfig($configName)
    {
        return $this->scopeConfig->getValue($configName);
    }

    /**
     * @return mixed
     */
    public function getCategoriesRoot()
    {
        $categories = $this->getConfig(self::XML_PATH_PIMGENTO_CATEGORIES_ROOT);
        return !$categories ? [] : explode(';', $categories);
    }

    /**
     * @return mixed
     */
    public function getAkeneoCategoryMain()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_CATEGORY_MAIN);
    }

    /**
     * @return array|bool
     */
    public function getCategoryAttributeMapping()
    {
        $categoryAttributeMapping = $this->getConfig(self::XML_PATH_PIMGENTO_CATEGORY_ATTR_MAPPING);
        return $categoryAttributeMapping && is_array($this->serializer->unserialize($categoryAttributeMapping))
            ? $this->serializer->unserialize($categoryAttributeMapping)
            : [];
    }

    /**
     * @return mixed website_mapping
     */
    public function getMainChannel()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_MAIN_CHANNEL);
    }

    /**
     * @return mixed
     */
    public function getAkeneoFtpType()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_FTP_TYPE);
    }

    /**
     * @return mixed
     */
    public function getAkeneoFtpHost()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_FTP_HOST);
    }

    /**
     * @return mixed
     */
    public function getAkeneoFtpUsername()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_FTP_USERNAME);
    }

    /**
     * @return mixed
     */
    public function getAkeneoFtpPassword()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_FTP_PASSWORD);
    }

    /**
     * @return mixed
     */
    public function getAkeneoFtpPort()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_FTP_PORT);
    }

    /**
     * @return mixed
     */
    public function getAkeneoFtpPath()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_FTP_PATH);
    }

    /**
     * @return array
     */
    public function getFtpConfig()
    {
        return [
            'host'     => $this->getAkeneoFtpHost(),
            'port'     => $this->getAkeneoFtpPort(),
            'username' => $this->getAkeneoFtpUsername(),
            'password' => $this->getAkeneoFtpPassword(),
            'timeout'  => 90
        ];
    }

    /**
     * @return mixed
     */
    public function getAkeneoApiUrl()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_API_URL);
    }

    /**
     * @return mixed
     */
    public function getAkeneoApiUsername()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_API_USERNAME);
    }

    /**
     * @return mixed
     */
    public function getAkeneoApiPassword()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_API_PASSWORD);
    }

    /**
     * @return mixed
     */
    public function getAkeneoApiClientId()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_API_CLIENT_ID);
    }

    /**
     * @return mixed
     */
    public function getAkeneoApiClientSecret()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_AKENEO_API_CLIENT_SECRET);
    }

    /**
     * @return array
     */
    public function getAkeneoApiConfig()
    {
        return [
            'url'           => $this->getAkeneoApiUrl(),
            'username'      => $this->getAkeneoApiUsername(),
            'password'      => $this->getAkeneoApiPassword(),
            'client_id'     => $this->getAkeneoApiClientId(),
            'client_secret' => $this->getAkeneoApiClientSecret(),
        ];
    }

    /**
     * @return mixed
     */
    public function getAttributSetForDisableRma()
    {
        $attributesSet = $this->getConfig(self::XML_PATH_PIMGENTO_CUSTOM_DISABLE_RMA);
        return $attributesSet ? explode(';', $attributesSet) : false;
    }

    /**
     * @return mixed
     */
    public function getProductImagePath()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_PRODUCT_IMAGE_PATH);
    }

    /**
     * @return mixed
     */
    public function getTaxClassIdByFamily()
    {
        $taxClassId = $this->getConfig(self::XML_PATH_PIMGENTO_TAX_CLASS_ID);
        return $taxClassId ? array_column(
            $this->serializer->unserialize($taxClassId),
            'tax_class_id',
            'family'
        ) : [];
    }

    /**
     * @return mixed
     */
    public function getSpecificAttributesToActivate()
    {
        $attributes = $this->getConfig(self::XML_PATH_PIMGENTO_SPECIFIC_ATTRIBUTE_TO_ACTIVATE);
        return $attributes ? explode(';', $attributes) : false;
    }

    /**
     * @return array|bool
     */
    public function getForceAttributeScopable()
    {
        $attributes = $this->getConfig(self::XML_PATH_PIMGENTO_ATTRIBUTE_FORCE_SCOPABLE);
        return $attributes ? explode(';', $attributes) : false;
    }

    /**
     * @return array|bool
     */
    public function getAttributesToAttachToImage()
    {
        $attributes = $this->getConfig(self::XML_PATH_PIMGENTO_IMAGE_ATTACH_TO_ATTRIBUT);
        return $attributes ? explode(';', $attributes) : false;
    }

    /**
     * @return bool
     */
    public function getPurgeCdnImage()
    {
        return $this->getConfig(self::XML_PATH_PIMGENTO_IMAGE_PURGE_CDN);
    }

    /**
     * @return array|bool
     */
    public function getProductAttributesExcluded()
    {
        $attributes = $this->getConfig(self::XML_PATH_PIMGENTO_PRODUCT_ATTRIBUTES_EXCLUDED);
        return $attributes ? explode(';', $attributes) : [];
    }

    /**
     * @return string
     */
    public function getPathProductImage()
    {
        return $this->getFileFullPath($this->getProductImagePath());
    }

    /**
     * Check if file is external
     * @param $file
     * @return bool
     */
    protected function isExternalFile($file)
    {
        return substr($file, 0, 1) == '/';
    }

    /**
     * Return file full path handling potential external files
     * @param $file
     * @return string
     */
    public function getFileFullPath($file)
    {
        return $this->isExternalFile($file) ? $file : $this->pimgentoHelper->getUploadDir().'/'.$file;
    }

    /**
     * @param string $type
     * @return array
     */
    public function getCleanCache($type)
    {
        $caches = $this->getConfig(
            str_replace('{type}', $type, self::XML_PATH_PIMGENTO_CLEAN_CACHE)
        );
        return $caches ? explode(',', $caches) : [];
    }
}
