<?php

namespace Project\Pimgento\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Store\Model\StoreManager;
use Magento\Tax\Model\TaxClass\Source\Product;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Pimgento\Helper\Config;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Synolia\Cron\Model\TaskFactory;

/**
 * Class InstallData
 * @package Project\Pimgento\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    const MAIN_IMAGE = 'main_image-ecommerce';

    const ATTRIBUTE_GALLERY_IMAGE = [
        self::MAIN_IMAGE,
        'hover-ecommerce',
        'additional_gallery_1-ecommerce',
        'additional_gallery_2-ecommerce',
        'additional_gallery_3-ecommerce',
        'additional_gallery_4-ecommerce',
        'additional_gallery_5-ecommerce',
        'additional_1-ecommerce',
        'additional_2-ecommerce',
        'additional_3-ecommerce'
    ];

    const TAX_CLASS_ID_BY_FAMILY = [
        'Clothing & related products - Shoes and shoe laces' => ['shoes'],
        'Clothing & related products - Handbags' => [
            'bags',
            'accessories'
        ],
        'Clothing & related products BtoC' => [
            'beauty_nails',
            'beauty_lips',
            'beauty_eyes',
            'beauty_perfumes'
        ]
    ];

    /**
     * @var \Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $storeManager;

    /**
     * @var \Project\Pimgento\Helper\Config
     */
    protected $config;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Magento\Tax\Model\TaxClass\Source\Product
     */
    protected $productTaxClass;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $file;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Synolia\Cron\Model\TaskFactory
     */
    protected $taskFactory;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * InstallData constructor.
     * @param \Synolia\Standard\Setup\ConfigSetupFactory $configSetupFactory
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Project\Pimgento\Helper\Config $config
     * @param \Magento\Tax\Model\TaxClass\Source\Product $productTaxClass
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Synolia\Cron\Model\TaskFactory $taskFactory
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        ConfigSetupFactory $configSetupFactory,
        StoreManager $storeManager,
        Config $config,
        Product $productTaxClass,
        SerializerInterface $serializer,
        EnvironmentManager $environmentManager,
        File $file,
        DirectoryList $directoryList,
        TaskFactory $taskFactory,
        TaskRepository $taskRepository
    ) {
        $this->configSetupFactory = $configSetupFactory;
        $this->storeManager       = $storeManager;
        $this->config             = $config;
        $this->productTaxClass    = $productTaxClass;
        $this->environmentManager = $environmentManager;
        $this->serializer         = $serializer;
        $this->file               = $file;
        $this->directoryList      = $directoryList;
        $this->taskFactory        = $taskFactory;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\FileSystemException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);
        $this->initConfig($setup);

        $setup->endSetup();
    }

    /**
     * @param $setup
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function initConfig($setup)
    {
        $websites = $this->storeManager->getWebsites();
        $config   = [];

        foreach ($websites as $website) {
            $config['website_mapping'][] = [
                'website' => $website->getCode(),
                'channel' => 'ecommerce'
            ];

            $config['tax_class'][$website->getCode()] = [
                'website'   => $website->getId(),
                'tax_class' => 2 // Default Magento ID
            ];
        }

        $taxClass       = $this->serializer->serialize($config['tax_class']);
        $websiteMapping = $this->serializer->serialize($config['website_mapping']);

        $this->configSetup->saveConfig('pimgento/general/website_mapping', $websiteMapping);
        $this->configSetup->saveConfig('pimgento/image/enabled', 1);
        $this->configSetup->saveConfig('pimgento/image/base_image', self::MAIN_IMAGE);
        $this->configSetup->saveConfig('pimgento/image/thumbnail_image', self::MAIN_IMAGE);
        $this->configSetup->saveConfig('pimgento/image/small_image', self::MAIN_IMAGE);
        $this->configSetup->saveConfig('pimgento/image/gallery_image', implode(',', self::ATTRIBUTE_GALLERY_IMAGE));

        $environment = $this->environmentManager->getEnvironment();
        if (in_array($environment, [Environment::EU, Environment::JP])) {
            $this->configSetup->saveConfig('pimgento/product/tax_class', $taxClass);
        } elseif ($environment == Environment::US) {
            $this->configSetup->saveConfig('pimgento/product/tax_class', $this->serializer->serialize([]));
            $this->setTaxClassId();
        }
        $this->setCategoryRootInPimEntities($setup, $environment);
        $this->createDirectoryPimgento();
        $this->saveSchedule();
    }

    /**
     * Set Config Class Id
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function setTaxClassId()
    {
        $config        = [];
        $taxClassNames = array_column($this->productTaxClass->getAllOptions(), 'value', 'label');

        foreach (self::TAX_CLASS_ID_BY_FAMILY as $taxClassName => $attributesSet) {
            if (empty($taxClassNames[$taxClassName])) {
                continue;
            }
            foreach ($attributesSet as $attributeSet) {
                $config[] = [
                    'family'       => $attributeSet,
                    'tax_class_id' => $taxClassNames[$taxClassName]
                ];
            }
        }

        $config = $this->serializer->serialize($config);
        $this->configSetup->saveConfig('pimgento/product/tax_class_id_by_family', $config);
    }

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param $environment
     */
    protected function setCategoryRootInPimEntities(ModuleDataSetupInterface $setup, $environment)
    {
        $connection = $setup->getConnection();
        $connection->insertOnDuplicate(
            $setup->getTable('pimgento_entities'),
            [
                [
                    'import'    => 'category',
                    'code'      => $environment,
                    'entity_id' => 2,
                ]
            ]
        );
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function createDirectoryPimgento()
    {
        $this->file->checkAndCreateFolder(
            $this->directoryList->getPath('var').'/import/pimgento/images',
            0775
        );
    }

    /**
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function saveSchedule()
    {
        $task = [
            'name'      => 'Pimgento Import Cron',
            'active'    => 1,
            'frequency' => '*/15 * * * *',
            'command'   => 'synolia:sync:launch',
            'parameter' => 'pimgento_cron_runner',
            'option'    => '',
            'isolated'  => 1
        ];

        /** @var \Synolia\Cron\Model\Task $taskModel */
        $taskModel = $this->taskFactory->create();
        $this->taskRepository->save(
            $taskModel->setData($task)
        );
    }
}
