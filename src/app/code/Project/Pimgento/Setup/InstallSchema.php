<?php

namespace Project\Pimgento\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Project\Pimgento\Api\Data\CronInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Class InstallSchema
 * @package Project\Pimgento\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallSchema implements InstallSchemaInterface
{
    protected $importTypes = [
        'category',
        'family',
        'attribute',
        'option',
        'variant',
        'variant_family',
        'product'
    ];

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable(CronInterface::TABLE_NAME))
            ->addColumn(
                CronInterface::CRON_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                CronInterface::IMPORT_TYPE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Import type'
            )
            ->addColumn(
                CronInterface::STATUS,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Status'
            )
            ->addColumn(
                CronInterface::ORDER,
                Table::TYPE_INTEGER,
                1,
                ['nullable' => false],
                'Order'
            )
            ->addColumn(
                CronInterface::FILE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'File'
            )
            ->addColumn(
                CronInterface::ZIP_FILE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'File Zip'
            )
            ->addColumn(
                CronInterface::EXECUTION_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Execution ID'
            )
            ->addColumn(
                CronInterface::EXECUTION_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Action'
            )
            ->addIndex(
                $setup->getIdxName(
                    CronInterface::TABLE_NAME,
                    [CronInterface::IMPORT_TYPE],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                [CronInterface::IMPORT_TYPE],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->setComment('Pimgento Import');

        $installer->getConnection()->createTable($table);

        $this->insertData($installer);

        $installer->endSetup();
    }

    /**
     * Insert Data
     * @param SchemaSetupInterface $installer
     */
    protected function insertData(SchemaSetupInterface $installer)
    {
        $data  = [];
        $order = 1;
        foreach ($this->importTypes as $importType) {
            $data[] = [
                CronInterface::IMPORT_TYPE => $importType,
                CronInterface::STATUS      => CronInterface::STATUS_FINISHED,
                CronInterface::ORDER       => $order
            ];
            $order++;
        }

        $installer->getConnection()
            ->insertOnDuplicate(
                $installer->getTable(CronInterface::TABLE_NAME),
                $data
            );
    }
}
