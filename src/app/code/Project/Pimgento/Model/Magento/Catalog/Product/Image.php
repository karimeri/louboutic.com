<?php

namespace Project\Pimgento\Model\Magento\Catalog\Product;

use Magento\Catalog\Model\Product\Image\NotLoadInfoImageException;
use Magento\Catalog\Model\Product\Image\ParamsBuilder;
use Magento\Catalog\Model\View\Asset\ImageFactory;
use Magento\Catalog\Model\View\Asset\PlaceholderFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Image
 * @package Project\Pimgento\Model\Magento\Catalog\Product
 */
class Image extends \Magento\Catalog\Model\Product\Image
{
    /**
     * @var ImageFactory
     */
    private $viewAssetImageFactory;

    /**
     * @var PlaceholderFactory
     */
    private $viewAssetPlaceholderFactory;

    /**
     * @var \Magento\Framework\View\Asset\LocalInterface
     */
    private $imageAsset;

    /**
     * @var ParamsBuilder
     */
    private $paramsBuilder;

    /**
     * @var string
     */
    private $cachePrefix = 'IMG_INFO';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * Image constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig
     * @param \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Image\Factory $imageFactory
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \Magento\Framework\View\FileSystem $viewFileSystem
     * @param ImageFactory $viewAssetImageFactory
     * @param PlaceholderFactory $viewAssetPlaceholderFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     * @param SerializerInterface|null $serializer
     * @param ParamsBuilder|null $paramsBuilder
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product\Media\Config $catalogProductMediaConfig,
        \Magento\MediaStorage\Helper\File\Storage\Database $coreFileStorageDatabase,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\Factory $imageFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\View\FileSystem $viewFileSystem,
        \Magento\Catalog\Model\View\Asset\ImageFactory $viewAssetImageFactory,
        \Magento\Catalog\Model\View\Asset\PlaceholderFactory $viewAssetPlaceholderFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [],
        SerializerInterface $serializer = null,
        ParamsBuilder $paramsBuilder = null
    ) {
        parent::__construct($context, $registry, $storeManager, $catalogProductMediaConfig, $coreFileStorageDatabase, $filesystem, $imageFactory, $assetRepo, $viewFileSystem, $viewAssetImageFactory, $viewAssetPlaceholderFactory, $scopeConfig, $resource, $resourceCollection, $data, $serializer, $paramsBuilder);
        $this->viewAssetImageFactory = $viewAssetImageFactory;
        $this->viewAssetPlaceholderFactory = $viewAssetPlaceholderFactory;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(SerializerInterface::class);
        $this->paramsBuilder = $paramsBuilder ?: ObjectManager::getInstance()->get(ParamsBuilder::class);
    }

    /**
     * Set filenames for base file and new file
     *
     * @param string $file
     * @return $this
     * @throws \Exception
     */
    public function setBaseFile($file)
    {
        $this->_isBaseFilePlaceholder = false;

        $this->imageAsset = $this->viewAssetImageFactory->create(
            [
                'miscParams' => $this->getMiscParams(),
                'filePath' => $file,
            ]
        );
        if ($file == 'no_selection' || !$this->_fileExists($this->imageAsset->getSourceFile())) {
            $this->_isBaseFilePlaceholder = true;
            $this->imageAsset = $this->viewAssetPlaceholderFactory->create(
                [
                    'type' => $this->getDestinationSubdir(),
                ]
            );
        }

        $this->_baseFile = $this->imageAsset->getSourceFile();
        // Louboutin custom : fix $this->getNewFile empty, deprecated since M2.1, keep it for Pimgento use
        $this->_newFile = $this->imageAsset->getPath();

        return $this;
    }

    /**
     * Retrieve misc params based on all image attributes
     *
     * @return array
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function getMiscParams()
    {
        return $this->paramsBuilder->build(
            [
                'type' => $this->getDestinationSubdir(),
                'width' => $this->getWidth(),
                'height' => $this->getHeight(),
                'frame' => $this->_keepFrame,
                'constrain' => $this->_constrainOnly,
                'aspect_ratio' => $this->_keepAspectRatio,
                'transparency' => $this->_keepTransparency,
                'background' => $this->_backgroundColor,
                'angle' => $this->_angle,
                'quality' => $this->getQuality()
            ]
        );
    }

    /**
     * Save file
     *
     * @return $this
     */
    public function saveFile()
    {
        if ($this->_isBaseFilePlaceholder) {
            return $this;
        }
        $filename = $this->getBaseFile() ? $this->imageAsset->getPath() : null;
        $this->getImageProcessor()->save($filename);
        $this->_coreFileStorageDatabase->saveFile($filename);
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->imageAsset->getUrl();
    }

    /**
     * Check is image cached
     *
     * @return bool
     */
    public function isCached()
    {
        $path = $this->imageAsset->getPath();
        return is_array($this->loadImageInfoFromCache($path)) || file_exists($path);
    }

    /**
     * Return resized product image information
     *
     * @return array
     * @throws NotLoadInfoImageException
     */
    public function getResizedImageInfo()
    {
        try {
            if ($this->isBaseFilePlaceholder() == true) {
                $image = $this->imageAsset->getSourceFile();
            } else {
                $image = $this->imageAsset->getPath();
            }

            $imageProperties = $this->getImageSize($image);

            return $imageProperties;
        } finally {
            if (empty($imageProperties)) {
                throw new NotLoadInfoImageException(__('Can\'t get information about the picture: %1', $image));
            }
        }
    }

    /**
     * Get image size
     *
     * @param string $imagePath
     * @return array
     */
    private function getImageSize($imagePath)
    {
        $imageInfo = $this->loadImageInfoFromCache($imagePath);
        if (!isset($imageInfo['size'])) {
            $imageSize = getimagesize($imagePath);
            $this->saveImageInfoToCache(['size' => $imageSize], $imagePath);
            return $imageSize;
        } else {
            return $imageInfo['size'];
        }
    }

    /**
     * Save image data to cache
     *
     * @param array $imageInfo
     * @param string $imagePath
     * @return void
     */
    private function saveImageInfoToCache(array $imageInfo, string $imagePath)
    {
        $imagePath = $this->cachePrefix  . $imagePath;
        $this->_cacheManager->save(
            $this->serializer->serialize($imageInfo),
            $imagePath,
            [$this->cachePrefix]
        );
    }

    /**
     * Load image data from cache
     *
     * @param string $imagePath
     * @return array|false
     */
    private function loadImageInfoFromCache(string $imagePath)
    {
        $imagePath = $this->cachePrefix  . $imagePath;
        $cacheData = $this->_cacheManager->load($imagePath);
        if (!$cacheData) {
            return false;
        } else {
            return $this->serializer->unserialize($cacheData);
        }
    }
}
