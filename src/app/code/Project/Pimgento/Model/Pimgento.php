<?php

namespace Project\Pimgento\Model;

use Project\Pimgento\Api\PimgentoInterface;
use Project\Pimgento\Api\Data\CronInterface;

/**
 * Class Pimgento
 * @package Project\Pimgento\Model
 * @author Synolia <contact@synolia.com>
 */
class Pimgento implements PimgentoInterface
{
    /**
     * @var CronRepository
     */
    protected $cronRepository;

    /**
     * Pimgento constructor.
     * @param \Project\Pimgento\Model\CronRepository $cronRepository
     */
    public function __construct(
        CronRepository $cronRepository
    ) {
        $this->cronRepository = $cronRepository;
    }

    /**
     * Lunch import pimgento by type and file
     * @param string $type
     * @param string $file
     * @param string $zipFile
     * @param string|null $action
     * @param int $executionId
     * @return mixed|void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function launch(string $type, string $file, string $zipFile = null, string $action = null, int $executionId)
    {
        $cron = $this->cronRepository->getByImportType($type);
        $cron->setExecutionId($executionId)
            ->setStatus(CronInterface::STATUS_PENDING)
            ->setFile($file)
            ->setZipFile($zipFile)
            ->setAction($action);
        $this->cronRepository->save($cron);
    }
}
