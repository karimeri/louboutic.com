<?php

namespace Project\Pimgento\Model\Akeneo;

use Magento\Framework\Filesystem\Io\Sftp;
use Magento\Framework\Filesystem\Io\Ftp as BaseFtp;
use Project\Pimgento\Helper\Config as ConfigHelper;
use Pimgento\Import\Helper\Config as PimgentoHelper;

/**
 * Class Ftp
 * @package Project\Pimgento\Model\Akeneo
 * @author Synolia <contact@synolia.com>
 */
class Ftp
{
    /**
     * @var Sftp
     */
    protected $sftp;

    /**
     * @var BaseFtp
     */
    protected $ftp;

    /**
     * @var PimgentoHelper
     */
    protected $pimgentoHelper;

    /**
     * @var Sftp|Ftp
     */
    protected $connection;

    /**
     * @var ConfigHelper
     */
    protected $config;

    /**
     * @var array
     */
    protected $ftpDefaultConfig = ['passive' => true, 'ssl' => false];

    /**
     * Ftp constructor.
     * @param Sftp $sftp
     * @param BaseFtp $ftp
     * @param ConfigHelper $config
     * @param PimgentoHelper $pimgentoHelper
     * @throws \Exception
     */
    public function __construct(
        Sftp $sftp,
        BaseFtp $ftp,
        ConfigHelper $config,
        PimgentoHelper $pimgentoHelper
    ) {
        $this->sftp           = $sftp;
        $this->ftp            = $ftp;
        $this->pimgentoHelper = $pimgentoHelper;
        $this->config         = $config;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function open()
    {
        $type   = $this->config->getAkeneoFtpType();
        $config = $this->config->getFtpConfig();
        $config = $type == 'ftp' ? array_merge($this->ftpDefaultConfig, $config) : $config;

        $this->$type->open($config);

        $this->connection = $this->$type;

        $this->connection->cd($this->config->getAkeneoFtpPath());

        return $this;
    }

    /**
     * @param $path
     * @return $this
     */
    public function changeDirectory($path)
    {
        $this->connection->cd($path);
        return $this;
    }

    /**
     * Download file
     * @param $remoteFile path remote file
     * @param null $localFile
     * @return bool
     */
    public function downloadFile($remoteFile, $localFile = null)
    {
        $localFile = $localFile ?? $this->pimgentoHelper->getUploadDir().'/'.$remoteFile;
        $read = $this->connection->read($remoteFile, $localFile);
        return $read ? $localFile : false;
    }

    /**
     * @param $filename
     * @return bool
     */
    public function isFileExist($filename)
    {
        $files = array_column($this->connection->ls(), 'text');
        return in_array($filename, $files);
    }

    /**
     * Close connection
     * @return $this
     */
    public function close()
    {
        $this->connection->close();
        return $this;
    }
}
