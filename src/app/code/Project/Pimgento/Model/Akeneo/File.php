<?php

namespace Project\Pimgento\Model\Akeneo;

use Magento\Framework\Filesystem\Io\File as FileSystem;
use ZipArchive;

/**
 * Class File
 * @package Project\Pimgento\Model\Akeneo
 * @author Synolia <contact@synolia.com>
 */
class File
{
    /**
     * @var \Project\Pimgento\Model\Akeneo\Ftp
     */
    protected $ftp;

    /**
     * @var \Project\Pimgento\Model\Akeneo\File
     */
    protected $fileSystem;

    /**
     * @var \ZipArchive
     */
    protected $zipArchive;

    /**
     * File constructor.
     * @param \Project\Pimgento\Model\Akeneo\Ftp $ftp
     * @param \Magento\Framework\Filesystem\Io\File $fileSystem
     * @param \ZipArchive $zipArchive
     */
    public function __construct(
        Ftp $ftp,
        FileSystem $fileSystem,
        ZipArchive $zipArchive
    ) {

        $this->ftp = $ftp;
        $this->fileSystem = $fileSystem;
        $this->zipArchive = $zipArchive;
    }

    /**
     * Open connection
     * @throws \Exception
     */
    public function open()
    {
        $this->ftp->open();
    }

    /**
     * Close connection
     */
    public function close()
    {
        $this->ftp->close();
    }

    /**
     * @param $pathFile
     * @param bool|string $destination
     * @return bool
     * @throws \Exception
     */
    public function proccessFile($pathFile, $destination = null)
    {
        $pathinfo = $this->fileSystem->getPathInfo($pathFile);
        $this->ftp->changeDirectory($pathinfo['dirname']);

        if (!$this->ftp->isFileExist($pathinfo['basename'])) {
            throw new \Exception(__("File doesn\'t exist ($pathFile)"));
        }
        if (!$file = $this->ftp->downloadFile($pathinfo['basename'])) {
            throw new \Exception(__("Unable to download the file ($pathFile)"));
        }

        if ($pathinfo['extension'] == 'zip' && $destination) {
            $this->unzipFile($file, $destination);
        }

        return $file;
    }

    /**
     * @param $zipFile
     * @param $destination
     * @throws \Exception
     */
    public function unzipFile($zipFile, $destination)
    {
        $this->fileSystem->checkAndCreateFolder($destination);
        if ($this->zipArchive->open($zipFile) === true) {
            $this->zipArchive->extractTo($destination);
            $this->zipArchive->close();
        }
    }
}
