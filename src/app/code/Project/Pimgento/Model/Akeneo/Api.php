<?php

namespace Project\Pimgento\Model\Akeneo;

use Project\Pimgento\Helper\Config as PimgentoHelper;
use GuzzleHttp\Client;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Logger\Model\Logger;
use Magento\Framework\Filesystem\DirectoryList;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Api
 * @package Project\Pimgento\Model\Akeneo
 * @author Synolia <contact@synolia.com>
 */
class Api
{
    const HTTP_OK           = 200;
    const PATH_TOKEN        = '/api/oauth/v1/token';
    const PATH_NOTIFICATION = '/api/rest/v1/job_execution/add_notification/{executionId}';

    protected $token;

    /**
     * @var PimgentoHelper
     */
    protected $pimgentoHelper;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var object
     */
    protected $config;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Api constructor.
     * @param PimgentoHelper $pimgentoHelper
     * @param Client $client
     * @param LoggerFactory $loggerFactory
     * @param DirectoryList $directoryList
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        PimgentoHelper $pimgentoHelper,
        Client $client,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList
    ) {
        $this->pimgentoHelper = $pimgentoHelper;
        $this->client         = $client;
        $this->config         = (object)$this->pimgentoHelper->getAkeneoApiConfig();

        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log').'/akeneo_api.log']
        );
    }

    /**
     * Get Token
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken()
    {
        try {
            $response = $this->client->request(
                'POST',
                $this->getUrl(self::PATH_TOKEN),
                [
                    'auth'    => [$this->config->client_id, $this->config->client_secret],
                    'json'    => $this->getBodyToken(),
                    'headers' => [
                        'content-type' => 'application/json'
                    ]
                ]
            );
            $content = \GuzzleHttp\json_decode($response->getBody()->getContents());
            return $content->access_token;
        } catch (RequestException $exception) {
            $this->proccessException($exception);
            return false;
        }
    }

    /**
     * @param $url
     * @param $data
     * @param string $method
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function request($url, $data, $method = 'POST')
    {
        try {
            $response = $this->client->request(
                $method,
                $url,
                [
                    'json'    => $data,
                    'headers' => [
                        'Authorization' => 'Bearer '.$this->getToken(),
                        'content-type'  => 'application/json'
                    ],
                ]
            );
            return $response->getBody()->getContents();
        } catch (RequestException $exception) {
            $this->proccessException($exception);
            return false;
        }
    }

    /**
     * @param $executionId
     * @param $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendNotification($executionId, $data)
    {
        return $this->request(
            $this->getUrlNotification($executionId),
            $data
        );
    }

    /**
     * @param RequestException $exception
     */
    protected function proccessException(RequestException $exception)
    {
        if ($exception->hasResponse()) {
            $body = \GuzzleHttp\json_decode($exception->getResponse()->getBody()->getContents());
            if (!empty($body->message)) {
                $this->logger->addError("CODE : {$exception->getCode()}, message : $body->message");
            }
        } else {
            $this->logger->addError("CODE : {$exception->getCode()}, message : {$exception->getMessage()}");
        }
    }


    /**
     * @param $executionId
     * @return mixed
     */
    protected function getUrlNotification($executionId)
    {
        return $this->getUrl(str_replace('{executionId}', $executionId, self::PATH_NOTIFICATION));
    }

    /**
     * @param $path
     * @return string
     */
    protected function getUrl($path)
    {
        return $this->config->url.$path;
    }

    /**
     * @return array
     */
    protected function getBodyToken()
    {
        return [
            'grant_type' => 'password',
            'username'   => $this->config->username,
            'password'   => $this->config->password
        ];
    }
}
