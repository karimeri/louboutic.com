<?php

namespace Project\Pimgento\Model\Entities\ResourceModel;

use \Zend_Db_Expr as Expr;

/**
 * Class Entities
 * @package Project\Pimgento\Model\Entities\ResourceModel
 * @author Marwen JELLOUL
 */
class Entities extends \Pimgento\Entities\Model\ResourceModel\Entities
{
    /**
     * Execute insert by rows
     *
     * @param string $file
     * @param string $tableName
     * @param string $fieldsTerminated
     * @param string $fieldsEnclosure
     * @param int    $queryNumber
     *
     * @return int
     *
     * @throws \Exception
     */
    public function insertByRows($file, $tableName, $fieldsTerminated = ';', $fieldsEnclosure = '"', $queryNumber = 1000)
    {
        if (!file_exists($file)) {
            throw new \Exception(__("%s does not exist.", $file));
        }

        if (!is_readable($file)) {
            throw new \Exception(__("Unable to read %s.", $file));
        }

        $fileHandle = fopen($file, "r");
        if ($fileHandle === false) {
            throw new \Exception(__("Unable to open %s.", $file));
        }

        $fileSize = filesize($file);
        if ($fileSize == 0) {
            fclose($fileHandle);
            throw new \Exception(__("Unable to open %s.", $file));
        }

        $columnNames  = [];
        $columnValues = [];
        $rowCount = 0;

        $connection = $this->getConnection();

        while (($csvLine = fgetcsv($fileHandle, null, $fieldsTerminated, $fieldsEnclosure)) !== false) {
            $rowCount++;

            if ($rowCount == 1) {
                // Get column names as first row - assumes first row always has this data
                foreach ($csvLine as $key => $value) {
                    array_push($columnNames, $value);
                }
                continue;
            }

            // Build column => value map for insert
            foreach ($csvLine as $key => $value) {
                if (!array_key_exists($key, $columnNames)) {
                    throw new \Exception('The line #'.$rowCount.' has too many columns');
                }

                if(strpos($columnNames[$key], "name") === 0 ){
                    $value = ucwords(strtolower($value));
                    $value = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
                }

                $columnValues[$rowCount][$columnNames[$key]] = $value;
            }

            if ($rowCount % $queryNumber == 0) {
                // Insert our row into the tmp table
                $connection->insertMultiple($tableName, $columnValues);
                $columnValues = array();
            }
        }

        if (count($columnValues) > 0) {
            $connection->insertMultiple($tableName, $columnValues);
        }

        fclose($fileHandle);

        return $connection->fetchOne(
            $connection->select()->from($tableName, array(new Expr('COUNT(*)')))
        );
    }

}
