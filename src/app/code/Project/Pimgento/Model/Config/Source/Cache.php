<?php
namespace Project\Pimgento\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\App\Cache\Manager;

/**
 * Class Cache
 * @package Project\Pimgento\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class Cache implements ArrayInterface
{
    /**
     * @var \Magento\Framework\App\Cache\Manage
     */
    protected $cacheManager;

    /**
     * Cache constructor.
     * @param \Magento\Framework\App\Cache\Manager $cacheManager
     */
    public function __construct(Manager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    /**
     * Return options array
     * @return array
     */
    public function toOptionArray()
    {
        $caches = $this->cacheManager->getAvailableTypes();
        foreach ($caches as $cache) {
            $options[] = [
                'value' => $cache,
                'label' => $cache
            ];
        }
        return $options;
    }
}
