<?php
namespace Project\Pimgento\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Ftp
 * @package Project\Pimgento\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class Ftp implements ArrayInterface
{
    /**
     * Options getter
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'ftp', 'label' => __('Ftp')], ['value' => 'sftp', 'label' => __('Sftp')]];
    }

    /**
     * Get options in "key-value" format
     * @return array
     */
    public function toArray()
    {
        return ['ftp' => __('Ftp'), 'sftp' => __('Sftp')];
    }
}
