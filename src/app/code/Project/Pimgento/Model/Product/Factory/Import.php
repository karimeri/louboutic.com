<?php

namespace Project\Pimgento\Model\Product\Factory;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Link;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface as scopeConfig;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Module\Manager as moduleManager;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Notification\NotifierInterface;
use Magento\Rma\Helper\Data as RmaHelper;
use Magento\Catalog\Model\CategoryFactory as CategoryFactory;
use Pimgento\Entities\Model\Entities;
use Pimgento\Import\Helper\Config as helperConfig;
use Pimgento\Import\Helper\Serializer as Json;
use Pimgento\Import\Helper\UrlRewrite as urlRewriteHelper;
use Pimgento\Log\Api\Data\LogInterface;
use Pimgento\Log\Model\Log as LogModel;
use Pimgento\Product\Helper\Config as productHelper;
use Pimgento\Product\Helper\Media as mediaHelper;
use Pimgento\Product\Model\Factory\Import as BaseImport;
use Pimgento\Product\Model\Factory\Import\Media;
use Pimgento\Product\Model\Factory\Import\Related;
use Project\Pimgento\Model\Product\ProductRepository;
use Project\Pimgento\Model\PimgentoRepository;
use Project\Pimgento\Helper\Config as PimgentoHelper;
use Project\Pimgento\Helper\Reporting as ReportingHelper;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Pimgento\Model\Store\WebsiteRepository;
use Project\Pimgento\Model\Product\Factory\Import\Media as PimgentoMedia;
use Project\Pimgento\Model\QueryRegistry;
use \Zend_Db_Expr as Expr;

/**
 * Class Import
 * @SuppressWarnings(PHPMD)
 * @package Project\Pimgento\Model\Product\Factory
 * @author Synolia <contact@synolia.com>
 */
class Import extends BaseImport
{
    const ENTITY_ID = 'entity_id';
    const ERROR_PRICE = 'Some products do not have a price';

    /**
     * @var bool|array
     */
    protected $websites = false;

    /**
     * @var bool|array
     */
    protected $websitesWithStores = false;

    /**
     * @var \Project\Pimgento\Model\PimgentoRepository
     */
    protected $pimgentoRepository;

    /**
     * @var \Project\Pimgento\Helper\Config
     */
    protected $pimgentoHelper;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Project\Pimgento\Model\Store\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @var string
     */
    protected $tmpTable;

    /**
     * @var \Project\Pimgento\Model\Product\Factory\Import\Media
     */
    protected $pimgentoMedia;
    /**
     * @var \Magento\Rma\Helper\Data
     */
    protected $rmaHelper;

    /**
     * @var \Project\Pimgento\Model\QueryRegistry
     */
    protected $queryRegistry;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Project\Pimgento\Helper\Reporting
     */
    protected $reportingHelper;

    /**
     * @var \Magento\Framework\Notification\NotifierInterface
     */
    protected $notifierPool;

    /**
     * @var \Pimgento\Log\Model\Log
     */
    protected $log;

    /**
     * Import constructor.
     * @param helperConfig $helperConfig
     * @param ManagerInterface $eventManager
     * @param moduleManager $moduleManager
     * @param scopeConfig $scopeConfig
     * @param Entities $entities
     * @param TypeListInterface $cacheTypeList
     * @param SetFactory $attributeSetFactory
     * @param productHelper $productHelper
     * @param mediaHelper $mediaHelper
     * @param urlRewriteHelper $urlRewriteHelper
     * @param Related $related
     * @param Media $media
     * @param Product $product
     * @param Json $serializer
     * @param PimgentoRepository $pimgentoRepository
     * @param PimgentoHelper $pimgentoHelper
     * @param EnvironmentManager $environmentManager
     * @param WebsiteRepository $websiteRepository
     * @param PimgentoMedia $pimgentoMedia
     * @param RmaHelper $rmaHelper
     * @param QueryRegistry $queryRegistry
     * @param CategoryFactory $categoryFactory
     * @param ReportingHelper $reportingHelper
     * @param NotifierInterface $notifier
     * @param LogModel $log
     * @param array $data
     */
    public function __construct(
        helperConfig $helperConfig,
        ManagerInterface $eventManager,
        moduleManager $moduleManager,
        scopeConfig $scopeConfig,
        Entities $entities,
        TypeListInterface $cacheTypeList,
        SetFactory $attributeSetFactory,
        productHelper $productHelper,
        mediaHelper $mediaHelper,
        urlRewriteHelper $urlRewriteHelper,
        Related $related,
        Media $media,
        Product $product,
        Json $serializer,
        PimgentoRepository $pimgentoRepository,
        PimgentoHelper $pimgentoHelper,
        EnvironmentManager $environmentManager,
        WebsiteRepository $websiteRepository,
        PimgentoMedia $pimgentoMedia,
        RmaHelper $rmaHelper,
        QueryRegistry $queryRegistry,
        CategoryFactory $categoryFactory,
        ReportingHelper $reportingHelper,
        NotifierInterface $notifier,
        LogModel $log,
        array $data = []
    ) {
        parent::__construct(
            $helperConfig,
            $eventManager,
            $moduleManager,
            $scopeConfig,
            $entities,
            $cacheTypeList,
            $attributeSetFactory,
            $productHelper,
            $mediaHelper,
            $urlRewriteHelper,
            $related,
            $media,
            $product,
            $serializer,
            $data
        );
        $this->pimgentoRepository = $pimgentoRepository;
        $this->pimgentoHelper = $pimgentoHelper;
        $this->environmentManager = $environmentManager;
        $this->websiteRepository = $websiteRepository;
        $this->connection = $this->_entities->getResource()->getConnection();
        $this->pimgentoMedia = $pimgentoMedia;
        $this->rmaHelper = $rmaHelper;
        $this->queryRegistry = $queryRegistry;
        $this->categoryFactory = $categoryFactory;
        $this->reportingHelper = $reportingHelper;
        $this->notifierPool = $notifier;
        $this->log = $log;
    }

    /**
     * Set website
     */
    public function setWebsites()
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        if ($this->connection->tableColumnExists($tmpTable, 'sites')) {
            $this->pimgentoRepository->associateToWebsites(
                $this->getEntityIdsWith('sites'),
                $this->getWebsites()
            );
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function enableToWebsites()
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        if ($this->connection->tableColumnExists($tmpTable, 'product_status')) {
            $this->pimgentoRepository->enableToWebsites(
                $this->getEntityIdsWith('product_status'),
                Product::ENTITY,
                $this->getWebsitesWithStores()
            );
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function enableSpecificAttributes()
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        if ($attributes = $this->pimgentoHelper->getSpecificAttributesToActivate()) {
            $attributeIds = $this->getAttributeIds(array_values($attributes));
            foreach ($attributes as $attribute) {
                if ($this->connection->tableColumnExists($tmpTable, $attribute)) {
                    $this->pimgentoRepository->enableSpecificAttribute(
                        $this->getEntityIdsWith($attribute),
                        $attributeIds[$attribute],
                        Product::ENTITY,
                        $this->getWebsitesWithStores()
                    );
                }
            }
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function initStock()
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        $select = $this->connection->select()->from(
            $tmpTable,
            [self::ENTITY_ID => '_entity_id', 'sites', 'type_id' => '_type_id']
        );

        $this->pimgentoRepository->initStock(
            $this->queryRegistry->processFetchAll($this->connection, $select, \PDO::FETCH_GROUP | \PDO::FETCH_UNIQUE),
            $this->getWebsites()
        );
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function disableRmaEligibility()
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        $attributSet = $this->pimgentoHelper->getAttributSetForDisableRma();
        if ($attributSet && $this->connection->tableColumnExists($tmpTable, 'family')) {
            $this->pimgentoRepository->disableRmaEligibility(
                $this->getEntityIdsWith('family', ['family' => $attributSet])
            );
            $this->pimgentoRepository->disableRmaEligibility(
                $this->getEntityIdsWith('family', ['family' => $attributSet], true),
                $this->rmaHelper->isEnabled() ? 2 : 1
            );
        }
    }

    /**
     * @param $field
     * @param array $where
     * @param bool $invert
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getEntityIdsWith($field, $where = [], $invert = false)
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        $select = $this->connection->select()->from($tmpTable, [self::ENTITY_ID => '_entity_id', $field]);

        if (count($where)) {
            foreach ($where as $field => $value) {
                $condIn = $invert ? "$field NOT IN (?)" : "$field IN (?)";
                $condEqual = $invert ? "$field != ?" : "$field = ?";
                $cond = is_array($value) ? $condIn : $condEqual;
                $select->where($cond, $value);
            }
        }

        return $this->queryRegistry->processFetchAll($this->connection, $select, \PDO::FETCH_KEY_PAIR);
    }

    /**
     * Set product related (related, upsell, x_sell, xrossell)
     */
    public function setRelated()
    {
        $this->_related->setCode($this->getCode());

        $related = [];
        $environment = strtoupper($this->environmentManager->getEnvironment());
        $tmpTable = $this->_entities->getTableName($this->getCode());

        $data = [
            "RELATED_$environment-" => Link::LINK_TYPE_RELATED,
            "UPSELL_$environment-" => Link::LINK_TYPE_UPSELL,
            "X_SELL_$environment-" => Link::LINK_TYPE_CROSSSELL,
            "CROSSSELL_$environment-" => Link::LINK_TYPE_CROSSSELL,
        ];

        foreach ($data as $suffix => $typeId) {
            foreach (['products', 'product_models'] as $prefix) {
                $column = $suffix . $prefix;
                if ($this->connection->tableColumnExists($tmpTable, $column)) {
                    $related[] = [
                        'type_id' => $typeId,
                        'column' => $column,
                    ];
                }
            }
        }

        $this->_related->relatedCreateTmpTables();
        foreach ($related as $type) {
            $this->_related->relatedImportColumn($type);
        }
        $this->_related->relatedDropTmpTables();
    }

    /**
     * Set values to attributes
     */
    public function setValues() //@codingStandardsIgnoreLine Generic.Metrics.CyclomaticComplexity.TooHigh
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());

        $environment = $this->environmentManager->getEnvironment();

        $resource = $this->_entities->getResource();

        $stores = array_merge(
            $this->_helperConfig->getStores(['lang']), // en_US
            $this->_helperConfig->getStores(['lang', 'channel_code']), // en_US-channel
            $this->_helperConfig->getStores(['channel_code']), // channel
            $this->_helperConfig->getStores(['currency']), // USD
            $this->_helperConfig->getStores(['channel_code', 'currency']), // channel-USD
            $this->_helperConfig->getStores(['lang', 'channel_code', 'currency']) // en_US-channel-USD
        );

        $columns = array_keys($this->connection->describeTable($tmpTable));
        $excludedAttributes = $this->pimgentoHelper->getProductAttributesExcluded();

        $except = array_merge(
            [
                '_entity_id',
                '_is_new',
                '_status',
                '_type_id',
                '_options_container',
                '_tax_class_id',
                '_attribute_set_id',
                '_visibility',
                '_children',
                '_axis',
                'sku',
                'categories',
                'family',
                'groups',
                'parent',
                'enabled'
            ],
            $excludedAttributes
        );

        $values = [
            0 => [
                'options_container' => '_options_container',
                'tax_class_id' => '_tax_class_id',
                'visibility' => '_visibility',
            ]
        ];

        if (!in_array($environment, [Environment::EU, Environment::JP])) {
            unset($values[0]['tax_class_id']);
        }

        if ($this->connection->tableColumnExists($tmpTable, 'enabled')) {
            $values[0]['status'] = '_status';
        }

        if (in_array($environment, [Environment::EU, Environment::JP])) {
            $taxClasses = $this->_productHelper->getProductTaxClasses();
            if (count($taxClasses)) {
                foreach ($taxClasses as $storeId => $taxClassId) {
                    $values[$storeId]['tax_class_id'] = new Expr($taxClassId);
                }
            }
        }

        foreach ($columns as $column) {
            $columnPrefix = explode('-', $column);
            $columnPrefix = reset($columnPrefix);

            if (in_array($column, $except) ||
                in_array($columnPrefix, $except) ||
                preg_match('/-unit/', $column)
            ) {
                continue;
            }

            foreach ($stores as $suffix => $affected) {
                if (preg_match('/^' . $columnPrefix . '-' . $suffix . '$/', $column)) {
                    foreach ($affected as $store) {
                        if (!isset($values[$store['store_id']])) {
                            $values[$store['store_id']] = [];
                        }
                        $values[$store['store_id']][$columnPrefix] = $column;
                    }
                }
            }

            if (!isset($values[0][$columnPrefix])) {
                $values[0][$columnPrefix] = $column;
            }
        }

        foreach ($values as $storeId => $data) {
            $this->_entities->setValues(
                $this->getCode(),
                $resource->getTable('catalog_product_entity'),
                $data,
                4,
                $storeId,
                AdapterInterface::INSERT_ON_DUPLICATE
            );
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function disableProductByPrice()
    {
        $tmpTable = $this->_entities->getTableName($this->getCode());
        if ($this->connection->tableColumnExists($tmpTable, 'product_status')) {
            $select = $this->connection->select()->from(
                $tmpTable,
                [self::ENTITY_ID => '_entity_id', 'product_status', 'sku', 'reactivated']
            );

            $result = $this->pimgentoRepository->disableProductByPrice(
                $this->queryRegistry->processFetchAll($this->connection, $select,
                    \PDO::FETCH_GROUP | \PDO::FETCH_UNIQUE),
                $this->getWebsitesWithStores()
            );

            if (count($result)) {
                $this->setData('warning', [
                    [
                        'message' => self::ERROR_PRICE,
                        'data' => $result
                    ]
                ]);
            }
        }
    }

    /**
     * @return array|bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getWebsites()
    {
        if (!$this->websites) {
            $this->websites = $this->websiteRepository->getWebsites();
        }
        return $this->websites;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getWebsitesWithStores()
    {
        if (!$this->websitesWithStores) {
            $this->websitesWithStores = $this->websiteRepository->getWebsitesWithStores();
        }
        return $this->websitesWithStores;
    }

    /**
     * @param array $attributes
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getAttributeIds(array $attributes)
    {
        $select = $this->connection->select()
            ->from(
                $this->_entities->getResource()->getTable('pimgento_entities'),
                ['code', 'entity_id']
            )
            ->where('import = ?', 'attribute')
            ->where('code IN (?)', $attributes);
        //@codingStandardsIgnoreLine Ecg.Performance.FetchAll.Found
        return $this->queryRegistry->processFetchAll($this->connection, $select, \PDO::FETCH_KEY_PAIR);
    }

    /**
     * Set categories
     */
    public function setCategories()
    {
        $resource = $this->_entities->getResource();
        $connection = $resource->getConnection();
        $tmpTable = $this->_entities->getTableName($this->getCode());

        if (!$connection->tableColumnExists($tmpTable, 'categories')) {
            $this->setStatus(false);
            $this->setMessage(
                __('Column categories not found')
            );
        } else {

            $select = $connection->select()
                ->from(
                    array(
                        'c' => $resource->getTable('pimgento_entities')
                    ),
                    array(
                        'position' => new Expr('184'),
                    )
                )
                ->joinInner(
                    array('p' => $tmpTable),
                    'FIND_IN_SET(`c`.`code`, `p`.`categories`) AND `c`.`import` = "category"',
                    array(
                        'category_id' => 'c.entity_id',
                        'product_id' => 'p._entity_id'
                    )
                )
                ->joinInner(
                    array('e' => $resource->getTable('catalog_category_entity')),
                    'c.entity_id = e.entity_id',
                    array()
                );

            $productsToInsert = $connection->fetchAll($select);
            $categoryIds = array_unique(array_column($productsToInsert, 'category_id'));
            $productsToInsertWithPositions = [];

            foreach ($categoryIds as $categoryId) {
                $categoryProductCount = $this->categoryFactory->create()->load($categoryId)->getProductCount();

                $productsToInsertByCategory = array_filter($productsToInsert,
                    function ($v) use ($categoryId) {
                        return $v['category_id'] == $categoryId;
                    });

                foreach ($productsToInsertByCategory as $key => $product) {
                    $productsToInsertByCategory[$key]['position'] = $categoryProductCount;
                    $categoryProductCount++;
                }

                $productsToInsertWithPositions = array_merge($productsToInsertWithPositions,
                    $productsToInsertByCategory);
            }

            $connection->insertOnDuplicate(
                $resource->getTable('catalog_category_product'),
                $productsToInsertWithPositions
            );

            //Remove product from old categories
            $selectToDelete = $connection->select()
                ->from(
                    array(
                        'c' => $resource->getTable('pimgento_entities')
                    ),
                    array()
                )
                ->joinInner(
                    array('p' => $tmpTable),
                    '!FIND_IN_SET(`c`.`code`, `p`.`categories`) AND `c`.`import` = "category"',
                    array(
                        'category_id' => 'c.entity_id',
                        'product_id' => 'p._entity_id'
                    )
                )
                ->joinInner(
                    array('e' => $resource->getTable('catalog_category_entity')),
                    'c.entity_id = e.entity_id',
                    array()
                );

            $connection->delete($resource->getTable('catalog_category_product'),
                '(category_id, product_id) IN (' . $selectToDelete->assemble() . ')');
        }
    }

    /**
     * @throws \Exception
     */
    public function sendReport()
    {
        $reportData = $this->pimgentoRepository->getReportData();
        if ($reportData) {
            $nbKO = 0;
            $nbTotal = count($reportData);
            foreach ($reportData as $reportDatum) {
                if ($reportDatum['update'] == 'KO') {
                    $nbKO++;
                }
            }
            $this->reportingHelper->sendReport();
            $message = $nbKO > 0 ? sprintf('%s product(s) status update out of %d have failed.', $nbKO, $nbTotal) :
                'product(s) status update OK.';
            $this->notifierPool->addMajor('Pimgento Product Import', $message);
            $this->log->load($this->getIdentifier(), LogInterface::IDENTIFIER);
            $this->log->addStep(
                array(
                    'log_id' => $this->log->getId(),
                    'identifier' => $this->getIdentifier(),
                    'number' => $this->getStep(),
                    'method' => $this->getMethod(),
                    'message' => $this->getPrefix() . '[report] ' . $message,
                    'continue' => $this->getContinue() ? 1 : 0,
                    'status' => $this->getStatus() ? 1 : 0
                )
            );
        }
    }
}
