<?php

namespace Project\Pimgento\Model\Product\Factory\Import;

use Louboutin\Cdn\Model\PurgeManager;
use Magento\Catalog\Model\Product\Image;
use Magento\Framework\App\Config\ScopeConfigInterface as scopeConfig;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Module\Manager as moduleManager;
use Pimgento\Entities\Model\Entities;
use Pimgento\Import\Helper\Config as helperConfig;
use Project\Pimgento\Helper\Config as CustomHelperConfig;
use Pimgento\Product\Helper\Media as mediaHelper;
use Pimgento\Product\Model\Factory\Import\Media as BaseMedia;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Psr\Log\LoggerInterface;
use Zend_Db_Expr as Expr;

/**
 * Class Media
 * @package Project\Pimgento\Model\Product\Factory\Import
 * @author Synolia <contact@synolia.com>
 */
class Media extends BaseMedia
{
    /**
     * @var CustomHelperConfig
     */
    protected $customHelperConfig;

    /**
     * @var PurgeManager
     */
    protected $purgeManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Media constructor.
     * @param helperConfig $helperConfig
     * @param ManagerInterface $eventManager
     * @param moduleManager $moduleManager
     * @param scopeConfig $scopeConfig
     * @param Entities $entities
     * @param mediaHelper $mediaHelper
     * @param Image $image
     * @param CustomHelperConfig $customHelperConfig
     * @param PurgeManager $purgeManager
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        helperConfig $helperConfig,
        ManagerInterface $eventManager,
        moduleManager $moduleManager,
        scopeConfig $scopeConfig,
        Entities $entities,
        mediaHelper $mediaHelper,
        Image $image,
        CustomHelperConfig $customHelperConfig,
        PurgeManager $purgeManager,
        LoggerInterface $logger,
        array $data = []
    ) {
        parent::__construct(
            $helperConfig,
            $eventManager,
            $moduleManager,
            $scopeConfig,
            $entities,
            $mediaHelper,
            $image,
            $data
        );
        $this->customHelperConfig = $customHelperConfig;
        $this->purgeManager = $purgeManager;
        $this->logger = $logger;
    }

    /***
     * Disable Gallery Images
     */
    public function disableGalleryImages()
    {

        $resource     = $this->_entities->getResource();
        $connection   = $resource->getConnection();
        $tableMedia   = $this->_entities->getTableName('media');
        $tableGallery = $resource->getTable('catalog_product_entity_media_gallery_value');
        $identifier   = $this->_entities->getColumnIdentifier($tableGallery);
        $maxId        = $this->mediaGetMaxId($tableGallery, 'record_id');
        $step         = 5000;

        for ($k = 1; $k <= $maxId; $k += $step) {
            $min = $k;
            $max = $k + $step;

            $values = ["$tableMedia.record_id" => new Expr("$tableGallery.record_id")];
            $where  = [
                "$tableGallery.record_id >= ?"  => $min,
                "$tableGallery.record_id < ?"   => $max,
                "$tableGallery.$identifier = ?" => new Expr("$tableMedia.entity_id"),
                "$tableGallery.value_id = ?"    => new Expr("$tableMedia.value_id"),
                "$tableGallery.store_id = ?"    => new Expr("$tableMedia.store_id"),
            ];

            $connection->update(
                new Expr("$tableMedia, $tableGallery"),
                $values,
                $where
            );
        }

        $select = $connection->select()
            ->from(
                ['t' => $tableMedia],
                [
                    'record_id' => 'record_id',
                    'value_id'  => 'value_id',
                    'store_id'  => new Expr('0'),
                    $identifier => 'entity_id',
                    'disabled'  => new Expr('IF (attribute_id IS NULL, 0, 1)'),
                ]
            )
            ->where('t.record_id IS NOT NULL')
            ->order('t.attribute_id desc');

        $query  = $connection->insertFromSelect(
            $select,
            $tableGallery,
            ['record_id', 'value_id', 'store_id', $identifier, 'disabled'],
            AdapterInterface::INSERT_ON_DUPLICATE
        );

        $connection->query($query);
    }

    /**
     * Copy the medias to the media folder of magento
     *
     * @return void
     */
    public function mediaCopyFiles()
    {
        $connection = $this->_entities->getResource()->getConnection();
        $tableMedia = $this->_entities->getTableName('media');

        $maxId = $this->mediaGetMaxId($tableMedia, 'id');
        if ($maxId < 1) {
            return;
        }

        $step = 5000;
        for ($k = 1; $k <= $maxId; $k += $step) {
            $min = $k;
            $max = $k + $step;
            $select = $connection->select()
                ->from(
                    ['t' => $tableMedia],
                    [
                        'from' => 'media_original',
                        'to'   => 'media_value',
                    ]
                )->where(
                    "id >= $min AND id < $max"
                );
            $medias = $connection->fetchAll($select);
            foreach ($medias as $media) {
                $from = rtrim($this->_mediaHelper->getImportFolder(), '/').'/'.ltrim($media['from'], '/');
                $to = rtrim($this->_mediaHelper->getMediaAbsolutePath(), '/').'/'.ltrim($media['to'], '/');
                $cleanCache = false;

                // if it does not exist, we pass
                if (!is_file($from)) {
                    continue;
                }

                // create the final folder
                if (!is_dir(dirname($to))) {
                    mkdir(dirname($to), 0775, true);
                }

                // remove the file if it exist
                if (is_file($to)) {
                    // Louboutin custom : remove file even if same name
                    if (md5_file($from) === md5_file($to)) {
                        $cleanCache = true;
                    }
                    unlink($to);
                }

                copy($from, $to);

                // clean the cache only if we need to
                if ($cleanCache) {
                    $this->clearImageCache($media['to']);
                }
            }
        }
    }

    /**
     * Clear image resize cache for every defined types
     *
     * @param string $imageFile
     */
    protected function clearImageCache($imageFile)
    {
        foreach ($this->_mediaHelper->getImageResizeTypes() as $resizeType) {
            if (array_key_exists('type', $resizeType)) {
                $this->image->setDestinationSubdir($resizeType['type']);
            }

            // Used for the size folder
            if (array_key_exists('width', $resizeType)) {
                $this->image->setWidth($resizeType['width']);
            }
            if (array_key_exists('height', $resizeType)) {
                $this->image->setHeight($resizeType['height']);
            }

            // Those parameters are used to calculate the hash in the file path
            if (array_key_exists('aspect_ratio', $resizeType)) {
                $this->image->setKeepAspectRatio($resizeType['aspect_ratio']);
            }
            if (array_key_exists('frame', $resizeType)) {
                $this->image->setKeepFrame($resizeType['frame']);
            }
            if (array_key_exists('transparency', $resizeType)) {
                $this->image->setKeepTransparency($resizeType['transparency']);
            }
            if (array_key_exists('constrain', $resizeType)) {
                $this->image->setConstrainOnly($resizeType['constrain']);
            }
            if (array_key_exists('background', $resizeType)) {
                $this->image->setBackgroundColor($resizeType['background']);
            }

            // Those does not exists in view.xsd ; only keeping them for reference, and maybe future usage ?
            if (array_key_exists('quality', $resizeType)) {
                $this->image->setQuality($resizeType['quality']);
            }
            if (array_key_exists('angle', $resizeType)) {
                $this->image->setAngle($resizeType['angle']);
            }

            $this->image->setBaseFile($imageFile);
            $imagePath = $this->image->getNewFile();
            $imageCdnPath = preg_replace('/\/app\/pub/', '', $imagePath);
            // Delete image in cache
            $this->_mediaHelper->deleteMediaFile($imagePath);
            // Purge image in cdn
            if ($this->customHelperConfig->getPurgeCdnImage()) {
                try {
                    $this->purgeManager->purge($imageCdnPath);
                } catch (\Exception $e) {
                    $this->logger->error($e->getMessage());
                }
            }
        }
    }
}
