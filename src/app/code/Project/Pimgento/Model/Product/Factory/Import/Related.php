<?php

namespace Project\Pimgento\Model\Product\Factory\Import;

use Pimgento\Product\Model\Factory\Import\Related as BaseRelated;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Zend_Db_Expr as Expr;
use Magento\Catalog\Model\Product\Link;

/**
 * Class Related
 * @package Project\Pimgento\Model\Product\Factory\Import
 * @author Synolia <contact@synolia.com>
 */
class Related extends BaseRelated
{
    /**
     * @var array
     */
    protected $productIds = [];

    /**
     * @var bool|\Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection = false;

    /**
     * @var bool|\Magento\Framework\Model\ResourceModel\Db\AbstractDb
     */
    protected $resource = false;

    /**
     * @param array $type
     * @return bool
     * @throws \Zend_Db_Statement_Exception
     */
    public function relatedImportColumn($type)
    {
        $resource     = $this->getResource();
        $connection   = $this->getConnection();
        $tmpTable     = $this->_entities->getTableName($this->getCode());
        $tableNumber  = $this->_entities->getTableName('numbers');
        $tableRelated = $this->_entities->getTableName('related');
        $tableProduct = $resource->getTable('catalog_product_entity');

        $this->addColumns($tableRelated);

        $column = $type['column'];

        $ids = $this->getMaxAndMinIds($column);
        if ($ids['max'] < 1) {
            return false;
        }

        $childSku = new Expr("substring_index(substring_index(substring_index(t.`$column`,',',n.n),',',-1),'|',1)");
        $position = new Expr("substring_index(substring_index(substring_index(t.`$column`,',',n.n),',',-1),'|',-1)");

        // We must do this step by step because of a mysql usage limitation
        $step = 1000;
        $min = $ids['min'] + $step;
        $max = $ids['max'] + $step;
        for ($limit = $min; $limit <= $max; $limit += $step) {
            $select = $connection->select()
                ->from(
                    ['t' => $tmpTable],
                    ['parent_sku' => 't.sku']
                )
                ->joinInner(
                    ['n' => $tableNumber],
                    "
                        `t`.`$column` <> ''
                        AND `t`.`_entity_id` <= $limit
                        AND (char_length(t.`$column`) - char_length(replace(t.`$column`, ',', ''))) >= n.n-1
                    ",
                    [
                        'child_sku' => $childSku,
                        'position'  => $position
                    ]
                );

            $connection->query(
                $connection->insertFromSelect(
                    $select,
                    $tableRelated,
                    ['parent_sku', 'child_sku', 'position'],
                    AdapterInterface::INSERT_ON_DUPLICATE
                )
            );

            $connection->update(
                $tmpTable,
                [$column => ''],
                ["_entity_id <= $limit" ]
            );
        }

        // Get the product ids for parents
        $query = $connection->select()
            ->from(false, ['parent_id' => 'p.'.$this->_entities->getColumnIdentifier($tableProduct)])
            ->joinLeft(
                ['p' => $tableProduct],
                'r.parent_sku = p.sku',
                []
            );

        $connection->query(
            $connection->updateFromSelect($query, ['r' => $tableRelated])
        );

        // Get the product ids for links
        $query = $connection->select()
            ->from(false, ['child_id' => 'p.'.$this->_entities->getColumnIdentifier($tableProduct)])
            ->joinLeft(
                ['p' => $tableProduct],
                'r.child_sku = p.sku',
                []
            );

        $connection->query(
            $connection->updateFromSelect($query, ['r' => $tableRelated])
        );

        // Delete bad links
        $connection->delete($tableRelated, 'child_id IS NULL or parent_id IS NULL');

        $this->deleteRelated($tableRelated); // Delete related
        $this->insertLinks($tableRelated, $type); // Save the links
        $this->insertPosition($tableRelated, $type); // Save position
        $this->setProductIds($tableRelated);

        $connection->delete($tableRelated);

        return true;
    }

    /**
     * @param $tableRelated
     */
    protected function deleteRelated($tableRelated)
    {
        $this->getConnection()->query(
            $this->getConnection()->select()
                ->from(
                    ['link' => $this->getResource()->getTable('catalog_product_link')]
                )->join(
                    ['related' => $tableRelated],
                    'related.parent_id=link.product_id',
                    ['product_id']
                )
                ->where('link.product_id NOT IN (?)', $this->productIds)
                ->deleteFromSelect('link')
        );
    }

    /**
     * @param $tableRelated
     * @param array $type
     */
    protected function insertLinks($tableRelated, array $type)
    {
        $select = $this->getConnection()->select()
            ->from(
                ['l' => $tableRelated],
                [
                    'product_id'        => 'l.parent_id',
                    'linked_product_id' => 'l.child_id',
                    'link_type_id'      => new Expr($type['type_id'])
                ]
            );

        $query = $this->getConnection()->insertFromSelect(
            $select,
            $this->getResource()->getTable('catalog_product_link'),
            ['product_id', 'linked_product_id', 'link_type_id'],
            AdapterInterface::INSERT_ON_DUPLICATE
        );
        $this->getConnection()->query($query);
    }

    /**
     * @param $tableRelated
     */
    protected function addColumns($tableRelated)
    {
        $this->getConnection()->addColumn(
            $tableRelated,
            'position',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => false,
                'comment'  => 'Position'
            ]
        );
    }

    /**
     * @param $tableRelated
     * @param array $type
     * @throws \Zend_Db_Statement_Exception
     */
    protected function insertPosition($tableRelated, array $type)
    {
        $select = $this->getConnection()->select()
            ->from(
                ['related' => $tableRelated],
                [
                    'product_link_attribute_id' => new Expr(
                        $this->getProductLinkAttributId('position', $type['type_id'])
                    )
                ]
            )->join(
                ['link' => $this->getResource()->getTable('catalog_product_link')],
                'related.parent_id=link.product_id AND related.child_id=link.linked_product_id AND link.link_type_id=4',
                ['link_id', 'value' => 'related.position']
            );

        $query = $this->getConnection()->insertFromSelect(
            $select,
            $this->getResource()->getTable('catalog_product_link_attribute_int'),
            ['product_link_attribute_id', 'link_id', 'value'],
            AdapterInterface::INSERT_ON_DUPLICATE
        );
        $this->getConnection()->query($query);
    }

    /**
     * @param $tableRelated
     * @throws \Zend_Db_Statement_Exception
     */
    protected function setProductIds($tableRelated)
    {
        $select = $this->getConnection()->select()
            ->from(
                ['link' => $this->getResource()->getTable('catalog_product_link')],
                ['link.product_id']
            )->join(
                ['related' => $tableRelated],
                'related.parent_id=link.product_id'
            )->group('link.product_id'); //@codingStandardsIgnoreLine Ecg.Sql.SlowQuery.SlowSql

        $productIds = $this->getConnection()->query($select)->fetchAll(\PDO::FETCH_COLUMN); //@codingStandardsIgnoreLine Ecg.Performance.FetchAll.Found
        $this->productIds = array_merge($this->productIds, $productIds);
    }

    /**
     * @param $linkAttributCode
     * @param $linkTypeId
     * @return mixed
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getProductLinkAttributId($linkAttributCode, $linkTypeId)
    {
        $select = $this->getConnection()->select()
            ->from(
                $this->getResource()->getTable('catalog_product_link_attribute'),
                ['product_link_attribute_id']
            )
            ->where('link_type_id = ?', $linkTypeId)
            ->where('product_link_attribute_code = ?', $linkAttributCode);

        return $this->getConnection()->query($select)->fetchColumn();
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->getResource()->getConnection();
        }
        return $this->connection;
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb
     */
    protected function getResource()
    {
        if (!$this->resource) {
            $this->resource = $this->_entities->getResource();
        }
        return $this->resource;
    }
}
