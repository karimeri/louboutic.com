<?php

namespace Project\Pimgento\Model;

use Magento\Framework\DB\Select;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Class QueryRegistry
 * @package Project\Pimgento\Model
 * @author Synolia <contact@synolia.com>
 */
class QueryRegistry
{
    /**
     * @var array
     */
    protected $resultQueries = [];

    /**
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
     * @param \Magento\Framework\DB\Select $select
     * @param null $style
     * @return mixed
     * @throws \Zend_Db_Statement_Exception
     */
    public function processFetchAll(AdapterInterface $connection, Select $select, $style = null)
    {
        $selectBase64 = base64_encode($select);
        if (!empty($this->resultQueries[$selectBase64])) {
            return $this->resultQueries[$selectBase64];
        }
        //@codingStandardsIgnoreLine Ecg.Performance.FetchAll.Found
        $this->resultQueries[$selectBase64] = $connection->query($select)->fetchAll($style);
        return $this->resultQueries[$selectBase64];
    }
}
