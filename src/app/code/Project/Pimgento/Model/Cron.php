<?php

namespace Project\Pimgento\Model;

use Project\Pimgento\Api\Data\CronInterface;
use Magento\Framework\Model\AbstractModel;
use Project\Pimgento\Model\ResourceModel\Cron as ResourceCron;

/**
 * Class Cron
 * @package Project\Pimgento\Model
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class Cron extends AbstractModel implements CronInterface
{
    /**
     * @var string
     */
    protected $_idFieldName = self::CRON_ID; //@codingStandardsIgnoreLine
    
    /**
     * Initialize resource model
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(ResourceCron::class);
    }

    /**
     * Get Import Type
     * @return string
     */
    public function getImportType()
    {
        return $this->getData(self::IMPORT_TYPE);
    }

    /**
     * Get status
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Get order
     * @return int
     */
    public function getOrder()
    {
        return $this->getData(self::ORDER);
    }

    /**
     * Get File
     * @return int
     */
    public function getFile()
    {
        return $this->getData(self::FILE);
    }

    /**
     * Get Zip File
     * @return int
     */
    public function getZipFile()
    {
        return $this->getData(self::ZIP_FILE);
    }

    /**
     * Get execution ID
     * @return int
     */
    public function getExecutionId()
    {
        return $this->getData(self::EXECUTION_ID);
    }

    /**
     * Get execution ID
     * @return null|string
     */
    public function getAction()
    {
        return $this->getData(self::ACTION);
    }

    /**
     * Set Import Type
     * @param string $importType
     * @return CronInterface
     */
    public function setImportType($importType)
    {
        return $this->setData(self::IMPORT_TYPE, $importType);
    }

    /**
     * Set status
     * @param string $status
     * @return CronInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Set order
     * @param string $order
     * @return CronInterface
     */
    public function setOrder($order)
    {
        return $this->setData(self::ORDER, $order);
    }

    /**
     * Set File
     * @param string $file
     * @return CronInterface
     */
    public function setFile($file)
    {
        return $this->setData(self::FILE, $file);
    }

    /**
     * Set Zip File
     * @param null|string $zipFile
     * @return CronInterface
     */
    public function setZipFile($zipFile)
    {
        return $this->setData(self::ZIP_FILE, $zipFile);
    }


    /**
     * Set execution ID
     * @param string $executionId
     * @return CronInterface
     */
    public function setExecutionId($executionId)
    {
        return $this->setData(self::EXECUTION_ID, $executionId);
    }

    /**
     * Set action
     * @param string $action
     * @return CronInterface
     */
    public function setAction($action)
    {
        return $this->setData(self::ACTION, $action);
    }
}
