<?php

namespace Project\Pimgento\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Project\Pimgento\Api\CronRepositoryInterface;
use Project\Pimgento\Api\Data\CronInterface;
use Project\Pimgento\Model\ResourceModel\Cron as ResourceCron;
use Project\Pimgento\Model\CronFactory;

/**
 * Class CronRepository
 * @package Project\Pimgento\Model
 * @author Synolia <contact@synolia.com>
 */
class CronRepository implements CronRepositoryInterface
{
    /**
     * @var ResourceCron
     */
    protected $resourceCron;

    /**
     * @var CronFactory
     */
    protected $cronFactory;

    /**
     * CronRepository constructor.
     * @param ResourceCron $resourceCron
     * @param CronFactory $cronFactory
     */
    public function __construct(
        ResourceCron $resourceCron,
        CronFactory $cronFactory
    ) {
        $this->resourceCron = $resourceCron;
        $this->cronFactory  = $cronFactory;
    }

    /**
     * @param CronInterface $cron
     * @return mixed|CronInterface
     * @throws CouldNotSaveException
     */
    public function save(CronInterface $cron)
    {
        try {
            $this->resourceCron->save($cron);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $cron;
    }

    /**
     * Load Cron
     * @param string $importType
     * @return Cron $cron
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByImportType($importType)
    {
        /** @var Cron $cron */
        $cron = $this->cronFactory->create();
        $this->resourceCron->load($cron, $importType, CronInterface::IMPORT_TYPE);
        if (!$cron->getId()) {
            throw new NoSuchEntityException(__('Cron with importType "%1" does not exist.', $importType));
        }
        return $cron;
    }

    /**
     * Load Cron
     * @param string $status
     * @return Cron $cron
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByStatus($status)
    {
        /** @var Cron $cron */
        $cron = $this->cronFactory->create();
        $this->resourceCron->load($cron, $status, CronInterface::STATUS);
        if (!$cron->getId()) {
            throw new NoSuchEntityException(__('Cron with status "%1" does not exist.', $status));
        }
        return $cron;
    }
}
