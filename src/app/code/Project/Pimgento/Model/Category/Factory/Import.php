<?php

namespace Project\Pimgento\Model\Category\Factory;

use Magento\Catalog\Model\Category;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface as scopeConfig;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Module\Manager as moduleManager;
use Pimgento\Category\Model\Factory\Import as BaseImport;
use Pimgento\Category\Helper\Config;
use Pimgento\Entities\Model\Entities;
use Pimgento\Import\Helper\Config as helperConfig;
use Pimgento\Import\Helper\UrlRewrite as urlRewriteHelper;
use Project\Pimgento\Model\PimgentoRepository;
use Project\Pimgento\Model\Store\WebsiteRepository;
use Project\Pimgento\Helper\Cache;

/**
 * Class Import
 * @SuppressWarnings(PHPMD)
 * @package Project\Pimgento\Model\Category\Factory
 * @author Synolia <contact@synolia.com>
 */
class Import extends BaseImport
{
    const ENTITY_ID = 'entity_id';

    /**
     * @var PimgentoRepository
     */
    protected $pimgentoRepository;

    /**
     * @var bool|array
     */
    protected $websitesWithStores = false;

    /**
     * @var \Project\Pimgento\Model\Store\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var \Project\Pimgento\Helper\Cache
     */
    protected $cacheHelper;

    /**
     * Lower case in the Class is normal, the parent is like that
     * Import constructor.
     * @param Entities $entities
     * @param helperConfig $helperConfig
     * @param moduleManager $moduleManager
     * @param scopeConfig $scopeConfig
     * @param ManagerInterface $eventManager
     * @param Category $category
     * @param TypeListInterface $cacheTypeList
     * @param urlRewriteHelper $urlRewriteHelper
     * @param PimgentoRepository $pimgentoRepository
     * @param \Project\Pimgento\Model\Store\WebsiteRepository $websiteRepository
     * @param \Project\Pimgento\Helper\Cache $cacheHelper
     * @param array $data
     */
    public function __construct(
        Entities $entities,
        helperConfig $helperConfig,
        moduleManager $moduleManager,
        scopeConfig $scopeConfig,
        ManagerInterface $eventManager,
        Category $category,
        TypeListInterface $cacheTypeList,
        urlRewriteHelper $urlRewriteHelper,
        PimgentoRepository $pimgentoRepository,
        WebsiteRepository $websiteRepository,
        Cache $cacheHelper,
        array $data = []
    ) {
        parent::__construct(
            $entities,
            $helperConfig,
            $moduleManager,
            $scopeConfig,
            $eventManager,
            $category,
            $cacheTypeList,
            $urlRewriteHelper,
            $data
        );
        $this->pimgentoRepository = $pimgentoRepository;
        $this->websiteRepository  = $websiteRepository;
        $this->cacheHelper        = $cacheHelper;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityExceptionL
     * @throws \Zend_Db_Statement_Exception
     */
    public function enableToWebsites()
    {
        $connection = $this->_entities->getResource()->getConnection();
        $tmpTable   = $this->_entities->getTableName($this->getCode());
        $select     = $connection->select()->from($tmpTable, [self::ENTITY_ID => '_entity_id', 'sites']);
        $categories = $connection->query($select)->fetchAll(\PDO::FETCH_KEY_PAIR); //@codingStandardsIgnoreLine Ecg.Performance.FetchAll.Found

        $this->pimgentoRepository->enableToWebsites($categories, Category::ENTITY, $this->getWebsitesWithStores(), 0);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    protected function getWebsitesWithStores()
    {
        if (!$this->websitesWithStores) {
            $this->websitesWithStores = $this->websiteRepository->getWebsitesWithStores();
        }
        return $this->websitesWithStores;
    }

    /**
     * Set categories Url Key
     */
    public function setUrlKey()
    {
        $connection = $this->_entities->getResource()->getConnection();
        $tmpTable   = $this->_entities->getTableName($this->getCode());

        $stores = $this->_helperConfig->getStores('lang');

        foreach ($stores as $local => $affected) {

            $keys = [];

            if ($connection->tableColumnExists($tmpTable, 'label-'.$local)) {

                $connection->addColumn($tmpTable, 'url_key-' . $local, [
                    'type' => 'text',
                    'length' => 255,
                    'default' => '',
                    'COMMENT' => ' ',
                    'nullable' => false
                ]);

                $select = $connection->select()
                    ->from(
                        $tmpTable,
                        ['entity_id' => '_entity_id', 'name' => 'label-'.$local, 'parent' => 'parent']
                    );

                $updateUrlKeyConfig = $this->_scopeConfig->getValue(Config::CONFIG_PIMGENTO_CATEGORY_UPDATE_URL_KEY);

                if (!$updateUrlKeyConfig) {
                    $select->where('_is_new = ?', 1);
                }

                $query = $connection->query($select);

                while (($row = $query->fetch())) {
                    $urlKey = $this->_category->formatUrlKey($row['name']);

                    // Start SYNOLIA
                    $finalKey             = $urlKey;
                    $increment            = 1;
                    $parentCategoryPrefix = $row['parent'].'-';
                    $categoryKey          = $parentCategoryPrefix.$finalKey;

                    while (in_array($finalKey, $keys)) {
                        $finalKey    = $urlKey.'-'.$increment++;
                        $categoryKey = $parentCategoryPrefix.$finalKey;
                    }

                    $keys[] = $categoryKey;
                    // End SYNOLIA

                    $connection->update(
                        $tmpTable, ['url_key-'.$local => $finalKey], ['_entity_id = ?' => $row['entity_id']]
                    );
                }

                if (!$updateUrlKeyConfig) {
                    $connection->update(
                        $tmpTable,
                        ['url_key-'.$local => \Pimgento\Entities\Model\ResourceModel\Entities::IGNORE_VALUE],
                        ['_is_new = ?' => 0]
                    );
                }
            }
        }
    }

    /**
     *
     */
    public function cleanCustomCache()
    {
        $this->cacheHelper->cleaCache('category');
    }
}
