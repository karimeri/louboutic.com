<?php

namespace Project\Pimgento\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Pimgento\Api\Data\CronInterface;

/**
 * Class Cron
 * @package Project\Pimgento\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class Cron extends AbstractDb
{
    /**
     * Initialize resource model
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(CronInterface::TABLE_NAME, CronInterface::CRON_ID);
    }
}
