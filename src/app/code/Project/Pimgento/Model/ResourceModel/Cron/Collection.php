<?php

namespace Project\Pimgento\Model\ResourceModel\Cron;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Pimgento\Model\Cron;
use Project\Pimgento\Model\ResourceModel\Cron as ResourceCron;

/**
 * Class Collection
 * @package Project\Pimgento\Model\ResourceModel\Cron
 * @author Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->_init(Cron::class, ResourceCron::class);
    }
}
