<?php

namespace Project\Pimgento\Model\Message;

/**
 * Class Report
 * @package Project\Pimgento\Model\Message
 */
class Report implements \Magento\Framework\Notification\MessageInterface
{
    /**
     * @var \Project\Pimgento\Model\Session
     */
    protected $session;

    /**
     * @var \Pimgento\Log\Model\ResourceModel\Log\CollectionFactory
     */
    protected $logCollectionFactory;

    /**
     * @var \Pimgento\Log\Model\ResourceModel\Log
     */
    protected $logResource;

    /**
     * Report constructor.
     * @param \Project\Pimgento\Model\Session $session
     * @param \Pimgento\Log\Model\ResourceModel\Log\CollectionFactory $collectionFactory
     * @param \Pimgento\Log\Model\ResourceModel\Log $logResource
     */
    public function __construct(
        \Project\Pimgento\Model\Session $session,
        \Pimgento\Log\Model\ResourceModel\Log\CollectionFactory $collectionFactory,
        \Pimgento\Log\Model\ResourceModel\Log $logResource
    ) {
        $this->session = $session;
        $this->logCollectionFactory = $collectionFactory;
        $this->logResource = $logResource;
    }

    /**
     * Retrieve unique message identity
     *
     * @return string
     */
    public function getIdentity()
    {
        return 'pimgento_product_import_report';
    }

    /**
     * Check whether
     *
     * @return bool
     */
    public function isDisplayed()
    {
        return !empty($this->getMessage());
    }

    /**
     * Retrieve message text
     *
     * @return string
     */
    public function getText()
    {
        return 'Pimgento Product Import : ' . $this->getMessage();
    }

    /**
     * Retrieve message severity
     *
     * @return int
     */
    public function getSeverity()
    {
        return self::SEVERITY_NOTICE;
    }

    /**
     * @return int
     */
    private function getLastProductImportLogId()
    {
        /** @var \Pimgento\Log\Model\ResourceModel\Log\Collection $logCollection */
        $logCollection = $this->logCollectionFactory->create();
        $logCollection->addFieldToSelect('log_id');
        $logCollection->addFieldToFilter('code', ['eq' => 'product']);
        $logCollection->addOrder('created_at');
        $logCollection->setPageSize(1);
        $log = $logCollection->getFirstItem();
        return $log->getData('log_id');
    }

    /**
     * @param int $logId
     * @return array
     */
    private function getProductImportLogSteps($logId)
    {
        $connection = $this->logResource->getConnection();

        return $connection->fetchAll(
            $connection->select()
                ->from($connection->getTableName('pimgento_import_log_step'))
                ->where('log_id = ?', $logId)
                ->where('method = ?', 'sendReport')
                ->order('step_id ASC')
        );
    }

    /**
     * @return string
     */
    private function getMessage()
    {
        $logId = $this->getLastProductImportLogId();
        $logSteps = $this->getProductImportLogSteps($logId);
        $message = '';
        foreach ($logSteps as $logStep) {
            if (strpos($logStep['message'], '[report]')) {
                $message = preg_replace('/\[(.*?)\]/', '', $logStep['message']);
            }
        }
        return $message;
    }
}
