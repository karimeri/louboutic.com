<?php

namespace Project\Pimgento\Model\Store;

use Magento\Store\Model\ResourceModel\Website as ResourceWebsite;

/**
 * Class WebsiteRepository
 * @package Project\Pimgento\Model\Store
 * @author Synolia <contact@synolia.com>
 */
class WebsiteRepository
{
    /**
     * @var ResourceWebsite
     */
    protected $resourceWebsite;

    /**
     * WebsiteRepository constructor.
     * @param ResourceWebsite $resourceWebsite
     */
    public function __construct(ResourceWebsite $resourceWebsite)
    {
        $this->resourceWebsite = $resourceWebsite;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function getWebsitesWithStores()
    {
        $connection = $this->resourceWebsite->getConnection();
        $select     = $connection->select()
            ->from(
                ['website' => $this->resourceWebsite->getMainTable()],
                ['code']
            )
            ->joinLeft(
                ['store' => $connection->getTableName('store')],
                'store.website_id = website.website_id',
                ['store_id']
            );

        //@codingStandardsIgnoreLine Ecg.Performance.FetchAll.Found
        return $connection->query($select)->fetchAll(\PDO::FETCH_GROUP);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function getWebsites()
    {
        $connection = $this->resourceWebsite->getConnection();
        $select     = $connection->select()
            ->from(
                ['website' => $this->resourceWebsite->getMainTable()],
                ['code', 'website_id']
            );
        //@codingStandardsIgnoreLine Ecg.Performance.FetchAll.Found
        return $connection->query($select)->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}
