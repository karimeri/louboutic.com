<?php

namespace Project\Pimgento\Model\Variant\Factory;

/**
 * Class Import
 * @package Project\Pimgento\Model\Variant\Factory
 */
class Import extends \Pimgento\Variant\Model\Factory\Import
{
    /**
     * Create temporary table
     */
    public function createTable()
    {
        $file = $this->getFileFullPath();

        if (!is_file($file)) {
            $this->setContinue(false);
            $this->setStatus(false);
            $this->setMessage($this->getFileNotFoundErrorMessage());
        } else {
            clearstatcache();
            // If file is empty
            if (filesize($file) === 0) {
                $this->setData('is_empty', true);
            }
            $this->_entities->createTmpTableFromFile($file, $this->getCode(), array('code'));
        }
    }
}
