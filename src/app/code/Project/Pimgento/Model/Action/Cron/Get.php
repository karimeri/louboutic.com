<?php

namespace Project\Pimgento\Model\Action\Cron;

use Magento\Framework\Exception\NoSuchEntityException;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Project\Pimgento\Api\Data\CronInterface;
use Synolia\Sync\Helper\Flow;

/**
 * Class Get
 * @package Project\Pimgento\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class Get extends AbstractCron implements ActionInterface
{
    const MESSAGE_NO_IMPORT         = 'No import to process.';
    const MESSAGE_IMPORT_PROCESSING = 'There is already an import in progress.';
    const MESSAGE_IMPORT_ERROR      = 'There is an import in error.';

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return array
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        try {
            $this->cronRepository->getByStatus(CronInterface::STATUS_PROCESSING);
        } catch (NoSuchEntityException $exception) {
            try {
                $cron = $this->cronRepository->getByStatus(CronInterface::STATUS_ERROR);
                $this->reportingHelper->sendCronErrorAlert(['importType' => $cron->getImportType()]);
            } catch (NoSuchEntityException $exception)
            {
                // If no cron in error - continue
                try {
                    $cron = $this->cronRepository->getByStatus(CronInterface::STATUS_PENDING);
                } catch (NoSuchEntityException $exception) {
                    throw new NoSuchEntityException(__(self::MESSAGE_NO_IMPORT));
                }
                $cron->setStatus(CronInterface::STATUS_PROCESSING);
                $this->cronRepository->save($cron);
                $this->setCron($cron);
                return $data;
            }
            throw new NoSuchEntityException(__(self::MESSAGE_IMPORT_ERROR));
        }
        throw new NoSuchEntityException(__(self::MESSAGE_IMPORT_PROCESSING));
    }
}
