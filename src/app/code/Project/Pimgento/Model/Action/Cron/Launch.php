<?php

namespace Project\Pimgento\Model\Action\Cron;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Pimgento\Import\Model\Import;

/**
 * Class Launch
 * @package Project\Pimgento\Model\Action\Cron
 * @author Synolia <contact@synolia.com>
 */
class Launch extends AbstractCron implements ActionInterface
{
    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        if (!$this->hasCron()) {
            return $data;
        }
        $this->launch($data);
    }

    /**
     * @param array $data
     * @return \Pimgento\Import\Model\Factory
     * @throws \Exception
     */
    protected function initImport(array $data)
    {
        $cron   = $this->getCron();
        $import = $this->import->load($cron->getImportType());

        if (!empty($data['file'])) {
            $import->setFile($data['file'])->setStep(0);
        }

        return $import;
    }

    /**
     * @param array $data
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function launch(array $data)
    {
        $import = $this->initImport($data);

        while ($import->canExecute()) {
            $import->execute();
            if (!$import->getContinue()) {
                break;
            }
            $import->next();
        }

        $this->sendAkeneo(null, $import);
    }
}
