<?php

namespace Project\Pimgento\Model\Action\Cron;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Project\Pimgento\Model\Akeneo\File;
use Project\Pimgento\Helper\Config;

/**
 * Class DownloadFiles
 * @package Project\Pimgento\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class DownloadFiles extends AbstractCron implements ActionInterface
{
    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return array
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        if (!$this->hasCron()) {
            return $data;
        }
        return $this->downloadFiles($data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Exception
     */
    protected function downloadFiles(array $data)
    {
        $cron = $this->getCron();
        $this->akeneoFile->open();

        if ($cron->getFile()) {
            $data['file'] = $this->akeneoFile->proccessFile($cron->getFile());
        }
        if ($cron->getZipFile()) {
            $this->akeneoFile->proccessFile(
                $cron->getZipFile(),
                $this->config->getPathProductImage()
            );
        }

        $this->akeneoFile->close();
        return $data;
    }
}
