<?php

namespace Project\Pimgento\Model\Action\Cron;

use Magento\Framework\Registry;
use Magento\Framework\Filesystem\DirectoryList;
use Pimgento\Import\Model\Import;
use Pimgento\Import\Model\Factory;
use Project\Core\Manager\EnvironmentManager;
use Project\Pimgento\Helper\Reporting;
use Project\Pimgento\Model\Akeneo\Api;
use Project\Pimgento\Model\CronRepository;
use Project\Pimgento\Api\Data\CronInterface;
use Project\Pimgento\Model\Akeneo\File;
use Project\Pimgento\Helper\Config;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Sync\Logger\LoggerFactory as SyncLoggerFactory;

/**
 * Class AbstractCron
 * @package Project\Pimgento\Model\Action\Cron
 * @author Synolia <contact@synolia.com>
 */
class AbstractCron
{
    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $errorLogger;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Project\Pimgento\Model\CronRepository
     */
    protected $cronRepository;

    /**
     * @var \Project\Pimgento\Model\Akeneo\Api
     */
    protected $akeneoApi;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Project\Pimgento\Model\Akeneo\File
     */
    protected $akeneoFile;

    /**
     * @var \Project\Pimgento\Helper\Config
     */
    protected $config;

    /**
     * @var \Pimgento\Import\Model\Import
     */
    protected $import;

    /**
     * @var \Project\Pimgento\Helper\Reporting
     */
    protected $reportingHelper;

    /**
     * @var \Synolia\Sync\Logger\LoggerFactory
     */
    protected $syncLoggerFactory;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $logger;

    /**
     * Report constructor.
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Pimgento\Model\CronRepository $cronRepository
     * @param \Magento\Framework\Registry $registry
     * @param \Project\Pimgento\Model\Akeneo\Api $akeneoApi
     * @param \Project\Pimgento\Model\Akeneo\File $akeneoFile
     * @param \Project\Pimgento\Helper\Config $config
     * @param \Pimgento\Import\Model\Import $import
     * @param \Project\Pimgento\Helper\Reporting $reportingHelper
     * @param \Synolia\Sync\Logger\LoggerFactory $syncLoggerFactory
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        EnvironmentManager $environmentManager,
        CronRepository $cronRepository,
        SyncLoggerFactory $syncLoggerFactory,
        Registry $registry,
        Api $akeneoApi,
        File $akeneoFile,
        Config $config,
        Import $import,
        Reporting $reportingHelper
    ) {
        $this->environmentManager = $environmentManager;
        $this->cronRepository     = $cronRepository;
        $this->akeneoApi          = $akeneoApi;
        $this->syncLoggerFactory  = $syncLoggerFactory;
        $this->registry           = $registry;
        $this->akeneoFile         = $akeneoFile;
        $this->config             = $config;
        $this->akeneoFile         = $akeneoFile;
        $this->import             = $import;
        $this->reportingHelper    = $reportingHelper;

        $this->errorLogger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log').'/pimgento_cron.log']
        );

        $this->logger = $this->syncLoggerFactory->get('pimgento_cron_runner');
    }

    /**
     * @param $error
     * @param \Pimgento\Import\Model\Factory $import
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function sendAkeneo($error, Factory $import = null)
    {
        $cron = $this->getCron();
        try {
            $this->akeneoApi->sendNotification(
                $cron->getExecutionId(),
                [
                    'error'    => $error,
                    'instance' => $this->environmentManager->getEnvironment(),
                    'warning'  => $import ? $import->getWarning() : null
                ]
            );
        } catch (\Exception $exception) {
            $error = true;
            $this->errorLogger->addError($exception->getMessage());
        }

        $cron->setStatus($error ? CronInterface::STATUS_ERROR : CronInterface::STATUS_FINISHED);
        $this->cronRepository->save($cron);
    }

    /**
     * @param \Project\Pimgento\Api\Data\CronInterface $cron
     */
    public function setCron(CronInterface $cron)
    {
        if ($this->getCron()) {
            $this->registry->unregister('cron');
        }
        $this->registry->register('cron', $cron);
    }

    /**
     * @return null|\Project\Pimgento\Api\Data\CronInterface
     */
    public function getCron()
    {
        return $this->registry->registry('cron');
    }

    /**
     * @return bool
     */
    protected function hasCron()
    {
        return $this->getCron() && $this->getCron()->getStatus() != CronInterface::STATUS_ERROR;
    }
}
