<?php

namespace Project\Pimgento\Model;

use Magento\Eav\Model\AttributeRepository;
use Magento\Catalog\Model\ResourceModel\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Layer\Filter\PriceFactory;
use Magento\Eav\Model\ResourceModel\Entity\AttributeFactory;
use Magento\Catalog\Model\Product;
use Magento\Store\Model\Store;
use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeInterface;

/**
 * Class PimgentoRepository
 * @package Project\Pimgento\Model
 * @author Synolia <contact@synolia.com>
 */
class PimgentoRepository
{
    const ENTITY_ID                    = 'entity_id';
    const ATTRIBUTE_CATEGORY_IS_ACTIVE = 'is_active';
    const ATTRIBUTE_PRODUCT_IS_ACTIVE  = 'status';

    /**
     * @var array $oldStatus
     */
    protected static $oldStatus = array(
        1 => 'disabled',
        2 => 'enabled'
    );

    /**
     * @var \Magento\Eav\Model\AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category
     */
    protected $resourceCategory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $resourceProduct;

    /**
     * @var \Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface
     */
    protected $stockRegistryProvider;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Attribute
     */
    protected $resourceAttribute;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Layer\Filter\Price
     */
    protected $resourcePrice;

    /**
     * @var \Project\Pimgento\Model\QueryRegistry
     */
    protected $queryRegistry;

    /**
     * @var \Project\Pimgento\Model\Session
     */
    protected $session;

    /**
     * PimgentoRepository constructor.
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     * @param \Magento\Catalog\Model\ResourceModel\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\ProductFactory $productFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $attributeFactory
     * @param \Magento\Catalog\Model\ResourceModel\Layer\Filter\PriceFactory $priceFactory
     * @param \Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface $stockRegistryProvider
     * @param \Project\Pimgento\Model\QueryRegistry $queryRegistry
     * @param \Project\Pimgento\Model\Session $session
     */
    public function __construct(
        AttributeRepository $attributeRepository,
        CategoryFactory $categoryFactory,
        ProductFactory $productFactory,
        AttributeFactory $attributeFactory,
        PriceFactory $priceFactory,
        StockRegistryProviderInterface $stockRegistryProvider,
        QueryRegistry $queryRegistry,
        Session $session
    ) {
        $this->attributeRepository   = $attributeRepository;
        $this->resourceCategory      = $categoryFactory->create();
        $this->resourceProduct       = $productFactory->create();
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->resourceAttribute     = $attributeFactory->create();
        $this->resourcePrice         = $priceFactory->create();
        $this->queryRegistry         = $queryRegistry;
        $this->session               = $session;
    }

    /**
     * @param array $entityIds
     * @param $entityType
     * @param array $websites
     * @param int $value
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function enableToWebsites(array $entityIds, $entityType, array $websites, $value = 2)
    {
        $attributeIsActiveId = $this->attributeRepository->get(
            $entityType,
            $this->getColumnEnabled($entityType)
        )->getAttributeId();
        $this->enableSpecificAttribute($entityIds, $attributeIsActiveId, $entityType, $websites, $value);
    }

    /**
     * @param array $entityIds
     * @param $attributeId
     * @param $entityType
     * @param array $websites
     * @param int $value
     * @throws \Zend_Db_Statement_Exception
     */
    public function enableSpecificAttribute(array $entityIds, $attributeId, $entityType, array $websites, $value = 0)
    {
        $rowIds     = $this->getRowIdsByEntityIds(array_keys($entityIds), $entityType);
        $insertData = [];
        $deleteData = [];
        foreach ($entityIds as $entityId => $websiteCodes) {
            $isNotEmptyWebsiteCode = $websiteCodes != '';
            $websiteCodes = \explode(',', strtolower($websiteCodes));
            foreach ($websites as $code => $stores) {
                if (in_array($code, $websiteCodes)) {
                    continue;
                }
                foreach ($stores as $store) {
                    $insertData[] = [
                        'attribute_id' => $attributeId,
                        'row_id'       => $rowIds[$entityId],
                        'store_id'     => $store['store_id'],
                        'value'        => $code == Store::ADMIN_CODE && $isNotEmptyWebsiteCode ? 1 : $value
                    ];
                    $deleteData[$rowIds[$entityId]][] = $store['store_id'];
                }
            }
        }
        $connection = $this->getResource($entityType)->getConnection();
        //Fix Louboutin delete after save ok
       /* $rowIds     = array_values($rowIds);

        $connection->delete(
            $connection->getTableName($entityType.'_entity_int'),
            [
                'attribute_id = ?' => $attributeId,
                'row_id IN (?)'    => $rowIds,
                'store_id != ?'    => Store::DEFAULT_STORE_ID
            ]
        );*/

        if (!empty($insertData)) {
            $connection->insertOnDuplicate(
                $connection->getTableName($entityType.'_entity_int'),
                $insertData
            );
        }

        //fix louboutin
        foreach ($deleteData as $rowId => $stores) {
            array_push($stores,  Store::DEFAULT_STORE_ID);

            $connection->delete(
                $connection->getTableName($entityType.'_entity_int'),
                [
                    'attribute_id = ?' => $attributeId,
                    'row_id = ?'    => $rowId,
                    'store_id NOT IN (?) '  => array_values($stores)
                ]
            );
        }
    }

    /**
     * @param array $entityIds
     * @param array $websites
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function disableProductByPrice(array $entityIds, array $websites)
    {
        $prices     = $this->getPriceByEntityIds(array_keys($entityIds));
        $attribute  = $this->attributeRepository->get(Product::ENTITY, self::ATTRIBUTE_PRODUCT_IS_ACTIVE);
        $rowIds     = $this->getRowIdsByEntityIds(array_keys($entityIds), Product::ENTITY);
        $insertData = $output = [];
        $reportData = [];

        foreach ($entityIds as $entityId => $infos) {
            $disableProductForDefaultStore = true;
            $reactivated = $infos['reactivated'];
            $websiteCodes = \explode(',', strtolower($infos['product_status']));
            foreach ($websites as $code => $stores) {
                if (!in_array($code, $websiteCodes)) {
                    if ($reactivated) {
                        $reportData[$rowIds[$entityId]] = [
                            'old_status' => static::$oldStatus[$reactivated],
                            'update' => __('OK'),
                            'reason' => ''
                        ];
                    }
                    continue;
                }
                foreach ($stores as $store) {
                    if (!empty($prices[$entityId][$store['store_id']])) {
                        $disableProductForDefaultStore = false;
                        if ($reactivated) {
                            $reportData[$rowIds[$entityId]] = [
                                'old_status' => static::$oldStatus[$reactivated],
                                'update' => __('OK'),
                                'reason' => ''
                            ];
                        }
                        continue;
                    }
                    $insertData[] = [
                        'attribute_id' => $attribute->getAttributeId(),
                        'row_id'       => $rowIds[$entityId],
                        'store_id'     => $store['store_id'],
                        'value'        => 2
                    ];
                    $output[$infos['sku']]['website'][] = strtoupper($code);
                    if ($reactivated) {
                        $reportData[$rowIds[$entityId]] = [
                            'old_status' => static::$oldStatus[$reactivated],
                            'update' => __('KO'),
                            'reason' => __('Product without price')
                        ];
                    }
                }
            }
            if ($disableProductForDefaultStore) {
                $insertData[] = [
                    'attribute_id' => $attribute->getAttributeId(),
                    'row_id'       => $rowIds[$entityId],
                    'store_id'     => Store::DEFAULT_STORE_ID,
                    'value'        => 2
                ];
            }
        }
        if (!empty($insertData)) {
            $connection = $this->resourceProduct->getConnection();
            $connection->insertOnDuplicate(
                $connection->getTableName(Product::ENTITY.'_entity_int'),
                $insertData
            );
        }
        if (!empty($reportData)) {
            $this->session->setData('report_data', $reportData);
        }

        return $output;
    }

    /**
     * @param array $entityIds
     * @param $websites
     */
    public function associateToWebsites(array $entityIds, $websites)
    {
        $insertData = [];
        $deleteData = [];
        foreach ($entityIds as $entityId => $websiteCodes) {
            $websiteCodes = \explode(',', strtolower($websiteCodes));
            foreach ($websites as $code => $websiteId) {
                if (!in_array($code, $websiteCodes) || $code == Store::ADMIN_CODE) {
                    continue;
                }
                $insertData[] = [
                    'product_id' => $entityId,
                    'website_id' => $websiteId
                ];
                $deleteData[$entityId][] = (int) $websiteId;
            }
        }
        $connection  = $this->resourceProduct->getConnection();
        $productsIds = array_keys($entityIds);

        //fix Louboutin, if error in insert we lost products website, not delete before insert!
        /*$connection->delete(
            $connection->getTableName('catalog_product_website'),
            [
                'product_id IN (?)' => $productsIds,
                'website_id != ?'   => Store::DEFAULT_STORE_ID
            ]
        );*/

        if (!empty($insertData)) {
            $connection->insertOnDuplicate(
                $connection->getTableName('catalog_product_website'),
                $insertData
            );
        }

        //fix Louboutin
       if (!empty($deleteData)) {
            foreach ($deleteData as $entityId => $websiteCodes) {
                array_push($websiteCodes,  Store::DEFAULT_STORE_ID);
                $connection->delete(
                    $connection->getTableName('catalog_product_website'),
                    [
                        'product_id = (?)' => $entityId,
                        'website_id NOT IN (?)'   => array_values($websiteCodes)
                    ]
                );
            }
        }


    }

    /**
     * @param $entityIds
     * @param $websites
     */
    public function initStock($entityIds, $websites)
    {
        $insertData = [];

        foreach ($entityIds as $entityId => $infos) {
            $websiteCodes = \explode(',', strtolower($infos['sites']));

            foreach ($websiteCodes as $websiteCode) {
                $websiteId = $websites[$websiteCode] ?? false;
                if (!$websiteId) {
                    continue;
                }

                $stock     = $this->stockRegistryProvider->getStock($websiteId);
                $stockId   = $stock->getStockId() ?? 1;
                $websiteId = $stockId === 1 ? 0 : $websiteId;
                $key       = $entityId.'-'.$stockId;

                if (!empty($insertData[$key])) {
                    continue;
                }

                $insertData[$key] = [
                    'product_id'                => $entityId,
                    'stock_id'                  => $stockId,
                    'qty'                       => 0,
                    'is_in_stock'               => $infos['type_id'] == Configurable::TYPE_CODE ? 1 : 0,
                    'low_stock_date'            => null,
                    'stock_status_changed_auto' => 0,
                    'website_id'                => $websiteId,
                ];
            }
        }

        $connection = $this->resourceProduct->getConnection();
        if (!empty($insertData)) {
            $connection->insertOnDuplicate(
                $connection->getTableName('cataloginventory_stock_item'),
                $insertData,
                ['item_id']
            );
        }
    }

    /**
     * @param $entityType
     * @return \Magento\Catalog\Model\ResourceModel\Category|\Magento\Catalog\Model\ResourceModel\Product
     */
    protected function getResource($entityType)
    {
        return $entityType == Product::ENTITY ? $this->resourceProduct : $this->resourceCategory;
    }

    /**
     * @param $entityType
     * @return string
     */
    protected function getColumnEnabled($entityType)
    {
        return $entityType == Product::ENTITY ? self::ATTRIBUTE_PRODUCT_IS_ACTIVE : self::ATTRIBUTE_CATEGORY_IS_ACTIVE;
    }

    /**
     * @param array $entityIds
     * @param $entityType
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getRowIdsByEntityIds(array $entityIds, $entityType)
    {
        $resource   = $this->getResource($entityType);
        $connection = $resource->getConnection();

        $select = $connection->select()
            ->from(
                $resource->getEntityTable(),
                [self::ENTITY_ID, 'row_id']
            )
            ->where(self::ENTITY_ID.' IN (?)', $entityIds);

        return $this->queryRegistry->processFetchAll($connection, $select, \PDO::FETCH_KEY_PAIR);
    }

    /**
     * @param array $entityIds
     * @param int $value
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function disableRmaEligibility(array $entityIds, $value = 0)
    {
        $entityType = Product::ENTITY;
        $attribute  = $this->attributeRepository->get($entityType, 'is_returnable');
        $rowIds     = $this->getRowIdsByEntityIds(array_keys($entityIds), $entityType);
        $entityIds  = array_keys($entityIds);
        $insertData = [];

        foreach ($entityIds as $entityId) {
            $insertData[] = [
                'attribute_id' => $attribute->getAttributeId(),
                'row_id'       => $rowIds[$entityId],
                'store_id'     => Store::DEFAULT_STORE_ID,
                'value'        => $value
            ];
        }

        $connection = $this->getResource($entityType)->getConnection();

        if (!empty($insertData)) {
            $connection->insertOnDuplicate(
                $connection->getTableName($entityType.'_entity_varchar'),
                $insertData
            );
        }
    }

    /**
     * @param array $attributeCodes
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function getAttributeByAttributeCodes(array $attributeCodes)
    {
        $connection = $this->resourceAttribute->getConnection();
        $select     = $connection->select()
            ->from(
                $this->resourceAttribute->getMainTable(),
                [AttributeInterface::ATTRIBUTE_CODE, AttributeInterface::ATTRIBUTE_ID]
            )
            ->where(AttributeInterface::ATTRIBUTE_CODE.' IN (?)', $attributeCodes);

        return $this->queryRegistry->processFetchAll($connection, $select, \PDO::FETCH_KEY_PAIR);
    }

    /**
     * @param array $skus
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getSku(array $skus)
    {
        $connection = $this->resourceProduct->getConnection();
        $select     = $connection->select()
            ->from(
                $this->resourceProduct->getEntityTable(),
                ['sku', $this->resourceProduct->getEntityIdField()]
            )
            ->where('sku IN (?)', $skus);

        return $this->queryRegistry->processFetchAll($connection, $select, \PDO::FETCH_KEY_PAIR);
    }

    /**
     * @param array $entityIds
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function getPriceByEntityIds(array $entityIds)
    {
        $attribute  = $this->attributeRepository->get(Product::ENTITY, 'price');
        $connection = $this->resourceProduct->getConnection();
        $select = $connection->select()
            ->from(
                $connection->getTableName('catalog_product_entity_decimal'),
                ['row_id', 'store_id']
            )
            ->where(AttributeInterface::ATTRIBUTE_ID.' = ?', $attribute->getAttributeId())
            ->where('store_id != ?', Store::DEFAULT_STORE_ID)
            ->where('value > ?', 0)
            ->where('row_id IN (?)', $entityIds);

        $prices = $this->queryRegistry->processFetchAll($connection, $select);
        return array_reduce(
            $prices,
            function ($result, $data) {
                $result[$data['row_id']][$data['store_id']] = $data['store_id'];
                return $result;
            }
        );
    }

    /**
     * @param array $categoryCodes
     * @return mixed
     * @throws \Zend_Db_Statement_Exception
     */
    public function getCategoryIdsByCodes(array $categoryCodes)
    {
        $connection = $this->resourceCategory->getConnection();
        $select = $connection->select()
            ->from(
                $connection->getTableName('pimgento_entities'),
                ['entity_id']
            )
            ->where('import = ?', 'category')
            ->where('code IN (?)', $categoryCodes);

        $result = $this->queryRegistry->processFetchAll($connection, $select);
        return array_column($result, 'entity_id');
    }

    /**
     * @param array $skus
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getProductIdsBySkus(array $skus)
    {
        $connection = $this->resourceProduct->getConnection();
        $select = $connection->select()
            ->from(
                $connection->getTableName('pimgento_entities'),
                ['entity_id']
            )
            ->where('import = ?', 'product')
            ->where('code IN (?)', $skus);

        $result = $this->queryRegistry->processFetchAll($connection, $select);
        return array_column($result, 'entity_id');
    }

    public function getReportData()
    {
        return $this->session->getData('report_data');
    }
}
