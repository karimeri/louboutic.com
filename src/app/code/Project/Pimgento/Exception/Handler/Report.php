<?php

namespace Project\Pimgento\Exception\Handler;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Exception\Handler\ActionInterface;
use Project\Pimgento\Model\Action\Cron\AbstractCron;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Report
 * @package Project\Pimgento\Exception\Handler
 * @author Synolia <contact@synolia.com>
 */
class Report extends AbstractCron implements ActionInterface
{
    /**
     * @param \Throwable $throwable
     * @param array $parameters
     * @param \Synolia\Sync\Console\ConsoleOutput $output
     * @param string $stepName
     * @return mixed|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(
        \Throwable $throwable,
        array $parameters,
        ConsoleOutput $output,
        string $stepName
    ) {
        if (!($throwable instanceof NoSuchEntityException)) {
            $this->sendAkeneo($throwable->getMessage());
            $this->logger->addError($throwable->getMessage());
            return;
        }
        $this->logger->debug($throwable->getMessage());
    }
}
