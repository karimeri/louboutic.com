<?php

namespace Project\Pimgento\Cron;

use Magento\Framework\Exception\NoSuchEntityException;
use Pimgento\Import\Model\Import as ImportModel;
use Project\Pimgento\Model\CronRepository;
use Project\Pimgento\Model\Akeneo\File as AkeneoFile;
use Project\Pimgento\Model\Akeneo\Api as AkeneoApi;
use Project\Pimgento\Api\Data\CronInterface;
use Project\Core\Manager\EnvironmentManager;
use Synolia\Logger\Model\LoggerFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Event\ManagerInterface;
use Project\Pimgento\Helper\Config;

/**
 * Class Launch
 * @package Project\Pimgento\Cron
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Launch
{
    /**
     * @var \Project\Pimgento\Model\CronRepository
     */
    protected $cronRepository;

    /**
     * @var \Pimgento\Import\Model\Import
     */
    protected $import;

    /**
     * @var \Project\Pimgento\Model\Akeneo\Api
     */
    protected $akeneoApi;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $errorLogger;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Project\Pimgento\Api\Data\CronInterface
     */
    protected $cron;

    /**
     * @var \Pimgento\Import\Model\Factory
     */
    protected $pimgentoImport;

    /**
     * @var \Project\Pimgento\Model\Akeneo\File
     */
    protected $akeneoFile;

    /**
     * @var \Project\Pimgento\Helper\Config
     */
    protected $config;

    /**
     * Launch constructor.
     * @param \Project\Pimgento\Model\CronRepository $cronRepository
     * @param \Pimgento\Import\Model\Import $import
     * @param \Project\Pimgento\Model\Akeneo\Api $akeneoApi
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Project\Pimgento\Model\Akeneo\File $akeneoFile
     * @param \Project\Pimgento\Helper\Config $config
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        CronRepository $cronRepository,
        ImportModel $import,
        AkeneoApi $akeneoApi,
        EnvironmentManager $environmentManager,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        ManagerInterface $eventManager,
        AkeneoFile $akeneoFile,
        Config $config
    ) {
        $this->cronRepository     = $cronRepository;
        $this->import             = $import;
        $this->akeneoApi          = $akeneoApi;
        $this->environmentManager = $environmentManager;
        $this->eventManager       = $eventManager;
        $this->akeneoFile         = $akeneoFile;

        $this->errorLogger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log').'/pimgento_cron.log']
        );
        $this->config = $config;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        try {
            $this->cronRepository->getByStatus(CronInterface::STATUS_PROCESSING);
        } catch (NoSuchEntityException $exception) {
            try {
                $this->cron = $this->cronRepository->getByStatus(CronInterface::STATUS_PENDING);
                $this->eventManager->dispatch('pimgento_import_cron_start_'.$this->cron->getImportType());
                $this->cron->setStatus(CronInterface::STATUS_PROCESSING);
                $this->cronRepository->save($this->cron);

                $this->pimgentoImport = $this->import->load($this->cron->getImportType());
                $this->downloadFiles();

                while ($this->pimgentoImport->canExecute()) {
                    $this->pimgentoImport->execute();
                    if (!$this->pimgentoImport->getContinue()) {
                        break;
                    }
                    $this->pimgentoImport->next();
                }

                $this->eventManager->dispatch('pimgento_import_cron_end_'.$this->cron->getImportType());
                $this->sendAkeneo(null);
            } catch (NoSuchEntityException $exception) {
                return;
            } catch (\Exception $exception) {
                if (!empty($this->cron)) {
                    $this->sendAkeneo($exception->getMessage());
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    protected function downloadFiles()
    {
        $this->akeneoFile->open();

        if ($this->cron->getFile()) {
            $this->pimgentoImport->setFile(
                $this->akeneoFile->proccessFile($this->cron->getFile())
            );
        }
        if ($this->cron->getZipFile()) {
            $this->pimgentoImport->setZipFile(
                $this->akeneoFile->proccessFile($this->cron->getZipFile(), $this->config->getPathProductImage())
            );
        }

        $this->akeneoFile->close();
    }

    /**
     * @param $error
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function sendAkeneo($error)
    {
        try {
            $this->akeneoApi->sendNotification(
                $this->cron->getExecutionId(),
                [
                    'error'    => $error,
                    'instance' => $this->environmentManager->getEnvironment(),
                    'warning'  => $this->pimgentoImport->getWarning()
                ]
            );
        } catch (\Exception $exception) {
            $error = true;
            $this->errorLogger->addError($exception->getMessage());
        }

        $this->cron->setStatus($error ? CronInterface::STATUS_ERROR : CronInterface::STATUS_FINISHED);
        $this->cronRepository->save($this->cron);
    }
}
