<?php

namespace Project\Pimgento\Block\Email;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status as StatusSource;
use Magento\Framework\View\Element\Template\Context;
use Project\Pimgento\Model\PimgentoRepository;

/**
 * Class Report
 * @package Project\Pimgento\Block\Email
 */
class Report extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_template = 'Project_Pimgento::email/report_grid.phtml';

    /**
     * @var PimgentoRepository
     */
    protected $pimgentoRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $statusSource;

    /**
     * Report constructor.
     * @param Context $context
     * @param PimgentoRepository $pimgentoRepository
     * @param ProductRepositoryInterface $productRepository
     * @param StatusSource $statusSource
     * @param array $data
     */
    public function __construct(
        Context $context,
        PimgentoRepository $pimgentoRepository,
        ProductRepositoryInterface $productRepository,
        StatusSource $statusSource,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->pimgentoRepository = $pimgentoRepository;
        $this->productRepository = $productRepository;
        $this->statusSource = $statusSource;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProducts()
    {
        $products = [];
        foreach ($this->pimgentoRepository->getReportData() as $entityId => $reportDatum) {
            if ($reportDatum['old_status'] === 'disabled') {
                $product = $this->productRepository->getById($entityId);
                $products[$entityId] = [
                    'sku'           => $product->getSku(),
                    'status'        => $this->statusSource->getOptionText($product->getStatus()),
                    'old_status'    => __('Disabled'),
                    'update_status' => $reportDatum['update'],
                    'reason'        => $reportDatum['reason']
                ];
            }
        };
        return $products;
    }

    public function getDisabledProducts()
    {
        $products = [];
        foreach ($this->pimgentoRepository->getReportData() as $entityId => $reportDatum) {
            if ($reportDatum['old_status'] === 'enabled') {
                $product = $this->productRepository->getById($entityId);
                $products[$entityId] = [
                    'sku'           => $product->getSku(),
                    'status'        => $this->statusSource->getOptionText($product->getStatus()),
                    'old_status'    => __('Enabled'),
                    'update_status' => $reportDatum['update'],
                    'reason'        => $reportDatum['reason']
                ];
            }
        };
        return $products;
    }
}
