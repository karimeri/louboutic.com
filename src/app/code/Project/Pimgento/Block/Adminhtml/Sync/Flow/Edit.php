<?php

namespace Project\Pimgento\Block\Adminhtml\Sync\Flow;

use Project\Pimgento\Controller\Adminhtml\Cron\Unlock;

/**
 * Class Edit
 * @package Project\Pimgento\Block\Adminhtml\Sync\Flow
 */
class Edit extends \Synolia\Sync\Block\Adminhtml\Flow\Edit
{
    protected function _construct()
    {
        parent::_construct();

        if ($this->getFlowCode() === 'pimgento_cron_runner') {
            if ($this->_authorization->isAllowed(Unlock::ADMIN_RESOURCE)) {
                $this->addButton(
                    'unlock_cron',
                    [
                        'label' => __('Unlock Cron'),
                        'onclick' => 'setLocation(\'' . $this->getUnlockCronUrl() . '\')',
                        'class' => 'primary unlock-cron'
                    ],
                    10,
                    10
                );
            }
        }
    }

    protected function getUnlockCronUrl()
    {
        return $this->getUrl('pimgento/cron/unlock', ['flow_code' => $this->getFlowCode()]);
    }
}
