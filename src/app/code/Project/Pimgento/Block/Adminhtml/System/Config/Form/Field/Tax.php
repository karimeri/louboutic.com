<?php

namespace Project\Pimgento\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Tax\Model\TaxClass\Source\Product;
use Magento\Catalog\Model\Product\AttributeSet\Options;

/**
 * Class Tax
 * @package Project\Pimgento\Block\Adminhtml\System\Config\Form\Field
 * @author Synolia <contact@synolia.com>
 */
class Tax extends AbstractFieldArray
{
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * @var \Magento\Tax\Model\TaxClass\Source\Product
     */
    protected $productTaxClass;

    /**
     * @var \Magento\Catalog\Model\Product\AttributeSet
     */
    protected $attributeSet;

    /**
     * Tax constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Magento\Tax\Model\TaxClass\Source\Product $productTaxClass
     * @param \Magento\Catalog\Model\Product\AttributeSet\Options $attributeSet
     * @param array $data
     */
    public function __construct(
        Context $context,
        Factory $elementFactory,
        Product $productTaxClass,
        Options $attributeSet,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->elementFactory  = $elementFactory;
        $this->productTaxClass = $productTaxClass;
        $this->attributeSet    = $attributeSet;
    }

    /**
     * Initialise form fields
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->addColumn('family', ['label' => __('Familly')]);
        $this->addColumn('tax_class_id', ['label' => __('Tax Class')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * @param string $columnName
     * @return mixed|string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName == 'tax_class_id' && isset($this->_columns[$columnName])) {
            $options = $this->productTaxClass->getAllOptions();
            return $this->createSelect($columnName, $options);
        }

        return parent::renderCellTemplate($columnName);
    }

    /**
     * @param $attributesSet
     * @return mixed
     */
    protected function getAttributesSetName($attributesSet)
    {
        foreach ($attributesSet as $key => $attributeSet) {
            $attributesSet[$key]['value'] =  $attributeSet['label'];
        }
        return $attributesSet;
    }

    /**
     * @param $columnName
     * @param $options
     * @return mixed
     */
    protected function createSelect($columnName, $options)
    {
        $element = $this->elementFactory->create('select');
        $element->setForm($this->getForm())
            ->setName($this->_getCellInputElementName($columnName))
            ->setHtmlId($this->_getCellInputElementId('<%- _id %>', $columnName))
            ->setValues($options);
        return str_replace("\n", '', $element->getElementHtml());
    }
}
