<?php

namespace Project\Pimgento\Block\Adminhtml\Log\Grid\Column\Renderer;

use Magento\Framework\DataObject;

/**
 * Class Status
 * @package Project\Pimgento\Block\Adminhtml\Log\Grid\Column\Renderer
 */
class Status extends \Pimgento\Log\Block\Adminhtml\Grid\Column\Renderer\Status
{
    /**
     * @inheritdoc
     */
    public function render(DataObject $row)
    {
        $class = '';
        $text = '';
        switch ($this->_getValue($row)) {
            case 1:
                $class = 'grid-severity-notice';
                $text = __('Success');
                break;
            case 2:
                $class = 'grid-severity-critical';
                $text = __('Error');
                break;
            case 3:
                $class = 'grid-severity-minor';
                $text = __('Processing');
                break;
            case 4:
                $class = 'grid-severity-notice';
                $text = __('Empty');
                break;
        }
        return '<span class="' . $class . '"><span>' . $text . '</span></span>';
    }
}
