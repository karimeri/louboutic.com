<?php

namespace Project\Pimgento\Controller\Adminhtml\Import;

use Pimgento\Import\Controller\Adminhtml\Import\Upload as BaseUpload;
use \Magento\Framework\App\RequestInterface;
use \Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\Result\RawFactory;
use \Magento\Framework\Data\Form\FormKey;
use \Pimgento\Import\Helper\Config as configHelper;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Upload extends BaseUpload
{
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;

    /**
     * @var \Pimgento\Import\Helper\Config
     */
    protected $_helperConfig;

    /** @var DateTime $datetime */
    protected $datetime;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Pimgento\Import\Helper\Config $helperConfig
     * @param DateTime $datetime
     */
    public function __construct(
        Context $context,
        RawFactory $resultRawFactory,
        FormKey $formKey,
        configHelper $helperConfig,
        DateTime $datetime
    ) {
        parent::__construct(
            $context,
            $resultRawFactory,
            $formKey,
            $helperConfig,
            $datetime
            );
        $this->getRequest()->setParams(
            array('form_key' => $this->_formKey->getFormKey())
        );
    }

}
