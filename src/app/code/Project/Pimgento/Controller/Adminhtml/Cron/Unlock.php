<?php

namespace Project\Pimgento\Controller\Adminhtml\Cron;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\ResponseInterface;
use Project\Pimgento\Api\CronRepositoryInterface;
use Project\Pimgento\Api\Data\CronInterface;

/**
 * Class Unlock
 * @package Project\Pimgento\Controller\Adminhtml\Cron
 */
class Unlock extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Project_Pimgento::pimgento_unlock_cron';

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var CronRepositoryInterface
     */
    protected $cronRepository;

    /**
     * Unlock constructor.
     * @param Context $context
     * @param CacheInterface $cache
     * @param CronRepositoryInterface $cronRepository
     */
    public function __construct(
        Context $context,
        CacheInterface $cache,
        CronRepositoryInterface $cronRepository
    ) {
        parent::__construct($context);
        $this->cache = $cache;
        $this->cronRepository = $cronRepository;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $flowCode  = $this->getRequest()->getParam('flow_code');

        try {
            $cron = $this->cronRepository->getByStatus(CronInterface::STATUS_ERROR);
            $cron->setStatus(CronInterface::STATUS_PENDING);
            $this->cronRepository->save($cron);
            $this->messageManager->addSuccessMessage('Pimgento ' . $cron->getImportType()
                . ' import cron has been unlocked.');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('synolia_sync/flow/edit', ['flow_code' => $flowCode]);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}
