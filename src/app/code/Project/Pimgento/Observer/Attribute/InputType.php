<?php

namespace Project\Pimgento\Observer\Attribute;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product\Attribute\Frontend\Image;

/**
 * Class InputType
 * @package Project\Pimgento\Observer\Attribute
 * @author Synolia <contact@synolia.com>
 */
class InputType implements ObserverInterface
{
    /**
     * @codingStandardsIgnoreStart
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $response = $observer->getResponse();
        $types    = $response->getTypes();

        $types = array_merge(
            $types,
            [
                'media_image' => [
                    'backend_type'   => 'varchar',
                    'frontend_input' => 'media_image',
                    'backend_model'  => NULL,
                    'source_model'   => NULL,
                    'frontend_model' => Image::class,
                ]
            ]
        );

        $response->setTypes($types);
    }
}