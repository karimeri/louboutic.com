<?php

namespace Project\Pimgento\Observer\Attribute;

use Magento\Framework\Event\Observer;
use Project\Pimgento\Observer\ObserverAbstract;

/**
 * Class InputType
 * @package Project\Pimgento\Observer\Attribute
 * @author Synolia <contact@synolia.com>
 */
class Scopable extends ObserverAbstract
{
    const COLUMN_SCOPABLE = 'scopable';
    const COLUMN_CODE     = 'code';

    /**
     * @codingStandardsIgnoreStart
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Pimgento\Category\Model\Factory\Import $import */
        $import = $observer->getImport();
        $file   = $this->getFileFullPath($import->getFile());

        if ($this->fileSystem->fileExists($file) &&
            $import->getStep() == 0
        ) {
            $this->csvProcessor->saveData(
                $file,
                $this->addData(
                    $this->csvProcessor->getData($file)
                )
            );
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function addData($data)
    {
        $columns     = current($data);
        $keyScopable = array_search(self::COLUMN_SCOPABLE, $columns);
        $keyCode     = array_search(self::COLUMN_CODE, $columns);
        $attributes  = $this->helperConfig->getForceAttributeScopable();

        foreach ($data as $key => $line) {
            if ($key == 0) {
                continue;
            }
            if (in_array($line[$keyCode], $attributes)) {
                $data[$key][$keyScopable] = 1;
            }
        }

        return $data;
    }
}