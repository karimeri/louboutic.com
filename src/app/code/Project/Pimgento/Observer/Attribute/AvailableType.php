<?php

namespace Project\Pimgento\Observer\Attribute;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AvailableType
 * @package Project\Pimgento\Observer\Attribute
 * @author Synolia <contact@synolia.com>
 */
class AvailableType implements ObserverInterface
{
    /**
     * @codingStandardsIgnoreStart
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $response = $observer->getResponse();
        $types    = $response->getTypes();
        $types    = array_merge($types, ['media_image' => 'media_image']);
        $response->setTypes($types);
    }
}