<?php

namespace Project\Pimgento\Observer;

use Magento\Framework\File\Csv;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem\Io\File;
use Pimgento\Import\Helper\Config as HelperPimgento;
use Pimgento\Log\Model\Log as PimgentoLog;
use Pimgento\Import\Model\Factory as ImportFactory;
use Project\Pimgento\Helper\Config;
use Project\Core\Manager\EnvironmentManager;
use Project\Pimgento\Model\PimgentoRepository;

/**
 * Class ObserverAbstract
 * @package Project\Pimgento\Observer\Variant
 * @author Synolia <contact@synolia.com>
 */
abstract class ObserverAbstract implements ObserverInterface
{
    const CODE_VARIANT  = 'variant';
    const CODE_PRODUCT  = 'product';
    const CODE_CATEGORY = 'category';

    /**
     * @var Csv
     */
    protected $csvProcessor;

    /**
     * @var \Project\Pimgento\Helper\mixedwebsite_mapping
     */
    protected $mainChannel;

    /**
     * @var array
     */
    protected $mappingKeys = [];

    /**
     * @var string
     */
    protected $keyBrand;

    /**
     * @var Config
     */
    protected $helperConfig;

    /**
     * @var HelperPimgento
     */
    protected $helperPimgento;

    /**
     * @var File
     */
    protected $fileSystem;

    /**
     * @var PimgentoLog
     */
    protected $pimgentoLog;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Project\Pimgento\Model\PimgentoRepository
     */
    protected $pimgentoRepository;

    /**
     * @param Csv $csvProcessor
     * @param Config $helperConfig
     * @param HelperPimgento $helperPimgento
     * @param File $fileSystem
     * @param PimgentoLog $pimgentoLog
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Pimgento\Model\PimgentoRepository $pimgentoRepository
     */
    public function __construct(
        Csv $csvProcessor,
        Config $helperConfig,
        HelperPimgento $helperPimgento,
        File $fileSystem,
        PimgentoLog $pimgentoLog,
        EnvironmentManager $environmentManager,
        PimgentoRepository $pimgentoRepository
    ) {
        $this->csvProcessor       = $csvProcessor;
        $this->helperConfig       = $helperConfig;
        $this->mainChannel        = $helperConfig->getMainChannel();
        $this->helperPimgento     = $helperPimgento;
        $this->fileSystem         = $fileSystem;
        $this->pimgentoLog        = $pimgentoLog;
        $this->environmentManager = $environmentManager;
        $this->pimgentoRepository = $pimgentoRepository;
        $this->csvProcessor->setDelimiter(';');
    }

    /**
     * @codingStandardsIgnoreStart
     * @param Observer $observer
     * @return void
     */
    abstract function execute(Observer $observer);

    /**
     * @param $file
     * @return string
     */
    public function getFileFullPath($file)
    {
        return $this->helperConfig->getFileFullPath($file);
    }

    /**
     * @param ImportFactory $import
     * @param PimgentoLog $log
     * @throws \Exception
     */
    protected function log(ImportFactory $import, PimgentoLog $log)
    {
        $log->addStep(
            [
                'log_id'     => $log->getId(),
                'identifier' => $import->getIdentifier(),
                'number'     => $import->getStep(),
                'method'     => $import->getMethod(),
                'message'    => $import->getMessage(),
                'continue'   => $import->getContinue() ? 1 : 0,
                'status'     => $import->getStatus() ? 1 : 0
            ]
        );
    }
}