<?php

namespace Project\Pimgento\Observer\Log;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use \Pimgento\Log\Api\Data\LogInterface;
use \Pimgento\Log\Model\Log as LogModel;

/**
 * Class PimgentoImportStepFinishEmptyObserver
 * @package Project\Pimgento\Observer\Log
 */
class PimgentoImportStepFinishEmptyObserver implements ObserverInterface
{
    /**
     * @var \Pimgento\Log\Model\Log
     */
    protected $_log;

    /**
     * Constructor
     *
     * @param \Pimgento\Log\Model\Log $log
     */
    public function __construct(LogModel $log)
    {
        $this->_log = $log;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var $import \Pimgento\Import\Model\Factory */
        $import = $observer->getEvent()->getImport();

        $this->_log->load($import->getIdentifier(), LogInterface::IDENTIFIER);

        if ($this->_log->hasData()) {

            if ($import->getData('is_empty')) {
                $this->_log->setStatus(4)->save(); // Empty
            }

            $this->_log->addStep(
                array(
                    'log_id' => $this->_log->getId(),
                    'identifier' => $import->getIdentifier(),
                    'number' => $import->getStep(),
                    'method' => $import->getMethod(),
                    'message' => $import->getMessage(),
                    'continue' => $import->getContinue() ? 1 : 0,
                    'status' => $import->getStatus() ? 1 : 0
                )
            );
        }
    }
}
