<?php

namespace Project\Pimgento\Observer\Product;

use Magento\Framework\Event\Observer;
use Project\Pimgento\Observer\ObserverAbstract;
use Project\Pimgento\Api\Data\CronInterface;

/**
 * Class DeleteSkuNotFound
 * @package Project\Pimgento\Observer\Product
 * @author Synolia <contact@synolia.com>
 */
class DeleteSkuNotFound extends ObserverAbstract
{
    const COLUMN_CODE   = 'code';
    const COLUMN_PARENT = 'parent';

    protected $keyTaxClassId;

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Pimgento\Category\Model\Factory\Import $import */
        $import = $observer->getImport();
        $file   = $this->getFileFullPath($import->getFile());

        if ($this->fileSystem->fileExists($file) &&
            $import->getStep() == 0 &&
            $import->getAction() == CronInterface::ACTION_MASS
        ) {
            $data = $this->csvProcessor->getData($file);
            $data = $this->deleteLine($data);
            $this->csvProcessor->saveData($file, $data);
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    protected function deleteLine(array $data)
    {
        $keySku = array_search('sku', $data[0]);
        $skus   = array_column($data, $keySku);
        $skus   = $this->pimgentoRepository->getSku($skus);

        foreach ($data as $key => $line) {
            if ($key == 0 || !empty($skus[$line[$keySku]])) {
                continue;
            }
            unset($data[$key]);
        }
        return $data;
    }
}
