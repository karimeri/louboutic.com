<?php

namespace Project\Pimgento\Observer\Product;

use Magento\Framework\Event\Observer;
use Project\Pimgento\Observer\ObserverAbstract;
use Project\Core\Model\Environment;

/**
 * Class TaxClassId
 * @package Project\Pimgento\Observer\Category
 * @author Synolia <contact@synolia.com>
 */
class TaxClassId extends ObserverAbstract
{
    const COLUMN_CODE   = 'code';
    const COLUMN_PARENT = 'parent';

    protected $keyTaxClassId;
    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Pimgento\Category\Model\Factory\Import $import */
        $import = $observer->getImport();
        $file   = $this->getFileFullPath($import->getFile());

        if ($this->fileSystem->fileExists($file) &&
            $import->getStep() == 0 &&
            $this->environmentManager->getEnvironment() == Environment::US
        ) {
            $data    = $this->csvProcessor->getData($file);
            $columns = current($data);
            $data[0] = $this->addColumns($columns);
            $data    = $this->addData($data);
            $this->csvProcessor->saveData($file, $data);
        }
    }

    /**
     * @param array $columns
     * @return array
     */
    protected function addColumns(array $columns)
    {
        if (!$this->keyTaxClassId = array_search('tax_class_id', $columns)) {
            $this->keyTaxClassId = array_push($columns, 'tax_class_id') - 1;
        }
        return $columns;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function addData(array $data)
    {
        $keyAttributSet = array_search('family', $data[0]);
        $taxClassIds    = $this->helperConfig->getTaxClassIdByFamily();
        foreach ($data as $key => $line) {
            if ($key == 0) {
                continue;
            }
            $data[$key][$this->keyTaxClassId] = $taxClassIds[$line[$keyAttributSet]] ?? null;
        }
        return $data;
    }
}
