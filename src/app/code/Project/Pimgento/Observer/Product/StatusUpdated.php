<?php

namespace Project\Pimgento\Observer\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\ProductFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Io\File;
use Pimgento\Import\Helper\Config as HelperPimgento;
use Pimgento\Log\Model\Log as PimgentoLog;
use Project\Core\Manager\EnvironmentManager;
use Project\Pimgento\Helper\Config;
use Project\Pimgento\Model\PimgentoRepository;
use Project\Pimgento\Model\QueryRegistry;
use Project\Pimgento\Observer\ObserverAbstract;

/**
 * Class StatusUpdated
 * @package Project\Pimgento\Observer\Product
 */
class StatusUpdated extends ObserverAbstract
{

    /**
     * @var \Pimgento\Product\Model\Factory\Import
     */
    protected $import;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $resourceProduct;

    /**
     * @var \Project\Pimgento\Model\QueryRegistry
     */
    protected $queryRegistry;

    protected $keyReactivated;

    /**
     * StatusUpdated constructor.
     * @param Csv $csvProcessor
     * @param Config $helperConfig
     * @param HelperPimgento $helperPimgento
     * @param File $fileSystem
     * @param PimgentoLog $pimgentoLog
     * @param EnvironmentManager $environmentManager
     * @param PimgentoRepository $pimgentoRepository
     * @param AttributeRepositoryInterface $attributeRepository
     * @param ProductFactory $productFactory
     * @param QueryRegistry $queryRegistry
     */
    public function __construct(
        Csv $csvProcessor,
        Config $helperConfig,
        HelperPimgento $helperPimgento,
        File $fileSystem,
        PimgentoLog $pimgentoLog,
        EnvironmentManager $environmentManager,
        PimgentoRepository $pimgentoRepository,
        AttributeRepositoryInterface $attributeRepository,
        ProductFactory $productFactory,
        QueryRegistry $queryRegistry
    ) {
        parent::__construct($csvProcessor, $helperConfig, $helperPimgento, $fileSystem, $pimgentoLog, $environmentManager, $pimgentoRepository);
        $this->attributeRepository = $attributeRepository;
        $this->resourceProduct = $productFactory->create();
        $this->queryRegistry = $queryRegistry;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    function execute(Observer $observer)
    {
        /** @var \Pimgento\Product\Model\Factory\Import $import */
        $import = $observer->getImport();
        $this->import = $observer->getImport();
        $file   = $this->getFileFullPath($import->getFile());

        if ($this->fileSystem->fileExists($file) &&
            $import->getStep() == 0
        ) {
            $data    = $this->csvProcessor->getData($file);
            if (is_array($data) && count($data)) {
                $columns = current($data);
                $data[0] = $this->addColumns($columns);
                $data = $this->addData($data);
                $this->csvProcessor->saveData($file, $data);
            }
        }
    }

    /**
     * @param array $columns
     * @return array
     */
    protected function addColumns(array $columns)
    {
        if (!$this->keyReactivated = array_search('reactivated', $columns)) {
            $this->keyReactivated = array_push($columns, 'reactivated') - 1;
        }
        return $columns;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    protected function addData(array $data)
    {
        $keyProductStatus = array_search('product_status', $data[0]);
        $keySku = array_search('sku', $data[0]);
        $skus   = array_column($data, $keySku);
        $existingSkus   = $this->pimgentoRepository->getSku($skus);

        $previousStatuses = $this->getPreviousStatuses($skus);

        foreach ($data as $key => $line) {
            if ($key == 0) {
                continue;
            }
            if (isset($existingSkus[$line[$keySku]]) &&
                $previousStatuses[$line[$keySku]] == 2 && !empty($data[$key][$keyProductStatus])) {
                // product re-activated (disabled > enabled)
                $data[$key][$this->keyReactivated] = 1;
            } elseif (isset($existingSkus[$line[$keySku]]) &&
                $previousStatuses[$line[$keySku]] == 1 && empty($data[$key][$keyProductStatus]))  {
                // product de-activated (enabled > disabled)
                $data[$key][$this->keyReactivated] = 2;
            } else {
                // existing products already activated
                $data[$key][$this->keyReactivated] = 0;
            }
        }
        return $data;
    }

    /**
     * @param array $skus
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    private function getPreviousStatuses($skus)
    {
        $attribute  = $this->attributeRepository->get(Product::ENTITY, 'status');
        $entityIds  = $this->pimgentoRepository->getProductIdsBySkus($skus);
        $rowIds     = $this->pimgentoRepository->getRowIdsByEntityIds(array_values($entityIds), Product::ENTITY);

        $connection = $this->resourceProduct->getConnection();
        $select = $connection->select()
            ->from(
                ['c' => $connection->getTableName(Product::ENTITY.'_entity_int')],
                ['row_id', 'value']
            )
            ->joinInner(
                ['p' => $connection->getTableName('catalog_product_entity')],
                'c.row_id = p.row_id',
                ['sku']
            )
            ->where('c.attribute_id = ?', $attribute->getAttributeId())
            ->where('c.row_id IN (?)', $rowIds)
            ->where('c.store_id = ?', 0);
        $result = $this->queryRegistry->processFetchAll($connection, $select);

        $previousStatuses = [];
        foreach ($result as $item) {
            $previousStatuses[$item['sku']] = $item['value'];
        }
        return $previousStatuses;
    }
}
