<?php

namespace Project\Pimgento\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AddFinalStep
 * @package Project\Pimgento\Observer\Product
 * @author Synolia <contact@synolia.com>
 */
class AddFinalStep implements ObserverInterface
{
    /**
     * @codingStandardsIgnoreStart
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $response = $observer->getResponse();
        $steps = $response->getData('final_steps');

        $newSteps = [
            [
                'comment' => __('Disable RMA eligibility'),
                'method'  => 'disableRmaEligibility'
            ],
            [
                'comment' => __('Enable products to websites'),
                'method'  => 'enableToWebsites'
            ],
            [
                'comment' => __('Enable/Disable specific attributes'),
                'method'  => 'enableSpecificAttributes'
            ],
            [
                'comment' => __('Enable/Disable product by price/store'),
                'method'  => 'disableProductByPrice'
            ],
            [
                'comment' => __('Send report'),
                'method'  => 'sendReport'
            ]
        ];

        $response->setData('final_steps', array_merge($steps, $newSteps));
    }
}