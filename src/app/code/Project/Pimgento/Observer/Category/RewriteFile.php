<?php

namespace Project\Pimgento\Observer\Category;

use Magento\Framework\Event\Observer;
use Project\Pimgento\Observer\ObserverAbstract;

/**
 * Class RewriteFile
 * @package Project\Pimgento\Observer\Category
 * @author Synolia <contact@synolia.com>
 */
class RewriteFile extends ObserverAbstract
{
    const COLUMN_CODE   = 'code';
    const COLUMN_PARENT = 'parent';

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Pimgento\Category\Model\Factory\Import $import */
        $import = $observer->getImport();
        $file   = $this->getFileFullPath($import->getFile());
        
        if ($this->fileSystem->fileExists($file) &&
            $import->getStep() == 0
        ) {
            $categoriesRoot     = $this->helperConfig->getCategoriesRoot();
            $akeneoCategoryMain = $this->helperConfig->getAkeneoCategoryMain();
            $data               = $this->csvProcessor->getData($file);
            $columns            = current($data);
            $keyParent          = array_search(self::COLUMN_PARENT, $columns);
            $keyCode            = array_search(self::COLUMN_CODE, $columns);
            foreach ($data as $key => $category) {
                if ($category[$keyCode] == $akeneoCategoryMain) {
                    unset($data[$key]);
                } elseif (in_array($category[$keyCode], $categoriesRoot)) {
                    $data[$key][$keyParent] = null;
                }
            }
            $this->csvProcessor->saveData($file, $data);
        }
    }
}
