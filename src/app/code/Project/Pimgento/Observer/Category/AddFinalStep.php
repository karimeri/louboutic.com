<?php

namespace Project\Pimgento\Observer\Category;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AddFinalStep
 * @package Project\Pimgento\Observer\Category
 * @author Synolia <contact@synolia.com>
 */
class AddFinalStep implements ObserverInterface
{
    /**
     * @codingStandardsIgnoreStart
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $response = $observer->getResponse();
        $steps    = $response->getData('final_steps');

        $newSteps = [
            [
                'comment' => __('Enable categories to websites'),
                'method'  => 'enableToWebsites'
            ],
            [
                'comment' => __('Clean custom cache'),
                'method'  => 'cleanCustomCache',
            ]
        ];

        $response->setData('final_steps', array_merge($steps, $newSteps));
    }
}