<?php

namespace Project\Pimgento\Plugin\Pimgento\Product\Model\Factory\Import;

use Pimgento\Product\Model\Factory\Import\Media;
use Project\Pimgento\Model\Product\Factory\Import\Media as ProjectPimgentoMedia;

/**
 * Class MediaPlugin
 * @package Project\Pimgento\Plugin\Pimgento\Product\Model\Factory\Import
 * @author Synolia <contact@synolia.com>
 */
class MediaPlugin
{
    /**
     * @var \Project\Pimgento\Model\Product\Factory\Import\Media
     */
    protected $media;

    /**
     * Media constructor.
     * @param \Project\Pimgento\Model\Product\Factory\Import\Media $media
     */
    public function __construct(ProjectPimgentoMedia $media)
    {
        $this->media = $media;
    }

    /**
     * @param \Pimgento\Product\Model\Factory\Import\Media $subject
     * @param $result
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterMediaUpdateDataBase(Media $subject, $result)
    {
        $this->media->disableGalleryImages();
        return $result;
    }
}
