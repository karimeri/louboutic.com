<?php

namespace Project\Pimgento\Plugin\Pimgento\Product\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Pimgento\Product\Helper\Media as BaseMedia;
use Project\Pimgento\Model\PimgentoRepository;
use Project\Pimgento\Helper\Config;

/**
 * Class MediaPlugin
 * @package Project\Pimgento\Plugin\Pimgento\Helper
 * @author Synolia <contact@synolia.com>
 */
class MediaPlugin
{
    /**
     * @var \Project\Pimgento\Model\PimgentoRepository
     */
    protected $pimgentoRepository;

    /**
     * @var \Project\Pimgento\Helper\Config
     */
    protected $config;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * Media constructor.
     * @param \Project\Pimgento\Model\PimgentoRepository $pimgentoRepository
     * @param \Project\Pimgento\Helper\Config $config
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     */
    public function __construct(
        PimgentoRepository $pimgentoRepository,
        Config $config,
        DirectoryList $directoryList
    ) {
        $this->pimgentoRepository = $pimgentoRepository;
        $this->config = $config;
        $this->directoryList = $directoryList;
    }

    /**
     * @param BaseMedia $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetFields(BaseMedia $subject, $result)
    {
        if (count($result) == 5) {
            $attributes = $this->config->getAttributesToAttachToImage();
            if (!$attributes) {
                return $result;
            }
            $attributes = $this->pimgentoRepository->getAttributeByAttributeCodes($attributes);
            foreach ($attributes as $attributeCode => $attributeId) {
                $result[$attributeCode] = [
                    'columns' => [$attributeCode.'-ecommerce'],
                    'attribute_id' => $attributeId
                ];
            }
        }
        return $result;
    }

    /**
     * @param \Pimgento\Product\Helper\Media $subject
     * @param callable $proceed
     * @param $filePath
     * phpcs:disable Ecg.Security.ForbiddenFunction.Found
     */
    public function aroundDeleteMediaFile(BaseMedia $subject, callable $proceed, $filePath)
    {
        if (!$filePath) {
            return;
        }
        $absolutePath = rtrim('/' . ltrim($filePath, '/'));
        // Synolia Change: Do not remove file if it's a directory
        if (file_exists($absolutePath) && !is_dir($absolutePath)) {
            unlink($absolutePath);
        }
    }
}
