<?php

namespace Project\Pimgento\Plugin\Pimgento\Entities\Model;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Pimgento\Entities\Model\Entities;
use Project\Pimgento\Helper\Config as ConfigHelper;

/**
 * Class EntitiesPlugin
 * @package Project\Pimgento\Plugin\Pimgento\Entities\Model
 */
class EntitiesPlugin
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    public function __construct(
        ConfigHelper $configHelper
    ) {
        $this->configHelper = $configHelper;
    }

    /**
     * @param Entities $subject
     * @param $tableSuffix
     * @param $entityTable
     * @param $values
     * @param $entityTypeId
     * @param $storeId
     * @param int $mode
     * @return array
     */
    public function beforeSetValues(
        Entities $subject,
        $tableSuffix,
        $entityTable,
        $values,
        $entityTypeId,
        $storeId,
        $mode = AdapterInterface::INSERT_ON_DUPLICATE
    ) {
        $replace_mode = $mode;
        if ($tableSuffix === 'category') {
            $attributesMapping = $this->configHelper->getCategoryAttributeMapping();

            if (!empty($values['is_active']) && !empty($attributesMapping)) {
                foreach ($attributesMapping as $attribute) {
                    $values[$attribute['magento_attribute']] = $attribute['pim_attribute'];
                }
                $replace_mode = AdapterInterface::INSERT_ON_DUPLICATE;
            }
        }


        return [$tableSuffix, $entityTable, $values, $entityTypeId, $storeId, $replace_mode];
    }
}
