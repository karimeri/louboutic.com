<?php

namespace Project\PreOrder\Model;

use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\CatalogInventory\Model\Spi\StockStateProviderInterface;

/**
 * Class StockState
 * @package Project\PreOrder\Model
 * @author Synolia <contact@synolia.com>
 */
class StockState implements StockStateInterface
{
    /**
     * @var \Magento\CatalogInventory\Model\Spi\StockStateProviderInterface
     */
    protected $stockStateProvider;

    /**
     * @var \Project\PreOrder\Model\StockRegistryProvider
     */
    protected $stockRegistryProvider;

    /**
     * @var \Magento\CatalogInventory\Api\StockConfigurationInterface
     */
    protected $stockConfiguration;

    /**
     * StockState constructor.
     * @param \Magento\CatalogInventory\Model\Spi\StockStateProviderInterface $stockStateProvider
     * @param \Project\PreOrder\Model\StockRegistryProvider $stockRegistryProvider
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     */
    public function __construct(
        StockStateProviderInterface $stockStateProvider,
        StockRegistryProvider $stockRegistryProvider,
        StockConfigurationInterface $stockConfiguration
    ) {
        $this->stockStateProvider    = $stockStateProvider;
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->stockConfiguration    = $stockConfiguration;
    }

    /**
     * @param int $productId
     * @param int|null $scopeId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function verifyStock($productId, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        return $this->stockStateProvider->verifyStock($stockItem);
    }

    /**
     * @param int $productId
     * @param int|null $scopeId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function verifyNotification($productId, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        return $this->stockStateProvider->verifyNotification($stockItem);
    }

    /**
     * @param int $productId
     * @param float $qty
     * @param int|null $scopeId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkQty($productId, $qty, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        return $this->stockStateProvider->checkQty($stockItem, $qty);
    }

    /**
     * Returns suggested qty that satisfies qty increments and minQty/maxQty/minSaleQty/maxSaleQty conditions
     * or original qty if such value does not exist
     * @param int $productId
     * @param float $qty
     * @param int|null $scopeId
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function suggestQty($productId, $qty, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        return $this->stockStateProvider->suggestQty($stockItem, $qty);
    }

    /**
     * Retrieve stock qty whether product is composite or no
     * @param int $productId
     * @param int|null $scopeId
     * @return float
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStockQty($productId, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        return $this->stockStateProvider->getStockQty($stockItem);
    }

    /**
     * @param int $productId
     * @param float $qty
     * @param int|null $websiteId
     * @return \Magento\Framework\DataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkQtyIncrements($productId, $qty, $websiteId = null)
    {
        $websiteId = $websiteId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $websiteId);
        return $this->stockStateProvider->checkQtyIncrements($stockItem, $qty);
    }

    /**
     * @param int $productId
     * @param float $itemQty
     * @param float $qtyToCheck
     * @param float $origQty
     * @param int|null $scopeId
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkQuoteItemQty($productId, $itemQty, $qtyToCheck, $origQty, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        return $this->stockStateProvider->checkQuoteItemQty($stockItem, $itemQty, $qtyToCheck, $origQty);
    }
}
