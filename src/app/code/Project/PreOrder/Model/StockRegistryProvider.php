<?php
namespace Project\PreOrder\Model;

use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Api\Data\StockInterfaceFactory;
use Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory;
use Magento\CatalogInventory\Api\Data\StockStatusInterfaceFactory;
use Magento\CatalogInventory\Model\StockRegistryStorage;
use Magento\CatalogInventory\Model\ResourceModel\Stock as StockResource;
use Project\PreOrder\Model\ResourceModel\Stock\Item as StockItemResource;
use Project\PreOrder\Model\ResourceModel\Stock\Status as StockStatusResource;
use Project\PreOrder\Helper\Config;

/***
 * Class StockRegistryProvider
 * @package Project\PreOrder\Model
 * @author Synolia <contact@synolia.com>
 */
class StockRegistryProvider implements StockRegistryProviderInterface
{
    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $config;

    /**
     * @var \Magento\CatalogInventory\Api\Data\StockInterfaceFactory
     */
    protected $stockFactory;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistryStorage
     */
    protected $stockRegistry;

    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock
     */
    protected $stockResource;

    /**
     * @var \Project\PreOrder\Model\ResourceModel\Stock\Item
     */
    protected $stockItemResource;

    /**
     * @var \Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory
     */
    protected $stockItemFactory;

    /**
     * @var \Project\PreOrder\Model\ResourceModel\Stock\Status
     */
    protected $stockStatusResource;

    /**
     * @var \Magento\CatalogInventory\Api\Data\StockStatusInterfaceFactory
     */
    protected $stockStatusFactory;

    /**
     * StockRegistryProvider constructor.
     * @param \Project\PreOrder\Helper\Config $config
     * @param \Magento\CatalogInventory\Api\Data\StockInterfaceFactory $stockFactory
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory $stockItemFactory
     * @param \Magento\CatalogInventory\Api\Data\StockStatusInterfaceFactory $stockStatusFactory
     * @param \Magento\CatalogInventory\Model\StockRegistryStorage $stockRegistry
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock $stockResource
     * @param \Project\PreOrder\Model\ResourceModel\Stock\Item $stockItemResource
     * @param \Project\PreOrder\Model\ResourceModel\Stock\Status $stockStatusResource
     */
    public function __construct(
        Config $config,
        StockInterfaceFactory $stockFactory,
        StockItemInterfaceFactory $stockItemFactory,
        StockStatusInterfaceFactory $stockStatusFactory,
        StockRegistryStorage $stockRegistry,
        StockResource $stockResource,
        StockItemResource $stockItemResource,
        StockStatusResource $stockStatusResource
    ) {
        $this->config              = $config;
        $this->stockFactory        = $stockFactory;
        $this->stockItemFactory    = $stockItemFactory;
        $this->stockRegistry       = $stockRegistry;
        $this->stockResource       = $stockResource;
        $this->stockItemResource   = $stockItemResource;
        $this->stockStatusResource = $stockStatusResource;
        $this->stockStatusFactory  = $stockStatusFactory;
    }

    /**
     * @param int $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockInterface
     */
    public function getStock($scopeId)
    {
        $stock = $this->stockRegistry->getStock($this->getKey($scopeId));
        if (is_null($stock)) {
            $stock = $this->stockFactory->create();
            $this->stockResource->load($stock, $this->getStockName($scopeId), 'stock_name');
            if ($stock->getStockId()) {
                $this->stockRegistry->setStock($this->getKey($scopeId), $stock);
            }
        }
        return $stock;
    }

    /**
     * @param int $productId
     * @param int $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStockItem($productId, $scopeId)
    {
        $stockItem = $this->stockRegistry->getStockItem($productId, $this->getKey($scopeId));
        if (is_null($stockItem)) {
            $stockItem = $this->stockItemFactory->create();
            $this->stockItemResource->loadByProductId($stockItem, $productId, $scopeId);
            if ($stockItem->getItemId()) {
                $this->stockRegistry->setStockItem($productId, $this->getKey($scopeId), $stockItem);
            }
        }
        return $stockItem;
    }

    /**
     * @param int $productId
     * @param int $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockStatusInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStockStatus($productId, $scopeId)
    {
        $stockStatus = $this->stockRegistry->getStockStatus($productId, $this->getKey($scopeId));
        if (is_null($stockStatus)) {
            $stockStatus = $this->stockStatusFactory->create();
            $this->stockStatusResource->loadByProductId($stockStatus, $productId, $scopeId);
            if ($stockStatus->getStockId()) {
                $this->stockRegistry->setStockStatus($productId, $this->getKey($scopeId), $stockStatus);
            }
        }
        return $stockStatus;
    }

    /**
     * @param int $scopeId
     * @return null|string
     */
    protected function getStockName($scopeId)
    {
        return $this->config->getStockName($scopeId);
    }

    /**
     * @param $scopId
     * @return string
     */
    protected function getKey($scopId)
    {
        return "$scopId-P";
    }
}
