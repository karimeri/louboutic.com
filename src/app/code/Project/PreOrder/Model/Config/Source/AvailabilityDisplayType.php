<?php
namespace Project\PreOrder\Model\Config\Source;

/**
 * Class DateDisplayType
 * @package Project\PreOrder\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class AvailabilityDisplayType
{
    /**
     * Options getter
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'soon_availability', 'label' => __('Soon available')],
            ['value' => 'available_from', 'label' => __('Available from')]
        ];
    }

    /**
     * Get options in "key-value" format
     * @return array
     */
    public function toArray()
    {
        return ['soon_availability' => __('Soon available'), 'available_from' => __('Available from')];
    }
}
