<?php
namespace Project\PreOrder\Model;

use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockManagementInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Model\ResourceModel\Stock as ResourceStock;

/**
 * Class StockManagement
 * @package Project\PreOrder\Model
 * @author Synolia <contact@synolia.com>
 */
class StockManagement implements StockManagementInterface
{
    /**
     * @var \Project\PreOrder\Model\StockRegistryProvider
     */
    protected $stockRegistryProvider;

    /**
     * @var \Project\PreOrder\Model\StockState
     */
    protected $stockState;

    /**
     * @var \Project\PreOrder\Model\Configuration
     */
    protected $stockConfiguration;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock
     */
    protected $resource;

    /**
     * @var \Project\PreOrder\Model\StockRepository
     */
    protected $qtyCounter;

    /**
     * StockManagement constructor.
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock $stockResource
     * @param \Project\PreOrder\Model\StockRegistryProvider $stockRegistryProvider
     * @param \Project\PreOrder\Model\StockState $stockState
     * @param \Project\PreOrder\Model\Configuration $stockConfiguration
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Project\PreOrder\Model\StockRepository $qtyCounter
     */
    public function __construct(
        ResourceStock $stockResource,
        StockRegistryProvider $stockRegistryProvider,
        StockState $stockState,
        Configuration $stockConfiguration,
        ProductRepositoryInterface $productRepository,
        StockRepository $qtyCounter
    ) {
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->stockState            = $stockState;
        $this->stockConfiguration    = $stockConfiguration;
        $this->productRepository     = $productRepository;
        $this->qtyCounter            = $qtyCounter;
        $this->resource              = $stockResource;
    }

    /**
     * Subtract product qtys from stock.
     * Return array of items that require full save
     * @param array $items
     * @param int|null $websiteId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function registerProductsSale(array $items, $websiteId = null)
    {
        $this->getResource()->beginTransaction();
        $lockedItems   = $this->getResource()->lockProductsStock(array_keys($items), $websiteId);
        $fullSaveItems = $registeredItems = [];

        foreach ($lockedItems as $key => $lockedItemRecord) {
            $lockedItemRecord['product_id'] = $key;
            $productId = $lockedItemRecord['product_id'];
            /** @var StockItemInterface $stockItem */
            $orderedQty     = $items[$productId];
            $stockItem      = $this->stockRegistryProvider->getStockItem($productId, $websiteId);
            $canSubtractQty = $stockItem->getItemId() && $this->canSubtractQty($stockItem);
            if (!$canSubtractQty || !$this->stockConfiguration->isQty($lockedItemRecord['type_id'])) {
                continue;
            }
            if (!$stockItem->hasAdminArea()
                && !$this->stockState->checkQty($productId, $orderedQty, $stockItem->getWebsiteId())
            ) {
                $this->getResource()->rollBack();
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Not all of your products are available in the requested quantity.')
                );
            }
            if ($this->canSubtractQty($stockItem)) {
                $stockItem->setQty($stockItem->getQty() - $orderedQty);
            }
            $registeredItems[$productId] = $orderedQty;
            if (!$this->stockState->verifyStock($productId, $stockItem->getWebsiteId())
                || $this->stockState->verifyNotification(
                    $productId,
                    $stockItem->getWebsiteId()
                )
            ) {
                $fullSaveItems[] = $stockItem;
            }
        }
        $this->qtyCounter->correctItemsQty($registeredItems, $websiteId, '-', true);
        $this->getResource()->commit();
        return $fullSaveItems;
    }

    /**
     * @param array $items
     * @param int|null $websiteId
     * @return bool
     */
    public function revertProductsSale(array $items, $websiteId = null)
    {
        $websiteId = $websiteId ?? $this->stockConfiguration->getDefaultScopeId();
        $this->qtyCounter->correctItemsQty($items, $websiteId, '+', true);
        return true;
    }

    /**
     * Get back to stock (when order is canceled or whatever else)
     * @param int $productId
     * @param float $qty
     * @param int|null $scopeId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function backItemQty($productId, $qty, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockItem = $this->stockRegistryProvider->getStockItem($productId, $scopeId);
        if ($stockItem->getItemId() && $this->stockConfiguration->isQty($this->getProductType($productId))) {
            if ($this->canSubtractQty($stockItem)) {
                $stockItem->setQty($stockItem->getQty() + $qty);
            }
            if ($this->stockConfiguration->getCanBackInStock($stockItem->getStoreId()) &&
                $stockItem->getQty() > $stockItem->getMinQty()
            ) {
                $stockItem->setIsInStock(true);
                $stockItem->setStockStatusChangedAutomaticallyFlag(true);
            }
            $stockItem->save();
        }
        return true;
    }

    /**
     * Get Product type
     * @param int $productId
     * @return null|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getProductType($productId)
    {
        return $this->productRepository->getById($productId)->getTypeId();
    }

    /**
     * @return \Magento\CatalogInventory\Model\ResourceModel\Stock
     */
    protected function getResource()
    {
        return $this->resource;
    }

    /**
     * Check if is possible subtract value from item qty
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @return bool
     */
    protected function canSubtractQty(StockItemInterface $stockItem)
    {
        return $stockItem->getManageStock() && $this->stockConfiguration->canSubtractQty();
    }
}
