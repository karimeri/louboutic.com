<?php

namespace Project\PreOrder\Model;

use Magento\CatalogInventory\Model\ResourceModel\Stock as ResourceStock;
use Magento\CatalogInventory\Model\ResourceModel\QtyCounterInterface;
use Magento\CatalogInventory\Model\StockRegistry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\PreOrder\Helper\Config;
use Project\PreOrder\Model\StockRegistry as StockRegistryPreOrder;

/**
 * Class StockRepository
 * @package Project\PreOrder\Model
 * @author Synolia <contact@synolia.com>
 */
class StockRepository implements QtyCounterInterface
{
    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock
     */
    protected $resourceStock;

    /**
     * @var false|\Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $stockRegistryPreOrder;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $helperConfig;

    /**
     * StockRepository constructor.
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock $resourceStock
     * @param \Magento\CatalogInventory\Model\StockRegistry $stockRegistry
     * @param \Project\PreOrder\Model\StockRegistry $stockRegistryPreOrder
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Project\PreOrder\Helper\Config $helperConfig
     */
    public function __construct(
        ResourceStock $resourceStock,
        StockRegistry $stockRegistry,
        StockRegistryPreOrder $stockRegistryPreOrder,
        TimezoneInterface $timezone,
        StoreManagerInterface $storeManager,
        Config $helperConfig
    ) {
        $this->resourceStock = $resourceStock;
        $this->connection = $resourceStock->getConnection();
        $this->stockRegistry = $stockRegistry;
        $this->stockRegistryPreOrder = $stockRegistryPreOrder;
        $this->timezone = $timezone;
        $this->storeManager = $storeManager;
        $this->helperConfig = $helperConfig;
    }

    /**
     * @param array $items
     * @param int $websiteId
     * @param string $operator
     * @param bool $isPreorder
     */
    public function correctItemsQty(array $items, $websiteId, $operator, $isPreorder = false)
    {
        if (empty($items)) {
            return;
        }

        $conditions = [];
        foreach ($items as $productId => $qty) {
            $case = $this->connection->quoteInto('?', $productId);
            $result = $this->connection->quoteInto("qty{$operator}?", $qty);
            $conditions[$case] = $result;
        }

        $stock = $isPreorder ? $this->stockRegistryPreOrder->getStock() : $this->stockRegistry->getStock();
        $value = $this->connection->getCaseSql('product_id', $conditions, 'qty');
        $where = [
            'product_id IN (?)' => array_keys($items),
            'website_id = ?' => $isPreorder ? $stock->getWebsiteId() : $websiteId,
            'stock_id = ?' => $stock->getStockId()
        ];

        $this->connection->beginTransaction();

        $this->connection->update(
            $this->resourceStock->getTable('cataloginventory_stock_item'),
            ['qty' => $value],
            $where
        );

        $this->connection->commit();
    }

    /**
     * @param $productId
     * @return bool
     */
    public function isConfigurableOnPreOrder($productId)
    {
        $today = $this->timezone->date()->format('Y-m-d');
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        $stockName = $this->helperConfig->getStockName($websiteId);

        $subSelect = $this->connection->select()
            ->from(
                'catalog_product_relation',
                ['child_id']
            )
            ->where('parent_id = ?', $productId);

        $select = $this->connection->select()
            ->from(
                ['stock_item' => 'cataloginventory_stock_item'],
                ['item_id']
            )
            ->join(
                ['stock' => 'cataloginventory_stock'],
                'stock.stock_id = stock_item.stock_id AND stock.website_id = stock_item.website_id'
            )
            ->where('product_id IN (?)', new \Zend_Db_Expr($subSelect->assemble()))
            ->where('stock.stock_name = ?', $stockName)
            ->where('availability_date > ?', $today)
            ->where('qty > 0')
            ->where('is_in_stock = 1')
            ->limit(1);

            $result = $this->connection->fetchOne($select);

            return (bool) $result;
    }
}
