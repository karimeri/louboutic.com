<?php

namespace Project\PreOrder\Model\ResourceModel\Stock;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\CatalogInventory\Model\Indexer\Stock\Processor;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Model\Stock\Item as ModelItem;
use Project\PreOrder\Helper\Config;

/**
 * Class Item
 * @package Project\PreOrder\Model\ResourceModel\Stock
 * @author Synolia <contact@synolia.com>
 */
class Item extends AbstractDb
{
    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $config;

    /**
     * @var \Magento\CatalogInventory\Model\Indexer\Stock\Processor
     */
    protected $stockIndexerProcessor;

    /**
     * Item constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\CatalogInventory\Model\Indexer\Stock\Processor $processor
     * @param \Project\PreOrder\Helper\Config $config
     * @param string|null $connectionName
     */
    public function __construct(
        Context $context,
        Processor $processor,
        Config $config,
        string $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->stockIndexerProcessor = $processor;
        $this->config = $config;
    }


    /**
     * Resource initialization
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init(ModelItem::ENTITY, 'item_id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $item
     * @param mixed $value
     * @param string|null $field
     * @param int|null $scopeId
     * @return $this|\Magento\CatalogInventory\Model\ResourceModel\Stock\Item
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function load(AbstractModel $item, $value, $field = null, $scopeId = null)
    {
        $select = $this->getLoadSelect($field, $value, $item, $scopeId);
        $data   = $this->getConnection()->fetchRow($select);
        $data ? $item->setData($data) : $item->setStockQty(0); // see \Magento\CatalogInventory\Model\Stock\Item::getStockQty
        $this->_afterLoad($item);
        return $this;
    }

    /**
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $item
     * @param int $productId
     * @param int|null $scopeId
     * @return $this|\Magento\CatalogInventory\Model\ResourceModel\Stock\Item
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadByProductId(StockItemInterface $item, $productId, $scopeId = null)
    {
        $select = $this->getLoadSelect('product_id', $productId, $item, $scopeId)
            ->join(
                ['p' => $this->getTable('catalog_product_entity')],
                'product_id=p.entity_id',
                ['type_id']
            );
        $data   = $this->getConnection()->fetchRow($select);
        $data ? $item->setData($data) : $item->setStockQty(0); // see \Magento\CatalogInventory\Model\Stock\Item::getStockQty
        $this->_afterLoad($item);
        return $this;
    }

    /**
     * Retrieve select object and join it to product entity table to get type ids
     * @param string $field
     * @param int $value
     * @param \Magento\CatalogInventory\Model\Stock\Item $object
     * @param int|null $scopId
     * @return \Magento\Framework\DB\Select
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getLoadSelect($field, $value, $object, $scopId = null)
    {
        return parent::_getLoadSelect($field, $value, $object)
            ->join(
                ['stock' => $this->getTable('cataloginventory_stock')],
                $this->getMainTable().'.stock_id = stock.stock_id'
            )
            ->where('stock.stock_name = ?', $this->getStockName($scopId));
    }

    /**
     * Reindex CatalogInventory save event
     * @param AbstractModel $object
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _afterSave(AbstractModel $object)
    {
        parent::_afterSave($object);
        /** @var StockItemInterface $object */
        if ($this->processIndexEvents) {
            $this->stockIndexerProcessor->reindexRow($object->getProductId());
        }
        return $this;
    }


    /**
     * @param int $scopeId
     * @return null|string
     */
    protected function getStockName($scopeId = null)
    {
        return $this->config->getStockName($scopeId);
    }
}
