<?php

namespace Project\PreOrder\Model\ResourceModel\Stock;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\CatalogInventory\Api\Data\StockStatusInterface;
use Magento\Framework\Model\AbstractModel;
use Project\PreOrder\Helper\Config;

/**
 * Class Status
 * @package Project\PreOrder\Model\ResourceModel\Stock
 * @author Synolia <contact@synolia.com>
 */
class Status extends AbstractDb
{
    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $config;

    /**
     * Status constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Project\PreOrder\Helper\Config $config
     * @param string|null $connectionName
     */
    public function __construct(
        Context $context,
        Config $config,
        string $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->config = $config;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $status
     * @param mixed $value
     * @param string|null $field
     * @param int|null $scopeId
     * @return $this|\Magento\CatalogInventory\Model\ResourceModel\Stock\Item
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function load(AbstractModel $status, $value, $field = null, $scopeId = null)
    {
        $select = $this->getLoadSelect($field, $value, $status, $scopeId);
        $data   = $this->getConnection()->fetchRow($select);
        $status->setData($data ?: []);
        $this->_afterLoad($status);
        return $this;
    }

    /**
     * Resource initialization
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init('cataloginventory_stock_status', 'product_id');
    }

    /**
     * @param \Magento\CatalogInventory\Api\Data\StockStatusInterface $status
     * @param int $productId
     * @param int|null $scopeId
     * @return $this|\Magento\CatalogInventory\Model\ResourceModel\Stock\Item
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadByProductId(StockStatusInterface $status, $productId, $scopeId = null)
    {
        $mainTable = $this->getMainTable();
        $select = $this->getLoadSelect('product_id', $productId, $status, $scopeId)
            ->join(
                ['p' => $this->getTable('catalog_product_entity')],
                "$mainTable.product_id=p.entity_id",
                ['type_id']
            );
        $data = $this->getConnection()->fetchRow($select);
        $status->setData($data ?: []);
        $this->_afterLoad($status);
        return $this;
    }

    /**
     * Retrieve select object and join it to product entity table to get type ids
     * @param string $field
     * @param mixed $value
     * @param \Magento\CatalogInventory\Model\Stock\Item $object
     * @param int|null $scopeId
     * @return \Magento\Framework\DB\Select
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getLoadSelect($field, $value, $object, $scopeId = null)
    {
        $mainTable = $this->getMainTable();

        return parent::_getLoadSelect($field, $value, $object)
            ->join(
                ['stock' => $this->getTable('cataloginventory_stock')],
                $this->getMainTable().'.stock_id = stock.stock_id'
            )->join(
                ['item' => $this->getTable('cataloginventory_stock_item')],
                "$mainTable.website_id = item.website_id 
                 AND $mainTable.stock_id = item.stock_id 
                 AND $mainTable.product_id = item.product_id",
                ['availability_date']
            )
            ->where('stock.stock_name = ?', $this->getStockName($scopeId));
    }

    /**
     * @param int $scopeId
     * @return null|string
     */
    protected function getStockName($scopeId)
    {
        return $this->config->getStockName($scopeId);
    }
}
