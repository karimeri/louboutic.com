<?php

namespace Project\PreOrder\Model;

use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\CatalogInventory\Helper\Minsaleqty as MinsaleqtyHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CatalogInventory\Model\Configuration as BaseConfiguration;
use Magento\Store\Model\Store;

/**
 * Class Configuration
 * @package Project\PreOrder\Model
 * @author Synolia <contact@synolia.com>
 */
class Configuration extends BaseConfiguration
{
    /**
     * @var \Project\PreOrder\Model\StockRegistryProvider
     */
    protected $stockRegistryProvider;

    /**
     * Configuration constructor.
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $config
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\CatalogInventory\Helper\Minsaleqty $minsaleqtyHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Project\PreOrder\Model\StockRegistryProvider $stockRegistryProvider
     */
    public function __construct(
        ConfigInterface $config,
        ScopeConfigInterface $scopeConfig,
        MinsaleqtyHelper $minsaleqtyHelper,
        StoreManagerInterface $storeManager,
        StockRegistryProvider $stockRegistryProvider
    ) {
        parent::__construct($config, $scopeConfig, $minsaleqtyHelper, $storeManager);
        $this->stockRegistryProvider = $stockRegistryProvider;
    }

    /**
     * @return int
     */
    public function getDefaultScopeId()
    {
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        $stock     = $this->stockRegistryProvider->getStock($websiteId);
        return $stock->getStockId() ? $websiteId : Store::DEFAULT_STORE_ID;
    }
}
