<?php
namespace Project\PreOrder\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\CatalogInventory\Model\Stock\Status as StockStatus;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class StockRegistry
 * @package Project\PreOrder\Model
 * @author Synolia <contact@synolia.com>
 */
class StockRegistry implements StockRegistryInterface
{
    /**
     * @var \Project\PreOrder\Model\Configuration
     */
    protected $stockConfiguration;

    /**
     * @var \Project\PreOrder\Model\StockRegistryProvider
     */
    protected $stockRegistryProvider;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\CatalogInventory\Api\StockItemRepositoryInterface
     */
    protected $stockItemRepository;

    /**
     * @var \Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory
     */
    protected $criteriaFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * StockRegistry constructor.
     * @param \Project\PreOrder\Model\Configuration $stockConfiguration
     * @param \Project\PreOrder\Model\StockRegistryProvider $stockRegistryProvider
     * @param \Magento\CatalogInventory\Api\StockItemRepositoryInterface $stockItemRepository
     * @param \Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory $criteriaFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
    public function __construct(
        Configuration $stockConfiguration,
        StockRegistryProvider $stockRegistryProvider,
        StockItemRepositoryInterface $stockItemRepository,
        StockItemCriteriaInterfaceFactory $criteriaFactory,
        ProductFactory $productFactory,
        TimezoneInterface $timezone
    ) {
        $this->stockConfiguration    = $stockConfiguration;
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->stockItemRepository   = $stockItemRepository;
        $this->criteriaFactory       = $criteriaFactory;
        $this->productFactory        = $productFactory;
        $this->timezone              = $timezone;
    }

    /**
     * @param int|null $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockInterface
     */
    public function getStock($scopeId = null)
    {
        $scopeId = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        return $this->stockRegistryProvider->getStock($scopeId);
    }

    /**
     * @param int $productId
     * @param int|null $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStockItem($productId, $scopeId = null)
    {
        $scopeId = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        return $this->stockRegistryProvider->getStockItem($productId, $scopeId);
    }

    /**
     * @param string $productSku
     * @param int|null $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockItemBySku($productSku, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $productId = $this->resolveProductId($productSku);
        return $this->stockRegistryProvider->getStockItem($productId, $scopeId);
    }

    /**
     * @param int $productId
     * @param int|null $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockStatusInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStockStatus($productId, $scopeId = null)
    {
        $scopeId = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        return $this->stockRegistryProvider->getStockStatus($productId, $scopeId);
    }

    /**
     * @param string $productSku
     * @param int|null $scopeId
     * @return \Magento\CatalogInventory\Api\Data\StockStatusInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStockStatusBySku($productSku, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $productId = $this->resolveProductId($productSku);
        return $this->getStockStatus($productId, $scopeId);
    }

    /**
     * Retrieve Product stock status
     * @param int $productId
     * @param int|null $scopeId
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductStockStatus($productId, $scopeId = null)
    {
        $scopeId     = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockStatus = $this->getStockStatus($productId, $scopeId);
        return $stockStatus->getStockStatus();
    }

    /**
     * @param string $productSku
     * @param int|null $scopeId
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductStockStatusBySku($productSku, $scopeId = null)
    {
        $scopeId   = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $productId = $this->resolveProductId($productSku);
        return $this->getProductStockStatus($productId, $scopeId);
    }

    /**
     * @param int $scopeId
     * @param float $qty
     * @param int $currentPage
     * @param int $pageSize
     * @return \Magento\CatalogInventory\Api\Data\StockItemCollectionInterface|\Magento\CatalogInventory\Api\Data\StockStatusCollectionInterface
     */
    public function getLowStockItems($scopeId, $qty, $currentPage = 1, $pageSize = 0)
    {
        $criteria = $this->criteriaFactory->create();
        $criteria->setLimit($currentPage, $pageSize);
        $criteria->setScopeFilter($scopeId);
        $criteria->setStockFilter($this->stockRegistryProvider->getStock($scopeId));
        $criteria->setQtyFilter('<=', $qty);
        $criteria->addField('qty');
        return $this->stockItemRepository->getList($criteria);
    }

    /**
     * @param string $productSku
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateStockItemBySku($productSku, StockItemInterface $stockItem)
    {
        $productId     = $this->resolveProductId($productSku);
        $websiteId     = $stockItem->getWebsiteId() ?: null;
        $origStockItem = $this->getStockItem($productId, $websiteId);
        $data          = $stockItem->getData();
        if ($origStockItem->getItemId()) {
            unset($data['item_id']);
        }
        $origStockItem->addData($data);
        $origStockItem->setProductId($productId);
        return $this->stockItemRepository->save($origStockItem)->getItemId();
    }

    /**
     * @param string $productSku
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function resolveProductId($productSku)
    {
        $product   = $this->productFactory->create();
        $productId = $product->getIdBySku($productSku);
        if (!$productId) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
                __('Product with SKU "%1" does not exist', $productSku)
            );
        }
        return $productId;
    }

    /**
     * @param int $productId
     * @param int|null $scopeId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isInStock($productId, $scopeId = null)
    {
        $today       = $this->timezone->date()->format('Y-m-d');
        $scopeId     = $scopeId ?? $this->stockConfiguration->getDefaultScopeId();
        $stockStatus = $this->getStockStatus($productId, $scopeId);
        return (bool)$stockStatus->getStockStatus() == StockStatus::STATUS_IN_STOCK
            && $stockStatus->getAvailabilityDate() > $today;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isSalable(ProductInterface $product)
    {
        return $this->isInStock($product->getId(), $product->getStore()->getWebsiteId());
    }
}
