<?php

namespace Project\PreOrder\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

/**
 * Class InstallSchema
 * @package Project\PreOrder\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (!$connection->tableColumnExists($setup->getTable('cataloginventory_stock_item'), 'availability_date')) {
            $connection
                ->addColumn(
                    $setup->getTable('cataloginventory_stock_item'),
                    'availability_date',
                    [
                        'type'     => Table::TYPE_DATE,
                        'nullable' => true,
                        'default'  => null,
                        'comment'  => 'Resupply Date'
                    ]
                );
        }

        if (!$connection->tableColumnExists($setup->getTable('quote_item'), 'is_preorder')) {
            $connection
                ->addColumn(
                    $setup->getTable('quote_item'),
                    'is_preorder',
                    [
                        'type'     => Table::TYPE_BOOLEAN,
                        'default'  => false,
                        'comment'  => 'Is PreOrder'
                    ]
                );
        }

        if (!$connection->tableColumnExists($setup->getTable('sales_order_item'), 'is_preorder')) {
            $connection
                ->addColumn(
                    $setup->getTable('sales_order_item'),
                    'is_preorder',
                    [
                        'type'     => Table::TYPE_BOOLEAN,
                        'default'  => false,
                        'comment'  => 'Is PreOrder'
                    ]
                );
        }

        $installer->endSetup();
    }
}
