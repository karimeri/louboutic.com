<?php

namespace Project\PreOrder\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\WebsiteRepository;
use Magento\Store\Model\ScopeInterface;
use Synolia\MultiStock\Setup\StockSetupFactory;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Project\Core\Model\Environment;
use Project\Core\Manager\EnvironmentManager;
use Project\PreOrder\Helper\Config;

/**
 * Class InstallData
 * @package Project\PreOrder\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    protected $stockPreOrder = [
        Environment::EU => [
            [
                'website_id' => Store::DEFAULT_STORE_ID,
                'stock_name' => 'B52P'
            ]
        ],
        Environment::JP => [
            [
                'website_id' => Store::DEFAULT_STORE_ID,
                'stock_name' => 'JP77P'
            ]
        ],
        Environment::HK => [
            [
                'website_id' => Store::DEFAULT_STORE_ID,
                'stock_name' => 'JP77P'
            ]
        ],
        Environment::US => [
            [
                'website_id' => 'us',
                'stock_name' => 'B25P'
            ],
            [
                'website_id' => 'ca',
                'stock_name' => 'BB6P'
            ]
        ],
    ];

    /**
     * @var \Magento\Store\Model\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var \Synolia\MultiStock\Setup\StockSetup
     */
    protected $stockSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;

    /**
     * InstallData constructor.
     * @param \Magento\Store\Model\WebsiteRepository $websiteRepository
     * @param \Synolia\MultiStock\Setup\StockSetupFactory $setupFactory
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Synolia\Standard\Setup\ConfigSetupFactory $configSetupFactory
     */
    public function __construct(
        WebsiteRepository $websiteRepository,
        StockSetupFactory $setupFactory,
        EnvironmentManager $environmentManager,
        ConfigSetupFactory $configSetupFactory
    ) {
        $this->websiteRepository  = $websiteRepository;
        $this->stockSetup         = $setupFactory->create();
        $this->environmentManager = $environmentManager;
        $this->configSetupFactory = $configSetupFactory;
    }

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);
        $this->configSetup->saveConfig('cataloginventory/item_options/is_multistock', 1);
        $this->configSetup->saveConfig(Config::XML_PATH_PRE_ORDER_INCREMENT_ID_SUFFIX, '_PO');
        $this->createStocks();

        $setup->endSetup();
    }


    /**
     * Create Stock Name
     */
    public function createStocks()
    {
        $instance    = $this->environmentManager->getEnvironment();
        $stocks      = $this->stockPreOrder[$instance] ?? [];

        foreach ($stocks as $key => $stock) {
            if (is_string($stock['website_id'])) {
                try {
                    $website = $this->websiteRepository->get($stock['website_id']);
                    $stocks[$key]['website_id'] = $website->getId();
                    $this->configSetup->saveConfig(
                        'louboutin_wms/general/stock_code_preorder',
                        $stock['stock_name'],
                        ScopeInterface::SCOPE_WEBSITES,
                        $website->getId()
                    );
                } catch (\Exception $exception) {
                    continue;
                }
            } else {
                $this->configSetup->saveConfig('louboutin_wms/general/stock_code_preorder', $stock['stock_name']);
            }
        }

        if ($stocks) {
            $this->stockSetup->createStocks($stocks);
        }
    }
}
