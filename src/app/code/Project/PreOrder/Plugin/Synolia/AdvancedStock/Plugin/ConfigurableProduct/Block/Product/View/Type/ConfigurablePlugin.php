<?php

namespace Project\PreOrder\Plugin\Synolia\AdvancedStock\Plugin\ConfigurableProduct\Block\Product\View\Type;

use Project\PreOrder\Helper\Product as ProductPreOrderHelper;
use Synolia\AdvancedStock\Plugin\ConfigurableProduct\Block\Product\View\Type\ConfigurablePlugin as Configurable;
use Magento\ConfigurableProduct\Block\Product\View\Type\Configurable as BaseConfigurable;
use Project\PreOrder\Helper\Config as PreOrderHelper;
use Project\PreOrder\Model\StockRegistry;

/**
 * Class ConfigurablePlugin
 * @package Project\PreOrder\Plugin\Magento\ConfigurableProduct\Block\Product\View\Type
 * @author Synolia <contact@synolia.com>
 */
class ConfigurablePlugin
{
    /**
     * @var \Project\PreOrder\Helper\Product
     */
    protected $productPreOrderHelper;

    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $preOrderHelper;

    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $stockRegistry;

    /**
     * ConfigurablePlugin constructor.
     * @param \Project\PreOrder\Helper\Product $productPreOrderHelper
     * @param \Project\PreOrder\Helper\Config $preOrderHelper
     * @param \Project\PreOrder\Model\StockRegistry $stockRegistry
     */
    public function __construct(
        ProductPreOrderHelper $productPreOrderHelper,
        PreOrderHelper $preOrderHelper,
        StockRegistry $stockRegistry
    ) {
        $this->productPreOrderHelper = $productPreOrderHelper;
        $this->preOrderHelper        = $preOrderHelper;
        $this->stockRegistry         = $stockRegistry;
    }

    /**
     * @param \Synolia\AdvancedStock\Plugin\ConfigurableProduct\Block\Product\View\Type\ConfigurablePlugin $subject
     * @param $result
     * @param \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $configurable
     * @return string|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterGetOptionStocks(Configurable $subject, $result, BaseConfigurable $configurable)
    {
        $stocks     = $result;
        $isPreOrder = false;

        foreach ($configurable->getAllowProducts() as $product) {
            $productId = $product->getId();
            if (!empty($stocks[$productId])) {
                if (!$stocks[$productId]['saleable']) {
                    if ($isPreOrder = $this->productPreOrderHelper->isPreOrder($product)) {
                        $stocks[$productId]['label'] = __('Pre Order');
                        $stocks[$productId]['preorder_txt'] = sprintf(
                            __($this->preOrderHelper->getDisplayText()),
                            $this->productPreOrderHelper->formatDate(
                                $this->stockRegistry->getStockStatus($productId)->getAvailabilityDate()
                            )
                        );
                    }
                }
            }
            $stocks[$productId]['is_preorder'] = $isPreOrder;
        }
        return $stocks;
    }
}
