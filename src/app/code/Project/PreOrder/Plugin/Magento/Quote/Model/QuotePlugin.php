<?php

namespace Project\PreOrder\Plugin\Magento\Quote\Model;

use Magento\Framework\App\Request\Http;
use Magento\Quote\Model\Quote;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type\AbstractType;

/**
 * Class QuotePlugin
 * @package Project\PreOrder\Plugin\Magento\Quote\Model
 * @author Synolia <contact@synolia.com>
 */
class QuotePlugin
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * QuotePlugin constructor.
     * @param \Magento\Framework\App\Request\Http $request
     */
    public function __construct(Http $request)
    {
        $this->request = $request;
    }

    /**
     * @param \Magento\Quote\Model\Quote $subject
     * @param \Closure $proceed
     * @param \Magento\Catalog\Model\Product $product
     * @param null $request
     * @param string $processMode
     * @return mixed
     */
    public function aroundAddProduct(
        Quote $subject,
        \Closure $proceed,
        Product $product,
        $request = null,
        $processMode = AbstractType::PROCESS_MODE_FULL
    ) {
        if ($this->request->getParam('is_preorder')) {
            $product->setSalable(true);
        }

        $parentItem = $proceed($product, $request, $processMode);

        $product->unsetData('salable');

        return $parentItem;
    }
}
