<?php
namespace Project\PreOrder\Plugin\Magento\Quote\Model\Quote\Item;

use Magento\Quote\Model\Quote\Item\ToOrderItem;
use Magento\Quote\Model\Quote\Item\AbstractItem;

/**
 * Class ToOrderItemPlugin
 * @package Project\PreOrder\Plugin\Magento\Quote\Model\Quote\Item
 * @author Synolia <contact@synolia.com>
 */
class ToOrderItemPlugin
{
    /**
     * @param \Magento\Quote\Model\Quote\Item\ToOrderItem $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param array $additional
     * @return mixed
     */
    public function aroundConvert(
        ToOrderItem $subject,
        \Closure $proceed,
        AbstractItem $item,
        $additional = []
    ) {
        $orderItem = $proceed($item, $additional);
        $orderItem->setIsPreorder($item->getIsPreorder());
        return $orderItem;
    }
}
