<?php

namespace Project\PreOrder\Plugin\Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option;
use Magento\Quote\Model\Quote\Item\Option as ItemOption;
use Magento\Quote\Model\Quote\Item;
use Project\PreOrder\Model\StockRegistry;

/**
 * Class OptionPlugin
 * @package Project\PreOrder\Plugin\Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer
 * @author Synolia <contact@synolia.com>
 */
class OptionPlugin
{
    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * OptionPlugin constructor.
     * @param \Project\PreOrder\Model\StockRegistry $stockRegistry
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        StockRegistry $stockRegistry,
        Registry $registry
    ) {
        $this->stockRegistry = $stockRegistry;
        $this->registry = $registry;
    }

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item\Option $option
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundGetStockItem(Option $subject, \Closure $proceed, ItemOption $option, Item $quoteItem)
    {
        if (!$quoteItem->getIsPreorder()) {
            return $proceed($option, $quoteItem);
        }

        $stockItem = $this->stockRegistry->getStockItem(
            $option->getProduct()->getId(),
            $quoteItem->getStore()->getWebsiteId()
        );

        if (!$stockItem->getItemId()) {
            throw new LocalizedException(__('The stock item for Product in option is not valid.'));
        }

        $stockItem->setIsChildItem(true);
        $stockItem->setSuppressCheckQtyIncrements(true);
        
        return $stockItem;
    }

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option $subject
     * @param \Magento\Quote\Model\Quote\Item\Option $option
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @param float $qty
     * @return array
     */
    public function beforeInitialize(Option $subject, ItemOption $option, Item $quoteItem, $qty)
    {
        if ($quoteItem->getIsPreorder()) {
            $this->registry->register('is_preorder', true);
        }
        return [$option, $quoteItem, $qty];
    }
}
