<?php

namespace Project\PreOrder\Plugin\Magento\CatalogInventory\Model;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\CatalogInventory\Model\StockStateProvider;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Project\PreOrder\Model\StockRegistryProvider;
use Project\PreOrder\Helper\Product as HelperPreOrderProduct;

/**
 * Class StockStateProviderPlugin
 * @package Project\PreOrder\Plugin\Magento\CatalogInventory\Model
 * @author Synolia <contact@synolia.com>
 */
class StockStateProviderPlugin
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Project\PreOrder\Model\StockRegistryProvider
     */
    protected $stockRegistryProvider;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Project\PreOrder\Helper\Product
     */
    protected $helperPreOrderProduct;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * StockStateProviderPlugin constructor.
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Project\PreOrder\Model\StockRegistryProvider $stockRegistryProvider
     * @param \Magento\Framework\Registry $registry
     * @param \Project\PreOrder\Helper\Product $helperPreOrderProduct
     */
    public function __construct(
        StockRegistryProvider $stockRegistryProvider,
        HelperPreOrderProduct $helperPreOrderProduct,
        Http $request,
        Registry $registry
    ) {
        $this->stockRegistryProvider = $stockRegistryProvider;
        $this->helperPreOrderProduct = $helperPreOrderProduct;
        $this->request               = $request;
        $this->registry              = $registry;
    }

    /**
     * @param \Magento\CatalogInventory\Model\StockStateProvider $subject
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @param float $qty
     * @param $summaryQty
     * @param int $origQty
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function beforeCheckQuoteItemQty(
        StockStateProvider $subject,
        StockItemInterface $stockItem,
        $qty,
        $summaryQty,
        $origQty = 0
    ) {
        $preOrderOnFront = $this->request->get('is_preorder') || $this->registry->registry('is_preorder');
        $preOrderOnBack  = $this->helperPreOrderProduct->isPreOrderAdmin($stockItem->getProductId());

        if ($preOrderOnFront || $preOrderOnBack) {
            $preOrderStockItem = $this->stockRegistryProvider->getStockItem(
                $stockItem->getProductId(),
                $stockItem->getWebsiteId()
            );
            $stockItem = $preOrderStockItem->getStockId() ? $preOrderStockItem : $stockItem;
            $this->registry->unregister('is_preorder');
        }
        return [$stockItem, $qty, $summaryQty, $origQty];
    }
}
