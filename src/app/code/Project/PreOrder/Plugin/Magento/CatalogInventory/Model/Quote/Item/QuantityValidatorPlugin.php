<?php

namespace Project\PreOrder\Plugin\Magento\CatalogInventory\Model\Quote\Item;

use Magento\Catalog\Model\Product\Type;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Item;
use Project\PreOrder\Helper\Product as HelperPreOrderProduct;
use Project\PreOrder\Model\StockRegistry as PreOrderStockRegistry;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Model\Stock;
use Magento\CatalogInventory\Helper\Data;
use Project\PreOrder\Model\StockState;

/**
 * Class QuantityValidatorPlugin
 * @package Project\PreOrder\Plugin\Magento\CatalogInventory\Model\Quote\Item
 * @author Synolia <contact@synolia.com>
 */
class QuantityValidatorPlugin
{
    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $preOrderStockRegistry;

    /**
     * @var \Project\PreOrder\Helper\Product
     */
    protected $helperPreOrderProduct;

    /**
     * @var \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option
     */
    protected $optionInitializer;

    /**
     * @var \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem
     */
    protected $stockItemInitializer;

    /**
     * @var \Project\PreOrder\Model\StockState
     */
    protected $stockStatePreOrder;

    /**
     * QuantityValidatorPlugin constructor.
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\Option $optionInitializer
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem $stockItemInitializer
     * @param \Project\PreOrder\Model\StockRegistry $preOrderStockRegistry
     * @param \Project\PreOrder\Helper\Product $helperPreOrderProduct
     * @param \Project\PreOrder\Model\StockState $stockStatePreOrder
     */
    public function __construct(
        Option $optionInitializer,
        StockItem $stockItemInitializer,
        PreOrderStockRegistry $preOrderStockRegistry,
        HelperPreOrderProduct $helperPreOrderProduct,
        StockState $stockStatePreOrder
    ) {
        $this->preOrderStockRegistry = $preOrderStockRegistry;
        $this->helperPreOrderProduct = $helperPreOrderProduct;
        $this->optionInitializer = $optionInitializer;
        $this->stockItemInitializer = $stockItemInitializer;
        $this->stockStatePreOrder = $stockStatePreOrder;
    }

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Event\Observer $observer
     * @return mixed
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * phpcs:disable Generic.Metrics.CyclomaticComplexity
     */
    public function aroundValidate(
        QuantityValidator $subject,
        \Closure $proceed,
        Observer $observer
    ) {
        /* @var $quoteItem Item */
        $quoteItem = $observer->getEvent()->getItem();
        if (!$quoteItem ||
            !$quoteItem->getProductId() ||
            !$quoteItem->getQuote() ||
            $quoteItem->getQuote()->getIsSuperMode() || $quoteItem->getProductType() != Type::TYPE_SIMPLE
        ) {
            return;
        }
        $product = $quoteItem->getProduct();

        if (!$this->helperPreOrderProduct->isPreOrderByType($product)) {
            return $proceed($observer);
        }

        $qty = $quoteItem->getQty();

        /* @var \Magento\CatalogInventory\Model\Stock\Item $stockItem */
        $stockItem = $this->preOrderStockRegistry->getStockItem(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );
        if (!$stockItem instanceof StockItemInterface) {
            throw new LocalizedException(__('The stock item for Product is not valid.'));
        }

        /* @var \Magento\CatalogInventory\Api\Data\StockStatusInterface $stockStatus */
        $stockStatus = $this->preOrderStockRegistry->getStockStatus(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );

        if ($stockStatus) {
            if ($stockStatus->getStockStatus() === Stock::STOCK_OUT_OF_STOCK) {
                $quoteItem->addErrorInfo(
                    'cataloginventory',
                    Data::ERROR_QTY,
                    __('This product is out of stock.')
                );
                $quoteItem->getQuote()->addErrorInfo(
                    'stock',
                    'cataloginventory',
                    Data::ERROR_QTY,
                    __('Some of the products are out of stock.')
                );
                return;
            } else {
                // Delete error from item and its quote, if it was set due to item out of stock
                $this->removeErrorsFromQuoteAndItem($quoteItem, Data::ERROR_QTY);
            }
        }

        /**
         * Check item for options
         */
        if (($options = $quoteItem->getQtyOptions()) && $qty > 0) {
            $qty = $product->getTypeInstance()->prepareQuoteItemQty($qty, $product);
            $quoteItem->setData('qty', $qty);
            if ($stockStatus) {
                $result = $this->stockStatePreOrder->checkQtyIncrements(
                    $product->getId(),
                    $qty,
                    $product->getStore()->getWebsiteId()
                );

                if ($result->getHasError()) {
                    $quoteItem->addErrorInfo(
                        'cataloginventory',
                        Data::ERROR_QTY_INCREMENTS,
                        $result->getMessage()
                    );

                    $quoteItem->getQuote()->addErrorInfo(
                        $result->getQuoteMessageIndex(),
                        'cataloginventory',
                        Data::ERROR_QTY_INCREMENTS,
                        $result->getQuoteMessage()
                    );
                } else {
                    // Delete error from item and its quote, if it was set due to qty problems
                    $this->removeErrorsFromQuoteAndItem(
                        $quoteItem,
                        Data::ERROR_QTY_INCREMENTS
                    );
                }
            }
            // variable to keep track if we have previously encountered an error in one of the options
            $removeError = true;

            foreach ($options as $option) {
                $result = $this->optionInitializer->initialize($option, $quoteItem, $qty);
                if ($result->getHasError()) {
                    $option->setHasError(true);
                    //Setting this to false, so no error statuses are cleared
                    $removeError = false;
                    $this->addErrorInfoToQuote($result, $quoteItem, $removeError);
                }
            }
            if ($removeError) {
                $this->removeErrorsFromQuoteAndItem($quoteItem, Data::ERROR_QTY);
            }
        } else {
            if ($quoteItem->getParentItem() === null) {
                $result = $this->stockItemInitializer->initialize($stockItem, $quoteItem, $qty);
                if ($result->getHasError()) {
                    $this->addErrorInfoToQuote($result, $quoteItem);
                } else {
                    $this->removeErrorsFromQuoteAndItem($quoteItem, Data::ERROR_QTY);
                }
            }
        }

        return $this;
    }

    /**
     * Add error information to Quote Item
     *
     * @param \Magento\Framework\DataObject $result
     * @param Item $quoteItem
     * @return void
     */
    private function addErrorInfoToQuote($result, Item $quoteItem)
    {
        $quoteItem->addErrorInfo(
            'cataloginventory',
            Data::ERROR_QTY,
            $result->getMessage()
        );

        $quoteItem->getQuote()->addErrorInfo(
            $result->getQuoteMessageIndex(),
            'cataloginventory',
            Data::ERROR_QTY,
            $result->getQuoteMessage()
        );
    }

    /**
     * Removes error statuses from quote and item, set by this observer
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param int $code
     * @return void
     */
    protected function removeErrorsFromQuoteAndItem($item, $code)
    {
        if ($item->getHasError()) {
            $params = ['origin' => 'cataloginventory', 'code' => $code];
            $item->removeErrorInfosByParams($params);
        }

        $quote = $item->getQuote();
        if ($quote->getHasError()) {
            $quoteItems = $quote->getItemsCollection();
            $canRemoveErrorFromQuote = true;
            foreach ($quoteItems as $quoteItem) {
                if ($quoteItem->getItemId() == $item->getItemId()) {
                    continue;
                }

                $errorInfos = $quoteItem->getErrorInfos();
                foreach ($errorInfos as $errorInfo) {
                    if ($errorInfo['code'] == $code) {
                        $canRemoveErrorFromQuote = false;
                        break;
                    }
                }

                if (!$canRemoveErrorFromQuote) {
                    break;
                }
            }

            if ($canRemoveErrorFromQuote) {
                $params = ['origin' => 'cataloginventory', 'code' => $code];
                $quote->removeErrorInfosByParams(null, $params);
            }
        }
    }
}
