<?php

namespace Project\PreOrder\Plugin\Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer;

use Magento\Framework\Registry;
use Magento\CatalogInventory\Api\Data\StockItemInterface;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem;
use Magento\Quote\Model\Quote\Item;
use Project\PreOrder\Model\StockRegistry;

/**
 * Class StockItemPlugin
 * @package Project\PreOrder\Plugin\Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer
 * @author Synolia <contact@synolia.com>
 */
class StockItemPlugin
{
    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * StockItemPlugin constructor.
     * @param \Project\PreOrder\Model\StockRegistry $stockRegistry
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        StockRegistry $stockRegistry,
        Registry $registry
    ) {
        $this->stockRegistry = $stockRegistry;
        $this->registry      = $registry;
    }

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem $subject
     * @param \Magento\CatalogInventory\Api\Data\StockItemInterface $stockItem
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @param float $qty
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeInitialize(
        StockItem $subject,
        StockItemInterface $stockItem,
        Item $quoteItem,
        $qty
    ) {
        if (!$quoteItem->getIsPreorder()) {
            return [$stockItem, $quoteItem, $qty];
        }

        $stockItem = $this->stockRegistry->getStockItem(
            $quoteItem->getProduct()->getId(),
            $quoteItem->getStore()->getWebsiteId()
        );

        $this->registry->register('is_preorder', true);

        return [$stockItem, $quoteItem, $qty];
    }
}
