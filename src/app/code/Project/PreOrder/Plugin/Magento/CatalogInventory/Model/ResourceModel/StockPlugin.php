<?php

namespace Project\PreOrder\Plugin\Magento\CatalogInventory\Model\ResourceModel;

use Magento\CatalogInventory\Model\ResourceModel\Stock;
use Synolia\MultiStock\Helper\Config;
use Project\PreOrder\Model\StockRepository;

/**
 * Class StockPlugin
 * @package Project\PreOrder\Plugin\Magento\CatalogInventory\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class StockPlugin
{
    /**
     * @var \Synolia\MultiStock\Helper\Config
     */
    protected $config;

    /**
     * @var \Project\PreOrder\Model\StockRepository
     */
    protected $stockRepository;


    /**
     * StockPlugin constructor.
     * @param \Synolia\MultiStock\Helper\Config $config
     * @param \Project\PreOrder\Model\StockRepository $stockRepository
     */
    public function __construct(
        Config $config,
        StockRepository $stockRepository
    ) {
        $this->config          = $config;
        $this->stockRepository = $stockRepository;
    }

    /**
     * @param \Magento\CatalogInventory\Model\ResourceModel\Stock $subject
     * @param \Closure $proceed
     * @param array $items
     * @param int $websiteId
     * @param string $operator
     * @return void
     */
    public function aroundCorrectItemsQty(Stock $subject, \Closure $proceed, array $items, $websiteId, $operator)
    {
        if (!$this->config->isEnabled()) {
            return $proceed($items, $websiteId, $operator);
        }
        $this->stockRepository->correctItemsQty($items, $websiteId, $operator);
    }
}
