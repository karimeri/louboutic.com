<?php

namespace Project\PreOrder\Plugin\Magento\CatalogInventory\Observer;

use Magento\CatalogInventory\Observer\SubtractQuoteInventoryObserver;
use Magento\Framework\Event\Observer;
use Magento\CatalogInventory\Observer\ProductQty;
use Magento\CatalogInventory\Observer\ItemsForReindex;
use Project\PreOrder\Model\StockManagement;
use Project\PreOrder\Helper\Quote as QuoteHelper;

/**
 * Class SubtractQuoteInventoryObserverPlugin
 * @package Project\PreOrder\Plugin\Magento\CatalogInventory\Observer
 * @author Synolia <contact@synolia.com>
 */
class SubtractQuoteInventoryObserverPlugin
{
    /**
     * @var \Project\PreOrder\Model\StockManagement
     */
    protected $stockManagement;

    /**
     * @var \Magento\CatalogInventory\Observer\ProductQty
     */
    protected $productQty;

    /**
     * @var \Magento\CatalogInventory\Observer\ItemsForReindex
     */
    protected $itemsForReindex;

    /**
     * @var \Project\PreOrder\Helper\Quote
     */
    protected $quoteHelper;

    /**
     * SubtractQuoteInventoryObserverPlugin constructor.
     * @param \Project\PreOrder\Model\StockManagement $stockManagement
     * @param \Magento\CatalogInventory\Observer\ProductQty $productQty
     * @param \Magento\CatalogInventory\Observer\ItemsForReindex $itemsForReindex
     * @param \Project\PreOrder\Helper\Quote $quoteHelper
     */
    public function __construct(
        StockManagement $stockManagement,
        ProductQty $productQty,
        ItemsForReindex $itemsForReindex,
        QuoteHelper $quoteHelper
    ) {
        $this->stockManagement = $stockManagement;
        $this->productQty      = $productQty;
        $this->itemsForReindex = $itemsForReindex;
        $this->quoteHelper = $quoteHelper;
    }

    /**
     * @param \Magento\CatalogInventory\Observer\SubtractQuoteInventoryObserver $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundExecute(SubtractQuoteInventoryObserver $subject, \Closure $proceed, Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        if (!$this->quoteHelper->isPreOrder($quote)) {
            return $proceed($observer);
        }
        if ($quote->getInventoryProcessed()) {
            return $this;
        }

        $itemsForReindex = $this->stockManagement->registerProductsSale(
            $this->productQty->getProductQty($quote->getAllItems()),
            $quote->getStore()->getWebsiteId()
        );
        $this->itemsForReindex->setItems($itemsForReindex);

        $quote->setInventoryProcessed(true);
        return $this;
    }
}
