<?php

namespace Project\PreOrder\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 * @package Project\PreOrder\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_STOCK_CODE_PREORDER                       = 'louboutin_wms/general/stock_code_preorder';
    const XML_PATH_PRE_ORDER_AVAILABILITY_DISPLAY_TYPE       = 'louboutin_preorder/availability_display/type';
    const XML_PATH_PRE_ORDER_AVAILABILITY_DISPLAY            = 'louboutin_preorder/availability_display/';
    const XML_PATH_PRE_ORDER_INCREMENT_ID_SUFFIX             = 'louboutin_preorder/order/suffix';
    const XML_PATH_PRE_ORDER_RESTRICTION_MAXIMUM_AMOUNT      = 'louboutin_preorder/restriction/maximum_amount';
    const XML_PATH_PRE_ORDER_RESTRICTION_MAXIMUM_AMOUNT_TEXT = 'louboutin_preorder/restriction/maximum_amount_text';

    /**
     * @param null|int $scopeCode
     * @return int|null
     */
    public function getStockName($scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_STOCK_CODE_PREORDER,
            ScopeInterface::SCOPE_WEBSITES,
            $scopeCode
        );
    }

    /**
     * @param null|int $scopeCode
     * @return null|string
     */
    public function getAvailabilityDisplayType($scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PRE_ORDER_AVAILABILITY_DISPLAY_TYPE,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param null|int $scopeCode
     * @return null|string
     */
    public function getDisplayText($scopeCode = null)
    {
        $type = $this->getAvailabilityDisplayType($scopeCode);
        return $this->scopeConfig->getValue(
            self::XML_PATH_PRE_ORDER_AVAILABILITY_DISPLAY.$type,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param null|int $scopeCode
     * @return null|string
     */
    public function getOrderSuffix($scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PRE_ORDER_INCREMENT_ID_SUFFIX,
            ScopeInterface::SCOPE_WEBSITES,
            $scopeCode
        );
    }

    /**
     * @param null|int $scopeCode
     * @return null|string
     */
    public function getMaximumAmount($scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PRE_ORDER_RESTRICTION_MAXIMUM_AMOUNT,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param null|int $scopeCode
     * @return null|string
     */
    public function getMaximumAmountText($scopeCode = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PRE_ORDER_RESTRICTION_MAXIMUM_AMOUNT_TEXT,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }
}
