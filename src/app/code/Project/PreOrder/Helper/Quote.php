<?php

namespace Project\PreOrder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Quote\Model\Quote as QuoteModel;

/**
 * Class Quote
 * @package Project\PreOrder\Helper
 * @author Synolia <contact@synolia.com>
 */
class Quote extends AbstractHelper
{
    const ERROR_MESSAGE_RESTRICT_QUOTE = 'All pre-order merchandise must be shipped and charged separately';

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    public function isPreOrder(QuoteModel $quote)
    {
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getIsPreorder()) {
                return true;
            }
        }
        return false;
    }
}
