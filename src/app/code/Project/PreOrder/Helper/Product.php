<?php

namespace Project\PreOrder\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Registry;
use Magento\Framework\App\State;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Model\Stock\Status as StockStatus;
use Magento\CatalogInventory\Api\StockRegistryInterface as StockRegistry;
use\Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ProductRepository;
use Project\PreOrder\Model\StockRegistry as StockRegistryPreOrder;
use Project\PreOrder\Helper\Config as PreOrderHelper;
use Project\PreOrder\Model\StockRegistry as PreOrderStockRegistry;
use Project\PreOrder\Model\StockRepository;

/**
 * Class Product
 * @package Project\PreOrder\Helper
 * @author Synolia <contact@synolia.com>
 */
class Product extends AbstractHelper
{
    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $stockRegistryPreOrder;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $preOrderHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $preOrderStockRegistry;

    /**
     * @var \Project\PreOrder\Model\StockRepository
     */
    protected $stockRepository;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * Product constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Project\PreOrder\Model\StockRegistry $stockRegistryPreOrder
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Project\PreOrder\Helper\Config $preOrderHelper
     * @param \Magento\Framework\Registry $registry
     * @param \Project\PreOrder\Model\StockRegistry $preOrderStockRegistry
     * @param \Project\PreOrder\Model\StockRepository $stockRepository
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        Context $context,
        StockRegistry $stockRegistry,
        StockRegistryPreOrder $stockRegistryPreOrder,
        TimezoneInterface $timezone,
        PreOrderHelper $preOrderHelper,
        Registry $registry,
        PreOrderStockRegistry $preOrderStockRegistry,
        StockRepository $stockRepository,
        State $state,
        ProductRepository $productRepository
    ) {
        parent::__construct($context);
        $this->stockRegistry         = $stockRegistry;
        $this->stockRegistryPreOrder = $stockRegistryPreOrder;
        $this->timezone              = $timezone;
        $this->preOrderHelper        = $preOrderHelper;
        $this->registry              = $registry;
        $this->preOrderStockRegistry = $preOrderStockRegistry;
        $this->stockRepository       = $stockRepository;
        $this->state                 = $state;
        $this->productRepository     = $productRepository;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param bool $productIsSalable
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isPreOrder(ProductInterface $product, $productIsSalable = false)
    {
        $productIsSalable = $productIsSalable ? $this->isSalable($product) : false;
        if ($productIsSalable) {
            return false;
        }
        return (bool) $this->stockRegistryPreOrder->isSalable($product) ;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isPreOrderByType(ProductInterface $product)
    {
        if ($product->getTypeId() === Type::TYPE_SIMPLE) {
            return (bool) $this->isPreOrder($product);
        }

        return (bool) $this->stockRepository->isConfigurableOnPreOrder($product->getId());
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface|int $product
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isPreOrderAdmin($product)
    {
        if ($this->state->getAreaCode() != Area::AREA_ADMINHTML) {
            return false;
        } elseif (is_int($product)) {
            $product = $this->productRepository->getById($product);
        }
        return $this->isPreOrderByType($product);
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool
     */
    public function isSalable(ProductInterface $product)
    {
        $stockStatus = $this->stockRegistry->getStockStatus(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );
        return (bool)$stockStatus->getStockStatus() === StockStatus::STATUS_IN_STOCK && $product->isSalable();
    }

    /**
     * @param $date
     * @return string
     */
    public function formatDate($date)
    {
        return $this->timezone->formatDate($date);
    }

    /**
     * @param $product
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAvailabilityDate($product)
    {
        if (is_null($product)) {
            $product = $this->getCurrentProduct();
        }

        $stockStatus = $this->preOrderStockRegistry->getStockStatus($product->getId());

        return $stockStatus->getAvailabilityDate();
    }

    /**
     * @param $availibilityDate
     * @return string
     */
    public function getPreorderMessage($availibilityDate)
    {
        return sprintf(
            __($this->preOrderHelper->getDisplayText()),
            $this->formatDate(
                $availibilityDate,
                'Y/m/d'
            )
        );
    }

    /**
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }
}
