<?php

namespace Project\PreOrder\Observer;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Request\Http;
use Project\PreOrder\Helper\Config;
use Project\PreOrder\Helper\Quote;

/**
 * Class RestrictAddToCart
 * @package Project\PreOrder\Observer
 * @author Synolia <contact@synolia.com>
 */
class RestrictAddToCart implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Project\PreOrder\Helper\Quote
     */
    protected $quoteHelper;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $helperConfig;

    /**
     * RestrictAddToCart constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Project\PreOrder\Helper\Quote $quoteHelper
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Project\PreOrder\Helper\Config $helperConfig
     */
    public function __construct(
        ManagerInterface $messageManager,
        Session $checkoutSession,
        Quote $quoteHelper,
        ProductRepository $productRepository,
        Config $helperConfig
    ) {
        $this->messageManager    = $messageManager;
        $this->checkoutSession   = $checkoutSession;
        $this->quoteHelper       = $quoteHelper;
        $this->productRepository = $productRepository;
        $this->helperConfig      = $helperConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getRequest();
        $quote   = $this->checkoutSession->getQuote();
        $productAdded = $this->productRepository->getById($request->get('product'));
        $productAddedIsPreorder = (bool) $request->get('is_preorder');
        $maximumAmount = $this->helperConfig->getMaximumAmount();

        if (!empty($maximumAmount) && $productAddedIsPreorder && $productAdded->getPrice() > $maximumAmount) {
            $this->processError(
                $request,
                $this->helperConfig->getMaximumAmountText()
            );
        }

        if (($this->quoteHelper->isPreOrder($quote)) || ($quote->hasItems() && $productAddedIsPreorder)) {
            $this->processError(
                $request,
                __(Quote::ERROR_MESSAGE_RESTRICT_QUOTE)
            );
        }

        return $this;
    }

    /**
     * @param \Magento\Framework\App\Request\Http $request
     * @param string $message
     * @return $this
     */
    protected function processError(Http $request, $message)
    {
        $this->messageManager->addError($message);
        $request->setParam('product', false);
        return $this;
    }
}
