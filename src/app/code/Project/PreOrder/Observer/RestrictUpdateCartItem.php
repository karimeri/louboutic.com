<?php

namespace Project\PreOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session;
use Project\PreOrder\Helper\Quote;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class RestrictUpdateCartItem
 * @package Project\PreOrder\Observer
 * @author Synolia <contact@synolia.com>
 */
class RestrictUpdateCartItem implements ObserverInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Project\PreOrder\Helper\Quote
     */
    protected $quoteHelper;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Project\PreOrder\Helper\Quote $quoteHelper
     */
    public function __construct(
        Session $checkoutSession,
        Quote $quoteHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->quoteHelper     = $quoteHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $quote = $this->checkoutSession->getQuote();

        if ($this->quoteHelper->isPreOrder($quote)) {
            throw new LocalizedException(__(Quote::ERROR_MESSAGE_RESTRICT_QUOTE));
        }

        return $this;
    }
}
