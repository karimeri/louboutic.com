<?php

namespace Project\PreOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Project\PreOrder\Helper\Quote;

/**
 * Class RestrictQuoteMerge
 * @package Project\PreOrder\Observer
 * @author Synolia <contact@synolia.com>
 */
class RestrictQuoteMerge implements ObserverInterface
{
    /**
     * @var \Project\PreOrder\Helper\Quote
     */
    protected $quoteHelper;

    /**
     * @param \Project\PreOrder\Helper\Quote $quoteHelper
     */
    public function __construct(
        Quote $quoteHelper
    ) {
        $this->quoteHelper = $quoteHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this|void
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        /** @var \Magento\Quote\Model\Quote $quoteSource */
        $quote       = $observer->getQuote();
        $quoteSource = $observer->getSource();

        if ($this->quoteHelper->isPreOrder($quote) || $this->quoteHelper->isPreOrder($quoteSource)) {
            $quoteSource->removeAllItems();
        }

        return $this;
    }
}
