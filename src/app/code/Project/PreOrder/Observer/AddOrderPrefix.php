<?php

namespace Project\PreOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Project\PreOrder\Helper\Quote;
use Project\PreOrder\Helper\Config;

/**
 * Class AddOrderPrefix
 * @package Project\PreOrder\Observer
 * @author Synolia <contact@synolia.com>*
 */
class AddOrderPrefix implements ObserverInterface
{
    /**
     * @var \Project\PreOrder\Helper\Quote
     */
    protected $quoteHelper;

    /**
     * @var \Project\PreOrder\Helper\Config
     */
    protected $config;

    /**
     * AddOrderPrefix constructor.
     * @param \Project\PreOrder\Helper\Quote $quoteHelper
     * @param \Project\PreOrder\Helper\Config $config
     */
    public function __construct(
        Quote $quoteHelper,
        Config $config
    ) {
        $this->quoteHelper = $quoteHelper;
        $this->config      = $config;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        /** @var \Magento\Quote\Model\Quote $quote */
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        if ($this->quoteHelper->isPreOrder($quote)) {
            $incrementId = $order->getIncrementId().$this->config->getOrderSuffix();
            $order->setIncrementId($incrementId);
            $quote->setReservedOrderId($incrementId);
        }
    }
}
