<?php

namespace Project\PreOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http;
use Project\PreOrder\Helper\Product;

/**
 * Class AddQuoteItem
 * @package Project\PreOrder\Observer
 * @author Synolia <contact@synolia.com>
 */
class AddQuoteItem implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Project\PreOrder\Helper\Product
     */
    protected $helperPreOrderProduct;

    /**
     * BeforeQuoteItemSave constructor.
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Project\PreOrder\Helper\Product $product
     */
    public function __construct(
        Http $request,
        Product $product
    ) {
        $this->request = $request;
        $this->helperPreOrderProduct = $product;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
        $quoteItem = $observer->getEvent()->getQuoteItem();
        if ($this->request->get('is_preorder') &&
            !$quoteItem->getParentItemId() ||
            $this->helperPreOrderProduct->isPreOrderAdmin($quoteItem->getProduct())
        ) {
            $quoteItem->setIsPreorder(true);
        }
    }
}
