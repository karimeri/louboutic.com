<?php
namespace Project\PreOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Project\PreOrder\Model\StockRegistry;
use Project\PreOrder\Helper\Product;

/**
 * Class EmailSetTemplateVar
 * @package Project\PreOrder\Observer
 * @author Synolia <contact@synolia.com>
 */
class EmailSetTemplateVar implements ObserverInterface
{
    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $stockRegistry;

    /**
     * @var \Project\PreOrder\Helper\Product
     */
    protected $preOrderHelperProduct;

    /**
     * EmailSetTemplateVar constructor.
     * @param \Project\PreOrder\Model\StockRegistry $stockRegistry
     * @param \Project\PreOrder\Helper\Product $preOrderHelperProduct
     */
    public function __construct(
        StockRegistry $stockRegistry,
        Product $preOrderHelperProduct
    ) {
        $this->stockRegistry = $stockRegistry;
        $this->preOrderHelperProduct = $preOrderHelperProduct;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\DataObject $transport */
        $transport = $observer->getTransport();
        $order     = $transport->getOrder();

        $availabilityDate = $this->getAvailabilityDate($order);
        $transport->setIsPreorder($availabilityDate ? true : false);
        $transport->setAvailabilityDate($this->preOrderHelperProduct->formatDate($availabilityDate, 'Y/m/d'));
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getAvailabilityDate(Order $order)
    {
        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            if ($item->getIsPreorder()) {
                $children = $item->getChildrenItems();
                $productId = $children ? current($children)->getProductId() : $item->getProductId() ;
                $stockItem = $this->stockRegistry->getStockItem($productId);
                return $stockItem->getAvailabilityDate();
            }
        }
        return false;
    }
}
