define([
    'jquery',
    'mage/translate'
], function (
    $
) {
    'use strict';

    $.widget('project.cbSecurityInput', {
        options : {
            selector: 'input'
        },

        /**
         * Constructor
         * @private
         */
        _create: function () {
           // this._bindEvents();
        },

        /**
         * Binds events
         * @private
         */
        _bindEvents: function() {
            $(document).on('keyup keydown blur focus', this.options.selector, this._inputsEventsKey);
        },

        /**
         * On keyup/down in inputs
         * @private
         */
        _inputsEventsKey: function () {
            var val  = $(this).val().replace(/[ .-]/g, ''),
                $div = $(this).next('.cb-error');

            if ($div.length === 1) {
                $div.remove();
            }

            if (/^[0-9]{15,16}$/.test(val) && $(this).attr('id') !== 'veritrans_cc_cc_number') {
                $(this).val('');

                $div = $('<div/>')
                    .addClass('mage-error cb-error')
                    .attr('generated', true)
                    .text($.mage.__('It seems to be a card number.'));

                $(this).after($div);
            }
        }
    });

    return $.project.cbSecurityInput;
});
