<?php

namespace Project\SampleData\Console\Command;

use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Framework\App\State;
use Magento\Framework\App\ResourceConnection;
use \Zend_Db_Expr as Expr;

/**
 * Class GenerateProductStock
 * @package Project\SampleData\Console\Command
 * @author Synolia <contact@synolia.com>
 */
class GenerateProductStock extends Command
{
    const INPUT_MIN_QTY = 'min_qty';
    const INPUT_MAX_QTY = 'max_qty';

    /**
     * @var ProductCollection
     */
    protected $productCollection;

    /**
     * @var array
     */
    protected $products = [];

    /**
     * @var State
     */
    protected $state;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var StockRegistryProviderInterface
     */
    protected $stockRegistry;

    /**
     * GenerateProductStock constructor.
     * @param ProductCollection $productCollection
     * @param StockRegistryProviderInterface $stockRegistry
     * @param State $state
     * @param ResourceConnection $resource
     */
    public function __construct(
        ProductCollection $productCollection,
        StockRegistryProviderInterface $stockRegistry,
        State $state,
        ResourceConnection $resource
    ) {
        parent::__construct();
        $this->productCollection = $productCollection;
        $this->state             = $state;
        $this->resource          = $resource;
        $this->stockRegistry     = $stockRegistry;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:sampledata:generate:stock')
            ->setDescription('Generate Product Stock For Louboutin');

        $this->addArgument(self::INPUT_MAX_QTY, InputArgument::OPTIONAL, __('Type a number'));
        $this->addArgument(self::INPUT_MIN_QTY, InputArgument::OPTIONAL, __('Type a number'));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode('adminhtml');
        $this->products = $this->productCollection;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stockData    = [];
        $countProduct = count($this->products);
        $minQty       = $input->getArgument(self::INPUT_MAX_QTY) ?? 100;
        $maxQty       = $input->getArgument(self::INPUT_MIN_QTY) ?? 500;

        foreach ($this->products as $product) {
            /** @var \Magento\Catalog\Model\Product $product */
            foreach ($product->getWebsiteIds() as $websiteId) {
                $stock   = $this->stockRegistry->getStock($websiteId);
                $stockId = $stock->getStockId() ?? 1;
                $key     = $product->getId().'-'.$stockId;
                $qty     = rand($minQty, $maxQty);

                if (!empty($stockData[$key])) {
                    continue;
                }

                $stockData[$key] = [
                    'product_id'                => new Expr($product->getId()),
                    'stock_id'                  => new Expr($stockId),
                    'qty'                       => new Expr($qty),
                    'is_in_stock'               => new Expr($qty == 0 ? 0 : 1),
                    'low_stock_date'            => new Expr('NULL'),
                    'stock_status_changed_auto' => new Expr(0),
                    'website_id'                => $stockId == 1 ? 0 : $websiteId,
                ];
            }
        }

        $this->resource
            ->getConnection()
            ->insertOnDuplicate(
                $this->resource->getTableName('cataloginventory_stock_item'),
                $stockData
            );

        $output->writeln("<info>Stock generated for $countProduct Products</info>");
    }
}
