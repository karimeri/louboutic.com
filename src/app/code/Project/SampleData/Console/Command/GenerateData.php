<?php

namespace Project\SampleData\Console\Command;

use Composer\Json\JsonFile;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Component\ComponentRegistrarInterface;
use Magento\Framework\Console\Cli;
use Magento\Framework\Filesystem\DirectoryList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Synolia\Standard\Setup\EavSetupFactory;

/**
 * Class GenerateData
 *
 * @package Project\Core\Console\Command
 * @author Synolia <contact@synolia.com>
 */
class GenerateData extends Command
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;
    /**
     * @var ComponentRegistrarInterface
     */
    protected $moduleRegistry;
    /**
     * @var array|null
     */
    protected $data;
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    private $output;

    /**
     * GenerateData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param ComponentRegistrarInterface $moduleRegistry
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepository $categoryRepository
     * @param DirectoryList $directoryList
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        ComponentRegistrarInterface $moduleRegistry,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        DirectoryList $directoryList
    ) {
        parent::__construct();
        $this->eavSetupFactory    = $eavSetupFactory->create();
        $this->moduleRegistry     = $moduleRegistry;
        $this->categoryFactory    = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('project:sampledata:generate')
            ->setDescription('Generate Data For Louboutin');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $json       = new JsonFile($this->getFile());
        $this->data = $json->exists() ? $json->read() : null;
    }

    /**
     * @return string
     */
    protected function getFile()
    {
        $modulePaths = $this->moduleRegistry->getPaths(ComponentRegistrar::MODULE);

        // @codingStandardsIgnoreStart
        return $modulePaths['Project_SampleData'] . '/fixtures/' . getenv('PROJECT_ENV') . '.json';
        // @codingStandardsIgnoreEnd
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        try {
            if (\is_null($this->data) || empty($this->data)) {
                $this->output->writeln('<comment>No data imported</comment>');

                return Cli::RETURN_SUCCESS;
            }
            $this->createCategories();
        } catch (\Exception $exception) {
            $this->output->writeln('<error>' . $exception->getMessage() . '</error>');

            return Cli::RETURN_FAILURE;
        }

        return Cli::RETURN_SUCCESS;
    }

    /**
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function createCategories()
    {
        /** @var array $categories */
        $categories     = $this->data['categories'];
        $newsCategories = [];

        foreach ($categories as $category) {
            $modelCategory = $this->categoryFactory->create();

            if (!empty($newsCategories[$category['parent_id']])) {
                $category['parent_id'] = $newsCategories[$category['parent_id']];
            }

            $modelCategory->setData($category);
            $newCategory = $this->categoryRepository->save($modelCategory);
            $this->output->writeln('<info>Category ' . $newCategory->getName() . ' created with id : ' . $newCategory->getId() . '</info>');
            $newsCategories[$newCategory->getName()] = $newCategory->getId();
        }
    }
}
