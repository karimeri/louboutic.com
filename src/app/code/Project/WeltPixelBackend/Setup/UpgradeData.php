<?php

namespace Project\WeltPixelBackend\Setup;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class UpgradeData
 * @package Project\WeltPixel\Backend\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    const UPDATE_CRON_STRING_PATH = "weltpixel/crontab/license";

    /** @var WriterInterface */
    protected $configWriter;

    /**
     * UpgradeData constructor.
     * @param WriterInterface $configWriter
     */
    public function __construct(
        WriterInterface $configWriter
    )
    {
        $this->configWriter = $configWriter;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0) {
            $this->configWriter->save(self::UPDATE_CRON_STRING_PATH, null);
        }

        $installer->endSetup();
    }
}