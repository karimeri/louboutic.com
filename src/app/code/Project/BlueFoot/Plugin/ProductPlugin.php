<?php

namespace Project\BlueFoot\Plugin;

use Magento\Store\Model\StoreManager;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class ProductPlugin
 * @package Project\BlueFoot\Plugin
 * @author Synolia <contact@synolia.com>
 */
class ProductPlugin
{
    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * ProductPlugin constructor.
     *
     * @param StoreManager $storeManager
     * @param ProductRepositoryInterface $productRepositoryInterface
     */
    public function __construct
    (
        StoreManager $storeManager,
        ProductRepositoryInterface $productRepositoryInterface
    ) {
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepositoryInterface;
    }

    /**
     * Fix GetProduct by using the current store ID
     *
     * @param \Gene\BlueFoot\Model\Attribute\Data\Widget\Product $subject
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface|null
     */
    public function aroundGetProduct(\Gene\BlueFoot\Model\Attribute\Data\Widget\Product $subject)
    {
        try {
            $dataAttributeCode = $subject->getEntity()->getData($subject->getAttribute()->getData('attribute_code'));
            $storeId = $this->storeManager->getStore()->getId();
            $product = $this->productRepository->getById($dataAttributeCode, false, $storeId);

            if ($product->getId()) {
                return $product;
            }
        } catch (\Exception $e) {
            return null;
        }

        return null;
    }
}
