<?php

namespace Project\Backend\Block\Adminhtml\Widget\Grid\Column\Renderer;

/**
 * Class DatetimeWithoutTimeZone
 * @package Project\Backend\Block\Adminhtml\Widget\Grid\Column\Renderer
 * @author Synolia <contact@synolia.com>
 */
class DatetimeWithoutTimeZone extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Renders grid column
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $format = $this->getColumn()->getFormat();
        $date = $this->_getValue($row);
        if ($date) {
            if (!($date instanceof \DateTimeInterface)) {
                $date = new \DateTime($date);
            }
            return $this->_localeDate->formatDateTime(
                $date,
                $format ?: \IntlDateFormatter::MEDIUM,
                $format ?: \IntlDateFormatter::MEDIUM,
                null,
                'UTC'
            );
        }
        return $this->getColumn()->getDefault();
    }
}
