<?php

namespace Project\Backend\Plugin\Block\Store;

use Magento\Backend\Block\Store\Switcher;

/**
 * Class SwitcherPlugin
 * @package Project\Backend\Plugin\Block\Store
 * @author Synolia <contact@synolia.com>
 */
class SwitcherPlugin
{
    /**
     * @param \Magento\Backend\Block\Store\Switcher $subject
     * @param array $result
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetWebsites(Switcher $subject, array $result)
    {
        usort($result, function ($first, $second) {
            return strcmp($first->getName(), $second->getName());
        });

        return $result;
    }
}
