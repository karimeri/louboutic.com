<?php

namespace Project\EmailSent\Console\Command;

use Project\EmailSent\Model\EmailRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Console\Cli;
use Project\EmailSent\Helper\Config;

/**
 * Class DeleteEmailSent
 * @package Project\EmailSent\Console\Command
 */
class DeleteEmailSent extends Command
{
    const MAX_DAYS_HISTORY = 30;

    /**
     * @var EmailRepository
     */
    private $emailRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * DeleteEmailSent constructor.
     * @param EmailRepository $emailRepository
     * @param Config $config
     */
    public function __construct(
        EmailRepository $emailRepository,
        Config $config
    ) {
        $this->emailRepository = $emailRepository;
        $this->config = $config;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:delete:email')
            ->setDescription('Remove old email');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $nbDay = $this->config->getDays();
            if (!isset($nbDay) || ($nbDay > static::MAX_DAYS_HISTORY)) {
                $nbDay = static::MAX_DAYS_HISTORY;
            }
            $this->emailRepository->deleteDaily($nbDay);
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');
            return Cli::RETURN_FAILURE;
        }
        return Cli::RETURN_SUCCESS;
    }
}
