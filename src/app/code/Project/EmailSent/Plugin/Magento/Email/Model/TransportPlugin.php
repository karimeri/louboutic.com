<?php

namespace Project\EmailSent\Plugin\Magento\Email\Model;

use Magento\Email\Model\Transport as MailTransport;
use Magento\Store\Model\StoreManagerInterface;
use Project\EmailSent\Model\EmailRepository;
use Project\EmailSent\Model\Email;
use Project\EmailSent\Api\Data\EmailInterfaceFactory;
use Project\Security\Helper\Config as ConfigSecurity;
use Zend\Mail\Message;

use Magento\Framework\Mail\EmailMessageInterface;
use Magento\Framework\Mail\MimeMessageInterface;
use Magento\Framework\Mail\MimePartInterface;

/**
 * Class TransportPlugin
 * @package Project\EmailSent\Plugin\Magento\Email\Model
 */
class TransportPlugin
{
    const TYPE_HTML = 'text/html';
    /**
     * @var EmailRepository
     */
    protected $emailRepository;

    /**
     * @var EmailInterfaceFactory
     */
    protected $emailFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ConfigSecurity
     */
    protected $configSecurity;

    /**
     * TransportPlugin constructor.
     * @param EmailRepository $emailRepository
     * @param EmailInterfaceFactory $emailFactory
     * @param StoreManagerInterface $storeManager
     * @param ConfigSecurity $configSecurity
     */
    public function __construct(
        EmailRepository $emailRepository,
        EmailInterfaceFactory $emailFactory,
        StoreManagerInterface $storeManager,
        ConfigSecurity $configSecurity
    ) {
        $this->emailRepository = $emailRepository;
        $this->emailFactory = $emailFactory;
        $this->storeManager = $storeManager;
        $this->configSecurity = $configSecurity;
    }

    /**
     * @param MailTransport $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function afterSendMessage(MailTransport $subject, $result)
    {
        $message = $subject->getMessage();

        $zendMessage = Message::fromString($message->getRawMessage())->setEncoding('utf-8');

        /** @var Email $email */
        $email = $this->emailFactory->create();
        $sender = '';
        $recipient = '';
        if($zendMessage->getFrom()->rewind())
            $sender = $zendMessage->getFrom()->rewind()->getEmail();
        if($zendMessage->getTo()->rewind())
            $recipient = $zendMessage->getTo()->rewind()->getEmail();

        if ($recipient && ($this->configSecurity->getAlertEmail() === $recipient)) {
            return $result;
        }

        $sender ? $email->setFrom($sender) : '';
        $recipient ? $email->setTo($recipient) : '';
        $subjectMail = $this->decodeSubject($subject->getMessage()->getSubject());

        $email->setSubject($subjectMail);

        foreach ($message->getBody()->getParts() as $messagePart) {
            if ($messagePart->getType() === self::TYPE_HTML) {
                $messageContent = $messagePart->getContent();
            }
        }
        $email->setMessage($messageContent);

        $storeId = $this->storeManager->getStore()->getId();
        $email->setStoreId($storeId);

        $this->emailRepository->save($email);

        return $result;
    }

    /**
     * @param $subject
     * @return bool|mixed|string
     * phpcs:disable Ecg.Security.ForbiddenFunction.Found
     */
    private function decodeSubject($subject)
    {
        if (strpos($subject, '?utf-8?B') !== false) {
            $subject = str_replace("?utf-8?B", "", $subject);
            $subject = base64_decode($subject);
        }

        return $subject;
    }
}
