<?php

namespace Project\EmailSent\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 * @package Project\EmailSent\Helper
 */
class Config extends AbstractHelper
{
    const XML_PATH_LOUBOUTIN_EMAIL_CONFIG = 'louboutin_email';
    const DAYS = 'days';
    const SEND_EMAIL = 'send_email';
    const EMAIL_IDENTITY = 'email_identity';
    const EMAIL_TEMPLATE = 'email_template';

    /**
     * @param      $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getDays(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::DAYS, $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null   $scopeCode
     * @return mixed
     */
    public function getSendEmailOrderManualCancel(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::SEND_EMAIL, 'manual_cancel', $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null   $scopeCode
     * @return mixed
     */
    public function getEmailIdentityOrderManualCancel(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::EMAIL_IDENTITY, 'manual_cancel', $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getEmailTemplateOrderManualCancel(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::EMAIL_TEMPLATE, 'manual_cancel', $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null   $scopeCode
     * @return mixed
     */
    public function getSendEmailRmaReturnReceived(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::SEND_EMAIL, 'return_received', $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null   $scopeCode
     * @return mixed
     */
    public function getEmailIdentityRmaReturnReceived(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::EMAIL_IDENTITY, 'return_received', $scopeType, $scopeCode);
    }

    /**
     * @param string $scopeType
     * @param null $scopeCode
     * @return mixed
     */
    public function getEmailTemplateRmaReturnReceived(
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->getConfig(self::EMAIL_TEMPLATE, 'return_received', $scopeType, $scopeCode);
    }

    /**
     * @param        $configName
     * @param string $configGroup
     * @param        $scopeType
     * @param null   $scopeCode
     * @return mixed
     */
    protected function getConfig(
        $configName,
        $configGroup = 'history',
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        return $this->scopeConfig->getValue(
            self::XML_PATH_LOUBOUTIN_EMAIL_CONFIG."/$configGroup/$configName",
            $scopeType,
            $scopeCode
        );
    }
}
