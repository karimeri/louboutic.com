<?php

namespace Project\EmailSent\Model;

use Project\EmailSent\Api\EmailRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Project\EmailSent\Model\ResourceModel\Email as ResourceEmail;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Project\EmailSent\Model\ResourceModel\Email\CollectionFactory as EmailCollectionFactory;
use Project\EmailSent\Api\Data\EmailInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Project\EmailSent\Api\Data\EmailInterface;
use Magento\Framework\App\ResourceConnection;

/**
 * Class EmailRepository
 *
 * @package Project\EmailSent\Model
 */
class EmailRepository implements EmailRepositoryInterface
{
    const TABLE_NAME = 'project_emailsent_email';

    /**
     * @var EmailFactory
     */
    protected $emailFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var EmailInterfaceFactory
     */
    protected $dataEmailFactory;

    /**
     * @var ResourceEmail
     */
    protected $resource;

    /**
     * @var EmailCollectionFactory
     */
    protected $emailCollectionFactory;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * EmailRepository constructor.
     * @param ResourceEmail $resource
     * @param EmailFactory $emailFactory
     * @param EmailInterfaceFactory $dataEmailFactory
     * @param EmailCollectionFactory $emailCollectionFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceEmail $resource,
        EmailFactory $emailFactory,
        EmailInterfaceFactory $dataEmailFactory,
        EmailCollectionFactory $emailCollectionFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resource;
        $this->emailFactory = $emailFactory;
        $this->emailCollectionFactory = $emailCollectionFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataEmailFactory = $dataEmailFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param EmailInterface $email
     * @return EmailInterface
     * @throws CouldNotSaveException
     */
    public function save(EmailInterface $email)
    {
        try {
            $email->getResource()->save($email);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the email: %1',
                $exception->getMessage()
            ));
        }
        return $email;
    }

    /**
     * @param int $emailId
     * @return Email
     * @throws NoSuchEntityException
     */
    public function getById($emailId)
    {
        $email = $this->emailFactory->create();
        $email->getResource()->load($email, $emailId);
        if (!$email->getId()) {
            throw new NoSuchEntityException(__('email with id "%1" does not exist.', $emailId));
        }
        return $email;
    }

    /**
     * @param EmailInterface $email
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(EmailInterface $email)
    {
        try {
            $email->getResource()->delete($email);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the email: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param int $emailId
     * @return bool
     */
    public function deleteById($emailId)
    {
        return $this->delete($this->getById($emailId));
    }

    /**
     * @param int $nbDays
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function deleteDaily($nbDays)
    {
        try {
            $limitDate = (new \DateTime())->modify("-" . $nbDays . " days");
            $collection = $this->emailCollectionFactory->create();
            $collection->addFieldToFilter(EmailInterface::CREATION_TIME, ['lt' => $limitDate->format('Y-m-d h:m:s')]);
            $collection->walk('delete');
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Error while deletings emails : %1',
                $exception->getMessage()
            ));
        }
        return true;
    }
}
