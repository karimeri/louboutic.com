<?php

namespace Project\EmailSent\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Email
 * @package Project\EmailSent\Model\ResourceModel
 */
class Email extends AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct()
    {
        $this->_init('project_emailsent_email', 'email_id');
    }
}
