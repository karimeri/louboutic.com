<?php

namespace Project\EmailSent\Model\ResourceModel\Email;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Email
 */
class Collection extends AbstractCollection
{
    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * Define resource model
     *
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore
    protected function _construct()
    {
        $this->_init(
            'Project\EmailSent\Model\Email',
            'Project\EmailSent\Model\ResourceModel\Email'
        );
    }
}
