<?php

namespace Project\EmailSent\Model;

use Project\EmailSent\Api\Data\EmailInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Email
 *
 * @package Project\EmailSent\Model
 */
class Email extends AbstractModel implements EmailInterface
{
    /**
     * @return void
     *
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct()
    {
        $this->_init('Project\EmailSent\Model\ResourceModel\Email');
    }

    /**
     * Get email_id
     *
     * @return int
     */
    public function getEmailId()
    {
        return $this->getData(self::EMAIL_ID);
    }

    /**
     * Set email_id
     *
     * @param string $emailId
     *
     * @return EmailInterface
     */
    public function setEmailId($emailId)
    {
        return $this->setData(self::EMAIL_ID, $emailId);
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->getData(self::FROM);
    }

    /**
     * Set from
     *
     * @param string $from
     *
     * @return EmailInterface
     */
    public function setFrom($from)
    {
        return $this->setData(self::FROM, $from);
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->getData(self::TO);
    }

    /**
     * Set to
     *
     * @param string $to
     *
     * @return EmailInterface
     */
    public function setTo($to)
    {
        return $this->setData(self::TO, $to);
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->getData(self::SUBJECT);
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return EmailInterface
     */
    public function setSubject($subject)
    {
        return $this->setData(self::SUBJECT, $subject);
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return EmailInterface
     */
    public function setMessage($message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Get store_id
     *
     * @return string
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * Set store_id
     *
     * @param int $storeId
     *
     * @return EmailInterface
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * Get creation_time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Set creation time
     *
     * @param string $creationTime
     *
     * @return EmailInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }
}
