<?php

namespace Project\EmailSent\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Project\EmailSent\Api\Data\EmailInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package Project\EmailSent\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $tableProjectEmailsentEmail = $setup->getConnection()->newTable($setup->getTable('project_emailsent_email'));


        $tableProjectEmailsentEmail->addColumn(
            EmailInterface::EMAIL_ID,
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true,
            ],
            'Entity ID'
        )->addColumn(
            EmailInterface::FROM,
            Table::TYPE_TEXT,
            255,
            [],
            'from'
        )->addColumn(
            EmailInterface::TO,
            Table::TYPE_TEXT,
            255,
            [],
            'to'
        )->addColumn(
            EmailInterface::SUBJECT,
            Table::TYPE_TEXT,
            255,
            [],
            'subject'
        )->addColumn(
            EmailInterface::MESSAGE,
            Table::TYPE_TEXT,
            null,
            [],
            'message'
        )->addColumn(
            EmailInterface::STORE_ID,
            Table::TYPE_SMALLINT,
            null,
            [],
            'store_id'
        )->addColumn(
            EmailInterface::CREATION_TIME,
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT
            ],
            'creation time'
        );

        $setup->getConnection()->createTable($tableProjectEmailsentEmail);

        $setup->endSetup();
    }
}
