<?php

namespace Project\EmailSent\Block\Adminhtml\Email\Preview;

use Magento\Backend\Block\Template;

/**
+ * Class BackButton
+ * @package Project\EmailSent\Block\Adminhtml\Email\Preview
+ */
class BackButton extends Template
{
    /**
     * @var string
     */
    protected $template = 'backButton.phtml';

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }
}
