<?php

namespace Project\EmailSent\Block\Adminhtml\Email\Preview;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;

/**
 * Class Email
 * @package Project\EmailSent\Block\Adminhtml\Template
 */
class Email extends Template
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var string
     */
    protected $template = 'preview.phtml';

    /**
     * @param Registry $coreRegistry,
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Registry $coreRegistry,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
    }

    public function getEmailPreview()
    {
        $email = $this->coreRegistry->registry('project_emailsent_email');

        return $email->getMessage();
    }
}
