<?php

namespace Project\EmailSent\Api\Data;

/**
 * Interface EmailInterface
 *
 * @package Project\EmailSent\Api\Data
 */
interface EmailInterface
{
    const TO = 'to';
    const STORE_ID = 'store_id';
    const EMAIL_ID = 'email_id';
    const MESSAGE = 'message';
    const SUBJECT = 'subject';
    const FROM = 'from';
    const CREATION_TIME = 'creation_time';

    /**
     * Get email_id
     *
     * @return int|null
     */
    public function getEmailId();

    /**
     * Set email_id
     *
     * @param int $emailId
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setEmailId($emailId);

    /**
     * Get from
     *
     * @return string|null
     */
    public function getFrom();

    /**
     * Set from
     *
     * @param string $from
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setFrom($from);

    /**
     * Get to
     *
     * @return string|null
     */
    public function getTo();

    /**
     * Set to
     *
     * @param string $to
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setTo($to);

    /**
     * Get subject
     *
     * @return string|null
     */
    public function getSubject();

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setSubject($subject);

    /**
     * Get message
     *
     * @return string|null
     */
    public function getMessage();

    /**
     * Set message
     *
     * @param string $message
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setMessage($message);

    /**
     * Get store_Id
     *
     * @return string|null
     */
    public function getStoreId();

    /**
     * Set store_Id
     *
     * @param int $storeId
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setStoreId($storeId);

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Set creation_time
     *
     * @param string $creationTime
     *
     * @return \Project\EmailSent\Api\Data\EmailInterface
     */
    public function setCreationTime($creationTime);
}
