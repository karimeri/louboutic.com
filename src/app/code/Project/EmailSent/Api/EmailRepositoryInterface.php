<?php


namespace Project\EmailSent\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Project\EmailSent\Api\Data\EmailInterface;

interface EmailRepositoryInterface
{
    /**
     * Save email
     *
     * @param EmailInterface $email
     * @return EmailInterface
     */
    public function save(EmailInterface $email);

    /**
     * Retrieve email
     *
     * @param int $emailId
     * @return int|null
     */
    public function getById($emailId);

    /**
     * Delete email
     *
     * @param EmailInterface $email
     * @return bool true on success
     */
    public function delete(EmailInterface $email);

    /**
     * Delete email by ID
     *
     * @param int $emailId
     *
     * @return bool true on success
     */
    public function deleteById($emailId);
}
