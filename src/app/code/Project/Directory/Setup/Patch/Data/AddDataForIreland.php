<?php

namespace Project\Directory\Setup\Patch\Data;

use Magento\Directory\Setup\DataInstaller;
use Magento\Directory\Setup\Patch\Data\InitializeDirectoryData;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Project\Directory\Setup\DataInstallerFactory;

/**
 * Add Regions for Ireland.
 */
class AddDataForIreland implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Directory\Setup\DataInstallerFactory
     */
    private $dataInstallerFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Directory\Setup\DataInstallerFactory $dataInstallerFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        DataInstallerFactory $dataInstallerFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->dataInstallerFactory = $dataInstallerFactory;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        /** @var DataInstaller $dataInstaller */
        $dataInstaller = $this->dataInstallerFactory->create();
        $dataInstaller->addCountryRegions(
            $this->moduleDataSetup->getConnection(),
            $this->getDataForIreland()
        );
    }

    /**
     * Ireland states data.
     * Format : [country_id, region_code, default_name, [locale => local_name]]
     * ex : ['IE', 'DUB', 'Dublin'], ['fr_FR' => 'Dublin', 'ch_FR' => 'Dublin']],
     * @return array
     */
    private function getDataForIreland()
    {
        return [
            // ['IE', 'ANT', 'Antrim'], // North Ireland
            // ['IE', 'ARM', 'Armagh'], // North Ireland
            ['IE', 'CAV', 'Cavan'],
            // ['IE', 'LDY', 'Londonderry (Derry)'], // North Ireland
            ['IE', 'DON', 'Donegal'],
            // ['IE', 'DOW', 'Down'], // North Ireland
            // ['IE', 'FER', 'Fermanagh'], // North Ireland
            ['IE', 'MON', 'Monaghan'],
            // ['IE', 'TYR', 'Tyrone'], // North Ireland
            ['IE', 'GAL', 'Galway'],
            ['IE', 'LET', 'Leitrim'],
            ['IE', 'MAY', 'Mayo'],
            ['IE', 'ROS', 'Roscommon'],
            ['IE', 'SLI', 'Sligo'],
            ['IE', 'CAR', 'Carlow'],
            ['IE', 'DUB', 'Dublin'],
            ['IE', 'KID', 'Kildare'],
            ['IE', 'KIK', 'Kilkenny'],
            ['IE', 'LEX', 'Laois/Leix (Queens)'],
            ['IE', 'LOG', 'Longford'],
            ['IE', 'LOU', 'Louth'],
            ['IE', 'MEA', 'Meath'],
            ['IE', 'OFF', 'Offaly (Kings)'],
            ['IE', 'WEM', 'Westmeath'],
            ['IE', 'WEX', 'Wexford'],
            ['IE', 'WIC', 'Wicklow'],
            ['IE', 'CLA', 'Clare'],
            ['IE', 'COR', 'Cork'],
            ['IE', 'KER', 'Kerry'],
            ['IE', 'LIM', 'Limerick'],
            ['IE', 'TIP', 'Tipperary'],
            ['IE', 'WAT', 'Waterford'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [
            InitializeDirectoryData::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
