<?php

namespace Project\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Model\ProductRepository;
use Project\Core\Helper\AttributeSet;

/**
 * Class Product
 * @package Project\Core\Helper
 * @author Synolia <contact@synolia.com>
 */
class Product extends AbstractHelper
{
    /**
     * @var ProductRepository
     */
    protected $_productRepository;

    /**
     * Product constructor.
     * @param Context $context
     * @param ProductRepository $productRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProductRepository $productRepository,
        AttributeSet $attributeSetHelper,
        array $data = []
    )
    {
        $this->_productRepository = $productRepository;
        $this->_attributeSetHelper = $attributeSetHelper;
        parent::__construct($context);
    }

    /**
     * @param $id
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id)
    {
        try {
            $product = $this->_productRepository->getById($id);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noEntityException) {
            $product = null;
        }
        return $product;
    }

    /**
     * @param $sku
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductBySku($sku)
    {
        try {
            $product = $this->_productRepository->get($sku);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noEntityException) {
            $product = null;
        }
        return $product;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    public function isConfigurable($product)
    {
        return $product->getTypeId() === ConfigurableProductType::TYPE_CODE;
    }

    /**
     * @param $product
     * @return mixed|string|null
     */
    public function getProductSize($product, $itemOptions)
    {
        if ($this->_attributeSetHelper->isBeauty($product))
            $size = '';
        else
            $size = __('TU');

        if ($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            /** @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable $productTypeInstance */
            $productTypeInstance = $product->getTypeInstance();
            $productTypeInstance->setStoreFilter($product->getStoreId(), $product);
            $attributes = $productTypeInstance->getConfigurableAttributes($product);
            $sizeAttribute = $attributes->getFirstitem();
            $sizeStoreLabels = array_unique($sizeAttribute->getProductAttribute()->getStoreLabels());
            if(in_array($itemOptions[0]['label'], $sizeStoreLabels))
                $size = $itemOptions[0]['value'];
        }
        if ($this->_attributeSetHelper->isBeauty($product)){
            if ($product->getData('beauty_volume'))
                $size = $product->getData('beauty_volume');
            elseif ($product->getData('beauty_weight'))
                $size = $product->getData('beauty_weight');
        } elseif (($this->_attributeSetHelper->isAccessories($product) || $this->_attributeSetHelper->isBag($product)) &&
            $product->getData('length') && $product->getData('width') && $product->getData('height')){
            $height =  $product->getData('height') * 10;
            $length =  $product->getData('length') * 10;
            $width =  $product->getData('width') * 10;
            $size_array = null;
            if ($height * $width * $length > 0)
                $size_array = [$height. 'mm', $length. 'mm', $width . 'mm'];
            if (!is_null($size_array))
                $size = implode(' x ', $size_array);
            else
                $size = '';
        } elseif (($this->_attributeSetHelper->isAccessories($product) || $this->_attributeSetHelper->isBelts($product)) && $product->getData('size_belts'))
            $size = $product->getData('size_belts');

        return is_array($size) ? $size[0] : $size;
    }
}
