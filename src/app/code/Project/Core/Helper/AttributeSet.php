<?php

namespace Project\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Eav\Model\AttributeSetRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\CacheInterface;
use Magento\Eav\Model\Cache\Type as CacheType;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Serialize\Serializer\Json as Serializer;

/**
 * Class AttributeSet
 * @package Project\Core\Helper
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AttributeSet extends AbstractHelper
{
    const ATTRIBUTE_SETS_SHOES  = ['Pim Shoes'];
    const ATTRIBUTE_SETS_BEAUTY = [
        'Pim Beauty-Eyes',
        'Pim Beauty-Lips',
        'Pim Beauty-Nails',
        'Pim Beauty-Perfumes'
    ];
    const ATTIBUTE_SETS_ACCESSORIES = ['Pim Accessories'];
    const ATTRIBUTE_SETS_BAGS       = ['Pim Bags'];
    const ATTRIBUTE_SETS_EYES       = ['Pim Beauty-Eyes'];
    const ATTRIBUTE_SETS_LIPS       = ['Pim Beauty-Lips'];
    const ATTRIBUTE_SETS_NAILS      = ['Pim Beauty-Nails'];
    const ATTRIBUTE_SETS_PERFUMES   = ['Pim Beauty-Perfumes'];
    const ATTRIBUTE_SETS_BELTS      = ['Pim Belts'];
    const ATTRIBUTE_SETS_BRACELETS  = ['Pim Bracelets'];

    /**
     * @var AttributeSetRepository
     */
    protected $attributeSetRepository;

    /**
     * @var SearchCriteriaBuilder $searchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var array
     */
    protected $attributeSetMapping = [];

    /**
     * @var array
     */
    protected static $defaultCacheTags = [CacheType::CACHE_TAG, Attribute::CACHE_TAG];

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var []
     */
    protected $cacheTags;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * AttributeSet constructor.
     * @param Context $context
     * @param AttributeSetRepository $attributeSetRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CacheInterface $cache
     * @param Serializer $serializer
     * @param array|null $cacheTags
     */
    public function __construct(
        Context $context,
        AttributeSetRepository $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CacheInterface $cache,
        Serializer $serializer,
        array $cacheTags = null
    ) {
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder  = $searchCriteriaBuilder;
        $this->cache                  = $cache;
        $this->cacheTags              = $cacheTags ?: self::$defaultCacheTags;
        $this->serializer             = $serializer;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Eav\Api\Data\AttributeSetSearchResultsInterface
     */
    protected function getAllAttributeSets()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_type_id', 4)
            ->create();

        $attributeSetResult = $this->attributeSetRepository->getList($searchCriteria);

        return $attributeSetResult;
    }

    /**
     * @param $attributeSetResult
     * @return array
     */
    protected function buildAttributeSetMapping($attributeSetResult)
    {
        if (!empty($this->attributeSetMapping)) {
            return $this->attributeSetMapping;
        }

        $this->attributeSetMapping = [];

        foreach ($attributeSetResult->getItems() as $attributeSet) {
            $this->attributeSetMapping[$attributeSet->getAttributeSetId()] = $attributeSet->getAttributeSetName();
        }

        $this->attributeSetMapping = array_unique($this->attributeSetMapping);
    }


    /**
     * @return array
     */
    protected function getAttributeSetMapping()
    {
        $cacheKey = 'catalog-product-list-attributesets';

        $cacheString = $this->cache->load($cacheKey);

        if (false === $cacheString) {
            $attributeSetResult = $this->getAllAttributeSets();

            if ($attributeSetResult->getTotalCount()==0) {
                return [];
            }

            $this->buildAttributeSetMapping($attributeSetResult);

            $this->cache->save(
                $this->serializer->serialize($this->attributeSetMapping),
                $cacheKey,
                $this->cacheTags
            );
        } else {
            $this->attributeSetMapping = $this->serializer->unserialize($cacheString);
        }

        return $this->attributeSetMapping;
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
     * @return string
     */
    public function getAttributeSetName($product)
    {
        if (empty($this->attributeSetMapping)) {
            $attributeSetMapping = $this->getAttributeSetMapping();
        } else {
            $attributeSetMapping = $this->attributeSetMapping;
        }

        if (!array_key_exists($product->getAttributeSetId(), $attributeSetMapping)) {
            return '';
        }

        return $attributeSetMapping[$product->getAttributeSetId()];
    }

    public function checkAttributeSet($product, $attributeSetNameToCompare)
    {
        $attributeSetName = $this->getAttributeSetName($product);

        return array_key_exists($attributeSetName, array_flip($attributeSetNameToCompare));
    }

    /**
     * @param $product
     * @return bool
     */
    public function isBag($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_BAGS);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isBeauty($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_BEAUTY);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isShoes($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_SHOES);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isAccessories($product)
    {
        return $this->checkAttributeSet($product, self::ATTIBUTE_SETS_ACCESSORIES);
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool
     */
    public function isBelts($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_BELTS);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isEyes($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_EYES);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isLips($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_LIPS);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isNails($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_NAILS);
    }

    /**
     * @param $product
     * @return bool
     */
    public function isPerfumes($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_PERFUMES);
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool
     */
    public function isBracelets($product)
    {
        return $this->checkAttributeSet($product, self::ATTRIBUTE_SETS_BRACELETS);
    }
}
