<?php

namespace Project\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Attribute
 * @package Project\Core\Helper
 * @author Synolia <contact@synolia.com>
 */
class Attribute extends AbstractHelper
{
    /**
     * @var \Project\Catalog\Helper\Category
     */
    protected $categoryHelper;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Attribute constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Project\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Project\Catalog\Helper\Category $categoryHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->categoryHelper = $categoryHelper;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * @param $product
     * @param $attributeCode
     * @return string
     */
    public function getAttributeAdminLabel($product, $attributeCode)
    {
        /* @var $attributeSource \Magento\Eav\Model\Entity\Attribute\Source\Table */
        $attributeSource = $product->getResource()
            ->getAttribute($attributeCode)
            ->getSource();

        $allOptions = $attributeSource->getAllOptions(false, true);

        $indexedOptions = array_combine(
            array_column($allOptions, 'value'),
            array_column($allOptions, 'label')
        );

        $attributeValue = $product->getData($attributeCode);

        if (!array_key_exists($attributeValue, $indexedOptions)) {
            return '';
        }

        return $indexedOptions[$attributeValue];
    }

    /**
     * @return false|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomStoreLabel()
    {
        $storeCodeArr = explode("_", $this->getStoreCode());
        if($storeCodeArr[0] == 'fr'){
            $urlKey = $this->categoryHelper->getLevelCategory(2)->getUrlKey();
            switch ($urlKey) {
                case 'femme':
                    $label = 'Pointures Femme';
                    break;
                case 'homme':
                    $label = 'Pointures Homme';
                    break;
                default:
                    return false;
            }
            return $label;
        }
        if($storeCodeArr[1] == 'en'){
            $urlKey = $this->categoryHelper->getLevelCategory(2)->getUrlKey();
            switch ($urlKey) {
                case 'women':
                    $label = 'Women\'s size ';
                    break;
                case 'men':
                    $label = 'Men\'s size';
                    break;
                default:
                    return false;
            }
            return $label;
        }
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }
}
