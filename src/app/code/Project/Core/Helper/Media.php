<?php

namespace Project\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;

/**
 * Class Media
 * @package Project\Core\Helper
 * @author Synolia <contact@synolia.com>
 */
class Media extends AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * Media constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param AttributeSet $attributeSetHelper
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        AttributeSetHelper $attributeSetHelper
    ) {
        parent::__construct($context);

        $this->storeManager           = $storeManager;
        $this->attributeSetHelper     = $attributeSetHelper;
    }

    /**
     * @param $path
     * @return string
     */
    public function getMediaUrl($path)
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$path ;
    }

    /**
     * @param $attributeCode
     * @param $adminValue
     * @return string
     */
    public function getFilterImage($attributeCode, $adminValue)
    {
        return $this->getMediaUrl('synolia/customfilter/'.$attributeCode.'/'.strtolower($adminValue).'.png');
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
     * @return string
     */
    public function getProductListingImageId($product)
    {
        if (!empty($product->getData('override_main_image_listing'))) {
            return 'category_page_grid_override_main';
        } elseif (!empty($product->getData('main_image'))) {
            return 'category_page_grid_main_image';
        } else {
            return 'category_page_grid';
        }
    }

    /**
     * @param $product \Magento\Catalog\Model\Product
     * @return string
     */
    public function getProductListingImageIdHover($product)
    {
        if (!empty($product->getData('override_hover'))) {
            return 'category_page_grid_override_hover';
        } elseif (!empty($product->getData('hover'))) {
            return 'category_page_grid_hover';
        } else {
            return 'category_page_grid';
        }
    }
}
