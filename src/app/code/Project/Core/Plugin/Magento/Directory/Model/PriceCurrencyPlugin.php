<?php

namespace Project\Core\Plugin\Magento\Directory\Model;

use Magento\Directory\Model\PriceCurrency;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class PriceCurrencyPlugin
 * @package Project\Core\Plugin\Magento\Directory\Model
 * @author Synolia <contact@synolia.com>
 */
class PriceCurrencyPlugin
{
    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * PriceCurrencyPlugin constructor.
     *
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        EnvironmentManager $environmentManager
    ) {
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Magento\Directory\Model\PriceCurrency $subject
     * @param callable $proceed
     * @param $amount
     * @param bool $includeContainer
     * @param int $precision
     * @param null $scope
     * @param null $currency
     *
     * @return mixed
     */
    public function aroundFormat(
        PriceCurrency $subject,
        callable $proceed,
        $amount,
        $includeContainer = true,
        $precision = PriceCurrency::DEFAULT_PRECISION,
        $scope = null,
        $currency = null
    ) {
        $requiredPrecision = $this->environmentManager->getEnvironment() === Environment::JP ? 0 : $precision;

        return $proceed(
            $amount,
            $includeContainer,
            $requiredPrecision,
            $scope,
            $currency
        );
    }
}
