<?php

namespace Project\Core\Plugin\Magento\Sales\Model;

use Magento\Sales\Model\Order;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class OrderPlugin
 * @package Project\Core\Plugin\Magento\Sales\Model
 * @author Synolia <contact@synolia.com>
 */
class OrderPlugin
{
    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * PriceCurrencyPlugin constructor.
     *
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        EnvironmentManager $environmentManager
    ) {
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Magento\Sales\Model\Order $subject
     * @param $price
     * @param $precision
     * @param bool $addBrackets
     *
     * @return mixed
     */
    public function beforeFormatPricePrecision(
        Order $subject,
        $price,
        $precision,
        $addBrackets = false
    ) {
        $requiredPrecision = $this->environmentManager->getEnvironment() === Environment::JP ? 0 : $precision;

        return [$price, $requiredPrecision, [], true, $addBrackets];
    }
}
