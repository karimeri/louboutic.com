<?php

namespace Project\Core\Plugin\Customer\Model\ResourceModel\Address\Attribute\Source;

use Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollectionFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CountryPlugin
 *
 * Fixes Allowed countries list in Backend only contains first store countries
 *
 * @package Project\Core\Plugin\Customer\Model\ResourceModel\Address\Attribute\Source
 * @author Synolia <contact@synolia.com>
 */
class CountryPlugin
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $countriesFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * CountryPlugin constructor.
     *
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countriesFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        CountryCollectionFactory $countriesFactory,
        StoreManagerInterface $storeManager,
        State $state
    ) {
        $this->countriesFactory = $countriesFactory;
        $this->storeManager = $storeManager;
        $this->state = $state;
    }

    /**
     * @param \Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Country $subject
     * @param callable $proceed
     *
     * @return array
     */
    public function aroundGetAllOptions(
        Country $subject,
        callable $proceed
    ) {
        if (!$this->options) {
            // Call $proceed for avoiding conflict with other plugins
            $this->options = $proceed();

            // Fill options with all stores values if we are in admin area
            try {
                if ($this->state->getAreaCode() === Area::AREA_ADMINHTML) {
                    foreach ($this->storeManager->getStores() as $store) {
                        $newOptions = $this->createCountriesCollection()->loadByStore(
                            $store->getId()
                        )->toOptionArray();
                        $this->options = \array_merge(
                            $this->options,
                            \array_filter($newOptions, [$this, 'combineOptions'])
                        );
                    }
                }
            } catch (LocalizedException $e) {
                // Unable to determine Area. Just go with normal options
            }
        }

        return $this->options;
    }

    /**
     * Only add new options and drop duplicate
     *
     * @param array $candidateOption
     *
     * @return bool
     */
    public function combineOptions($candidateOption)
    {
        foreach ($this->options as $option) {
            if ($candidateOption['value'] == $option['value']) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    protected function createCountriesCollection()
    {
        return $this->countriesFactory->create();
    }
}
