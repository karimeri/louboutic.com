<?php

namespace Project\Core\Console\Command;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Cms\Model\BlockRepository;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Project\Core\Manager\EnvironmentManager;
use Eu\Core\Setup\UpgradeData as EuUpgradeData;
use Hk\Core\Setup\UpgradeData as HkUpgradeData;
use Jp\Core\Setup\UpgradeData as JpUpgradeData;
use Us\Core\Setup\UpgradeData as UsUpgradeData;

/**
 * Class LandingAssociation
 * @package Project\Core\Console\Command
 * @author Synolia <contact@synolia.com>
 */
class LandingAssociation extends Command
{

    const LANDING_LINKS_BLOCKS = [
        'landing-links-women',
        'landing-links-men',
        'landing-links-leather-goods',
        'landing-links-beauty'
    ];

    protected $landingCmsBlocks = [
        ['identifier' => 'landing-content-women'],
        ['identifier' => 'landing-content-men'],
        ['identifier' => 'landing-content-leather-goods'],
        ['identifier' => 'landing-content-beauty']
    ];

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var array
     */
    protected $topCategories;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SearchCriteriaBuilder
     */

    protected $searchCriteriaBuilder;
    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var EuUpgradeData
     */
    protected $euUpgradeData;

    /**
     * @var HkUpgradeData
     */
    protected $hkUpgradeData;

    /**
     * @var JpUpgradeData
     */
    protected $jpUpgradeData;

    /**
     * @var UsUpgradeData
     */
    protected $usUpgradeData;

    /**
     * @var array
     */
    protected $stores;


    /**
     * LandingPageAssociation constructor.
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param BlockRepository $blockRepository
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepository $categoryRepository
     * @param EnvironmentManager $environmentManager
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param EuUpgradeData $euUpgradeData
     * @param HkUpgradeData $hkUpgradeData
     * @param JpUpgradeData $jpUpgradeData
     * @param UsUpgradeData $usUpgradeData
     */
    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        BlockRepository $blockRepository,
        CategoryRepository $categoryRepository,
        StoreManagerInterface $storeManager,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        EnvironmentManager $environmentManager,
        EuUpgradeData $euUpgradeData,
        HkUpgradeData $hkUpgradeData,
        JpUpgradeData $jpUpgradeData,
        UsUpgradeData $usUpgradeData
    ) {
        parent::__construct();

        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->blockRepository           = $blockRepository;
        $this->categoryRepository        = $categoryRepository;
        $this->storeManager              = $storeManager;
        $this->searchCriteriaBuilder     = $searchCriteriaBuilder;
        $this->environmentManager        = $environmentManager;
        $this->euUpgradeData             = $euUpgradeData;
        $this->hkUpgradeData             = $hkUpgradeData;
        $this->jpUpgradeData             = $jpUpgradeData;
        $this->usUpgradeData             = $usUpgradeData;
    }

    public function configure()
    {
        $this->setName('project:landingpage:association')
            ->setDescription('Associate top categories with landing CMS blocks');
    }

    /**
     * @var $input InputInterface
     * @var $output OutputInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $this->findStores();
        $this->findTopCategories();
        $this->findLandingCmsBlockIds();
        $this->attachCmsBlocksToCategories();
        $this->changeLandingLinks();
    }

    protected function findStores()
    {
        if ($this->environmentManager->getEnvironment()) {
            try {
                switch ($this->environmentManager->getEnvironment()) {
                    case 'eu':
                        $euStores = $this->euUpgradeData->getStoresIndexedByLocale();
                        $this->stores = array_merge(
                            $euStores['fr_FR'],
                            $euStores['en_GB']
                        );
                        break;
                    case 'hk':
                        $hkStores = $this->hkUpgradeData->getStoresIndexedByLocale();
                        $this->stores = array_merge(
                            $hkStores['en_AU'],
                            $hkStores['zh_Hans_CN'],
                            $hkStores['zh_Hant_HK']
                        );
                        break;
                    case 'jp':
                        $jpStores = $this->jpUpgradeData->getStores();
                        $this->stores = $jpStores[JpUpgradeData::STORE_CODE_JP_JA];
                        break;
                    case 'us':
                        $usStores = $this->usUpgradeData->getStores();
                        $this->stores = array_merge(
                            $usStores[UsUpgradeData::STORE_CODE_US_EN],
                            $usStores[UsUpgradeData::STORE_CODE_CA_EN],
                            $usStores[UsUpgradeData::STORE_CODE_CA_FR],
                            $usStores[UsUpgradeData::STORE_CODE_OT_EN]
                        );
                        break;
                }
            } catch (\Exception $exception) {
                $this->output->writeln('<error>'.$exception->getMessage().'</error>');
            }
        }
    }

    /**
     * Get landing page cms blocks
     */
    protected function findLandingCmsBlockIds()
    {
        foreach ($this->landingCmsBlocks as &$blockData) {
            /* We first search on default scope and then loop on env stores if no block were found */
            $searchCriteriaAdminStore = $this->searchCriteriaBuilder
                ->addFilter('identifier', $blockData['identifier'])
                ->addFilter('store_id', 0)
                ->create();

            $searchResult = $this->blockRepository->getList($searchCriteriaAdminStore);
            $nbResults = $searchResult->getTotalCount();

            if ($nbResults) {
                $cmsBlock = array_values($searchResult->getItems())[0];
                $blockData['values'][0] = $cmsBlock->getId();
                $this->output->writeln('<info>Cms block '.$blockData['identifier']
                    .' was found on default scope</info>');
            } else {
                foreach ($this->stores as $storeId) {
                    $searchCriteriaStore = $this->searchCriteriaBuilder
                        ->addFilter('identifier', $blockData['identifier'])
                        ->addFilter('store_id', $storeId)
                        ->create();

                    $searchResult = $this->blockRepository->getList($searchCriteriaStore);
                    $nbResults = $searchResult->getTotalCount();

                    if ($nbResults) {
                        $cmsBlock = array_values($searchResult->getItems())[0];
                        $blockData['values'][$storeId] = $cmsBlock->getId();
                        $this->output->writeln('<info>Cms block '.$blockData['identifier']
                            .' was found on store '.$storeId.' scope</info>');
                    }
                }
            }

            if (!$nbResults) {
                $this->output->writeln('<error>Cms block '.$blockData['identifier']
                    .' was not found. Skipping.</error>');
            }
        }
    }

    /**
     * Find active, included in menu, top level categories
     * @throws LocalizedException
     */
    protected function findTopCategories()
    {
        $this->topCategories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('level', 2)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('include_in_menu', 1)
            ->setOrder('position', 'ASC')
            ->getItems();

        $categoryIds = implode(',', array_keys($this->topCategories));

        if (count($this->topCategories)) {
            $this->topCategories = array_values($this->topCategories);
        }

        $this->output->writeln('<info>'.count($this->topCategories).' top categories found ('.$categoryIds.')'
            .'(filtered on is_active & include_in_menu)</info>');
    }


    /**
     * Affect landing cms block to top categories
     * @throws LocalizedException
     */
    protected function attachCmsBlocksToCategories()
    {
        $index = 0;
        /** @var Category $category */
        foreach ($this->topCategories as $category) {
            if (!array_key_exists($index, $this->landingCmsBlocks)
                || !array_key_exists('values', $this->landingCmsBlocks[$index])) {
                $this->output->writeln('<error>No cms block for category number '.$index.', skipping.</error>');
                continue;
            }

            foreach ($this->landingCmsBlocks[$index]['values'] as $storeId => $cmsBlockId) {
                $this->storeManager->setCurrentStore($storeId); // else attribute are saved on the first store

                $category->setDisplayMode(Category::DM_PAGE)
                    ->setLandingPage($cmsBlockId);

                try {
                    // @codingStandardsIgnoreLine
                    $this->categoryRepository->save($category);
                    $this->output->writeln('<info>Category '.$category->getId()
                        .' was successfully attached to cms block '.$cmsBlockId
                        .' on store '.$storeId.'</info>');
                } catch (CouldNotSaveException $exception) {
                    $this->output->writeln('<error>Error on Category '.$category->getId()
                        .' when trying a modification on store '.$storeId.'</error>');
                    $this->output->writeln('<error>'.$exception->getMessage().'</error>');
                }
            }

            $index++;
        }
    }

    /**
     * Parse landing blocks to find category links widget and change param id_path to make it target real categories
     */
    protected function changeLandingLinks()
    {
        $index = 0;
        if (count($this->topCategories)) {
            foreach (self::LANDING_LINKS_BLOCKS as $identifier) {
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter('identifier', $identifier)
                    ->create();

                $searchResult = $this->blockRepository->getList($searchCriteria);

                foreach ($searchResult->getItems() as $cmsBlock) {
                    $this->insertWidgetInsteadOfLinks($index, $cmsBlock);
                }

                $index++;
            }
        }
    }

    /**
     * @param $index
     * @param $cmsBlock
     */
    protected function insertWidgetInsteadOfLinks($index, $cmsBlock)
    {
        $content = $cmsBlock->getContent();

        /* @var Category $category */
        $category = $this->topCategories[$index];

        $categoryLinks = '';
        $nbChildren = 0;
        foreach ($category->getChildrenCategories() as $childCategory) {
            if ($nbChildren>4) {
                break;
            }

            $categoryLinks .= '
                            <li class="filter-options-item">
                                {{widget type="Magento\Catalog\Block\Category\Widget\Link"
                                    anchor_text="'.$childCategory->getName().'"
                                    title="'.$childCategory->getName().'"
                                    template="category/widget/link/link_inline.phtml"
                                    id_path="category/'.$childCategory->getId().'"}}
                            </li>';
            $nbChildren++;
        }
        $replacedContent = str_replace(
            '<li class="no-display">__CHILD_CATEGORIES__</li>',
            $categoryLinks,
            $content
        );

        $cmsBlock->setContent($replacedContent);

        try {
            // @codingStandardsIgnoreLine
            $this->blockRepository->save($cmsBlock);
            $this->output->writeln('<info>Cms block '.$cmsBlock->getIdentifier()
                .' successfully modified</info>');
        } catch (CouldNotSaveException $exception) {
            $this->output->writeln('<error>'.$exception->getMessage().'</error>');
        }
    }
}
