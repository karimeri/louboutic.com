<?php

namespace Project\Core\Console\Command;

use Magento\Cms\Model\BlockRepository;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Cms\Model\PageRepository;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Project\Core\Manager\EnvironmentManager;
use Eu\Core\Setup\UpgradeData as EuUpgradeData;
use Hk\Core\Setup\UpgradeData as HkUpgradeData;
use Jp\Core\Setup\UpgradeData as JpUpgradeData;
use Us\Core\Setup\UpgradeData as UsUpgradeData;

/**
 * Class CmsStoreAssociation
 * @package Projec\Core\Helper
 * @author Synolia <contact@synolia.com>
 */
class CmsStoreAssociation extends Command
{
    const CMS_BLOCK_FIELDS_TO_KEEP_ON_DUPLICATION = [
        'is_active'  => 1,
        'identifier' => 1,
        'title'      => 1,
        'content'    => 1
    ];

    const CMS_PAGE_FIELDS_TO_KEEP_ON_DUPLICATION = [
        'is_active'        => 1,
        'identifier'       => 1,
        'meta_title'       => 1,
        'meta_description' => 1,
        'title'            => 1,
        'content_heading'  => 1,
        'content'          => 1,
        'page_layout'      => 1
    ];

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var PageRepository
     */
    protected $pageRepository;

    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var EuUpgradeData
     */
    protected $euUpgradeData;

    /**
     * @var HkUpgradeData
     */
    protected $hkUpgradeData;

    /**
     * @var JpUpgradeData
     */
    protected $jpUpgradeData;

    /**
     * @var UsUpgradeData
     */
    protected $usUpgradeData;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;


    /**
     * CmsStoreAssociation constructor.
     * @param BlockRepository $blockRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param BlockFactory $blockFactory
     * @param PageRepository $pageRepository
     * @param PageFactory $pageFactory
     * @param EuUpgradeData $euUpgradeData
     * @param HkUpgradeData $hkUpgradeData
     * @param JpUpgradeData $jpUpgradeData
     * @param UsUpgradeData $usUpgradeData
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        BlockRepository $blockRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        BlockFactory $blockFactory,
        PageRepository $pageRepository,
        PageFactory $pageFactory,
        EuUpgradeData $euUpgradeData,
        HkUpgradeData $hkUpgradeData,
        JpUpgradeData $jpUpgradeData,
        UsUpgradeData $usUpgradeData,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct();

        $this->blockRepository       = $blockRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->blockFactory          = $blockFactory;
        $this->pageRepository        = $pageRepository;
        $this->pageFactory           = $pageFactory;
        $this->environmentManager    = $environmentManager;
        $this->euUpgradeData         = $euUpgradeData;
        $this->hkUpgradeData         = $hkUpgradeData;
        $this->jpUpgradeData         = $jpUpgradeData;
        $this->usUpgradeData         = $usUpgradeData;
    }

    protected function configure()
    {
        $this->setName('project:cms:storeassociation')
            ->setDescription('Duplicate the CMS Blocks & Pages previously registred on default scope'.
             ' and affect them to specifics env stores');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stores = [];
        if ($this->environmentManager->getEnvironment()) {
            try {
                switch ($this->environmentManager->getEnvironment()) {
                    case 'eu':
                        $stores = $this->euUpgradeData->getStoresIndexedByLocale();
                        break;
                    case 'hk':
                        $stores = $this->hkUpgradeData->getStoresIndexedByLocale();
                        break;
                    case 'jp':
                        $stores = $this->jpUpgradeData->getStores();
                        break;
                    case 'us':
                        $stores = $this->usUpgradeData->getStores();
                        break;
                }
            } catch (\Exception $exception) {
                $output->writeln('<error>'.$exception->getMessage().'</error>');
            }

            if ($stores) {
                $output->writeln('Assign Admin Cms Blocks To Stores...');
                $errors = $this->assignAdminCmsBlocksToStores($stores);
                foreach ($errors as $error) {
                    $output->writeln('<info>'.$error.'</info>');
                }

                $output->writeln('Assign Admin Cms Pages To Stores...');
                $errors = $this->assignAdminCmsPagesToStores($stores);
                foreach ($errors as $error) {
                    $output->writeln('<info>'.$error.'</info>');
                }
            }
        }
    }

    /**
     * @param $storesList
     * @return array
     */
    public function assignAdminCmsBlocksToStores($storesList)
    {
        $messages = [];
        $blocksToDelete = [];

        $searchCriteria = $this->searchCriteriaBuilder->addFilter('store_id', 0)
            ->create();

        $searchResult = $this->blockRepository->getList($searchCriteria);

        foreach ($storesList as $stores) {
            if ($searchResult->getTotalCount()>0) {
                /* @var $initialCmsBlock \Magento\Cms\Model\Block */
                foreach ($searchResult->getItems() as $initialCmsBlock) {
                    $dataToKeep = array_intersect_key(
                        $initialCmsBlock->getData(),
                        self::CMS_BLOCK_FIELDS_TO_KEEP_ON_DUPLICATION
                    );

                    /* @var $newBlock \Magento\Cms\Model\Block */
                    $newBlock = $this->blockFactory->create();
                    $newBlock->addData($dataToKeep)
                        ->setStoreId($stores);

                    try {
                        // @codingStandardsIgnoreLine
                        $this->blockRepository->save($newBlock);
                    } catch (CouldNotSaveException $exception) {
                        $messages[] = __('Error when duplicating block '.$initialCmsBlock->getIdentifier()
                            .' '.$initialCmsBlock->getId()
                            .' ('.implode(',', $initialCmsBlock->getStoreId()).')'
                            .' on stores '.implode(',', $stores))
                            ."\n".$exception->getMessage()."\n";
                        continue;
                    }

                    $blocksToDelete[] = $initialCmsBlock->getId();
                }
            }
        }

        foreach (array_unique($blocksToDelete) as $blockId) {
            try {
                $this->blockRepository->deleteById($blockId);
            } catch (\Exception $exception) {
                $messages[] = $exception->getMessage();
                continue;
            }
        }

        return $messages;
    }

    /**
     * @param $storesList
     * @return array
     */
    public function assignAdminCmsPagesToStores($storesList)
    {
        $messages = [];
        $pagesToDelete = [];

        $searchCriteria = $this->searchCriteriaBuilder->addFilter('store_id', 0)
            ->create();

        $searchResult = $this->pageRepository->getList($searchCriteria);

        foreach ($storesList as $stores) {
            if ($searchResult->getTotalCount()>0) {
                /* @var $initialCmsBlock \Magento\Cms\Model\Block */
                foreach ($searchResult->getItems() as $initialCmsPage) {
                    $dataToKeep = array_intersect_key(
                        $initialCmsPage->getData(),
                        self::CMS_PAGE_FIELDS_TO_KEEP_ON_DUPLICATION
                    );

                    /** @var $newPage \Magento\Cms\Model\Page */
                    $newPage = $this->pageFactory->create();
                    $newPage->addData($dataToKeep)
                        ->setStoreId($stores);

                    try {
                        // @codingStandardsIgnoreLine
                        $this->pageRepository->save($newPage);
                    } catch (CouldNotSaveException $exception) {
                        $messages[] = __('Error when duplicating page '.$initialCmsPage->getIdentifier()
                            .' '.$initialCmsPage->getId()
                            .' ('.implode(',', $initialCmsPage->getStoreId()).')'
                            .' on stores '.implode(',', $stores))
                            ."\n".$exception->getMessage()."\n";
                        continue;
                    }

                    $pagesToDelete[] = $initialCmsPage->getId();
                }
            }
        }

        foreach (array_unique($pagesToDelete) as $pageId) {
            try {
                $this->pageRepository->deleteById($pageId);
            } catch (\Exception $exception) {
                $messages[] = $exception->getMessage();
                continue;
            }
        }

        return $messages;
    }
}
