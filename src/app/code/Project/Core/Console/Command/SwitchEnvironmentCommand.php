<?php

namespace Project\Core\Console\Command;

use Magento\Framework\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * Class SwitchEnvironmentCommand
 *
 * @package   Project\Core\Console\Command
 * @author    Synolia <contact@synolia.com>
 */
class SwitchEnvironmentCommand extends Command
{
    const INPUT_ENVIRONMENT = 'environment';
    const INPUT_DEV_OPTION  = 'dev';
    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * SwitchEnvironmentCommand constructor.
     *
     * @param \Magento\Framework\Filesystem $fileSystem
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:switch:environment')
            ->setDescription('Changes the louboutin environment')
            ->addArgument(self::INPUT_ENVIRONMENT, InputArgument::REQUIRED, 'Which environment?')
            ->addOption(self::INPUT_DEV_OPTION, null, InputOption::VALUE_NONE, 'Enable dev mode');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $environment = $input->getArgument(self::INPUT_ENVIRONMENT);

            $output->writeln(sprintf("Rename env_%s.php into env.php ...\n", $environment));
            $writer = $this->fileSystem->getDirectoryWrite('etc');
            $writer->copyFile(sprintf('env_%s.php', $environment), 'env.php');
            $output->writeln("<info>env.php rename successful !</info>");

            $output->writeln(sprintf("Rename projectEnv_%s.php into projectEnv.php ...", $environment));
            $writer->copyFile(sprintf('projectEnv_%s.php', $environment), 'projectEnv.php');
            $output->writeln("<info>projectEnv.php rename successful !</info>");

            $output->writeln("Flushing cache ...");
            $cacheFlushProcess = new Process('php bin/magento cache:flush');
            $cacheFlushProcess->run();

            if (!$cacheFlushProcess->isSuccessful()) {
                throw new ProcessFailedException($cacheFlushProcess);
            }

            $output->writeln($cacheFlushProcess->getOutput());
            $output->writeln("<info>Cache flush successful !</info>");

            // Enable developer mode only if we specified it
            if ($input->getOption(self::INPUT_DEV_OPTION)) {
                $output->writeln("<comment>Enabling developer mode ...</comment>");

                $devModeProcess = new Process('php bin/magento deploy:mode:set developer');
                $devModeProcess->run();

                if (!$devModeProcess->isSuccessful()) {
                    throw new ProcessFailedException($devModeProcess);
                }

                $output->writeln($devModeProcess->getOutput());
            }
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');

            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}
