<?php

namespace Project\Core\Block\Checkout\Minicart;

use Magento\Framework\View\Element\Html\Links;
use Magento\Catalog\Block\ShortcutInterface;
use Magento\Framework\View\Element\Template;

/**
 * Class Shortcuts
 * @package Project\Core\Block\Checkout\Minicart
 * @author Synolia <contact@synolia.com>
 */
class Shortcuts extends Links implements ShortcutInterface
{
    /**
     * @var string
     */
    protected $alias;

    /**
     * Shortcuts constructor.
     * @param Template\Context $context
     * @param string $alias
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        string $alias = '',
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
