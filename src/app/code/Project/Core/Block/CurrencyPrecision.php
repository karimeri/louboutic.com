<?php

namespace Project\Core\Block;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class CurrencyPrecision
 * @package Project\Core\Block
 * @author Synolia <contact@synolia.com>
 */
class CurrencyPrecision extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * CurrencyPrecision constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct($context);

        $this->environmentManager = $environmentManager;
    }

    /**
     * @return int|string
     */
    public function getCurrencyPrecision()
    {
        return $this->environmentManager->getEnvironment() === Environment::JP ? 0 : 'null';
    }
}
