<?php

namespace Project\Core\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

/**
 * Class Video
 * @package Project\Core\Block\Widget
 * @author Synolia <contact@synolia.com>
 * @codingStandardsIgnoreFile
 */
class Video extends Template implements BlockInterface
{
    protected $_template = "widget/video.phtml";

    /**
     * @param $videoUrl
     * @return string|bool
     */
    public function retrieveVideoUrl($videoUrl)
    {
        $content = \file_get_contents($videoUrl);
        //get config js in page
        $config = \preg_match("(var config =.*;)", $content, $newConfig);

        if ($config){
            $json = \substr($newConfig[0], 13, -1);
            $array = \json_decode($json, true);
            //retrieve video url in object
            $video = $array['request']['files']['progressive'][0];
            return $video['url'];
        }

        return false;
    }
}