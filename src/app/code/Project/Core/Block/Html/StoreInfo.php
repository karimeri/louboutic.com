<?php

namespace Project\Core\Block\Html;

use Magento\Framework\View\Element\Template;

/**
 * Class StoreInfo
 * @package Project\Core\Block\Html
 * @author Synolia <contact@synolia.com>
 */
class StoreInfo extends Template
{
    /**
     * @return \Magento\Store\Api\Data\WebsiteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWebsite()
    {
        return $this->_storeManager->getWebsite();
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }
}
