<?php

namespace Project\Core\Block\Html;

/**
 * Class BreadcrumbsCopy
 * @package Project\Core\Block\Html
 * @author Synolia <contact@synolia.com>
 */
class BreadcrumbsCopy extends \Magento\Theme\Block\Html\Breadcrumbs
{
    /**
     * @return string
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _toHtml()
    {
        return $this->getLayout()->getBlock('breadcrumbs')
                                 ->setIsFooterBreadcrumbs(1)
                                 ->toHtml();
    }
}
