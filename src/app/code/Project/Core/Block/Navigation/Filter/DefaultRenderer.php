<?php

namespace Project\Core\Block\Navigation\Filter;

use Magento\Framework\View\Element\Template;
use Magento\LayeredNavigation\Block\Navigation\FilterRenderer;
use Magento\Framework\App\CacheInterface;
use Magento\Eav\Model\Cache\Type as CacheType;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Serialize\Serializer\Json as Serializer;

/**
 * Class DefaultRenderer
 * @package Project\Core\Block\Navigation\Filter
 * @author Synolia <contact@synolia.com>
 */
class DefaultRenderer extends FilterRenderer
{
    /**
     * @var array
     */
    protected static $defaultCacheTags = [CacheType::CACHE_TAG, Attribute::CACHE_TAG];

    /**
     * @var []
     */
    protected $attributeOptions;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var []
     */
    protected $cacheTags;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * DefaultRenderer constructor.
     * @param Template\Context $context
     * @param CacheInterface $cache
     * @param Serializer $serializer
     * @param array|null $cacheTags
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CacheInterface $cache,
        Serializer $serializer,
        array $cacheTags = null,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->cache      = $cache;
        $this->cacheTags  = $cacheTags ?: self::$defaultCacheTags;
        $this->serializer = $serializer;
    }

    /**
     * @param $item \Synolia\CustomFilter\Model\Layer\Filter\Item
     * @return string
     */
    public function getAdminLabelFromFilterItem($item)
    {
        if (!$this->attributeOptions) {
            try {
                /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
                $attribute = $item->getFilter()->getAttributeModel();

                $cacheKey = 'customfilter-attribute-navigation-option-' .
                    $attribute->getAttributeCode() . '-' .
                    $this->_storeManager->getStore()->getId();

                $cacheString = $this->cache->load($cacheKey);
                if (false === $cacheString) {
                    $allOptions = $attribute->getSource()->getAllOptions(false, true);

                    $optionIds   = array_column($allOptions, 'value');
                    $adminLabels = array_column($allOptions, 'label');

                    $this->attributeOptions = array_combine($optionIds, $adminLabels);

                    $this->cache->save(
                        $this->serializer->serialize($this->attributeOptions),
                        $cacheKey,
                        $this->cacheTags
                    );
                } else {
                    $this->attributeOptions = $this->serializer->unserialize($cacheString);
                }
            } catch (\Exception $e) {
                return '';
            }
        }

        $optionId = $item->getValue();

        if (!array_key_exists($optionId, $this->attributeOptions)) {
            return '';
        }

        return $this->attributeOptions[$optionId];
    }

    /**
     * @param $adminLabel
     * @return string
     * @SuppressWarnings(PHPMD)
     */
    public function getIconClass($adminLabel)
    {
        $class = "";

        if ($adminLabel) {
            switch ($adminLabel) {
                case "(0-30mm / 0-1.2 pouces)":
                case "(0-30mm / 0-1.2 inches)":
                    $class = " icon-1";
                    break;
                case "(30-60mm / 1.2-2.3 pouces)":
                case "(30-60mm / 1.2-2.3 inches)":
                    $class = " icon-2";
                    break;
                case "(60-120mm / 2.3-4.7 pouces)":
                case "(60-120mm / 2.3-4.7 inches)":
                    $class = " icon-3";
                    break;
                case "(120-200mm / 4.7-7.8 pouces)":
                case "(120-200mm / 4.7-7.8 inches)":
                    $class = " icon-4";
                    break;
            }
        }

        return $class;
    }
}
