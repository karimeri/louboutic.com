<?php

namespace Project\Core\Block\Navigation\Menu;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Synolia\AdvancedMenu\Helper\Config;
use Synolia\AdvancedMenu\Model\Catalog\Resource\CategoryFactory as ResourceCategoryFactory;
use Synolia\AdvancedMenu\Model\Eav\Entity\Category\Menu\ConfigFactory;

/**
 * Class RootMenu
 * @package Project\Core\Block\Navigation\Menu\RootView
 * @author Synolia <contact@synolia.com>
 */
class RootMenu extends \Synolia\AdvancedMenu\Block\Catalog\Navigation\Menu
{
    /**
     * @var CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * RootMenu constructor.
     * @param Context $context
     * @param Config $helperConfig
     * @param ConfigFactory $categoryMenuConfig
     * @param Registry $registry
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ResourceCategoryFactory $resourceCategoryFactory
     * @param CollectionFactory $categoryCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $helperConfig,
        ConfigFactory $categoryMenuConfig,
        Registry $registry,
        CategoryRepositoryInterface $categoryRepository,
        ResourceCategoryFactory $resourceCategoryFactory,
        CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $helperConfig,
            $categoryMenuConfig,
            $registry,
            $categoryRepository,
            $resourceCategoryFactory,
            $categoryCollectionFactory,
            $data
        );

        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }


    /**
     * @param string $attributeCode
     * @param string $attributeValue
     * @return mixed
     */
    public function findCategoriesByAttributeValue($attributeCode, $attributeValue)
    {
        if (!is_null($this->mainCategories) && count($this->mainCategories)) {
            return $this->mainCategories;
        }

        $rootCategoryId = $this->_storeManager->getStore()->getRootCategoryId();

        $this->mainCategories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToFilter($attributeCode, ['eq' => $attributeValue])
            ->addFieldToFilter('parent_id', $rootCategoryId)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('include_in_menu', 1)
            ->setOrder('position', 'ASC')
            ->getItems();

        return $this->mainCategories;
    }

    /**
     * @param string $attributeCode
     * @param string $attributeValue
     * @return mixed
     */
    public function excludeCategoriesByAttributeValue($attributeCode, $attributeValue)
    {
        if (!is_null($this->mainCategories) && count($this->mainCategories)) {
            return $this->mainCategories;
        }

        $rootCategoryId = $this->_storeManager->getStore()->getRootCategoryId();

        $this->mainCategories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('name')
            ->addAttributeToFilter($attributeCode, [
                ['neq' => $attributeValue],
                ['null' => true]
            ])
            ->addFieldToFilter('parent_id', $rootCategoryId)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('include_in_menu', 1)
            ->setOrder('position', 'ASC')
            ->getItems();

        return $this->mainCategories;
    }

    /**
     * @return array
     */
    public function getMainCategories()
    {
        if (is_null($this->mainCategories)) {
            if (!$this->hasActiveChildren($this->getRootCategory())) {
                return [];
            }

            $this->mainCategories = $this->getChildrenCategories($this->getRootCategory());
        }

        foreach ($this->mainCategories as &$category) {
            $category->setMenuTemplate($this->getCategoryTemplate($category));
            $category->setMenuBlockType($this->getCategoryBlockType($category));
        }

        return $this->mainCategories;
    }
}
