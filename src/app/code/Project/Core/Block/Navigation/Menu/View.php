<?php

namespace Project\Core\Block\Navigation\Menu;

use Magento\Framework\Filter\FilterManager;
use Magento\Framework\View\Element\Template\Context;
use Synolia\AdvancedMenu\Helper\Config;
use Synolia\AdvancedMenu\Model\Eav\Entity\Category\Menu\ConfigFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\CategoryFactory;
use Magento\Catalog\Model\Category;
use Magento\Framework\Registry;
use Synolia\AdvancedMenu\Model\Catalog\Resource\CategoryFactory as ResourceCategoryFactory;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Class View
 * @package Project\Core\Block\Navigation\Menu
 * @author Synolia <contact@synolia.com>
 */
class View extends \Synolia\AdvancedMenu\Block\Catalog\Navigation\Menu\View
{
    const MENU_SEGMENT_CMS_BLOCK_PATTERN = 'menu-%s-links';

    /**
     * @var []
     */
    protected $expandsCategories;

    /**
     * @var []
     */
    protected $nonExpandsCategories;

    /**
     * Core data
     *
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * View constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Synolia\AdvancedMenu\Helper\Config $helperConfig
     * @param \Synolia\AdvancedMenu\Model\Eav\Entity\Category\Menu\ConfigFactory $categoryMenuConfig
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Catalog\Model\ResourceModel\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\Category $category
     * @param \Synolia\AdvancedMenu\Model\Catalog\Resource\CategoryFactory $resourceCategoryFactory
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $helperConfig,
        ConfigFactory $categoryMenuConfig,
        Registry $registry,
        CategoryRepositoryInterface
        $categoryRepository,
        CategoryFactory $categoryFactory,
        Category $category,
        ResourceCategoryFactory $resourceCategoryFactory,
        FilterManager $filterManager,
        CustomerSession $customerSession,
        array $data
    ) {
        parent::__construct(
            $context,
            $helperConfig,
            $categoryMenuConfig,
            $registry,
            $categoryRepository,
            $categoryFactory,
            $category,
            $resourceCategoryFactory,
            $customerSession,
            $data
        );

        $this->filterManager = $filterManager;
    }

    /**
     * @return bool
     */
    public function isExpandable()
    {
        if ($this->getMenuLevel()!=0 || !is_array($this->getActiveChildren()) || ($this->getActiveChildren() && count($this->getActiveChildren())==0)) {
            return false;
        }

        if (is_null($this->getExpandedCategories()) || !is_array($this->getActiveChildren()) || count($this->getExpandedCategories()) == 0) {
            return false;
        }

        return true;
    }

    protected function sortExpandedCategories()
    {
        if ($this->getActiveChildren())
            foreach ($this->getActiveChildren() as $category) {
                if ($category->getLeftMenuExpands()) {
                    $this->expandsCategories[] = $category;
                } else {
                    $this->nonExpandsCategories[] = $category;
                }
            }
    }

    /**
     * @return array
     */
    public function getExpandedCategories()
    {
        if (!is_null($this->expandsCategories)) {
            return $this->expandsCategories;
        }

        $this->sortExpandedCategories();

        return $this->expandsCategories;
    }

    /**
     * @return array
     */
    public function getNonExpandedCategories()
    {
        if (!is_null($this->expandsCategories)) {
            return $this->nonExpandsCategories;
        }

        $this->sortExpandedCategories();

        return $this->nonExpandsCategories;
    }

    /**
     * @return string
     */
    public function getMenuCmsBlockHtml()
    {
        $category = $this->getCategory();

        $categoryName = $category->getName();

        $cmsBlockIdentifier = sprintf(
            self::MENU_SEGMENT_CMS_BLOCK_PATTERN,
            $this->filterManager->translitUrl($categoryName)
        );

        return $this->getLayout()
            ->createBlock(
                'Magento\Cms\Block\Block'
            )->setBlockId($cmsBlockIdentifier)
            ->toHtml();
    }
}
