<?php

namespace Project\Core\Block\Product\Price;

use Magento\Framework\View\Element\Template;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class Adjustment
 * @package Project\Core\Block\Product\Price
 * @author Synolia <contact@synolia.com>
 *
 * @method setTemplates(array $templates)
 * @method array getTemplates()
 */
class Adjustment extends \Magento\Framework\View\Element\Template
{
    /**
     * @var EnvironmentManager
     */
    protected $environnmentManager;

    /**
     * Adjustment constructor.
     * @param Template\Context $context
     * @param EnvironmentManager $environmentManager
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        EnvironmentManager $environmentManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->environnmentManager = $environmentManager;
    }

    /**
     * @return mixed|string
     */
    public function getTemplate()
    {
        $environmentCode = $this->environnmentManager->getEnvironment();
        $templates = $this->getTemplates();

        if ($environmentCode && array_key_exists($environmentCode, $templates)) {
            return $templates[$environmentCode];
        }

        return parent::getTemplate();
    }
}
