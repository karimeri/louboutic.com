<?php

namespace Project\Core\Block\Product;

use Magento\Catalog\Block\Product\View\Description;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;
use Project\Core\Helper\Attribute as AttributeHelper;
use Synolia\StandardDesign\Helper\Cms as CmsHelper;

/**
 * Class SizeSuggestions
 * @package Project\Core\Block\Product
 * @author Synolia <contact@synolia.com>
 *
 * @method array getGuideCmsBlocks()
 * @method setGuideCmsBlocks(array $cmsBlocks)
 * @method array getBeltGuideCmsBlocks()
 * @method setBeltGuideCmsBlocks(array $cmsBlocks)
 * @method array getBraceletGuideCmsBlocks()
 * @method setBraceletGuideCmsBlocks(array $cmsBlocks)
 * @method array getSuggestionsCmsBlocks()
 * @method setSuggestionsCmsBlocks(array $cmsBlocks)
 */
class SizeSuggestions extends Description
{
    const GENDER_ATTRIBUTE      = 'gender';
    const SIZE_ADVICE_ATTRIBUTE = 'size_advice';

    /**
     * @var AttributeSetHelper
     */
    protected $attributeSetHelper;

    /**
     * @var AttributeHelper
     */
    protected $attributeHelper;

    /**
     * @var CmsHelper
     */
    protected $cmsHelper;

    /**
     * SizeSuggestions constructor.
     * @param Context $context
     * @param Registry $registry
     * @param AttributeSetHelper $attributeSetHelper
     * @param AttributeHelper $attributeHelper
     * @param CmsHelper $cmsHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AttributeSetHelper $attributeSetHelper,
        AttributeHelper $attributeHelper,
        CmsHelper $cmsHelper,
        array $data = []
    ) {
        $this->attributeSetHelper = $attributeSetHelper;
        $this->attributeHelper = $attributeHelper;
        $this->cmsHelper = $cmsHelper;

        parent::__construct($context, $registry, $data);
    }

    /**
     * @return string
     */
    public function getSizeGuideHtml()
    {
        $product = $this->getProduct();

        if (!$this->attributeSetHelper->isShoes($product)) {
            return '';
        }

        $genderValue = $this->attributeHelper->getAttributeAdminLabel($product, self::GENDER_ATTRIBUTE);

        return $this->getCmsBlockHtml($this->getGuideCmsBlocks(), $genderValue);
    }

    /**
     * @return string
     */
    public function getBeltSizeGuideHtml()
    {
        $product = $this->getProduct();

        if (!$this->attributeSetHelper->isBelts($product)) {
            return '';
        }

        $genderValue = $this->attributeHelper->getAttributeAdminLabel($product, self::GENDER_ATTRIBUTE);

        return $this->getCmsBlockHtml($this->getBeltGuideCmsBlocks(), $genderValue);
    }

    /**
     * @return string
     */
    public function getBraceletSizeGuideHtml()
    {
        $product = $this->getProduct();

        if (!$this->attributeSetHelper->isBracelets($product)) {
            return '';
        }

        $genderValue = $this->attributeHelper->getAttributeAdminLabel($product, self::GENDER_ATTRIBUTE);

        return $this->getCmsBlockHtml($this->getBraceletGuideCmsBlocks(), 'Women');
    }

    /**
     * @return string
     */
    public function getSizeSuggestionsHtml()
    {
        $product = $this->getProduct();

        if (!$this->attributeSetHelper->isShoes($product)) {
            return '';
        }

        $suggestionValue = $this->attributeHelper->getAttributeAdminLabel(
            $product,
            self::SIZE_ADVICE_ATTRIBUTE
        );

        return $this->getCmsBlockHtml($this->getSuggestionsCmsBlocks(), $suggestionValue);
    }

    /**
     * @param $cmsBlocks
     * @param $attributeValue
     * @return string
     */
    protected function getCmsBlockHtml($cmsBlocks, $attributeValue)
    {
        //Belt
        if ($this->attributeSetHelper->isBelts($this->getProduct())) {
            $attributeValue .= ' Belt';
        }
        //Bracelet
        if ($this->attributeSetHelper->isBracelets($this->getProduct())) {
            $attributeValue .= ' Bracelet';
        }

        $indexedCmsBlocks = array_combine(
            array_column($cmsBlocks, 'attribute_value'),
            array_column($cmsBlocks, 'cms_block_id')
        );

        if (!array_key_exists($attributeValue, $indexedCmsBlocks)) {
            return '';
        }

        return $this->cmsHelper->getCmsBlockHtml($indexedCmsBlocks[$attributeValue]);
    }
}
