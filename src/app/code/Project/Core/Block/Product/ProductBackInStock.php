<?php

namespace Project\Core\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Synolia\StandardDesign\Helper\Cms as CmsHelper;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;
use Project\BackInStock\Helper\BackInStockHelper as BackInStockHelper;

/**
 * Class ProductBackInStock
 * @package Project\Core\Block\Product
 *
 * @method string getCall()
 * @method setCall(string $methodName)
 * @method string getCssClass()
 * @method setCssClass(string $cssClass)
 */
class ProductBackInStock extends \Magento\Framework\View\Element\Template
{
    const CMS_BLOCK_ID = 'product-size-backinstock';

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var AttributeSetHelper
     */
    protected $attributeSetHelper;

    /**
     * @var CmsHelper
     */
    protected $cmsHelper;

    /**
     * @var BackInStockHelper
     */
    protected $backInStockHelper;

    /**
     * Tab constructor.
     * @param Context $context
     * @param Registry $registry
     * @param AttributeSetHelper $attributeSetHelper
     * @param CmsHelper $cmsHelper
     * @param array $data
     * @param BackInStockHelper $backInStockHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AttributeSetHelper $attributeSetHelper,
        CmsHelper $cmsHelper,
        BackInStockHelper $backInStockHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_coreRegistry = $registry;
        $this->attributeSetHelper = $attributeSetHelper;
        $this->cmsHelper          = $cmsHelper;
        $this->backInStockHelper   = $backInStockHelper;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    /**
     * @return bool|int
     */
    public function isBackInStock()
    {
        try {
            return $this->backInStockHelper->isBackInStock();
        } catch (\Throwable $throwable) {
            $this->_logger->error($throwable->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getBackInStockBlockHtml()
    {
        if ($this->attributeSetHelper->isShoes($this->getProduct())) {
            return $this->cmsHelper->getCmsBlockHtml(self::CMS_BLOCK_ID);
        } else {
            return '';
        }
    }
}
