<?php

namespace Project\Core\Block\Product;

use Magento\Catalog\Block\Product\View\Description;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Synolia\StandardDesign\Helper\Cms as CmsHelper;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;

/**
 * Class Tab
 * @package Project\Core\Block\Product
 * @author Synolia <contact@synolia.com>
 *
 * @method string getCall()
 * @method setCall(string $methodName)
 * @method string getCssClass()
 * @method setCssClass(string $cssClass)
 */
class Tab extends Description
{
    const CARE_LINK_CMS_BLOCK_PATTERN = 'product-care-link-%s';

    const SHIPPING_BEAUTY_CMS_BLOCK   = 'product-shipping-beauty';
    const SHIPPING_DEFAULT_CMS_BLOCK  = 'product-shipping';

    const RETURNS_BEAUTY_CMS_BLOCK    = 'product-returns-beauty';
    const RETURNS_DEFAULT_CMS_BLOCK   = 'product-returns';

    /**
     * @var AttributeSetHelper
     */
    protected $attributeSetHelper;

    /**
     * @var CmsHelper
     */
    protected $cmsHelper;

    /**
     * Tab constructor.
     * @param Context $context
     * @param Registry $registry
     * @param AttributeSetHelper $attributeSetHelper
     * @param CmsHelper $cmsHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AttributeSetHelper $attributeSetHelper,
        CmsHelper $cmsHelper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);

        $this->attributeSetHelper = $attributeSetHelper;
        $this->cmsHelper          = $cmsHelper;
    }

    /**
     * @return string
     */
    public function getCareLinkBlockHtml()
    {
        $product = $this->getProduct();

        $attributeSetName = $this->attributeSetHelper->getAttributeSetName($product);

        return $this->cmsHelper->getCmsBlockHtml(sprintf(
            self::CARE_LINK_CMS_BLOCK_PATTERN,
            strtolower(str_replace(' ', '-', $attributeSetName))
        ));
    }

    /**
     * @return string
     */
    public function getShippingBlockHtml()
    {
        $product = $this->getProduct();

        $isBeauty = $this->attributeSetHelper->isBeauty($product);

        if ($isBeauty) {
            return $this->cmsHelper->getCmsBlockHtml(self::SHIPPING_BEAUTY_CMS_BLOCK);
        } else {
            return $this->cmsHelper->getCmsBlockHtml(self::SHIPPING_DEFAULT_CMS_BLOCK);
        }
    }

    /**
     * @return string
     */
    public function getReturnsBlockHtml()
    {
        $product = $this->getProduct();

        $isBeauty = $this->attributeSetHelper->isBeauty($product);

        if ($isBeauty) {
            return $this->cmsHelper->getCmsBlockHtml(self::RETURNS_BEAUTY_CMS_BLOCK);
        } else {
            return $this->cmsHelper->getCmsBlockHtml(self::RETURNS_DEFAULT_CMS_BLOCK);
        }
    }
}
