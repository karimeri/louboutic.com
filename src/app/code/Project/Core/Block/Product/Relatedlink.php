<?php

namespace Project\Core\Block\Product;

use Magento\Catalog\Block\Product\View\Attributes as AttributesBase;
/**
 * Class Adjustment
 * @package Project\Core\Block\Product\Price
 * @author Synolia <contact@synolia.com>
 */
class Relatedlink extends AttributesBase
{
    const BEAUTY_TYPOLOGY = [
        'Beauty-Eyes',
        'Beauty-Lips',
        'Beauty-Nails',
        'Beauty-Perfumes'
    ];

    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSet;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    private $pageFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * Relatedlink constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Api\AttributeSetRepositoryInterface $attributeSet
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Api\AttributeSetRepositoryInterface $attributeSet,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ){
        $this->attributeSet = $attributeSet;
        $this->pageFactory = $pageFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context, $registry, $priceCurrency, $data);
    }

    /**
     * @return mixed
     */
    public function getAttributeSetName() {
        $product = $this->getProduct();
        $attributeSetRepository = $this->attributeSet->get($product->getAttributeSetId());
        return $attributeSetRepository->getAttributeSetName();
    }

    /**
     * @return bool
     */
    public function isBeautyEyesProduct() {
        if(strpos($this->getAttributeSetName(),self::BEAUTY_TYPOLOGY[0]) === false){
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getBeautyEyesHref()
    {
        return $this->getUrl().'discover-eyes.html';
    }

    /**
     * @return bool
     */
    public function isBeautyLipsProduct() {
        if(strpos($this->getAttributeSetName(),self::BEAUTY_TYPOLOGY[1]) === false){
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getBeautyLipsHref()
    {
        return $this->getUrl().'discover-lips.html';
    }

    /**
     * @return bool
     */
    public function isBeautyNailsProduct() {
        if(strpos($this->getAttributeSetName(),self::BEAUTY_TYPOLOGY[2]) === false){
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getBeautyNailsHref()
    {
        return $this->getUrl().'discover-nails.html';
    }

    /**
     * @return bool
     */
    public function isParfumeProduct() {
        if(strpos($this->getAttributeSetName(), self::BEAUTY_TYPOLOGY[3]) === false){
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getBeautyPerfumesHref()
    {
        return $this->getUrl().'discover-loubiworld';
    }

    /**
     * @param $identifier
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBeautyDiscoverPage($identifier){
        $page = $this->pageFactory->create()
                                  ->getCollection()
                                  ->addFieldToSelect('is_active')
                                  ->addStoreFilter($this->getStoreId(), false)
                                  ->addFieldToFilter('identifier', $identifier)
                                  ->getFirstItem();
        if ($page && $page->getData('is_active'))
            return true;
        return false;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreId(){
        return $this->storeManager->getStore()->getId();
    }
}
