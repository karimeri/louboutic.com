<?php

namespace Project\Core\Setup;

use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Module\Dir;
use Magento\Framework\File\Csv;

/**
 * Class TaxInstallData
 * @package   Project\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class TaxInstallData
{
    /**
     * @var Csv
     */
    protected $csvProcessor;

    /**
     * @var Reader
     */
    protected $moduleReader;

    /**
     * TaxInstallData constructor.
     * @param Csv    $csvProcessor
     * @param Reader $moduleReader
     */
    public function __construct(
        Csv $csvProcessor,
        Reader $moduleReader
    ) {
        $this->csvProcessor              = $csvProcessor;
        $this->moduleReader              = $moduleReader;
    }

    /**
     * @param array $rawData
     * @return array
     */
    protected function formatRawData(array $rawData)
    {
        foreach ($rawData as $rowIndex => $dataRow) {
            if ($rowIndex == 0) {
                $csvHeader[] = $dataRow;
            } else {
                $csvData = $dataRow;
                foreach ($csvData as $key => $data) {
                    $formattingData[$csvHeader[0][$key]] = $data;
                }
                $taxClassArray[] = $formattingData;
            }
        }

        return $taxClassArray;
    }

    /**
     * @param string $moduleName
     * @param string $filePath
     * @return array
     * @throws \Exception
     */
    protected function getDataTaxFile($moduleName, $filePath)
    {
        $file = $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, $moduleName).DIRECTORY_SEPARATOR.$filePath;
        return $this->csvProcessor->getData($file);
    }
}
