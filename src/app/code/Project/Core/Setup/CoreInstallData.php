<?php

namespace Project\Core\Setup;

use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Module\Dir;
use Magento\Framework\Module\Dir\Reader;
use Magento\Store\Model\StoreManager;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class CoreInstallData
 * @package   Project\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class CoreInstallData
{
    const PROJECT_ENV_FILE = 'projectEnv.php';

    /**
     * @var DirectoryList
     */
    protected $directoryList;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * CoreInstallData constructor.
     * @param DirectoryList   $directoryList
     * @param Csv             $csvProcessor
     * @param Reader          $moduleReader
     * @param StoreManager    $storeManager
     * @param WriterInterface $configWriter
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $configWriter
    ) {
        $this->directoryList = $directoryList;
        $this->csvProcessor  = $csvProcessor;
        $this->moduleReader  = $moduleReader;
        $this->storeManager  = $storeManager;
        $this->configWriter  = $configWriter;

        include_once $this->directoryList->getPath('etc').DIRECTORY_SEPARATOR.self::PROJECT_ENV_FILE;
    }

    /**
     * @param string $moduleName
     * @param string $filePath
     * @return array
     * @throws \Exception
     */
    public function getDataFile($moduleName, $filePath)
    {
        $file = $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, $moduleName).DIRECTORY_SEPARATOR.$filePath;
        return $this->csvProcessor->getData($file);
    }

    /**
     * @param array $rawData
     * @return array
     */
    public function formatRawData(array $rawData)
    {
        foreach ($rawData as $rowIndex => $dataRow) {
            if ($rowIndex == 0) {
                $csvHeader[] = $dataRow;
            } else {
                $csvData = $dataRow;
                foreach ($csvData as $key => $data) {
                    $formattingData[$csvHeader[0][$key]] = $data;
                }
                $dataArray[] = $formattingData;
            }
        }

        return $dataArray;
    }

    /**
     * @param array $data
     */
    public function saveConfig($data)
    {
        $formatingData = $this->formatRawData($data);

        foreach ($formatingData as $data) {
            if (isset($data['websiteCode'])) {
                $scopeId = $this->storeManager->getWebsite($data['websiteCode'])->getId();
            } elseif (isset($data['storeCode'])) {
                $scopeId = $this->storeManager->getWebsite($data['storeCode'])->getId();
            }
            $this->configWriter->save($data['path'], $data['value'], $data['scope'], $scopeId);
        }
    }
}
