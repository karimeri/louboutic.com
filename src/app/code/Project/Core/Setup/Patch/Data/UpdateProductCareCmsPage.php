<?php

declare(strict_types=1);

namespace Project\Core\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreManagerInterface;
use Synolia\Standard\Setup\CmsSetup;

class UpdateProductCareCmsPage implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var CmsSetup */
    private $cmsSetup;

    /** @var StoreManagerInterface */
    private $storeManager;

    /** @var array */
    private $cmsPageData = [
        'product-care-fr' => [
            'title' => 'Conseils d\'entretien - Christian Louboutin Boutique en ligne',
            'stores' => [
                'fr_fr',
                'ch_fr',
                'lu_fr',
                'be_fr',
                'mc_fr'
            ]
        ],
        'product-care-en' => [
            'title' => 'Product Care - Christian Louboutin Online Boutique',
            'stores' => [
                'uk_en',
                'it_en',
                'de_en',
                'es_en',
                'ch_en',
                'nl_en',
                'lu_en',
                'be_en',
                'at_en',
                'ie_en',
                'pt_en',
                'mc_en',
                'gr_en'
            ]
        ]
    ];

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CmsSetup $cmsSetup,
        StoreManagerInterface $storeManager
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->storeManager = $storeManager;
        $this->cmsSetup = $cmsSetup;
    }

    public function apply(): void
    {
        $connection = $this->moduleDataSetup->getConnection();
        $connection->startSetup();

        foreach ($this->cmsPageData as $templateName => $templateData) {
            $pageContent = $this->cmsSetup->getCmsPageContent($templateName);
            $storeIds = [];
            foreach ($templateData['stores'] as $storeCode) {
                $storeIds[] = $this->storeManager->getStore($storeCode)->getId();
            }
            $this->cmsSetup->savePage([
                'title' => $templateData['title'],
                'identifier' => 'product-care',
                'content' => $pageContent,
                'is_active' => true,
                'stores' => $storeIds
            ]);
        }

        $connection->endSetup();
    }

    public static function getDependencies(): array
    {
        // dependency patch classes if existing can be specified here
        return [];
    }

    public function getAliases(): array
    {
        // here we can specify an alias for this patch
        return [];
    }
}

