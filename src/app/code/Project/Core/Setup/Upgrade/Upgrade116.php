<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Project\Core\Setup\UpgradeData;
use Magento\Eav\Model\AttributeSetRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Upgrade116
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade116
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var AttributeSetRepository
     */
    protected $attributeSetRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Upgrade116 constructor.
     * @param CmsSetup $cmsSetup
     * @param AttributeSetRepository $attributeSetRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        CmsSetup $cmsSetup,
        AttributeSetRepository $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->cmsSetup               = $cmsSetup;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder  = $searchCriteriaBuilder;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $identifiers = [
            'product-returns',
            'product-returns-beauty',
            'product-shipping',
            'product-shipping-beauty'
        ];

        $cmsBlocks = [];
        foreach ($identifiers as $identifier) {
            $cmsBlocks[] = [
                'title'      => 'PRODUCT > '.ucfirst(str_replace('-', ' ', $identifier)),
                'identifier' => $identifier,
                'content'    => $this->cmsSetup->getCmsBlockContent($identifier),
                'is_active'  => 1,
                'stores'     => array(0),
                'store_id'   => array(0)
            ];
        }

        foreach ($this->getAllAttributeSetExceptBeauty() as $attributeSet) {
            $attributeSetName = $attributeSet->getAttributeSetName();
            $identifier = 'product-care-link-'.strtolower(str_replace(' ', '-', $attributeSetName));

            $cmsBlocks[] = [
                'title'      => 'PRODUCT > Product care link '.strtolower(str_replace('-', ' ', $attributeSetName)),
                'identifier' => $identifier,
                'content'    => $this->cmsSetup->getCmsBlockContent($identifier),
                'is_active'  => 1,
                'stores'     => array(0),
                'store_id'   => array(0)
            ];
        }

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
    }

    /**
     * @return \Magento\Eav\Api\Data\AttributeSetInterface[]
     */
    protected function getAllAttributeSetExceptBeauty()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_type_id', 4)
            ->addFilter('attribute_set_name', '%beauty%', 'nlike')
            ->create();

        $attributeSetResult = $this->attributeSetRepository->getList($searchCriteria);

        return $attributeSetResult->getItems();
    }
}
