<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade150
 * @package Project\Core\Setup\Upgrade
 */
class Upgrade150
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade150 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    public function run(UpgradeData $upgradeObject)
    {
        $this->configSetup->saveConfig('synolia_recaptcha/general/enabled', 1);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Enable recaptcha';
    }
}
