<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade114
 * @package Project\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade114
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade114 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(ConfigSetup $configSetup)
    {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->configSetup->saveConfig('synolia_storeswitcher/general/enable', 1);
        $this->configSetup->saveConfig('synolia_storeswitcher/general/service', 'geoip');
        $this->configSetup->saveConfig('synolia_storeswitcher/general/force_redirect', 2);
    }
}
