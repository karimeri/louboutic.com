<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade145
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade145
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade145 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blog/'.$locale;

            $headArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-cabaraparis-collection-head',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-cabaraparis-collection-title',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $contentArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-cabaraparis-collection-content',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $blogListArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-list-article-cabaraparis-collection',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'Article - There’s only one Cabaraparis - Head',
                    'identifier' => 'news-cabaraparis-collection-block-head',
                    'content' => $headArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - There’s only one Cabaraparis - Title',
                    'identifier' => 'news-cabaraparis-collection-block-title',
                    'content' => $titleArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - There’s only one Cabaraparis - Content',
                    'identifier' => 'news-cabaraparis-collection-block-content',
                    'content' => $contentArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Blog List - There’s only one Cabaraparis',
                    'identifier' => 'blog-list-news-cabaraparis-collection-block',
                    'content' => $blogListArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier,$stores) {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);

        if($cmsBlockCollection->count()>0){
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }

        return $identifier;
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $blockHeadId = $this->getCmsBlock("news-cabaraparis-collection-block-head",$stores);
            $blockTitleId = $this->getCmsBlock("news-cabaraparis-collection-block-title",$stores);
            $blockContentId = $this->getCmsBlock("news-cabaraparis-collection-block-content",$stores);

            $article =
'{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockHeadId . '"}}
<div class="article-content">
{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockTitleId . '"}}
<div class="text-content">
{{widget type="Magento\Cms\Block\Widget\Block"
    template="widget/static_block/default.phtml"
    block_id="' . $blockContentId . '"}}
 </div>
</div>';

            $articlePage = [
                'title' => "There’s only one Cabaraparis",
                'page_layout' => 'article',
                'identifier' => 'therea-s-only-one-cabaraparis',
                'content_heading' => '',
                'content' => $article,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($articlePage, false);
        }
    }
}
