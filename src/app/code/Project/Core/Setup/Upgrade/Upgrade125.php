<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade125
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade125
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade125 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(CmsSetup $cmsSetup)
    {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $cmsBlocks = [
            [
                'title'      => 'ACCOUNT - Newsletter - Top Info',
                'identifier' => 'newsletter-top-info',
                'content'    => $this->cmsSetup->getCmsBlockContent('newsletter-top-info'),
                'is_active'  => 1,
                'stores'     => [0],
                'store_id'   => [0]
            ],
            [
                'title'      => 'ACCOUNT - Newsletter - Bottom Info',
                'identifier' => 'newsletter-bottom-info',
                'content'    => $this->cmsSetup->getCmsBlockContent('newsletter-bottom-info'),
                'is_active'  => 1,
                'stores'     => [0],
                'store_id'   => [0]
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
    }
}
