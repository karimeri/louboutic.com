<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade120
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade120
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade120 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $cmsBlocks = [
            // Women
            [
                'title'      => 'Landing pages > Women',
                'identifier' => 'landing-content-women',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-content-women'),
                'store_id'   => [0],
                'is_active'  => 1
            ],
            [
                'title'      => 'Landing Pages > Women > Links',
                'identifier' => 'landing-links-women',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-links-women'),
                'store_id'   => [0],
                'is_active'  => 1
            ],

            // Men
            [
                'title'      => 'Landing pages > Men',
                'identifier' => 'landing-content-men',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-content-men'),
                'store_id'   => [0],
                'is_active'  => 1
            ],
            [
                'title'      => 'Landing Pages > Men > Links',
                'identifier' => 'landing-links-men',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-links-women'),
                'store_id'   => [0],
                'is_active'  => 1
            ],

            // Leather goods
            [
                'title'      => 'Landing pages > Leather goods',
                'identifier' => 'landing-content-leather-goods',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-content-leather-goods'),
                'store_id'   => [0],
                'is_active'  => 1
            ],
            [
                'title'      => 'Landing Pages > Leather Goods > Links',
                'identifier' => 'landing-links-leather-goods',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-links-women'),
                'store_id'   => [0],
                'is_active'  => 1
            ],

            // Beauty
            [
                'title'      => 'Landing pages > Beauty',
                'identifier' => 'landing-content-beauty',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-content-beauty'),
                'store_id'   => [0],
                'is_active'  => 1
            ],
            [
                'title'      => 'Landing Pages > Beauty > Links',
                'identifier' => 'landing-links-beauty',
                'content'    => $this->cmsSetup->getCmsBlockContent('landing-links-women'),
                'store_id'   => [0],
                'is_active'  => 1
            ],
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
    }
}
