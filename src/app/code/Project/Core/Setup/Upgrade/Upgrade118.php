<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\Eav\CategorySetup;

/**
 * Class Upgrade118
 * @package Project\Core\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade118
{
    /**
     * @var CategorySetup
     */
    protected $categorySetup;

    /**
     * Upgrade118 constructor.
     * @param CategorySetup $categorySetup
     */
    public function __construct(CategorySetup $categorySetup)
    {
        $this->categorySetup = $categorySetup;
    }

    public function run()
    {
        /**
         * Set "default" string as default value for category attribute menu_display_mode
         * So that new category created programaticaly are displayed in the left menu in frontend
         */
        $this->categorySetup->saveCategoryAttribute('menu_display_mode', [
            'default' => 'left-side'
        ]);
    }
}
