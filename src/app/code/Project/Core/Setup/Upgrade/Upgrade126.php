<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade126
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade126
{
    /**
     * @var CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade1026 constructor.
     * @param CustomerAddressSetup $customerAddressSetup
     * @param EavSetup $eavSetup
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup = $eavSetup;
    }

    public function run()
    {
        $attribute = [
            'type' => 'customer_address',
            'code' => 'contact_telephone',
            'entity_type_id' => 2,
            'data' => [
                'backend_type'      => 'static',
                'frontend_label'    => 'Contact Phone',
                'input'             => 'text',
                'required'          => false,
                'visible'           => true,
                'is_user_defined'   => true,
                'is_system'         => 0,
                'validate_rules'    => '{"max_text_length":"35","input_validation":"length"}',
                'sort_order'        => 125,
                'used_in_forms'     => ['customer_register_address', 'customer_address_edit']
            ]
        ];

        if (!$this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
            $this->eavSetup->addAttribute($attribute['entity_type_id'], $attribute['code'], array());
        }

        foreach ($attribute['data'] as $field => $value) {
            $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
        }
    }
}
