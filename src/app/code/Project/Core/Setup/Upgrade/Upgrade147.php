<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade147
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade147
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade147 constructor.
     * @param CmsSetup $cmsSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param $upgradeDataObject
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blog/'.$locale;

            $blogSubmenu = $this->cmsSetup->getCmsBlockContent(
                'blog-submenu',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'louboutinWorld - menu news',
                    'identifier' => 'menu-news',
                    'content' => $blogSubmenu,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }
}
