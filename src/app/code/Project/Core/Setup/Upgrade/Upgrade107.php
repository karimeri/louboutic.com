<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\CategorySetup;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade107
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade107
{
    /**
     * @var CategorySetup
     */
    protected $categorySetup;

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade107 constructor.
     * @param CategorySetup $categorySetup
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        CategorySetup $categorySetup,
        ConfigSetup $configSetup
    ) {
        $this->categorySetup = $categorySetup;
        $this->configSetup = $configSetup;
    }

    /**
     * @param $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        // Creation of category attributes left_menu_expands, segmentable

        $this->categorySetup->saveCategoryAttribute(
            'left_menu_expands',
            [
                'type'                    => 'int',
                'label'                   => 'Left menu expands',
                'input'                   => 'select',
                'source'                  => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required'                => false,
                'sort_order'              => 120,
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'used_in_product_listing' => false,
                'group'                   => 'Display Settings',
                'visible'                 => true
            ]
        );

        $this->categorySetup->saveCategoryAttribute(
            'segmentable',
            [
                'type'                    => 'int',
                'label'                   => 'Segmentable',
                'input'                   => 'select',
                'source'                  => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required'                => false,
                'sort_order'              => 130,
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'used_in_product_listing' => false,
                'group'                   => 'Display Settings',
                'visible'                 => true
            ]
        );
    }
}
