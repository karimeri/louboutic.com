<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade141
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade141
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade141 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        $templatePath = 'misc/cms/blocks/';
        $simpleImage = $this->cmsSetup->getCmsBlockContent(
            'discover-simple-image',
            'Project_Core',
            '',
            '',
            $templatePath
        );

        $cmsBlocks = [
            [
                'title' => 'Landing Example - Simple image',
                'identifier' => 'landing-example-simple-image',
                'content' => $simpleImage,
                'is_active' => 1,
                'stores'     => [0]
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);

        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $titleText = $this->cmsSetup->getCmsBlockContent(
                'discover-title-text',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-title-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'Landing example - Title image',
                    'identifier' => 'landing-example-title-image',
                    'content' => $titleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Landing example - Title text',
                    'identifier' => 'landing-example-title-text',
                    'content' => $titleText,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier,$stores) {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);

        if($cmsBlockCollection->count()>0){
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }

        return $identifier;
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $blockIds = [
                $this->getCmsBlock("landing-example-title-image",$stores),
                $this->getCmsBlock("landing-example-title-text",$stores),
                $this->getCmsBlock("landing-example-simple-image",$stores),
            ];

            $discover = "";
            foreach ($blockIds as $blockId) {
                $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block"
             template="widget/static_block/default.phtml"
             block_id="' . $blockId . '"}}
</div>';
            }
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'Landing Example Page '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'landing-example-page',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($discoverPage, false);
        }
    }
}
