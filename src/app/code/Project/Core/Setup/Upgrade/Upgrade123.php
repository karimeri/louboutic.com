<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\Eav\EavSetup;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade123
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade123
{

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade123 constructor.
     * @param EavSetup $eavSetup
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        EavSetup $eavSetup,
        CmsSetup $cmsSetup
    ) {
        $this->eavSetup = $eavSetup;
        $this->cmsSetup = $cmsSetup;
    }

    public function run()
    {
        $this->updatePrefixAttribute();
        $this->addCmsBlock();
    }

    public function updatePrefixAttribute()
    {
        $attribute = array(
            'type' => 'customer_address',
            'code' => 'prefix',
            'data' => [
                'frontend_label' => 'Prefix',
                'sort_order' => 10,
            ],
        );

        foreach ($attribute['data'] as $field => $value) {
            $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
        }
    }

    public function addCmsBlock()
    {
        $cmsBlockId = 'order-thanks';

        $cmsBlock = [
            'title'      => 'CHECKOUT SUCCESS - Order thanks',
            'identifier' => $cmsBlockId,
            'content'    => 'CMS Block order-thanks',
            'is_active'  => 1,
            'stores'     => [0],
            'store_id'   => [0]
        ];

        $this->cmsSetup->saveBlock($cmsBlock);
    }
}
