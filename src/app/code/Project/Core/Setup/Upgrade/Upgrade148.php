<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade138
 * @package Project\Core\Setup\Upgrade
 * @author Rime BENTIKOUK
 */
class Upgrade148
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade138 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $simpleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-simple-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-title-text',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-title-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $linkCategory = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-link-category',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $imageSingleProductRight = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-image-single-product-right',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $imageSingleProductLeft = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-image-single-product-left',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-title-subtitle-slider',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider2 = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-title-subtitle-slider2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $simpleImage2 = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-simple-image2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $simpleImage3 = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-simple-image3',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $simpleImage4 = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-simple-image4',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText2 = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-title-text2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText3 = $this->cmsSetup->getCmsBlockContent(
                'discover-matte-fluids-title-text3',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'DISCOVER Matte Fluids - Title image',
                    'identifier' => 'discover-matte-fluids-title-image',
                    'content' => $titleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Simple image',
                    'identifier' => 'discover-matte-fluids-simple-image',
                    'content' => $simpleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Simple image 2',
                    'identifier' => 'discover-matte-fluids-simple-image2',
                    'content' => $simpleImage2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Simple image 3',
                    'identifier' => 'discover-matte-fluids-simple-image3',
                    'content' => $simpleImage3,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Simple image 4',
                    'identifier' => 'discover-matte-fluids-simple-image4',
                    'content' => $simpleImage4,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Title text',
                    'identifier' => 'discover-matte-fluids-title-text',
                    'content' => $titleText,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Title text 2',
                    'identifier' => 'discover-matte-fluids-title-text2',
                    'content' => $titleText2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Title text 3',
                    'identifier' => 'discover-matte-fluids-title-text3',
                    'content' => $titleText3,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Link category',
                    'identifier' => 'discover-matte-fluids-link-category',
                    'content' => $linkCategory,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Image single product right',
                    'identifier' => 'discover-matte-fluids-image-single-product-right',
                    'content' => $imageSingleProductRight,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Image single product left',
                    'identifier' => 'discover-matte-fluids-image-single-product-left',
                    'content' => $imageSingleProductLeft,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Title Subtitle Slider',
                    'identifier' => 'discover-matte-fluids-title-subtitle-slider',
                    'content' => $titleSubtitleSlider,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Matte Fluids - Title Subtitle Slider 2',
                    'identifier' => 'discover-matte-fluids-title-subtitle-slider2',
                    'content' => $titleSubtitleSlider2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'discover-matte-fluids-products-slider',
                'title'          => 'DISCOVER Matte Fluids - Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-matte-fluids-products-slider',
                        'title'        => 'DISCOVER Matte Fluids - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ],
            [
                'identifier'     => 'discover-matte-fluids-products-slider2',
                'title'          => 'DISCOVER Matte Fluids - Products Slider 2',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-matte-fluids-products-slider2',
                        'title'        => 'DISCOVER Matte Fluids - Product slider 2',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "discover-matte-fluids-title-image",
            "discover-matte-fluids-title-text",
            "discover-matte-fluids-image-single-product-right",
            "discover-matte-fluids-image-single-product-left",
            "discover-matte-fluids-title-subtitle-slider",
            "discover-matte-fluids-title-subtitle-slider2",
            "discover-matte-fluids-simple-image",
            "discover-matte-fluids-title-text2",
            "discover-matte-fluids-simple-image2",
            "discover-matte-fluids-simple-image3",
            "discover-matte-fluids-title-text3",
            "discover-matte-fluids-simple-image4",
            "discover-matte-fluids-link-category"
        ];

        $discover = "";
        foreach ($blockIds as $blockId) {
            $discover .= '
                <div>
                    {{widget type="Magento\Cms\Block\Widget\Block"
                             template="widget/static_block/default.phtml"
                             block_id="'. $blockId .'"}}
                </div>';
        }
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'Discover Matte Fluids Page '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'discover-matte-fluids',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($discoverPage, false);
        }
    }
}
