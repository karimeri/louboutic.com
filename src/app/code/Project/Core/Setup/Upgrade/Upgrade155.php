<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade155
 * @package Project\Core\Setup\Upgrade
 * @author Oussama Chebbi
 */
class Upgrade155
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;


    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade146 constructor.
     * @param CmsSetup $cmsSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $returnArticle = $this->cmsSetup->getCmsBlockContent(
                'rma-order-search',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'rma-order-search',
                    'identifier' => 'rma-order-search',
                    'content' => $returnArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

}

