<?php

namespace Project\Core\Setup\Upgrade;

use Magento\Eav\Model\Attribute;
use Project\Core\Setup\UpgradeData;

/**
 * Class Upgrade136
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade137
{
    const ENTITY_TYPE = 'catalog_product';
    const ATTRIBUTE_CODE = 'size_belts';
    const ATTRIBUTE_LABEL_FIELD_NAME = 'frontend_label';
    const ATTRIBUTE_LABEL_VALUE = 'Size Belts';

    /**
     * @var Attribute
     */
    protected $_attributeModel;

    /**
     * Upgrade136 constructor.
     * @param Attribute $attributeModel
     */
    public function __construct(
        Attribute $attributeModel
    ) {
        $this->_attributeModel = $attributeModel;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->_attributeModel->loadByCode(
            self::ENTITY_TYPE,
            self::ATTRIBUTE_CODE
        );
        $this->_attributeModel->setData(
            self::ATTRIBUTE_LABEL_FIELD_NAME,
            self::ATTRIBUTE_LABEL_VALUE
        );
        $this->_attributeModel->save();
    }
}
