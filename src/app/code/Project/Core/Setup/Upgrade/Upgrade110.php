<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade110
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade110
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade110 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {

        $simpleImage                = $this->cmsSetup->getCmsBlockContent('discover-simple-image');
        $titleText                  = $this->cmsSetup->getCmsBlockContent('discover-title-text');
        $titleImage                 = $this->cmsSetup->getCmsBlockContent('discover-title-image');
        $imageSingleProductLeft     = $this->cmsSetup->getCmsBlockContent('discover-image-single-product-left');
        $linkCategory               = $this->cmsSetup->getCmsBlockContent('discover-link-category');
        $imageSingleProductRight    = $this->cmsSetup->getCmsBlockContent('discover-image-single-product-right');
        $titleSubtitleSlider        = $this->cmsSetup->getCmsBlockContent('discover-title-subtitle-slider');

        $cmsBlocks = [
            [
                'title' => 'DISCOVER - Title image',
                'identifier' => 'discover-title-image',
                'content' => $titleImage,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'DISCOVER - Simple image',
                'identifier' => 'discover-simple-image',
                'content' => $simpleImage,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'DISCOVER - Title text',
                'identifier' => 'discover-title-text',
                'content' => $titleText,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'DISCOVER - Image single product left',
                'identifier' => 'discover-image-single-product-left',
                'content' => $imageSingleProductLeft,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'DISCOVER - Link category',
                'identifier' => 'discover-link-category',
                'content' => $linkCategory,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'DISCOVER - Image single product right',
                'identifier' => 'discover-image-single-product-right',
                'content' => $imageSingleProductRight,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'DISCOVER - Title Subtitle Slider',
                'identifier' => 'discover-title-subtitle-slider',
                'content' => $titleSubtitleSlider,
                'is_active' => 1,
                'stores' => array(0),
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'discover-products-slider',
                'title'          => 'DISCOVER - Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-products-slider',
                        'title'        => 'DISCOVER - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => [],
                    ],
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "discover-title-image",
            "discover-simple-image",
            "discover-title-text",
            "discover-image-single-product-left",
            "discover-link-category",
            "discover-image-single-product-right",
            "discover-title-subtitle-slider"
        ];

        $discover = "";

        foreach ($blockIds as $blockId) {
            $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block" 
             template="widget/static_block/default.phtml" 
             block_id="'. $blockId .'"}}
</div>';
        }

        $discoverPage = [
            'title'             => 'Discover Page',
            'page_layout'       => 'nofullscreen',
            'identifier'        => 'discover',
            'content_heading'   => '',
            'content'           => $discover,
            'is_active'         => 1,
            'stores'            => array(0),
        ];

        $this->cmsSetup->savePage($discoverPage, false);
    }
}
