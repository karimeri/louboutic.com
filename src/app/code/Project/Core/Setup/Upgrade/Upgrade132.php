<?php

namespace Project\Core\Setup\Upgrade;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade132
 * @package Project\Core\Setup\Upgrade
 */
class Upgrade132
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade124 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(ConfigSetup $configSetup)
    {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $installer = $upgradeDataObject->getSetup();
        $table     = $installer->getTable('sales_order_status');
        if ($installer->getConnection()->isTableExists($table) == true) {
            $this->initNewStatus($installer);
            $this->initNewStatusState($installer);
        }
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    private function initNewStatus(ModuleDataSetupInterface $installer)
    {
        $newStatus = [
            [
                'status' => 'documents_ready_to_be_printed',
                'label' => 'Documents ready to be printed'
            ]
        ];
        $installer->getConnection()->insertMultiple(
            $installer->getTable('sales_order_status'),
            $newStatus
        );
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    private function initNewStatusState(ModuleDataSetupInterface $installer)
    {
        $newStatusState = [
            [
                'status' => 'documents_ready_to_be_printed',
                'state' => 'documents_ready_to_be_printed',
                'is_default' => 0,
                'visible_on_front' => 1
            ]
        ];
        $installer->getConnection()->insertMultiple(
            $installer->getTable('sales_order_status_state'),
            $newStatusState
        );
    }
}
