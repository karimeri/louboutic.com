<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ThemeSetup;

/**
 * Class Update101
 * @package   Project\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade101
{
    /**
     * @var ThemeSetup
     */
    protected $themeSetup;

    /**
     * Upgrade101 constructor.
     * @param ThemeSetup $themeSetup
     */
    public function __construct(
        ThemeSetup $themeSetup
    ) {
        $this->themeSetup = $themeSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->themeSetup->installThemeInDesignConfigGrid("frontend/Synolia/louboutin");
    }
}
