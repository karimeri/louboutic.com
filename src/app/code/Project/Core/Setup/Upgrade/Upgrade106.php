<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade106
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade106
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade106 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {

        $topboxVideo         = $this->cmsSetup->getCmsBlockContent('home-topbox-video');
        $topmosaicVideo      = $this->cmsSetup->getCmsBlockContent('home-topmosaic-video');
        $topboxImage         = $this->cmsSetup->getCmsBlockContent('home-topbox-image');
        $topmosaicImage      = $this->cmsSetup->getCmsBlockContent('home-topmosaic-image');
        $topboxSlider        = $this->cmsSetup->getCmsBlockContent('home-topbox-slider');
        $topmosaicSlider     = $this->cmsSetup->getCmsBlockContent('home-topmosaic-slider');
        $imagesProductsRight = $this->cmsSetup->getCmsBlockContent('home-images-products-right');
        $imagesProductsLeft  = $this->cmsSetup->getCmsBlockContent('home-images-products-left');
        $news                = $this->cmsSetup->getCmsBlockContent('home-news');

        $cmsBlocks = [
            [
                'title' => 'HOME - Topbox video',
                'identifier' => 'home-topbox-video',
                'content' => $topboxVideo,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Topmosaic video',
                'identifier' => 'home-topmosaic-video',
                'content' => $topmosaicVideo,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Topbox image',
                'identifier' => 'home-topbox-image',
                'content' => $topboxImage,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Topmosaic image',
                'identifier' => 'home-topmosaic-image',
                'content' => $topmosaicImage,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Topbox slider',
                'identifier' => 'home-topbox-slider',
                'content' => $topboxSlider,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Topmosaic slider',
                'identifier' => 'home-topmosaic-slider',
                'content' => $topmosaicSlider,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Images products right',
                'identifier' => 'home-images-products-right',
                'content' => $imagesProductsRight,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Images products left',
                'identifier' => 'home-images-products-left',
                'content' => $imagesProductsLeft,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - News',
                'identifier' => 'home-news',
                'content' => $news,
                'is_active' => 1,
                'stores' => array(0),
            ],
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'home-topbox-slider',
                'title'          => 'HOME - Topbox Slider',
                'autoplay'       => 1,
                'autoplay_speed' => 4000,
                'infinite'       => 1,
                'slides'         => [
                    [
                        'identifier'   => 'home-test-slider1',
                        'title'        => 'HOME - Test Slider 1',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '480x500-black.png',
                        'image_medium' => '768x600-black.png',
                        'image_big'    => '1200x800-black.png',
                        'content'      => ''
                    ],
                    [
                        'identifier'   => 'home-test-slider2',
                        'title'        => 'HOME - Test Slider 2',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '480x500-grey.png',
                        'image_medium' => '768x600-grey.png',
                        'image_big'    => '1200x800-grey.png',
                        'content'      => ''
                    ],
                    [
                        'identifier'   => 'home-test-slider3',
                        'title'        => 'HOME - Test Slider 3',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '480x500-blue.png',
                        'image_medium' => '768x600-blue.png',
                        'image_big'    => '1200x800-blue.png',
                        'content'      => ''
                    ],
                ]
            ],
            [
                'identifier'     => 'home-topmosaic-slider',
                'title'          => 'HOME - Topmosaic Slider',
                'autoplay'       => 1,
                'autoplay_speed' => 4000,
                'infinite'       => 1,
                'slides'         => [
                    'home-test-slider1',
                    'home-test-slider2',
                    'home-test-slider3'
                ]
            ],
            [
                'identifier'     => 'home-products-slider',
                'title'          => 'HOME - Products slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'home-produtcs-slider',
                        'title'        => 'HOME - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => [],
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "home-topbox-video",
            "home-topmosaic-video",
            "home-topbox-image",
            "home-topmosaic-image",
            "home-topbox-slider",
            "home-topmosaic-slider",
            "home-images-products-right",
            "home-images-products-left",
            "home-news"
        ];

        $home = "";

        foreach ($blockIds as $blockId) {
            $home .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block" 
             template="widget/static_block/default.phtml" 
             block_id="'. $blockId .'"}}
</div>';
        }

        $homePage = [
            'title'             => 'Home page',
            'page_layout'       => 'nofullscreen',
            'identifier'        => 'home',
            'content_heading'   => '',
            'content'           => $home,
            'is_active'         => 1,
            'stores'            => array(0),
        ];

        $this->cmsSetup->savePage($homePage, false);
    }
}
