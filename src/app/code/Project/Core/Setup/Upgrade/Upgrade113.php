<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade113
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade113
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade113 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    public function run()
    {
        $this->addCmsBlocks();
        $this->addCmsPage();
    }

    public function addCmsBlocks()
    {

        $article                = $this->cmsSetup->getCmsBlockContent('blog-blog-article');
        $submenuArticle         = $this->cmsSetup->getCmsBlockContent('blog-submenu-blog-article');
        $louboutinWorldContent  = $this->cmsSetup->getCmsBlockContent('louboutin-world-top-content');

        $louboutinWorldMenu     = $this->cmsSetup->getCmsBlockContent('louboutin-world-menu');



        $cmsBlocks = [
            [
                'title'         => 'Blog - article',
                'identifier'    => 'blog-blog-article-1',
                'content'       => $article,
                'is_active'     => 1,
                'stores'        => [0],
                'store_id'      => [0],
            ],
            [
                'title'         => 'Blog - article',
                'identifier'    => 'blog-blog-article-2',
                'content'       => $article,
                'is_active'     => 1,
                'stores'        => [0],
                'store_id'      => [0],
            ],
            [
                'title'         => 'Blog - header',
                'identifier'    => 'blog-header',
                'content'       => $louboutinWorldContent,
                'is_active'     => 1,
                'stores'        => [0],
                'store_id'      => [0],
            ],
            [
                'title'         => 'louboutinWorld - menu news',
                'identifier'    => 'menu-news',
                'content'       => $louboutinWorldMenu,
                'is_active'     => 1,
                'stores'        => [0],
                'store_id'      => [0],
            ],
            [
                'title'         => 'Blog - submenu - article',
                'identifier'    => 'blog-submenu-article',
                'content'       =>  $submenuArticle,
                'is_active'     => 1,
                'stores'        => [0],
                'store_id'      => [0],
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
    }

    public function addCmsPage()
    {
        $blockIds = [
            "blog-blog-article-1",
            "blog-blog-article-2",
        ];

        $blogContent = '';

        foreach ($blockIds as $blockId) {
            $blogContent .= sprintf($this->cmsSetup->getCmsBlockContent('single-blog-article-loop'), $blockId);
        }

        $blog = sprintf($this->cmsSetup->getCmsPageContent('page-news-content'), $blogContent);

        $blogPage = [
            [
                'title'             => 'News',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'news',
                'content_heading'   => '',
                'content'           => $blog,
                'is_active'         => 1,
                'stores'            => [0],
                'store_id'          => [0],
            ],
            [
                'title'             => 'Articles',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'articles',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
                'store_id'          => [0],
            ]
        ];

        $this->cmsSetup->saveMultiplePages($blogPage, false);
    }
}
