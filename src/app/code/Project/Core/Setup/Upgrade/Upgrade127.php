<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade127
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade127
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade127 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $topboxVideo    = $this->cmsSetup->getCmsBlockContent('home-topbox-video');
        $topmosaicVideo = $this->cmsSetup->getCmsBlockContent('home-topmosaic-video');

        $cmsBlocks = [
            [
                'title' => 'HOME - Topbox video',
                'identifier' => 'home-topbox-video',
                'content' => $topboxVideo,
                'is_active' => 1,
                'stores' => array(0),
            ],
            [
                'title' => 'HOME - Topmosaic video',
                'identifier' => 'home-topmosaic-video',
                'content' => $topmosaicVideo,
                'is_active' => 1,
                'stores' => array(0),
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, true);
    }
}
