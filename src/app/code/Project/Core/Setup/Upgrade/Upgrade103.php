<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade103
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade103
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade103 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        // Helper block
        $helperContent = <<<CONTENT
<ul>
<li><a href="{{store url=''}}faq">FAQ</a></li>
<li><a href="{{store url=''}}product-care">Conseils d'entretien</a></li>
<li><a href="{{store url=''}}rma/return/search">Créer un Retour</a></li>
<li><a href="{{store url=''}}stopfake">Stopfake</a></li>
</ul>
<ul>
<li><a href="{{store url=''}}terms-sale#transport-and-delivery">Livraisons</a></li>
<li><a href="{{store url=''}}terms-sale#cancellations">Retours</a></li>
<li><a href="{{store url=''}}policy">Confidentialité</a></li>
</ul>
CONTENT;

        // Footer desktop block
        $footerDesktopContent = <<<CONTENT
<div class="links-inline">
<ul>
<li><a href="{{store url=''}}contacts">Nous contacter</a></li>
<li><a href="{{store url=''}}stopfake" target="_blank">Stopfake</a></li>
<li><a href="{{store url=''}}product-care">Conseils d'entretien</a></li>
<li><a href="{{store url=''}}rma/return/search/">Retour</a></li>
<li><a href="{{store url=''}}terms-sale">CGV</a></li>
<li><a href="{{store url=''}}terms-use">CGU</a></li>
<li><a href="{{store url=''}}policy">Confidentialité</a></li>
</ul>
</div>
<div class="find-store-container">
<p>Trouver une boutique</p>
<p>
    <a href="{{store url=''}}storelocator/all-stores">
        <img src="{{media url='cms/block/default/footer/find-store01.jpg'}}" />
    </a>
</p>
</div>
CONTENT;

        // Footer mobile block
        $footerMobileContent = <<<CONTENT
<ul class="contact">
    <li>
        <a class="tel btn" href="tel:0800 945 804 - OPTION 2">
            <span class="icon icon-i8"></span>
            <span class="text">0800 945 804 - OPTION 2</span>
        </a>
    </li>
    <li>
        <a class="mail btn" href="mailto:eboutique.europe@christianlouboutin.fr">
            <span class="icon icon-i13"></span>
            <span class="text">Nous contacter</span>
        </a>
    </li>
</ul>
<ul class="footer-links-mobile">
<li><a href="{{store url=''}}stopfake" target="_blank">Stopfake</a></li>
<li><a href="{{store url=''}}product-care">Conseils d'entretien</a></li>
<li><a href="{{store url=''}}rma/return/search/">Retour</a></li>
<li><a href="{{store url=''}}terms-sale">CGV</a></li>
<li><a href="{{store url=''}}terms-use">CGU</a></li>
<li><a href="{{store url=''}}policy">Confidentialité</a></li>
</ul>
CONTENT;

        $cmsBlocks = [
            [
                'identifier' => 'menu-help',
                'title'      => 'menu-help',
                'is_active'  => 1,
                'content'    => $helperContent,
                'stores'     => 0,
            ],
            [
                'identifier' => 'footer-desktop-ecom',
                'title'      => 'footer-desktop-ecom',
                'is_active'  => 1,
                'content'    => $footerDesktopContent,
                'stores'     => 0,
            ],
            [
                'identifier' => 'footer-mobile-ecom',
                'title'      => 'footer-mobile-ecom',
                'is_active'  => 1,
                'content'    => $footerMobileContent,
                'stores'     => 0,
            ],
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
    }
}
