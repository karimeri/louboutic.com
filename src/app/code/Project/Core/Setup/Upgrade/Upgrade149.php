<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Project\Core\Setup\UpgradeData;
use Magento\Eav\Model\AttributeSetRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Upgrade149
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade149
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var AttributeSetRepository
     */
    protected $attributeSetRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Upgrade149 constructor.
     * @param CmsSetup $cmsSetup
     * @param AttributeSetRepository $attributeSetRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        CmsSetup $cmsSetup,
        AttributeSetRepository $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->cmsSetup               = $cmsSetup;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder  = $searchCriteriaBuilder;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function run(UpgradeData $upgradeDataObject)
    {

        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsBlocks = [];
            $templatePath = 'misc/cms/blocks/'.$locale;
            foreach ($this->getAllAttributeSetExceptBeauty() as $attributeSet) {
                $attributeSetName = $attributeSet->getAttributeSetName();
                $identifier = 'product-care-link-' . strtolower(str_replace(' ', '-', $attributeSetName));

                $cmsBlocks[] = [
                    'title' => 'PRODUCT > Product care link ' . strtolower(str_replace('-', ' ', $attributeSetName)),
                    'identifier' => $identifier,
                    'content' => $this->cmsSetup->getCmsBlockContent(
                        $identifier,
                        'Project_Core',
                        '',
                        '',
                        $templatePath
                    ),
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ];
            }
            $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
        }

    }

    /**
     * @return \Magento\Eav\Api\Data\AttributeSetInterface[]
     */
    protected function getAllAttributeSetExceptBeauty()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_type_id', 4)
            ->addFilter('attribute_set_name', '%beauty%', 'nlike')
            ->create();

        $attributeSetResult = $this->attributeSetRepository->getList($searchCriteria);

        return $attributeSetResult->getItems();
    }
}

