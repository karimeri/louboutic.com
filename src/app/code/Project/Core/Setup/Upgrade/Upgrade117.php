<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade117
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade117
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade117 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $cmsBlock = [
            'title'      => 'PRODUCT > social networks share links',
            'identifier' => 'product-share',
            'content'    => $this->cmsSetup->getCmsBlockContent('product-share'),
            'is_active'  => 1,
            'stores'     => array(0),
            'store_id'   => array(0)
        ];

        $this->cmsSetup->saveBlock($cmsBlock);
    }
}
