<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class Upgrade102
 * @package   Project\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade102
{
    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $upgradeDataObject->getSetup()->getConnection()->addColumn(
            $upgradeDataObject->getSetup()->getTable('tax_class'),
            'code',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => null,
                'comment' => 'Tax class code'
            ]
        );
        $upgradeDataObject->getConfigSetup()->saveConfig('dev/static/sign', 0);
    }
}
