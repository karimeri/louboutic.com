<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade138
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade138
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade138 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $simpleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-simple-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-title-text',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-title-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $linkCategory = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-link-category',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $imageSingleProductRight = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-image-single-product-right',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-title-subtitle-slider',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $simpleImage2 = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-simple-image2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $simpleImage3 = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-simple-image3',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $simpleImage4 = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-simple-image4',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText2 = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-title-text2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText3 = $this->cmsSetup->getCmsBlockContent(
                'discover-elisa-title-text3',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'DISCOVER Elisa - Title image',
                    'identifier' => 'discover-elisa-title-image',
                    'content' => $titleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Simple image',
                    'identifier' => 'discover-elisa-simple-image',
                    'content' => $simpleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Simple image 2',
                    'identifier' => 'discover-elisa-simple-image2',
                    'content' => $simpleImage2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Simple image 3',
                    'identifier' => 'discover-elisa-simple-image3',
                    'content' => $simpleImage3,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Simple image 4',
                    'identifier' => 'discover-elisa-simple-image4',
                    'content' => $simpleImage4,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Title text',
                    'identifier' => 'discover-elisa-title-text',
                    'content' => $titleText,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Title text 2',
                    'identifier' => 'discover-elisa-title-text2',
                    'content' => $titleText2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Title text 3',
                    'identifier' => 'discover-elisa-title-text3',
                    'content' => $titleText3,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Link category',
                    'identifier' => 'discover-elisa-link-category',
                    'content' => $linkCategory,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Image single product right',
                    'identifier' => 'discover-elisa-image-single-product-right',
                    'content' => $imageSingleProductRight,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Elisa - Title Subtitle Slider',
                    'identifier' => 'discover-elisa-title-subtitle-slider',
                    'content' => $titleSubtitleSlider,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'discover-elisa-products-slider',
                'title'          => 'DISCOVER Elisa - Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-elisa-products-slider',
                        'title'        => 'DISCOVER Elisa - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "discover-elisa-title-image",
            "discover-elisa-title-text",
            "discover-elisa-image-single-product-right",
            "discover-elisa-title-subtitle-slider",
            "discover-elisa-simple-image",
            "discover-elisa-title-text2",
            "discover-elisa-simple-image2",
            "discover-elisa-simple-image3",
            "discover-elisa-title-text3",
            "discover-elisa-simple-image4",
            "discover-elisa-link-category"
        ];

        $discover = "";
        foreach ($blockIds as $blockId) {
            $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block"
             template="widget/static_block/default.phtml"
             block_id="'. $blockId .'"}}
</div>';
        }
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'Discover Elisa Page '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'discover-elisa',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($discoverPage, false);
        }
    }
}
