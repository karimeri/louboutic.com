<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;
use Magento\Eav\Model\AttributeRepository;

/**
 * Class Upgrade128
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade128
{
    /**
     * @var CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Upgrade1026 constructor.
     *
     * @param CustomerAddressSetup $customerAddressSetup
     * @param EavSetup $eavSetup
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup,
        AttributeRepository $attributeRepository,
        EnvironmentManager $environmentManager
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup = $eavSetup;
        $this->attributeRepository = $attributeRepository;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function run()
    {
        $attribute = [
            'type' => 'customer_address',
            'code' => 'contact_telephone',
            'entity_type_id' => 2,
            'data' => [
                'backend_type' => 'static',
                'frontend_label' => 'Contact Phone',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'is_user_defined' => true,
                'is_system' => 0,
                'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                'sort_order' => 125,
                'used_in_forms' => ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address'],
            ],
        ];

        if ($this->environmentManager->getEnvironment() === Environment::EU) {
            if (!$this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->addAttribute($attribute['entity_type_id'], $attribute['code'], []);
            }

            $eavAttribute = $this->attributeRepository->get($attribute['type'], $attribute['code']);

            foreach ($attribute['data'] as $field => $value) {
                $eavAttribute->setData($field, $value);
            }

            $eavAttribute->save();
        } else {
            if ($this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->removeAttribute($attribute['type'], $attribute['code']);
            }
        }
    }
}
