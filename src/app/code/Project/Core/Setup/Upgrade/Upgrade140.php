<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade140
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade140
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade140 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        $templatePath = 'misc/cms/blocks/';
        $simpleImage = $this->cmsSetup->getCmsBlockContent(
            'discover-nudes-simple-image',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $imageSingleProductRight = $this->cmsSetup->getCmsBlockContent(
            'discover-nudes-image-single-product-right',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $imageSingleProductLeft = $this->cmsSetup->getCmsBlockContent(
            'discover-nudes-image-single-product-left',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $simpleImage2 = $this->cmsSetup->getCmsBlockContent(
            'discover-nudes-simple-image2',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $simpleImage3 = $this->cmsSetup->getCmsBlockContent(
            'discover-nudes-simple-image3',
            'Project_Core',
            '',
            '',
            $templatePath
        );

        $cmsBlocks = [
            [
                'title' => 'DISCOVER Nudes - Simple image',
                'identifier' => 'discover-nudes-simple-image',
                'content' => $simpleImage,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Nudes - Simple image 2',
                'identifier' => 'discover-nudes-simple-image2',
                'content' => $simpleImage2,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Nudes - Simple image 3',
                'identifier' => 'discover-nudes-simple-image3',
                'content' => $simpleImage3,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Nudes - Image single product right',
                'identifier' => 'discover-nudes-image-single-product-right',
                'content' => $imageSingleProductRight,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Nudes - Image single product left',
                'identifier' => 'discover-nudes-image-single-product-left',
                'content' => $imageSingleProductLeft,
                'is_active' => 1,
                'stores'     => [0]
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);

        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $titleText = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-title-text',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-title-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $linkCategory = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-link-category',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-title-subtitle-slider',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider2 = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-title-subtitle-slider2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText2 = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-title-text2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText3 = $this->cmsSetup->getCmsBlockContent(
                'discover-nudes-title-text3',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'DISCOVER Nudes - Title image',
                    'identifier' => 'discover-nudes-title-image',
                    'content' => $titleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Nudes - Title text',
                    'identifier' => 'discover-nudes-title-text',
                    'content' => $titleText,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Nudes - Title text 2',
                    'identifier' => 'discover-nudes-title-text2',
                    'content' => $titleText2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Nudes - Title text 3',
                    'identifier' => 'discover-nudes-title-text3',
                    'content' => $titleText3,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Nudes - Link category',
                    'identifier' => 'discover-nudes-link-category',
                    'content' => $linkCategory,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Nudes - Title Subtitle Slider',
                    'identifier' => 'discover-nudes-title-subtitle-slider',
                    'content' => $titleSubtitleSlider,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Nudes - Title Subtitle Slider 2',
                    'identifier' => 'discover-nudes-title-subtitle-slider2',
                    'content' => $titleSubtitleSlider2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'discover-nudes-products-slider',
                'title'          => 'DISCOVER Nudes - Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-nudes-products-slider',
                        'title'        => 'DISCOVER Nudes - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => [],
                    ],
                ]
            ],
            [
                'identifier'     => 'discover-nudes-products-slider2',
                'title'          => 'DISCOVER Nudes - Products Slider 2',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-nudes-products-slider2',
                        'title'        => 'DISCOVER Nudes - Product slider 2',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => [],
                    ],
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "discover-nudes-title-image",
            "discover-nudes-title-text",
            "discover-nudes-image-single-product-right",
            "discover-nudes-title-subtitle-slider",
            "discover-nudes-simple-image",
            "discover-nudes-title-text2",
            "discover-nudes-image-single-product-left",
            "discover-nudes-title-subtitle-slider2",
            "discover-nudes-simple-image2",
            "discover-nudes-title-text3",
            "discover-nudes-simple-image3",
            "discover-nudes-link-category"
        ];

        $discover = "";
        foreach ($blockIds as $blockId) {
            $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block"
             template="widget/static_block/default.phtml"
             block_id="'. $blockId .'"}}
</div>';
        }
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'DISCOVER Nudes Page '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'discover-nudes',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($discoverPage, false);
        }
    }
}
