<?php
namespace Project\Core\Setup\Upgrade;
use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade157
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade157
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade157 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $templatePath = 'misc/cms/blocks/'.$locale;

            $titleSubtitleSlider2 = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-subtitle-slider2',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'DISCOVER Loubishark - Title Subtitle Slider 2',
                    'identifier' => 'discover-loubishark-title-subtitle-slider2',
                    'content' => $titleSubtitleSlider2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'discover-loubishark-products-slider1',
                'title'          => 'DISCOVER Loubishark - Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-loubishark-products-slider',
                        'title'        => 'DISCOVER Loubishark - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ],
            [
                'identifier'     => 'discover-loubishark-products-slider2',
                'title'          => 'DISCOVER Loubishark - Products Slider 2',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-loubishark-products-slider2',
                        'title'        => 'DISCOVER Loubishark - Product slider 2',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ]
        ];
        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }
    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $blockIds = [
                $this->getCmsBlock("discover-loubishark-title-image",$stores),
                $this->getCmsBlock("discover-loubishark-title-text",$stores),
                "discover-loubishark-image-single-product-right",
                $this->getCmsBlock("discover-loubishark-title-subtitle-slider",$stores),
                "discover-loubishark-simple-image",
                $this->getCmsBlock("discover-loubishark-title-text2",$stores),
                "discover-loubishark-image-single-product-left",
                $this->getCmsBlock("discover-loubishark-title-subtitle-slider2",$stores),
                "discover-loubishark-simple-image2",
                $this->getCmsBlock("discover-loubishark-title-text3",$stores),
                "discover-loubishark-simple-image3",
                $this->getCmsBlock("discover-loubishark-link-category",$stores)
            ];
            $discover = "";
            foreach ($blockIds as $blockId) {
                $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block"
             template="widget/static_block/default.phtml"
             block_id="' . $blockId . '"}}
</div>';
            }
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'Discover Loubishark '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'discover-loubishark',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];
            $this->cmsSetup->savePage($discoverPage, false);
        }
    }
    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier,$stores) {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);
        if($cmsBlockCollection->count()>0){
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }
        return $identifier;
    }
}
