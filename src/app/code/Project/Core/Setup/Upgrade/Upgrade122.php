<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade122
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade122
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade122 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(CmsSetup $cmsSetup)
    {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $cmsBlockId = 'contact-infos';

        $cmsBlock = [
            'title'      => 'Contact > Infos',
            'identifier' => $cmsBlockId,
            'content'    => $this->cmsSetup->getCmsBlockContent($cmsBlockId),
            'is_active'  => 1,
            'stores'     => [0],
            'store_id'   => [0]
        ];

        $this->cmsSetup->saveBlock($cmsBlock);
    }
}
