<?php
namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Magento\Store\Model\StoreManager;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade108
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>

 */
class Upgrade108
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;


    /**
     * @param CmsSetup $cmsSetup
     * @param StoreManager $storeManager
     */
    public function __construct(
        CmsSetup $cmsSetup,
        StoreManager $storeManager
    ) {
        $this->cmsSetup     = $cmsSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        /**
         * menu-women-links
         */
        $menuWomanLinksBlockContent = $this->cmsSetup->getCmsBlockContent(
            'menu-women-links'
        );

        $this->cmsSetup->saveBlock([
            'title'      => 'MENU > Menu Woman Links',
            'identifier' => 'menu-women-links',
            'content'    => $menuWomanLinksBlockContent,
            'is_active'  => 1,
            'stores'     => [0]
        ]);


        /**
         * menu-links-mobile
         */
        $menuLinksMobileBlockContent = $this->cmsSetup->getCmsBlockContent(
            'menu-links-mobile'
        );

        $this->cmsSetup->saveBlock([
            'title'      => 'MENU > Menu Mobile Links',
            'identifier' => 'menu-links-mobile',
            'content'    => $menuLinksMobileBlockContent,
            'is_active'  => 1,
            'stores'     => [0]
        ]);


        /**
         * menu-help-mobile
         */
        $menuHelpMobileBlockContent = $this->cmsSetup->getCmsBlockContent(
            'menu-help-mobile'
        );

        $this->cmsSetup->saveBlock([
            'title'      => 'MENU > Menu Help Mobile',
            'identifier' => 'menu-help-mobile',
            'content'    => $menuHelpMobileBlockContent,
            'is_active'  => 1,
            'stores'     => [0]
        ]);
    }
}
