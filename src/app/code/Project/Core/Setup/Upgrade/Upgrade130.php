<?php

namespace Project\Core\Setup\Upgrade;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade130
 * @package Project\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade130
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade124 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(ConfigSetup $configSetup)
    {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $installer = $upgradeDataObject->getSetup();
        $table     = $installer->getTable('sales_order_status');
        if ($installer->getConnection()->isTableExists($table) == true) {
            $this->initNewStatus($installer);
            $this->initNewStatusState($installer);
            $this->renameExistingStatus($installer);
        }
        $this->configSetup->saveConfig('sales/orders/delete_pending_after', 525600);
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    protected function initNewStatus(ModuleDataSetupInterface $installer)
    {
        $newStatus = [
            [
                'status' => 'sent_to_logistic',
                'label' => 'Sent to logistic'
            ],
            [
                'status' => 'picking',
                'label' => 'Picking progress'
            ],
            [
                'status' => 'packing',
                'label' => 'Packing'
            ],
            [
                'status' => 'shipping',
                'label' => 'Shipping'
            ],
            [
                'status' => 'capture_rejected',
                'label' => 'Capture rejected'
            ],
            [
                'status' => 'invoiced',
                'label' => 'Invoiced'
            ],
            [
                'status' => 'auto_cancel',
                'label' => 'Auto cancel'
            ],
            [
                'status' => 'label_generation_error',
                'label' => 'Label generation error'
            ],
            [
                'status' => 'authorization_error',
                'label' => 'Authorization error'
            ]
        ];
        $installer->getConnection()->insertMultiple(
            $installer->getTable('sales_order_status'),
            $newStatus
        );
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    private function initNewStatusState(ModuleDataSetupInterface $installer)
    {
        $newStatusState = [
            [
                'status' => 'sent_to_logistic',
                'state' => 'sent_to_logistic',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'picking',
                'state' => 'picking',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'packing',
                'state' => 'packing',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'shipping',
                'state' => 'shipping',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'capture_rejected',
                'state' => 'capture_rejected',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'invoiced',
                'state' => 'invoiced',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'auto_cancel',
                'state' => 'auto_cancel',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'label_generation_error',
                'state' => 'label_generation_error',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'authorization_error',
                'state' => 'authorization_error',
                'is_default' => 0,
                'visible_on_front' => 1
            ]
        ];
        $installer->getConnection()->insertMultiple(
            $installer->getTable('sales_order_status_state'),
            $newStatusState
        );
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    protected function renameExistingStatus(ModuleDataSetupInterface $installer)
    {
        $renameStatus = [
            [
                'status' => [
                    'old_status' => 'complete',
                    'row' => [
                        'status' => 'complete',
                        'label' => 'Shipped'
                    ]
                ]
            ],
            [
                'status' => [
                    'old_status' => 'canceled',
                    'row' => [
                        'status' => 'canceled',
                        'label' => 'Cancel'
                    ]
                ]
            ],
            [
                'status' => [
                    'old_status' => 'processing',
                    'row' => [
                        'status' => 'processing',
                        'label' => 'Payment authorized'
                    ]
                ]
            ],
            [
                'status' => [
                    'old_status' => 'pending_payment',
                    'row' => [
                        'status' => 'pending_payment',
                        'label' => 'Payment authorized'
                    ]
                ]
            ],
            [
                'status' => [
                    'old_status' => 'holded',
                    'row' => [
                        'status' => 'holded',
                        'label' => 'Holded'
                    ]
                ]
            ],
            [
                'status' => [
                    'old_status' => 'payment_review',
                    'row' => [
                        'status' => 'payment_review',
                        'label' => 'On Hold'
                    ]
                ]
            ]
        ];
        foreach ($renameStatus as $status) {
            $installer->getConnection()->update(
                $installer->getTable('sales_order_status'),
                $status['status']['row'],
                ['status = ?' => $status['status']['old_status']]
            );
        }
    }
}
