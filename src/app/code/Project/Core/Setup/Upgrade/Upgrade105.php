<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Symfony\Component\Finder\Finder;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade105 to initialise non ecom footer block
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade105
{
    const FIXTURES_DIRECTORY = 'fixtures' . \DIRECTORY_SEPARATOR . 'cms';
    const FIXTURE_FOOTER_DESKTOP_FILE = 'footer-desktop';
    const FIXTURE_FOOTER_MOBILE_FILE = 'footer-mobile';
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;
    /**
     * Upgrade103 constructor.
     *
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @throws \RuntimeException
     */
    public function run($upgradeDataObject)
    {
        // Footer desktop block
        $footerDesktopContent = $this->getFixtureContent(self::FIXTURE_FOOTER_DESKTOP_FILE);

        // Footer mobile block
        $footerMobileContent = $this->getFixtureContent(self::FIXTURE_FOOTER_MOBILE_FILE);

        $cmsBlocks = [
            [
                'identifier' => 'footer-desktop',
                'title'      => 'footer-desktop',
                'is_active'  => 1,
                'content'    => $footerDesktopContent,
                'stores'     => 0,
            ],
            [
                'identifier' => 'footer-mobile',
                'title'      => 'footer-mobile',
                'is_active'  => 1,
                'content'    => $footerMobileContent,
                'stores'     => 0,
            ],
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
    }

    /**
     * @param string $filename
     *
     * @return string
     */
    private function getFixtureContent($filename)
    {
        return $this->cmsSetup->getCmsBlockContent(
            $filename,
            'Project_Core',
            '',
            '',
            'Setup' . \DIRECTORY_SEPARATOR . 'Upgrade' . \DIRECTORY_SEPARATOR . self::FIXTURES_DIRECTORY
        );
    }
}
