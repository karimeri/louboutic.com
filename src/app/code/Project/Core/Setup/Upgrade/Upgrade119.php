<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade119
 * @package Project\Core\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade119
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade119 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(CmsSetup $cmsSetup)
    {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $cmsBlockId = 'cookie-banner';

        $cmsBlock = [
            'title'      => 'Cookie banner',
            'identifier' => $cmsBlockId,
            'content'    => $this->cmsSetup->getCmsBlockContent($cmsBlockId),
            'is_active'  => 1,
            'stores'     => array(0),
            'store_id'   => array(0)
        ];

        $this->cmsSetup->saveBlock($cmsBlock);
    }
}
