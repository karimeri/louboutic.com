<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Project\Core\Setup\UpgradeData;

/**
 * Class Upgrade131
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade131
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade131 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $cmsBlocks = [
            [
                'title'      => 'PRODUCT > Belts sizes table men',
                'identifier' => 'belt-sizes-table-men',
                'content'    => $this->cmsSetup->getCmsBlockContent('belt-sizes-table-men'),
                'is_active'  => 1,
                'stores'     => array(0),
                'store_id'   => array(0)
            ],
            [
                'title'      => 'PRODUCT > Belts sizes table women',
                'identifier' => 'belt-sizes-table-women',
                'content'    => $this->cmsSetup->getCmsBlockContent('belt-sizes-table-women'),
                'is_active'  => 1,
                'stores'     => array(0),
                'store_id'   => array(0)
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
    }
}
