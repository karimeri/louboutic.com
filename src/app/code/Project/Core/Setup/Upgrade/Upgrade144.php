<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade144
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade144
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade144 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blog/'.$locale;

            $headArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-nudes-collection-head',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-nudes-collection-title',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $contentArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-nudes-collection-content',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $blogListArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-list-article-nudes-collection',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'Article - Nudes collection - Head',
                    'identifier' => 'news-nudes-collection-block-head',
                    'content' => $headArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - Nudes collection - Title',
                    'identifier' => 'news-nudes-collection-block-title',
                    'content' => $titleArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - Nudes collection - Content',
                    'identifier' => 'news-nudes-collection-block-content',
                    'content' => $contentArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Blog List - Nudes collection',
                    'identifier' => 'blog-list-news-nudes-collection-block',
                    'content' => $blogListArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier,$stores) {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);

        if($cmsBlockCollection->count()>0){
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }

        return $identifier;
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $blockHeadId = $this->getCmsBlock("news-nudes-collection-block-head",$stores);
            $blockTitleId = $this->getCmsBlock("news-nudes-collection-block-title",$stores);
            $blockContentId = $this->getCmsBlock("news-nudes-collection-block-content",$stores);

            $article =
'{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockHeadId . '"}}
<div class="article-content">
{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockTitleId . '"}}
<div class="text-content">
{{widget type="Magento\Cms\Block\Widget\Block"
    template="widget/static_block/default.phtml"
    block_id="' . $blockContentId . '"}}
 </div>
</div>';

            $articlePage = [
                'title' => "Isn't about time you went Nude?",
                'page_layout' => 'article',
                'identifier' => 'news-nudes-collection',
                'content_heading' => '',
                'content' => $article,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($articlePage, false);
        }
    }
}
