<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade139
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade139
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade139 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        $templatePath = 'misc/cms/blocks/';
        $simpleImage = $this->cmsSetup->getCmsBlockContent(
            'discover-icons-simple-image',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $imageSingleProductRight = $this->cmsSetup->getCmsBlockContent(
            'discover-icons-image-single-product-right',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $imageSingleProductLeft = $this->cmsSetup->getCmsBlockContent(
            'discover-icons-image-single-product-left',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $simpleImage2 = $this->cmsSetup->getCmsBlockContent(
            'discover-icons-simple-image2',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $simpleImage3 = $this->cmsSetup->getCmsBlockContent(
            'discover-icons-simple-image3',
            'Project_Core',
            '',
            '',
            $templatePath
        );

        $cmsBlocks = [
            [
                'title' => 'DISCOVER Icons - Simple image',
                'identifier' => 'discover-icons-simple-image',
                'content' => $simpleImage,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Icons - Simple image 2',
                'identifier' => 'discover-icons-simple-image2',
                'content' => $simpleImage2,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Icons - Simple image 3',
                'identifier' => 'discover-icons-simple-image3',
                'content' => $simpleImage3,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Icons - Image single product right',
                'identifier' => 'discover-icons-image-single-product-right',
                'content' => $imageSingleProductRight,
                'is_active' => 1,
                'stores'     => [0]
            ],
            [
                'title' => 'DISCOVER Icons - Image single product left',
                'identifier' => 'discover-icons-image-single-product-left',
                'content' => $imageSingleProductLeft,
                'is_active' => 1,
                'stores'     => [0]
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);

        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $titleText = $this->cmsSetup->getCmsBlockContent(
                'discover-icons-title-text',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-icons-title-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $linkCategory = $this->cmsSetup->getCmsBlockContent(
                'discover-icons-link-category',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $titleSubtitleSlider = $this->cmsSetup->getCmsBlockContent(
                'discover-icons-title-subtitle-slider',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText2 = $this->cmsSetup->getCmsBlockContent(
                'discover-icons-title-text2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText3 = $this->cmsSetup->getCmsBlockContent(
                'discover-icons-title-text3',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'DISCOVER Icons - Title image',
                    'identifier' => 'discover-icons-title-image',
                    'content' => $titleImage,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Icons - Title text',
                    'identifier' => 'discover-icons-title-text',
                    'content' => $titleText,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Icons - Title text 2',
                    'identifier' => 'discover-icons-title-text2',
                    'content' => $titleText2,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Icons - Title text 3',
                    'identifier' => 'discover-icons-title-text3',
                    'content' => $titleText3,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Icons - Link category',
                    'identifier' => 'discover-icons-link-category',
                    'content' => $linkCategory,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'DISCOVER Icons - Title Subtitle Slider',
                    'identifier' => 'discover-icons-title-subtitle-slider',
                    'content' => $titleSubtitleSlider,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'discover-icons-products-slider',
                'title'          => 'DISCOVER Icons - Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'discover-icons-products-slider',
                        'title'        => 'DISCOVER Icons - Product slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "discover-icons-title-image",
            "discover-icons-title-text",
            "discover-icons-image-single-product-right",
            "discover-icons-title-subtitle-slider",
            "discover-icons-simple-image",
            "discover-icons-title-text2",
            "discover-icons-image-single-product-left",
            "discover-icons-simple-image2",
            "discover-icons-title-text3",
            "discover-icons-simple-image3",
            "discover-icons-link-category"
        ];

        $discover = "";
        foreach ($blockIds as $blockId) {
            $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block"
             template="widget/static_block/default.phtml"
             block_id="'. $blockId .'"}}
</div>';
        }
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'DISCOVER Icons Page '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'discover-icons',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($discoverPage, false);
        }
    }
}
