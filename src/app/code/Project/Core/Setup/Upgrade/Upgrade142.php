<?php

namespace Project\Core\Setup\Upgrade;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Core\Setup\UpgradeData;
/**
 * Class Upgrade142
 * @package Project\Core\Setup\upgrade
 * @author <@kesseddouk>
 */
class Upgrade142
{
    /**
     * @array regions name english ch
     */
    const regions_ch_en = [
        '336' => 'Geneva',
        '315' => 'Aargau',
        '321' => 'Appenzell Ausserrhoden',
        '318' => 'Appenzell Inner Rhodes',
        '327' => 'Basel-Landschaft',
        '330' => 'Basel landscape',
        '324' => 'Bern',
        '333' => 'Freiburg',
        '339' => 'Glarus',
        '342' => 'Graubünden',
        '345' => 'Jura',
        '348' => 'lucerne',
        '351' => 'Neuchâtel',
        '354' => 'Nidwalden',
        '357' => 'Obwalden',
        '363' => 'Schaffhausen',
        '369' => 'Schwyz',
        '366' => 'Solothurn',
        '360' => 'St. Gallen',
        '375' => 'Ticino',
        '372' => 'Thurgau',
        '378' => 'Uri',
        '381' => 'Vaud',
        '384' => 'Valais',
        '387' => 'Zug',
        '390' => 'Zurich'
    ];

    /**
     * @var prefix used in two tab
     */
    const DIRECTORY_COUNTRY_REGION = 'directory_country_region';
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeObject)
    {
        $installer = $upgradeObject->getSetup();
        $table_region = $installer->getTable(self::DIRECTORY_COUNTRY_REGION);
        $table_region_name = $installer->getTable(self::DIRECTORY_COUNTRY_REGION.'_name');
        if ($installer->getConnection()->isTableExists($table_region) == true &&
            $installer->getConnection()->isTableExists($table_region_name) == true) {
            foreach (self::regions_ch_en as $region_id => $name) {
                $installer->getConnection()->update(
                    self::DIRECTORY_COUNTRY_REGION,
                    ['default_name' => $name],
                    ['region_id = ?' => $region_id]
                );
                $installer->getConnection()->update(
                    self::DIRECTORY_COUNTRY_REGION . '_name',
                    ['name' => $name],
                    ['region_id = ?' => $region_id]
                );
            }
        }

    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'fix translation of region on Ch_EN from German to English';
    }
}
