<?php

namespace Project\Core\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Project\Core\Setup\UpgradeData;

class Upgrade135
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade135 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $cmsBlockIdentifier = 'email-social-media';
        $cmsBlocks = [
            [
                'title'      => 'EMAIL > social media block',
                'identifier' => $cmsBlockIdentifier,
                'content'    => $this->cmsSetup->getCmsBlockContent($cmsBlockIdentifier),
                'is_active'  => 1,
                'stores'     => [0],
                'store_id'   => [0]
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($cmsBlocks);
    }
}
