<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade153
 * @package Project\Core\Setup\Upgrade
 */
class Upgrade153
{
    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade153 constructor.
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * @param UpgradeData $upgradeObject
     */
    public function run(UpgradeData $upgradeObject)
    {
        $attributes = [
            [
                'type' => 'customer',
                'code' => 'firstname',
                'data' => [
                    'validate_rules' => '{"min_text_length":"1","max_text_length":"17","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer',
                'code' => 'lastname',
                'data' => [
                    'validate_rules' => '{"min_text_length":"1","max_text_length":"17","input_validation":"length"}',
                ],
            ]
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Set max-length value for customer name attributes';
    }
}
