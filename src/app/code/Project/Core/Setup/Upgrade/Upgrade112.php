<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade112
 * @package Project\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade112
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade112 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(ConfigSetup $configSetup)
    {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->configSetup->saveConfig('catalog/magento_targetrule/upsell_position_limit', 5);
    }
}
