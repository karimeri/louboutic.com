<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade154
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade154
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;
    /**
     * @var SliderSetup
     */
    protected $sliderSetup;
    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade154 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    )
    {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        $templatePath = 'misc/cms/blocks/';
        $simpleImage = $this->cmsSetup->getCmsBlockContent(
            'discover-loubishark-simple-image',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $imageSingleProductRight = $this->cmsSetup->getCmsBlockContent(
            'discover-loubishark-image-single-product-right',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $imageSingleProductLeft = $this->cmsSetup->getCmsBlockContent(
            'discover-loubishark-image-single-product-left',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $simpleImage2 = $this->cmsSetup->getCmsBlockContent(
            'discover-loubishark-simple-image2',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $simpleImage3 = $this->cmsSetup->getCmsBlockContent(
            'discover-loubishark-simple-image3',
            'Project_Core',
            '',
            '',
            $templatePath
        );
        $cmsBlocks = [
            [
                'title' => 'DISCOVER Loubishark - Simple image',
                'identifier' => 'discover-loubishark-simple-image',
                'content' => $simpleImage,
                'is_active' => 1,
                'stores' => [0]
            ],
            [
                'title' => 'DISCOVER Loubishark - Simple image 2',
                'identifier' => 'discover-loubishark-simple-image2',
                'content' => $simpleImage2,
                'is_active' => 1,
                'stores' => [0]
            ],
            [
                'title' => 'DISCOVER Loubishark - Simple image 3',
                'identifier' => 'discover-loubishark-simple-image3',
                'content' => $simpleImage3,
                'is_active' => 1,
                'stores' => [0]
            ],
            [
                'title' => 'DISCOVER Loubishark - Image single product right',
                'identifier' => 'discover-loubishark-image-single-product-right',
                'content' => $imageSingleProductRight,
                'is_active' => 1,
                'stores' => [0]
            ],
            [
                'title' => 'DISCOVER Loubishark - Image single product left',
                'identifier' => 'discover-loubishark-image-single-product-left',
                'content' => $imageSingleProductLeft,
                'is_active' => 1,
                'stores' => [0]
            ]
        ];
        $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $templatePath = 'misc/cms/blocks/' . $locale;
            $titleText = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-text',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleImage = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-image',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $linkCategory = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-link-category',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-subtitle-slider',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleSubtitleSlider2 = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-subtitle-slider2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText2 = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-text2',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleText3 = $this->cmsSetup->getCmsBlockContent(
                'discover-loubishark-title-text3',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $cmsBlocks = [
                [
                    'title' => 'DISCOVER Loubishark - Title image',
                    'identifier' => 'discover-loubishark-title-image',
                    'content' => $titleImage,
                    'is_active' => 1,
                    'store_id' => $stores,
                    'stores' => $stores
                ],
                [
                    'title' => 'DISCOVER Loubishark - Title text',
                    'identifier' => 'discover-loubishark-title-text',
                    'content' => $titleText,
                    'is_active' => 1,
                    'store_id' => $stores,
                    'stores' => $stores
                ],
                [
                    'title' => 'DISCOVER Loubishark - Title text 2',
                    'identifier' => 'discover-loubishark-title-text2',
                    'content' => $titleText2,
                    'is_active' => 1,
                    'store_id' => $stores,
                    'stores' => $stores
                ],
                [
                    'title' => 'DISCOVER Loubishark - Title text 3',
                    'identifier' => 'discover-loubishark-title-text3',
                    'content' => $titleText3,
                    'is_active' => 1,
                    'store_id' => $stores,
                    'stores' => $stores
                ],
                [
                    'title' => 'DISCOVER Loubishark - Link category',
                    'identifier' => 'discover-loubishark-link-category',
                    'content' => $linkCategory,
                    'is_active' => 1,
                    'store_id' => $stores,
                    'stores' => $stores
                ],
                [
                    'title' => 'DISCOVER Loubishark - Title Subtitle Slider',
                    'identifier' => 'discover-loubishark-title-subtitle-slider',
                    'content' => $titleSubtitleSlider,
                    'is_active' => 1,
                    'store_id' => $stores,
                    'stores' => $stores
                ]
            ];
            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier' => 'discover-loubishark-products-slider1',
                'title' => 'DISCOVER Loubishark - Products Slider',
                'autoplay' => 0,
                'autoplay_speed' => 0,
                'infinite' => 0,
                'slides' => [
                    [
                        'identifier' => 'discover-loubishark-products-slider',
                        'title' => 'DISCOVER Loubishark - Product slider',
                        'type' => 0,
                        'link' => '/',
                        'target' => '_self',
                        'position' => 0,
                        'image_small' => '',
                        'image_medium' => '',
                        'image_big' => '',
                        'content' => '',
                        'associated_products' => ['3615482993961', '3615482993961', '3615482993961', '3615482993961'],
                    ],
                ]
            ],
            [
                'identifier' => 'discover-loubishark-products-slider2',
                'title' => 'DISCOVER Loubishark - Products Slider 2',
                'autoplay' => 0,
                'autoplay_speed' => 0,
                'infinite' => 0,
                'slides' => [
                    [
                        'identifier' => 'discover-loubishark-products-slider',
                        'title' => 'DISCOVER Loubishark - Product slider',
                        'type' => 0,
                        'link' => '/',
                        'target' => '_self',
                        'position' => 0,
                        'image_small' => '',
                        'image_medium' => '',
                        'image_big' => '',
                        'content' => '',
                        'associated_products' => ['3615482993961', '3615482993961', '3615482993961', '3615482993961'],
                    ],
                ]
            ]
        ];
        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $blockIds = [
                $this->getCmsBlock("discover-loubishark-title-image", $stores),
                $this->getCmsBlock("discover-loubishark-title-text", $stores),
                "discover-loubishark-image-single-product-right",
                $this->getCmsBlock("discover-loubishark-title-subtitle-slider", $stores),
                "discover-loubishark-simple-image",
                $this->getCmsBlock("discover-loubishark-title-text2", $stores),
                "discover-loubishark-image-single-product-left",
                $this->getCmsBlock("discover-loubishark-title-subtitle-slider2", $stores),
                "discover-loubishark-simple-image2",
                $this->getCmsBlock("discover-loubishark-title-text3", $stores),
                "discover-loubishark-simple-image3",
                $this->getCmsBlock("discover-loubishark-link-category", $stores)
            ];
            $discover = "";
            foreach ($blockIds as $blockId) {
                $discover .= '
<div>
    {{widget type="Magento\Cms\Block\Widget\Block"
             template="widget/static_block/default.phtml"
             block_id="' . $blockId . '"}}
</div>';
            }
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $discoverPage = [
                'title' => 'Discover Loubishark ' . $ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'discover-loubishark',
                'content_heading' => '',
                'content' => $discover,
                'is_active' => 1,
                'store_id' => $stores,
                'stores' => $stores
            ];
            $this->cmsSetup->savePage($discoverPage, false);
        }
    }

    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier, $stores)
    {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);
        if ($cmsBlockCollection->count() > 0) {
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }
        return $identifier;
    }
}
