<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade143
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade143
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade143 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blog/'.$locale;

            $headArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-fw20-collection-head',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-fw20-collection-title',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $contentArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-fw20-collection-content',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $blogListArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-list-article-fw20-collection',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'Article - Fall-Winter 2020 collection - Head',
                    'identifier' => 'christian-louboutin-presents-his-fall-winter-2020-collection-block-head',
                    'content' => $headArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - Fall-Winter 2020 collection - Title',
                    'identifier' => 'christian-louboutin-presents-his-fall-winter-2020-collection-block-title',
                    'content' => $titleArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - Fall-Winter 2020 collection - Content',
                    'identifier' => 'christian-louboutin-presents-his-fall-winter-2020-collection-block-content',
                    'content' => $contentArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Blog List - Fall-Winter 2020 collection',
                    'identifier' => 'blog-list-christian-louboutin-presents-his-fall-winter-2020-collection-block',
                    'content' => $blogListArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier,$stores) {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);

        if($cmsBlockCollection->count()>0){
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }

        return $identifier;
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $blockHeadId = $this->getCmsBlock("christian-louboutin-presents-his-fall-winter-2020-collection-block-head",$stores);
            $blockTitleId = $this->getCmsBlock("christian-louboutin-presents-his-fall-winter-2020-collection-block-title",$stores);
            $blockContentId = $this->getCmsBlock("christian-louboutin-presents-his-fall-winter-2020-collection-block-content",$stores);

            $article =
'{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockHeadId . '"}}
<div class="article-content">
{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockTitleId . '"}}
<div class="text-content">
{{widget type="Magento\Cms\Block\Widget\Block"
    template="widget/static_block/default.phtml"
    block_id="' . $blockContentId . '"}}
 </div>
</div>';

            $articlePage = [
                'title' => 'Christian Louboutin presents his Fall-Winter 2020 collection',
                'page_layout' => 'article',
                'identifier' => 'christian-louboutin-presents-his-fall-winter-2020-collection',
                'content_heading' => '',
                'content' => $article,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($articlePage, false);
        }
    }
}
