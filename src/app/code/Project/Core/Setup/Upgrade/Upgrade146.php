<?php

namespace Project\Core\Setup\Upgrade;

use Project\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Magento\Cms\Model\BlockFactory;

/**
 * Class Upgrade146
 * @package Project\Core\Setup\Upgrade
 * @author Marwen JELLOUL
 */
class Upgrade146
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * Upgrade146 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup,
        BlockFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blog/'.$locale;

            $headArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-new-store-collection-head',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $titleArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-new-store-collection-title',
                'Project_Core',
                '',
                '',
                $templatePath
            );
            $contentArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-page-article-new-store-collection-content',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $blogListArticle = $this->cmsSetup->getCmsBlockContent(
                'blog-list-article-new-store-collection',
                'Project_Core',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'Article - Christian Louboutin unveils a new store on rue Saint-Honoré - Head',
                    'identifier' => 'news-new-store-collection-block-head',
                    'content' => $headArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - Christian Louboutin unveils a new store on rue Saint-Honoré - Title',
                    'identifier' => 'news-new-store-collection-block-title',
                    'content' => $titleArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Article - Christian Louboutin unveils a new store on rue Saint-Honoré - Content',
                    'identifier' => 'news-new-store-collection-block-content',
                    'content' => $contentArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Blog List - Christian Louboutin unveils a new store on rue Saint-Honoré',
                    'identifier' => 'blog-list-news-new-store-collection-block',
                    'content' => $blogListArticle,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $identifier
     * @param $stores
     * @return mixed
     */
    public function getCmsBlock($identifier,$stores) {
        $cmsBlockCollection = $this->blockFactory->create()
            ->getCollection()
            ->addStoreFilter($stores, true)
            ->addFieldToFilter('identifier', $identifier);

        if($cmsBlockCollection->count()>0){
            $cmsBlock = $cmsBlockCollection->getFirstItem();
            return $cmsBlock->getBlockId();
        }

        return $identifier;
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $blockHeadId = $this->getCmsBlock("news-new-store-collection-block-head",$stores);
            $blockTitleId = $this->getCmsBlock("news-new-store-collection-block-title",$stores);
            $blockContentId = $this->getCmsBlock("news-new-store-collection-block-content",$stores);

            $article =
'{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockHeadId . '"}}
<div class="article-content">
{{widget type="Magento\Cms\Block\Widget\Block"
template="widget/static_block/default.phtml"
block_id="' . $blockTitleId . '"}}
<div class="text-content">
{{widget type="Magento\Cms\Block\Widget\Block"
    template="widget/static_block/default.phtml"
    block_id="' . $blockContentId . '"}}
 </div>
</div>';

            $articlePage = [
                'title' => "From draft to craft: Christian Louboutin unveils a new store on rue Saint-Honoré",
                'page_layout' => 'article',
                'identifier' => 'from-draft-to-craft-christian-louboutin-unveils-a-new-store-on-rue-saint-honor',
                'content_heading' => '',
                'content' => $article,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($articlePage, false);
        }
    }
}
