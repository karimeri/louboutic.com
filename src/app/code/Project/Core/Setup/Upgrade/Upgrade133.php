<?php

namespace Project\Core\Setup\Upgrade;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\Store;
use Project\Core\Setup\UpgradeData;

/**
 * Class Upgrade133
 * @package Project\Core\Setup\Upgrade
 */
class Upgrade133
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var ConfigInterface
     */
    protected $configResource;

    /**
     * @var array
     */
    protected $configs = [
        'crontab/default/jobs/synolia_cron_linkshare_export_products/instance',
        'crontab/default/jobs/synolia_cron_linkshare_export_products/method',
        'crontab/default/jobs/synolia_cron_linkshare_export_products/schedule/cron_expr',
        'crontab/default/jobs/synolia_cron_linkshare_export_products/task_id',
        'crontab/default/jobs/synolia_cron_linkshare_export_orders/instance',
        'crontab/default/jobs/synolia_cron_linkshare_export_orders/method',
        'crontab/default/jobs/synolia_cron_linkshare_export_orders/schedule/cron_expr',
        'crontab/default/jobs/synolia_cron_linkshare_export_orders/task_id',
        'crontab/default/jobs/synolia_cron_linkshare_export_canceled_orders/instance',
        'crontab/default/jobs/synolia_cron_linkshare_export_canceled_orders/method',
        'crontab/default/jobs/synolia_cron_linkshare_export_canceled_orders/schedule/cron_expr',
        'crontab/default/jobs/synolia_cron_linkshare_export_canceled_orders/task_id',
        'synchronizations_general/linkshare/MID'
    ];

    /**
     * Upgrade133 constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ConfigInterface $configResource
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ConfigInterface $configResource
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configResource = $configResource;
    }

    /**
     * Clean Project_Linkshare references
     *
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject) {
        $this->moduleDataSetup->deleteTableRow('setup_module', 'module', 'Project_Linkshare');

        foreach ($this->configs as $config) {
            $this->configResource->deleteConfig(
                $config,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                Store::DEFAULT_STORE_ID
            );
        }
    }
}