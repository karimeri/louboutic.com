<?php
namespace Project\Core\Setup\Upgrade;

use Magento\Store\Model\StoreManager;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade121
 * @package Project\Core\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade121
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;


    /**
     * @param CmsSetup $cmsSetup
     * @param StoreManager $storeManager
     */
    public function __construct(
        CmsSetup $cmsSetup,
        StoreManager $storeManager
    ) {
        $this->cmsSetup     = $cmsSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function run()
    {
        $this->cmsSetup->saveBlock([
            'title'      => 'Info Box',
            'identifier' => 'info-box',
            'content'    => $this->cmsSetup->getCmsBlockContent('info-box'),
            'is_active'  => 1,
            'store_id'   => [0]
        ]);
    }
}
