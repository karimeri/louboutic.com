<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade29
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade29 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade29 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('algoliasearch_products/products/number_product_results', '1000');
        // @codingStandardsIgnoreEnd
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Config for Number of products per page in the instant result page Algolia';
    }
}
