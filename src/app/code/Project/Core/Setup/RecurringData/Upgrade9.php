<?php

namespace Project\Core\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade9
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade9 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Upgrade4 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        EnvironmentManager $environmentManager
    ) {
        $this->configSetup = $configSetup;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() === Environment::JP) {
            $this->configSetup->saveConfig('carriers/owsh1/active', 0);
        } else {
            $this->configSetup->saveConfig('carriers/owsh1/active', 1);
        }

        $this->configSetup->saveConfig('carriers/flatrate/active', 0);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Activate Owebia / Deactivate Flatrate';
    }
}
