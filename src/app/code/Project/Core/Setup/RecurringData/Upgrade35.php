<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade35
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade35 implements UpgradeDataSetupInterface
{

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade35 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }


    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('mst_core/css/include_font_awesome', 0);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Configs mst_core';
    }
}
