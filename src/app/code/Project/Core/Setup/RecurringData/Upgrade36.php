<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade36
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade36 implements UpgradeDataSetupInterface
{

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade36 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }


    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('amoptimizer/images/lazy_load_script', 1);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Configs Amasty lazy loading';
    }
}
