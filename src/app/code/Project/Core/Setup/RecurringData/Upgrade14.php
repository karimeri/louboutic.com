<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade14
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade14 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade14 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->cmsSetup->saveMultiplePages([
            [
                'title'             => 'Stopfake',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'stopfake',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ],
            [
                'title'             => 'Terms sale',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'terms-sale',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ],
            [
                'title'             => 'Terms use',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'terms-use',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ],
            [
                'title'             => 'Policy',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'policy',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ],
            [
                'title'             => 'FAQ',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'faq',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Create blank pages : stopfake, terms-sale, terms-use, policy, faq';
    }
}
