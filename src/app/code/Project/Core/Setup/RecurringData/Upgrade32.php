<?php

namespace Project\Core\Setup\RecurringData;

use Magento\Framework\MessageQueue\Config\Reader\Env;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Store\Model\StoreManager;
use Synolia\SalesCustomSequence\Setup\SequenceProfileSetupFactory;
use Synolia\SalesCustomSequence\Setup\SequenceProfileSetup;

/**
 * Class Upgrade32
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade32 implements UpgradeDataSetupInterface
{
    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var SequenceProfileSetup
     */
    protected $sequenceProfileSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Upgrade109 constructor.
     * @param StoreManager $storeManager
     * @param SequenceProfileSetupFactory $profileSetupFactory
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        StoreManager $storeManager,
        SequenceProfileSetupFactory $profileSetupFactory,
        EnvironmentManager $environmentManager
    ) {
        $this->storeManager = $storeManager;
        $this->sequenceProfileSetup = $profileSetupFactory->create();
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgrade
     */
    public function run(Upgrade $upgrade)
    {
        $stores = $this->storeManager->getStores(false, true);

        switch ($this->environmentManager->getEnvironment()) {
            case Environment::EU:
                $instanceCodes = [
                    'fr_fr' => 2,
                    'uk_en' => 11,
                    'it_en' => 9,
                    'de_en' => 15,
                    'es_en' => 21,
                    'ch_en' => 23,
                    'ch_fr' => 24,
                    'nl_en' => 13,
                    'lu_en' => 17,
                    'lu_fr' => 18,
                    'be_en' => 19,
                    'be_fr' => 20,
                    'at_en' => 25,
                    'ie_en' => 27,
                    'pt_en' => 29,
                    'mc_en' => 31,
                    'mc_fr' => 32,
                    'gr_en' => 33
                ];
                break;
            case Environment::US:
                $instanceCodes = [
                    'us_en' => 10,
                    'ca_en' => 14,
                    'ca_fr' => 17
                ];
                break;
            case Environment::JP:
                $instanceCodes = [
                    'jp_ja' => 2
                ];
                break;
            case Environment::HK:
                $instanceCodes = [
                    'au_en' => 14,
                    'my_en' => 11,
                    'sg_en' => 30,
                    'sg_sc' => 40,
                    'tw_en' => 60,
                    'tw_tc' => 10,
                    'hk_en' => 61,
                    'hk_tc' => 20
                ];
                break;
        }

        foreach ($stores as $code => $store) {
            if (isset($instanceCodes[$code])) {
                $this->sequenceProfileSetup->updateProfileByStoreIds(
                    [$store->getStoreId()],
                    ['order', 'shipment', 'rma_item'],
                    [
                        'prefix' => $instanceCodes[$code],
                    ]
                );
            }
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Change prefix order increment id';
    }
}
