<?php

namespace Project\Core\Setup\RecurringData;

use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManager;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade8
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade8 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade6 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        EnvironmentManager $environmentManager,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->environmentManager = $environmentManager;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->setDefaultTaxCountry();

        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $this->configSetup->saveConfig('tax/general/service', 'avalara');
            $this->setupDisplayFullTaxSummary();
        }

        if ($this->environmentManager->getEnvironment() == Environment::HK) {
            $this->setupDutyCalculator();
            $this->setupDisplayFullTaxSummary();

            $auWebsite = $this->storeManager->getWebsite('au');
            $this->configSetup->saveConfig(
                'tax/general/dutycalculator_calculation_threshold',
                1000,
                ScopeInterface::SCOPE_WEBSITES,
                $auWebsite->getWebsiteId()
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Init Tax Config';
    }

    /**
     * Enable Duty Calculator Tax service
     */
    protected function setupDutyCalculator()
    {
        $this->configSetup->saveConfig('tax/general/service', 'dutycalculator');
        $this->configSetup->saveConfig('tax/calculation/algorithm', 'ROW_BASE_CALCULATION');
    }

    /**
     * Set default country for tax calculation on each website
     */
    protected function setDefaultTaxCountry()
    {
        /** @var \Magento\Store\Api\Data\WebsiteInterface $website */
        foreach ($this->storeManager->getWebsites() as $website) {
            $this->configSetup->saveConfig(
                'tax/defaults/country',
                \strtoupper($website->getCode()),
                ScopeInterface::SCOPE_WEBSITES,
                $website->getId()
            );
        }
    }

    /**
     * Enable display full tax summary on cart and sales documents
     */
    protected function setupDisplayFullTaxSummary()
    {
        $this->configSetup->saveConfig('tax/cart_display/full_summary', 1);
        $this->configSetup->saveConfig('tax/sales_display/full_summary', 1);
    }
}
