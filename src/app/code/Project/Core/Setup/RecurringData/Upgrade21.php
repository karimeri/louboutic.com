<?php

namespace Project\Core\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade21
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade21 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade21 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        EnvironmentManager $environmentManager,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->environmentManager = $environmentManager;
        $this->taskRepository = $taskRepository;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            [
                'name' => 'delete_alert_back_in_stock',
                'active' => 1,
                'frequency' => '0 0 * * *',
                'command' => 'project:delete:alert',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'delete_email_sent',
                'active' => 1,
                'frequency' => '0 0 * * *',
                'command' => 'project:delete:email',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'export_newsletter_sales_force',
                'active' => 1,
                'frequency' => '0 5 * * *',
                'command' => 'synolia:sync:launch export_newsletter',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'import_bounce_sales_force',
                'active' => 1,
                'frequency' => '0 1 * * *',
                'command' => 'synolia:sync:launch import_bounce',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'import_unsubscribe_sales_force',
                'active' => 1,
                'frequency' => '0 1 * * *',
                'command' => 'synolia:sync:launch import_unsubscribe',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ]
        ];

        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $dataUS = [
                [
                    'name' => 'send_label_generation_to_wms_us',
                    'active' => 1,
                    'frequency' => '10 * * * *',
                    'command' => 'synolia:sync:launch send_label_generation_to_wms_us',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'send_label_generation_to_wms_ca',
                    'active' => 1,
                    'frequency' => '10 * * * *',
                    'command' => 'synolia:sync:launch send_label_generation_to_wms_ca',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'workflow_send_order_to_wms_us',
                    'active' => 1,
                    'frequency' => '*/15 * * * *',
                    'command' => 'synolia:sync:launch send_order_to_wms_us',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'workflow_send_order_to_wms_ca',
                    'active' => 1,
                    'frequency' => '*/15 * * * *',
                    'command' => 'synolia:sync:launch send_order_to_wms_ca',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ]
            ];

            $data = array_merge($dataUS, $data);
        } else {
            $data[] =
                [
                    'name' => 'workflow_send_order',
                    'active' => 1,
                    'frequency' => '*/15 * * * *',
                    'command' => 'synolia:sync:launch send_order_to_wms',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ];
            $data[] =
                [
                    'name' => 'send_label_generation_to_wms',
                    'active' => 1,
                    'frequency' => '10 * * * *',
                    'command' => 'synolia:sync:launch send_label_generation_to_wms',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ];
        }

        foreach ($data as $task) {
            $taskModel = $this->task->setData($task);
            $this->taskRepository->save($taskModel);
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Init platform cron';
    }
}
