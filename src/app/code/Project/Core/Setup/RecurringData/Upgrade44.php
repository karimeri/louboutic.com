<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Eav\EavSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade44
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade44 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade44 constructor.
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            [
                'type' => \Magento\Catalog\Model\Product::ENTITY,
                'code' => 'beauty_volume',
                'data' => [
                    'is_visible_on_front' => 1,
                ],
            ],
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Update beauty_volume attribute';
    }
}
