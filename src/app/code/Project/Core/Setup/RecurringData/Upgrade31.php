<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade31
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade31 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade31 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('louboutin_preorder/availability_display/soon_availability', 'This Product is Available Upon Request');
        $this->configSetup->saveConfig('louboutin_preorder/availability_display/available_from', 'This Product is expected to be available on %s');
        // @codingStandardsIgnoreEnd
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Pre-order availability date config';
    }
}
