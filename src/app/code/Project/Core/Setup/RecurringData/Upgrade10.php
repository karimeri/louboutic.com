<?php

namespace Project\Core\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade9
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade10 implements UpgradeDataSetupInterface
{
    const MAPPING = [
        Environment::US => ' United Parcel Service',
        Environment::EU => ' DHL',
        Environment::HK => ' DHL',
        Environment::JP => ' 宅配',
    ];

    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Upgrade4 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        EnvironmentManager $environmentManager
    ) {
        $this->configSetup = $configSetup;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig(
            'carriers/owsh1/title',
            self::MAPPING[$this->environmentManager->getEnvironment()]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Edit Owebia Title';
    }
}
