<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade34
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade34 implements UpgradeDataSetupInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * Upgrade34 constructor
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }


    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $newStatusState = [
            [
                'status' => 'need_to_reship',
                'state' => 'complete',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
            [
                'status' => 'ready_to_reship',
                'state' => 'complete',
                'is_default' => 0,
                'visible_on_front' => 1
            ],
        ];
        $this->setup->getConnection()->insertMultiple(
            $this->setup->getTable('sales_order_status_state'),
            $newStatusState
        );

        $newStatus = [
            [
                'status' => 'need_to_reship',
                'label' => 'Need to reship'
            ],
            [
                'status' => 'ready_to_reship',
                'label' => 'Ready to reship'
            ]
        ];
        $this->setup->getConnection()->insertMultiple(
            $this->setup->getTable('sales_order_status'),
            $newStatus
        );
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Add status for reship';
    }
}
