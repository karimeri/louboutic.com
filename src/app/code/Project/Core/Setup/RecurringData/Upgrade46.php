<?php

namespace Project\Core\Setup\RecurringData;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Sales\Model\Magento\Sales\Order;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade46
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade46 implements UpgradeDataSetupInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * Upgrade34 constructor
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }

    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->setup->getConnection()->update(
            $this->setup->getTable('sales_order_status_state'),
            ['state' => Order::STATE_NEED_TO_RESHIP],
            [
                'status = ?' => Order::STATE_NEED_TO_RESHIP,
                'state = ?' => Order::STATE_COMPLETE
            ]
        );

        $this->setup->getConnection()->update(
            $this->setup->getTable('sales_order_status_state'),
            ['state' => Order::STATE_READY_TO_RESHIP],
            [
                'status = ?' => Order::STATE_READY_TO_RESHIP,
                'state = ?' => Order::STATE_COMPLETE
            ]
        );
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Update status for reship';
    }
}
