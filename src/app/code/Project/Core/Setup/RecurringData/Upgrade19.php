<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade19
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade19 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade19 constructor.
     * @param ConfigSetup $configSetup
     * @param StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('catalog/productalert_cron/frequency', 'D');
        $this->configSetup->saveConfig('crontab/default/jobs/catalog_product_alert/schedule/cron_expr', '59 23 * * *');
        $this->configSetup->saveConfig('catalog/productalert_cron/time', '23,59,00');
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Cron product alert stock';
    }
}
