<?php

namespace Project\Core\Setup\RecurringData;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Store;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade45
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade45 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigInterface
     */
    protected $configResource;

    /**
     * Upgrade45 constructor.
     * @param ConfigInterface $configResource
     */
    public function __construct(
        ConfigInterface $configResource
    ) {
        $this->configResource = $configResource;
    }

    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configResource->saveConfig(
            'general/locale/code',
            'en_GB',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            Store::DEFAULT_STORE_ID
            );
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Update admin store locale';
    }
}
