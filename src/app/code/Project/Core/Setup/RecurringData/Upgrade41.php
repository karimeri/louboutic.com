<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade41
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade41 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade39 constructor.
     * @param \Synolia\Standard\Setup\Eav\CustomerAddressSetup $customerAddressSetup
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup             = $eavSetup;
    }

    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'telephone',
                'data' => [
                    'validate_rules' => '{"max_text_length":"16","input_validation":"length"}',
                ],
            ],
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Update telephone attribute';
    }
}
