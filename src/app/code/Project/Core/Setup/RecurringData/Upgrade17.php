<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade17
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade17 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade17 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $discoverFragrance = $this->cmsSetup->getCmsBlockContent(
            'discover-fragrance',
            'Project_Core',
            '',
            '',
            'misc/cms/pages'
        );

        $this->cmsSetup->savePage(
            [
                'title'             => 'Christian Louboutin - Discover Fragrance',
                'page_layout'       => 'fullscreen',
                'identifier'        => 'discover-fragrance',
                'content_heading'   => '',
                'content'           => $discoverFragrance,
                'is_active'         => 1,
                'stores'            => [0],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Create page cms discover-fragrance';
    }
}
