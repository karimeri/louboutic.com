<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\BlockRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Upgrade2
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade2 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteria;

    /**
     * @var \Magento\Cms\Model\BlockRepository
     */
    protected $blockRepository;

    public function __construct(
        CmsSetup $cmsSetup,
        SearchCriteriaBuilder $searchCriteria,
        BlockRepository $blockRepository
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->searchCriteria = $searchCriteria;
        $this->blockRepository = $blockRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $blocksToDelete = [];
        $messages = [];

        $search = $this->searchCriteria->addFilter('identifier', 'checkout-reassurance')
                                       ->create();

        $blockList = $this->blockRepository->getList($search);

        if ($blockList->getTotalCount()>0) {
            foreach ($blockList->getItems() as $cmsBlock) {
                $blocksToDelete[] = $cmsBlock->getId();
            }
        }

        foreach (array_unique($blocksToDelete) as $blockId) {
            try {
                $this->blockRepository->deleteById($blockId);
            } catch (\Exception $exception) {
                $messages[] = $exception->getMessage();
                continue;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Remove CMS Block checkout-reassurance';
    }
}
