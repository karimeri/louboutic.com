<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade1
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\Eav\CustomerAddressSetup $customerAddressSetup
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup             = $eavSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'firstname',
                'data' => [
                    'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'lastname',
                'data' => [
                    'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'company',
                'data' => [
                    'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'telephone',
                'data' => [
                    'validate_rules' => '{"max_text_length":"25","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'street',
                'data' => [
                    'validate_rules' => '{"max_text_length":"38","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'city',
                'data' => [
                    'validate_rules' => '{"max_text_length":"38","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'postcode',
                'data' => [
                    'validate_rules' => '{"max_text_length":"38","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'country_id',
                'data' => [
                    'validate_rules' => '{"max_text_length":"2","input_validation":"length"}',
                ],
            ],
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Set max-length value for customer address attributes';
    }
}
