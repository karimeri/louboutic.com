<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\EavSetup;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade20
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade20 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade20 constructor.
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        EavSetup $eavSetup,
        CmsSetup $cmsSetup
    ) {
        $this->eavSetup = $eavSetup;
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'telephone',
                'data' => [
                    'sort_order' => 64,
                ]
            ],
            [
                'type' => 'customer_address',
                'code' => 'contact_telephone',
                'data' => [
                    'sort_order' => 66,
                ]
            ],
            [
                'type' => 'customer_address',
                'code' => 'region',
                'data' => [
                    'sort_order' => 120,
                ]
            ],
            [
                'type' => 'customer_address',
                'code' => 'region_id',
                'data' => [
                    'sort_order' => 120,
                ]
            ]
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                if ($this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                    $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Set sort order of shipping address fields';
    }
}
