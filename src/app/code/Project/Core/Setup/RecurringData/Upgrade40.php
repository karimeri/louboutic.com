<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade40
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade40 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade40 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            'name' => 'stock_index_full',
            'active' => 1,
            'frequency' => '0 4 * * *',
            'command' => 'indexer:reindex',
            'parameter' => 'cataloginventory_stock',
            'option' => '',
            'isolated' => 1
        ];

        $taskModel = $this->task->setData($data);
        $this->taskRepository->save($taskModel);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add cron full reindex stock';
    }
}
