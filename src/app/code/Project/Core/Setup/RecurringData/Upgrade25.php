<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade25
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade25 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade25 constructor.
     * @param ConfigSetup $configSetup
     * @param StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('algoliasearch_products/products/product_additional_attributes', '[{"attribute":"name","searchable":"1","retrievable":"1","order":"ordered"},{"attribute":"path","searchable":"1","retrievable":"1","order":"ordered"},{"attribute":"categories","searchable":"1","retrievable":"1","order":"ordered"},{"attribute":"color","searchable":"1","retrievable":"1","order":"ordered"},{"attribute":"sku","searchable":"1","retrievable":"1","order":"ordered"},{"attribute":"price","searchable":"2","retrievable":"1","order":"ordered"},{"attribute":"ordered_qty","searchable":"2","retrievable":"2","order":"ordered"},{"attribute":"stock_qty","searchable":"2","retrievable":"2","order":"ordered"},{"attribute":"rating_summary","searchable":"2","retrievable":"1","order":"ordered"},{"attribute":"heel_height","searchable":"2","retrievable":"1","order":"ordered"},{"attribute":"additional_1","searchable":"2","retrievable":"2","order":"ordered"},{"attribute":"material_product_page","searchable":"2","retrievable":"1","order":"ordered"},{"attribute":"heel_height_map","searchable":"2","retrievable":"2","order":"ordered"},{"attribute":"meta_keyword","searchable":"1","retrievable":"1","order":"ordered"}]');
        $this->configSetup->saveConfig('algoliasearch_advanced/advanced/remove_pub_dir_in_url', '1');
        // @codingStandardsIgnoreEnd
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Configurations for algolia';
    }
}
