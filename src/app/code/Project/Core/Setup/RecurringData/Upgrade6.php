<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade6
 *
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade6 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade4 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('catalog/magento_catalogpermissions/enabled', 1);
        $this->configSetup->saveConfig('catalog/magento_catalogpermissions/grant_catalog_category_view', 1);
        $this->configSetup->saveConfig('catalog/magento_catalogpermissions/grant_catalog_product_price', 1);
        $this->configSetup->saveConfig('catalog/magento_catalogpermissions/grant_checkout_items', 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Set category permissions config';
    }
}
