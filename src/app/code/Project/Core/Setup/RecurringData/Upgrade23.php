<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade23
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade23 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade23 constructor.
     * @param ConfigSetup $configSetup
     * @param StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('synolia_custommenu/general/item_menu/', '{"_1520355346180_180":{"group_menu":"default","label":"Louboutin World","title":"","link":"\/news","is_openable":"1","is_target_blank":"1","css_class":"louboutin-world","block_id":"menu-news","sort":"10"},"_1520407974910_910":{"group_menu":"default","label":"Stores","title":"","link":"\/","is_openable":"0","is_target_blank":"1","css_class":"stores widescreen","block_id":"","sort":"20"}}');
        // @codingStandardsIgnoreEnd
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Add link in custommenu';
    }
}
