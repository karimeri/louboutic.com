<?php

namespace Project\Core\Setup\RecurringData;


use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Sales\Model\Magento\Sales\Order;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade47
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade47 implements UpgradeDataSetupInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * Upgrade34 constructor
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }

    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $newStatusState = [
            [
                'status' => Order::STATE_REFUND_ERROR,
                'state' => Order::STATE_REFUND_ERROR,
                'is_default' => 0,
                'visible_on_front' => 0
            ]
        ];
        $this->setup->getConnection()->insertMultiple(
            $this->setup->getTable('sales_order_status_state'),
            $newStatusState
        );

        $newStatus = [
            [
                'status' => Order::STATE_REFUND_ERROR,
                'label' => 'Refund error'
            ]
        ];
        $this->setup->getConnection()->insertMultiple(
            $this->setup->getTable('sales_order_status'),
            $newStatus
        );
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Add status for refund error';
    }
}
