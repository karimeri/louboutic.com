<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade13
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade13 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade13 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('checkout/options/display_billing_address_on', 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Display billing address after payment method';
    }
}
