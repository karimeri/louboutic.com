<?php

namespace Project\Core\Setup\RecurringData;

use Magento\Store\Model\StoreManagerInterface;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade43
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade43 implements UpgradeDataSetupInterface
{
    const STORES_FR = [
        'fr_fr',
        'ch_fr',
        'lu_fr',
        'be_fr',
        'mc_fr'
    ];

    const STORES_EN = [
        'uk_en',
        'it_en',
        'de_en',
        'es_en',
        'ch_en',
        'nl_en',
        'lu_en',
        'be_en',
        'at_en',
        'ie_en',
        'pt_en',
        'mc_en',
        'gr_en'
    ];

    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Upgrade43 constructor.
     * @param CmsSetup $cmsSetup
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CmsSetup $cmsSetup,
        StoreManagerInterface $storeManager
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function run(Upgrade $upgradeObject)
    {
        /**
         * menu-help-mobile-fr
         */
        $menuHelpMobileFrBlockContent = $this->cmsSetup->getCmsBlockContent(
            'menu-help-mobile-fr'
        );

        $storesFr = [];
        foreach (self::STORES_FR as $storeCode) {
            $storesFr[] = $this->storeManager->getStore($storeCode)->getId();
        }

        $this->cmsSetup->saveBlock([
            'title'      => 'MENU > Menu Help Mobile',
            'identifier' => 'menu-help-mobile',
            'content'    => $menuHelpMobileFrBlockContent,
            'is_active'  => 1,
            'stores'     => $storesFr
        ]);

        /**
         * menu-help-mobile-en
         */
        $menuHelpMobileFrBlockContent = $this->cmsSetup->getCmsBlockContent(
            'menu-help-mobile-en'
        );

        $storesEn = [];
        foreach (self::STORES_EN as $storeCode) {
            $storesEn[] = $this->storeManager->getStore($storeCode)->getId();
        }

        $this->cmsSetup->saveBlock([
            'title'      => 'MENU > Menu Help Mobile',
            'identifier' => 'menu-help-mobile',
            'content'    => $menuHelpMobileFrBlockContent,
            'is_active'  => 1,
            'stores'     => $storesEn
        ]);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Add Create Return link in menu-help-mobile block';
    }
}
