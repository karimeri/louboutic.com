<?php

namespace Project\Core\Setup\RecurringData;

use Project\Sales\Model\Magento\Sales\Order;
use Synolia\Standard\Setup\Upgrade;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade38
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade38 implements UpgradeDataSetupInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * Upgrade38 constructor
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }


    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $newStatusState = [
            [
                'status' => Order::STATE_PREORDER_ENTERING_STOCK,
                'state' => Order::STATE_PREORDER_ENTERING_STOCK,
                'is_default' => 0,
                'visible_on_front' => 1
            ]
        ];
        $this->setup->getConnection()->insertMultiple(
            $this->setup->getTable('sales_order_status_state'),
            $newStatusState
        );

        $newStatus = [
            [
                'status' => Order::STATE_PREORDER_ENTERING_STOCK,
                'label' => 'Preorder entering stock'
            ]
        ];
        $this->setup->getConnection()->insertMultiple(
            $this->setup->getTable('sales_order_status'),
            $newStatus
        );
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Add status for preorder';
    }
}
