<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Store\Model\StoreManager;
use Synolia\SalesCustomSequence\Setup\SequenceProfileSetupFactory;
use Synolia\SalesCustomSequence\Setup\SequenceProfileSetup;

/**
 * Class Upgrade18
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade18 implements UpgradeDataSetupInterface
{
    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var SequenceProfileSetup
     */
    protected $sequenceProfileSetup;

    /**
     * Upgrade109 constructor.
     * @param StoreManager $storeManager
     * @param SequenceProfileSetupFactory $profileSetupFactory
     */
    public function __construct(
        StoreManager $storeManager,
        SequenceProfileSetupFactory $profileSetupFactory
    ) {
        $this->storeManager         = $storeManager;
        $this->sequenceProfileSetup = $profileSetupFactory->create();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgrade
     */
    public function run(Upgrade $upgrade)
    {
        $websites   = $this->storeManager->getWebsites();
        $websiteIds = array_keys($websites);
        $letter     = getenv('PROJECT_ENV') == 'eu' ? 'F' : 'I'; //@codingStandardsIgnoreLine


        $this->sequenceProfileSetup->updateProfileByWebsiteIds(
            $websiteIds,
            ['invoice'],
            [
                'prefix' => "$letter{yyyy}-",
                'suffix' => '_{STOCKCODE}',
            ]
        );

        $this->sequenceProfileSetup->updateProfileByWebsiteIds(
            $websiteIds,
            ['creditmemo'],
            [
                'prefix' => 'C{yyyy}-',
                'suffix' => '_{STOCKCODE}',
            ]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Adding profile code';
    }
}
