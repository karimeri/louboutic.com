<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade42
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade42 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade4 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('checkout/cart_link/use_qty', 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Set shopping cart number item config';
    }
}
