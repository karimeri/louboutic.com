<?php

declare(strict_types=1);

namespace Project\Core\Setup\RecurringData;

use Magento\Store\Model\StoreManagerInterface;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade49
 * @package Project\Core\Setup\RecurringData
 * Add product care link to mobile help block
 */
class Upgrade49 implements UpgradeDataSetupInterface
{
    /** @var CmsSetup */
    protected $cmsSetup;

    /** @var StoreManagerInterface */
    protected $storeManager;

    /** @var array */
    private $templateStoreMapping = [
        'menu-help-mobile-fr' => [
            'fr_fr',
            'ch_fr',
            'lu_fr',
            'be_fr',
            'mc_fr'
        ],
        'menu-help-mobile-en' => [
            'uk_en',
            'it_en',
            'de_en',
            'es_en',
            'ch_en',
            'nl_en',
            'lu_en',
            'be_en',
            'at_en',
            'ie_en',
            'pt_en',
            'mc_en',
            'gr_en'
        ]
    ];

    public function __construct(
        CmsSetup $cmsSetup,
        StoreManagerInterface $storeManager
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->storeManager = $storeManager;
    }

    public function run(Upgrade $upgradeData): void
    {
        foreach ($this->templateStoreMapping as $template => $storeCodes) {
            $blockContent = $this->cmsSetup->getCmsBlockContent($template);
            $storeIds = [];
            foreach ($storeCodes as $storeCode) {
                $storeIds[] = $this->storeManager->getStore($storeCode)->getId();
            }
            $this->cmsSetup->saveBlock([
                'title' => 'MENU > Menu Help Mobile',
                'identifier' => 'menu-help-mobile',
                'content' => $blockContent,
                'is_active' => true,
                'stores' => $storeIds
            ]);
        }
    }

    public function getDescription(): string
    {
        return 'Add product care link to mobile help block';
    }
}

