<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade39
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade39 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade39 constructor.
     * @param \Synolia\Standard\Setup\Eav\CustomerAddressSetup $customerAddressSetup
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup             = $eavSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'firstname',
                'data' => [
                    'validate_rules' => '{"max_text_length":"17","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'lastname',
                'data' => [
                    'validate_rules' => '{"max_text_length":"17","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'street',
                'data' => [
                    'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'city',
                'data' => [
                    'validate_rules' => '{"max_text_length":"30","input_validation":"length"}',
                ],
            ]
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Set max-length value for customer address attributes';
    }
}
