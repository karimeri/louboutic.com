<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade5
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade5 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade5 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->cmsSetup->saveMultiplePages([
            [
                'title'             => 'Product Care',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'product-care',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ],
            [
                'title'             => 'Product Care - Leather Goods',
                'page_layout'       => 'nofullscreen',
                'identifier'        => 'product-care-leather-goods',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 1,
                'stores'            => [0],
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Create blank pages : product-care, product-care-leather-goods';
    }
}
