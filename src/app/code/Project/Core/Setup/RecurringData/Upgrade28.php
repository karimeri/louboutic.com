<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\PageRepository;

/**
 * Class Upgrade28
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade28 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Magento\Cms\Model\PageRepository
     */
    protected $pageRepository;

    /**
     * Upgrade28 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Magento\Cms\Model\PageRepository $pageRepository
     */
    public function __construct(
        CmsSetup $cmsSetup,
        PageRepository $pageRepository
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->pageRepository     = $pageRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {

        $page = $this->cmsSetup->getPage('discover-fragrance', 0);

        if ($page->getId()) {
            $this->pageRepository->delete($page);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Delete page cms discover-fragrance on default store';
    }
}
