<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade33
 * @package Project\Core\Setup\RecurringData
 */
class Upgrade33 implements UpgradeDataSetupInterface
{

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade33 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }


    /**
     * Runs setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('amoptimizer/general/enabled', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/javascript/movejs', 0);
        $this->configSetup->saveConfig('amoptimizer/settings/javascript/merge_js', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/javascript/minify_js', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/javascript/js_bundling', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/css/merge_css', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/css/minify_css', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/css/move_font', 1);
        $this->configSetup->saveConfig('amoptimizer/settings/html/minify_html', 1);
        // @codingStandardsIgnoreEnd
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Configs Amasty';
    }
}
