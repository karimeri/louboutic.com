<?php

namespace Project\Core\Setup\RecurringData;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade26
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade26 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * Upgrade26 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager,
        ModuleDataSetupInterface $setup
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
        $this->setup = $setup;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->setup->getConnection()->insertOnDuplicate(
            'sales_order_status_state',
            [
                [
                    'status' => 'processing',
                    'state' => 'new',
                    'is_default' => 1,
                    'visible_on_front' => 1
                ]
            ]
        );

        $this->setup->getConnection()->update(
            'sales_order_status_state',
            ['is_default' => 0],
            [
                'status = ?' => 'pending',
                'state = ?' => 'new'
            ]
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Add new state for cash on delivery';
    }
}
