<?php

namespace Project\Core\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade24
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade24 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade21 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        EnvironmentManager $environmentManager,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->environmentManager = $environmentManager;
        $this->taskRepository = $taskRepository;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $data = [
                [
                    'name' => 'send_rma_to_wms_ca',
                    'active' => 1,
                    'frequency' => '20 * * * *',
                    'command' => 'synolia:sync:launch send_rma_to_wms_ca',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'send_rma_to_wms_us',
                    'active' => 1,
                    'frequency' => '20 * * * *',
                    'command' => 'synolia:sync:launch send_rma_to_wms_us',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'update_rma_with_wms_ca',
                    'active' => 1,
                    'frequency' => '30 * * * *',
                    'command' => 'synolia:sync:launch update_rma_with_wms_ca',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'update_rma_with_wms_us',
                    'active' => 1,
                    'frequency' => '30 * * * *',
                    'command' => 'synolia:sync:launch update_rma_with_wms_us',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ]
            ];
        } else {
            $data = [
                [
                    'name' => 'send_rma_to_wms',
                    'active' => 1,
                    'frequency' => '20 * * * *',
                    'command' => 'synolia:sync:launch send_rma_to_wms',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ],
                [
                    'name' => 'update_rma_with_wms',
                    'active' => 1,
                    'frequency' => '30 * * * *',
                    'command' => 'synolia:sync:launch update_rma_with_wms',
                    'parameter' => '',
                    'option' => '',
                    'isolated' => 1
                ]
            ];
        }

        foreach ($data as $task) {
            $taskModel = $this->task->setData($task);
            $this->taskRepository->save($taskModel);
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Init platform cron';
    }
}
