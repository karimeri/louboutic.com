<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade16
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade16 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade16 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgrade
     * @throws \Magento\Framework\Exception\FileSystemException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgrade)
    {
        $emailFooterContent = $this->cmsSetup->getCmsBlockContent('email-footer');
        $emailHeaderContent = $this->cmsSetup->getCmsBlockContent('email-header');

        $blocks = [
            [
                'title'      => 'Email - Footer',
                'identifier' => 'email-footer',
                'content'    => $emailFooterContent,
                'is_active'  => 1,
                'stores'     => [0]
            ],
            [
                'title'      => 'Email - Header',
                'identifier' => 'email-header',
                'content'    => $emailHeaderContent,
                'is_active'  => 1,
                'stores'     => [0]
            ]
        ];

        $this->cmsSetup->saveMultipleBlocks($blocks);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Creating CMS block email-header and email-footer';
    }
}
