<?php

namespace Project\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade7
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade7 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade7 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgrade
     * @throws \Magento\Framework\Exception\FileSystemException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgrade)
    {
        $storeSwitcherCountryListContent = $this->cmsSetup->getCmsBlockContent('store-switcher-country-list');

        $this->cmsSetup->saveBlock([
            'title'      => 'Store Switcher Country List',
            'identifier' => 'store-switcher-country-list',
            'content'    => $storeSwitcherCountryListContent,
            'is_active'  => 1,
            'stores'     => [0]
        ]);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Creating CMS block store-switcher-country-list';
    }
}
