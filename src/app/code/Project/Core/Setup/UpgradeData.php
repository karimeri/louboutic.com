<?php

namespace Project\Core\Setup;

use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepository;

/**
 * Class UpgradeData
 * @package   Project\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class UpgradeData implements UpgradeDataInterface
{
    const VERSIONS = [
        '1.0.1' => '101',
        '1.0.2' => '102',
        '1.0.3' => '103',
        '1.0.5' => '105',
        '1.0.6' => '106',
        '1.0.7' => '107',
        '1.0.8' => '108',
        '1.1.0' => '110',
        '1.1.2' => '112',
        '1.1.3' => '113',
        '1.1.4' => '114',
        '1.1.5' => '115',
        '1.1.6' => '116',
        '1.1.7' => '117',
        '1.1.8' => '118',
        '1.1.9' => '119',
        '1.2.0' => '120',
        '1.2.1' => '121',
        '1.2.2' => '122',
        '1.2.3' => '123',
        '1.2.5' => '125',
        '1.2.6' => '126',
        '1.2.7' => '127',
        '1.2.8' => '128',
        '1.2.9' => '129',
        '1.3.0' => '130',
        '1.3.1' => '131',
        '1.3.2' => '132',
        '1.3.3' => '133',
        '1.3.5' => '135',
        '1.3.6' => '136',
        '1.3.7' => '137',
        '1.3.8' => '138',
        '1.3.9' => '139',
        '1.4.0' => '140',
        '1.4.1' => '141',
        '1.4.2' => '142',
        '1.4.3' => '143',
        '1.4.4' => '144',
        '1.4.5' => '145',
        '1.4.6' => '146',
        '1.4.7' => '147',
        '1.4.8' => '148',
        '1.4.9' => '149',
        '1.5.0' => '150',
        '1.5.1' => '151',
        '1.5.2' => '152',
        '1.5.3' => '153',
        '1.5.4' => '154',
        '1.5.5' => '155',
        '1.5.6' => '156',
        '1.5.7' => '157'

    ];

    const WEBSITE_CODE_FR = 'fr';
    const WEBSITE_CODE_UK = 'uk';
    const WEBSITE_CODE_IT = 'it';
    const WEBSITE_CODE_DE = 'de';
    const WEBSITE_CODE_ES = 'es';
    const WEBSITE_CODE_CH = 'ch';
    const WEBSITE_CODE_NL = 'nl';
    const WEBSITE_CODE_LU = 'lu';
    const WEBSITE_CODE_BE = 'be';
    const WEBSITE_CODE_AT = 'at';
    const WEBSITE_CODE_IE = 'ie';
    const WEBSITE_CODE_PT = 'pt';
    const WEBSITE_CODE_MC = 'mc';
    const WEBSITE_CODE_GR = 'gr';

    const STORE_CODE_FR_FR = 'fr_fr';
    const STORE_CODE_UK_EN = 'uk_en';
    const STORE_CODE_IT_EN = 'it_en';
    const STORE_CODE_DE_EN = 'de_en';
    const STORE_CODE_ES_EN = 'es_en';
    const STORE_CODE_CH_EN = 'ch_en';
    const STORE_CODE_CH_FR = 'ch_fr';
    const STORE_CODE_NL_EN = 'nl_en';
    const STORE_CODE_LU_FR = 'lu_fr';
    const STORE_CODE_LU_EN = 'lu_en';
    const STORE_CODE_BE_FR = 'be_fr';
    const STORE_CODE_BE_EN = 'be_en';
    const STORE_CODE_AT_EN = 'at_en';
    const STORE_CODE_IE_EN = 'ie_en';
    const STORE_CODE_PT_EN = 'pt_en';
    const STORE_CODE_MC_FR = 'mc_fr';
    const STORE_CODE_MC_EN = 'mc_en';
    const STORE_CODE_GR_EN = 'gr_en';

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * UpgradeData constructor.
     * @param ConsoleOutput      $output
     */
    public function __construct(
        ConsoleOutput $output,
        ConfigSetupFactory $configSetupFactory,
        StoreManager $storeManager,
        StoreRepository $storeRepository
    ) {
        $this->output        = $output;
        $this->objectManager = ObjectManager::getInstance();
        $this->configSetupFactory = $configSetupFactory;
        $this->storeManager       = $storeManager;
        $this->storeRepository    = $storeRepository;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $this->output->writeln(""); // new line in console

        foreach (self::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Core setup : $version");

                $currentSetup = $this->getObjectManager()->create('Project\Core\Setup\Upgrade\Upgrade'.$fileData);
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ModuleDataSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return ConfigSetup
     */
    public function getConfigSetup()
    {
        return $this->configSetup;
    }

    /**
     * @return StoreManager
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * @param string $storeCode
     * @return int
     */
    public function getStoreId($storeCode)
    {
        return $this->getStoreManager()->getStore($storeCode)->getId();
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocale()
    {
        return [
            'fr_FR' => [
                $this->storeRepository->get(self::STORE_CODE_FR_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CH_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_FR)->getId()
            ],
            'en_GB' => [
                $this->storeRepository->get(self::STORE_CODE_CH_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_UK_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_DE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_ES_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_NL_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_AT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_PT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_GR_EN)->getId()
            ]
        ];
    }
}
