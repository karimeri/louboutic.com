<?php

namespace Project\Core\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreRepositoryFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\App\Config;
use Magento\Store\Model\WebsiteRepository;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Synolia\Standard\Setup\ShopSetupFactory;

/**
 * Class InstallData
 * @package   Project\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \Synolia\Standard\Setup\ShopSetup
     */
    protected $shopSetup;

    /**
     * @var \Magento\Store\Model\StoreRepository
     */
    protected $storeRepository;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * InstallData constructor.
     * @param Config                 $config
     * @param ShopSetupFactory       $shopSetupFactory
     * @param StoreRepositoryFactory $storeRepositoryFactory
     * @param ConfigSetupFactory     $configSetupFactory
     * @param WebsiteRepository      $websiteRepository
     */
    public function __construct(
        Config $config,
        ShopSetupFactory $shopSetupFactory,
        StoreRepositoryFactory $storeRepositoryFactory,
        ConfigSetupFactory $configSetupFactory,
        WebsiteRepository $websiteRepository
    ) {
        $this->config             = $config;
        $this->shopSetup          = $shopSetupFactory->create();
        $this->storeRepository    = $storeRepositoryFactory->create();
        $this->configSetupFactory = $configSetupFactory;
        $this->websiteRepository  = $websiteRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $this->config->clean();

        $setup->endSetup();
    }
}
