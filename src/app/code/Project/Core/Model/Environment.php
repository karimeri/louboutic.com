<?php

namespace Project\Core\Model;

/**
 * Class Environment
 *
 * @package Project\Core\Model
 * @author Synolia <contact@synolia.com>
 */
class Environment
{
    const EU = 'eu';
    const US = 'us';
    const HK = 'hk';
    const JP = 'jp';

    /**
     * Get an array of all environments
     * @return array
     */
    public static function getAll()
    {
        return [self::EU, self::US, self::HK, self::JP];
    }
}
