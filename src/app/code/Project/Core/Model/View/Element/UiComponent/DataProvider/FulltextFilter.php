<?php

namespace Project\Core\Model\View\Element\UiComponent\DataProvider;

use Magento\Framework\Api\Filter;
use Magento\Framework\Data\Collection;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\View\Element\UiComponent\DataProvider\FulltextFilter as MagentoFulltextFilter;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

/**
 * Class Fulltext
 */
class FulltextFilter extends MagentoFulltextFilter
{
    /**
     * Override to use older request without escaping characters
     *
     * @param Collection $collection
     * @param Filter $filter
     * @return void
     */
    public function apply(Collection $collection, Filter $filter)
    {
        if (!$collection instanceof AbstractDb) {
            throw new \InvalidArgumentException('Database collection required.');
        }

        /** @var SearchResult $collection */
        $mainTable = $collection->getMainTable();
        $columns = $this->getFulltextIndexColumns($collection, $mainTable);
        if (!$columns) {
            return;
        }

        $columns = $this->addTableAliasToColumns($columns, $collection, $mainTable);
        $collection->getSelect()
            ->where(
                'MATCH(' . implode(',', $columns) . ') AGAINST(?)',
                $filter->getValue()
            );
    }
}