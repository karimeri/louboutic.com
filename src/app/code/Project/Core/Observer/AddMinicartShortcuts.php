<?php

namespace Project\Core\Observer;

use Magento\Catalog\Block\ShortcutButtons;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Project\Core\Block\Checkout\Minicart\Shortcuts;
use Magento\Framework\View\Element\Html\Link;
use Magento\Customer\Model\Session;

/**
 * Class AddMinicartShortcuts
 * @package Project\Core\Observer
 * @author Synolia <contact@synolia.com>
 */
class AddMinicartShortcuts implements ObserverInterface
{
    /**
     * @var Session $session
     */
    protected $session;


    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        if (!$this->session->isLoggedIn() || $observer->getData('is_catalog_product')) {
            return;
        }

        /** @var ShortcutButtons $shortcutButtons */
        $shortcutButtons = $observer->getEvent()->getContainer();

        $layout = $shortcutButtons->getLayout();

        /** @var $links Shortcuts */
        $links = $layout->createBlock('Project\Core\Block\Checkout\Minicart\Shortcuts', 'minicart.account.shortcuts')
            ->setCssClass('extra-links');

        /** @var $ordersLinkBlock Link */
        $ordersLinkBlock = $layout->createBlock('Magento\Framework\View\Element\Html\Link')
            ->setData([
                'label' => __('Orders'),
                'path'  => 'sales/order/history',
                'class' => 'action customer-orders-link'
            ]);

        /** @var $logoutLinkBlock Link */
        $logoutLinkBlock = $layout->createBlock('Magento\Framework\View\Element\Html\Link')
            ->setData([
                'label' => __('Logout'),
                'path'  => 'customer/account/logout',
                'class' => 'action customer-logout-link'
            ]);

        $links->setChild('minicart.link.orders', $ordersLinkBlock);
        $links->setChild('minicart.link.logout', $logoutLinkBlock);

        $shortcutButtons->addShortcut($links);
    }
}
