<?php

namespace Project\Core\Framework\Data\Form;

class Element extends \Magento\Framework\Data\Form\Element\Multiline
{

    /**
     * Overrided to add address validated max-length rules
     * Get element HTML
     *
     * @return string
     */
    public function getElementHtml()
    {
        $html = '';
        $lineCount = $this->getLineCount();

        for ($i = 0; $i < $lineCount; $i++) {
            if ($i == 0 && $this->getRequired()) {
                $class = implode(' ', array_unique(explode(' ', 'input-text admin__control-text required-entry _required ' . $this->getClass())));
                $this->setClass($class);
            } else {
                $class =  str_replace('required-entry _required', '',implode(' ', array_unique(explode(' ', 'input-text admin__control-text ' . $this->getClass()))));
                $this->setClass($class);
            }
            $html .= '<div class="multi-input admin__field-control"><input id="' .
                $this->getHtmlId() .
                $i .
                '" name="' .
                $this->getName() .
                '[' .
                $i .
                ']' .
                '" value="' .
                $this->getEscapedValue(
                    $i
                ) . '" ' . $this->serialize(
                    $this->getHtmlAttributes()
                ) . '  ' . $this->_getUiId(
                    $i
                ) . '/>' . "\n";
            if ($i == 0) {
                $html .= $this->getAfterElementHtml();
            }
            $html .= '</div>';
        }
        return $html;
    }
}
