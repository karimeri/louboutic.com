<?php

namespace Project\Core\Framework\View\Result;

use Magento\Framework;
use Magento\Framework\View;
use Magento\Framework\Locale\Resolver as LocaleResolver;

/**
 * Class Page
 * @package Project\Core\Framework\View\Result
 * @author Synolia <contact@synolia.com>
 */
class Page extends \Magento\Framework\View\Result\Page
{
    /**
     * @var LocaleResolver
     */
    protected $localeResolver;

    /**
     * Page constructor.
     * @param View\Element\Template\Context $context
     * @param LocaleResolver $localeResolver
     * @param View\LayoutFactory $layoutFactory
     * @param View\Layout\ReaderPool $layoutReaderPool
     * @param Framework\Translate\InlineInterface $translateInline
     * @param View\Layout\BuilderFactory $layoutBuilderFactory
     * @param View\Layout\GeneratorPool $generatorPool
     * @param View\Page\Config\RendererFactory $pageConfigRendererFactory
     * @param View\Page\Layout\Reader $pageLayoutReader
     * @param string $template
     * @param bool $isIsolated
     * @param View\EntitySpecificHandlesList|null $entitySpecificHandlesList
     */
    public function __construct(
        View\Element\Template\Context $context,
        LocaleResolver $localeResolver,
        View\LayoutFactory $layoutFactory,
        View\Layout\ReaderPool $layoutReaderPool,
        Framework\Translate\InlineInterface $translateInline,
        View\Layout\BuilderFactory $layoutBuilderFactory,
        View\Layout\GeneratorPool $generatorPool,
        View\Page\Config\RendererFactory $pageConfigRendererFactory,
        View\Page\Layout\Reader $pageLayoutReader,
        $template,
        $isIsolated = false,
        View\EntitySpecificHandlesList $entitySpecificHandlesList = null
    ) {
        parent::__construct(
            $context,
            $layoutFactory,
            $layoutReaderPool,
            $translateInline,
            $layoutBuilderFactory,
            $generatorPool,
            $pageConfigRendererFactory,
            $pageLayoutReader,
            $template,
            $isIsolated,
            $entitySpecificHandlesList
        );
        
        $this->localeResolver = $localeResolver;
    }

    /**
     * Add default body classes for current page layout
     *
     * @return $this
     */
    protected function addDefaultBodyClasses()
    {
        parent::addDefaultBodyClasses();

        $this->getConfig()->addBodyClass($this->localeResolver->getLocale());

        return $this;
    }
}
