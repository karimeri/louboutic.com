<?php

namespace Project\Core\Manager;

use Magento\Framework\Filesystem\DirectoryList;
use Project\Core\Model\Environment;

/**
 * Class EnvironmentManager
 *
 * @package Project\Core\Manager
 * @author Synolia <contact@synolia.com>
 */
class EnvironmentManager
{
    const PROJECT_ENV_FILE = 'projectEnv.php';
    const PROJECT_ENV_VAR = 'PROJECT_ENV';

    /**
     * CoreInstallData constructor.
     *
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     *
     * @throws \Zend_Exception
     */
    public function __construct(
        DirectoryList $directoryList
    ) {
        \Zend_Loader::loadFile(self::PROJECT_ENV_FILE, $directoryList->getPath('etc'), true);
    }

    /**
     * Return current PROJECT_ENV
     * @return string
     * @throws \RuntimeException
     */
    public function getEnvironment()
    {
        // @codingStandardsIgnoreStart
        $environment = (string) \getenv(self::PROJECT_ENV_VAR);
        // @codingStandardsIgnoreEnd

        if (!\in_array($environment, Environment::getAll())) {
            throw new \RuntimeException('Environment ' . $environment . ' is not supported');
        }

        return $environment;
    }
}
