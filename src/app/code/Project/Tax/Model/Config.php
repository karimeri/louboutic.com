<?php

namespace Project\Tax\Model;

use Magento\Store\Model\Store;

/**
 * Class Config
 * @package Project\Tax\Model
 * @author Synolia <contact@synolia.com>
 */
class Config extends \Magento\Tax\Model\Config
{
    const XML_PATH_DISPLAY_MINICART_SUBTOTAL = 'louboutin_tax/minicart_display/subtotal';

    /**
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function displayMinicartSubtotalInclTax($store = null)
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_DISPLAY_MINICART_SUBTOTAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        ) == self::DISPLAY_TYPE_INCLUDING_TAX;
    }

    /**
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function displayMinicartSubtotalExclTax($store = null)
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_DISPLAY_MINICART_SUBTOTAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        ) == self::DISPLAY_TYPE_EXCLUDING_TAX;
    }

    /**
     * @param null|string|bool|int|Store $store
     * @return bool
     */
    public function displayMinicartSubtotalBoth($store = null)
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_DISPLAY_MINICART_SUBTOTAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        ) == self::DISPLAY_TYPE_BOTH;
    }
}
