<?php

namespace Project\Tax\CustomerData;

use \Project\Tax\Model\Config;
use Magento\Customer\CustomerData\JsLayoutDataProviderInterface;

/**
 * Class CheckoutTotalsJsLayoutDataProviderPlugin
 * @package Project\Tax\Plugin\CustomerData
 * @author Synolia <contact@synolia.com>
 */
class CheckoutTotalsJsLayoutDataProvider implements JsLayoutDataProviderInterface
{
    /**
     * @var \Project\Tax\Model\Config
     */
    protected $taxConfig;

    /**
     * CheckoutTotalsJsLayoutDataProviderPlugin constructor.
     * @param \Project\Tax\Model\Config $taxConfig
     */
    public function __construct(
        Config $taxConfig
    ) {
        $this->taxConfig = $taxConfig;
    }

    public function getData()
    {
        return [
            'components' => [
                'minicart_content' => [
                    'children' => [
                        'subtotal.container' => [
                            'children' => [
                                'subtotal' => [
                                    'children' => [
                                        'subtotal.totals' => [
                                            'config' => $this->getTotalsConfig(),
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getTotalsConfig()
    {
        return [
            'display_minicart_subtotal_incl_tax' => (int)$this->taxConfig->displayMinicartSubtotalInclTax(),
            'display_minicart_subtotal_excl_tax' => (int)$this->taxConfig->displayMinicartSubtotalExclTax(),
        ];
    }
}
