<?php

namespace Project\ConfigurableProduct\Model\Magento\ResourceModel\Product;

use Magento\ConfigurableProduct\Model\ResourceModel\Product\StockStatusBaseSelectProcessor as BaseSelectProcessor;
use Magento\Framework\DB\Select;

/**
 * Class StockStatusBaseSelectProcessor
 * @package Project\ConfigurableProduct\Model\Magento\ResourceModel\Product\StockStatusBaseSelectProcessor
 * @author Synolia <contact@synolia.com>
 */
class StockStatusBaseSelectProcessor extends BaseSelectProcessor
{
    /**
     * {@inheritdoc}
     */
    public function process(Select $select)
    {
        return $select;
    }
}
