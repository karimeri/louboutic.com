<?php

namespace Project\StoreSwitcher\Setup;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Recurring
 * @package Project\StoreSwitcher\Setup
 */
class Recurring implements InstallSchemaInterface
{
    const STORESWITCHER_ENABLE_CONFIG_PATH = 'synolia_storeswitcher/general/enable';
    const STORESWITCHER_FORCE_REDIRECT_CONFIG_PATH = 'synolia_storeswitcher/general/force_redirect';

    /**
     * @var ConfigInterface
     */
    protected $configResource;

    /**
     * Recurring constructor.
     * @param ConfigInterface $configResource
     */
    public function __construct(
        ConfigInterface $configResource
    )
    {
        $this->configResource = $configResource;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->updateStoresSwitcherConfig();
    }

    /**
     *  Update Synolia Stores Switcher
     *  Enable Config
     *  Don't force redirection
     */
    private function updateStoresSwitcherConfig()
    {
        $this->configResource->saveConfig(
            self::STORESWITCHER_ENABLE_CONFIG_PATH,
            1,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
        $this->configResource->saveConfig(
            self::STORESWITCHER_FORCE_REDIRECT_CONFIG_PATH,
            0,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
    }
}
