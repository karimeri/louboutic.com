<?php

namespace Project\StoreSwitcher\Model;

use Magento\Cms\Block\BlockFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Response\Http as ResponseHttp;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Locale\ListsInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\StoreSwitcher\Helper\Data as HelperStoreswitcher;
use Synolia\StoreSwitcher\Model\LocalisationServiceFactory;
use Synolia\StoreSwitcher\Model\Processor as BaseProcessor;
use Synolia\StoreSwitcher\Model\UrlRewriteRepository;

/**
 * Class Processor
 * @package Project\StoreSwitcher\Model
 * @author  Synolia <contact@synolia.com>
 */
class Processor extends BaseProcessor
{
    /**
     * Locale model
     *
     * @var ListsInterface
     */
    protected $localeLists;

    /**
     * Processor constructor.
     * @param \Magento\Framework\Model\Context                             $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Synolia\StoreSwitcher\Helper\Data                           $helperStoreSwitcher
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     * @param \Magento\Framework\App\Request\Http                          $request
     * @param \Magento\Framework\Json\Helper\Data                          $jsonHelperData
     * @param \Magento\Framework\App\Response\Http                         $response
     * @param \Synolia\Logger\Model\LoggerFactory                          $loggerFactory
     * @param \Magento\Framework\UrlInterface                              $urlBuilder
     * @param \Magento\Cms\Block\BlockFactory                              $blockFactory
     * @param \Synolia\StoreSwitcher\Model\LocalisationServiceFactory      $localisationServiceFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface           $scopeConfig
     * @param \Synolia\StoreSwitcher\Model\UrlRewriteRepository            $urlRewriteRepository
     * @param \Magento\Framework\Locale\ListsInterface                     $localeLists
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null           $resourceCollection
     * @param array                                                        $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        HelperStoreswitcher $helperStoreSwitcher,
        StoreManagerInterface $storeManager,
        Http $request,
        Data $jsonHelperData,
        ResponseHttp $response,
        LoggerFactory $loggerFactory,
        UrlInterface $urlBuilder,
        BlockFactory $blockFactory,
        LocalisationServiceFactory $localisationServiceFactory,
        ScopeConfigInterface $scopeConfig,
        UrlRewriteRepository $urlRewriteRepository,
        ListsInterface $localeLists,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $helperStoreSwitcher,
            $storeManager,
            $request,
            $jsonHelperData,
            $response,
            $loggerFactory,
            $urlBuilder,
            $blockFactory,
            $localisationServiceFactory,
            $scopeConfig,
            $urlRewriteRepository,
            $resource,
            $resourceCollection,
            $data
        );

        $this->localeLists = $localeLists;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return mixed
     */
    public function init()
    {
        $data = [
            'action' => null,
            'jsonConfig' => null,
            'redirect_url' => null
        ];

        $helper         = $this->helperStoreSwitcher;
        $configRedirect = $helper->getConfigRedirect();

        $this->initLogger();

        $this->log("___Start process___");

        if (!$this->shouldProcessCurrentObserver()) {
            $this->log("module not enable or bot or empty config");

            return $data;
        }

        $currentStoreId = $this->storeManager->getStore()->getId();
        $requestUrl     = $this->request->getServer('HTTP_REFERER');

        $data['jsonConfig'] = $this->prepareStoresData();
        //phpcs:ignore Ecg.Security.ForbiddenFunction.Found
        $this->log(print_r($data['jsonConfig'], true));

        $serviceName          = $this->helperStoreSwitcher->getLocalisationServiceName();

        /** @var \Synolia\GeoIp\Model\Service\GeoIpService $localisationService */
        $localisationService  = $this->localisationServiceFactory->create($serviceName);
        $localizedCountryCode = $localisationService->getCountryByIp();
        $localisationService->setCountryNameCookie();

        $localizedConfigRedirect = $helper->getRedirectConfigByCountryCode($configRedirect, $localizedCountryCode);

        $this->log("country code for current IP : ".$localizedCountryCode);

        $forceRedirectValue = $this->helperStoreSwitcher->getForceRedirect(); // ForceRedirect can have 0,1 or 2 value
        $baseUrlUnsecure    = $this->scopeConfig->getValue('web/unsecure/base_url');
        $baseUrlSecure      = $this->scopeConfig->getValue('web/secure/base_url');
        if ($forceRedirectValue === '1'
            || ($forceRedirectValue === '2' &&
                ($requestUrl == $baseUrlUnsecure
                    || $requestUrl == $baseUrlSecure
                )
            )
        ) {
            $this->log('Redirect forceredirect');
            $data['action']       = 'redirect';
            $data['redirect_url'] = $localizedConfigRedirect['external_url'];

            return $data;
        }

        $configRedirect = $this->helperStoreSwitcher->getConfigRedirect();

        $suggestedCountry = (string)$this->localeLists->getCountryTranslation($localizedCountryCode, 'en_US');

        if (isset($configRedirect[$localizedCountryCode]) &&
            $configRedirect[$localizedCountryCode]['store_id'] != $currentStoreId) {
            $this->log('Open popup with store id != current store');
            $data['action']           = 'openPopup';
            $data['suggestedCountry'] = $suggestedCountry;
            $data['currentCountry']   = $this->storeManager->getWebsite()->getName();
            $data['suggestedUrl']     = $configRedirect[$localizedCountryCode]['external_url'];
        }

        return $data;
    }
}
