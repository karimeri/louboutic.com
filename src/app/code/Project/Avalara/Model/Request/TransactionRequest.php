<?php

namespace Project\Avalara\Model\Request;

use Synolia\Avalara\Model\Avalara\AvaTaxClientFactory;
use Synolia\Avalara\Model\Avalara\TransactionBuilderFactory;
use Magento\Framework\Exception\LocalizedException;
use Synolia\Avalara\Helper\Config;
use Synolia\Avalara\Model\Request\Data\Document\LineDataBuilder;
use Synolia\Avalara\Model\Request\Data\Document\AddressDataBuilder;
use Synolia\Avalara\Model\Request\Data\Document\ShippingLineDataBuilder;
use Project\Avalara\Model\Request\Data\Document\CreditNoteLineDataBuilder;
use Project\Avalara\Model\Request\Data\Document\RepairFeesLineDataBuilder;
use Project\Avalara\Model\Request\Data\Document\ReturnChargeLineDataBuilder;
use Project\Avalara\Model\Request\Data\Document\ShippingFeesLineDataBuilder;

/**
 * Class TransactionRequest
 * @package Project\Avalara\Model\Request
 * @author Synolia <contact@synolia.com>
 */
class TransactionRequest extends \Synolia\Avalara\Model\Request\TransactionRequest
{
    /**
     * @var \Project\Avalara\Model\Request\Data\Document\CreditNoteLineDataBuilder
     */
    protected $creditNoteLineDataBuilder;

    /**
     * @var \Project\Avalara\Model\Request\Data\Document\RepairFeesLineDataBuilder
     */
    protected $repairFeesLineDataBuilder;

    /**
     * @var \Project\Avalara\Model\Request\Data\Document\ReturnChargeLineDataBuilder
     */
    protected $returnChargeDataBuilder;

    /**
     * @var \Project\Avalara\Model\Request\Data\Document\ShippingFeesLineDataBuilder
     */
    protected $shippingFeesLineDataBuilder;

    /**
     * TransactionRequest constructor.
     * @param \Synolia\Avalara\Model\Avalara\AvaTaxClientFactory $clientFactory
     * @param \Synolia\Avalara\Helper\Config $configHelper
     * @param \Synolia\Avalara\Model\Avalara\TransactionBuilderFactory $transactionBuilderFactory
     * @param \Synolia\Avalara\Model\Request\Data\Document\LineDataBuilder $lineDataBuilder
     * @param \Synolia\Avalara\Model\Request\Data\Document\AddressDataBuilder $addressDataBuilder
     * @param \Synolia\Avalara\Model\Request\Data\Document\ShippingLineDataBuilder $shippingLineDataBuilder
     * @param \Project\Avalara\Model\Request\Data\Document\CreditNoteLineDataBuilder $creditNoteLineDataBuilder
     * @param \Project\Avalara\Model\Request\Data\Document\RepairFeesLineDataBuilder $repairFeesLineDataBuilder
     * @param \Project\Avalara\Model\Request\Data\Document\ReturnChargeLineDataBuilder $returnChargeDataBuilder
     * @param \Project\Avalara\Model\Request\Data\Document\ShippingFeesLineDataBuilder $shippingFeesLineDataBuilder
     * @param string $type
     * @param string $customerCode
     */
    public function __construct(
        AvaTaxClientFactory $clientFactory,
        Config $configHelper,
        TransactionBuilderFactory $transactionBuilderFactory,
        LineDataBuilder $lineDataBuilder,
        AddressDataBuilder $addressDataBuilder,
        ShippingLineDataBuilder $shippingLineDataBuilder,
        CreditNoteLineDataBuilder $creditNoteLineDataBuilder,
        RepairFeesLineDataBuilder $repairFeesLineDataBuilder,
        ReturnChargeLineDataBuilder $returnChargeDataBuilder,
        ShippingFeesLineDataBuilder $shippingFeesLineDataBuilder,
        $type,
        $customerCode
    ) {
        parent::__construct(
            $clientFactory,
            $configHelper,
            $transactionBuilderFactory,
            $lineDataBuilder,
            $addressDataBuilder,
            $shippingLineDataBuilder,
            $type,
            $customerCode
        );

        $this->creditNoteLineDataBuilder = $creditNoteLineDataBuilder;
        $this->repairFeesLineDataBuilder = $repairFeesLineDataBuilder;
        $this->returnChargeDataBuilder= $returnChargeDataBuilder;
        $this->shippingFeesLineDataBuilder = $shippingFeesLineDataBuilder;
    }

    /**
     * Add credit note line to transaction request
     *
     * @param $item
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addCreditNoteLine($item)
    {
        try {
            $lineItemModel = $this->creditNoteLineDataBuilder->getDataModel($item);

            $this->transactionBuilder->withLine(
                $lineItemModel->amount,
                $lineItemModel->quantity,
                $lineItemModel->itemCode,
                $lineItemModel->taxCode
            );
        } catch (\Exception $exception) {
            throw new LocalizedException(
                __('Fail to add credit note line in request : %1', $exception->getMessage())
            );
        }

        return $this;
    }

    /**
     * Add repair fees line to transaction request
     *
     * @param $item
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addRepairFeesLine($item)
    {
        try {
            $lineItemModel = $this->repairFeesLineDataBuilder->getDataModel($item);

            $this->transactionBuilder->withLine(
                $lineItemModel->amount,
                $lineItemModel->quantity,
                $lineItemModel->itemCode,
                $lineItemModel->taxCode
            );
        } catch (\Exception $exception) {
            throw new LocalizedException(
                __('Fail to add repair fees line in request : %1', $exception->getMessage())
            );
        }

        return $this;
    }

    /**
     * Add return charge line to transaction request
     *
     * @param $item
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addReturnChargeLine($item)
    {
        try {
            $lineItemModel = $this->returnChargeDataBuilder->getDataModel($item);

            $this->transactionBuilder->withLine(
                $lineItemModel->amount,
                $lineItemModel->quantity,
                $lineItemModel->itemCode,
                $lineItemModel->taxCode
            );
        } catch (\Exception $exception) {
            throw new LocalizedException(
                __('Fail to add return charge line in request : %1', $exception->getMessage())
            );
        }

        return $this;
    }

    /**
     * Add shipping fees line to transaction request
     *
     * @param $item
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addShippingFeesLine($item)
    {
        try {
            $lineItemModel = $this->shippingFeesLineDataBuilder->getDataModel($item);

            $this->transactionBuilder->withLine(
                $lineItemModel->amount,
                $lineItemModel->quantity,
                $lineItemModel->itemCode,
                $lineItemModel->taxCode
            );
        } catch (\Exception $exception) {
            throw new LocalizedException(
                __('Fail to add shipping fees line in request : %1', $exception->getMessage())
            );
        }

        return $this;
    }
}
