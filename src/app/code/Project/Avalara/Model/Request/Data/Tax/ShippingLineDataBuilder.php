<?php

namespace Synolia\Avalara\Model\Request\Data\Tax;

use Avalara\LineItemModel;
use Magento\Tax\Helper\Data;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;
use Synolia\Avalara\Helper\Tax;
use Synolia\Avalara\Api\Data\Transaction\ShippingLineDataBuilderInterface;

/**
 * Class ShippingLineDataBuilder
 * @package Synolia\Avalara\Model\Request\Data\Tax
 * @author Synolia <contact@synolia.com>
 */
class ShippingLineDataBuilder implements ShippingLineDataBuilderInterface
{
    /**
     * @var \Synolia\Avalara\Helper\Tax
     */
    protected $synoTaxHelper;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    protected $magentoTaxHelper;

    /**
     * ShippingLineDataBuilder constructor.
     *
     * @param \Synolia\Avalara\Helper\Tax $synoTaxHelper
     * @param \Magento\Tax\Helper\Data $magentoTaxHelper
     */
    public function __construct(
        Tax $synoTaxHelper,
        Data $magentoTaxHelper
    ) {
        $this->synoTaxHelper = $synoTaxHelper;
        $this->magentoTaxHelper = $magentoTaxHelper;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\Rate $rate
     *
     * @return \Avalara\LineItemModel
     */
    public function getDataModel($rate)
    {
        $dataModel = new LineItemModel();

        $store = $rate->getAddress()->getQuote()->getStore();

        $dataModel->number = null;
        $dataModel->quantity = 1;
        $dataModel->amount = $rate->getPrice();
        $dataModel->addresses = null;
        $dataModel->taxCode = $this->synoTaxHelper->getTaxCode($this->magentoTaxHelper->getShippingTaxClass($store));
        $dataModel->customerUsageType = null;
        $dataModel->entityUseCode = null;
        $dataModel->itemCode = CommonTaxCollector::ITEM_CODE_SHIPPING;
        $dataModel->exemptionCode = null;
        $dataModel->discounted = null;
        $dataModel->taxIncluded = null;
        $dataModel->revenueAccount = null;
        $dataModel->ref1 = null;
        $dataModel->ref2 = null;
        $dataModel->description = null;
        $dataModel->businessIdentificationNo = null;
        $dataModel->taxOverride = null;
        $dataModel->parameters = null;
        $dataModel->hsCode = null;

        return $dataModel;
    }
}
