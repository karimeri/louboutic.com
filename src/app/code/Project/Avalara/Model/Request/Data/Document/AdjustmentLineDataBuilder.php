<?php

namespace Project\Avalara\Model\Request\Data\Document;

use Avalara\LineItemModel;
use Synolia\Avalara\Helper\Tax;
use Project\Erp\Helper\Config\Y2\Sales;
use Project\Erp\Helper\Config;

use Synolia\Avalara\Api\Data\DataBuilderInterface;

/**
 * Class AdjustmentLineDataBuilder
 * @package Synolia\Project\Model\Request\Data\Tax
 * @author Synolia <contact@synolia.com>
 */
abstract class AdjustmentLineDataBuilder implements DataBuilderInterface
{
    /**
     * @var \Synolia\Avalara\Helper\Tax
     */
    protected $synoTaxHelper;

    /**
     * @var \Project\Erp\Helper\Config\Y2\Sales
     */
    protected $erpHelper;

    /**
     * @var \Project\Erp\Helper\Config
     */
    protected $configHelper;

    /**
     * AdjustmentLineDataBuilder constructor.
     *
     * @param \Synolia\Avalara\Helper\Tax $synoTaxHelper
     * @param \Project\Erp\Helper\Config\Y2\Sales $erpHelper
     * @param \Project\Erp\Helper\Config $configHelper
     */
    public function __construct(
        Tax $synoTaxHelper,
        Sales $erpHelper,
        Config $configHelper
    ) {
        $this->synoTaxHelper = $synoTaxHelper;
        $this->erpHelper = $erpHelper;
        $this->configHelper = $configHelper;
    }

    /**
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $document
     *
     * @return \Avalara\LineItemModel
     */
    public function getDataModel($document)
    {
        $dataModel = new LineItemModel();

        $storeId = $document->getStoreId();

        $dataModel->number = null;
        $dataModel->quantity = 1;
        $dataModel->amount = null;
        $dataModel->addresses = null;
        $dataModel->taxCode = $this->synoTaxHelper->getTaxCode(
            $this->configHelper->getCreditmemoAdjustTaxClass($storeId)
        );
        $dataModel->customerUsageType = null;
        $dataModel->entityUseCode = null;
        $dataModel->itemCode = null;
        $dataModel->exemptionCode = null;
        $dataModel->discounted = null;
        $dataModel->taxIncluded = null;
        $dataModel->revenueAccount = null;
        $dataModel->ref1 = null;
        $dataModel->ref2 = null;
        $dataModel->description = null;
        $dataModel->businessIdentificationNo = null;
        $dataModel->taxOverride = null;
        $dataModel->parameters = null;
        $dataModel->hsCode = null;

        return $dataModel;
    }
}
