<?php

namespace Project\Avalara\Model\Request\Data\Document;

use Avalara\LineItemModel;

/**
 * Class ReturnChargeLineDataBuilder
 * @package Synolia\Project\Model\Request\Data\Tax
 * @author Synolia <contact@synolia.com>
 */
class ReturnChargeLineDataBuilder extends AdjustmentLineDataBuilder
{
    /**
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $document
     *
     * @return \Avalara\LineItemModel
     */
    public function getDataModel($document)
    {
        $storeId = $document->getStoreId();

        $dataModel = parent::getDataModel($document);
        $dataModel->amount = $document->getBaseAdjustmentReturnCharge();
        $dataModel->itemCode = $this->erpHelper->getY2VirtualReferenceChargereturn($storeId);

        return $dataModel;
    }
}