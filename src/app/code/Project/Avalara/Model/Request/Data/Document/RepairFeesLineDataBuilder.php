<?php

namespace Project\Avalara\Model\Request\Data\Document;

use Avalara\LineItemModel;

/**
 * Class RepairFeesLineDataBuilder
 * @package Synolia\Project\Model\Request\Data\Tax
 * @author Synolia <contact@synolia.com>
 */
class RepairFeesLineDataBuilder extends AdjustmentLineDataBuilder
{
    /**
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $document
     *
     * @return \Avalara\LineItemModel
     */
    public function getDataModel($document)
    {
        $storeId = $document->getStoreId();

        $dataModel = parent::getDataModel($document);
        $dataModel->amount = $document->getBaseAdjustmentRepairingFees();
        $dataModel->itemCode = $this->erpHelper->getY2VirtualReferenceRepair($storeId);

        return $dataModel;
    }
}
