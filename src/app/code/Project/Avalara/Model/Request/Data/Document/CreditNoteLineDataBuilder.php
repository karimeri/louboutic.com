<?php

namespace Project\Avalara\Model\Request\Data\Document;

use Avalara\LineItemModel;

/**
 * Class CreditNoteLineDataBuilder
 * @package Synolia\Project\Model\Request\Data\Tax
 * @author Synolia <contact@synolia.com>
 */
class CreditNoteLineDataBuilder extends AdjustmentLineDataBuilder
{
    /**
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $document
     *
     * @return \Avalara\LineItemModel
     */
    public function getDataModel($document)
    {
        $storeId = $document->getStoreId();

        $dataModel = parent::getDataModel($document);
        $dataModel->amount = -(float)$document->getBaseAdjustmentCreditNote();
        $dataModel->itemCode = $this->erpHelper->getY2VirtualReferenceCreditnote($storeId);

        return $dataModel;
    }
}
