<?php

namespace Project\Avalara\Model\Request\Transaction;

use Avalara\TransactionAddressType;
use Magento\Store\Model\StoreManagerInterface;
use Synolia\Avalara\Helper\Config;
use Synolia\Avalara\Helper\Tax;
use Project\Avalara\Model\Request\TransactionRequestFactory;

/**
 * Class DocumentRequest
 * Avalara Request object using magento native objects
 * @package Project\Avalara\Model\Request\Transaction
 * @author Synolia <contact@synolia.com>
 */
class DocumentRequest extends \Synolia\Avalara\Model\Request\Transaction\DocumentRequest
{
    /**
     * @var \Project\Avalara\Model\Request\TransactionRequestFactory
     */
    protected $transactionRequestFactory;

    /**
     * @var \Synolia\Avalara\Helper\Tax
     */
    protected $taxHelper;

    /**
     * @var \Synolia\Avalara\Helper\Config $config
     */
    protected $config;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * DocumentRequest constructor.
     * @param \Project\Avalara\Model\Request\TransactionRequestFactory $transactionRequestFactory
     * @param \Synolia\Avalara\Helper\Tax $taxHelper
     * @param \Synolia\Avalara\Helper\Config $config
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        TransactionRequestFactory $transactionRequestFactory,
        Tax $taxHelper,
        Config $config,
        StoreManagerInterface $storeManager
    ) {
        $this->transactionRequestFactory = $transactionRequestFactory;
        $this->taxHelper = $taxHelper;
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * @return \Avalara\TransactionModel
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getResult()
    {
        if (!$this->result) {
            $websiteId = $this->storeManager->getStore($this->document->getStoreId())->getWebsiteId();
            /** @var \Project\Avalara\Model\Request\TransactionRequest $transactionRequest */
            $transactionRequest = $this->getTransactionRequest();

            $transactionRequest->addTransactionCode($this->document->getIncrementId());
            $transactionRequest->addAddressModel(
                TransactionAddressType::C_SHIPFROM,
                $this->taxHelper->getOriginAddressModel($websiteId)
            );
            $transactionRequest->addAddress(TransactionAddressType::C_SHIPTO, $this->document->getShippingAddress());

            foreach ($this->document->getItems() as $invoiceItem) {
                $transactionRequest->addLine($invoiceItem);
            }

            if ($this->type === "ReturnInvoice") {
                $transactionRequest->addShippingFeesLine($this->document);
                $transactionRequest->addRepairFeesLine($this->document);
                $transactionRequest->addCreditNoteLine($this->document);
                $transactionRequest->addReturnChargeLine($this->document);
            } else {
                $transactionRequest->addShippingLine($this->document);
            }

            if ($this->config->getCommitTransaction()) {
                $transactionRequest->commit();
            }

            $transactionRequest->createTransaction();
        }

        return $this->result;
    }
}
