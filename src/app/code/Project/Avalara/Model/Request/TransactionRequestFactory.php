<?php

namespace Project\Avalara\Model\Request;

/**
 * Class TransactionRequestFactory
 * @package Project\Avalara\Model\Request
 */
class TransactionRequestFactory extends \Synolia\Avalara\Model\Request\TransactionRequestFactory
{
    /**
     * Create class instance with specified parameters
     *
     * @param null|string $type
     * @param array $data
     *
     * @return \Synolia\Avalara\Model\Request\TransactionRequest
     */
    public function create($type = null, array $data = array())
    {
        if ($type === null) {
            $type = TransactionRequest::class;
        }

        return $this->objectManager->create($type, $data);
    }
}
