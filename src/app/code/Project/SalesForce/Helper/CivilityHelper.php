<?php

namespace Project\SalesForce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class CivilityHelper
 *
 * @package Project\SalesForce\Helper
 * @author Synolia <contact@synolia.com>
 */
class CivilityHelper extends AbstractHelper
{
    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * CivilityHelper constructor.
     *
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        EnvironmentManager $environmentManager,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->environmentManager = $environmentManager;
        parent::__construct($context);
    }

    /**
     * @param int $civility
     *
     * @return string
     * @throws \RuntimeException
     */
    public function getCivility($civility)
    {
        $civilityConverted = '';

        $environment = $this->environmentManager->getEnvironment();

        $civilityConverted = 'MR';
        if ($civility == 2) {
            $civilityConverted = 'MS';
        }

        if (Environment::JP == $environment) {
            if ($civility == 2) {
                $civilityConverted = '女性';
            } else {
                $civilityConverted = '男性';
            }
        }

        return $civilityConverted;
    }
}
