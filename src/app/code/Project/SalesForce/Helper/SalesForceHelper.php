<?php

namespace Project\SalesForce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Newsletter\Model\Subscriber;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class SalesForceHelper
 *
 * @package Project\SalesForce\Helper
 * @author Synolia <contact@synolia.com>
 */
class SalesForceHelper extends AbstractHelper
{
    const XML_PATH_LOUBOUTIN_SALESFORCE = 'salesforce';
    const NEWSLETTER_IMPORT_EXPORT      = 'newsletter_importexport';
    const NEWSLETTER_SOURCE_CODE        = 'source_code';

    protected static $availableSource = ['Eu', 'Us', 'Jp', 'Asia'];
    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * SalesForceHelper constructor.
     *
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        EnvironmentManager $environmentManager,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->environmentManager = $environmentManager;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function isImportExportEnabled()
    {
        return (bool) $this->getConfig(static::NEWSLETTER_IMPORT_EXPORT);
    }

    /**
     * @return mixed
     */
    public function getSourceCode()
    {
        return $this->getConfig(static::NEWSLETTER_SOURCE_CODE);
    }

    /**
     * @param $source
     *
     * @return bool
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function isValidSource($source)
    {
        if (!\in_array($source, static::$availableSource)) {
            throw new \UnexpectedValueException('Unknow source provided : ' . $source);
        }

        $environment = $this->environmentManager->getEnvironment();

        $exceptionMessage = 'Source ' . $source . ' is not equal of the current environment ' . $environment;
        switch ($source) {
            case 'Eu':
                if (Environment::EU !== $environment) {
                    throw new \UnexpectedValueException($exceptionMessage);
                }
                break;
            case 'Us':
                if (Environment::US !== $environment) {
                    throw new \UnexpectedValueException($exceptionMessage);
                }
                break;
            case 'Jp':
                if (Environment::JP !== $environment) {
                    throw new \UnexpectedValueException($exceptionMessage);
                }
                break;
            case 'Asia':
                if (Environment::HK !== $environment) {
                    throw new \UnexpectedValueException($exceptionMessage);
                }
                break;
        }

        return true;
    }

    /**
     * @param string $status
     *
     * @return string
     */
    public function getSubscriberStatus($status)
    {
        $statusConverted = '';

        switch ($status) {
            case Subscriber::STATUS_SUBSCRIBED:
                $statusConverted = 'Subscribed';
                break;
            case Subscriber::STATUS_NOT_ACTIVE:
                $statusConverted = 'Not Activated';
                break;
            case Subscriber::STATUS_UNSUBSCRIBED:
                $statusConverted = 'Unsubscribed';
                break;
            case Subscriber::STATUS_UNCONFIRMED:
                $statusConverted = 'UnConfirmed';
                break;
        }

        return $statusConverted;
    }

    /**
     * Return correct source for salesForce depending current environment
     *
     * @return string
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    public function getSource()
    {
        $environment = $this->environmentManager->getEnvironment();

        switch ($environment) {
            case Environment::EU:
            case Environment::JP:
            case Environment::US:
                return \ucfirst($environment);
            case Environment::HK:
                return 'Asia';
            default:
                throw new \UnexpectedValueException('PROJECT_ENV is not correctly set');
        }
    }

    /**
     * Return unique string
     *
     * @return string
     */
    public function getUniq($limit)
    {
        // @codingStandardsIgnoreLine
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    /**
     * @param string $configName
     * @param string $configGroup
     *
     * @return mixed
     */
    protected function getConfig($configName, $configGroup = 'general')
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_LOUBOUTIN_SALESFORCE . "/$configGroup/$configName"
        );
    }
}
