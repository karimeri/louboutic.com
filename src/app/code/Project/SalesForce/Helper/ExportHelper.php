<?php

namespace Project\SalesForce\Helper;

use Synolia\Sync\Helper\Config;

/**
 * Class ExportHelper
 *
 * @package Project\SalesForce\Helper
 */
class ExportHelper
{
    const LOCAL_PATH_CURRENT = 'out/salesforce/subscriber/current';
    const LOCAL_PATH_ARCHIVE = 'out/salesforce/subscriber/archive/';

    /**
     * @param string $path
     * @return string
     */
    public function getFullPath($path)
    {
        return \BP . Config::BASE_PATH_SYNC_FILE . $path;
    }
}
