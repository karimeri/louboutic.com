<?php

namespace Project\SalesForce\Manager;

use Magento\Newsletter\Model\Subscriber;
use Project\SalesForce\Model\SalesForceSubscriber;
use Project\SalesForce\Model\SalesForceSubscriberFactory;

/**
 * Class SubscriptionManager
 *
 * @package Project\SalesForce\Manager
 * @author Synolia <contact@synolia.com>
 */
class SubscriptionManager
{
    /**
     * @var \Project\SalesForce\Model\SalesForceSubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * SubscriptionManager constructor.
     *
     * @param \Project\SalesForce\Model\SalesForceSubscriberFactory $subscriberFactory
     */
    public function __construct(
        SalesForceSubscriberFactory $subscriberFactory
    ) {
        $this->subscriberFactory = $subscriberFactory;
    }

    /**
     * Unsubscribe user then change date if user subscribe/unsubscribe in same day to not export it again
     * @param array $emails
     * @throws \Exception
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     */
    public function unsubscribeFromMailList(array $emails)
    {
        $date = (new \DateTime())->modify('-1 day');
        foreach ($emails as $email) {
            $subscriber = $this->subscriberFactory->create();
            $subscriber->loadByEmail($email);

            if ($subscriber->isSubscribed()) {
                $dateSubscriber = new \DateTime($subscriber->getChangeStatusAt());
                if ($dateSubscriber->format('Y-m-d') == $date->format('Y-m-d')) {
                    $subscriber->setChangeStatusAt($dateSubscriber->modify('-1 day')->format('Y-m-d 00:00:00'));
                }
                $subscriber->setStatus(Subscriber::STATUS_UNSUBSCRIBED);
                $subscriber->save();
            }
        }
    }
}
