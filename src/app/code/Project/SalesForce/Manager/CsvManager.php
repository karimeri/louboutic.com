<?php

namespace Project\SalesForce\Manager;

use League\Csv\Reader;
use League\Csv\Writer;
use Project\SalesForce\Helper\SalesForceHelper;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class CsvManager
 *
 * @package Project\SalesForce\Manager
 * @author Synolia <contact@synolia.com>
 */
class CsvManager
{
    /**
     * @var \Project\SalesForce\Helper\SalesForceHelper
     */
    protected $salesForceHelper;

    /**
     * CsvManager constructor.
     *
     * @param \Project\SalesForce\Helper\SalesForceHelper $salesForceHelper
     */
    public function __construct(
        SalesForceHelper $salesForceHelper
    ) {
        $this->salesForceHelper = $salesForceHelper;
    }

    /**
     * Parse a Bounce CSV File and return an array of email to unsubscribe
     *
     * @param \Symfony\Component\Finder\SplFileInfo $file
     *
     * @return array
     * @throws \League\Csv\Exception
     */
    public function processBounceFile(SplFileInfo $file)
    {
        /** @var Reader $reader */
        $reader = Reader::createFromPath($file->getRealPath());
        $reader->setDelimiter(';');
        $reader = $this->addBounceHeaderToCsv($reader);

        $records = $reader->getRecords();
        $emails  = $this->parseRecordsToGetEmails($records);

        return $emails;
    }

    /**
     * Parse a Unsubscribe CSV File and return an array of email to unsubscribe
     *
     * @param \Symfony\Component\Finder\SplFileInfo $file
     *
     * @return array
     * @throws \League\Csv\Exception
     */
    public function processUnsubscribeFile(SplFileInfo $file)
    {
        /** @var Reader $reader */
        $reader = Reader::createFromPath($file->getRealPath());
        $reader->setDelimiter(';');
        $reader = $this->addUnsubscribeHeaderToCsv($reader);

        $records = $reader->getRecords();
        $emails  = $this->parseRecordsToGetEmails($records);

        return $emails;
    }

    /**
     * @param \Iterator $records
     *
     * @return array
     */
    private function parseRecordsToGetEmails(\Iterator $records)
    {
        $emails  = [];
        foreach ($records as $record) {
            try {
                if ($this->salesForceHelper->isValidSource($record['source'])) {
                    $emails[] = $record['email'];
                }
            } catch (\Throwable $throwable) {
                // Simply ignore this line
            }
        }

        return $emails;
    }

    /**
     * @param \League\Csv\Reader $reader
     *
     * @return \League\Csv\AbstractCsv|\League\Csv\Reader|static
     * @throws \League\Csv\Exception
     * @throws \TypeError
     */
    private function addBounceHeaderToCsv(Reader $reader)
    {
        $wantedHeaders = [
            'ignore_1',
            'ignore_2',
            'ignore_3',
            'ignore_4',
            'ignore_5',
            'source',
            'email'
        ];

        $tmp = new \SplTempFileObject();
        $writer = Writer::createFromFileObject($tmp);
        $writer->insertOne($wantedHeaders);
        $writer->insertAll($reader->getRecords());

        $reader = Reader::createFromFileObject($tmp);
        $reader->setHeaderOffset(0);

        return $reader;
    }

    /**
     * @param \League\Csv\Reader $reader
     *
     * @return \League\Csv\AbstractCsv|\League\Csv\Reader|static
     * @throws \League\Csv\Exception
     * @throws \TypeError
     */
    private function addUnsubscribeHeaderToCsv(Reader $reader)
    {
        $wantedHeaders = [
            'ignore_1',
            'ignore_2',
            'source',
            'email',
            'ignore_3',
            'ignore_4'
        ];

        $tmp = new \SplTempFileObject();

        $writer = Writer::createFromFileObject($tmp);
        $writer->insertOne($wantedHeaders);
        $writer->insertAll($reader->getRecords());

        $reader = Reader::createFromFileObject($tmp);
        $reader->setHeaderOffset(0);

        return $reader;
    }
}
