<?php

namespace Project\SalesForce\Manager;

use League\Csv\Writer;
use Magento\Newsletter\Model\Subscriber;
use Project\SalesForce\Helper\SalesForceHelper;
use Project\SalesForce\Helper\CivilityHelper;
use Project\SalesForce\Helper\ExportHelper;
use Symfony\Component\Filesystem\Filesystem;
use Zend_Locale;

/**
 * Class ExportCsvManager
 *
 * @package Project\SalesForce\Manager
 * @author Synolia <contact@synolia.com>
 */
class ExportCsvManager
{
    /**
     * @var SalesForceHelper
     */
    protected $salesForceHelper;

    /**
     * @var CivilityHelper
     */
    protected $civilityHelper;

    /**
     * @var ExportHelper
     */
    protected $exportHelper;

    /**
     * @var string
     */
    protected $filepath;

    /**
     * ExportCsvManager constructor.
     *
     * @param SalesForceHelper $salesForceHelper
     * @param CivilityHelper $civilityHelper
     * @param ExportHelper $exportHelper
     */
    public function __construct(
        SalesForceHelper $salesForceHelper,
        CivilityHelper $civilityHelper,
        ExportHelper $exportHelper
    ) {
        $this->salesForceHelper = $salesForceHelper;
        $this->civilityHelper = $civilityHelper;
        $this->exportHelper = $exportHelper;
    }

    /**
     * @param $datas
     * @return string
     * @throws \League\Csv\Exception
     */
    public function processNewsletterFile($datas)
    {
        $filename = $this->createFile();
        $this->fillCsv($datas);

        $replace = str_replace('"', "", file_get_contents($this->filepath) );
        file_put_contents($this->filepath, $replace);
        return $filename;
    }

    /**
     * @return string
     */
    private function createFile()
    {
        $date = new \DateTime();
        $path = $this->exportHelper->getFullPath($this->exportHelper::LOCAL_PATH_CURRENT);
        $filename = $date->format('YmdHis').'-'.$this->salesForceHelper->getUniq(10).'.csv';
        $this->filepath = $path.\DIRECTORY_SEPARATOR.$filename;
        $fs = new Filesystem();
        try {
            $fs->mkdir($path, 0775);
            $fs->touch($this->filepath);
        } catch (\Exception $exception) {
            $exception->getMessage();
        }

        return $filename;
    }

    /**
     * @param $datas
     * @throws \League\Csv\Exception
     * @throws \Zend_Locale_Exception
     */
    private function fillCsv($datas)
    {
        $newsletterData = $this->getNewsletterData($datas);

        $csv = Writer::createFromPath($this->filepath);
        $csv->setDelimiter(';');
        $csv->setNewline("\r\n");
        $csv->insertAll($newsletterData);
    }

    /**
     * @param $datas
     * @return array
     * @throws \Zend_Locale_Exception
     */
    private function getNewsletterData($datas)
    {
        $newsletterData[] = [
            'ID',
            'Email',
            'Type',
            'Gender',
            'First Name',
            'Last Name',
            'Status',
            'Website',
            'Store',
            'Store View',
            'Telephone',
            'Prefix',
            'Country',
            'Newsletter',
            'Created at',
            'Source'
        ];
        /** @var Subscriber $data */
        foreach ($datas as $data) {
            $locale = new Zend_Locale($data->getData('storeCode'));
            if (!\is_null($locale)) {
                $countries = $locale->getTranslationList('Territory', $locale->getLanguage(), 2);
            }
            $changeStatusDate = new \DateTime($data->getChangeStatusAt());
            $newsletterData [] = [
                $data->getSubscriberId(),
                $data->getSubscriberEmail(),
                ($data->getCustomerId()) ? 'Customer' : 'Guest',
                $data->getData('subscriber_collection_gender') == 1 ? 'men' : 'women',
                $data->getData('firstname'),
                $data->getdata('lastname'),
                $this->salesForceHelper->getSubscriberStatus($data->getSubscriberStatus()),
                $data->getData('websiteName'),
                strtoupper($data->getData('groupCode')),
                $data->getData('storeName'),
                $data->getData('telephone'),
                $this->civilityHelper->getCivility((int)$data->getData('gender')),
                (isset($countries[\strtoupper($data->getData('websiteCode'))])) ?
                    $countries[\strtoupper($data->getData('websiteCode'))] : '',
                'all (commerce & beauty)',
                strtoupper($changeStatusDate->format('M d Y')),
                $this->salesForceHelper->getSourceCode(),
            ];
        }

        return $newsletterData;
    }
}
