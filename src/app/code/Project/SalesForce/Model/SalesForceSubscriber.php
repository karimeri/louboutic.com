<?php

namespace Project\SalesForce\Model;

use Magento\Newsletter\Model\Subscriber;

/**
 * Class SalesForceSubscriber
 *
 * @package Project\SalesForce\Model
 * @author Synolia <contact@synolia.com>
 */
class SalesForceSubscriber extends Subscriber
{
    /**
     * Overrided to avoid altering changed date when we disable subscription.
     *
     * Used Only in subscriptionManager, and should never be used elsewhere
     *
     * @return $this
     */
    public function beforeSave()
    {
        return $this;
    }
}
