<?php

namespace Project\SalesForce\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Project\SalesForce\Helper\SalesForceHelper;
use Project\SalesForce\Manager\CsvManager;
use Project\SalesForce\Manager\SubscriptionManager;
use Symfony\Component\Console\Output\ConsoleOutput as SymfonyOutput;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Synolia\Sync\Helper\Config;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class AbstractSalesForceImport
 *
 * @package Project\SalesForce\Action
 * @author Synolia <contact@synolia.com>
 */
abstract class AbstractSalesForceImport implements ActionInterface
{
    /**
     * Location of file to import in /var/sync
     */
    const FILE_LOCATION = '';
    const FILE_ARCHIVE_LOCATION = '';
    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $output;
    /**
     * @var \Project\SalesForce\Manager\CsvManager
     */
    protected $csvManager;
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $filesystem;
    /**
     * @var \Project\SalesForce\Manager\SubscriptionManager
     */
    protected $subscriptionManager;
    /**
     * @var \Project\SalesForce\Helper\SalesForceHelper
     */
    protected $salesForceHelper;

    /**
     * AbstractSalesForceImport constructor.
     *
     * @param \Project\SalesForce\Manager\CsvManager $csvManager
     * @param \Symfony\Component\Console\Output\ConsoleOutput $output
     * @param \Symfony\Component\Filesystem\Filesystem $filesystem
     * @param \Project\SalesForce\Manager\SubscriptionManager $subscriptionManager
     * @param \Magento\Framework\App\State $state
     * @param \Project\SalesForce\Helper\SalesForceHelper $salesForceHelper
     */
    public function __construct(
        CsvManager $csvManager,
        SymfonyOutput $output,
        Filesystem $filesystem,
        SubscriptionManager $subscriptionManager,
        State $state,
        SalesForceHelper $salesForceHelper
    ) {
        $this->csvManager          = $csvManager;
        $this->output              = $output;
        $this->filesystem          = $filesystem;
        $this->subscriptionManager = $subscriptionManager;
        $this->salesForceHelper = $salesForceHelper;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @return \IteratorAggregate
     */
    protected function getFilesToImport()
    {
        $finder = new Finder();
        $finder->files()->in(\BP . Config::BASE_PATH_SYNC_FILE . static::FILE_LOCATION);

        return $finder;
    }

    /**
     * @param \Symfony\Component\Finder\SplFileInfo $file
     */
    protected function archiveFile(SplFileInfo $file)
    {
        $archivePath = \BP . Config::BASE_PATH_SYNC_FILE . static::FILE_ARCHIVE_LOCATION;
        // Add year and month directory to reduce size of archive directory
        $archivePath .= date('Y') . \DIRECTORY_SEPARATOR . date('m') . \DIRECTORY_SEPARATOR . $file->getFilename();

        $this->filesystem->copy($file->getRealPath(), $archivePath);
        $this->filesystem->remove($file->getRealPath());
    }
}
