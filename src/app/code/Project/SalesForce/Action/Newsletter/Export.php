<?php

namespace Project\SalesForce\Action\Newsletter;

use Magento\Newsletter\Model\Subscriber;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory;
use Symfony\Component\Filesystem\Filesystem;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Project\SalesForce\Manager\ExportCsvManager;
use Project\SalesForce\Helper\ExportHelper;
use Project\SalesForce\Helper\SalesForceHelper;

/**
 * Class Export
 * @package Project\SalesForce\Action\Newsletter
 */
class Export implements ActionInterface
{
    /**
     * @var CollectionFactory
     */
    private $subscriberCollectionFactory;

    /**
     * @var ExportCsvManager
     */
    private $exportCsvManager;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var ExportHelper
     */
    private $exportHelper;

    /**
     * @var SalesForceHelper
     */
    private $salesForceHelper;

    /**
     * Export constructor.
     *
     * @param CollectionFactory $subscriberCollectionFactory
     * @param ExportCsvManager $exportCsvManager
     * @param ExportHelper $exportHelper
     * @param SalesForceHelper $salesForceHelper
     * @param Filesystem $filesystem
     * @param State $state
     */
    public function __construct(
        CollectionFactory $subscriberCollectionFactory,
        ExportCsvManager $exportCsvManager,
        ExportHelper $exportHelper,
        SalesForceHelper $salesForceHelper,
        Filesystem $filesystem,
        State $state
    ) {
        $this->subscriberCollectionFactory = $subscriberCollectionFactory;
        $this->exportCsvManager = $exportCsvManager;
        $this->exportHelper = $exportHelper;
        $this->salesForceHelper = $salesForceHelper;
        $this->filesystem = $filesystem;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     */
    public function execute(array $params, array $data, $flowCode, ConsoleOutput $consoleOutput)
    {
        if (!$this->salesForceHelper->isImportExportEnabled()) {
            return [];
        }

        if (isset($params['replay_start_date']) && isset($params['replay_end_date'])) {
            $date = new \DateTime($params['replay_start_date']);
            $dateStart = $date->format('Y-m-d H:i:s');
            $date = new \DateTime($params['replay_end_date']);
            $dateEnd = $date->format('Y-m-d H:i:s');
        } else {
            $date = (new \DateTime())->modify('-1 day');
            $dateStart = $date->format('Y-m-d 00:00:00');
            $dateEnd = $date->format('Y-m-d 23:59:59');
        }

        /** @var \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection $collection */
        $collection = $this->subscriberCollectionFactory->create()
            ->addFieldToFilter('change_status_at', ['gteq' => $dateStart])
            ->addFieldToFilter('change_status_at', ['lteq' => $dateEnd]);
        $collection->getSelect()
            ->joinLeft(
                'customer_entity',
                'customer_entity.entity_id = customer_id',
                ['firstname', 'lastname', 'gender']
            )
            ->joinLeft(
                'customer_address_entity',
                'customer_entity.default_billing = customer_address_entity.entity_id',
                ['telephone']
            )
            ->joinLeft(
                'store',
                'store.store_id = main_table.store_id',
                ['storeName' => 'name', 'storeCode' => 'code']
            )
            ->joinLeft(
                'store_website',
                'store_website.website_id = store.website_id',
                ['websiteName' => 'name', 'websiteCode' => 'code']
            )
            ->joinLeft(
                'store_group',
                'store_group.group_id = store_website.default_group_id',
                ['groupCode' => 'store_group.code']
            );

        if ($collection->count() > 0) {
            $file = $this->exportCsvManager->processNewsletterFile($collection->getItems());
            $this->archiveFile($file);
        }

        return ['subscribers' => $collection];
    }

    /**
     * @param $file
     */
    private function archiveFile($file)
    {
        $archivePath = $this->exportHelper->getFullPath($this->exportHelper::LOCAL_PATH_ARCHIVE);
        // Add year and month directory to reduce size of archive directory
        $archivePath .= date('Y') . \DIRECTORY_SEPARATOR . date('m') . \DIRECTORY_SEPARATOR . $file;

        $this->filesystem->copy(
            $this->exportHelper->getFullPath($this->exportHelper::LOCAL_PATH_CURRENT) . \DIRECTORY_SEPARATOR . $file,
            $archivePath
        );
    }
}
