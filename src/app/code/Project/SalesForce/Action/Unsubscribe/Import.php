<?php

namespace Project\SalesForce\Action\Unsubscribe;

use Project\SalesForce\Action\AbstractSalesForceImport;
use Synolia\Sync\Console\ConsoleOutput;

/**
 * Class Import
 *
 * @package Project\SalesForce\Action\Unsubscribe
 * @author Synolia <contact@synolia.com>
 */
class Import extends AbstractSalesForceImport
{
    /**
     * Location of file to import in /var/sync
     */
    const FILE_LOCATION         = 'in/salesforce/unsubscribe/current';
    const FILE_ARCHIVE_LOCATION = 'in/salesforce/unsubscribe/archive/';

    /**
     * Main method for sync sequence block
     *
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        if (!$this->salesForceHelper->isImportExportEnabled()) {
            return [];
        }

        /** @var \IteratorAggregate $files */
        $files = $this->getFilesToImport();

        $emails = [];
        foreach ($files as $file) {
            /** @var \Symfony\Component\Finder\SplFileInfo $file */
            $this->output->writeln('Process file ' . $file->getFilename());
            try {
                $results = $this->csvManager->processUnsubscribeFile($file);
                $emails  = \array_merge($emails, $results);
            } catch (\Throwable $throwable) {
                $this->output->writeln([
                    '<error>',
                    'Error while processing file ' . $file->getFilename(),
                    $throwable->getMessage(),
                    '</error>',
                ]);
            }
            try {
                $this->archiveFile($file);
            } catch (\Throwable $throwable) {
                $this->output->writeln([
                    '<error>',
                    'Error while archiving file ' . $file->getFilename(),
                    $throwable->getMessage(),
                    '</error>',
                ]);
            }
        }

        if (!empty($emails)) {
            try {
                $this->subscriptionManager->unsubscribeFromMailList($emails);
            } catch (\Throwable $throwable) {
                $this->output->writeln($throwable->getMessage());
            }
        }

        return [];
    }
}
