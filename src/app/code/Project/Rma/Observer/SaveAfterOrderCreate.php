<?php

namespace Project\Rma\Observer;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderRepository;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class SaveAfterOrderCreate
 * @package Project\Rma\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SaveAfterOrderCreate implements ObserverInterface
{
    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * SaveAfterOrderCreate constructor.
     *
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        OrderRepository $orderRepository,
        State $state
    ) {
        $this->orderRepository = $orderRepository;
        $this->state = $state;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $orderId = $observer->getData('quote')->getOrderId();
        $rmaId = $observer->getData('quote')->getRmaId();
        $creditmemoId = $observer->getData('quote')->getCreditmemoId();

        if ($this->state->getAreaCode() === Area::AREA_ADMINHTML && !empty($orderId)) {
            /** @var \Project\Sales\Model\Magento\Sales\Order $order */
            $order = $this->orderRepository->get($orderId);
            $order->setStatus(Order::STATE_CREDITMEMO_EXCHANGED);

            $this->orderRepository->save($order);

            if (!empty($rmaId)) {
                $observer->getData('order')->setRmaId($rmaId);
                $this->orderRepository->save($observer->getData('order'));
            }

            if (!empty($creditmemoId)) {
                $observer->getData('order')->setCreditmemoId($creditmemoId);
                $this->orderRepository->save($observer->getData('order'));
            }
        }
    }
}
