<?php

namespace Project\Rma\Observer\ApiRest;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Rma\Model\Item;
use Magento\Rma\Helper\Eav as RmaEav;

/**
 * Class SaveBeforeRmaItem
 * @package Project\Rma\Observer\ApiRest
 * @author Synolia <contact@synolia.com>
 */
class SaveBeforeRmaItem implements ObserverInterface
{
    /**
     * Rma eav
     *
     * @var RmaEav
     */
    protected $rmaEav;

    /**
     * SaveAfterRmaItem constructor.
     *
     * @param \Magento\Rma\Helper\Eav $rmaEav
     */
    public function __construct(
        RmaEav $rmaEav
    ) {
        $this->rmaEav = $rmaEav;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var Item $rma */
        $rmaItem = $observer->getRmaItem();

        $this->setExtensionAttributes($rmaItem);
    }

    /**
     * @param Item $rmaItem
     */
    protected function setExtensionAttributes(Item $rmaItem)
    {
        $extAttributes = $rmaItem->getExtensionAttributes();

        if (isset($extAttributes) && method_exists($extAttributes, 'getCondition')) {
            $condition = $extAttributes->getCondition();
            $conditionArray = $this->rmaEav->getAttributeOptionValues('condition', 0);
            $conditionArray = array_flip($conditionArray);
            $rmaItem->setCondition($conditionArray[$condition]);
        }
    }
}
