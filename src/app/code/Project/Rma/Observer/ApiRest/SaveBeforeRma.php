<?php

namespace Project\Rma\Observer\ApiRest;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Project\Rma\Model\Rma;

/**
 * Class SaveBeforeRma
 * @package Project\Rma\Observer\ApiRest
 */
class SaveBeforeRma implements ObserverInterface
{

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Rma $rma */
        $rma = $observer->getRma();

        $this->setExtensionAttributes($rma);
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     */
    protected function setExtensionAttributes(Rma $rma)
    {
        $extAttributes = $rma->getExtensionAttributes();

        if (isset($extAttributes) && method_exists($extAttributes, 'getDeliveryDate')) {
            $attribute = $extAttributes->getDeliveryDate();
            $rma->setDeliveryDate($attribute);
        }

        if (isset($extAttributes) && method_exists($extAttributes, 'getHasImages')) {
            $attribute = $extAttributes->getHasImages();
            $rma->setHasImages($attribute);
        }
    }
}
