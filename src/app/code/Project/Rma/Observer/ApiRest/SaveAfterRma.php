<?php

namespace Project\Rma\Observer\ApiRest;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Rma\Model\Rma\Source\Status;
use Magento\Rma\Model\RmaRepository;
use Project\Rma\Model\Rma;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class SaveAfterRma
 * @package Project\Rma\Observer\ApiRest
 * @author Synolia <contact@synolia.com>
 */
class SaveAfterRma implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Rma\Model\RmaRepository
     */
    protected $rmaRepository;

    /**
     * SaveAfterRma constructor.
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Rma\Model\RmaRepository $rmaRepository
     */
    public function __construct(
        TimezoneInterface $timezone,
        RmaRepository $rmaRepository
    ) {
        $this->timezone = $timezone;
        $this->rmaRepository = $rmaRepository;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Rma $rma */
        $rma = $observer->getRma();

        $this->saveReceivedDate($rma);
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     */
    protected function saveReceivedDate(Rma $rma)
    {
        if ($rma->getStatus() === Status::STATE_RECEIVED && is_null($rma->getReceivedDate())) {
            $today = $this->timezone->date()->format('Y-m-d');
            $rma->setReceivedDate($today);
            $this->rmaRepository->save($rma);
        }
    }
}
