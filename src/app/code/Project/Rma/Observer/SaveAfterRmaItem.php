<?php

namespace Project\Rma\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Rma\Helper\Eav as RmaEav;
use Magento\Rma\Model\Item;
use Magento\Framework\App\ResourceConnection;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Model\Order\ItemFactory;
use Project\Rma\Model\Rma;

/**
 * Class SaveAfterRmaItem
 * @package Project\Rma\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SaveAfterRmaItem implements ObserverInterface
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * Rma eav
     *
     * @var RmaEav
     */
    protected $rmaEav;

    /**
     * @var \Magento\Sales\Model\Order\ItemFactory
     */
    protected $itemFactory;

    /**
     * @var \Magento\Rma\Model\RmaRepository
     */
    protected $rmaRepository;

    /**
     * SaveAfterRmaItem constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Rma\Helper\Eav $rmaEav
     * @param \Magento\Sales\Model\Order\ItemFactory $itemFactory
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        RmaEav $rmaEav,
        ItemFactory $itemFactory,
        RmaRepository $rmaRepository
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->rmaEav = $rmaEav;
        $this->itemFactory = $itemFactory;
        $this->rmaRepository = $rmaRepository;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Item $rmaItem */
        $rmaItem = $observer->getRmaItem();
        $this->setExtensionAttributes($rmaItem);
    }

    /**
     * @param \Magento\Rma\Model\Item $rmaItem
     */
    protected function setExtensionAttributes(Item $rmaItem)
    {
        if (!empty($rmaItem->getCondition())) {
            $condition = $rmaItem->getCondition();
            $conditionArray = $this->rmaEav->getAttributeOptionValues('condition', 0);
            $this->setRmaCondition($conditionArray[$condition], $rmaItem);
        }
    }

    /**
     * @param string $itemCondition
     * @param \Magento\Rma\Model\Item $rmaItem
     */
    private function setRmaCondition($itemCondition, Item $rmaItem)
    {
        if ($itemCondition == Rma::ITEM_CONDITION_DAMAGED || $itemCondition == Rma::ITEM_CONDITION_SHOES_DAMAGED) {
            $rmaCondition = Rma::ITEM_CONDITION_DAMAGED;
        } else {
            $rmaCondition = Rma::ITEM_CONDITION_NORMAL;
        }

        $this->rmaRepository->get($rmaItem->getRmaEntityId())
            ->setItemCondition($rmaCondition)
            ->save();
    }
}
