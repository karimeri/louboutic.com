<?php

namespace Project\Rma\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Rma\Model\Rma\Status\HistoryFactory as RmaHistoryFactory;
use Project\Rma\Helper\Config as RmaConfigHelper;
use Project\Rma\Helper\Pickup as RmaPickupHelper;
use Project\Rma\Model\EmailManager;
use Project\Rma\Model\LabelManager;
use Project\Rma\Model\Rma;
use Psr\Log\LoggerInterface;

/**
 * Class SaveAfterReturnAddress
 * @package Project\Rma\Observer
 */
class SaveAfterReturnAddress implements ObserverInterface
{
    /**
     * @var RmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var RmaConfigHelper
     */
    protected $rmaConfigHelper;

    /**
     * @var RmaPickupHelper
     */
    protected $rmaPickupHelper;

    /**
     * @var RmaHistoryFactory
     */
    protected $rmaHistoryFactory;

    /**
     * @var LabelManager
     */
    protected $labelManager;

    /**
     * @var EmailManager
     */
    protected $emailManager;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * SaveAfterReturnAddress constructor.
     * @param RmaRepositoryInterface $rmaRepository
     * @param RmaConfigHelper $rmaConfigHelper
     * @param RmaPickupHelper $rmaPickupHelper
     * @param RmaHistoryFactory $rmaHistoryFactory
     * @param LabelManager $labelManager
     * @param EmailManager $emailManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        RmaRepositoryInterface $rmaRepository,
        RmaConfigHelper $rmaConfigHelper,
        RmaPickupHelper $rmaPickupHelper,
        RmaHistoryFactory $rmaHistoryFactory,
        LabelManager $labelManager,
        EmailManager $emailManager,
        LoggerInterface $logger
    ) {
        $this->rmaRepository        = $rmaRepository;
        $this->rmaConfigHelper      = $rmaConfigHelper;
        $this->rmaPickupHelper      = $rmaPickupHelper;
        $this->rmaHistoryFactory    = $rmaHistoryFactory;
        $this->labelManager         = $labelManager;
        $this->emailManager         = $emailManager;
        $this->logger               = $logger;
    }

    /**
     * Regenerate ShippingLabel on RMA Address Or RMA Return Address update
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var Rma $rma */
            $rma = $observer->getData('rma');

            if ($rma->getRmaAddressChangedFlag() || ($rma->getOrigData('rma_return_address_id') !== $rma->getData('rma_return_address_id'))) {
                $method = $this->rmaPickupHelper->getShippingMethod($rma);
                if($method){
                    $code = \strtolower($method->getCode());

                    // label generation
                    $this->labelManager->generateLabel($rma, $method);
                }


                // cancel current pickup
                if ($rma->getPickupConfirmationNumber()) {
                    switch (true) {
//                        case strpos($code, 'dhl') !== false:
//                            $response = $this->rmaPickupHelper->cancelDhl($rma);
//                            break;
                        case strpos($code, 'ups') !== false:
                            $response = $this->rmaPickupHelper->cancelUps($rma);
                            break;
                        default:
                            return;
                    }

                    if (!empty($response)) {
                        throw new LocalizedException(__($response));
                    }

                    $rma->setPickupConfirmationNumber(null);
                }

                // request new pickup
                if ($rma->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_AUTHORIZED) {
                    switch (true) {
//                        case strpos($code, 'dhl') !== false:
//                            $confirmationNumber = $this->rmaPickupHelper->requestDhl($rma);
//                            break;
                        case strpos($code, 'ups') !== false:
                            $confirmationNumber = $this->rmaPickupHelper->requestUps($rma);
                            break;
                        default:
                            return;
                    }

                    if ($confirmationNumber) {
                        $rma->setPickupConfirmationNumber($confirmationNumber);

                        /** @var \Magento\Rma\Model\Rma\Status\History $statusHistory */
                        $statusHistory = $this->rmaHistoryFactory->create();
                        $statusHistory->setRmaEntityId($rma->getEntityId());
                        $statusHistory->saveComment(
                            __('Pickup confirmed with number "%1"', $confirmationNumber),
                            false,
                            true
                        );
                    }
                }

                $this->rmaRepository->save($rma);

                // Send email
                $this->emailManager->sendRmaEmail($rma, true);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
