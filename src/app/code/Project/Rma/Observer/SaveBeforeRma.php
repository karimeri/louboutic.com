<?php

namespace Project\Rma\Observer;

use Magento\Framework\App\Area;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Project\Rma\Helper\Pickup as PickupHelper;
use Project\Rma\Model\Config\Source\Origin;
use Project\Rma\Model\EmailManager;
use Project\Rma\Model\ReturnAddressRepository;
use Project\Rma\Model\Rma;
use Project\Wms\Helper\Config;

/**
 * Class SaveBeforeRma
 * @package Project\Rma\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SaveBeforeRma implements ObserverInterface
{
    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var PickupHelper
     */
    protected $pickupHelper;

    /**
     * @var Config
     */
    protected $wmsHelper;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var EmailManager
     */
    protected $emailManager;

    /**
     * SaveBeforeRma constructor.
     * @param ReturnAddressRepository      $returnAddressRepository
     * @param RequestInterface             $request
     * @param PickupHelper                 $pickupHelper
     * @param Config                       $wmsHelper
     * @param \Magento\Framework\App\State $state
     * @param  EmailManager $emailManager
     */
    public function __construct(
        ReturnAddressRepository $returnAddressRepository,
        RequestInterface $request,
        PickupHelper $pickupHelper,
        Config $wmsHelper,
        State $state,
        EmailManager $emailManager
    ) {
        $this->returnAddressRepository = $returnAddressRepository;
        $this->request                 = $request;
        $this->pickupHelper            = $pickupHelper;
        $this->wmsHelper               = $wmsHelper;
        $this->state                   = $state;
        $this->emailManager         = $emailManager;
    }

    /**
     * @param Observer $observer
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \ReflectionException
     */
    public function execute(Observer $observer)
    {
        /** @var Rma $rma */
        $rma = $observer->getRma();
        $this->setPickupDate($rma);
        $this->setReturnAddress($rma);
        $this->setProperties($rma);
        $this->resetFlagExportedWms($rma);
        $this->setOrigin($rma);
    }

    /**
     * @param Rma $rma
     * @throws LocalizedException
     */
    protected function setOrigin(Rma $rma)
    {
        if (!$rma->getOrigin() && empty($rma->getId())) {
            if ($this->state->getAreaCode() === Area::AREA_FRONTEND) {
                $rma->setOrigin(Origin::FO);
            } else {
                $rma->setOrigin(Origin::BO);
            }
        }
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setReturnAddress(Rma $rma)
    {
        $post = $this->request->getPostValue();

        if ($this->state->getAreaCode() === Area::AREA_ADMINHTML &&
            !empty($post['rma_return_address_id']) &&
            empty($rma->getId())
        ) {
            /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
            $returnAddress = $this->returnAddressRepository->getById($post['rma_return_address_id']);
            $rma->setRmaReturnAddressId($returnAddress->getId());
        }

        $this->setDefaultReturnAddress($rma);
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function setDefaultReturnAddress(Rma $rma)
    {
        if (empty($rma->getRmaReturnAddressId()) && empty($rma->getId())) {
            // set default return address
            /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
            $returnAddress = $this->returnAddressRepository->getByEstablishmentCode(
                $this->wmsHelper->getEstablishmentCode($rma->getOrder()->getStore()->getWebsiteId())
            );
            $rma->setRmaReturnAddressId($returnAddress->getId());
        }
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setProperties(Rma $rma)
    {
        $post = $this->request->getPostValue();
        if ($this->state->getAreaCode() === Area::AREA_ADMINHTML) {
            if (!empty($post['rma_category'])) {
                $rma->setCategory($post['rma_category']);
            }
            if (!empty($post['rma_priority'])) {
                $rma->setPriority($post['rma_priority']);
            }
            if (!empty($post['rma_stock_code'])) {
                $rma->setStockCode($post['rma_stock_code']);
            }
        } else {
            $rma->setStockCode($this->wmsHelper->getStockCode());
        }
    }

    /**
     * @param Rma $rma
     * @throws LocalizedException
     * @throws \ReflectionException
     */
    protected function setPickupDate(Rma $rma)
    {
        $post = $this->request->getPostValue();

        if (isset($post['pickup_date']) && $post['pickup_date'] !== 'access_point') {
            $allowedDates = $this->pickupHelper->getAllowedDates($rma->getOrder()->getStoreId());

            if (!\in_array($post['pickup_date'], $allowedDates)) {
                throw new LocalizedException(__('This pickup date is not allowed'));
            }

            $rma->setPickupDate($post['pickup_date']);
        }
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     */
    protected function resetFlagExportedWms(Rma $rma)
    {
        if ($rma->getOrigData()['status'] != $rma->getStatus()) {
            $rma->setWmsExported(0);
        }
    }
}
