<?php

namespace Project\Rma\Observer;

use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Base64Json;
use Magento\Rma\Model\Item;
use Magento\Rma\Model\Rma\Source\Status;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Model\Order\AddressFactory;
use Magento\Sales\Model\OrderRepository;
use Project\Rma\Controller\Rmaaddress\Create as RmaAddressCreate;
use Project\Rma\Manager\EavValueManager;
use Project\Rma\Model\EmailManager;
use Project\Rma\Model\Rma;
use Project\Rma\Model\RmaAddressRepository;
use Project\Sales\Model\Magento\Sales\Order;
use Magento\Rma\Helper\Eav as RmaEav;
use Project\Rma\Model\Config\Source\Resolution;

/**
 * Class SaveAfterRma
 * @package Project\Rma\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SaveAfterRma implements ObserverInterface
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var RmaAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var EavConfig
     */
    protected $eavConfig;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Sales\Model\Order\AddressFactory
     */
    protected $addressFactory;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Base64Json
     */
    protected $base64Json;

    /**
     * @var \Magento\Rma\Helper\Eav
     */
    protected $rmaEav;

    /**
     * @var bool
     */
    protected $save = false;

    /**
     * @var \Magento\Rma\Model\Rma
     */
    protected $rma;

    /**
     * @var \Magento\Rma\Model\ResourceModel\Item\Collection
     */
    protected $rmaItems;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Project\Rma\Manager\EavValueManager
     */
    protected $eavValueManager;

    /**
     * @var EmailManager
     */
    protected $emailManager;

    /**
     * SaveAfterRma constructor.
     *
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Project\Rma\Model\RmaAddressRepository $rmaAddressRepository
     * @param \Magento\Rma\Model\RmaRepository $rmaRepository
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Sales\Model\Order\AddressFactory $addressFactory
     * @param \Magento\Framework\Serialize\Serializer\Base64Json $base64Json
     * @param \Magento\Rma\Helper\Eav $rmaEav
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Project\Rma\Manager\EavValueManager $eavValueManager
     */
    public function __construct(
        Registry $coreRegistry,
        RmaAddressRepository $rmaAddressRepository,
        RmaRepository $rmaRepository,
        EavConfig $eavConfig,
        OrderRepository $orderRepository,
        RequestInterface $request,
        AddressFactory $addressFactory,
        Base64Json $base64Json,
        RmaEav $rmaEav,
        ResourceConnection $resourceConnection,
        EavValueManager $eavValueManager,
        EmailManager $emailManager
    ) {
        $this->coreRegistry         = $coreRegistry;
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->rmaRepository        = $rmaRepository;
        $this->eavConfig            = $eavConfig;
        $this->orderRepository      = $orderRepository;
        $this->request = $request;
        $this->addressFactory = $addressFactory;
        $this->base64Json = $base64Json;
        $this->rmaEav = $rmaEav;
        $this->resourceConnection = $resourceConnection;
        $this->eavValueManager = $eavValueManager;
        $this->emailManager         = $emailManager;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {

        /** @var Rma $rma */
        $observerRma = $observer->getRma();
        $this->rma = $this->rmaRepository->get($observer->getRma()->getId());
        $this->rmaItems = $this->rma->getItemsForDisplay();
        $this->save = false;
        $this->setDefaultResolution();
        $this->setResolution();
        $this->setAmount();
        $this->createRmaAddress();
        $this->changeOrderStatus();

        if ($this->save) {
            $this->rmaRepository->save($this->rma);
        }

        if ($observerRma->getPickupDate() && ($observerRma->getOrigData('pickup_date') != $observerRma->getPickupDate())) {
            $this->emailManager->sendRmaEmail($observerRma, true);
        }
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function setDefaultResolution()
    {
        $items = $this->rmaItems;

        if (!$items) {
            return;
        }

        $saveResolution    = false;
        $attributeOptionId = null;
        foreach ($items as $item) {
            $resolution = $item->getResolution() ?: $this->eavValueManager->getValueByAttributeAndItem(
                Item::RESOLUTION,
                $item
            );

            if (!$resolution) {
                if ($saveResolution === false) {
                    $saveResolution = true;

                    $attribute = $this->eavConfig->getAttribute(Item::ENTITY, Item::RESOLUTION);
                    foreach ($attribute->getSource()->getAllOptions(true, true) as $option) {
                        if (\strcasecmp($option['label'], 'Refund') === 0) {
                            $attributeOptionId = $option['value'];
                            break;
                        }
                    }
                }

                if ($saveResolution && $attributeOptionId) {
                    $connection = $this->resourceConnection->getConnection();
                    $connection->insertOnDuplicate(
                        'magento_rma_item_entity_int',
                        [
                            'attribute_id' => $attribute->getId(),
                            'value' => $attributeOptionId,
                            'entity_id' => $item->getEntityId()
                        ]
                    );
                }
            }
        }
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function setResolution()
    {
        $items = $this->rmaItems;

        if (!$items) {
            return;
        }

        $resolutions = $this->rmaEav->getAttributeOptionValues('resolution');
        $originalResolution = $this->rma->getResolution();

        $nbItems = $items->count();
        foreach ($items as $item) {
            $resolution = $item->getResolution() ?: $this->eavValueManager->getOptionIdByAttributeNameAndItem(
                Item::RESOLUTION,
                $item
            );

            if ($resolution && isset($resolutions[$resolution])) {
                if ($this->rma->getResolution() &&
                    $this->rma->getResolution() !== $resolutions[$resolution] &&
                    $nbItems > 1
                ) {
                    $this->rma->setResolution(Resolution::MIX);

                    break;
                }

                $this->rma->setResolution($resolutions[$resolution]);
            }
        }

        if ($this->rma->getResolution() && $this->rma->getResolution() !== $originalResolution) {
            $this->save = true;
        }
    }

    protected function setAmount()
    {
        $items = $this->rmaItems;

        if (!$items) {
            return;
        }

        $order = $this->rma->getOrder();
        $amount = 0;
        $save = false;

        /** @var \Magento\Rma\Model\Item $item */
        foreach ($items as $item) {
            if ($item->getOrderItemId()) {
                $orderItem = $order->getItemById($item->getOrderItemId());
                if ($orderItem) {
                    $amount += $orderItem->getRowTotalInclTax();
                    $save = true;
                }
            }
        }

        if ($save && $this->rma->getAmount() != $amount) {
            $this->rma->setAmount($amount);
            $this->save = true;
        }
    }

    public function createRmaAddress()
    {
        try {
            /** @var \Project\Rma\Model\RmaAddress $rmaAddress */
            $rmaAddress = $this->rmaAddressRepository->getByParentId($this->rma->getId());
        } catch (NoSuchEntityException $exception) {
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->rma->getOrder();

        if ($order && empty($rmaAddress)) {
            $post = $this->request->getPostValue();
            // create
            if (!empty($post[RmaAddressCreate::ADDRESS_PARAM])) {
                $address = $this->addressFactory->create();
                $address->setData($this->base64Json->unserialize($post[RmaAddressCreate::ADDRESS_PARAM]));
            } else {
                $address = $order->getShippingAddress()->getData();
                $new_address = $this->request->getParam('new_shipping');
                $new_address = (array)json_decode($new_address);
                $entity_id= $address['entity_id'];
                foreach ($new_address as $key => $value){
                    if(isset($address[$key])) {
                        $address[$key] = $value;
                    }
                }
                $address['entity_id'] = $entity_id;
                $model = $this->addressFactory->create();
                $model->setData($address);
                $address = $model;
            }

            $this->rmaAddressRepository->createFromShippingAddress($address, $this->rma);
        }
    }

    protected function changeOrderStatus()
    {
        /** @var \Project\Sales\Model\Magento\Sales\Order $order */
        $order = $this->rma->getOrder();

        if ($this->rma->getStatus() === Status::STATE_REJECTED) {
            $order->setStatus(Order::STATE_RMA_CLOSED);
            $this->orderRepository->save($order);
        }

        if ($this->rma->getStatus() === Status::STATE_APPROVED) {
            $order->setStatus(Order::STATE_WAITING_CREDITMEMO);
            $this->orderRepository->save($order);
        }

        if ($this->rma->getStatus() === Status::STATE_PENDING) {
            $order->setStatus(Order::STATE_PROCESSING_RMA);
            $this->orderRepository->save($order);
        }
    }
}
