<?php

namespace Project\Rma\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Rma\Helper\Eav as RmaEav;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Model\OrderRepository;
use Project\Rma\Model\Rma;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class SaveAfterCreditMemo
 * @package Project\Rma\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SaveAfterCreditMemo implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Rma\Model\RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Rma\Helper\Eav
     */
    protected $rmaEav;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * SaveAfterCreditMemo constructor.
     * @param \Magento\Framework\App\RequestInterface           $request
     * @param \Magento\Rma\Model\RmaRepository                  $rmaRepository
     * @param \Magento\Sales\Model\OrderRepository              $orderRepository
     * @param \Magento\Rma\Helper\Eav                           $rmaEav
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        RequestInterface $request,
        RmaRepository $rmaRepository,
        OrderRepository $orderRepository,
        RmaEav $rmaEav,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->request = $request;
        $this->rmaRepository = $rmaRepository;
        $this->orderRepository = $orderRepository;
        $this->rmaEav = $rmaEav;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Creditmemo $creditmemo */
        $creditmemo = $observer->getCreditmemo();

        $post = $this->request->getPostValue();
        $rmaId = $this->request->getParam('rma_id');

        if (empty($rmaId)) {
            return;
        }

        /** @var Rma $rma */
        $rma = $this->rmaRepository->get($rmaId);
        /** @var Order $order */
        $order = $rma->getOrder();

        $rma->setStatus(\Magento\Rma\Model\Rma\Source\Status::STATE_PROCESSED_CLOSED);
        $this->rmaRepository->save($rma);

        // Force storeId to 0, to get admin option value (english value)
        $rmaResolutions = $this->rmaEav->getAttributeOptionValues('resolution', 0);


        $comment = $typeCreditMemo = '';

        if (isset($post['creditmemo']['do_offline'])) {
            if ($post['creditmemo']['do_offline'] == 1) {
                $typeCreditMemo = 'offline';
            } else {
                $typeCreditMemo = 'online';
            }
        }

        $comment .= __(\ucfirst($typeCreditMemo) . ': ');

        // phpcs:ignore Ecg.Performance.GetFirstItem
        $item = $rma->getItemsForDisplay()->getFirstItem();

        $comment .= $rmaResolutions[$item->getResolution()] . '. ';

        if ($rmaResolutions[$item->getResolution()] === 'Refund') {
            $comment .= __(
                'Refunded amount of %1 %2',
                $order->getBaseCurrency()->formatTxt($creditmemo->getBaseGrandTotal()),
                $typeCreditMemo
            )."<br/>\n";

            if ($typeCreditMemo === 'online') {
                $invoice = $creditmemo->getInvoice();
                $comment .= __('Transaction ID: %1. ', $invoice->getTransactionId());
            }

            if (!empty($typeCreditMemo)) {
                $comment .= __('Creditmemo %1 has been created', $creditmemo->getId());
            }

            $order->setStatus(Order::STATE_WAITING_CREDITMEMO);
        } elseif (strpos($rmaResolutions[$item->getResolution()], 'Exchange') !== false) {
            $comment .= __('Exchange %1', $typeCreditMemo);

            $order->setStatus(Order::STATE_CREDITMEMO_WAITING_EXCHANGE);
        }

        $order->addStatusHistoryComment($comment);

        $this->orderRepository->save($order);
    }
}
