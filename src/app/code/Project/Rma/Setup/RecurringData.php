<?php

namespace Project\Rma\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\Upgrade;

/**
 * Class RecurringData
 * @package Project\Rma\Setup
 * @author Synolia <contact@synolia.com>
 */
class RecurringData implements \Magento\Framework\Setup\InstallDataInterface
{
    /**
     * @var Upgrade
     */
    protected $synoliaSetup;

    /**
     * RecurringData constructor.
     * @param Upgrade $synoliaSetup
     */
    public function __construct(
        Upgrade $synoliaSetup
    ) {
        $this->synoliaSetup = $synoliaSetup;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->synoliaSetup->runUpgrade($this, 'RecurringData');
        $setup->endSetup();
    }
}
