<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\Rma\Model\RmaAddress;
use Project\Rma\Model\ResourceModel\RmaAddress as RmaAddressResource;

/**
 * Class UpgradeSchema103
 * @package Project\Rma\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema103
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        if (!$installer->getConnection()->isTableExists($installer->getTable(RmaAddressResource::TABLE_NAME))) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable(RmaAddressResource::TABLE_NAME)
            )->addColumn(
                RmaAddress::ENTITY_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )->addColumn(
                RmaAddress::PARENT_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Parent Id'
            )->addColumn(
                RmaAddress::REGION_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => true],
                'Region Id'
            )->addColumn(
                RmaAddress::FAX,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Fax'
            )->addColumn(
                RmaAddress::REGION,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Region'
            )->addColumn(
                RmaAddress::POSTCODE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Postcode'
            )->addColumn(
                RmaAddress::LASTNAME,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Lastname'
            )->addColumn(
                RmaAddress::STREET,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Street'
            )->addColumn(
                RmaAddress::CITY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'City'
            )->addColumn(
                RmaAddress::EMAIL,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Email'
            )->addColumn(
                RmaAddress::TELEPHONE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Phone Number'
            )->addColumn(
                RmaAddress::COUNTRY_ID,
                Table::TYPE_TEXT,
                2,
                ['nullable' => true],
                'Country Id'
            )->addColumn(
                RmaAddress::FIRSTNAME,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Firstname'
            )->addColumn(
                RmaAddress::PREFIX,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Prefix'
            )->addColumn(
                RmaAddress::MIDDLENAME,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Middlename'
            )->addColumn(
                RmaAddress::SUFFIX,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Suffix'
            )->addColumn(
                RmaAddress::COMPANY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Company'
            )->addIndex(
                'rma_address_parent_id',
                [RmaAddress::PARENT_ID]
            )->addForeignKey(
                $installer->getFkName(
                    RmaAddressResource::TABLE_NAME,
                    RmaAddress::PARENT_ID,
                    'magento_rma',
                    'entity_id'
                ),
                RmaAddress::PARENT_ID,
                $installer->getTable('magento_rma'),
                'entity_id',
                Table::ACTION_CASCADE
            );

            $installer->getConnection()->createTable($table);
        }
    }
}
