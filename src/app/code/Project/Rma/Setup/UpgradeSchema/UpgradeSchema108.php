<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\Rma\Model\Rma;

/**
 * Class UpgradeSchema108
 * @package Project\Rma\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeSchema108
{
    const TABLE_RMA = 'magento_rma';
    const TABLE_RMA_GRID = 'magento_rma_grid';
    const ITEM_CONDITION = 'item_condition';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            self::ITEM_CONDITION,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 7,
                'nullable' => true,
                'comment'  => 'Item condition',
                'default'  => Rma::ITEM_CONDITION_NORMAL,
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA_GRID),
            self::ITEM_CONDITION,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 7,
                'nullable' => true,
                'default'  => Rma::ITEM_CONDITION_NORMAL,
                'comment'  => 'Item condition',
            ]
        );

        $installer->getConnection()->addIndex(
            $installer->getTable(self::TABLE_RMA_GRID),
            $installer->getIdxName(self::TABLE_RMA_GRID, [self::ITEM_CONDITION]),
            [self::ITEM_CONDITION]
        );
    }
}
