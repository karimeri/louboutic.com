<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema102
 * @package Project\Rma\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema102
{
    const TABLE_RMA_GRID = 'magento_rma_grid';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA_GRID),
            'category',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 19,
                'nullable' => true,
                'default'  => 'Normal',
                'comment'  => 'RMA Category',
            ]
        );

        $installer->getConnection()->addIndex(
            $installer->getTable(self::TABLE_RMA_GRID),
            $installer->getIdxName(self::TABLE_RMA_GRID, ['category']),
            ['category']
        );

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA_GRID),
            'priority',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 6,
                'nullable' => true,
                'default'  => 'Normal',
                'comment'  => 'RMA Priority',
            ]
        );

        $installer->getConnection()->addIndex(
            $installer->getTable(self::TABLE_RMA_GRID),
            $installer->getIdxName(self::TABLE_RMA_GRID, ['priority']),
            ['priority']
        );

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA_GRID),
            'stock_code',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 4,
                'nullable' => true,
                'comment'  => 'RMA Stock code',
            ]
        );

        $installer->getConnection()->addIndex(
            $installer->getTable(self::TABLE_RMA_GRID),
            $installer->getIdxName(self::TABLE_RMA_GRID, ['stock_code']),
            ['stock_code']
        );
    }
}
