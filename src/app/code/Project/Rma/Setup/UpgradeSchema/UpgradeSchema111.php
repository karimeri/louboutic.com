<?php

namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema111
 * @package Project\Rma\Setup\UpgradeSchema
 */
class UpgradeSchema111
{
    const TABLE_RMA = 'magento_rma';
    const RETRY_COUNT = 'retry_count';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            self::RETRY_COUNT,
            [
                'type'     => Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => false,
                'comment'  => 'Retry Count',
                'default'  => 0
            ]
        );
    }
}
