<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema101
 * @package Project\Rma\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema101
{
    const TABLE_RMA = 'magento_rma';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            'category',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 19,
                'nullable' => true,
                'comment'  => 'RMA Category',
                'default'  => 'Normal',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            'priority',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 6,
                'nullable' => true,
                'comment'  => 'RMA Priority',
                'default'  => 'Normal',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            'stock_code',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 4,
                'nullable' => true,
                'comment'  => 'RMA Stock code',
            ]
        );
    }
}
