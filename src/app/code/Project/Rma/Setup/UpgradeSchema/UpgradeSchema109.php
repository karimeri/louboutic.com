<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\Rma\Model\Rma;

/**
 * Class UpgradeSchema109
 * @package Project\Rma\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeSchema109
{
    const TABLE_RMA = 'magento_rma';
    const PICKUP_CONFIRMATION_NUMBER = 'pickup_confirmation_number';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            self::PICKUP_CONFIRMATION_NUMBER,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Pickup confirmation number',
            ]
        );
    }
}
