<?php

namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema110
 * @package Project\Rma\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeSchema110
{
    const TABLE_RMA = 'magento_rma';
    const BACK_TO_STOCK = 'back_to_stock';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            self::BACK_TO_STOCK,
            [
                'type'     => Table::TYPE_BOOLEAN,
                'nullable' => false,
                'comment'  => 'Back to stock',
                'default' => 1
            ]
        );
    }
}
