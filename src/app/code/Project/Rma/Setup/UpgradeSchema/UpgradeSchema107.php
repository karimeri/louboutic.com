<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema107
 * @package Project\Rma\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeSchema107
{
    const TABLE_RMA = 'magento_rma';

    /**
     * @param UpgradeSchemaInterface $upgradeSchema
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            'pickup_date',
            [
                'type'     => Table::TYPE_DATE,
                'nullable' => true,
                'comment'  => 'Pickup selected date',
            ]
        );
    }
}
