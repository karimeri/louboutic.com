<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\Rma\Model\ImporterAddressRepository;
use Project\Rma\Model\ResourceModel\ImporterAddress as ImporterAddressResource;
use Project\Rma\Model\ImporterAddress;

/**
 * Class UpgradeSchema112
 * @package Project\Rma\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema112
{

    /**
     * @var ImporterAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var \Project\Rma\Model\ImporterAddressFactory
     */
    protected $rmaAddressFactory;

    /**
     * @var ImporterAddressResource
     */
    protected $rmaAddressResource;

    public function __construct(
        ImporterAddressRepository $rmaAddressRepository,
        \Project\Rma\Model\ImporterAddressFactory $rmaAddressFactory,
        ImporterAddressResource $rmaAddressResource
    ) {
        $this->rmaAddressResource = $rmaAddressResource;
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->rmaAddressFactory = $rmaAddressFactory;
    }


    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();
        $this->createImpoerterAddressTable($installer);
    }

    /**
     * @param $installer
     */
    private function createImpoerterAddressTable($installer)
    {
        if (!$installer->getConnection()->isTableExists($installer->getTable(ImporterAddressResource::TABLE_NAME))) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable(ImporterAddressResource::TABLE_NAME)
            )->addColumn(
                ImporterAddress::ENTITY_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )->addColumn(
                ImporterAddress::ADDRESS_NAME,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Name'
            )->addColumn(
                ImporterAddress::ADDRESS_COMPANY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Company'
            )->addColumn(
                ImporterAddress::ADDRESS_STREET,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Street'
            )->addColumn(
                ImporterAddress::ADDRESS_CODE_POSTAL,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Code postal'
            )->addColumn(
                ImporterAddress::ADDRESS_CITY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'City'
            )->addColumn(
                ImporterAddress::ADDRESS_COUNTRY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Country'
            )->addColumn(
                ImporterAddress::ADDRESS_VAT,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'VAT'
            );
            $installer->getConnection()->createTable($table);
            $rmaAddress = $this->rmaAddressFactory->create();
            $data = [
                'address_name'=>'CHRISTIAN LOUBOUTIN Suisse',
                'address_company'=>'Fiscal Representative Baseroma SRL',
                'address_street'=>'P.zza Lorenzo in Lucinda 22',
                'address_code_postal'=>'00186',
                'address_city'=>'Roma',
                'address_country'=>'ITALY',
                'address_vat'=>'11789141006',
            ];
            $rmaAddress->saveAddressData($rmaAddress,$data);
            $this->rmaAddressRepository->save($rmaAddress);
        }
    }
}
