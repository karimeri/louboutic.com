<?php
namespace Project\Rma\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\Rma\Api\Data\ReturnAddressInterface;
use Project\Rma\Model\ResourceModel\ReturnAddress as ReturnAddressResource;

/**
 * Class UpgradeSchema104
 * @package Project\Rma\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema104
{
    const TABLE_RMA = 'magento_rma';

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $this->createReturnAddressTable($installer);
        $this->updateRmaTable($installer);
    }

    /**
     * @param $installer
     */
    private function createReturnAddressTable($installer)
    {
        if (!$installer->getConnection()->isTableExists($installer->getTable(ReturnAddressResource::TABLE_NAME))) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable(ReturnAddressResource::TABLE_NAME)
            )->addColumn(
                ReturnAddressInterface::ENTITY_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )->addColumn(
                ReturnAddressInterface::REGION,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Region'
            )->addColumn(
                ReturnAddressInterface::POSTCODE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Postcode'
            )->addColumn(
                ReturnAddressInterface::NAME,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Name'
            )->addColumn(
                ReturnAddressInterface::STREET1,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Street line 1'
            )->addColumn(
                ReturnAddressInterface::STREET2,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Street line 2'
            )->addColumn(
                ReturnAddressInterface::STREET3,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Street line 3'
            )->addColumn(
                ReturnAddressInterface::CITY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'City'
            )->addColumn(
                ReturnAddressInterface::CONTACT_NAME,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Contact name'
            )->addColumn(
                ReturnAddressInterface::CONTACT_PHONE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Contact Phone Number'
            )->addColumn(
                ReturnAddressInterface::COUNTRY_ID,
                Table::TYPE_TEXT,
                2,
                ['nullable' => true],
                'Country Id'
            )->addColumn(
                ReturnAddressInterface::EMAIL_TEMPLATE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Email template'
            )->addColumn(
                ReturnAddressInterface::COMPANY,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Company'
            )->addColumn(
                ReturnAddressInterface::ESTABLISHMENT_CODE,
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Establishment Code'
            );

            $installer->getConnection()->createTable($table);
        }
    }

    /**
     * @param $installer
     */
    private function updateRmaTable($installer)
    {
        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_RMA),
            'rma_return_address_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'nullable' => true,
                'comment'  => 'RMA Return Address ID',
            ]
        );

        $installer->getConnection()->addIndex(
            $installer->getTable(self::TABLE_RMA),
            $installer->getIdxName(self::TABLE_RMA, ['rma_return_address_id']),
            ['rma_return_address_id']
        );
    }
}
