<?php
namespace Project\Rma\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class UpgradeSchema
 * @package Project\Rma\Setup
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const VERSIONS = [
        '1.0.1' => '101',
        '1.0.2' => '102',
        '1.0.3' => '103',
        '1.0.4' => '104',
        '1.0.7' => '107',
        '1.0.8' => '108',
        '1.0.9' => '109',
        '1.1.0' => '110',
        '1.1.1' => '111',
        '1.1.4' => '112',
    ];

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var SchemaSetupInterface
     */
    protected $setup;

    /**
     * UpgradeData constructor.
     * @param ConsoleOutput $output
     */
    public function __construct(
        ConsoleOutput $output
    ) {
        $this->objectManager = ObjectManager::getInstance();
        $this->output        = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;
        $this->setup->startSetup();
        $this->output->writeln(""); // new line in console

        foreach ($this::VERSIONS as $version => $fileData) {
            if (\version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln('Processing "Project/Rma" setup: ' . $version);

                $currentSetup = $this->getObjectManager()
                    ->create('Project\Rma\Setup\UpgradeSchema\UpgradeSchema' . $fileData);

                $currentSetup->run($this);
            }
        }
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return SchemaSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }
}
