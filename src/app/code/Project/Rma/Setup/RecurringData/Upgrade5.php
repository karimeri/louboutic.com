<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade5
 * @package Project\Rma\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade5 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';
    const TABLE_RMA_GRID = 'magento_rma_grid';
    const ITEM_AMOUNT = 'amount';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade5 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA),
            self::ITEM_AMOUNT,
            [
                'type'     => Table::TYPE_DECIMAL,
                'length'   => '12,4',
                'nullable' => true,
                'comment'  => 'Amount',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            self::ITEM_AMOUNT,
            [
                'type'     => Table::TYPE_DECIMAL,
                'length'   => '12,4',
                'nullable' => true,
                'comment'  => 'Amount',
            ]
        );

        $this->connection->addIndex(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            $this->connection->getIndexName(self::TABLE_RMA_GRID, [self::ITEM_AMOUNT]),
            [self::ITEM_AMOUNT]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add amount column to RMA';
    }
}
