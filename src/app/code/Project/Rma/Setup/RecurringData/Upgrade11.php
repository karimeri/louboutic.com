<?php

namespace Project\Rma\Setup\RecurringData;

use Synolia\Cron\Api\TaskRepositoryInterface;
use Synolia\Cron\Model\TaskFactory;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade11
 * @package Project\Rma\Setup\RecurringData
 */
class Upgrade11 implements UpgradeDataSetupInterface
{
    /**
     * @var TaskFactory
     */
    protected $taskFactory;

    /**
     * @var TaskRepositoryInterface
     */
    protected $taskRepository;

    public function __construct(
        TaskFactory $taskFactory,
        TaskRepositoryInterface $taskRepository
    ) {
        $this->taskFactory      = $taskFactory;
        $this->taskRepository   = $taskRepository;
    }

    /**
     * @param Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            'name' => 'retry_label_pickup',
            'active' => 1,
            'frequency' => '*/20 * * * *',
            'command' => 'project:rmalabel:retry',
            'parameter' => '',
            'option' => '',
            'isolated' => 1
        ];

        $taskModel = $this->taskFactory->create()->setData($data);
        $this->taskRepository->save($taskModel);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Add cron for RMA label retry';
    }
}
