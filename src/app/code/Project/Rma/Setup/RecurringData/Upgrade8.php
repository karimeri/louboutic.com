<?php

namespace Project\Rma\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Rma\Helper\Config;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade8
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade8 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Upgrade8 constructor.
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        EnvironmentManager $environmentManager
    ) {
        $this->configSetup = $configSetup;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $this->configSetup->saveConfig(Config::XML_PATH_RMA_ESTIMATED_WEIGHT, 5);
        } else {
            $this->configSetup->saveConfig(Config::XML_PATH_RMA_ESTIMATED_WEIGHT, 3);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Config for estimated weight for RMA PDF';
    }
}
