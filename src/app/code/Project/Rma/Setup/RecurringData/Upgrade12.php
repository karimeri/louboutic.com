<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Store\Model\StoreRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Standard\Setup\ConfigSetupFactory;
/**
 * Class Upgrade10
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade12
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    const STORE_CODE_FR_FR = 'fr_fr';
    const STORE_CODE_UK_EN = 'uk_en';
    const STORE_CODE_IT_EN = 'it_en';
    const STORE_CODE_DE_EN = 'de_en';
    const STORE_CODE_ES_EN = 'es_en';
    const STORE_CODE_CH_EN = 'ch_en';
    const STORE_CODE_CH_FR = 'ch_fr';
    const STORE_CODE_NL_EN = 'nl_en';
    const STORE_CODE_LU_FR = 'lu_fr';
    const STORE_CODE_LU_EN = 'lu_en';
    const STORE_CODE_BE_FR = 'be_fr';
    const STORE_CODE_BE_EN = 'be_en';
    const STORE_CODE_AT_EN = 'at_en';
    const STORE_CODE_IE_EN = 'ie_en';
    const STORE_CODE_PT_EN = 'pt_en';
    const STORE_CODE_MC_FR = 'mc_fr';
    const STORE_CODE_MC_EN = 'mc_en';
    const STORE_CODE_GR_EN = 'gr_en';


    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * Upgrade10 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        StoreRepository $storeRepository

    ) {
        $this->cmsSetup = $cmsSetup;
        $this->storeRepository    = $storeRepository;

    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->updateCmsBlock($upgradeObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function updateCmsBlock($upgradeDataObject)
    {
        $cmsBlockIdentifier = 'ups-pickup-information';
        foreach ($this->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsBlock = [];
            if($locale == 'fr_FR') {
                $cmsBlock = [
                    'title'      => 'UPS Pickup information',
                    'identifier'  => $cmsBlockIdentifier,
                    'stores'      => $stores
                ];
            }else if($locale == 'en_GB'){
                $cmsBlock = [
                    'title'      => 'UPS Pickup information',
                    'identifier' => $cmsBlockIdentifier,
                    'content'    => '<p>For a UPS Access Point relay deposit, please contact customer service.</p>',
                    'is_active'  => 1,
                    'stores'     => $stores
                ];
            }

            $this->cmsSetup->saveBlock($cmsBlock);
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add ups pickup block for return creation in frontend';
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocale()
    {
        return [
            'fr_FR' => [
                $this->storeRepository->get(self::STORE_CODE_FR_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CH_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_FR)->getId()
            ],
            'en_GB' => [
                $this->storeRepository->get(self::STORE_CODE_CH_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_UK_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_DE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_ES_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_NL_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_AT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_PT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_GR_EN)->getId()
            ]
        ];
    }
}
