<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Project\Rma\Model\Config\Source\Origin;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade3
 * @package Project\Rma\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';
    const TABLE_RMA_GRID = 'magento_rma_grid';
    const ITEM_ORIGIN = 'origin';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade3 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA),
            self::ITEM_ORIGIN,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 2,
                'nullable' => true,
                'comment'  => 'Origin',
                'default'  => Origin::FO,
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            self::ITEM_ORIGIN,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 2,
                'nullable' => true,
                'default'  => Origin::FO,
                'comment'  => 'Origin',
            ]
        );

        $this->connection->addIndex(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            $this->connection->getIndexName(self::TABLE_RMA_GRID, [self::ITEM_ORIGIN]),
            [self::ITEM_ORIGIN]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add origin column to RMA';
    }
}
