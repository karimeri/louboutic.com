<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade4
 * @package Project\Rma\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade4 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';
    const TABLE_RMA_GRID = 'magento_rma_grid';
    const ITEM_RESOLUTION = 'resolution';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade4 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA),
            self::ITEM_RESOLUTION,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Resolution',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            self::ITEM_RESOLUTION,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Resolution',
            ]
        );

        $this->connection->addIndex(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            $this->connection->getIndexName(self::TABLE_RMA_GRID, [self::ITEM_RESOLUTION]),
            [self::ITEM_RESOLUTION]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add resolution column to RMA';
    }
}
