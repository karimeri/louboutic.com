<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade7
 * @package Project\Rma\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade7 implements UpgradeDataSetupInterface
{
    const TABLE_QUOTE = 'quote';
    const COLUMN_RMA_ID = 'rma_id';
    const COLUMN_ORDER_ID = 'order_id';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade7 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_QUOTE),
            self::COLUMN_RMA_ID,
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'comment'  => 'Rma Id',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_QUOTE),
            self::COLUMN_ORDER_ID,
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'comment'  => 'Order Id',
            ]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add RMA Id to quote';
    }
}
