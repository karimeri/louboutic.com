<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade1
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnToAdd = [
            [
                'name' => 'wms_exported',
                'definition' => [
                    'nullable' => false,
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Is exported to WMS',
                    'default' => 0
                ]
            ]
        ];

        $this->abstractSetup->addColumn(
            self::TABLE_RMA,
            $columnToAdd[0]['name'],
            $columnToAdd[0]['definition']
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add column export wms to rma table';
    }
}
