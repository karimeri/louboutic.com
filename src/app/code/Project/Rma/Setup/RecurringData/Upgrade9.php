<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade9
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade9 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     * @param \Synolia\Cron\Model\Task $task
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        AbstractSetup $abstractSetup,
        Task $task,
        TaskRepository $taskRepository
    ) {
        $this->abstractSetup = $abstractSetup;
        $this->task = $task;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnToAdd = [
            [
                'name' => 'received_email_sent',
                'definition' => [
                    'nullable' => false,
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Is received email sent',
                    'default' => 0
                ]
            ]
        ];

        $this->abstractSetup->addColumn(
            self::TABLE_RMA,
            $columnToAdd[0]['name'],
            $columnToAdd[0]['definition']
        );

        $data = [
            'name' => 'send_rma_received_email',
            'active' => 1,
            'frequency' => '* * * * *',
            'command' => 'synolia:sync:launch send_rma_received_email',
            'parameter' => '',
            'option' => '',
            'isolated' => 1
        ];

        $taskModel = $this->task->setData($data);
        $this->taskRepository->save($taskModel);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add column received_email_sent to rma table';
    }
}
