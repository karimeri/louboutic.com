<?php

namespace Project\Rma\Setup\RecurringData;

use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade2
 * @package Project\Rma\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade2 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade2 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            'name' => 'generation_label_pickup',
            'active' => 1,
            'frequency' => '45 * * * *',
            'command' => 'project:rmalabel:generate',
            'parameter' => '',
            'option' => '',
            'isolated' => 0
        ];

        $taskModel = $this->task->setData($data);
        $this->taskRepository->save($taskModel);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add cron for RMA label generation';
    }
}
