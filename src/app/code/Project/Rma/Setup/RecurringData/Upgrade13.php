<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Store\Model\StoreRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Magento\Cms\Model\BlockRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Upgrade13
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade13
{

    const STORE_CODE_FR_FR = 'fr_fr';

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var SearchCriteriaBuilder
     */

    protected $searchCriteriaBuilder;

    /**
     * @param StoreRepository $storeRepository
     * @param BlockRepository $blockRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        StoreRepository $storeRepository,
        BlockRepository $blockRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->storeRepository = $storeRepository;
        $this->blockRepository = $blockRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;

    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsBlockIdentifier = 'ups-pickup-information';
        $store = $this->storeRepository->get(self::STORE_CODE_FR_FR);
        $searchCriteriaStore = $this->searchCriteriaBuilder
            ->addFilter('identifier', $cmsBlockIdentifier)
            ->addFilter('store_id', $store->getId())
            ->create();
        $blocks = $this->blockRepository->getList($searchCriteriaStore);
        foreach ($blocks->getItems() as $block) {
            if ($block->getId() && !$block->getContent()) {
                // phpcs:ignore
                $this->blockRepository->delete($block);
            }
            break;
        }
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Remove null content pickup block for fr_FR';
    }

}
