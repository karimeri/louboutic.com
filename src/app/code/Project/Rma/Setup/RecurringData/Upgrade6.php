<?php

namespace Project\Rma\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade6
 * @package Project\Rma\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade6 implements UpgradeDataSetupInterface
{
    const TABLE_RMA_GRID = 'magento_rma_grid';
    const ITEM_PICKUP_DATE = 'pickup_date';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade6 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            self::ITEM_PICKUP_DATE,
            [
                'type'     => Table::TYPE_DATE,
                'nullable' => true,
                'comment'  => 'Pickup Date',
            ]
        );

        $this->connection->addIndex(
            $this->connection->getTableName(self::TABLE_RMA_GRID),
            $this->connection->getIndexName(self::TABLE_RMA_GRID, [self::ITEM_PICKUP_DATE]),
            [self::ITEM_PICKUP_DATE]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add pickup date column to RMA grid';
    }
}
