<?php

namespace Project\Rma\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade10
 * @package Project\Rma\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade10 implements UpgradeDataSetupInterface
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade10 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsBlock = [
            'title'      => 'UPS Pickup information',
            'identifier' => 'ups-pickup-information',
            'content'    => '<p>Pour un dépôt en relai UPS Access Point, veuillez contacter le service client.</p>',
            'is_active'  => 1,
            'store_id'   => [0],
            'stores'     => [0]
        ];

        $this->cmsSetup->saveBlock($cmsBlock);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add ups pickup block for return creation in frontend';
    }
}
