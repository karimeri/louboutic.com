<?php
namespace Project\Rma\Setup\UpgradeData;

use Project\Rma\Setup\UpgradeData;

class UpgradeData107
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Hide company on checkout address form on EU
        $upgradeDataObject->getConfigSetup()->saveConfig('customer/address/company_show', 'opt');
    }
}
