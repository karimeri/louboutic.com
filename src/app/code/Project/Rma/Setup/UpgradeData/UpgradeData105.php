<?php
namespace Project\Rma\Setup\UpgradeData;

use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Rma\Model\Item;
use Magento\Eav\Setup\EavSetup;

/**
 * Class Upgrade105
 * Update Resolution attribute
 *
 * @package Project\Rma\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeData105
{
    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade105 constructor.
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * Update attribute
     */
    public function run()
    {
        $this->eavSetup->updateAttribute(
            Item::ENTITY,
            Item::RESOLUTION,
            EavAttributeInterface::IS_VISIBLE,
            false
        );
    }
}
