<?php
namespace Project\Rma\Setup\UpgradeData;

use Synolia\Standard\Setup\CmsSetup;

/**
 * Class UpgradeData106
 * Create CMS block for header of RMA order search
 *
 * @package Project\Rma\Setup\UpgradeData
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeData106
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * UpgradeData106 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * Create CMS block
     */
    public function run()
    {
        $cmsBlockIdentifier = 'rma-order-search';

        $cmsBlockContent = $this->cmsSetup->getCmsBlockContentData([
            'identifier' => $cmsBlockIdentifier,
            'moduleName' => 'Project_Rma',
        ]);

        $cmsBlock = [
            'title'      => $cmsBlockIdentifier,
            'identifier' => $cmsBlockIdentifier,
            'content'    => $cmsBlockContent,
            'is_active'  => 1,
            'store_id'   => [0],
            'stores'     => [0],
        ];

        $this->cmsSetup->saveBlock($cmsBlock, true);
    }
}
