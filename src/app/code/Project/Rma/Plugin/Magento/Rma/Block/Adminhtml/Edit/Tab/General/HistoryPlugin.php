<?php

namespace Project\Rma\Plugin\Magento\Rma\Block\Adminhtml\Edit\Tab\General;

use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\History;

/**
 * Class HistoryPlugin
 *
 * @package Project\Rma\Plugin\Magento\Rma\Block\Adminhtml\Edit\Tab\General
 * @author Synolia <contact@synolia.com>
 */
class HistoryPlugin
{
    /**
     * @param \Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\History $subject
     * @param \Magento\Rma\Model\ResourceModel\Rma\Status\History\Collection $result
     *
     * @return \Magento\Rma\Model\ResourceModel\Rma\Status\History\Collection
     */
    public function afterGetComments(History $subject, $result)
    {
        $result->setOrder('created_at');

        return $result;
    }
}
