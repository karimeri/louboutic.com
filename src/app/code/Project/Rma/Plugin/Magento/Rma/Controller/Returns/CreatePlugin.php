<?php
namespace Project\Rma\Plugin\Magento\Rma\Controller\Returns;

use Magento\Framework\App\Response\Http;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Rma\Controller\Returns\Create;
use Project\Rma\Helper\Config;

/**
 * Class CreatePlugin
 * @package Project\Rma\Plugin\Magento\Rma\Controller\Returns
 * @author  Synolia <contact@synolia.com>
 */
class CreatePlugin
{
    /**
     * @var Config
     */
    protected $rmaHelper;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Http
     */
    protected $http;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * CreatePlugin constructor.
     * @param Registry     $coreRegistry
     * @param Config       $rmaHelper
     * @param UrlInterface $urlBuilder
     * @param Http         $http
     */
    public function __construct(
        Registry $coreRegistry,
        Config $rmaHelper,
        UrlInterface $urlBuilder,
        Http $http
    ) {
        $this->rmaHelper    = $rmaHelper;
        $this->urlBuilder   = $urlBuilder;
        $this->http         = $http;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @param Create $subject
     * @param        $result
     */
    public function afterExecute(Create $subject, $result)
    {
        $order = $this->coreRegistry->registry('current_order');

        if (!$this->rmaHelper->isInDelays($order)) {
            $link = $this->urlBuilder->getUrl('sales/order/history', ['_current' => true, '_use_rewrite' => true]);
            $this->http->setRedirect($link);
            return;
        }

        return $result;
    }
}
