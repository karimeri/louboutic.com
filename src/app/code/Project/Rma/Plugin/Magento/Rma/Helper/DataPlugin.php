<?php

namespace Project\Rma\Plugin\Magento\Rma\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\State;
use Magento\Rma\Helper\Data as PluggedClass;
use Magento\Rma\Model\Product\Source;
use Project\Core\Helper\AttributeSet;

/**
 * Class DataPlugin
 * @package Project\Rma\Plugin\Magento\Rma\Helper
 */
class DataPlugin
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Project\Core\Helper\AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    const XML_PATH_PRODUCTS_RETURN_ALLOWED = 'sales/magento_rma/allowed_skus_rma';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * DataPlugin constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Project\Core\Helper\AttributeSet $attributeSetHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        State $state,
        AttributeSet $attributeSetHelper,
        ProductRepositoryInterface $productRepository
    ) {
        $this->state = $state;
        $this->attributeSetHelper = $attributeSetHelper;
        $this->productRepository = $productRepository;
        $this->scopeConfig = $context->getScopeConfig();

    }

    /**
     * @param \Magento\Rma\Helper\Data $subject
     * @param \Magento\Catalog\Model\Product $product
     * @param int|null $storeId
     * @return array
     */
    public function beforeCanReturnProduct(
        PluggedClass $subject,
        $product,
        $storeId = null
    ) {
        try {
            $product = $this->productRepository->getById($product->getId());
            $sku = $product->getSku();
            if($sku !== null && $this->_isAllowedReturnProduct($sku)) {
                if ($this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML) {
                        $product->setIsReturnable(Source::ATTRIBUTE_ENABLE_RMA_YES);
                } elseif ($this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_FRONTEND) {
                        $product->setIsReturnable(Source::ATTRIBUTE_ENABLE_RMA_NO);
                }
            }else {
                if ($this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML) {
                    if ($this->attributeSetHelper->isPerfumes($product)) {
                        $product->setIsReturnable(Source::ATTRIBUTE_ENABLE_RMA_NO);
                    }
                } elseif ($this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_FRONTEND) {
                    if ($this->attributeSetHelper->isBeauty($product)) {
                        $product->setIsReturnable(Source::ATTRIBUTE_ENABLE_RMA_NO);
                    }
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // do nothing
        }

        return [$product, $storeId];
    }

    public function _isAllowedReturnProduct($sku) {
       $skus = $this->scopeConfig->getValue(self::XML_PATH_PRODUCTS_RETURN_ALLOWED);
       $skus = explode(';',$skus);
       if(\is_array($skus) && \in_array($sku,$skus)){
           return true;
       }
       return false;
    }
}
