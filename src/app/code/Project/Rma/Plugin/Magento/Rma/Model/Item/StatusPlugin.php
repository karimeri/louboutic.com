<?php

namespace Project\Rma\Plugin\Magento\Rma\Model\Item;

use Magento\Rma\Model\Item\Attribute\Source\Status;
use Magento\Rma\Model\Item\Status as PluggedClass;

/**
 * Class StatusPlugin
 * @package Project\Rma\Plugin\Magento\Rma\Model\Item
 * @author Synolia <contact@synolia.com>
 */
class StatusPlugin
{
    /**
     * @param \Magento\Rma\Model\Item\Status $subject
     * @param $attribute
     * @return string
     */
    public function aroundGetBorderStatus(
        PluggedClass $subject,
        callable $proceed,
        $attribute
    ) {
        $result = $proceed($attribute);

        if ($attribute == 'condition') {
            return Status::STATE_APPROVED;
        }

        return $result;
    }
}
