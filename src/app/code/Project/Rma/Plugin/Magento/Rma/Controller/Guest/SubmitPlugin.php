<?php

namespace Project\Rma\Plugin\Magento\Rma\Controller\Guest;

use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\UrlInterface;

/**
 * Class SubmitPlugin
 * @package Project\Rma\Plugin\Magento\Rma\Controller\Guest
 */
class SubmitPlugin
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $_redirect;

    public function __construct(
        UrlInterface $url,
        RedirectFactory $resultRedirectFactory,
        RedirectInterface $redirect
    ) {
        $this->_url = $url;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->_redirect = $redirect;
    }

    public function afterExecute(\Magento\Rma\Controller\Guest\Submit $subject, $result)
    {
        $url = $this->_url->getUrl(\Project\Rma\Helper\Magento\Sales\Guest::DEFAULT_PATH);
        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->success($url));
    }
}
