<?php
namespace Project\Rma\Plugin\Magento\AdvancedCheckout\Block\Adminhtml\Sales\Order\Create\Sku;

use Magento\Framework\App\Response\RedirectInterface;
use Magento\AdvancedCheckout\Block\Adminhtml\Sales\Order\Create\Sku\Errors;

/**
 * Class ErrorsPlugin
 * @package Project\Rma\Plugin\Magento\AdvancedCheckout\Block\Adminhtml\Sales\Order\Create\Sku
 */
class ErrorsPlugin
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $sessionQuote;

    /**
     * ErrorsPlugin constructor.
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        RedirectInterface $redirect,
        \Magento\Backend\Model\Session\Quote $sessionQuote
    ) {
        $this->request = $request;
        $this->redirect = $redirect;
        $this->sessionQuote = $sessionQuote;
    }

    /**
     * @param \Magento\AdvancedCheckout\Block\Adminhtml\Sales\Order\Create\Sku\Errors $subject
     * @param string $result
     * @return string
     * @throws \Zend_Controller_Request_Exception
     * @throws \Zend_Controller_Router_Exception
     */
    public function afterGetConfigureUrl(Errors $subject, $result)
    {
        $request = $subject->getRequest();
        //$request = new \Zend_Controller_Request_Http($this->redirect->getRefererUrl());
        //\Zend_Controller_Front::getInstance()->getRouter()->route($request);
        $params = array_merge(
            $this->request->getParams(),
            $request->getParams()
        );

        $this->request->setParams($params);

        if (isset($params['creditmemo_id'])) {
            return $subject->getUrl(
                'sales/order_create/configureProductToAdd',
                [
                    'order_id' => $params['order_id'],
                    'rma_id' => $params['rma_id'],
                    'creditmemo_id' => $params['creditmemo_id'],
                ]
            );
        }

        return $result;
    }
}
