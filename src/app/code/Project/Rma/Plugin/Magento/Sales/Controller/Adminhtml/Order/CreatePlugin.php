<?php
namespace Project\Rma\Plugin\Magento\Sales\Controller\Adminhtml\Order;

use Magento\Framework\App\Response\RedirectInterface;
use Magento\Sales\Controller\Adminhtml\Order\Create;

/**
 * Class CreatePlugin
 *
 * @package Project\Rma\Plugin\Magento\Sales\Controller\Adminhtml\Order\Create
 * @author Synolia <contact@synolia.com>
 */
class CreatePlugin
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $sessionQuote;

    /**
     * CreatePlugin constructor.
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        RedirectInterface $redirect,
        \Magento\Backend\Model\Session\Quote $sessionQuote
    ) {
        $this->request = $request;
        $this->redirect = $redirect;
        $this->sessionQuote = $sessionQuote;
    }

    /**
     * @param \Magento\Sales\Controller\Adminhtml\Order\Create $subject
     * @throws \Zend_Controller_Request_Exception
     * @throws \Zend_Controller_Router_Exception
     */
    public function beforeExecute(Create $subject)
    {
        $request = $subject->getRequest();
        
        //$request = new \Zend_Controller_Request_Http($this->redirect->getRefererUrl());
        //\Zend_Controller_Front::getInstance()->getRouter()->route($request);
        $params = array_merge(
            $this->request->getParams(),
            $request->getParams()
        );

        $this->request->setParams($params);

        if (isset($params['creditmemo_id'])) {
            $this->sessionQuote->getQuote()
                ->setOrderId($params['order_id'])
                ->setRmaId($params['rma_id'])
                ->setCreditmemoId($params['creditmemo_id'])->save();
        }
    }
}
