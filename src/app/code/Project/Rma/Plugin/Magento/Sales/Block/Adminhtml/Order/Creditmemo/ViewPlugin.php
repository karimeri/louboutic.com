<?php

namespace Project\Rma\Plugin\Magento\Sales\Block\Adminhtml\Order\Creditmemo;

use Magento\Sales\Block\Adminhtml\Order\Creditmemo\View;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\Sales\Model\OrderFactory;

/**
 * Class ViewPlugin
 * @package Project\Rma\Plugin\Magento\Sales\Block\Adminhtml\Order\Creditmemo
 * @author  Synolia <contact@synolia.com>
 */
class ViewPlugin
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order
     */
    protected $orderResource;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * ViewPlugin constructor.
     *
     * @param \Magento\Sales\Model\ResourceModel\Order $orderResource
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     */
    public function __construct(
        Order $orderResource,
        OrderFactory $orderFactory
    ) {
        $this->orderResource = $orderResource;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @param \Magento\Sales\Block\Adminhtml\Order\Creditmemo\View $subject
     * @param string $result
     *
     * @return null
     */
    public function afterGetBackUrl(View $subject, string $result)
    {
        $creditmemo = $subject->getCreditmemo();
        $order = $creditmemo->getOrder();

        if ($order->getStatus() === \Project\Sales\Model\Magento\Sales\Order::STATE_CREDITMEMO_WAITING_EXCHANGE) {
            $urlOrderCreate = $subject->getUrl(
                'sales/order_create/index/',
                [
                    'customer_id' => $order->getCustomerId(),
                    'store_id' => $order->getStoreId(),
                    'order_id' => $order->getId(),
                    'creditmemo_id' => $creditmemo->getId(),
                    'rma_id' => $creditmemo->getRmaId(),
                ]
            );

            $subject->addButton(
                'create_order',
                [
                    'label' => __('Create order exchange'),
                    'onclick' => 'setLocation(\'' . $urlOrderCreate . '\')',
                    'class' => 'create_order',
                ],
                -1
            );
        }

        return $subject->getUrl('*/*/');
    }
}
