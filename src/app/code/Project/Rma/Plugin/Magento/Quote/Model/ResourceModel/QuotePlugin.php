<?php

namespace Project\Rma\Plugin\Magento\Quote\Model\ResourceModel;

use Magento\Quote\Model\ResourceModel\Quote as QuoteResource;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order\CreditmemoRepository;
use Magento\Sales\Model\OrderRepository;

/**
 * Class QuotePlugin
 *
 * @package Project\Rma\Plugin\Magento\Quote\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class QuotePlugin
{
    /**
     * @var \Magento\Sales\Model\Order\CreditmemoRepository
     */
    protected $creditmemoRepository;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * QuotePlugin constructor.
     *
     * @param \Magento\Sales\Model\Order\CreditmemoRepository $creditmemoRepository
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     */
    public function __construct(
        CreditmemoRepository $creditmemoRepository,
        OrderRepository $orderRepository
    ) {
        $this->creditmemoRepository = $creditmemoRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param \Magento\Quote\Model\ResourceModel\Quote $subject
     * @param callable $proceed
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return null
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundGetReservedOrderId(QuoteResource $subject, callable $proceed, Quote $quote)
    {
        if ($quote->getCreditmemoId()) {
            $creditMemo = $this->creditmemoRepository->get($quote->getCreditmemoId());
            $order = $this->orderRepository->get($creditMemo->getOrderId());

            return sprintf('%s-1', $order->getIncrementId());
        }

        return $proceed($quote);
    }
}
