<?php

namespace Project\Rma\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\Rma\Model\Rma;

/**
 * Class Config
 * @package Project\Rma\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_RMA_LIMIT_DELAY = 'louboutin_sales/rma/limit_delay';
    const XML_PATH_RMA_RETRY_LIMIT = 'louboutin_sales/rma/retry_limit';
    const XML_PATH_RMA_PICKUP_START = 'louboutin_sales/rma/pickup_start';
    const XML_PATH_RMA_PICKUP_END = 'louboutin_sales/rma/pickup_end';
    const XML_PATH_RMA_ESTIMATED_WEIGHT = 'louboutin_sales/rma/estimated_weight';
    const XML_PATH_RMA_UPS_REST_URL = 'louboutin_sales/rma/ups_rest_url';
    const XML_PATH_RMA_UPS_CANCEL_REST_URL = 'louboutin_sales/rma/ups_cancel_rest_url';
    const XML_PATH_RMA_UPS_ACCESS_LICENSE_NUMBER = 'louboutin_sales/rma/ups_access_license_number';
    const XML_PATH_RMA_UPS_USERNAME = 'louboutin_sales/rma/ups_username';
    const XML_PATH_RMA_UPS_PASSWORD = 'louboutin_sales/rma/ups_password';

    const XML_PATH_RMA_UPS_SHIPPER_NAME = 'louboutin_sales/rma/ups_shipper/name';
    const XML_PATH_RMA_UPS_SHIPPER_COMPANY = 'louboutin_sales/rma/ups_shipper/company';
    const XML_PATH_RMA_UPS_SHIPPER_STREET1 = 'louboutin_sales/rma/ups_shipper/street1';
    const XML_PATH_RMA_UPS_SHIPPER_STREET2 = 'louboutin_sales/rma/ups_shipper/street2';
    const XML_PATH_RMA_UPS_SHIPPER_STREET3 = 'louboutin_sales/rma/ups_shipper/street3';
    const XML_PATH_RMA_UPS_SHIPPER_CITY = 'louboutin_sales/rma/ups_shipper/city';
    const XML_PATH_RMA_UPS_SHIPPER_COUNTRY = 'louboutin_sales/rma/ups_shipper/country';
    const XML_PATH_RMA_UPS_SHIPPER_REGION = 'louboutin_sales/rma/ups_shipper/region';
    const XML_PATH_RMA_UPS_SHIPPER_POSTCODE = 'louboutin_sales/rma/ups_shipper/postcode';
    const XML_PATH_RMA_UPS_SHIPPER_CONTACT_NAME = 'louboutin_sales/rma/ups_shipper/contact_name';
    const XML_PATH_RMA_UPS_SHIPPER_CONTACT_PHONE = 'louboutin_sales/rma/ups_shipper/contact_phone';
    const XML_PATH_RMA_UPS_SHIPPER_ESTABLISHMENT_CODE = 'louboutin_sales/rma/ups_shipper/establishment_code';

    const XML_PATH_YASUMI_CLASSNAME = 'yasumi/classname';

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Config constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        EncryptorInterface $encryptor,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);

        $this->encryptor = $encryptor;
        $this->storeManager = $storeManager;
    }

    /**
     * @param int|null $scopeCode
     * @return int
     */
    public function getRmaLimitDelay($scopeCode = null)
    {
        return (int)$this->scopeConfig->getValue(
            $this::XML_PATH_RMA_LIMIT_DELAY,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @return int
     */
    public function getRmaRetryLimit()
    {
        return (int)$this->scopeConfig->getValue(
            $this::XML_PATH_RMA_RETRY_LIMIT
        );
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function isInDelays($order)
    {
        $compareDate = new \DateTime();
        $compareDate->sub(
            new \DateInterval(
                \sprintf('P%dD', $this->getRmaLimitDelay())
            )
        );

        foreach ($order->getInvoiceCollection() as $invoice) {
            $invoiceDate = new \DateTime($invoice->getCreatedAt());

            if ($invoiceDate >= $compareDate) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int|null $scopeCode
     * @return array
     */
    public function getPickupDayInterval($scopeCode = null)
    {
        $pickupStart = $this->scopeConfig->getValue(
            $this::XML_PATH_RMA_PICKUP_START,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );

        $pickupEnd = $this->scopeConfig->getValue(
            $this::XML_PATH_RMA_PICKUP_END,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );

        if (\is_null($pickupStart) || \is_null($pickupEnd)) {
            return [];
        }

        return [
            'start' => (int)$pickupStart,
            'end' => (int)$pickupEnd,
        ];
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getYasumiClassName($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            $this::XML_PATH_YASUMI_CLASSNAME,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @return int
     */
    public function getRmaEstimatedWeight()
    {
        return (int)$this->scopeConfig->getValue(
            $this::XML_PATH_RMA_ESTIMATED_WEIGHT
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsRestUrl($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_REST_URL,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsCancelRestUrl($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_CANCEL_REST_URL,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsAccessLicenseNumber($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_ACCESS_LICENSE_NUMBER,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsUsername($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_USERNAME,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsPassword($scopeCode = null)
    {
        $password = (string) $this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_PASSWORD,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );

        return $this->encryptor->decrypt($password);
    }

    /**
     * @param Rma $rma
     * @return bool|string
     */
    public function getRmaStoreCode(Rma $rma)
    {
        foreach ($this->storeManager->getStores() as $store) {
            if ($store->getId() === $rma->getStoreId()) {
                return $store->getCode();
            }
        }

        return false;
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperName($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_NAME,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperCompany($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_COMPANY,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperStreet1($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_STREET1,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperStreet2($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_STREET2,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperStreet3($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_STREET3,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperCity($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_CITY,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperCountry($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_COUNTRY,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperRegion($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_REGION,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperPostcode($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_POSTCODE,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperContactName($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_CONTACT_NAME,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperContactPhone($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_CONTACT_PHONE,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return string
     */
    public function getRmaUpsShipperEstablishmentCode($scopeCode = null)
    {
        return (string)$this->scopeConfig->getValue(
            self::XML_PATH_RMA_UPS_SHIPPER_ESTABLISHMENT_CODE,
            ScopeInterface::SCOPE_STORES,
            $scopeCode
        );
    }
}
