<?php
namespace Project\Rma\Helper\Magento\Sales;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\View\Result\Page;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Helper\Guest as GuestBase;

/**
 * Class Guest
 * @package Project\Rma\Helper\Magento\Rma
 * @author  Synolia <contact@synolia.com>
 */
class Guest extends GuestBase
{
    const DEFAULT_PATH = 'rma/order/search';

    /**
     * @var string
     */
    protected $inputExceptionMessageFix = 'You entered incorrect data. Please try again.';

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepositoryFix;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilderFix;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManagerFix;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * Guest constructor.
     * @param Context                     $context
     * @param StoreManagerInterface       $storeManager
     * @param Registry                    $coreRegistry
     * @param Session                     $customerSession
     * @param CookieManagerInterface      $cookieManager
     * @param CookieMetadataFactory       $cookieMetadataFactory
     * @param ManagerInterface            $messageManager
     * @param OrderFactory                $orderFactory
     * @param RedirectFactory             $resultRedirectFactory
     * @param OrderRepositoryInterface    $orderRepository
     * @param SearchCriteriaBuilder       $searchCriteria
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Registry $coreRegistry,
        Session $customerSession,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        ManagerInterface $messageManager,
        OrderFactory $orderFactory,
        RedirectFactory $resultRedirectFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteria,
        CustomerRepositoryInterface $customerRepository
    ) {
        parent::__construct(
            $context,
            $storeManager,
            $coreRegistry,
            $customerSession,
            $cookieManager,
            $cookieMetadataFactory,
            $messageManager,
            $orderFactory,
            $resultRedirectFactory,
            $orderRepository,
            $searchCriteria
        );
        $this->customerRepository       = $customerRepository;
        $this->orderRepositoryFix       = $orderRepository;
        $this->searchCriteriaBuilderFix = $searchCriteria;
        $this->storeManagerFix          = $storeManager;
    }

    /**
     * @param RequestInterface $request
     * @return Redirect|bool
     */
    public function checkEmail(RequestInterface $request)
    {
        $post = $request->getPostValue();

        try {
            $this->customerRepository->get(
                $post['order_email'] ?? '',
                $this->storeManagerFix->getStore()->getId()
            );

            $this->messageManager->addErrorMessage(__('We invite you to log into your account to organize the return of your order.'));
            return $this->resultRedirectFactory->create()->setPath($this::DEFAULT_PATH);
        } catch (NoSuchEntityException $exception) {
            return false;
        }
    }

    /**
     * Try to load valid order by $_POST or $_COOKIE
     *
     * @param RequestInterface $request
     * @return Redirect|bool
     * @throws \RuntimeException
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function loadValidOrder(RequestInterface $request)
    {
        $post = $request->getPostValue();
        $fromCookie = $this->cookieManager->getCookie($this::COOKIE_NAME);

        if (empty($post) && !$fromCookie) {
            return $this->resultRedirectFactory->create()->setPath($this::DEFAULT_PATH);
        }

        try {
            $order = (!empty($post) && isset($post['order_id']))
                ? $this->loadFromPost($post)
                : $this->loadFromCookie($fromCookie);

            $this->validateOrderStoreId($order->getStoreId());
            $this->coreRegistry->register('current_order', $order);

            return true;
        } catch (InputException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
            return $this->resultRedirectFactory->create()->setPath($this::DEFAULT_PATH);
        }
    }

    /**
     * Get Breadcrumbs for current controller action
     *
     * @param Page $resultPage
     * @return void
     */
    public function getBreadcrumbs(Page $resultPage)
    {
        $breadcrumbs = $resultPage->getLayout()->getBlock('breadcrumbs');

        if (!$breadcrumbs) {
            return;
        }

        $breadcrumbs->addCrumb(
            'home',
            [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->storeManagerFix->getStore()->getBaseUrl()
            ]
        );

        $breadcrumbs->addCrumb(
            'cms_page',
            ['label' => __('Order Information'), 'title' => __('Order Information')]
        );
    }

    /**
     * Set guest-view cookie
     *
     * @param string $cookieValue
     * @return void
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    protected function setGuestViewCookie($cookieValue)
    {
        $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
            ->setPath($this::COOKIE_PATH)
            ->setHttpOnly(true);

        $this->cookieManager->setPublicCookie($this::COOKIE_NAME, $cookieValue, $metadata);
    }

    /**
     * Load order from cookie
     *
     * @param string $fromCookie
     * @return OrderInterface
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    protected function loadFromCookie($fromCookie)
    {
        // phpcs:ignore Ecg.Security.ForbiddenFunction
        $cookieData  = \explode(':', \base64_decode($fromCookie));
        $protectCode = isset($cookieData[0]) ? $cookieData[0] : null;
        $incrementId = isset($cookieData[1]) ? $cookieData[1] : null;

        if (!empty($protectCode) && !empty($incrementId)) {
            /** @var Order $order */
            $order = $this->getOrderRecord($incrementId);

            if (\hash_equals((string) $order->getProtectCode(), $protectCode)) {
                $this->setGuestViewCookie($fromCookie);
                return $order;
            }
        }

        throw new InputException(__($this->inputExceptionMessageFix));
    }

    /**
     * Load order data from post
     *
     * @param array $postData
     * @return Order
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    protected function loadFromPost(array $postData)
    {
        if ($this->hasPostDataEmptyFields($postData)) {
            throw new InputException(__($this->inputExceptionMessageFix));
        }

        /** @var Order $order */
        $order = $this->getOrderRecord($postData['order_id']);

        if (!$this->compareSoredBillingDataWithInput($order, $postData)) {
            throw new InputException(__($this->inputExceptionMessageFix));
        }

        $this->setGuestViewCookie(
            \base64_encode($order->getProtectCode() . ':' . $postData['order_id'])
        );

        return $order;
    }

    /**
     * Check that billing data from the order and from the input are equal
     *
     * @param Order $order
     * @param array $postData
     * @return bool
     */
    protected function compareSoredBillingDataWithInput(Order $order, array $postData)
    {
        $email = $postData['order_email'];
        $billingAddress = $order->getBillingAddress();

        return \strtolower($email) === \strtolower($billingAddress->getEmail());
    }

    /**
     * Check post data for empty fields
     *
     * @param array $postData
     * @return bool
     */
    protected function hasPostDataEmptyFields(array $postData)
    {
        return
            empty($postData['order_id']) ||
            empty($this->storeManagerFix->getStore()->getId()) ||
            empty($postData['order_email']);
    }

    /**
     * Get order by increment_id and store_id
     *
     * @param string $incrementId
     * @return OrderInterface
     * @throws InputException
     */
    protected function getOrderRecord($incrementId)
    {
        $records = $this->orderRepositoryFix->getList(
            $this->searchCriteriaBuilderFix
                ->addFilter('increment_id', $incrementId)
                ->create()
        );

        if ($records->getTotalCount() < 1) {
            throw new InputException(__($this->inputExceptionMessageFix));
        }

        $items = $records->getItems();

        return \array_shift($items);
    }

    /**
     * Check that store_id from order are equals with system
     *
     * @param int $orderStoreId
     * @return void
     * @throws InputException
     */
    protected function validateOrderStoreId($orderStoreId)
    {
        if ($orderStoreId !== $this->storeManagerFix->getStore()->getId()) {
            throw new InputException(__($this->inputExceptionMessageFix));
        }
    }
}
