<?php
namespace Project\Rma\Helper\Magento\Rma;

use Magento\Backend\Model\Auth\Session as BackSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Model\Quote\AddressFactory;
use Magento\Rma\Helper\Data as DataBase;
use Magento\Rma\Model\ResourceModel\ItemFactory;
use Magento\Sales\Model\Order\Admin\Item as AdminOrderItem;
use Magento\Shipping\Helper\Carrier;
use Magento\Shipping\Model\CarrierFactory;
use Magento\Store\Model\StoreManagerInterface;
use Project\Rma\Model\Rma;

/**
 * Class Data
 * @package Project\Rma\Helper\Magento\Rma
 * @author  Synolia <contact@synolia.com>
 */
class Data extends DataBase
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Data constructor.
     * @param Context               $context
     * @param CountryFactory        $countryFactory
     * @param RegionFactory         $regionFactory
     * @param StoreManagerInterface $storeManager
     * @param TimezoneInterface     $localeDate
     * @param ItemFactory           $itemFactory
     * @param CustomerSession       $customerSession
     * @param BackSession           $authSession
     * @param AddressFactory        $addressFactory
     * @param CarrierFactory        $carrierFactory
     * @param FilterManager         $filterManager
     * @param DateTime              $dateTime
     * @param AdminOrderItem        $adminOrderItem
     * @param Carrier               $carrierHelper
     * @param Registry              $coreRegistry
     */
    public function __construct(
        Context $context,
        CountryFactory $countryFactory,
        RegionFactory $regionFactory,
        StoreManagerInterface $storeManager,
        TimezoneInterface $localeDate,
        ItemFactory $itemFactory,
        CustomerSession $customerSession,
        BackSession $authSession,
        AddressFactory $addressFactory,
        CarrierFactory $carrierFactory,
        FilterManager $filterManager,
        DateTime $dateTime,
        AdminOrderItem $adminOrderItem,
        Carrier $carrierHelper,
        Registry $coreRegistry
    ) {
        parent::__construct(
            $context,
            $countryFactory,
            $regionFactory,
            $storeManager,
            $localeDate,
            $itemFactory,
            $customerSession,
            $authSession,
            $addressFactory,
            $carrierFactory,
            $filterManager,
            $dateTime,
            $adminOrderItem,
            $carrierHelper
        );
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @override
     * @param int|null $store
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReturnAddressData($store = null)
    {
        /** @var Rma|null $rma */
        $rma = $this->coreRegistry->registry('current_rma');

        if (empty($rma)) {
            return parent::getReturnAddressData($store);
        }

        $address = $rma->getReturnAddress();

        $data = [
            'city' => $address->getCity(),
            'countryId' => $address->getCountryId(),
            'country' => $this->_countryFactory->create()->loadByCode($address->getCountryId())->getName(),
            'postcode' => $address->getPostcode(),
            'region' => $address->getRegion(),
            'street1' => $address->getStreet1(),
            'street2' => $address->getStreet2(),
            'street3' => $address->getStreet3(),
            'company' => $address->getCompany(),
            'telephone' => $address->getContactPhone(),
        ];

        return $data;
    }

    /**
     * @override
     * @param int|null $storeId
     * @return DataObject
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReturnContactName($storeId = null)
    {
        /** @var Rma|null $rma */
        $rma = $this->coreRegistry->registry('current_rma');

        if (empty($rma)) {
            return parent::getReturnContactName($storeId);
        }

        $address = $rma->getReturnAddress();

        $contactName = new DataObject();
        $contactName
            ->setFirstName('')
            ->setLastName($address->getContactName())
            ->setName($address->getContactName());

        return $contactName;
    }
}
