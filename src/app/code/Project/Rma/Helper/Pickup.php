<?php
namespace Project\Rma\Helper;

use DHL\Client\Web as WebserviceClient;
use DHL\Datatype\GB\Pickup as PickupRequest;
use DHL\Datatype\GB\PickupContact;
use DHL\Datatype\GB\PlaceProject;
use DHL\Datatype\GB\Requestor;
use DHL\Entity\GB\BookPURequestProject;
use DHL\Entity\GB\CancelPURequestProject;
use Magento\Rma\Model\Rma\Source\Status;
use Project\Rma\Model\Api\UpsConnector;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Store\Model\ScopeInterface;
use Synolia\Logger\Model\LoggerFactory;
use UPS\Datatype\Pickup\PickupCreationRequest;
use UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress;
use UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress\Phone;
use UPS\Datatype\Pickup\PickupCreationRequest\PickupDateInfo;
use UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece;
use UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece\Item;
use UPS\Datatype\Pickup\PickupCreationRequest\TotalWeight;
use UPS\Datatype\Pickup\PickupCreationRequest\Notification;
use Yasumi\Yasumi;
use Project\Rma\Model\Rma;
use Magento\Rma\Helper\Data;

/**
 * Class Pickup
 * @package Project\Rma\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Pickup extends AbstractHelper
{
    const LOG_DIRECTORY = 'dhl_pickup';
    const LOG_FILE = 'request_response.log';
    const ACCESS_POINT = 'access_point';

    const STORES_POUND_INCH = [
        'us_en' => true,
    ];

    /**
     * @var Config
     */
    protected $rmaHelper;

    /**
     * @var Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    protected $stringUtils;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Magento\Rma\Helper\Data
     */
    protected $rmaHelperData;

    /**
     * @var \Project\Rma\Model\Api\UpsConnector
     */
    protected $upsConnector;

    /**
     * Pickup constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Project\Rma\Helper\Config $rmaHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Stdlib\StringUtils $stringUtils
     * @param \Project\Rma\Model\Api\UpsConnector $upsConnector
     * @param \Magento\Rma\Helper\Data $rmaHelperData
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context,
        Config $rmaHelper,
        TimezoneInterface $timezone,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        StringUtils $stringUtils,
        UpsConnector $upsConnector,
        Data $rmaHelperData
    ) {
        parent::__construct($context);
        $this->rmaHelper = $rmaHelper;
        $this->timezone = $timezone;
        $logPath = $directoryList->getPath('log') . DIRECTORY_SEPARATOR . $this::LOG_DIRECTORY . DIRECTORY_SEPARATOR;
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath . $this::LOG_FILE]
        );
        $this->stringUtils = $stringUtils;
        $this->upsConnector = $upsConnector;
        $this->rmaHelperData = $rmaHelperData;
    }

    /**
     * @param Rma $rma
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getShippingMethod(Rma $rma)
    {
        $methods = $rma->getShippingMethods();
        $method = null;
        if(is_array($methods)){
            $method = \array_shift($methods);
        }


        return $method;
    }

    /**
     * @param int|null $scopeCode
     * @return array
     * @throws \ReflectionException
     */
    public function getAllowedDates($scopeCode = null)
    {
        $dateInterval = $this->rmaHelper->getPickupDayInterval($scopeCode);

        if (empty($dateInterval)) {
            return [];
        }

        $dates        = [];
        $dayCursor    = $dateInterval['start'];
        $holidays     = Yasumi::create($this->rmaHelper->getYasumiClassName($scopeCode), \date('Y'));
        $daysInterval = $dateInterval['end'] - $dateInterval['start'];

        do {
            $date = new \DateTime();
            $date->add(new \DateInterval(\sprintf('P%dD', $dayCursor)));

            $dayCursor++;

            if ($holidays->isWorkingDay($date) === false) {
                continue;
            }

            $dates[] = $date->format('Y-m-d');
        } while (\count($dates) <= $daysInterval);

        return $dates;
    }

    /**
     * @param Rma $rma
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function requestDhl(Rma $rma)
    {
        $dateMessage = new \DateTime();
        $rmaAddress = $rma->getRmaAddress();

        $request = new BookPURequestProject();
        $request->MessageTime = $dateMessage->format(\DateTime::W3C);
        $request->MessageReference = \md5(\uniqid('', true).\uniqid('', true).\uniqid('', true));
        $request->SiteID = $this->getCarrierConfigValue('id');
        $request->Password = $this->getCarrierConfigValue('password');

        $request->RegionCode = 'EU';

        $requestor = new Requestor();
        $requestor->AccountType = 'D';
        $requestor->AccountNumber = $this->getCarrierConfigValue('account');
        $request->Requestor = $requestor;

        $place = new PlaceProject();
        $place->LocationType = 'R';
        $place->CompanyName = $rmaAddress->getCompany();
        $place->Address1 = $this->stringUtils->substr($rmaAddress->getStreetLine(1), 0, 35);
        $place->PackageLocation = ' ';
        $place->City = $rmaAddress->getCity();
        $place->CountryCode = $rmaAddress->getCountryId();
        $place->PostalCode = $rmaAddress->getPostcode();
        $request->Place = $place;

        $pickup = new PickupRequest();
        $pickup->PickupDate = $rma->getPickupDate();
        $pickup->ReadyByTime = '10:00';
        $pickup->CloseTime = '17:00';
        $request->Pickup = $pickup;

        $contact = new PickupContact();
        $contact->PersonName = $rmaAddress->getFirstname() . ' ' . $rmaAddress->getLastname();
        $contact->Phone = $rmaAddress->getTelephone();
        $request->PickupContact = $contact;

        $this->logger->addInfo($request->toXML());

        $client = new WebserviceClient($this->getCarrierConfigValue('demo_mode'));
        $response = $client->call($request);

        $this->logger->addInfo($response);

        $xml = new \SimpleXMLElement($response);

        $confirmationNumber = $xml->xpath('/res:BookPUResponse/ConfirmationNumber');

        if (!empty($confirmationNumber)) {
            return \reset($confirmationNumber[0]);
        }

        $noteResponse = $xml->xpath('/res:PickupErrorResponse/Response/Status/Condition/ConditionData');

        if (!empty($noteResponse)) {
            throw new \Exception(__(\reset($noteResponse[0])));
        }

        $noteResponse = $xml->xpath('/res:ErrorResponse/Response/Status/Condition/ConditionData');

        if (!empty($noteResponse)) {
            throw new \Exception(__(\reset($noteResponse[0])));
        }

        throw new \Exception(__('Undefined error, please contact an administrator to see logs'));
    }

    /**
     * @param Rma $rma
     * @return string mixed
     * @throws \Exception
     */
    public function requestUps(Rma $rma)
    {
        $storeId = $rma->getStoreId();

        try {
            $response = $this->upsConnector->requestPickup(
                $storeId,
                $this->getPickupBody($rma)
            );

            $this->logger->addInfo('ups response : '.print_r($response, true));

            return $response->PickupCreationResponse->PRN;
        } catch (\Exception $e) {
            $this->logger->addError('ups response error : '.$e->getMessage());
        }

        throw new \Exception(__('ups response error ') . $e->getMessage());
    }

    /**
     * @param \Magento\Rma\Model\Rma $rma
     *
     * @return false|string
     */
    public function getPickupBody(\Magento\Rma\Model\Rma $rma)
    {
        $order = $rma->getOrder();
        $address = $order->getShippingAddress();

        $pickupDateInfo = new PickupDateInfo();
        $pickupDateInfo->setPickupDate($rma->getPickupDate());


        $pickupAddress = new PickupAddress();
        $pickupAddress->setCompanyName($address->getCompany() ?: $address->getName());
        $address->setPrefix('');
        $pickupAddress->setContactName($address->getName());
        $pickupAddress->setAddressLine(implode(' ', $address->getStreet()));
        $pickupAddress->setCity($address->getCity());
        $pickupAddress->setStateProvince($address->getRegionCode());
        $pickupAddress->setPostalCode($address->getPostcode());
        $pickupAddress->setCountryCode($address->getCountryId());
        $phone = new Phone();
        $phone->setNumber($address->getTelephone());
        $pickupAddress->setPhone($phone);

        $pickupPiece = new PickupPiece();
        /** @var \Magento\Rma\Model\Item $rmaItem */
        foreach ($rma->getItemsForDisplay() as $rmaItem) {
            if (!in_array($rmaItem->getStatus(), [Status::STATE_DENIED, Status::STATE_REJECTED])) {
                $pickupPieceItem = new Item();
                // service code for ups standard
                $pickupPieceItem->setServiceCode('011');
                $pickupPieceItem->setQuantity($rmaItem->getQtyAuthorized());
                $pickupPieceItem->setDestinationCountryCode(
                    (string)$this->rmaHelperData->getReturnAddressData($rma->getStoreId())['countryId']
                );

                $pickupPiece->addItem($pickupPieceItem);
            }
        }

        $rmaStoreCode = $this->rmaHelper->getRmaStoreCode($rma);

        $totalWeight = new TotalWeight();
        $totalWeight->setWeight($this->rmaHelper->getRmaEstimatedWeight());
        $totalWeight->setUnitOfMeasurement(
            (isset(self::STORES_POUND_INCH[$rmaStoreCode])) ? 'LBS' : 'KGS'
        );

        $notification = new Notification();
        $notification->setConfirmationEmailAddress(
            $rma->getCustomerCustomEmail() ?: $order->getCustomerEmail()
        );
        $notification->setUndeliverableEmailAddress(
            $rma->getCustomerCustomEmail() ?: $order->getCustomerEmail()
        );

        // now we build the object
        $pickupCreationRequest = new PickupCreationRequest();
        $pickupCreationRequest->setPickupDateInfo($pickupDateInfo);
        $pickupCreationRequest->setPickupAddress($pickupAddress);
        $pickupCreationRequest->setPickupPiece($pickupPiece);
        $pickupCreationRequest->setTotalWeight($totalWeight);
        $pickupCreationRequest->setNotification($notification);

        return json_encode($pickupCreationRequest->toArray(), JSON_PRETTY_PRINT);
    }

    /**
     * @param Rma $rma
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function cancelDhl(Rma $rma)
    {
        $dateMessage = new \DateTime();
        $rmaAddress = $rma->getRmaAddress();

        $request = new CancelPURequestProject();
        $request->MessageTime = $dateMessage->format(\DateTime::W3C);
        $request->MessageReference = \md5(\uniqid('', true).\uniqid('', true).\uniqid('', true));
        $request->SiteID = $this->getCarrierConfigValue('id');
        $request->Password = $this->getCarrierConfigValue('password');

        $request->RegionCode = 'EU';
        $request->ConfirmationNumber = $rma->getPickupConfirmationNumber();
        $request->RequestorName = $this->getCarrierConfigValue('account');
        $request->CountryCode = $rmaAddress->getCountryId();
        $request->PickupDate = $rma->getPickupDate();
        $request->CancelTime = \date('H:i');

        $client = new WebserviceClient($this->getCarrierConfigValue('demo_mode'));
        $response = $client->call($request);

        $xml = new \SimpleXMLElement($response);

        $noteResponse = $xml->xpath('/res:PickupErrorResponse/Response/Status/Condition/ConditionData');

        if (empty($noteResponse)) {
            return true;
        }

        return \reset($noteResponse[0]);
    }

    /**
     * @param Rma $rma
     * @return string|array
     * @throws \Exception
     */
    public function cancelUps(Rma $rma)
    {
        $storeId = $rma->getStoreId();

        try {
            $response = $this->upsConnector->requestPickupCancel(
                $storeId,
                $rma->getPickupConfirmationNumber()
            );

            $this->logger->addInfo('ups response : '.print_r($response, true));

            if (isset($response->response->errors)) {
                $this->logger->addError('ups response error : '.print_r($response, true));

                return sprintf(
                    "%s %s",
                    $response->response->errors->code,
                    $response->response->errors->message
                );
            }

            return [];
        } catch (\Exception $e) {
            $this->logger->addError('ups response error : '.$e->getMessage());
        }

        throw new \Exception(__('Undefined error, please contact an administrator to see logs'));
    }

    /**
     * @param $field
     * @return mixed
     */
    protected function getCarrierConfigValue($field)
    {
        $configValue = $this->scopeConfig->getValue(
            'carriers/dhl/' . $field,
            ScopeInterface::SCOPE_STORE
        );

        if ($field === 'demo_mode') {
            $configValue = ((bool) $configValue) ? 'staging' : 'production';
        }

        return $configValue;
    }
}
