<?php

namespace Project\Rma\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface as InlineTranslation;
use Magento\Store\Model\Store;

/**
 * Class Alerting
 * @package Project\Rma\Helper
 */
class Alerting extends AbstractHelper
{
    const XML_PATH_ALERTING_ENABLED
        = 'louboutin_sales/rma/alerting_enabled';
    const XML_PATH_ALERTING_EMAIL_SENDER
        = 'louboutin_sales/rma/alerting_email_sender';
    const XML_PATH_ALERTING_EMAIL_RECIPIENT
        = 'louboutin_sales/rma/alerting_email_recipient';
    const XML_PATH_ALERTING_EMAIL_COPY
        = 'louboutin_sales/rma/alerting_email_copy';
    const XML_PATH_ALERTING_EMAIL_COPY_METHOD
        = 'louboutin_sales/rma/alerting_copy_method';
    const XML_PATH_ALERTING_EMAIL_TEMPLATE
        = 'louboutin_sales/rma/alerting_email_template';

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var InlineTranslation
     */
    protected $inlineTranslation;

    /**
     * Alerting constructor.
     * @param Context $context
     * @param TransportBuilder $transportBuilder
     * @param InlineTranslation $inlineTranslation
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        InlineTranslation $inlineTranslation
    ) {
        parent::__construct($context);
        $this->transportBuilder     = $transportBuilder;
        $this->inlineTranslation    = $inlineTranslation;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ALERTING_ENABLED
        );
    }

    /**
     * @param array $templateVars
     * @return $this
     */
    public function alert($templateVars)
    {
        try {
            $this->inlineTranslation->suspend();

            $sender = $this->scopeConfig->getValue(
                self::XML_PATH_ALERTING_EMAIL_SENDER
            );
            $sendFrom = [
                'email' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/email'

                ),
                'name' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $sender . '/name'
                ),
            ];

            $copyTo = $this->getEmails(self::XML_PATH_ALERTING_EMAIL_COPY);
            $copyMethod = $this->scopeConfig->getValue(
                self::XML_PATH_ALERTING_EMAIL_COPY_METHOD
            );
            $bcc = [];
            if ($copyTo && $copyMethod == 'bcc') {
                $bcc = $copyTo;
            }

            $sendTo = [
                [
                    'email' => $this->scopeConfig->getValue(
                        self::XML_PATH_ALERTING_EMAIL_RECIPIENT
                    ),
                    'name' => null,
                ]
            ];
            if ($copyTo && $copyMethod == 'copy') {
                foreach ($copyTo as $email) {
                    $sendTo[] = ['email' => $email, 'name' => null];
                }
            }

            $template = $this->scopeConfig->getValue(
                self::XML_PATH_ALERTING_EMAIL_TEMPLATE
            );
            foreach ($sendTo as $recipient) {
                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                        'store' => Store::DEFAULT_STORE_ID
                    ]
                )->setTemplateVars(
                    $templateVars
                )->setReplyTo(
                    $sendFrom['email'],
                    $sendFrom['name']
                )->setFromByScope(
                    $sendFrom
                )->addTo(
                    $recipient['email'],
                    $recipient['name']
                )->addBcc(
                    $bcc
                )->getTransport();

                $transport->sendMessage();
            }

            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }
        return $this;
    }

    /**
     * @param string $configPath
     * @return array|false
     */
    protected function getEmails($configPath)
    {
        $data = $this->scopeConfig->getValue(
            $configPath
        );
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }
}
