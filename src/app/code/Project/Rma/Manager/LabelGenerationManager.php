<?php

namespace Project\Rma\Manager;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote\Address\Rate;
use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Rma\Model\ResourceModel\Item as ItemResource;
use Magento\Rma\Model\Rma\Status\HistoryFactory;
use Magento\Rma\Model\Shipping;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Store\Model\StoreManagerInterface;
use Project\Pdf\Model\Magento\Sales\Rma\Pdf\ProformaInvoiceFactory;
use Project\Rma\Helper\Config as ConfigHelper;
use Project\Rma\Helper\Pickup as PickupHelper;
use Project\Rma\Helper\Pickup;
use Project\Rma\Model\Item;
use Project\Rma\Model\Magento\Rma\Rma\Source\Status;
use Project\Rma\Model\Rma;

/**
 * Class LabelGenerationManager
 * @package Project\Rma\Manager
 */
class LabelGenerationManager
{
    const CONTAINER = [
        'dhl' => 'N',
        'ups' => '00',
    ];

    /**
     * @var ItemResource
     */
    protected $itemResource;

    /**
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * @var ConfigHelper
     */
    protected $rmaConfigHelper;

    /**
     * @var PickupHelper
     */
    protected $pickupHelper;

    /**
     * @var RmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var LabelService
     */
    protected $labelService;

    /**
     * @var HistoryFactory
     */
    protected $rmaHistoryFactory;

    /**
     * @var Shipping
     */
    protected $shippingModel;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProformaInvoiceFactory
     */
    protected $invoiceFactory;

    /**
     * LabelGenerationManager constructor.
     * @param ResourceConnection $resource
     * @param ItemResource $itemResource
     * @param ConfigHelper $configHelper
     * @param Pickup $pickupHelper
     * @param RmaRepositoryInterface $rmaRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param LabelService $labelService
     * @param HistoryFactory $historyFactory
     * @param Shipping $shippingModel
     * @param StoreManagerInterface $storeManager
     * @param ProformaInvoiceFactory $proformaInvoiceFactory
     */
    public function __construct(
        ResourceConnection $resource,
        ItemResource $itemResource,
        ConfigHelper $configHelper,
        PickupHelper $pickupHelper,
        RmaRepositoryInterface $rmaRepository,
        PriceCurrencyInterface $priceCurrency,
        LabelService $labelService,
        HistoryFactory $historyFactory,
        Shipping $shippingModel,
        StoreManagerInterface $storeManager,
        ProformaInvoiceFactory $proformaInvoiceFactory
    ) {
        $this->itemResource         = $itemResource;
        $this->connection           = $resource->getConnection();
        $this->rmaConfigHelper      = $configHelper;
        $this->pickupHelper         = $pickupHelper;
        $this->rmaRepository        = $rmaRepository;
        $this->priceCurrency        = $priceCurrency;
        $this->labelService         = $labelService;
        $this->rmaHistoryFactory    = $historyFactory;
        $this->shippingModel        = $shippingModel;
        $this->storeManager         = $storeManager;
        $this->invoiceFactory       = $proformaInvoiceFactory;
    }

    /**
     * @param Rma $rma
     */
    public function setItemsToAuthorized(Rma $rma)
    {
        $itemsCollection = $rma->getItemsForDisplay();
        $itemsIds = [];
        /** @var \Magento\Rma\Model\Item $item */
        foreach ($itemsCollection as $item) {
            $itemsIds[] = $item->getId();
        }

        $this->connection->update(
            $this->itemResource->getTable('magento_rma_item_entity'),
            [
                Item::QTY_AUTHORIZED => new \Zend_Db_Expr(Item::QTY_REQUESTED),
                Item::STATUS => Status::STATE_AUTHORIZED,
            ],
            [Item::ENTITY_ID . ' IN (?)' => $itemsIds]
        );
    }

    /**
     * @param Rma $rma
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function setRmaToAuthorized(Rma $rma)
    {
        $rma->setStatus(Status::STATE_AUTHORIZED);
        $this->rmaRepository->save($rma);
    }

    /**
     * @param Rma  $rma
     * @param Rate $method
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPackageParams(Rma $rma, Rate $method)
    {
        $items = $rma->getShippingMethods(true);
        $paramsItems = [];
        $customValue = 0;
        foreach ($items as $item) {
            $paramsItems[$item->getId()] = [
                'qty' => $item->getQty(),
                'price' => $item->getPrice(),
                'customs_value' => $item->getPrice(),
                'name' => $rma->getIncrementId(),
                'weight' => $item->getWeight(),
                'product_id' => $item->getProductId(),
                'order_item_id' => $item->getItemId(),
            ];

            $customValue += $item->getPrice() * $item->getQty();
        }

        $rmaStoreCode = $this->rmaConfigHelper->getRmaStoreCode($rma);

        $params = [
            'code' => $method->getCode(),
            'carrier_title' => $method->getCarrierTitle(),
            'method_title' => (string) $method->getMethodTitle(),
            'price' => $this->priceCurrency->convert($method->getPrice(), true, false),
            'packages' => [
                1 => [
                    'params' => [
                        'container' => self::CONTAINER[\strtolower($method->getCarrier())],
                        'weight' => $this->rmaConfigHelper->getRmaEstimatedWeight(),
                        'customs_value' => $customValue,
                        'length' => '3',
                        'width' => '3',
                        'height' => '3',
                        'weight_units' => (isset(Pickup::STORES_POUND_INCH[$rmaStoreCode])) ? 'POUND' : 'KILOGRAM',
                        'dimension_units' => (isset(Pickup::STORES_POUND_INCH[$rmaStoreCode])) ? 'INCH' : 'CENTIMETER',
                        'content_type' => '',
                        'content_type_other' => '',
                    ],
                    'items' => $paramsItems,
                ],
            ],
        ];

        return $params;
    }

    /**
     * @param Rma   $rma
     * @param array $params
     * @throws LocalizedException
     */
    public function generateLabel(Rma $rma, array $params)
    {
        $this->labelService->createShippingLabel($rma, $params);
    }

    /**
     * @param Rma  $rma
     * @param Rate $method
     * @throws \Exception
     */
    public function pickupRequest(Rma $rma, Rate $method)
    {
        switch (\strtolower($method->getCarrier())) {
            case 'dhl':
                $confirmationNumber = $this->pickupHelper->requestDhl($rma);
                break;
            case 'ups':
                $confirmationNumber = $this->pickupHelper->requestUps($rma);
                break;
            default:
                return;
        }

        $rma->setPickupConfirmationNumber($confirmationNumber);
        $this->rmaRepository->save($rma);

        /** @var \Magento\Rma\Model\Rma\Status\History $statusHistory */
        $statusHistory = $this->rmaHistoryFactory->create();
        $statusHistory->setRmaEntityId($rma->getEntityId());
        $statusHistory->saveComment(__('Pickup confirmed with number "%1"', $confirmationNumber), false, true);
    }

    /**
     * @param Rma $rma
     * @param bool $hasAttachments
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Pdf_Exception
     */
    public function sendRmaEmail(Rma $rma, bool $hasAttachments)
    {
        if ($hasAttachments) {
            $labelContent = $this->shippingModel->getShippingLabelByRma($rma)->getShippingLabel();

            if (!$labelContent) {
                return;
            }

            if (\stripos($labelContent, '%PDF-') !== false) {
                $pdfContent = $labelContent;
            } else {
                $pdf = new \Zend_Pdf();
                $page = $this->labelService->createPdfPageFromImageString($labelContent);

                if (!$page) {
                    return;
                }

                $pdf->pages[] = $page;
                $pdfContent = $pdf->render();
            }

            $attachments = [
                [
                    'file_content' => $pdfContent,
                    'mime_type' => 'application/pdf',
                    'disposition' => \Zend_Mime::DISPOSITION_ATTACHMENT,
                    'encoding' => \Zend_Mime::ENCODING_BASE64,
                    'filename' => 'ShippingLabel(' . $rma->getIncrementId() . ').pdf',
                ]
            ];

            $store = $this->storeManager->getStore($rma->getStoreId());
            if (0 === strpos($store->getCode(), 'ch')) {
                $pdf = $this->invoiceFactory->create()->getPdf([$rma]);
                $attachment = $pdf->render();

                $attachments[] = [
                    'file_content' => $attachment,
                    'mime_type' => 'application/pdf',
                    'disposition' => \Zend_Mime::DISPOSITION_ATTACHMENT,
                    'encoding' => \Zend_Mime::ENCODING_BASE64,
                    'filename' => 'proforma-invoice-' . $rma->getIncrementId() . '.pdf',
                ];
            }
        } else {
            $attachments = [];
        }

        /** @var \Project\Rma\Model\Magento\Rma\Rma\Status\History $statusHistory */
        $statusHistory = $this->rmaHistoryFactory->create();
        $statusHistory->setRmaEntityId($rma->getEntityId());
        $statusHistory->sendAuthorizeEmail($attachments);
        $statusHistory->saveSystemComment();
    }
}
