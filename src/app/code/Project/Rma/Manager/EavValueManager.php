<?php

namespace Project\Rma\Manager;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Class EavValueManager
 *
 * @package Project\Rma
 * @author  Synolia <contact@synolia.com>
 */
class EavValueManager
{
    /**
     * @var AdapterInterface
     */
    protected $connection;

    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param string $attributeName
     * @param Item   $item
     *
     * @return string|null
     */
    public function getValueByAttributeAndItem($attributeName, $item)
    {
        $optionId = $this->getOptionIdByAttributeNameAndItem($attributeName, $item);
        $bind     = ['option_id' => (int)$optionId];
        $select   = $this->connection->select()->from('eav_attribute_option_value')
            ->where('option_id = :option_id')
            ->limit(1)
        ;

        return $this->connection->fetchRow($select, $bind)['value'];
    }

    /**
     * @param string $attributeName
     * @param Item   $item
     *
     * @return int|null
     */
    public function getOptionIdByAttributeNameAndItem($attributeName, $item)
    {
        $attribute        = $item->getAttribute($attributeName);
        $this->connection = $attribute->getResource()->getConnection();
        $bind             = [
            'attribute_id' => (int)$attribute->getId(),
            'entity_id'    => (int)$item->getId(),
        ];
        $select           = $this->connection->select()->from('magento_rma_item_entity_int')
            ->where('attribute_id = :attribute_id')
            ->where('entity_id = :entity_id')
            ->limit(1)
        ;

        return $this->connection->fetchRow($select, $bind)['value'];
    }
}
