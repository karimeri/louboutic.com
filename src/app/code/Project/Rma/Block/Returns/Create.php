<?php
namespace Project\Rma\Block\Returns;

use Magento\Eav\Model\Config;
use Magento\Eav\Model\Form\Factory as FormFactory;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Data\Collection\ModelFactory;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Base64Json;
use Magento\Framework\View\Element\Template\Context;
use Magento\Rma\Block\Returns\Create as CreateBase;
use Magento\Rma\Helper\Data;
use Magento\Rma\Model\Item\FormFactory as ItemFormFactory;
use Magento\Rma\Model\ItemFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Sales\Model\Order\AddressFactory;
use Magento\Store\Model\StoreManager;
use Project\Rma\Controller\Rmaaddress\Create as RmaAddressCreate;
use Project\Rma\Helper\Pickup as PickupHelper;
use Us\Core\Setup\UpgradeData as UsUpgradeData;

/**
 * Class Create
 *
 * @package Project\Rma\Block\Returns
 * @author  Synolia <contact@synolia.com>
 */
class Create extends CreateBase
{
    /**
     * @var Http
     */
    protected $http;

    /**
     * @var PickupHelper
     */
    protected $pickupHelper;

    /**
     * @var \Magento\Sales\Model\Order\AddressFactory
     */
    protected $addressFactory;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Base64Json
     */
    protected $base64Json;

    /**
     * @var StoreManager $storeManager
     */
    protected $storeManager;

    /**
     * Create constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Collection\ModelFactory $modelFactory
     * @param \Magento\Eav\Model\Form\Factory $formFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Rma\Model\ItemFactory $itemFactory
     * @param \Magento\Rma\Model\Item\FormFactory $itemFormFactory
     * @param \Magento\Rma\Helper\Data $rmaData
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Framework\App\Response\Http $http
     * @param \Project\Rma\Helper\Pickup $pickupHelper
     * @param \Magento\Sales\Model\Order\AddressFactory $addressFactory
     * @param \Magento\Framework\Serialize\Serializer\Base64Json $base64Json
     * @param StoreManager $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        ModelFactory $modelFactory,
        FormFactory $formFactory,
        Config $eavConfig,
        ItemFactory $itemFactory,
        ItemFormFactory $itemFormFactory,
        Data $rmaData,
        Registry $registry,
        Renderer $addressRenderer,
        Http $http,
        PickupHelper $pickupHelper,
        AddressFactory $addressFactory,
        Base64Json $base64Json,
        StoreManager $storeManager,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $modelFactory,
            $formFactory,
            $eavConfig,
            $itemFactory,
            $itemFormFactory,
            $rmaData,
            $registry,
            $addressRenderer,
            $data
        );
        $this->http = $http;
        $this->pickupHelper = $pickupHelper;
        $this->addressFactory = $addressFactory;
        $this->base64Json = $base64Json;

        $this->_isScopePrivate = true;
        $this->storeManager = $storeManager;
    }

    /**
     * Redirects the customer to search order form
     */
    public function redirect()
    {
        $link = $this->getUrl('sales/guest/form', ['_current' => true, '_use_rewrite' => true]);
        $this->http->setRedirect($link);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        $addressParam = $this->_request->getParam(RmaAddressCreate::ADDRESS_PARAM);

        if ($addressParam) {
            $address = $this->addressFactory->create();
            $address->setData($this->base64Json->unserialize($addressParam));
            return $this->format($address, 'html');
        }

        return $this->format($this->getOrder()->getShippingAddress(), 'html');
    }

    /**
     * @return string
     */
    public function getAddressParamName()
    {
        return RmaAddressCreate::ADDRESS_PARAM;
    }

    /**
     * @return string
     */
    public function getAddressParamValue()
    {
        $addressParam = $this->_request->getParam(RmaAddressCreate::ADDRESS_PARAM);

        if ($addressParam) {
            return $addressParam;
        }

        return '';
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    /**
     * @return string
     */
    public function getRmaAddressCreateUrl()
    {
        $params = ['_secure' => true, 'order_id' => $this->getOrder()->getId()];

        $addressParam = $this->_request->getParam(RmaAddressCreate::ADDRESS_PARAM);

        if ($addressParam) {
            $params[RmaAddressCreate::ADDRESS_PARAM] = $addressParam;
        }

        return $this->getUrl(
            'rma/rmaaddress/create',
            $params
        );
    }

    /**
     * @return array
     */
    public function getPickupDates()
    {
        return $this->pickupHelper->getAllowedDates();
    }

    /**
     * Return if the store allowed customer to edit pickup address
     * Disallow in US and for guests
     * @return bool
     */
    public function isPickupAddressEditable()
    {
        $isUS = \in_array(
            $this->storeManager->getStore()->getCode(),
            [
                UsUpgradeData::STORE_CODE_US_EN,
                UsUpgradeData::STORE_CODE_CA_FR,
                UsUpgradeData::STORE_CODE_CA_EN,
            ]
        );

        return !($isUS || $this->getOrder()->getCustomerIsGuest());
    }
}
