<?php
namespace Project\Rma\Block\RmaAddress;

use Magento\Framework\Serialize\Serializer\Base64Json;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Customer\Block\Widget\Name;
use Magento\Directory\Block\Data as DirectoryBlockData;
use Magento\Directory\Helper\Data as DirectoryHelperData;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollectionFactory;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollectionFactory;
use Magento\Framework\App\Cache\Type\Config as CacheConfig;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\AddressFactory;
use Magento\Store\Model\ScopeInterface;
use Project\Rma\Controller\Rmaaddress\Create as RmaAddressCreate;

/**
 * Class Create
 * @package Project\Rma\Block\RmaAddress
 * @author  Synolia <contact@synolia.com>
 */
class Create extends DirectoryBlockData
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var OrderAddressInterface
     */
    protected $orderAddress;
    /**
     * @var \Magento\Sales\Model\Order\AddressFactory
     */
    protected $addressFactory;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Base64Json
     */
    protected $base64Json;

    /**
     * Create constructor.
     * @param \Magento\Framework\View\Element\Template\Context                 $context
     * @param \Magento\Directory\Helper\Data                                   $directoryHelper
     * @param \Magento\Framework\Json\EncoderInterface                         $jsonEncoder
     * @param \Magento\Framework\App\Cache\Type\Config                         $configCacheType
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory  $regionCollectionFactory
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Framework\Registry                                      $coreRegistry
     * @param \Magento\Sales\Model\Order\AddressFactory                        $addressFactory
     * @param \Magento\Framework\Serialize\Serializer\Base64Json               $base64Json
     * @param array                                                            $data
     */
    public function __construct(
        Context $context,
        DirectoryHelperData $directoryHelper,
        EncoderInterface $jsonEncoder,
        CacheConfig $configCacheType,
        RegionCollectionFactory $regionCollectionFactory,
        CountryCollectionFactory $countryCollectionFactory,
        Registry $coreRegistry,
        AddressFactory $addressFactory,
        Base64Json $base64Json,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $data
        );
        $this->coreRegistry = $coreRegistry;
        $this->addressFactory = $addressFactory;
        $this->base64Json = $base64Json;
    }

    /**
     * Return the Url to go back.
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl(
            'rma/returns/create',
            ['_secure' => true, 'order_id' => $this->getOrder()->getId()]
        );
    }

    /**
     * Return the Url for saving.
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl(
            'rma/rmaaddress/create',
            ['_secure' => true, 'order_id' => $this->getOrder()->getId()]
        );
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderAddressInterface|\Magento\Sales\Model\Order\Address|null
     */
    public function getAddress()
    {
        $addressParam = $this->_request->getParam(RmaAddressCreate::ADDRESS_PARAM);

        if ($addressParam) {
            $address = $this->addressFactory->create();
            $address->setData($this->base64Json->unserialize($addressParam));
            return $address;
        }

        return $this->getOrderAddress();
    }

    /**
     * @return OrderAddressInterface|Order\Address|null
     */
    public function getOrderAddress()
    {
        if (!isset($this->orderAddress)) {
            $this->orderAddress = $this->getOrder()->getShippingAddress();
        }

        return $this->orderAddress;
    }

    /**
     * Generate name block html.
     * @return string
     */
    public function getNameBlockHtml()
    {
        $nameBlock = $this->getLayout()
            ->createBlock(Name::class)
            ->setObject($this->getAddress());

        return $nameBlock->toHtml();
    }

    /**
     * Return the specified numbered street line.
     * @param int $lineNumber
     * @return string
     */
    public function getStreetLine($lineNumber)
    {
        $street = $this->getAddress()->getStreet();
        return isset($street[$lineNumber - 1]) ? $street[$lineNumber - 1] : '';
    }

    /**
     * @return mixed
     */
    public function displayAllRegions()
    {
        return $this->_scopeConfig->getValue(
            'general/region/display_all',
            ScopeInterface::SCOPE_STORE
        );
    }
}
