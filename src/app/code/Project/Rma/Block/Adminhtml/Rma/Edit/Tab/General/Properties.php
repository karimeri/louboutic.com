<?php
namespace Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Registry;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\AbstractGeneral;
use Project\Rma\Model\Config\Source\Category;
use Project\Rma\Model\Config\Source\Priority;
use Project\Rma\Model\Config\Source\StockCode;

/**
 * Class Properties
 * @package Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General
 * @author Synolia <contact@synolia.com>
 */
class Properties extends AbstractGeneral
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Priority
     */
    protected $priority;

    /**
     * @var StockCode
     */
    protected $stockCode;

    /**
     * Properties constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Category $category
     * @param Priority $priority
     * @param StockCode $stockCode
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Category $category,
        Priority $priority,
        StockCode $stockCode,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->category = $category;
        $this->priority = $priority;
        $this->stockCode = $stockCode;
    }

    /**
     * Prepare child blocks
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _prepareLayout()
    {
        // @codingStandardsIgnoreEnd
        $onclick = "submitAndReloadArea($('rma-properties-details-block').parentNode, '" . $this->getSubmitUrl() . "')";
        $button = $this->getLayout()->createBlock(
            Button::class
        )->setData(
            ['label' => __('Save Properties'), 'class' => 'action-save action-secondary', 'onclick' => $onclick]
        );
        $this->setChild('submit_button', $button);

        return parent::_prepareLayout();
    }

    /**
     * Get URL to save properties
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('adminhtml/*/saveProperties', ['id' => $this->getRmaData('entity_id')]);
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->category->toOptionArray();
    }

    /**
     * @return array
     */
    public function getPriorities()
    {
        return $this->priority->toOptionArray();
    }

    /**
     * @return array
     */
    public function getStockCodes()
    {
        return $this->stockCode->toOptionArray();
    }
}
