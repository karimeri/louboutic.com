<?php
namespace Project\Rma\Block\Adminhtml\Rma\Create\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\AbstractGeneral;
use Project\Rma\Model\Rma\Address\Renderer;

/**
 * Class RmaAddress
 * @package Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General
 * @author Synolia <contact@synolia.com>
 */
class RmaAddress extends AbstractGeneral
{
    /**
     * @var \Magento\Sales\Model\Order\Address\Renderer
     */
    protected $addressRenderer;

    /**
     * @var \Magento\Backend\Block\Template\Context
     */
    protected $context;

    /**
     * @var \Magento\Sales\Model\Order\Address
     */
    protected $address;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Sales\Model\Order\AddressFactory $address,
        array $data = []
    ) {
        $this->addressRenderer = $addressRenderer;
        $this->context = $context;
        $this->address = $address->create();
        parent::__construct($context, $registry, $data);
    }

    /**
     * Get order shipping address
     *
     * @return string|null
     */
    public function getOrderShippingAddressText()
    {
        return $this->getOrder()->getShippingAddress();
    }

    /**
     * Get order shipping address
     *
     * @return string|null
     */
    public function getOrderShippingAddress()
    {
        $address = $this->context->getRequest()->getParam('address');
        if(!$address){
            $address = $this->getOrder()->getShippingAddress();
        }else {
            $address = (array)json_decode($address);
            $old_addres = $this->getOrder()->getShippingAddress()->getData();
            $entity_id= $old_addres['entity_id'];
            foreach ($address as $key => $value){
                if(isset($old_addres[$key])) {
                    $old_addres[$key] = $value;
                }
            }
            $old_addres['entity_id'] = $entity_id;
            $model = $this->address;
            $model->setData($address);
            $address = $model;
        }
        if ($address instanceof \Magento\Sales\Model\Order\Address) {
            return $this->addressRenderer->format($address, 'html');
        } else {
            return null;
        }
    }

    public function getShippingAddressJson()
    {
        $address = $this->context->getRequest()->getParam('address');
        if($address) {
            return $address;
        }
        return '';
    }
    /**
     * Get link to edit rma address page
     * @param string $label
     * @return string
     */
    public function getAddressEditLink()
    {
        if ($this->_authorization->isAllowed('Magento_Rma::magento_rma')) {
            if (empty($label)) {
                $label = __('Edit');
            }
            $address = json_encode($this->getOrderShippingAddressText()->getData());
            $url = $this->getUrl('adminhtml/rma/editaddress', ['address' => $address]);
            return '<a href="' . $this->escapeUrl($url) . '">' . $this->escapeHtml($label) . '</a>';
        }
        return '';
    }
}
