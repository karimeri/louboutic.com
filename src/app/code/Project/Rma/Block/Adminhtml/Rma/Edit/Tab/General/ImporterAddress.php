<?php
namespace Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\AbstractGeneral;
use Magento\Store\Model\StoreManagerInterface;
use Project\Rma\Model\Rma\Address\Renderer;
use Project\Rma\Model\ResourceModel\ImporterAddress\Collection;
use Project\Rma\Model\ImporterAddress as ImporterAddressRessource;

/**
 * Class RmaAddress
 * @package Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General
 * @author Synolia <contact@synolia.com>
 */
class ImporterAddress extends AbstractGeneral
{
    /**
     * @var Renderer
     */
    protected $addressRenderer;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Collection
     */
    protected $addressCollection;

    /**
     * RmaAddress constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Renderer $addressRenderer
     * @param StoreManagerInterface $storeManager
     * @param Collection          $addressCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Renderer $addressRenderer,
        StoreManagerInterface $storeManager,
        Collection $addressCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->addressRenderer = $addressRenderer;
        $this->storeManager = $storeManager;
        $this->addressCollection = $addressCollection;

    }

    /**
     * @return null|string
     */
    public function getImporterAddress()
    {
        $addresses = $this->_getImporterAddressData();
        if($addresses && count($addresses)>0) {
            return $addresses['0'][ImporterAddressRessource::ADDRESS_NAME].'<br />'.
                   $addresses['0'][ImporterAddressRessource::ADDRESS_COMPANY].'<br />'.
                   $addresses['0'][ImporterAddressRessource::ADDRESS_STREET].'<br />'.
                   $addresses['0'][ImporterAddressRessource::ADDRESS_CODE_POSTAL].' '.
                   $addresses['0'][ImporterAddressRessource::ADDRESS_CITY]. '<br />'.
                   $addresses['0'][ImporterAddressRessource::ADDRESS_COUNTRY].'<br />'.
                   'VAT: '.$addresses['0'][ImporterAddressRessource::ADDRESS_VAT].'<br />';
        }
        return false ;
    }

    protected function _getImporterAddressData()
    {
        return $this->addressCollection->getData();
    }

    /**
     * @return \Project\Rma\Model\Rma
     */
    public function getRma()
    {
        return $this->_coreRegistry->registry('current_rma');
    }


    /**
     * Get link to edit rma address page
     * @param \Project\Rma\Model\RmaAddress $address
     * @param string $label
     * @return string
     */
    public function getAddressEditLink()
    {
        if ($this->_authorization->isAllowed('Magento_Rma::magento_rma')) {
            if (empty($label)) {
                $label = __('Edit');
            }
            $address_record = $this->_getImporterAddressData();
            $url = $this->getUrl('adminhtml/rma/EditImportaddress', ['rma_id'=>$this->getRma()->getId(),'entity_id' => $address_record[0][ImporterAddressRessource::ENTITY_ID]]);
            return '<a href="' . $this->escapeUrl($url) . '">' . $this->escapeHtml($label) . '</a>';
        }

        return '';
    }

    public function canDisplayProformaInvoiceEditAddressBlock($subject)
    {
        $store = $this->storeManager->getStore($subject->getStoreId());
        return strpos($store->getCode(), 'ch') !== false;
    }
}
