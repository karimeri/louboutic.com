<?php
namespace Project\Rma\Block\Adminhtml\Rma\Create\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\AbstractGeneral;
use Project\Rma\Model\Config\Source\ReturnAddress as ReturnAddressSource;
use Project\Rma\Model\ResourceModel\ReturnAddress\Collection;
use Project\Rma\Model\ReturnAddress\Renderer;
use Project\Rma\Model\ReturnAddressRepository;
use Project\Rma\Model\ReturnAddressFactory;
use Project\Wms\Helper\Config;

/**
 * Class Returnaddress
 * @package Project\Rma\Block\Adminhtml\Rma\Create\Tab\General
 * @author  Synolia <contact@synolia.com>
 */
class Returnaddress extends AbstractGeneral
{
    /**
     * @var \Project\Rma\Model\ReturnAddress\Renderer
     */
    protected $addressRenderer;

    /**
     * @var \Project\Rma\Model\Config\Source\ReturnAddress
     */
    protected $returnAddressSource;

    /**
     * @var \Project\Rma\Model\ResourceModel\ReturnAddress\Collection
     */
    protected $addressCollection;

    /**
     * @var Json
     */
    protected $jsonEncoder;

    /**
     * @var \Project\Wms\Helper\Config
     */
    protected $wmsConfig;

    /**
     * @var \Project\Rma\Model\ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * @var \Project\Rma\Model\ReturnAddressFactory
     */
    protected $returnAddressFactory;

    /**
     * Returnaddress constructor.
     * @param \Magento\Backend\Block\Template\Context                   $context
     * @param \Magento\Framework\Registry                               $registry
     * @param \Project\Rma\Model\ReturnAddress\Renderer                 $addressRenderer
     * @param \Project\Rma\Model\Config\Source\ReturnAddress            $returnAddressSource
     * @param \Project\Rma\Model\ResourceModel\ReturnAddress\Collection $addressCollection
     * @param \Magento\Framework\Serialize\Serializer\Json              $jsonEncoder
     * @param \Project\Wms\Helper\Config                                $wmsConfig
     * @param \Project\Rma\Model\ReturnAddressRepository                $returnAddressRepository
     * @param \Project\Rma\Model\ReturnAddressFactory                   $returnAddressFactory
     * @param array                                                     $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Renderer $addressRenderer,
        ReturnAddressSource $returnAddressSource,
        Collection $addressCollection,
        Json $jsonEncoder,
        Config $wmsConfig,
        ReturnAddressRepository $returnAddressRepository,
        ReturnAddressFactory $returnAddressFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->addressRenderer = $addressRenderer;
        $this->returnAddressSource = $returnAddressSource;
        $this->addressCollection = $addressCollection;
        $this->jsonEncoder = $jsonEncoder;
        $this->wmsConfig = $wmsConfig;
        $this->returnAddressRepository = $returnAddressRepository;
        $this->returnAddressFactory = $returnAddressFactory;
    }

    /**
     * @return null|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReturnAddress()
    {
        /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
        $returnAddress = $this->returnAddressRepository->getByEstablishmentCode(
            $this->wmsConfig->getEstablishmentCode($this->getOrder()->getStore()->getWebsiteId())
        );

        if ($returnAddress instanceof \Project\Rma\Model\ReturnAddress) {
            return $returnAddress;
        }

        return $this->returnAddressFactory->create();
    }

    /**
     * @return null|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReturnAddressHtml()
    {
        /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
        $returnAddress = $this->getReturnAddress();

        if (!empty($returnAddress)) {
            return $this->addressRenderer->format($returnAddress, 'html');
        }

        return null;
    }

    /**
     * @return array
     */
    public function getReturnAddresses()
    {
        return $this->returnAddressSource->toOptionArray();
    }

    /**
     * @return string
     */
    public function getJsonConfig()
    {
        $config = [];

        foreach ($this->addressCollection->load() as $address) {
            $config[$address->getId()] = $this->addressRenderer->format($address, 'html');
        }

        return $this->jsonEncoder->serialize($config);
    }
}
