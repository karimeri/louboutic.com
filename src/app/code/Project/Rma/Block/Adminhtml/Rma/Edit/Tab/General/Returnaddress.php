<?php
namespace Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\Returnaddress as ReturnaddressBase;
use Magento\Rma\Helper\Data;
use Project\Rma\Model\Config\Source\ReturnAddress as ReturnAddressSource;
use Project\Rma\Model\ResourceModel\ReturnAddress\Collection;
use Project\Rma\Model\ReturnAddress\Renderer;

/**
 * Class Returnaddress
 * @package Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General
 * @author  Synolia <contact@synolia.com>
 */
class Returnaddress extends ReturnaddressBase
{
    /**
     * @var Renderer
     */
    protected $addressRenderer;

    /**
     * @var ReturnAddressSource
     */
    protected $returnAddressSource;

    /**
     * @var Collection
     */
    protected $addressCollection;

    /**
     * @var Json
     */
    protected $jsonEncoder;

    /**
     * Returnaddress constructor.
     * @param Context             $context
     * @param Registry            $registry
     * @param Data                $rmaData
     * @param Renderer            $addressRenderer
     * @param ReturnAddressSource $returnAddressSource
     * @param Collection          $addressCollection
     * @param array               $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Data $rmaData,
        Renderer $addressRenderer,
        ReturnAddressSource $returnAddressSource,
        Collection $addressCollection,
        Json $jsonEncoder,
        array $data = []
    ) {
        parent::__construct($context, $registry, $rmaData, $data);
        $this->addressRenderer = $addressRenderer;
        $this->returnAddressSource = $returnAddressSource;
        $this->addressCollection = $addressCollection;
        $this->jsonEncoder = $jsonEncoder;
    }

    /**
     * Prepare child blocks
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable
     */
    protected function _prepareLayout()
    {
        // phpcs:enable
        $onclick = "submitAndReloadArea($('rma-return-address-details-block').parentNode, '".$this->getSubmitUrl()."')";
        $button = $this->getLayout()->createBlock(
            Button::class
        )->setData(
            ['label' => __('Save Address'), 'class' => 'action-save action-secondary', 'onclick' => $onclick]
        );
        $this->setChild('submit_button', $button);

        return parent::_prepareLayout();
    }

    /**
     * Get URL to save return address
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('adminhtml/*/saveReturnAddress', ['id' => $this->getRmaData('entity_id')]);
    }

    /**
     * @return null|string
     */
    public function getReturnAddress()
    {
        try {
            $address = $this->getRma()->getReturnAddress();
        } catch (NoSuchEntityException $exception) {
            $address = null;
        }

        if ($address instanceof \Project\Rma\Model\ReturnAddress) {
            return $this->addressRenderer->format($address, 'html');
        }

        return null;
    }

    /**
     * @return \Project\Rma\Model\Rma
     */
    public function getRma()
    {
        return $this->_coreRegistry->registry('current_rma');
    }

    /**
     * @return array
     */
    public function getReturnAddresses()
    {
        return $this->returnAddressSource->toOptionArray();
    }

    /**
     * @return string
     */
    public function getJsonConfig()
    {
        $config = [];

        foreach ($this->addressCollection->load() as $address) {
            $config[$address->getId()] = $this->addressRenderer->format($address, 'html');
        }

        return $this->jsonEncoder->serialize($config);
    }
}
