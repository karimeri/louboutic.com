<?php
namespace Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Registry;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\AbstractGeneral;
use Project\Rma\Helper\Pickup as PickupHelper;

/**
 * Class Pickup
 * @package Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General
 * @author  Synolia <contact@synolia.com>
 */
class Pickup extends AbstractGeneral
{
    /**
     * @var PickupHelper
     */
    protected $pickupHelper;

    /**
     * Properties constructor.
     * @param Context      $context
     * @param Registry     $registry
     * @param PickupHelper $pickupHelper
     * @param array        $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PickupHelper $pickupHelper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->pickupHelper = $pickupHelper;
    }

    /**
     * Prepare child blocks
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _prepareLayout()
    {
        $onclick = "submitAndReloadArea($('pickup-date-details-block').parentNode, '" . $this->getSubmitUrl() . "')";
        $button = $this->getLayout()->createBlock(
            Button::class
        )->setData(
            ['label' => __('Update pickup date'), 'class' => 'action-save action-secondary', 'onclick' => $onclick]
        );
        $this->setChild('submit_button', $button);

        return parent::_prepareLayout();
    }

    /**
     * Get URL to save properties
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('adminhtml/*/savePickup', ['id' => $this->getRmaData('entity_id')]);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getPickupDates()
    {
        return $this->pickupHelper->getAllowedDates($this->getOrder()->getStoreId());
    }
}
