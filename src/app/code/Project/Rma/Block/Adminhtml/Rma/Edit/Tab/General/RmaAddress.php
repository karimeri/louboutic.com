<?php
namespace Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\General\AbstractGeneral;
use Project\Rma\Model\Rma\Address\Renderer;

/**
 * Class RmaAddress
 * @package Project\Rma\Block\Adminhtml\Rma\Edit\Tab\General
 * @author Synolia <contact@synolia.com>
 */
class RmaAddress extends AbstractGeneral
{
    /**
     * @var Renderer
     */
    protected $addressRenderer;

    /**
     * RmaAddress constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Renderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Renderer $addressRenderer,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->addressRenderer = $addressRenderer;
    }

    /**
     * @return null|string
     */
    public function getRmaAddress()
    {
        $address = $this->getRma()->getRmaAddress();

        if ($address instanceof \Project\Rma\Model\RmaAddress) {
            return $this->addressRenderer->format($address, 'html');
        }

        return null;
    }

    /**
     * @return \Project\Rma\Model\Rma
     */
    public function getRma()
    {
        return $this->_coreRegistry->registry('current_rma');
    }

    /**
     * Get link to edit rma address page
     * @param \Project\Rma\Model\RmaAddress $address
     * @param string $label
     * @return string
     */
    public function getAddressEditLink($address, $label = '')
    {
        if ($this->_authorization->isAllowed('Magento_Rma::magento_rma')) {
            if (empty($label)) {
                $label = __('Edit');
            }

            $url = $this->getUrl('adminhtml/rma/editaddress', ['address_id' => $address->getId()]);
            return '<a href="' . $this->escapeUrl($url) . '">' . $this->escapeHtml($label) . '</a>';
        }

        return '';
    }
}
