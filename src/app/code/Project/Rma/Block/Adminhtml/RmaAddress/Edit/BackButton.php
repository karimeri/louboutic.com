<?php
namespace Project\Rma\Block\Adminhtml\RmaAddress\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class BackButton
 * @package Project\Rma\Block\Adminhtml\RmaAddress\Edit
 * @author Synolia <contact@synolia.com>
 */
class BackButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        /** @var \Project\Rma\Model\RmaAddress $address */
        $address = $this->coreRegistry->registry('rma_address');
        return $this->getUrl('adminhtml/*/edit', ['id' => $address ? $address->getRma()->getId() : null]);
    }
}
