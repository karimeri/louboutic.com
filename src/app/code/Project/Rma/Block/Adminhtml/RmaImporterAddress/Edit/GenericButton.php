<?php
namespace Project\Rma\Block\Adminhtml\RmaImporterAddress\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Project\Rma\Model\RmaAddressRepository;

/**
 * Class GenericButton
 * @package Project\Rma\Block\Adminhtml\RmaAddress\Edit
 * @author Synolia <contact@synolia.com>
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var RmaAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * GenericButton constructor.
     * @param Context $context
     * @param RmaAddressRepository $rmaAddressRepository
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        RmaAddressRepository $rmaAddressRepository,
        Registry $registry
    ) {
        $this->context = $context;
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->coreRegistry = $registry;
    }

    /**
     * @return int|null
     */
    public function getRmaAddressId()
    {
        try {
            return $this->rmaAddressRepository->getById(
                $this->context->getRequest()->getParam('entity_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }

        return null;
    }

    /**
     * Generate url by route and parameters
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
