<?php
namespace Project\Rma\Block\Adminhtml\RmaImporterAddress\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class BackButton
 * @package Project\Rma\Block\Adminhtml\RmaAddress\Edit
 * @author Synolia <contact@synolia.com>
 */
class BackButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        $rma_id = $this->context->getRequest()->getParam('rma_id');
        return $this->getUrl('adminhtml/*/edit', ['id' => $rma_id]);
    }
}
