<?php
namespace Project\Rma\Block\Adminhtml\ReturnAddress\Edit;

use Magento\Backend\Block\Widget\Context;

/**
 * Class GenericButton
 * @package Project\Rma\Block\Adminhtml\ReturnAddress\Edit
 * @author Synolia <contact@synolia.com>
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * GenericButton constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        $this->context = $context;
    }

    /**
     * Generate url by route and parameters
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
