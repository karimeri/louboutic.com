<?php
namespace Project\Rma\Block\Magento\Sales\Adminhtml\Order\Create;

use Magento\Sales\Block\Adminhtml\Order\Create\Form as FormBase;

/**
 * Class Form
 * @package Project\Rma\Block\Magento\Sales\Adminhtml\Order\Create
 * @author  Synolia <contact@synolia.com>
 */
class Form extends FormBase
{
    /**
     * @return string
     */
    public function getSaveUrl()
    {
        $orderId = $this->_request->getParam('order_id');
        $params = [];

        if (!empty($orderId)) {
            $params = [
                'order_id' => $orderId,
            ];
        }

        return $this->getUrl('sales/*/save', $params);
    }
}
