<?php
namespace Project\Rma\Block\Magento\Sales\Adminhtml\Order;

use Magento\Sales\Block\Adminhtml\Order\Create as CreateBase;

/**
 * Class Create
 * @package Project\Rma\Block\Magento\Sales\Adminhtml\Order
 * @author  Synolia <contact@synolia.com>
 */
class Create extends CreateBase
{
    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $customerId = $this->_request->getParam('customer_id');

        if (!empty($customerId)) {
            $this->_sessionQuote->setCustomerId((int) $customerId);
        }

        $storeId = $this->_request->getParam('store_id');

        if (!empty($storeId)) {
            $this->_sessionQuote->setStoreId((int) $storeId);
        }

        parent::_construct();
    }
}
