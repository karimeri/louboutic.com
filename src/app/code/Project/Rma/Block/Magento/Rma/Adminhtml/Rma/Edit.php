<?php
namespace Project\Rma\Block\Magento\Rma\Adminhtml\Rma;

use Magento\Rma\Block\Adminhtml\Rma\Edit as EditBase;

/**
 * Class Edit
 * @package Project\Rma\Block\Magento\Rma\Adminhtml\Rma
 * @author  Synolia <contact@synolia.com>
 */
class Edit extends EditBase
{
    /**
     * @override
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_objectId = 'entity_id';
        $this->_controller = 'adminhtml_rma';
        $this->_blockGroup = 'Magento_Rma';

        \Magento\Backend\Block\Widget\Form\Container::_construct(); // Synolia change

        if (!$this->getRma()) {
            return;
        }
        $statusIsClosed = in_array(
            $this->getRma()->getStatus(),
            [
                \Magento\Rma\Model\Rma\Source\Status::STATE_CLOSED,
                \Magento\Rma\Model\Rma\Source\Status::STATE_PROCESSED_CLOSED,
                // Synolia start change
                \Magento\Rma\Model\Rma\Source\Status::STATE_APPROVED,
                \Magento\Rma\Model\Rma\Source\Status::STATE_REJECTED,
                // Synolia end change
            ]
        );

        if (!$statusIsClosed) {
            $this->buttonList->add(
                'save_and_edit_button',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                100
            );

            $this->buttonList->add(
                'close',
                [
                    'label' => __('Close'),
                    'class' => 'close',
                    'onclick' => 'confirmSetLocation(\'' . __(
                        'Are you sure you want to close this returns request?'
                    ) . '\', \'' . $this->getCloseUrl() . '\')'
                ]
            );
        } else {
            $this->buttonList->remove('save');
            $this->buttonList->remove('reset');
        }

        // Synolia start change
        if ($this->getRma()->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_APPROVED) {
            $urlCreditMemo = $this->getUrl(
                'sales/order_creditmemo/new/',
                [
                    'order_id' => $this->getRma()->getOrderId(),
                    'rma_id' => $this->getRma()->getId(),
                    // phpcs:ignore Ecg.Performance.GetFirstItem
                    'invoice_id' => $this->getRma()->getOrder()->getInvoiceCollection()->getFirstItem()->getId(),
                ]
            );

            $this->buttonList->add(
                'credit_memo_button',
                [
                    'label' => __('Refund / Exchange'),
                    'class' => 'credit_memo',
                    'onclick' => 'setLocation(\'' . $urlCreditMemo . '\')',
                ],
                100
            );
        }
        // Synolia end change

        $order = $this->getOrder();
        $creditmemo = $order->getCreditmemosCollection()->getFirstItem();

        if ($order->getStatus() === \Project\Sales\Model\Magento\Sales\Order::STATE_CREDITMEMO_WAITING_EXCHANGE) {
            $urlOrderCreate = $this->getUrl(
                'sales/order_create/index/',
                [
                    'customer_id' => $order->getCustomerId(),
                    'store_id' => $order->getStoreId(),
                    'order_id' => $order->getId(),
                    'creditmemo_id' => $creditmemo->getId(),
                    'rma_id' => $this->getRma()->getId(),
                ]
            );

            $this->buttonList->add(
                'create_order',
                [
                    'label' => __('Create order exchange'),
                    'onclick' => 'setLocation(\'' . $urlOrderCreate . '\')',
                    'class' => 'create_order',
                ],
                -1
            );
        }

        $this->buttonList->add(
            'print',
            [
                'label' => __('Print'),
                'class' => 'print',
                'onclick' => 'setLocation(\'' . $this->getPrintUrl() . '\')'
            ],
            101
        );

        $this->buttonList->remove('delete');
    }

    /**
     * Declare order instance
     *
     * @return  \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }
}
