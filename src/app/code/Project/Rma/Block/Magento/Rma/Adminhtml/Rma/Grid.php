<?php
namespace Project\Rma\Block\Magento\Rma\Adminhtml\Rma;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\CustomerBalance\Block\Adminhtml\Widget\Grid\Column\Renderer\Currency;
use Magento\Rma\Block\Adminhtml\Rma\Grid as GridBase;
use Magento\Rma\Model\ResourceModel\Rma\Grid\CollectionFactory;
use Magento\Rma\Model\RmaFactory;
use Project\Backend\Block\Adminhtml\Widget\Grid\Column\Renderer\DatetimeWithoutTimeZone;
use Project\Rma\Model\Config\Source\Category;
use Project\Rma\Model\Config\Source\HasImages;
use Project\Rma\Model\Config\Source\Origin;
use Project\Rma\Model\Config\Source\Resolution;
use Project\Rma\Model\Config\Source\Priority;
use Project\Rma\Model\Config\Source\StockCode;
use Project\Rma\Model\Config\Source\ItemCondition;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection as EntityAttributeOptionCollection;

/**
 * Class Grid
 * @package Project\Rma\Block\Magento\Rma\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class Grid extends GridBase
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Priority
     */
    protected $priority;

    /**
     * @var StockCode
     */
    protected $stockCode;

    /**
     * @var ItemCondition
     */
    protected $itemCondition;

    /**
     * @var Origin
     */
    protected $origin;

    /**
     * @var Resolution
     */
    protected $resolution;

    /**
     * @var \Project\Rma\Model\Config\Source\HasImages
     */
    protected $hasImages;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var EntityAttributeOptionCollection
     */
    protected $entityAttributeOptionCollection;

    /**
     * @var string
     */
    protected $shoesDamaged_optionId;

    /**
     * Grid constructor.
     *
     * @param Context $context
     * @param Data $backendHelper
     * @param CollectionFactory $collectionFactory
     * @param RmaFactory $rmaFactory
     * @param Category $category
     * @param Priority $priority
     * @param StockCode $stockCode
     * @param ItemCondition $itemCondition
     * @param \Project\Rma\Model\Config\Source\Origin $origin
     * @param \Project\Rma\Model\Config\Source\Resolution $resolution
     * @param \Project\Rma\Model\Config\Source\HasImages $hasImages
     * @param \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $entityAttributeOptionCollection,
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CollectionFactory $collectionFactory,
        RmaFactory $rmaFactory,
        Category $category,
        Priority $priority,
        StockCode $stockCode,
        ItemCondition $itemCondition,
        Origin $origin,
        Resolution $resolution,
        HasImages $hasImages,
        AttributeRepositoryInterface $attributeRepository,
        EntityAttributeOptionCollection $entityAttributeOptionCollection,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $collectionFactory, $rmaFactory, $data);
        $this->category = $category;
        $this->priority = $priority;
        $this->stockCode = $stockCode;
        $this->itemCondition = $itemCondition;
        $this->origin = $origin;
        $this->resolution = $resolution;
        $this->hasImages = $hasImages;
        $this->attributeRepository = $attributeRepository;
        $this->entityAttributeOptionCollection = $entityAttributeOptionCollection;

    }

    /**
     * @override
     * Prepare grid columns
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _prepareColumns()
    {
        // @codingStandardsIgnoreEnd
        $this->addColumn(
            'increment_id',
            [
                'header' => __('RMA'),
                'index' => 'increment_id',
                'type' => 'text',
                'header_css_class' => 'col-rma-number',
                'column_css_class' => 'col-rma-number'
            ]
        );

        $this->addColumn(
            'date_requested',
            [
                'header' => __('Requested'),
                'index' => 'date_requested',
                'type' => 'datetime',
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'order_increment_id',
            [
                'header' => __('Order'),
                'type' => 'text',
                'index' => 'order_increment_id',
                'header_css_class' => 'col-order-number',
                'column_css_class' => 'col-order-number'
            ]
        );

        $this->addColumn(
            'order_date',
            [
                'header' => __('Ordered'),
                'index' => 'order_date',
                'type' => 'datetime',
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'customer_name',
            [
                'header' => __('Customer'),
                'index' => 'customer_name',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );
        /** @var $rmaModel \Magento\Rma\Model\Rma */
        $rmaModel = $this->_rmaFactory->create();
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => $rmaModel->getAllStatuses(),
                'header_css_class' => 'col-status',
                'column_css_class' => 'col-status'
            ]
        );

        $this->addExtendedColumns();

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('View'),
                        'url' => ['base' => $this->_getControllerUrl('edit')],
                        'field' => 'id',
                    ],
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'header_css_class' => 'col-actions',
                'column_css_class' => 'col-actions'
            ]
        );

        return Extended::_prepareColumns();
    }

    /**
     * Extends of original columns
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function addExtendedColumns()
    {
        $this->addColumn(
            'category',
            [
                'header' => __('Category'),
                'index' => 'category',
                'type' => 'options',
                'options' => $this->category->getAllOptionsForGrid(),
                'header_css_class' => 'col-category',
                'column_css_class' => 'col-category'
            ]
        );

        $this->addColumn(
            'priority',
            [
                'header' => __('Priority'),
                'index' => 'priority',
                'type' => 'options',
                'options' => $this->priority->getAllOptionsForGrid(),
                'header_css_class' => 'col-priority',
                'column_css_class' => 'col-priority'
            ]
        );

        $this->addColumn(
            'stock_code',
            [
                'header' => __('Stock Code'),
                'index' => 'stock_code',
                'type' => 'options',
                'options' => $this->stockCode->getAllOptionsForGrid(),
                'header_css_class' => 'col-stock_code',
                'column_css_class' => 'col-stock_code'
            ]
        );

        $this->addColumn(
            'item_condition',
            [
                'header' => __('Item Condition'),
                'index' => 'item_condition',
                'type' => 'options',
                'options' => $this->itemCondition->getAllOptionsForGrid(),
                'header_css_class' => 'col-item_condition',
                'column_css_class' => 'col-item_condition'
            ]
        );

        $this->addColumn(
            'origin',
            [
                'header' => __('Origin'),
                'index' => 'origin',
                'type' => 'options',
                'options' => $this->origin->getAllOptionsForGrid(),
                'header_css_class' => 'col-origin',
                'column_css_class' => 'col-origin'
            ]
        );

        $this->addColumn(
            'resolution',
            [
                'header' => __('Resolution'),
                'index' => 'resolution',
                'type' => 'options',
                'options' => $this->resolution->getAllOptionsForGrid(),
                'header_css_class' => 'col-resolution',
                'column_css_class' => 'col-resolution'
            ]
        );

        $this->addColumn(
            'pickup_date',
            [
                'header' => __('Pickup Date'),
                'index' => 'pickup_date',
                'type' => 'datetime',
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-pickup_date',
                'column_css_class' => 'col-pickup_date'
            ]
        );

        $this->addColumn(
            'amount',
            [
                'header' => __('Amount'),
                'index' => 'amount',
                'type' => 'price',
                'renderer' => Currency::class,
                'header_css_class' => 'col-amount',
                'column_css_class' => 'col-amount'
            ]
        );

        $this->addColumn(
            'has_images',
            [
                'header' => __('Has images'),
                'index' => 'has_images',
                'type' => 'options',
                'options' => $this->hasImages->getAllOptionsForGrid(),
                'header_css_class' => 'col-category',
                'column_css_class' => 'col-category'
            ]
        );

        $this->addColumn(
            'received_date',
            [
                'header' => __('Received Date'),
                'index' => 'received_date',
                'type' => 'datetime',
                'renderer' => DatetimeWithoutTimeZone::class,
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-pickup_date',
                'column_css_class' => 'col-pickup_date'
            ]
        );
    }


    public function getRowClass($row)
    {
        $items = $row->getItems();
        $shoesDamaged_optionId = $this->getShoesDamagedOptionId();

        $is_shoes_damaged = false;
        foreach ($items as $item) {
            if($item->getCondition() === $shoesDamaged_optionId){
                $is_shoes_damaged = true;
                break;
            }
        }

        if ($row->getCategory() === "After Sales Service"){
            $class = 'yellow';
        } elseif($is_shoes_damaged){
            $class = 'purple';
        } elseif ($row->getStatus() === "Processed and Closed"){
            $class = '';
        } else {
            $class = 'blue';
        }
        return $class;
    }

    public function getShoesDamagedOptionId ()
    {
        if(isset($this->shoesDamaged_optionId) && !is_null($this->shoesDamaged_optionId)){
            return $this->shoesDamaged_optionId;
        }
        $attribute = $this->attributeRepository->get('12', 'condition');
        $options = $this->entityAttributeOptionCollection
            ->setPositionOrder('asc')
            ->setAttributeFilter($attribute->getId())
            ->setStoreFilter()
            ->load();
        foreach ($options as $option) {
            if($option->getDefaultValue() === 'Shoes Damaged'){
                $this->shoesDamaged_optionId = $option->getOptionId();
                break;
            }
        }
        return $this->shoesDamaged_optionId;
    }
}
