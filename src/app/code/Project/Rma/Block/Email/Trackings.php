<?php

namespace Project\Rma\Block\Email;

use Magento\Framework\View\Element\Template;

/**
 * Class Trackings
 * @package Project\Rma\Block\Email
 * @author Synolia <contact@synolia.com>
 */
class Trackings extends Template
{
    /**
     * @var \Magento\Rma\Helper\Data
     */
    private $rmaHelper;

    /**
     * @var \Magento\Rma\Model\RmaRepository
     */
    private $rmaRepository;

    /**
     * Trackings constructor.
     * @param Template\Context $context
     * @param \Magento\Rma\Helper\Data $rmaHelper
     * @param \Magento\Rma\Model\RmaRepository $rmaRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Rma\Helper\Data $rmaHelper,
        \Magento\Rma\Model\RmaRepository $rmaRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->rmaHelper = $rmaHelper;
        $this->rmaRepository = $rmaRepository;
    }

    /**
     * @param \Magento\Rma\Model\Rma|\Magento\Rma\Model\Shipping $rma
     * @return string
     */
    public function getTrackingUrl($rma)
    {
        return $this->rmaHelper->getTrackingPopupUrlBySalesModel($rma);
    }
}
