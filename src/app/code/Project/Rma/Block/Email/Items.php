<?php

namespace Project\Rma\Block\Email;

/**
 * Class Items
 * @package Project\Rma\Block\Email
 * @author Synolia <contact@synolia.com>
 */
class Items extends \Magento\Rma\Block\Email\Items
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $configurableModel;

    /**
     * @var \Project\Core\Helper\Product
     */
    protected $productHelper;

    /**
     * @var \Magento\Sales\Api\OrderItemRepositoryInterface
     */
    protected $orderItemRepository;

    /**
     * Items constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Collection\ModelFactory $modelFactory
     * @param \Magento\Eav\Model\Form\Factory $formFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Rma\Helper\Eav $rmaEav
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableModel
     * @param \Project\Core\Helper\Product $productHelper
     * @param \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Collection\ModelFactory $modelFactory,
        \Magento\Eav\Model\Form\Factory $formFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Rma\Helper\Eav $rmaEav,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableModel,
        \Project\Core\Helper\Product $productHelper,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        array $data = []
    ) {
        parent::__construct($context, $modelFactory, $formFactory, $eavConfig, $rmaEav, $data);

        $this->productRepository = $productRepository;
        $this->configurableModel = $configurableModel;
        $this->productHelper = $productHelper;
        $this->orderItemRepository = $orderItemRepository;
    }

    /**
     * @param $product
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductConfigurableSku($product)
    {
        if ($product->getType() !== $this->configurableModel::TYPE_CODE) {
            $productId = $product->getId();
            $parentId = $this->configurableModel->getParentIdsByChild($productId);
            if (isset($parentId[0])) {
                $parentProduct = $this->productRepository->getById($parentId[0]);
                return $parentProduct->getSku();
            }
        }

        return '';
    }

    /**
     * Retrieve current size of item
     * @return string
     */
    public function getSize($product, $productOptions)
    {
        return $this->productHelper->getProductSize($product, $productOptions);
    }

    /**
     * @param $id
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id){
        return $this->productHelper->getProductById($id);
    }

    /**
     * @param $itemId
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    public function getOrderItem($itemId)
    {
        $orderItem = $this->orderItemRepository->get($itemId);
        return $orderItem;
    }
}
