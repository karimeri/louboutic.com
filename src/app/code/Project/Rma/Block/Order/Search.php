<?php
namespace Project\Rma\Block\Order;

use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Rma\Helper\Data as RmaHelper;
use Magento\Rma\Model\ResourceModel\Item as ItemResource;
use Magento\Rma\Model\ResourceModel\ItemFactory;
use Magento\Sales\Model\Order;
use Project\Rma\Helper\Config as ProjectRmaHelper;
use Project\Rma\Helper\Magento\Sales\Guest as GuestHelper;

/**
 * Class Search
 * @package Project\Rma\Block\Order
 * @author  Synolia <contact@synolia.com>
 */
class Search extends Template
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var RmaHelper
     */
    protected $rmaHelper;

    /**
     * @var ProjectRmaHelper
     */
    protected $projectRmaHelper;

    /**
     * @var ItemResource
     */
    protected $itemResource;

    /**
     * Search constructor.
     * @param Context          $context
     * @param Registry         $coreRegistry
     * @param RmaHelper        $rmaHelper
     * @param ProjectRmaHelper $projectRmaHelper
     * @param ItemFactory      $itemFactory
     * @param array            $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        RmaHelper $rmaHelper,
        ProjectRmaHelper $projectRmaHelper,
        ItemFactory $itemFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry     = $coreRegistry;
        $this->rmaHelper        = $rmaHelper;
        $this->projectRmaHelper = $projectRmaHelper;
        $this->itemResource     = $itemFactory->create();
    }

    /**
     * Return search form action url
     * @return string
     */
    public function getActionUrl()
    {
        return $this->getUrl(GuestHelper::DEFAULT_PATH);
    }

    /**
     * Return order url
     * @return string
     */
    public function getOrderUrl()
    {
        return $this->getUrl('sales/guest/view');
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }

    /**
     * @return Phrase|string
     */
    public function getMessage()
    {
        $availableItems = $this->itemResource->getOrderItemsCollection($this->getOrder()->getId());
        if (!$availableItems->count()) {
            return __('Return not possible: No product available');
        }

        if (!$this->rmaHelper->canCreateRma($this->getOrder())) {
            return __('Return not possible: No eligible product');
        }

        if (!$this->projectRmaHelper->isInDelays($this->getOrder())) {
            return __('Return not possible: Time limit exceeded');
        }

        return '';
    }

    /**
     * @return string
     */
    public function getShipTo()
    {
        return \sprintf(
            '%s %s',
            $this->escapeHtml($this->getOrder()->getShippingAddress()->getFirstname()),
            $this->escapeHtml($this->getOrder()->getShippingAddress()->getLastname())
        );
    }
}
