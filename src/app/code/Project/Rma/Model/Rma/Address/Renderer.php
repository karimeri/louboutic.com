<?php
namespace Project\Rma\Model\Rma\Address;

use Magento\Customer\Model\Address\Config as AddressConfig;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Project\Rma\Model\RmaAddress;

/**
 * Class Renderer
 * @package Project\Rma\Model\Rma\Address
 * @author Synolia <contact@synolia.com>
 */
class Renderer
{
    /**
     * @var AddressConfig
     */
    protected $addressConfig;

    /**
     * @var EventManager
     */
    protected $eventManager;

    /**
     * Constructor
     *
     * @param AddressConfig $addressConfig
     * @param EventManager $eventManager
     */
    public function __construct(
        AddressConfig $addressConfig,
        EventManager $eventManager
    ) {
        $this->addressConfig = $addressConfig;
        $this->eventManager = $eventManager;
    }

    /**
     * Format address in a specific way
     *
     * @param RmaAddress $address
     * @param string $type
     * @return string|null
     */
    public function format(RmaAddress $address, $type)
    {
        $this->addressConfig->setStore($address->getRma()->getOrder()->getStoreId());
        $formatType = $this->addressConfig->getFormatByCode($type);

        if (!$formatType || !$formatType->getRenderer()) {
            return null;
        }

        $this->eventManager->dispatch('customer_rma_address_format', ['type' => $formatType, 'address' => $address]);

        return $formatType->getRenderer()->renderArray($address->getData());
    }
}
