<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Project\Rma\Model\Rma;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Synolia\Logger\Model\LoggerFactory;

/**
 * Class for check can we save or update RMA.
 */
class CanReturn extends \Magento\Rma\Model\Rma\CanReturn
{
    const LOG_DIRECTORY = 'rma';
    const LOG_FILE = 'rma_object_debug.log';

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $logger;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderItemRepositoryInterface $orderItemRepository,
        DirectoryList $directoryList,
        LoggerFactory $loggerFactory
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $logPath = $directoryList->getPath('log') . DIRECTORY_SEPARATOR . $this::LOG_DIRECTORY . DIRECTORY_SEPARATOR;
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath . $this::LOG_FILE]
        );
    }

    /**
     * Check can we create or save RMA.
     *
     * @param mixed $rmaDataObject
     * @return bool
     */
    public function validate($rmaDataObject): bool
    {
        $canReturn = true;

        try {
            $this->orderRepository->get($rmaDataObject->getOrderId());
            foreach ($rmaDataObject->getItems() as $item) {
                $orderItem = $this->orderItemRepository->get($item->getOrderItemId());
                $arrayDataToLog = array(
                    "Order Item OrderId" => $orderItem->getOrderId(),
                    "RMA OrderId" => $rmaDataObject->getOrderId(),
                    "Order Item Qty Ordered" =>$orderItem->getQtyOrdered(),
                    "RMA Item Qty Requested" => $item->getQtyRequested(),
                );
                $this->logger->addDebug("RMA Object Debug",$arrayDataToLog);
                if ($orderItem->getOrderId() != $rmaDataObject->getOrderId()
                    || $orderItem->getQtyOrdered() < $item->getQtyRequested()) {
                    $canReturn = false;
                }
            }
        } catch (NoSuchEntityException $e) {
            $canReturn = false;
        }

        return $canReturn;
    }
}
