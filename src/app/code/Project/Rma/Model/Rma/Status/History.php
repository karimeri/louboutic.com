<?php

namespace Project\Rma\Model\Rma\Status;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Rma\Model\Rma;
use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Rma\Api\RmaAttributesManagementInterface;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;
use Project\Pdf\Model\Magento\Sales\Rma\Pdf\ProformaInvoiceFactory as InvoiceFactory;
use Project\Pdf\Magento\Mail\Template\TransportBuilder as CustomTransportBuilder;

/**
 * Class History
 *
 * @package Project\Rma\Model\Rma\Status
 * @author Synolia <contact@synolia.com>
 */
class History extends \Magento\Rma\Model\Rma\Status\History
{
    /**
     * @var \Project\Pdf\Model\Magento\Sales\Rma\Pdf\ProformaInvoiceFactory
     */
    protected $invoiceFactory;

    /**
     * History constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Rma\Model\RmaFactory $rmaFactory
     * @param \Magento\Rma\Model\Config $rmaConfig
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTimeDateTime
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Rma\Helper\Data $rmaHelper
     * @param TimezoneInterface $localeDate
     * @param RmaRepositoryInterface $rmaRepositoryInterface
     * @param RmaAttributesManagementInterface $metadataService
     * @param AddressRenderer $addressRenderer
     * @param InvoiceFactory $invoiceFactory
     * @param CustomTransportBuilder $customTransportBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $dataq
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Rma\Model\RmaFactory $rmaFactory,
        \Magento\Rma\Model\Config $rmaConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTimeDateTime,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Rma\Helper\Data $rmaHelper,
        TimezoneInterface $localeDate,
        RmaRepositoryInterface $rmaRepositoryInterface,
        RmaAttributesManagementInterface $metadataService,
        AddressRenderer $addressRenderer,
        InvoiceFactory $invoiceFactory,
        CustomTransportBuilder $customTransportBuilder,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $storeManager,
            $rmaFactory,
            $rmaConfig,
            $transportBuilder,
            $dateTimeDateTime,
            $inlineTranslation,
            $rmaHelper,
            $localeDate,
            $rmaRepositoryInterface,
            $metadataService,
            $addressRenderer,
            $resource,
            $resourceCollection,
            $data
        );

        $this->invoiceFactory = $invoiceFactory;
        $this->customTransportBuilder = $customTransportBuilder;
    }

    /**
     * Sending email with RMA data
     *
     * @return $this
     */
    public function sendNewRmaEmail()
    {
        return $this->_sendRmaEmailWithItems($this->getRma(), $this->_rmaConfig->getRootRmaEmail());
    }

    /**
     * Sending authorizing email with RMA data
     * @param Rma $rma
     * @param string $rootConfig
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore
    protected function _sendRmaEmailWithItems(Rma $rma, $rootConfig)
    {
        $storeId = $rma->getStoreId();
        $order = $rma->getOrder();

        $this->_rmaConfig->init($rootConfig, $storeId);
        if (!$this->_rmaConfig->isEnabled()) {
            return $this;
        }

        $this->inlineTranslation->suspend();

        $copyTo = $this->_rmaConfig->getCopyTo();
        $copyMethod = $this->_rmaConfig->getCopyMethod();

        if ($order->getCustomerIsGuest()) {
            $template = $this->_rmaConfig->getGuestTemplate();
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $template = $this->_rmaConfig->getTemplate();
            $customerName = $rma->getCustomerName();
        }

        $sendTo = [['email' => $order->getCustomerEmail(), 'name' => $customerName]];
        if ($rma->getCustomerCustomEmail()) {
            $sendTo[] = ['email' => $rma->getCustomerCustomEmail(), 'name' => $customerName];
        }
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $sendTo[] = ['email' => $email, 'name' => null];
            }
        }

        $returnAddress = $this->rmaHelper->getReturnAddress('html', [], $storeId);

        $bcc = [];
        if ($copyTo && $copyMethod == 'bcc') {
            $bcc = $copyTo;
        }

        foreach ($sendTo as $recipient) {
            $pdf = $this->invoiceFactory->create()->getPdf([$rma]);
            $attachment = $pdf->render();

            $transport = $this->customTransportBuilder->setTemplateIdentifier($template)
                ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                ->setTemplateVars(
                    [
                        'rma' => $rma,
                        'order' => $order,
                        'store' => $this->getStore(),
                        'return_address' => $returnAddress,
                        'item_collection' => $rma->getItemsForDisplay(),
                        'formattedShippingAddress' => $this->addressRenderer->format(
                            $order->getShippingAddress(),
                            'html'
                        ),
                        'formattedBillingAddress' => $this->addressRenderer->format(
                            $order->getBillingAddress(),
                            'html'
                        ),
                    ]
                )
                ->setFrom($this->_rmaConfig->getIdentity())
                ->addTo($recipient['email'], $recipient['name'])
                ->addBcc($bcc)
                ->addAttachment($attachment)
                ->getTransport();

            $transport->sendMessage();
        }

        $this->setEmailSent(true);

        $this->inlineTranslation->resume();

        return $this;
    }
}
