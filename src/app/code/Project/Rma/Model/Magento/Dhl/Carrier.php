<?php
namespace Project\Rma\Model\Magento\Dhl;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Directory\Helper\Data as DirectoryData;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Math\Division;
use Magento\Dhl\Model\Carrier as CarrierBase;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\DateTime as CoreDateTime;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Xml\Security;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory as RateErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory as RateMethodFactory;
use Magento\Sales\Model\Order\Shipment;
use Magento\Dhl\Model\Validator\XmlValidator;
use Magento\Shipping\Helper\Carrier as CarrierHelper;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory as RateResultFactory;
use Magento\Shipping\Model\Simplexml\ElementFactory as XmlElementFactory;
use Magento\Shipping\Model\Tracking\Result\ErrorFactory as TrackingErrorFactory;
use Magento\Shipping\Model\Tracking\Result\StatusFactory as TrackingStatusFactory;
use Magento\Shipping\Model\Tracking\ResultFactory as TrackingResultFactory;
use Magento\Store\Model\Information;
use Magento\Store\Model\StoreManagerInterface;
use Project\Rma\Model\ReturnAddressRepository;
use Project\Wms\Helper\Config as WmsConfig;
use Psr\Log\LoggerInterface;

/**
 * Class Carrier
 * @package Project\Rma\Model\Magento\Dhl
 * @author  Synolia <contact@synolia.com>
 */
class Carrier extends CarrierBase
{
    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * @var WmsConfig
     */
    protected $wmsConfig;

    /**
     * Carrier constructor.
     * @param ScopeConfigInterface    $scopeConfig
     * @param RateErrorFactory        $rateErrorFactory
     * @param LoggerInterface         $logger
     * @param Security                $xmlSecurity
     * @param XmlElementFactory       $xmlElFactory
     * @param RateResultFactory       $rateFactory
     * @param RateMethodFactory       $rateMethodFactory
     * @param TrackingResultFactory   $trackFactory
     * @param TrackingErrorFactory    $trackErrorFactory
     * @param TrackingStatusFactory   $trackStatusFactory
     * @param RegionFactory           $regionFactory
     * @param CountryFactory          $countryFactory
     * @param CurrencyFactory         $currencyFactory
     * @param DirectoryData           $directoryData
     * @param StockRegistryInterface  $stockRegistry
     * @param CarrierHelper           $carrierHelper
     * @param CoreDateTime            $coreDate
     * @param Reader                  $configReader
     * @param StoreManagerInterface   $storeManager
     * @param StringUtils             $string
     * @param Division                $mathDivision
     * @param ReadFactory             $readFactory
     * @param DateTime                $dateTime
     * @param ZendClientFactory       $httpClientFactory
     * @param ReturnAddressRepository $returnAddressRepository
     * @param WmsConfig               $wmsConfig
     * @param array                   $data
     * @param XmlValidator|null       $xmlValidator
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        RateErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        Security $xmlSecurity,
        XmlElementFactory $xmlElFactory,
        RateResultFactory $rateFactory,
        RateMethodFactory $rateMethodFactory,
        TrackingResultFactory $trackFactory,
        TrackingErrorFactory $trackErrorFactory,
        TrackingStatusFactory $trackStatusFactory,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        CurrencyFactory $currencyFactory,
        DirectoryData $directoryData,
        StockRegistryInterface $stockRegistry,
        CarrierHelper $carrierHelper,
        CoreDateTime $coreDate,
        Reader $configReader,
        StoreManagerInterface $storeManager,
        StringUtils $string,
        Division $mathDivision,
        ReadFactory $readFactory,
        DateTime $dateTime,
        ZendClientFactory $httpClientFactory,
        ReturnAddressRepository $returnAddressRepository,
        WmsConfig $wmsConfig,
        array $data = [],
        XmlValidator $xmlValidator = null
    ) {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $carrierHelper,
            $coreDate,
            $configReader,
            $storeManager,
            $string,
            $mathDivision,
            $readFactory,
            $dateTime,
            $httpClientFactory,
            $data,
            $xmlValidator
        );
        $this->returnAddressRepository = $returnAddressRepository;
        $this->wmsConfig = $wmsConfig;
    }

    /**
     * @override
     * @param RateRequest $request
     * @return bool|AbstractModel|Error|Result|null
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->canCollectRates()) {
            return $this->getErrorMessage();
        }

        $requestDhl = clone $request;
        $this->setStore($requestDhl->getStoreId());

        try {
            /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
            $returnAddress = $this->returnAddressRepository->getByEstablishmentCode(
                $this->wmsConfig->getEstablishmentCode($requestDhl->getWebsiteId())
            );

            $origCompanyName = $this->getDefaultAddressValue(
                $requestDhl->getOrigCompanyName(),
                $returnAddress->getCompany()
            );
            $origCountryId = $this->getDefaultAddressValue(
                $requestDhl->getOrigCountryId(),
                $returnAddress->getCountryId()
            );
            $origState = $this->getDefaultAddressValue($requestDhl->getOrigState(), $returnAddress->getRegion());
            $origCity = $this->getDefaultAddressValue($requestDhl->getOrigCity(), $returnAddress->getCity());
            $origPostcode = $this->getDefaultAddressValue(
                $requestDhl->getOrigPostcode(),
                $returnAddress->getPostcode()
            );
        } catch (\Exception $exception) {
            $origCompanyName = $this->_getDefaultValue(
                $requestDhl->getOrigCompanyName(),
                Information::XML_PATH_STORE_INFO_NAME
            );
            $origCountryId = $this->_getDefaultValue(
                $requestDhl->getOrigCountryId(),
                Shipment::XML_PATH_STORE_COUNTRY_ID
            );
            $origState = $this->_getDefaultValue($requestDhl->getOrigState(), Shipment::XML_PATH_STORE_REGION_ID);
            $origCity = $this->_getDefaultValue($requestDhl->getOrigCity(), Shipment::XML_PATH_STORE_CITY);
            $origPostcode = $this->_getDefaultValue($requestDhl->getOrigPostcode(), Shipment::XML_PATH_STORE_ZIP);
        }

        $requestDhl->setOrigCompanyName($origCompanyName)
            ->setCountryId($origCountryId)
            ->setOrigState($origState)
            ->setOrigCity($origCity)
            ->setOrigPostal($origPostcode);
        $this->setRequest($requestDhl);

        $this->_result = $this->_getQuotes();
        $this->_updateFreeMethodQuote($request);

        return $this->_result;
    }

    /**
     * @override
     * @param \SimpleXMLElement $shipmentDetails
     * @return $this|CarrierBase
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _addRate(\SimpleXMLElement $shipmentDetails)
    {
        if (isset($shipmentDetails->ProductShortName)
            && isset($shipmentDetails->GlobalProductCode)
            && array_key_exists((string)$shipmentDetails->GlobalProductCode, $this->getAllowedMethods())
        ) {
            // DHL product code, e.g. '3', 'A', 'Q', etc.
            $dhlProduct = (string)$shipmentDetails->GlobalProductCode;
            $dhlProductDescription = $this->getDhlProductTitle($dhlProduct);

            $totalEstimate = 0;
            $data = [
                'term' => $dhlProductDescription,
                'price_total' => $this->getMethodPrice($totalEstimate, $dhlProduct),
            ];
            if (!empty($this->_rates)) {
                foreach ($this->_rates as $product) {
                    if ($product['data']['term'] == $data['term']
                        && $product['data']['price_total'] == $data['price_total']
                    ) {
                        return $this;
                    }
                }
            }
            $this->_rates[] = ['service' => $dhlProduct, 'data' => $data];
        } else {
            $dhlProductDescription = false;
            if (isset($shipmentDetails->GlobalProductCode)) {
                $dhlProductDescription = $this->getDhlProductTitle((string)$shipmentDetails->GlobalProductCode);
            }
            $dhlProductDescription = $dhlProductDescription ? $dhlProductDescription : __("DHL");
            $this->_errors[] = __("Zero shipping charge for '%1' 2", $dhlProductDescription);
        }

        return $this;
    }

    /**
     * @param $origValue
     * @param $returnAddressValue
     * @return mixed
     */
    protected function getDefaultAddressValue($origValue, $returnAddressValue)
    {
        if (!$origValue) {
            return $returnAddressValue;
        }

        return $origValue;
    }
}
