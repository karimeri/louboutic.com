<?php

namespace Project\Rma\Model\Magento\Rma;

/**
 * Class Config
 * @package Project\Rma\Model\Magento\Rma
 */
class Config extends \Magento\Rma\Model\Config
{
    const XML_PATH_EMAIL_AFTER_SALES_SERVICE_TEMPLATE = '/after_sales_service_template';

    /**
     * @param string $path
     * @param null $store
     * @return mixed
     */
    public function getAfterSalesServiceTemplate($path = '', $store = null)
    {
        return $this->_getConfig($path . self::XML_PATH_EMAIL_AFTER_SALES_SERVICE_TEMPLATE, $store);
    }
}