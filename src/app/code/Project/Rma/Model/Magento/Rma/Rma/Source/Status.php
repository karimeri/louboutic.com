<?php
namespace Project\Rma\Model\Magento\Rma\Rma\Source;

use Magento\Rma\Model\Rma\Source\Status as StatusBase;

/**
 * Class Status
 * @package Project\Rma\Model\Magento\Rma\Rma\Source
 * @author  Synolia <contact@synolia.com>
 */
class Status extends StatusBase
{
    const STATE_ERROR = 'error';

    /**
     * Get state label based on the code
     *
     * @param string $state
     * @return \Magento\Framework\Phrase|string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getItemLabel($state)
    {
        switch ($state) {
            case self::STATE_PENDING:
                return __('Pending');
            case self::STATE_AUTHORIZED:
                return __('Authorized');
            case self::STATE_PARTIAL_AUTHORIZED:
                return __('Partially Authorized');
            case self::STATE_RECEIVED:
                return __('Return Received');
            case self::STATE_RECEIVED_ON_ITEM:
                return __('Return Partially Received');
            case self::STATE_APPROVED:
                return __('Approved');
            case self::STATE_APPROVED_ON_ITEM:
                return __('Partially Approved');
            case self::STATE_REJECTED:
                return __('Rejected');
            case self::STATE_REJECTED_ON_ITEM:
                return __('Partially Rejected');
            case self::STATE_DENIED:
                return __('Denied');
            case self::STATE_CLOSED:
                return __('Closed');
            case self::STATE_PROCESSED_CLOSED:
                return __('Processed and Closed');
            case self::STATE_ERROR:
                return __('Error');
            default:
                return $state;
        }
    }

    /**
     * @override
     * @param array $itemStatusArray Array of RMA items status
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * phpcs:disable Generic.Metrics.CyclomaticComplexity
     */
    public function getStatusByItems($itemStatusArray)
    {
        if (!is_array($itemStatusArray) || empty($itemStatusArray)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('This is the wrong RMA item status.'));
        }

        $itemStatusArray = array_unique($itemStatusArray);

        /** @var $itemStatusModel \Magento\Rma\Model\Item\Attribute\Source\Status */
        $itemStatusModel = $this->_statusFactory->create();

        foreach ($itemStatusArray as $status) {
            if (!$itemStatusModel->checkStatus($status)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('This is the wrong RMA item status.'));
            }
        }

        $itemStatusToBits = [
            \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_PENDING => 1,
            \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_AUTHORIZED => 2,
            \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_DENIED => 4,
            \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_RECEIVED => 8,
            \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_APPROVED => 16,
            \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_REJECTED => 32,
        ];
        $rmaBitMaskStatus = 0;
        foreach ($itemStatusArray as $status) {
            $rmaBitMaskStatus += $itemStatusToBits[$status];
        }

        if ($rmaBitMaskStatus == 1) {
            return self::STATE_PENDING;
        } elseif ($rmaBitMaskStatus == 2) {
            return self::STATE_AUTHORIZED;
        } elseif ($rmaBitMaskStatus == 4) {
            return self::STATE_REJECTED; // Synolia change
        } elseif ($rmaBitMaskStatus == 5) {
            return self::STATE_PENDING;
        } elseif ($rmaBitMaskStatus > 2 && $rmaBitMaskStatus < 8) {
            return self::STATE_PARTIAL_AUTHORIZED;
        } elseif ($rmaBitMaskStatus == 8) {
            return self::STATE_RECEIVED;
        } elseif ($rmaBitMaskStatus >= 9 && $rmaBitMaskStatus <= 15) {
            return self::STATE_RECEIVED_ON_ITEM;
        } elseif ($rmaBitMaskStatus == 16) {
            return self::STATE_APPROVED; // Synolia change
        } elseif ($rmaBitMaskStatus == 20) {
            return self::STATE_APPROVED; // Synolia change
        } elseif ($rmaBitMaskStatus >= 17 && $rmaBitMaskStatus <= 31) {
            return self::STATE_APPROVED_ON_ITEM;
        } elseif ($rmaBitMaskStatus == 32) {
            return self::STATE_REJECTED; // Synolia change
        } elseif ($rmaBitMaskStatus == 36) {
            return self::STATE_REJECTED; // Synolia change
        } elseif ($rmaBitMaskStatus >= 33 && $rmaBitMaskStatus <= 47) {
            return self::STATE_REJECTED_ON_ITEM;
        } elseif ($rmaBitMaskStatus == 48) {
            return self::STATE_APPROVED; // Synolia change
        } elseif ($rmaBitMaskStatus == 52) {
            return self::STATE_APPROVED; // Synolia change
        } elseif ($rmaBitMaskStatus > 48) {
            return self::STATE_APPROVED_ON_ITEM;
        }
    }

    /**
     * @override
     * @return string[]
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _getAvailableValues()
    {
        return [
            self::STATE_PENDING,
            self::STATE_AUTHORIZED,
            self::STATE_PARTIAL_AUTHORIZED,
            self::STATE_RECEIVED,
            self::STATE_RECEIVED_ON_ITEM,
            self::STATE_APPROVED_ON_ITEM,
            self::STATE_REJECTED_ON_ITEM,
            self::STATE_CLOSED,
            // Synolia start change
            self::STATE_REJECTED,
            self::STATE_APPROVED,
            self::STATE_ERROR,
            // Synolia end change
            self::STATE_PROCESSED_CLOSED,
        ];
    }
}
