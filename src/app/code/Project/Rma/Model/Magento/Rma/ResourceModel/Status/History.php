<?php

namespace Project\Rma\Model\Magento\Rma\ResourceModel\Status;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\StoreManager;
use Project\Wms\Model\Api\Connector;
use Synolia\Logger\Model\LoggerFactory;
use Magento\Framework\Filesystem\DirectoryList;

/**
 * Class History
 * @package Project\Rma\Model\Magento\Rma\ResourceModel\Status
 * @author Synolia <contact@synolia.com>
 */
class History extends \Magento\Rma\Model\ResourceModel\Rma\Status\History
{
    const BASE_API_PATH = '/api/magento/product_return_comments';
    const LOG_DIRECTORY = 'rma';
    const LOG_FILE = 'rma_history.log';

    /**
     * @var \Project\Wms\Model\Api\Connector
     */
    protected $wmsConnector;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $storeManager;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * History constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\App\State $state
     * @param null $connectionName
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Context $context,
        Connector $wmsConnector,
        StoreManager $storeManager,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        State $state,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $connectionName
        );

        $this->wmsConnector = $wmsConnector;
        $this->storeManager = $storeManager;
        $this->state = $state;

        $logPath = $directoryList->getPath('log') . DIRECTORY_SEPARATOR . self::LOG_DIRECTORY . DIRECTORY_SEPARATOR;
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath . self::LOG_FILE]
        );
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Rma\Model\ResourceModel\Rma\Status\History
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _afterSave(AbstractModel $object)
    {
        $rma = $object->getRma();
        $rmaWmsId = $rma->getWmsId();

        if (empty($rmaWmsId) || $this->state->getAreaCode() != Area::AREA_ADMINHTML || empty($object->getComment())) {
            return parent::_afterSave($object);
        }

        try {
            $request  = $this->getRequest($object->getComment(), $rma->getWmsId());
            $storeId = $rma->getStoreId();
            $store = $this->storeManager->getStore($storeId);

            $wmsResponse = $this->wmsConnector->request(self::BASE_API_PATH, $store->getWebsiteId(), $request, 'POST');

            if (!$wmsResponse || isset($wmsResponse->error)) {
                $message = $wmsResponse->error->message ?? 'See log for more information';
                throw new LocalizedException(__($message));
            }
        } catch (\Throwable $e) {
            $this->logger->addError('Error when send rma history to wms : '.$e->getMessage());
            throw new LocalizedException(__($e->getMessage()));
        }

        return parent::_afterSave($object);
    }

    /**
     * @param string $comment
     * @param int $wmsId
     * @return array
     */
    protected function getRequest($comment, $wmsId)
    {
        return [
            'comment' => $comment,
            'productReturn' => '/api/product_returns/'.$wmsId,
        ];
    }
}
