<?php

namespace Project\Rma\Model\Magento\Rma\Rma\Status;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Rma\Api\RmaAttributesManagementInterface;
use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Rma\Helper\Data;
use Magento\Rma\Model\Config;
use Magento\Rma\Model\Rma\Status\History as HistoryBase;
use Magento\Rma\Model\RmaFactory;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Project\Rma\Model\ReturnAddress;
use Project\Rma\Model\ReturnAddress\Renderer as ReturnAddressRenderer;
use Project\Rma\Model\RmaAddress;
use Project\Rma\Model\RmaAddress\Renderer as RmaAddressRenderer;
use Project\Rma\Model\Rma;
use Project\Pdf\Magento\Mail\Template\TransportBuilder as CustomTransportBuilder;

/**
 * Class History
 * @package Project\Rma\Model\Magento\Rma\Rma\Status
 * @author  Synolia <contact@synolia.com>
 */
class History extends HistoryBase
{
    /**
     * @var bool
     */
    protected $customerIsGuest;

    /**
     * @var ReturnAddress
     */
    protected $returnAddress;

    /**
     * @var ReturnAddressRenderer
     */
    protected $returnAddressRenderer;

    /**
     * @var RmaAddressRenderer
     */
    protected $rmaAddressRenderer;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    protected $appEmulation;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var CustomTransportBuilder
     */
    protected $customTransportBuilder;

    /**
     * History constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param StoreManagerInterface $storeManager
     * @param RmaFactory $rmaFactory
     * @param Config $rmaConfig
     * @param TransportBuilder $transportBuilder
     * @param DateTime $dateTimeDateTime
     * @param StateInterface $inlineTranslation
     * @param Data $rmaHelper
     * @param TimezoneInterface $localeDate
     * @param RmaRepositoryInterface $rmaRepositoryInterface
     * @param RmaAttributesManagementInterface $metadataService
     * @param AddressRenderer $addressRenderer
     * @param ReturnAddressRenderer $returnAddressRenderer
     * @param RmaAddressRenderer $rmaAddressRenderer
     * @param Emulation $appEmulation
     * @param State $appState
     * @param CustomTransportBuilder $customTransportBuilder
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        StoreManagerInterface $storeManager,
        RmaFactory $rmaFactory,
        Config $rmaConfig,
        TransportBuilder $transportBuilder,
        DateTime $dateTimeDateTime,
        StateInterface $inlineTranslation,
        Data $rmaHelper,
        TimezoneInterface $localeDate,
        RmaRepositoryInterface $rmaRepositoryInterface,
        RmaAttributesManagementInterface $metadataService,
        AddressRenderer $addressRenderer,
        ReturnAddressRenderer $returnAddressRenderer,
        RmaAddressRenderer $rmaAddressRenderer,
        Emulation $appEmulation,
        State $appState,
        CustomTransportBuilder $customTransportBuilder,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $storeManager,
            $rmaFactory,
            $rmaConfig,
            $transportBuilder,
            $dateTimeDateTime,
            $inlineTranslation,
            $rmaHelper,
            $localeDate,
            $rmaRepositoryInterface,
            $metadataService,
            $addressRenderer,
            $resource,
            $resourceCollection,
            $data
        );
        $this->returnAddressRenderer = $returnAddressRenderer;
        $this->rmaAddressRenderer = $rmaAddressRenderer;
        $this->appEmulation = $appEmulation;
        $this->appState = $appState;
        $this->customTransportBuilder = $customTransportBuilder;
    }

    /**
     * @override
     * @return $this
     */
    public function sendNewRmaEmail()
    {
        return $this->_sendRmaEmailWithItems($this->getRma(), $this->_rmaConfig->getRootRmaEmail());
    }

    /**
     * @override
     * @param array $attachments
     * @return $this
     */
    public function sendAuthorizeEmail(array $attachments = [])
    {
        return $this->_sendRmaEmailWithItems($this->getRma(), $this->_rmaConfig->getRootAuthEmail(), $attachments);
    }

    /**
     * @override
     * Sending authorizing email with RMA data
     * @param \Magento\Rma\Model\Rma $rma
     * @param string $rootConfig
     * @param array $attachments
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _sendRmaEmailWithItems(\Magento\Rma\Model\Rma $rma, $rootConfig, array $attachments = [])
    {
        $this->returnAddress = $this->getRma()->getReturnAddress();

        $this->_rmaConfig->init($rootConfig, $this->getRma()->getStoreId());
        if (!$this->_rmaConfig->isEnabled()) {
            return $this;
        }

        $this->inlineTranslation->suspend();

        $this->sendEmail($attachments);
        $this->setEmailSent(true);

        $this->inlineTranslation->resume();

        return $this;
    }

    /**
     * Sends email
     * @param array $attachments
     * @throws \Magento\Framework\Exception\MailException
     */
    private function sendEmail(array $attachments = [])
    {
        $rma = $this->getRma();
        $order = $rma->getOrder();

        $copyTo = $this->_rmaConfig->getCopyTo();
        $copyMethod = $this->_rmaConfig->getCopyMethod();

        $customerName = $this->getCustomerName();

        $sendTo = [['email' => $order->getCustomerEmail(), 'name' => $customerName]];

        if ($rma->getCustomerCustomEmail()) {
            $sendTo[] = ['email' => $rma->getCustomerCustomEmail(), 'name' => $customerName];
        }

        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $sendTo[] = ['email' => $email, 'name' => null];
            }
        }

        $bcc = [];
        if ($copyTo && $copyMethod == 'bcc') {
            $bcc = $copyTo;
        }

        $this->appEmulation->startEnvironmentEmulation($rma->getStoreId());
        $templateId = $this->getRmaEmailTemplate($rma);

        $itemCollection = $this->appState->emulateAreaCode(
            Area::AREA_FRONTEND,
            [$rma, 'getItemsForDisplay']
        );
        $this->appEmulation->stopEnvironmentEmulation();

        foreach ($sendTo as $recipient) {
            $this->customTransportBuilder
                ->setTemplateIdentifier($templateId)
                ->setTemplateOptions([
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->getRma()->getStoreId(),
                ])
                ->setTemplateVars([
                    'rma' => $rma,
                    'order' => $order,
                    'store' => $this->getStore(),
                    'return_address' => $this->getRmaEmailReturnAddress(),
                    'item_collection' => $itemCollection,
                    'formattedShippingAddress' => $this->rmaAddressRenderer->format(
                        $rma->getRmaAddress(),
                        'html'
                    ),
                    'formattedBillingAddress' => $this->addressRenderer->format(
                        $order->getBillingAddress(),
                        'html'
                    ),
                ])
                ->setFrom($this->_rmaConfig->getIdentity())
                ->addTo($recipient['email'], $recipient['name'])
                ->addBcc($bcc);

            $this->setAttachments($attachments);

            $transport = $this->customTransportBuilder->getTransport();
            $transport->sendMessage();
        }
    }

    /**
     * @param Rma $rma
     * @return string
     */
    protected function getRmaEmailTemplate(Rma $rma)
    {
        if ($rma->getData('category') === Rma::CATEGORY_AFTER_SALES_SERVICE) {
            return $this->_rmaConfig->getAfterSalesServiceTemplate();
        }

        if ($this->getCustomerIsGuest()) {
            return $this->_rmaConfig->getGuestTemplate();
        }

        return $this->_rmaConfig->getTemplate();
    }

    /**
     * @return string
     */
    protected function getCustomerName()
    {
        if ($this->getCustomerIsGuest()) {
            return $this->getRma()->getOrder()->getBillingAddress()->getName();
        }

        return $this->getRma()->getCustomerName();
    }

    /**
     * @return bool|int
     */
    protected function getCustomerIsGuest()
    {
        if (!isset($this->customerIsGuest)) {
            $this->customerIsGuest = $this->getRma()->getOrder()->getCustomerIsGuest();
        }

        return $this->customerIsGuest;
    }

    /**
     * @return string
     */
    protected function getRmaEmailReturnAddress()
    {
        return $this->returnAddressRenderer->format($this->returnAddress, 'html');
    }

    /**
     * @param array $attachments
     */
    protected function setAttachments(array $attachments)
    {
        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                $this->customTransportBuilder->addAttachment(
                    $attachment['file_content'],
                    $attachment['filename'],
                    $attachment['mime_type']
                );
            }
        }
    }
}
