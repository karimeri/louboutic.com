<?php
namespace Project\Rma\Model\Magento\Rma;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Rma\Model\Shipping as ShippingBase;
use Magento\Sales\Model\Order;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;
use Project\Rma\Model\RmaAddress;

/**
 * Class Shipping
 * @package Project\Rma\Model\Magento\Rma
 * @author  Synolia <contact@synolia.com>
 */
class Shipping extends ShippingBase
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var AttributeSetHelper
     */
    protected $attributeSetHelper;

    /**
     * Shipping constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Rma\Helper\Data $rmaData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Shipping\Model\Shipment\ReturnShipmentFactory $returnFactory
     * @param \Magento\Shipping\Model\CarrierFactory $carrierFactory
     * @param \Magento\Rma\Model\RmaFactory $rmaFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param ProductRepositoryInterface $productRepository
     * @param AttributeSetHelper $attributeSetHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Rma\Helper\Data $rmaData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Shipping\Model\Shipment\ReturnShipmentFactory $returnFactory,
        \Magento\Shipping\Model\CarrierFactory $carrierFactory,
        \Magento\Rma\Model\RmaFactory $rmaFactory,
        \Magento\Framework\Filesystem $filesystem,
        ProductRepositoryInterface $productRepository,
        AttributeSetHelper $attributeSetHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $rmaData,
            $scopeConfig,
            $orderFactory,
            $storeManager,
            $regionFactory,
            $returnFactory,
            $carrierFactory,
            $rmaFactory,
            $filesystem,
            $resource,
            $resourceCollection,
            $data
        );

        $this->productRepository = $productRepository;
        $this->attributeSetHelper = $attributeSetHelper;
    }

    /**
     * @override
     * @return \Magento\Framework\DataObject
     * @throws LocalizedException
     */
    public function requestToShipment()
    {
        /** @var \Project\Rma\Model\Rma $rma */
        $rma = $this->getRma();

        $shipmentStoreId = $rma->getStoreId();

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->_orderFactory->create()->load($this->getRma()->getOrderId());
        $this->setOrder($order);

        /** @var RmaAddress $shipperAddress */
        $shipperAddress = $rma->getRmaAddress();

        /** @var \Magento\Quote\Model\Quote\Address $recipientAddress */
        $recipientAddress = $this->_rmaData->getReturnAddressModel($this->getRma()->getStoreId());

        list($carrierCode, $shippingMethod) = \explode('_', $this->getCode(), 2);
        $shipmentCarrier = $this->_rmaData->getCarrier($this->getCode(), $shipmentStoreId);
        $baseCurrencyCode = $this->_storeManager->getStore($shipmentStoreId)->getBaseCurrencyCode();

        if (!$shipmentCarrier) {
            throw new LocalizedException(__('Invalid carrier: %1', $carrierCode));
        }

        $shipperRegionCode = $this->_regionFactory->create()->load($shipperAddress->getRegionId())->getCode();
        $recipientRegionCode = $recipientAddress->getRegion();
        $recipientContactName = $this->_rmaData->getReturnContactName($this->getRma()->getStoreId());

        $this->hasEnoughtInformationsFromRecipient($recipientContactName, $recipientAddress);

        $companyName = $this->getCompanyName($shipperAddress);

        /** @var $request \Magento\Shipping\Model\Shipment\ReturnShipment */
        $request = $this->_returnFactory->create();
        $request
            ->setOrderShipment($this)

            ->setShipperContactPersonName($shipperAddress->getFirstname() . ' ' . $shipperAddress->getLastname())
            ->setShipperContactPersonFirstName($shipperAddress->getFirstname())
            ->setShipperContactPersonLastName($shipperAddress->getLastname())
            ->setShipperContactCompanyName($companyName)
            ->setShipperContactPhoneNumber($shipperAddress->getTelephone())
            ->setShipperEmail($shipperAddress->getEmail())
            ->setShipperAddressStreet1($shipperAddress->getStreetLine(1))
            ->setShipperAddressStreet2($shipperAddress->getStreetLine(2))
            ->setShipperAddressCity($shipperAddress->getCity())
            ->setShipperAddressStateOrProvinceCode($shipperRegionCode)
            ->setShipperAddressPostalCode($shipperAddress->getPostcode())
            ->setShipperAddressCountryCode($shipperAddress->getCountryId())

            ->setRecipientContactPersonName($recipientContactName->getName())
            ->setRecipientContactPersonFirstName($recipientContactName->getFirstName())
            ->setRecipientContactPersonLastName($recipientContactName->getLastName())
            ->setRecipientContactCompanyName($recipientAddress->getCompany())
            ->setRecipientContactPhoneNumber($recipientAddress->getTelephone())
            ->setRecipientEmail($recipientAddress->getEmail())
            ->setRecipientAddressStreet($recipientAddress->getStreetFull())
            ->setRecipientAddressStreet1($recipientAddress->getStreetLine(1))
            ->setRecipientAddressStreet2($recipientAddress->getStreetLine(2))
            ->setRecipientAddressCity($recipientAddress->getCity())
            ->setRecipientAddressStateOrProvinceCode($recipientRegionCode)
            ->setRecipientAddressRegionCode($recipientRegionCode)
            ->setRecipientAddressPostalCode($recipientAddress->getPostcode())
            ->setRecipientAddressCountryCode($recipientAddress->getCountryId())

            ->setShippingMethod($shippingMethod)
            ->setPackageWeight($this->getWeight())
            ->setPackages($this->getPackages())
            ->setBaseCurrencyCode($baseCurrencyCode)
            ->setStoreId($shipmentStoreId);

        $referenceData = 'RMA #' . $request->getOrderShipment()->getRma()->getIncrementId() . ' P';
        $request->setReferenceData($referenceData);

        if ($this->hasDangerousGood($order)) {
            $request->setHasDangerousGood(true);
        }

        return $shipmentCarrier->returnOfShipment($request);
    }

    /**
     * @param RmaAddress $shipperAddress
     * @return mixed
     */
    protected function getCompanyName($shipperAddress)
    {
        $companyName = $shipperAddress->getCompany();

        if (empty($companyName)) {
            $companyName = '-';
        }

        return $companyName;
    }

    /**
     * @param $recipientContactName
     * @param $recipientAddress
     * @throws LocalizedException
     */
    protected function hasEnoughtInformationsFromRecipient($recipientContactName, $recipientAddress)
    {
        if (!$recipientContactName->getName() ||
            !$recipientContactName->getLastName() ||
            !$recipientAddress->getCompany() ||
            !$recipientAddress->getTelephone() ||
            !$recipientAddress->getStreetFull() ||
            !$recipientAddress->getCity() ||
            !$recipientAddress->getPostcode() ||
            !$recipientAddress->getCountryId()
        ) {
            throw new LocalizedException(
                __(
                    'We need more information to create your shipping label(s).
                    Please verify your store information and shipping settings.'
                )
            );
        }
    }

    /**
     * @param Order $order
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function hasDangerousGood(Order $order)
    {
        $hasDangerousGood = false;
        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        foreach ($order->getAllVisibleItems() as $item) {
            $product = $this->productRepository->getById($item->getProductId());
            if ($this->attributeSetHelper->isNails($product)) {
                $hasDangerousGood = true;
            }
        }

        return $hasDangerousGood;
    }
}
