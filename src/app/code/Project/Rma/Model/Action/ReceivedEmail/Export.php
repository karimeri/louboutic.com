<?php

namespace Project\Rma\Model\Action\ReceivedEmail;

use Magento\Rma\Model\Rma\Source\Status;
use Magento\Rma\Model\RmaRepository;
use Magento\Store\Model\StoreManager;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Rma\Model\ResourceModel\Rma\CollectionFactory as RmaCollectionFactory;
use Synolia\Sync\Console\ConsoleOutput;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Project\Sales\Model\Magento\Sales\OrderRepository;
use Project\Wms\Model\WmsErrorRepository;
use Synolia\Logger\Model\LoggerFactory;
use Magento\Store\Model\ScopeInterface;
use Project\EmailSent\Helper\Config;
use Magento\Framework\Filesystem\DirectoryList;
use Project\Rma\Model\Rma;

/**
 * Class Export
 * @package Project\Rma\Model\Action\ReceivedEmail
 * @author Synolia <contact@synolia.com>
 */
class Export implements ActionInterface
{
    const LOG_DIRECTORY = 'rma';
    const LOG_FILE = 'send_email.log';

    /**
     * @var RmaCollectionFactory
     */
    protected $rmaCollectionFactory;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var WmsErrorRepository
     */
    protected $wmsErrorRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var DateTimeFactory
     */
    protected $dateFactory;

    /**
     * @var Config
     */
    protected $emailConfigHelper;

    /**
     * @var \Synolia\Logger\Model\Logger
     */
    protected $logger;

    /**
     * @var \Magento\Rma\Model\RmaRepository
     */
    protected $rmaRepository;

    /**
     * Export constructor.
     * @param \Magento\Rma\Model\ResourceModel\Rma\CollectionFactory $rmaCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Project\Wms\Model\WmsErrorRepository $wmsErrorRepository
     * @param \Project\Sales\Model\Magento\Sales\OrderRepository $orderRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
     * @param \Project\EmailSent\Helper\Config $emailConfigHelper
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Rma\Model\RmaRepository $rmaRepository
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        RmaCollectionFactory $rmaCollectionFactory,
        State $state,
        StoreManager $storeManager,
        TransportBuilder $transportBuilder,
        WmsErrorRepository $wmsErrorRepository,
        OrderRepository $orderRepository,
        DateTimeFactory $dateFactory,
        Config $emailConfigHelper,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        RmaRepository $rmaRepository
    ) {
        $this->rmaCollectionFactory = $rmaCollectionFactory;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->wmsErrorRepository = $wmsErrorRepository;
        $this->orderRepository = $orderRepository;
        $this->dateFactory = $dateFactory;
        $this->emailConfigHelper = $emailConfigHelper;
        $this->rmaRepository = $rmaRepository;

        $logPath = $directoryList->getPath('log') . DIRECTORY_SEPARATOR . self::LOG_DIRECTORY . DIRECTORY_SEPARATOR;
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath . self::LOG_FILE]
        );

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return array|void
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $rmaCollection = $this->rmaCollectionFactory->create();
        $rmaCollection->addFieldToFilter('status', Status::STATE_RECEIVED);
        $rmaCollection->addFieldToFilter('received_email_sent', 0);
        $rmaCollection->load();

        foreach ($rmaCollection as $rma) {
            $this->sendEmail($rma);
        }
    }

    /**
     * @param \Project\Rma\Model\Rma $rma
     */
    protected function sendEmail(Rma $rma)
    {
        $storeId = $rma->getOrder()->getStore()->getStoreId();
        $order = $rma->getOrder();

        $sendEmail = $this->emailConfigHelper->getSendEmailRmaReturnReceived(
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        if ($sendEmail && $storeId) {
            $transport = $this->transportBuilder->setTemplateIdentifier(
                $this->emailConfigHelper->getEmailTemplateRmaReturnReceived(
                    ScopeInterface::SCOPE_STORE,
                    $storeId
                )
            )
                ->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => $storeId
                    ]
                )
                ->setTemplateVars([
                    'rma' => $rma,
                    'order' => $order
                ])
                ->setFrom(
                    $this->emailConfigHelper->getEmailIdentityRmaReturnReceived(
                        ScopeInterface::SCOPE_STORE,
                        $storeId
                    )
                );
            $transport->addTo($rma->getOrder()->getCustomerEmail());
            try {
                $transport->getTransport()->sendMessage();
                $rma->setReceivedEmailSent(1);
                $this->rmaRepository->save($rma);
            } catch (\Throwable $e) {
                $this->logger->addError('Error when send rma email : '.$e->getMessage());
            }
        }
    }
}
