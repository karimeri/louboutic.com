<?php

namespace Project\Rma\Model;

use Magento\Rma\Model\Rma\Status\HistoryFactory;
use Magento\Rma\Model\Shipping;
use Magento\Store\Model\StoreManagerInterface;
use Project\Pdf\Model\Magento\Sales\Rma\Pdf\ProformaInvoiceFactory;

/**
 * Class EmailManager
 * @package Project\Rma\Model
 */
class EmailManager
{
    /**
     * @var Shipping
     */
    protected $shippingModel;

    /**
     * @var Shipping\LabelService
     */
    protected $labelService;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProformaInvoiceFactory
     */
    protected $invoiceFactory;

    /**
     * @var HistoryFactory
     */
    protected $rmaHistoryFactory;

    /**
     * EmailManager constructor.
     * @param Shipping $shippingModel
     * @param Shipping\LabelService $labelService
     * @param StoreManagerInterface $storeManager
     * @param ProformaInvoiceFactory $invoiceFactory
     * @param HistoryFactory $rmaHistoryFactory
     */
    public function __construct(
        Shipping $shippingModel,
        Shipping\LabelService $labelService,
        StoreManagerInterface $storeManager,
        ProformaInvoiceFactory $invoiceFactory,
        HistoryFactory $rmaHistoryFactory
    ) {
        $this->shippingModel        = $shippingModel;
        $this->labelService         = $labelService;
        $this->storeManager         = $storeManager;
        $this->invoiceFactory       = $invoiceFactory;
        $this->rmaHistoryFactory    = $rmaHistoryFactory;
    }

    /**
     * @param Rma $rma
     * @param bool $hasAttachments
     * @throws \Zend_Pdf_Exception
     */
    public function sendRmaEmail(Rma $rma, bool $hasAttachments)
    {
        if ($hasAttachments) {
            $labelContent = $this->shippingModel->getShippingLabelByRma($rma)->getShippingLabel();

            if (!$labelContent) {
                return;
            }

            if (\stripos($labelContent, '%PDF-') !== false) {
                $pdfContent = $labelContent;
            } else {
                $pdf = new \Zend_Pdf();
                $page = $this->labelService->createPdfPageFromImageString($labelContent);

                if (!$page) {
                    return;
                }

                $pdf->pages[] = $page;
                $pdfContent = $pdf->render();
            }

            $attachments = [
                [
                    'file_content' => $pdfContent,
                    'mime_type' => 'application/pdf',
                    'disposition' => \Zend_Mime::DISPOSITION_ATTACHMENT,
                    'encoding' => \Zend_Mime::ENCODING_BASE64,
                    'filename' => 'ShippingLabel(' . $rma->getIncrementId() . ').pdf',
                ]
            ];

            $store = $this->storeManager->getStore($rma->getStoreId());
            if (0 === strpos($store->getCode(), 'ch')) {
                $pdf = $this->invoiceFactory->create()->getPdf([$rma]);
                $attachment = $pdf->render();

                $attachments[] = [
                    'file_content' => $attachment,
                    'mime_type' => 'application/pdf',
                    'disposition' => \Zend_Mime::DISPOSITION_ATTACHMENT,
                    'encoding' => \Zend_Mime::ENCODING_BASE64,
                    'filename' => 'proforma-invoice-' . $rma->getIncrementId() . '.pdf',
                ];
            }
        } else {
            $attachments = [];
        }

        /** @var \Project\Rma\Model\Magento\Rma\Rma\Status\History $statusHistory */
        $statusHistory = $this->rmaHistoryFactory->create();
        $statusHistory->setRmaEntityId($rma->getEntityId());
        $statusHistory->sendAuthorizeEmail($attachments);
        $statusHistory->saveSystemComment();
    }
}