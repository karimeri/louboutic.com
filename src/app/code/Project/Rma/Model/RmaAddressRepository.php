<?php
namespace Project\Rma\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Rma\Model\Rma;
use Magento\Sales\Model\Order\Address;
use Project\Rma\Model\RmaAddressFactory;
use Project\Rma\Model\ResourceModel\RmaAddress as RmaAddressResource;

/**
 * Class RmaAddressRepository
 * @package Project\Rma\Model
 * @author Synolia <contact@synolia.com>
 */
class RmaAddressRepository
{
    /**
     * @var RmaAddressResource
     */
    protected $rmaAddressResource;

    /**
     * @var RmaAddress
     */
    protected $rmaAddress;

    /**
     * @var \Project\Rma\Model\RmaAddressFactory
     */
    protected $rmaAddressFactory;

    /**
     * RmaAddressRepository constructor.
     * @param RmaAddressResource $rmaAddressResource
     * @param \Project\Rma\Model\RmaAddressFactory $rmaAddressFactory
     * @param RmaAddress $rmaAddress
     */
    public function __construct(
        RmaAddressResource $rmaAddressResource,
        RmaAddressFactory $rmaAddressFactory,
        RmaAddress $rmaAddress
    ) {
        $this->rmaAddressResource = $rmaAddressResource;
        $this->rmaAddress = $rmaAddress;
        $this->rmaAddressFactory = $rmaAddressFactory;
    }

    /**
     * @param int $id
     * @return RmaAddress
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $rmaAddress = $this->rmaAddressFactory->create();
        $this->rmaAddressResource->load($rmaAddress, $id);

        if (!$rmaAddress->getId()) {
            throw new NoSuchEntityException(__('Requested RMA Address doesn\'t exist'));
        }

        return $rmaAddress;
    }

    /**
     * @param int $parentId
     * @return RmaAddress
     * @throws NoSuchEntityException
     */
    public function getByParentId($parentId)
    {
        $rmaAddress = $this->rmaAddressFactory->create();
        $this->rmaAddressResource->load($rmaAddress, $parentId, 'parent_id');

        if (!$rmaAddress->getId()) {
            throw new NoSuchEntityException(__('Requested RMA Address doesn\'t exist'));
        }

        return $rmaAddress;
    }

    /**
     * @param Address $address
     * @param Rma $rma
     */
    public function createFromShippingAddress(Address $address, Rma $rma)
    {
        $this->rmaAddress
            ->setParentId($rma->getId())
            ->setRegionId($address->getRegionId())
            ->setFax($address->getFax())
            ->setRegion($address->getRegion())
            ->setPostcode($address->getPostcode())
            ->setLastname($address->getLastname())
            ->setStreet($address->getStreet())
            ->setCity($address->getCity())
            ->setEmail($address->getEmail())
            ->setTelephone($address->getTelephone())
            ->setCountryId($address->getCountryId())
            ->setFirstname($address->getFirstname())
            ->setPrefix($address->getPrefix())
            ->setMiddlename($address->getMiddlename())
            ->setSuffix($address->getSuffix())
            ->setCompany($address->getCompany());

        $this->save($this->rmaAddress);
    }

    public function saveAddressData(RmaAddress $rmaAddress, $data)
    {
        $rmaAddress
            ->setData($data);
    }

    /**
     * @param RmaAddress $rmaAddress
     */
    public function save(RmaAddress $rmaAddress)
    {
        $this->rmaAddressResource->save($rmaAddress);
    }
}
