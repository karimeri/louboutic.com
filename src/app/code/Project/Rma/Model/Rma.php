<?php

namespace Project\Rma\Model;

use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Escaper;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Session\Generic;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Quote\Model\Quote\Address\RateFactory;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Quote\Model\Quote\ItemFactory as QuoteItemFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Rma\Api\RmaAttributesManagementInterface;
use Magento\Rma\Helper\Data as RmaHelper;
use Magento\Rma\Model\GridFactory;
use Magento\Rma\Model\Item\Attribute\Source\StatusFactory as ItemAttributeStatusFactory;
use Magento\Rma\Model\ItemFactory as RmaItemFactory;
use Magento\Rma\Model\ResourceModel\Item\CollectionFactory as ItemCollectionFactory;
use Magento\Rma\Model\ResourceModel\ItemFactory;
use Magento\Rma\Model\ResourceModel\Shipping\CollectionFactory as ShippingCollectionFactory;
use Magento\Rma\Model\Rma as MagentoRma;
use Magento\Rma\Api\Data\ItemInterface;
use Magento\Rma\Model\Rma\Source\StatusFactory as RmaStatusFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory as OrderItemCollectionFactory;
use Magento\Shipping\Model\ShippingFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Rma\Model\Item\Attribute\Source\Status;
/**
 * Class Rma
 *
 * @package Project\Rma\Model
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class Rma extends MagentoRma
{
    const CATEGORY_NORMAL = 'Normal';
    const CATEGORY_AFTER_SALES_SERVICE = 'After Sales Service';

    const PRIORITY_NORMAL = 'Normal';
    const PRIORITY_HIGH = 'High';

    const ITEM_CONDITION_NORMAL = 'Normal';
    const ITEM_CONDITION_DAMAGED = 'Damaged';
    const ITEM_CONDITION_SHOES_DAMAGED = 'Shoes Damaged';

    const RETRY_COUNT = 'retry_count';

    // phpcs:disable
    /**
     * @var string
     */
    protected $_eventPrefix = 'rma_enterprise';

    /**
     * @var string
     */
    protected $_eventObject = 'rma';
    // phpcs:enable

    /**
     * @var RmaAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * Rma constructor.
     * @param Context                          $context
     * @param Registry                         $registry
     * @param ExtensionAttributesFactory       $extensionFactory
     * @param AttributeValueFactory            $customAttributeFactory
     * @param RmaHelper                        $rmaData
     * @param Generic                          $session
     * @param StoreManagerInterface            $storeManager
     * @param EavConfig                        $eavConfig
     * @param RmaItemFactory                   $rmaItemFactory
     * @param ItemAttributeStatusFactory       $attrSourceFactory
     * @param GridFactory                      $rmaGridFactory
     * @param RmaStatusFactory                 $statusFactory
     * @param ItemFactory                      $itemFactory
     * @param ItemCollectionFactory            $itemsFactory
     * @param ShippingCollectionFactory        $rmaShippingFactory
     * @param QuoteFactory                     $quoteFactory
     * @param RateFactory                      $quoteRateFactory
     * @param QuoteItemFactory                 $quoteItemFactory
     * @param OrderFactory                     $orderFactory
     * @param OrderItemCollectionFactory       $ordersFactory
     * @param RateRequestFactory               $rateRequestFactory
     * @param ShippingFactory                  $shippingFactory
     * @param Escaper                          $escaper
     * @param TimezoneInterface                $localeDate
     * @param ManagerInterface                 $messageManager
     * @param RmaAttributesManagementInterface $metadataService
     * @param RmaAddressRepository             $rmaAddressRepository
     * @param ReturnAddressRepository          $returnAddressRepository
     * @param OrderRepository                  $orderRepository
     * @param AbstractResource|null            $resource
     * @param AbstractDb|null                  $resourceCollection
     * @param array                            $data
     * @param Json|null                        $serializer
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        RmaHelper $rmaData,
        Generic $session,
        StoreManagerInterface $storeManager,
        EavConfig $eavConfig,
        RmaItemFactory $rmaItemFactory,
        ItemAttributeStatusFactory $attrSourceFactory,
        GridFactory $rmaGridFactory,
        RmaStatusFactory $statusFactory,
        ItemFactory $itemFactory,
        ItemCollectionFactory $itemsFactory,
        ShippingCollectionFactory $rmaShippingFactory,
        QuoteFactory $quoteFactory,
        RateFactory $quoteRateFactory,
        QuoteItemFactory $quoteItemFactory,
        OrderFactory $orderFactory,
        OrderItemCollectionFactory $ordersFactory,
        RateRequestFactory $rateRequestFactory,
        ShippingFactory $shippingFactory,
        Escaper $escaper,
        TimezoneInterface $localeDate,
        ManagerInterface $messageManager,
        RmaAttributesManagementInterface $metadataService,
        RmaAddressRepository $rmaAddressRepository,
        ReturnAddressRepository $returnAddressRepository,
        OrderRepository $orderRepository,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        Json $serializer = null
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $rmaData,
            $session,
            $storeManager,
            $eavConfig,
            $rmaItemFactory,
            $attrSourceFactory,
            $rmaGridFactory,
            $statusFactory,
            $itemFactory,
            $itemsFactory,
            $rmaShippingFactory,
            $quoteFactory,
            $quoteRateFactory,
            $quoteItemFactory,
            $orderFactory,
            $ordersFactory,
            $rateRequestFactory,
            $shippingFactory,
            $escaper,
            $localeDate,
            $messageManager,
            $metadataService,
            $resource,
            $resourceCollection,
            $data,
            $serializer
        );
        $this->returnAddressRepository = $returnAddressRepository;
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param bool $extended
     *
     * @return ItemInterface[]|mixed
     */
    public function getItems($extended = false)
    {
        if ($extended) {
            if (!parent::getItems() && $this->getId()) {
                $itemCollection = $this->_itemsFactory->create();
                $itemCollection->addAttributeToFilter('rma_entity_id', $this->getId());

                return $itemCollection;
            }
        }

        return parent::getItems();
    }

    /**
     * @override
     * @return \Magento\Rma\Model\Rma
     */
    public function close()
    {
        if ($this->canClose()) {
            $this->setStatus(\Magento\Rma\Model\Rma\Source\Status::STATE_CLOSED);
            // Synolia start change
            $order = $this->getOrder();
            $order->setStatus(\Project\Sales\Model\Magento\Sales\Order::STATE_RMA_CLOSED);
            $this->orderRepository->save($order);
            // Synolia end change
        }
        return $this;
    }

    /**
     * @return \Project\Rma\Model\ReturnAddress
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReturnAddress()
    {
        if ($this->getData('returnaddress') === null) {
            /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
            $returnAddress = $this->returnAddressRepository->getById($this->getRmaReturnAddressId());

            $this->setData(
                'returnaddress',
                $returnAddress
            );
        }

        return $this->getData('returnaddress');
    }

    /**
     * @return \Project\Rma\Model\RmaAddress
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRmaAddress()
    {
        if ($this->getData('address') === null) {
            /** @var \Project\Rma\Model\RmaAddress $rmaAddress */
            $rmaAddress = $this->rmaAddressRepository->getByParentId($this->getId());

            $this->setData(
                'address',
                $rmaAddress
            );
        }

        return $this->getData('address');
    }

    /**
     * @override
     * Gets Shipping Methods
     * @param bool $returnItems Flag if needs to return Items
     * @return array|bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getShippingMethods($returnItems = false)
    {
        $found = false;
        $address = false;
        /** @var $itemResource \Magento\Rma\Model\ResourceModel\Item */
        $itemResource = $this->_itemFactory->create();
        $rmaItems = $itemResource->getAuthorizedItems($this->getId());

        if (!empty($rmaItems)) {
            /** @var $quoteItemsCollection \Magento\Sales\Model\ResourceModel\Order\Item\Collection */
            $quoteItemsCollection = $this->_ordersFactory->create();
            $quoteItemsCollection->addFieldToFilter('item_id', ['in' => array_keys($rmaItems)])->getData();

            $quoteItems = [];
            $subtotal = $weight = $qty = $storeId = 0;
            foreach ($quoteItemsCollection as $item) {
                /** @var $itemModel \Magento\Quote\Model\Quote\Item */
                $itemModel = $this->_quoteItemFactory->create();

                $item['qty'] = $rmaItems[$item['item_id']]['qty'];
                $item['name'] = $rmaItems[$item['item_id']]['product_name'];
                $item['row_total'] = $item['price'] * $item['qty'];
                $item['base_row_total'] = $item['base_price'] * $item['qty'];
                $item['row_total_with_discount'] = 0;
                $item['row_weight'] = $item['weight'] * $item['qty'];
                $item['price_incl_tax'] = $item['price'];
                $item['base_price_incl_tax'] = $item['base_price'];
                $item['row_total_incl_tax'] = $item['row_total'];
                $item['base_row_total_incl_tax'] = $item['base_row_total'];

                $quoteItems[] = $itemModel->addData($item->toArray());

                $subtotal += $item['base_row_total'];
                $weight += $item['row_weight'];
                $qty += $item['qty'];

                if (!$storeId) {
                    $storeId = $item['store_id'];
                    // Synolia changes here : get RMA address instead of order address
                    $address = $this->getRmaAddress();
                }
                /** @var $quote \Magento\Quote\Model\Quote */
                $quote = $this->_quoteFactory->create();
                $quote->setStoreId($storeId);
                $itemModel->setQuote($quote);
            }

            if ($returnItems) {
                return $quoteItems;
            }

            $store = $this->_storeManager->getStore($storeId);
            $this->setStore($store);

            $found = $this->_requestShippingRates($quoteItems, $address, $store, $subtotal, $weight, $qty);
        }

        return $found;
    }

    /**
     * @override
     * @param array                              $items
     * @param bool|\Project\Rma\Model\RmaAddress $address
     * @param \Magento\Store\Model\Store         $store
     * @param int                                $subtotal
     * @param int                                $weight
     * @param int                                $qty
     * @return array|bool|false
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _requestShippingRates($items, $address, $store, $subtotal, $weight, $qty)
    {
        /** @var \Magento\Quote\Model\Quote\Address $shippingDestinationInfo */
        $shippingDestinationInfo = $this->_rmaData->getReturnAddressModel($this->getStoreId());

        /** @var $request \Magento\Quote\Model\Quote\Address\RateRequest */
        $request = $this->_rateRequestFactory->create();
        $request->setAllItems($items);
        $request->setDestCountryId($shippingDestinationInfo->getCountryId());
        $request->setDestRegionId($shippingDestinationInfo->getRegionId());
        // Synolia changes here : change getRegionId() to getRegionCode()
        $request->setDestRegionCode($shippingDestinationInfo->getRegionCode());
        $request->setDestStreet($shippingDestinationInfo->getStreetFull());
        $request->setDestCity($shippingDestinationInfo->getCity());
        $request->setDestPostcode($shippingDestinationInfo->getPostcode());
        $request->setDestCompanyName($shippingDestinationInfo->getCompany());

        $request->setPackageValue($subtotal);
        $request->setPackageValueWithDiscount($subtotal);
        $request->setPackageWeight($weight);
        $request->setPackageQty($qty);

        //shop destination address data
        //different carriers use different variables. So we duplicate them
        $request
            ->setOrigCountryId($address->getCountryId())
            ->setOrigCountry($address->getCountryId())
            ->setOrigState($address->getRegionId())
            ->setOrigRegionCode($address->getRegionId())
            ->setOrigCity($address->getCity())
            ->setOrigPostcode($address->getPostcode())
            ->setOrigPostal($address->getPostcode())
            ->setOrigCompanyName($address->getCompany() ? $address->getCompany() : 'NA')
            ->setOrig(true);

        /**
         * Need for shipping methods that use insurance based on price of physical products
         */
        $request->setPackagePhysicalValue($subtotal);

        $request->setFreeMethodWeight(0);

        /**
         * Store and website identifiers need specify from quote
         */
        $request->setStoreId($store->getId());
        $request->setWebsiteId($store->getWebsiteId());
        /**
         * Currencies need to convert in free shipping
         */
        $request->setBaseCurrency($store->getBaseCurrency());
        $request->setPackageCurrency($store->getCurrentCurrency());

        /*
         * For international shipments we must set customs value larger than zero
         * This number is being taken from items' prices
         * But for the case when we try to return bundle items from fixed-price bundle,
         * we have no items' prices. We should add this customs value manually
         */
        if ($request->getOrigCountryId() !== $request->getDestCountryId() && $request->getPackageValue() < 1) {
            $request->setPackageCustomsValue(1);
        }

        $request->setIsReturn(true);

        /** @var $shipping \Magento\Shipping\Model\Shipping */
        $shipping = $this->_shippingFactory->create();
        $result = $shipping->setCarrierAvailabilityConfigField('active_rma')->collectRates($request)->getResult();

        $found = false;
        if ($result) {
            $shippingRates = $result->getAllRates();

            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $shippingRate */
            foreach ($shippingRates as $shippingRate) {
                if (in_array($shippingRate->getCarrier(), array_keys($this->_rmaData->getShippingCarriers()))) {
                    /** @var $addressRate \Magento\Quote\Model\Quote\Address\Rate */
                    $addressRate = $this->_quoteRateFactory->create();
                    $found[] = $addressRate->importShippingRate($shippingRate);
                }
            }
        }

        return $found;
    }

    /**
     * @return string|null
     */
    public function getAirwayBillNumber()
    {
        if (!$this->getData('airway_bill_number')) {
            return $this->getTrackingNumbers()->getLastItem()->getTrackNumber();
        }

        return $this->getData('airway_bill_number');
    }

    /**
     * Send confirmation email only for authorized status
     *
     * By design only \Magento\Rma\Model\Item\Attribute\Source\Status::STATE_AUTHORIZED has such email
     * but other statuses also need it
     *
     * @param string $status
     * @return bool
     */
    private function isStatusNeedsAuthEmail($status): bool
    {
        $statusesNeedsEmail = [
            Status::STATE_AUTHORIZED,
            //Status::STATE_RECEIVED,
            //Status::STATE_APPROVED,
            //Status::STATE_REJECTED,
            //Status::STATE_DENIED
        ];

        return in_array($status, $statusesNeedsEmail);
    }

    /**
     * Overrided to call private isStatusNeedsAuthEmail
     * Creates rma items collection by passed data
     *
     * @param array $data
     * @return Item[]
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _createItemsCollection($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        $order = $this->getOrder();
        $itemModels = [];
        $errors = [];
        $errorKeys = [];

        foreach ($data['items'] as $key => $item) {
            if (isset($item['items'])) {
                $itemModel = $firstModel = false;
                $files = $f = [];
                foreach ($item['items'] as $id => $qty) {
                    if ($itemModel) {
                        $firstModel = $itemModel;
                    }
                    /** @var $itemModel Item */
                    $itemModel = $this->_rmaItemFactory->create();
                    $subItem = $item;
                    unset($subItem['items']);
                    $subItem['order_item_id'] = $id;
                    $subItem['qty_requested'] = $qty;

                    $itemPost = $this->_preparePost($subItem);

                    $f = $itemModel->setData($itemPost)->prepareAttributes($itemPost, $key);

                    /* Copy image(s) to another bundle items */
                    if (!empty($f)) {
                        $files = $f;
                    }
                    if (!empty($files) && $firstModel) {
                        foreach ($files as $code) {
                            $itemModel->setData($code, $firstModel->getData($code));
                        }
                    }
                    // @codingStandardsIgnoreStart
                    $errors = array_merge($itemModel->getErrors(), $errors);
                    // @codingStandardsIgnoreEnd

                    $itemModels[] = $itemModel;
                }
            } else {
                /** @var $itemModel Item */
                $itemModel = $this->_rmaItemFactory->create();
                if (isset($item['entity_id']) && $item['entity_id']) {
                    $itemModel->load($item['entity_id']);
                    if ($itemModel->getEntityId()) {
                        if (empty($item['reason'])) {
                            $item['reason'] = $itemModel->getReason();
                        }

                        if (empty($item['reason_other'])) {
                            $item['reason_other'] =
                                $itemModel->getReasonOther() === null ? '' : $itemModel->getReasonOther();
                        }

                        if (empty($item['condition'])) {
                            $item['condition'] = $itemModel->getCondition();
                        }

                        if (empty($item['qty_requested'])) {
                            $item['qty_requested'] = $itemModel->getQtyRequested();
                        }
                    }
                }

                $itemPost = $this->_preparePost($item);

                $itemModel->setData($itemPost)->prepareAttributes($itemPost, $key);
                // @codingStandardsIgnoreStart
                $errors = array_merge($itemModel->getErrors(), $errors);
                // @codingStandardsIgnoreEnd
                if ($errors) {
                    $errorKeys['tabs'] = 'items_section';
                }

                $itemModels[] = $itemModel;

                if ($this->isStatusNeedsAuthEmail($itemModel->getStatus())
                    && $itemModel->getOrigData(
                        'status'
                    ) !== $itemModel->getStatus()
                ) {
                    $this->setIsSendAuthEmail(1);
                }
            }
        }

        $result = $this->_checkPost($itemModels, $order->getId());

        if ($result !== true) {
            list($result, $errorKey) = $result;
            $errors = array_merge($result, $errors);
            $errorKeys = array_merge($errorKey, $errorKeys);
        }

        $eMessages = $this->messageManager->getMessages()->getErrors();
        if (!empty($errors) || !empty($eMessages)) {
            $this->_session->setRmaFormData($data);
            if (!empty($errorKeys)) {
                $this->_session->setRmaErrorKeys($errorKeys);
            }
            if (!empty($errors)) {
                foreach ($errors as $message) {
                    $this->messageManager->addError($message);
                }
            }
            return false;
        }
        $this->setItems($itemModels);

        return $this->getItems();
    }
}
