<?php
namespace Project\Rma\Model\RmaImporterAddress;

use Magento\Customer\Model\Address\Config as AddressConfig;
use Project\Rma\Model\RmaAddress;

/**
 * Class Renderer
 * @package Project\Rma\Model\RmaAddress
 * @author  Synolia <contact@synolia.com>
 */
class Renderer
{
    /**
     * @var AddressConfig
     */
    protected $addressConfig;

    /**
     * Renderer constructor.
     * @param AddressConfig $addressConfig
     */
    public function __construct(
        AddressConfig $addressConfig
    ) {
        $this->addressConfig = $addressConfig;
    }

    /**
     * Format address in a specific way
     * @param RmaAddress $address
     * @param string $type
     * @return string|null
     */
    public function format(RmaAddress $address, $type)
    {
        $formatType = $this->addressConfig->getFormatByCode($type);

        if (!$formatType || !$formatType->getRenderer()) {
            return null;
        }

        return $formatType->getRenderer()->renderArray($address->getData());
    }
}
