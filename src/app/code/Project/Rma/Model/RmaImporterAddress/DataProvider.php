<?php
namespace Project\Rma\Model\RmaImporterAddress;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Project\Rma\Model\ResourceModel\ImporterAddress\Collection;
use Project\Rma\Model\ResourceModel\ImporterAddress\CollectionFactory;
use Magento\Backend\Block\Widget\Context;
/**
 * Class DataProvider
 * @package Project\Rma\Model\RmaAddress
 * @author Synolia <contact@synolia.com>
 */
class DataProvider extends AbstractDataProvider
{
    const DATAPERSISTOR_NAME = 'project_rma_importer_address';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var Context
     */
    protected $context;
    /**
     * DataProvider constructor.
     * @param Context $context
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        Context $context,
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->dataPersistor = $dataPersistor;
        $this->collection = $collectionFactory->create();
        $this->context = $context;
    }

    /**
     * Get data
     * @return array
     */
    public function getData()
    {

        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        /** @var \Project\Rma\Model\RmaAddress[] $items */
        $items = $this->collection->getItems();
        $rma_id = $this->context->getRequest()->getParam('rma_id');
        foreach ($items as $model) {
            $model->setData('rma_id',$rma_id);
            $this->loadedData[$model->getId()] = $model->getData();
        }

        $data = $this->dataPersistor->get($this::DATAPERSISTOR_NAME);

        if (!empty($data)) {
            /** @var \Project\Rma\Model\RmaAddress $model */
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear($this::DATAPERSISTOR_NAME);
        }

        return $this->loadedData;
    }
}
