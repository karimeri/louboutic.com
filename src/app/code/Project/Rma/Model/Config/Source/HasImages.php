<?php

namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class HasImages
 * @package Project\Rma\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class HasImages implements ArrayInterface
{
    /**
     * @var string[]
     */
    protected $options = [
        0 => 'No',
        1 => 'Yes',
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->options as $value => $label) {
            $options[] = ['value' => $value, 'label' => $label];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $options = [];
        foreach ($this->options as $value => $label) {
            $options[$value] = $label;
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue(string $value)
    {
        return \in_array($value, $this->options);
    }
}
