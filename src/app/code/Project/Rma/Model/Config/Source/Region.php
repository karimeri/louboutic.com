<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;
use Magento\Framework\Registry;

/**
 * Class Region
 * @package Project\Rma\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class Region implements ArrayInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var CollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Region constructor.
     * @param CollectionFactory $regionCollectionFactory
     * @param Registry $coreRegistry
     */
    public function __construct(
        CollectionFactory $regionCollectionFactory,
        Registry $coreRegistry
    ) {
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options) {
            return $this->options;
        }

        /** @var \Project\Rma\Model\RmaAddress $rmaAddress */
        $rmaAddress = $this->coreRegistry->registry('rma_address');

        $this->options = [];
        $regionsCollection = $this->regionCollectionFactory->create()->load();
        foreach ($regionsCollection as $region) {
            if ($region->getCountryId() === $rmaAddress->getCountryId()) {
                $this->options[] = ['label' => $region->getDefaultName(), 'value' => $region->getId()];
            }
        }

        return $this->options;
    }
}
