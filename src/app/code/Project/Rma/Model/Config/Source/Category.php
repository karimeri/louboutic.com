<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Project\Rma\Model\Rma;

/**
 * Class Category
 * @package Project\Rma\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class Category implements ArrayInterface
{
    /**
     * @var string[]
     */
    protected $categories = [
        Rma::CATEGORY_NORMAL,
        Rma::CATEGORY_AFTER_SALES_SERVICE,
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->categories as $label) {
            $options[] = ['value' => $label, 'label' => $label];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $options = [];
        foreach ($this->categories as $label) {
            $options[$label] = $label;
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue(string $value)
    {
        return \in_array($value, $this->categories);
    }
}
