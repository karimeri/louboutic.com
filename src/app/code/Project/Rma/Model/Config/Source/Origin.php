<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Project\Rma\Model\Rma;

/**
 * Class Origin
 * @package Project\Rma\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class Origin implements ArrayInterface
{
    const FO = 'FO';
    const BO = 'BO';

    /**
     * @var string[]
     */
    protected $conditions = [
        self::FO,
        self::BO,
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->conditions as $label) {
            $options[] = ['value' => $label, 'label' => $label];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $options = [];
        foreach ($this->conditions as $label) {
            $options[$label] = $label;
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue($value)
    {
        return \in_array($value, $this->conditions);
    }
}
