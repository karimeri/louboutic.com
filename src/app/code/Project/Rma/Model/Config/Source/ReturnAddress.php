<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Project\Rma\Model\ResourceModel\ReturnAddress\Collection;

/**
 * Class ReturnAddress
 * @package Project\Rma\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class ReturnAddress implements ArrayInterface
{
    /**
     * Options array
     * @var array
     */
    protected $options;

    /**
     * @var Collection
     */
    protected $addressCollection;

    /**
     * ReturnAddress constructor.
     * @param Collection $addressCollection
     */
    public function __construct(
        Collection $addressCollection
    ) {
        $this->addressCollection = $addressCollection;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = $this->addressCollection->load()->toOptionArray();
        }

        return $this->options;
    }
}
