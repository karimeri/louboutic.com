<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Email\Model\ResourceModel\Template\Collection;
use Magento\Email\Model\ResourceModel\Template\CollectionFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Option\ArrayInterface;

/**
 * Class EmailTemplate
 * @package Project\Rma\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class EmailTemplate extends DataObject implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $templatesFactory;

    /**
     * EmailTemplate constructor.
     * @param CollectionFactory $templatesFactory
     * @param array             $data
     */
    public function __construct(
        CollectionFactory $templatesFactory,
        array $data = []
    ) {
        parent::__construct($data);
        $this->templatesFactory = $templatesFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        /** @var $collection Collection */
        $collection = $this->templatesFactory->create();
        $collection->load();
        $options = $collection->toOptionArray();
        \array_unshift(
            $options,
            ['value' => '', 'label' => __('-- Please Select --')]
        );

        return $options;
    }
}
