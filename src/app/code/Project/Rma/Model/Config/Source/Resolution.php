<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Rma\Helper\Eav as RmaEav;

/**
 * Class Resolution
 * @package Project\Rma\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class Resolution implements ArrayInterface
{
    const MIX = 'Mix';

    /**
     * @var \Magento\Rma\Helper\Eav
     */
    protected $rmaEav;

    /**
     * Resolution constructor.
     *
     * @param \Magento\Rma\Helper\Eav $rmaEav
     */
    public function __construct(
        RmaEav $rmaEav
    ) {
        $this->rmaEav = $rmaEav;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['value' => self::MIX, 'label' => self::MIX];

        foreach ($this->rmaEav->getAttributeOptionValues('resolution') as $resolution) {
            $options[] = ['value' => $resolution, 'label' => $resolution];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $options = [
            self::MIX => self::MIX
        ];

        foreach ($this->rmaEav->getAttributeOptionValues('resolution') as $resolution) {
            $options[$resolution] = $resolution;
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue($value)
    {
        return \in_array($value, $this->rmaEav->getAttributeOptionValues('resolution'));
    }
}
