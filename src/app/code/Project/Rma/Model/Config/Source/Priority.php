<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Project\Rma\Model\Rma;

/**
 * Class Priority
 * @package Project\Rma\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class Priority implements ArrayInterface
{
    /**
     * @var string[]
     */
    protected $priorities = [
        Rma::PRIORITY_NORMAL,
        Rma::PRIORITY_HIGH,
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->priorities as $label) {
            $options[] = ['value' => $label, 'label' => $label];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $options = [];
        foreach ($this->priorities as $label) {
            $options[$label] = $label;
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue(string $value)
    {
        return \in_array($value, $this->priorities);
    }
}
