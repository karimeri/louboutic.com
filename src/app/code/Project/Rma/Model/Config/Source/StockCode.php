<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Option\ArrayInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\Wms\Helper\Config as WmsConfig;

/**
 * Class StockCode
 * @package Project\Rma\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class StockCode implements ArrayInterface
{
    const UNDEFINED_OPTION_LABEL = 'None';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * StockCode constructor.
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $stores = $this->storeManager->getStores();
        $labels = [];

        $options = [];
        foreach ($stores as $store) {
            $label = $this->scopeConfig->getValue(
                WmsConfig::XML_PATH_STOCK_CODE,
                ScopeInterface::SCOPE_STORE,
                $store->getStoreId()
            );

            $labelDefective = $this->scopeConfig->getValue(
                WmsConfig::XML_PATH_STOCK_CODE_DEFECTIVE,
                ScopeInterface::SCOPE_STORE,
                $store->getStoreId()
            );

            if (!isset($labels[$label])) {
                $options[] = ['value' => $label, 'label' => $label];
                $labels[$label] = true;
            }

            if (!isset($labels[$labelDefective])) {
                $options[] = ['value' => $labelDefective, 'label' => $labelDefective];
                $labels[$labelDefective] = true;
            }
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $stores = $this->storeManager->getStores();

        $options = ['' => __($this::UNDEFINED_OPTION_LABEL)];
        foreach ($stores as $store) {
            $label = $this->scopeConfig->getValue(
                WmsConfig::XML_PATH_STOCK_CODE,
                ScopeInterface::SCOPE_STORE,
                $store->getStoreId()
            );

            $labelDefective = $this->scopeConfig->getValue(
                WmsConfig::XML_PATH_STOCK_CODE_DEFECTIVE,
                ScopeInterface::SCOPE_STORE,
                $store->getStoreId()
            );

            if (!isset($options[$label])) {
                $options[$label] = $label;
            }

            if (!isset($options[$labelDefective])) {
                $options[$labelDefective] = $labelDefective;
            }
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue(string $value)
    {
        $stores = $this->storeManager->getStores();
        $labels = ['' => true];

        foreach ($stores as $store) {
            $label = $this->scopeConfig->getValue(
                WmsConfig::XML_PATH_STOCK_CODE,
                ScopeInterface::SCOPE_STORE,
                $store->getStoreId()
            );

            $labelDefective = $this->scopeConfig->getValue(
                WmsConfig::XML_PATH_STOCK_CODE_DEFECTIVE,
                ScopeInterface::SCOPE_STORE,
                $store->getStoreId()
            );

            if (!isset($labels[$labelDefective])) {
                $labels[$labelDefective] = true;
            }

            if (!isset($labels[$label])) {
                $labels[$label] = true;
            }
        }

        return \array_key_exists($value, $labels);
    }
}
