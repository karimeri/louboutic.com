<?php
namespace Project\Rma\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Project\Rma\Model\Rma;

/**
 * Class ItemCondition
 * @package Project\Rma\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class ItemCondition implements ArrayInterface
{
    /**
     * @var string[]
     */
    protected $conditions = [
        Rma::ITEM_CONDITION_NORMAL,
        Rma::ITEM_CONDITION_DAMAGED,
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->conditions as $label) {
            $options[] = ['value' => $label, 'label' => $label];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptionsForGrid()
    {
        $options = [];
        foreach ($this->conditions as $label) {
            $options[$label] = $label;
        }

        return $options;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function checkValue(string $value)
    {
        return \in_array($value, $this->conditions);
    }
}
