<?php
namespace Project\Rma\Model\ReturnAddress;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Project\Rma\Model\ResourceModel\ReturnAddress\Collection;
use Project\Rma\Model\ResourceModel\ReturnAddress\CollectionFactory;

/**
 * Class DataProvider
 * @package Project\Rma\Model\ReturnAddress
 * @author Synolia <contact@synolia.com>
 */
class DataProvider extends AbstractDataProvider
{
    const DATAPERSISTOR_NAME = 'project_rma_return_address';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->dataPersistor = $dataPersistor;
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        /** @var \Project\Rma\Model\ReturnAddress[] $items */
        $items = $this->collection->getItems();

        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
        }

        $data = $this->dataPersistor->get($this::DATAPERSISTOR_NAME);

        if (!empty($data)) {
            /** @var \Project\Rma\Model\ReturnAddress $model */
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear($this::DATAPERSISTOR_NAME);
        }

        return $this->loadedData;
    }
}
