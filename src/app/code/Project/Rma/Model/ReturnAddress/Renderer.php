<?php
namespace Project\Rma\Model\ReturnAddress;

use Magento\Customer\Model\Address\Config as AddressConfig;
use Project\Rma\Model\ReturnAddress;

/**
 * Class Renderer
 * @package Project\Rma\Model\ReturnAddress
 * @author  Synolia <contact@synolia.com>
 */
class Renderer
{
    /**
     * @var AddressConfig
     */
    protected $addressConfig;

    /**
     * Renderer constructor.
     * @param AddressConfig $addressConfig
     */
    public function __construct(
        AddressConfig $addressConfig
    ) {
        $this->addressConfig = $addressConfig;
    }

    /**
     * Format address in a specific way
     * @param ReturnAddress $address
     * @param string $type
     * @return string|null
     */
    public function format(ReturnAddress $address, $type)
    {
        $address->setStreet();

        $formatType = $this->addressConfig->getFormatByCode($type);

        if (!$formatType || !$formatType->getRenderer()) {
            return null;
        }

        return $formatType->getRenderer()->renderArray($address->getData());
    }
}
