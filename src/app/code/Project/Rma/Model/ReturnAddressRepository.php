<?php
namespace Project\Rma\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Project\Rma\Api\Data\ReturnAddressInterface;
use Project\Rma\Model\ReturnAddressFactory;
use Project\Rma\Model\ResourceModel\ReturnAddress as ReturnAddressResource;

/**
 * Class ReturnAddressRepository
 * @package Project\Rma\Model
 * @author Synolia <contact@synolia.com>
 */
class ReturnAddressRepository
{
    /**
     * @var ReturnAddressResource
     */
    protected $returnAddressResource;

    /**
     * @var ReturnAddress
     */
    protected $returnAddress;

    /**
     * @var \Project\Rma\Model\ReturnAddressFactory
     */
    protected $returnAddressFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var SearchResultsInterface
     */
    protected $searchResultsFactory;

    /**
     * ReturnAddressRepository constructor.
     * @param ReturnAddressResource $returnAddressResource
     * @param \Project\Rma\Model\ReturnAddressFactory $returnAddressFactory
     * @param ReturnAddress $returnAddress
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SearchResultsInterfaceFactory $searchResultsInterfaceFactory
     */
    public function __construct(
        ReturnAddressResource $returnAddressResource,
        ReturnAddressFactory $returnAddressFactory,
        ReturnAddress $returnAddress,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsInterfaceFactory
    ) {
        $this->returnAddressResource = $returnAddressResource;
        $this->returnAddress = $returnAddress;
        $this->returnAddressFactory = $returnAddressFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsInterfaceFactory;
    }

    /**
     * @param int $id
     * @return ReturnAddress
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $returnAddress = $this->returnAddressFactory->create();
        $this->returnAddressResource->load($returnAddress, $id);
        if (!$returnAddress->getId()) {
            throw new NoSuchEntityException(__('Return Address with id %1 does not exist.', $id));
        }

        return $returnAddress;
    }

    /**
     * @param string $establishmentCode
     * @return ReturnAddress
     * @throws NoSuchEntityException
     */
    public function getByEstablishmentCode($establishmentCode)
    {
        $returnAddress = $this->returnAddressFactory->create();
        $this->returnAddressResource->load(
            $returnAddress,
            $establishmentCode,
            ReturnAddressInterface::ESTABLISHMENT_CODE
        );

        if (!$returnAddress->getId()) {
            throw new NoSuchEntityException(
                __('Return Address with establishment_code %1 does not exist.', $establishmentCode)
            );
        }

        return $returnAddress;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultsFactory->create();
        $this->collectionProcessor->process($searchCriteria, $searchResult);
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }

    /**
     * @param ReturnAddress $returnAddress
     */
    public function save(ReturnAddress $returnAddress)
    {
        $this->returnAddressResource->save($returnAddress);
    }

    /**
     * @param ReturnAddress $returnAddress
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(ReturnAddress $returnAddress)
    {
        try {
            $this->returnAddressResource->delete($returnAddress);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the return address: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * @param $returnAddressId
     * @return bool
     */
    public function deleteById($returnAddressId)
    {
        return $this->delete($this->getById($returnAddressId));
    }
}
