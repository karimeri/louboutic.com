<?php
namespace Project\Rma\Model;

use Magento\Framework\Model\AbstractModel;
use Project\Rma\Api\Data\ReturnAddressInterface;
use Project\Rma\Model\ResourceModel\ReturnAddress as ReturnAddressResource;

/**
 * Class ReturnAddress
 * @package Project\Rma\Model
 * @author Synolia <contact@synolia.com>
 */
class ReturnAddress extends AbstractModel implements ReturnAddressInterface
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable
     */
    public function _construct()
    {
        // phpcs:enable
        $this->_init(ReturnAddressResource::class);
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return (int) $this->getData(ReturnAddressInterface::ENTITY_ID);
    }

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->setData(ReturnAddressInterface::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getData(ReturnAddressInterface::NAME);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->setData(ReturnAddressInterface::NAME, $name);
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->getData(ReturnAddressInterface::REGION);
    }

    /**
     * @param string $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->setData(ReturnAddressInterface::REGION, $region);
        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->getData(ReturnAddressInterface::POSTCODE);
    }

    /**
     * @param string $postcode
     * @return $this
     */
    public function setPostcode($postcode)
    {
        $this->setData(ReturnAddressInterface::POSTCODE, $postcode);
        return $this;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->getData(ReturnAddressInterface::CONTACT_NAME);
    }

    /**
     * @param string $contactName
     * @return $this
     */
    public function setContactName($contactName)
    {
        $this->setData(ReturnAddressInterface::CONTACT_NAME, $contactName);
        return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->getData(ReturnAddressInterface::CONTACT_PHONE);
    }

    /**
     * @param string $contactPhone
     * @return $this
     */
    public function setContactPhone($contactPhone)
    {
        $this->setData(ReturnAddressInterface::CONTACT_PHONE, $contactPhone);
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet1()
    {
        return $this->getData(ReturnAddressInterface::STREET1);
    }

    /**
     * @param string $street1
     * @return $this
     */
    public function setStreet1($street1)
    {
        $this->setData(ReturnAddressInterface::STREET1, $street1);
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet2()
    {
        return $this->getData(ReturnAddressInterface::STREET2);
    }

    /**
     * @param string $street2
     * @return $this
     */
    public function setStreet2($street2)
    {
        $this->setData(ReturnAddressInterface::STREET2, $street2);
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet3()
    {
        return $this->getData(ReturnAddressInterface::STREET3);
    }

    /**
     * @param string $street3
     * @return $this
     */
    public function setStreet3($street3)
    {
        $this->setData(ReturnAddressInterface::STREET3, $street3);
        return $this;
    }

    /**
     * @return $this
     */
    public function setStreet()
    {
        if ($this->getData('street') === null) {
            $this->setData(
                'street',
                $this->getData(ReturnAddressInterface::STREET1) . ' ' .
                $this->getData(ReturnAddressInterface::STREET2) . ' ' .
                $this->getData(ReturnAddressInterface::STREET3)
            );
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getData(ReturnAddressInterface::CITY);
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->setData(ReturnAddressInterface::CITY, $city);
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailTemplate()
    {
        return $this->getData(ReturnAddressInterface::EMAIL_TEMPLATE);
    }

    /**
     * @param string $emailTemplate
     * @return $this
     */
    public function setEmail($emailTemplate)
    {
        $this->setData(ReturnAddressInterface::EMAIL_TEMPLATE, $emailTemplate);
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryId()
    {
        return $this->getData(ReturnAddressInterface::COUNTRY_ID);
    }

    /**
     * @param string $countryId
     * @return $this
     */
    public function setCountryId($countryId)
    {
        $this->setData(ReturnAddressInterface::COUNTRY_ID, $countryId);
        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->getData(ReturnAddressInterface::COMPANY);
    }

    /**
     * @param string $company
     * @return $this
     */
    public function setCompany($company)
    {
        $this->setData(ReturnAddressInterface::COMPANY, $company);
        return $this;
    }

    /**
     * @return string
     */
    public function getEstablishmentCode()
    {
        return $this->getData(ReturnAddressInterface::ESTABLISHMENT_CODE);
    }

    /**
     * @param string $establishmentCode
     * @return $this
     */
    public function setEstablishmentCode($establishmentCode)
    {
        $this->setData(ReturnAddressInterface::ESTABLISHMENT_CODE, $establishmentCode);
        return $this;
    }
}
