<?php
namespace Project\Rma\Model\ResourceModel\RmaAddress;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Rma\Model\RmaAddress;
use Project\Rma\Model\ResourceModel\RmaAddress as RmaAddressResource;

/**
 * Class Collection
 * @package Project\Rma\Model\ResourceModel\RmaAddress
 * @author Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(RmaAddress::class, RmaAddressResource::class);
    }
}
