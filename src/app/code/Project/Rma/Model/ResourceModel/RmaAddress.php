<?php
namespace Project\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Rma\Model\RmaAddress as RmaModel;

/**
 * Class RmaAddress
 * @package Project\Rma\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class RmaAddress extends AbstractDb
{
    const TABLE_NAME = 'rma_address';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init($this::TABLE_NAME, RmaModel::ENTITY_ID);
    }
}
