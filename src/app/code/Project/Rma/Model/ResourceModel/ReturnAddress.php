<?php
namespace Project\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Rma\Model\ReturnAddress as ReturnAddressModel;

/**
 * Class ReturnAddress
 * @package Project\Rma\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class ReturnAddress extends AbstractDb
{
    const TABLE_NAME = 'rma_return_address';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init($this::TABLE_NAME, ReturnAddressModel::ENTITY_ID);
    }
}
