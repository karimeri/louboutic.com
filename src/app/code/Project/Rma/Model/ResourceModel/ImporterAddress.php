<?php
namespace Project\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Rma\Model\ImporterAddress as ImporterAddressModel;

/**
 * Class ReturnAddress
 * @package Project\Rma\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class ImporterAddress extends AbstractDb
{
    const TABLE_NAME = 'rma_importer_address';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init($this::TABLE_NAME, ImporterAddressModel::ENTITY_ID);
    }
}
