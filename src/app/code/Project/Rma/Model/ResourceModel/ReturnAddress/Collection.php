<?php
namespace Project\Rma\Model\ResourceModel\ReturnAddress;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Rma\Model\ResourceModel\ReturnAddress as ReturnAddressResource;
use Project\Rma\Model\ReturnAddress;

/**
 * Class Collection
 * @package Project\Rma\Model\ResourceModel\ReturnAddress
 * @author Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(ReturnAddress::class, ReturnAddressResource::class);
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('entity_id');
    }
}
