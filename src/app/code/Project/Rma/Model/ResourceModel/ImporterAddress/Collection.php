<?php
namespace Project\Rma\Model\ResourceModel\ImporterAddress;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Rma\Model\ResourceModel\ImporterAddress as ImporterAddressResource;
use Project\Rma\Model\ImporterAddress;

/**
 * Class Collection
 * @package Project\Rma\Model\ResourceModel\ReturnAddress
 * @author Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(ImporterAddress::class, ImporterAddressResource::class);
    }

}
