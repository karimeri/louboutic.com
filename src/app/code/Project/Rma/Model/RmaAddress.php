<?php
namespace Project\Rma\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Project\Rma\Model\ResourceModel\RmaAddress as RmaAddressResource;
use Project\Rma\Model\RmaFactory;

/**
 * Class RmaAddress
 * @package Project\Rma\Model
 * @author Synolia <contact@synolia.com>
 */
class RmaAddress extends AbstractModel
{
    /**
     * Entity Id
     */
    const ENTITY_ID = 'entity_id';

    /**
     * Parent Id
     */
    const PARENT_ID = 'parent_id';

    /**
     * Region Id
     */
    const REGION_ID = 'region_id';

    /**
     * Fax
     */
    const FAX = 'fax';

    /**
     * Region
     */
    const REGION = 'region';

    /**
     * Post code
     */
    const POSTCODE = 'postcode';

    /**
     * Lastname
     */
    const LASTNAME = 'lastname';

    /**
     * Street
     */
    const STREET = 'street';

    /**
     * City
     */
    const CITY = 'city';

    /**
     * Email
     */
    const EMAIL = 'email';

    /**
     * Telephone
     */
    const TELEPHONE = 'telephone';

    /**
     * Country Id
     */
    const COUNTRY_ID = 'country_id';

    /**
     * Firstname
     */
    const FIRSTNAME = 'firstname';

    /**
     * Prefix
     */
    const PREFIX = 'prefix';

    /**
     * Middlename
     */
    const MIDDLENAME = 'middlename';

    /**
     * Suffix
     */
    const SUFFIX = 'suffix';

    /**
     * Company
     */
    const COMPANY = 'company';

    /**
     * @var Rma
     */
    protected $rma;

    /**
     * @var RmaFactory
     */
    protected $rmaFactory;

    /**
     * RmaAddress constructor.
     * @param Context $context
     * @param Registry $registry
     * @param \Project\Rma\Model\RmaFactory $rmaFactory
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RmaFactory $rmaFactory,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
        $this->rmaFactory = $rmaFactory;
    }

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(RmaAddressResource::class);
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return (int) $this->getData(self::ENTITY_ID);
    }

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->setData(self::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return (int) $this->getData(self::PARENT_ID);
    }

    /**
     * @param int $parentId
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->setData(self::PARENT_ID, $parentId);
        return $this;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return (int) $this->getData(self::REGION_ID);
    }

    /**
     * @param int $regionId
     * @return $this
     */
    public function setRegionId($regionId)
    {
        $this->setData(self::REGION_ID, $regionId);
        return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->getData(self::FAX);
    }

    /**
     * @param string $fax
     * @return $this
     */
    public function setFax($fax)
    {
        $this->setData(self::FAX, $fax);
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->getData(self::REGION);
    }

    /**
     * @param string $region
     * @return $this
     */
    public function setRegion($region)
    {
        $this->setData(self::REGION, $region);
        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->getData(self::POSTCODE);
    }

    /**
     * @param string $postcode
     * @return $this
     */
    public function setPostcode($postcode)
    {
        $this->setData(self::POSTCODE, $postcode);
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->getData(self::LASTNAME);
    }

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->setData(self::LASTNAME, $lastname);
        return $this;
    }

    /**
     * Retrieve street field of an address
     *
     * @return string[]
     */
    public function getStreet()
    {
        if (is_array($this->getData(self::STREET))) {
            return $this->getData(self::STREET);
        }
        return explode(PHP_EOL, $this->getData(self::STREET));
    }

    /**
     * Get street line by number
     *
     * @param int $number
     * @return string
     */
    public function getStreetLine($number)
    {
        $lines = $this->getStreet();
        return isset($lines[$number - 1]) ? $lines[$number - 1] : '';
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->setData(self::STREET, $street);
        return $this;
    }

    /**
     * Combine values of street lines into a single string
     *
     * @param string[]|string $value
     * @return string
     */
    protected function implodeStreetValue($value)
    {
        if (is_array($value)) {
            $value = trim(implode(PHP_EOL, $value));
        }
        return $value;
    }

    /**
     * Enforce format of the street field
     *
     * @param array|string $key
     * @param null $value
     * @return \Magento\Framework\DataObject
     */
    public function setData($key, $value = null)
    {
        if (is_array($key)) {
            $key = $this->implodeStreetField($key);
        } elseif ($key == self::STREET) {
            $value = $this->implodeStreetValue($value);
        }
        return parent::setData($key, $value);
    }

    /**
     * Implode value of the street field, if it is present among other fields
     *
     * @param array $data
     * @return array
     */
    protected function implodeStreetField(array $data)
    {
        if (array_key_exists(self::STREET, $data)) {
            $data[self::STREET] = $this->implodeStreetValue($data[self::STREET]);
        }
        return $data;
    }

    /**
     * Create fields street1, street2, etc.
     *
     * To be used in controllers for views data
     *
     * @return $this
     */
    public function explodeStreetAddress()
    {
        $streetLines = $this->getStreet();
        foreach ($streetLines as $lineNumber => $lineValue) {
            $this->setData(self::STREET . ($lineNumber + 1), $lineValue);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->setData(self::CITY, $city);
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->setData(self::EMAIL, $email);
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->getData(self::TELEPHONE);
    }

    /**
     * @param string $telephone
     * @return $this
     */
    public function setTelephone($telephone)
    {
        $this->setData(self::TELEPHONE, $telephone);
        return $this;
    }

    /**
     * @return string
     */
    public function getCountryId()
    {
        return $this->getData(self::COUNTRY_ID);
    }

    /**
     * @param string $countryId
     * @return $this
     */
    public function setCountryId($countryId)
    {
        $this->setData(self::COUNTRY_ID, $countryId);
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->getData(self::FIRSTNAME);
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->setData(self::FIRSTNAME, $firstname);
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->getData(self::PREFIX);
    }

    /**
     * @param string $prefix
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->setData(self::PREFIX, $prefix);
        return $this;
    }

    /**
     * @return string
     */
    public function getMiddlename()
    {
        return $this->getData(self::MIDDLENAME);
    }

    /**
     * @param string $middlename
     * @return $this
     */
    public function setMiddlename($middlename)
    {
        $this->setData(self::MIDDLENAME, $middlename);
        return $this;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        return $this->getData(self::SUFFIX);
    }

    /**
     * @param string $suffix
     * @return $this
     */
    public function setSuffix($suffix)
    {
        $this->setData(self::SUFFIX, $suffix);
        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->getData(self::COMPANY);
    }

    /**
     * @param string $company
     * @return $this
     */
    public function setCompany($company)
    {
        $this->setData(self::COMPANY, $company);
        return $this;
    }

    /**
     * @return Rma
     */
    public function getRma()
    {
        if (!$this->rma) {
            $this->rma = $this->rmaFactory->create()->load($this->getParentId());
        }

        return $this->rma;
    }
}
