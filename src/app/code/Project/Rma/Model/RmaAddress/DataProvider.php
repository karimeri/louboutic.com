<?php
namespace Project\Rma\Model\RmaAddress;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Project\Rma\Model\ResourceModel\RmaAddress\Collection;
use Project\Rma\Model\ResourceModel\RmaAddress\CollectionFactory;

/**
 * Class DataProvider
 * @package Project\Rma\Model\RmaAddress
 * @author Synolia <contact@synolia.com>
 */
class DataProvider extends AbstractDataProvider
{
    const DATAPERSISTOR_NAME = 'project_rma_address';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var Context
     */
    protected $context;

    /**
     * DataProvider constructor
     * @param Context $context
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        Context $context,
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->dataPersistor = $dataPersistor;
        $this->collection = $collectionFactory->create();
        $this->context = $context;
    }

    /**
     * Get data
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        /** @var \Project\Rma\Model\RmaAddress[] $items */
        $items = $this->collection->getItems();
        if(count($items) == 0) {
            $address = $this->context->getRequest()->getParam('address');
            $address = (array)json_decode($address);
            unset($address['entity_id']);
            unset($address['customer_address_id']);
            unset($address['quote_address_id']);
            unset($address['customer_id']);
            unset($address['address_type']);
            unset($address['address_type']);
            unset($address['vat_id']);
            unset($address['vat_is_valid']);
            unset($address['vat_request_id']);
            unset($address['vat_request_date']);
            unset($address['vat_request_success']);
            unset($address['giftregistry_item_id']);
            unset($address['contact_phone']);
            $model = $this->collection->getNewEmptyItem();
            $model->setData($address);
            $this->loadedData[$model->getId()] = $model->getData();
        }else {
            foreach ($items as $model) {
                $this->loadedData[$model->getId()] = $model->getData();
            }
        }

        $data = $this->dataPersistor->get($this::DATAPERSISTOR_NAME);

        if (!empty($data)) {
            /** @var \Project\Rma\Model\RmaAddress $model */
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear($this::DATAPERSISTOR_NAME);
        }

        return $this->loadedData;
    }
}
