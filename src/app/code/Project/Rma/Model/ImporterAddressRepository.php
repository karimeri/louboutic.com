<?php
namespace Project\Rma\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Rma\Model\Rma;
use Magento\Sales\Model\Order\Address;
use Project\Rma\Model\ImporterAddressFactory;
use Project\Rma\Model\ResourceModel\ImporterAddress as ImporterAddressResource;

/**
 * Class RmaAddressRepository
 * @package Project\Rma\Model
 * @author Synolia <contact@synolia.com>
 */
class ImporterAddressRepository
{
    /**
     * @var ImporterAddressResource
     */
    protected $rmaAddressResource;

    /**
     * @var RmaAddress
     */
    protected $rmaAddress;

    /**
     * @var \Project\Rma\Model\ImporterAddressFactory
     */
    protected $rmaAddressFactory;

    /**
     * RmaAddressRepository constructor.
     * @param ImporterAddressResource $ImporterAddressResource
     * @param \Project\Rma\Model\ImporterAddressFactory $rmaAddressFactory
     * @param ImporterAddress $rmaAddress
     */
    public function __construct(
        ImporterAddressResource $rmaAddressResource,
        ImporterAddressFactory $rmaAddressFactory,
        ImporterAddress $rmaAddress
    ) {
        $this->rmaAddressResource = $rmaAddressResource;
        $this->rmaAddress = $rmaAddress;
        $this->rmaAddressFactory = $rmaAddressFactory;
    }

    /**
     * @param int $id
     * @return RmaAddress
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $rmaAddress = $this->rmaAddressFactory->create();
        $this->rmaAddressResource->load($rmaAddress, $id);
        if (!$rmaAddress->getId()) {
            throw new NoSuchEntityException(__('Requested RMA Importer Address doesn\'t exist'));
        }

        return $rmaAddress;
    }

    public function saveAddressData(ImporterAddress $rmaAddress, $data)
    {
        $rmaAddress
            ->setData($data);
    }

    /**
     * @param RmaAddress $rmaAddress
     */
    public function save(ImporterAddress $rmaAddress)
    {
        $this->rmaAddressResource->save($rmaAddress);
    }
}
