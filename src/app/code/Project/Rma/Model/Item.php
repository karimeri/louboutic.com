<?php

namespace Project\Rma\Model;

use Magento\Rma\Model\Item as MagentoItem;

/**
 * Class Item
 *
 * @package Project\Rma\Model
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 */
class Item extends MagentoItem
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'rma_item_enterprise';

    /**
     * @var string
     */
    protected $_eventObject = 'rma_item';

    /**
     * @return int|null
     */
    public function getQty()
    {
        if ($this->getQtyApproved()) {
            return $this->getQtyApproved();
        } elseif ($this->getQtyAuthorized()) {
            return $this->getQtyAuthorized();
        }

        return $this->getQtyRequested();
    }
}
