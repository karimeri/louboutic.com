<?php

namespace Project\Rma\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote\Address\Rate;
use Magento\Rma\Model\Shipping\LabelService;
use Project\Rma\Helper\Config as RmaConfigHelper;
use Project\Rma\Helper\Pickup;

/**
 * Class LabelManager
 * @package Project\Rma\Model
 */
class LabelManager
{
    const CONTAINER = [
        'dhl' => 'N',
        'ups' => '00',
    ];

    /**
     * @var LabelService
     */
    protected $labelService;

    /**
     * @var RmaConfigHelper
     */
    protected $rmaConfigHelper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * LabelManager constructor.
     * @param LabelService $labelService
     * @param RmaConfigHelper $rmaConfigHelper
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        LabelService $labelService,
        RmaConfigHelper $rmaConfigHelper,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->labelService     = $labelService;
        $this->rmaConfigHelper  = $rmaConfigHelper;
        $this->priceCurrency    = $priceCurrency;
    }

    /**
     * @param Rma   $rma
     * @param Rate $method
     * @throws LocalizedException
     */
    public function generateLabel(Rma $rma, Rate $method)
    {
        // Get package params
        $params = $this->getPackageParams($rma, $method);
        // Label generation
        $this->labelService->createShippingLabel($rma, $params);
    }

    /**
     * @param Rma  $rma
     * @param Rate $method
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getPackageParams(Rma $rma, Rate $method)
    {
        $items = $rma->getShippingMethods(true);
        $paramsItems = [];
        $customValue = 0;
        foreach ($items as $item) {
            $paramsItems[$item->getId()] = [
                'qty' => $item->getQty(),
                'price' => $item->getPrice(),
                'customs_value' => $item->getPrice(),
                'name' => $rma->getIncrementId(),
                'weight' => $item->getWeight(),
                'product_id' => $item->getProductId(),
                'order_item_id' => $item->getItemId(),
            ];

            $customValue += $item->getPrice() * $item->getQty();
        }

        $rmaStoreCode = $this->rmaConfigHelper->getRmaStoreCode($rma);

        $params = [
            'code' => $method->getCode(),
            'carrier_title' => $method->getCarrierTitle(),
            'method_title' => (string) $method->getMethodTitle(),
            'price' => $this->priceCurrency->convert($method->getPrice(), true, false),
            'packages' => [
                1 => [
                    'params' => [
                        'container' => self::CONTAINER[\strtolower($method->getCarrier())],
                        'weight' => $this->rmaConfigHelper->getRmaEstimatedWeight(),
                        'customs_value' => $customValue,
                        'length' => '3',
                        'width' => '3',
                        'height' => '3',
                        'weight_units' => (isset(Pickup::STORES_POUND_INCH[$rmaStoreCode])) ? 'POUND' : 'KILOGRAM',
                        'dimension_units' => (isset(Pickup::STORES_POUND_INCH[$rmaStoreCode])) ? 'INCH' : 'CENTIMETER',
                        'content_type' => '',
                        'content_type_other' => '',
                    ],
                    'items' => $paramsItems,
                ],
            ],
        ];

        return $params;
    }
}