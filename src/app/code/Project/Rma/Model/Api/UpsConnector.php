<?php

namespace Project\Rma\Model\Api;

use GuzzleHttp\Client;
use Project\Rma\Helper\Config;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Logger\Model\Logger;
use Magento\Framework\Filesystem\DirectoryList;
use GuzzleHttp\Exception\RequestException;
use Zend\Http\Request;

/**
 * Class UpsConnector
 *
 * @package Project\Wms\Model\Api
 * @author Synolia <contact@synolia.com>
 */
class UpsConnector
{
    const ERROR_LOG_FILENAME = 'ups_api.log';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * UpsConnector constructor.
     *
     * @param \Project\Rma\Helper\Config $config
     * @param \GuzzleHttp\Client $client
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     *
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Config $config,
        Client $client,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList
    ) {
        $this->config = $config;
        $this->client = $client;

        $logPath = $directoryList->getPath('log');
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath.DIRECTORY_SEPARATOR.self::ERROR_LOG_FILENAME]
        );
    }

    /**
     * @param string $path
     * @param int|string $storeId
     * @param array $headers
     * @param string $data
     * @param string $method
     *
     * @return mixed
     * @throws \Exception
     */
    public function request($path, $storeId, $headers, $data = '', $method = Request::METHOD_POST)
    {
        try {
            $response = $this->client->request(
                $method,
                $path,
                [
                    'headers' => $headers,
                    'body' => $data,
                ]
            );

            return \GuzzleHttp\json_decode($response->getBody()->getContents());
        } catch (RequestException $exception) {
            $message = $this->processException($exception);
            throw new \Exception($message);
        }
    }

    /**
     * @param string|int $storeId
     * @param string $data
     * @param string $method
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function requestPickup($storeId, $data, $method = Request::METHOD_POST)
    {
        return $this->request(
            $this->config->getRmaUpsRestUrl($storeId),
            $storeId,
            [
                'content-type' => 'application/json',
                'accept' => 'application/json',
                'Username' => $this->config->getRmaUpsUsername($storeId),
                'Password' => $this->config->getRmaUpsPassword($storeId),
                'AccessLicenseNumber' => $this->config->getRmaUpsAccessLicenseNumber($storeId),
            ],
            $data,
            $method
        );
    }

    /**
     * @param string|int $storeId
     * @param string $prn
     * @param string $data
     * @param string $method
     *
     * @return mixed
     * @throws \Exception
     */
    public function requestPickupCancel($storeId, $prn, $data = '', $method = Request::METHOD_DELETE)
    {
        return $this->request(
            $this->config->getRmaUpsCancelRestUrl($storeId),
            $storeId,
            [
                'content-type' => 'application/json',
                'accept' => 'application/json',
                'Username' => $this->config->getRmaUpsUsername($storeId),
                'Password' => $this->config->getRmaUpsPassword($storeId),
                'AccessLicenseNumber' => $this->config->getRmaUpsAccessLicenseNumber($storeId),
                'Prn' => $prn,
            ],
            $data,
            $method
        );
    }

    /**
     * @param RequestException $exception
     * @return string
     */
    protected function processException(RequestException $exception)
    {
        if ($exception->hasResponse()) {
            $body = \GuzzleHttp\json_decode($exception->getResponse()->getBody());
            if (isset($body->response->errors[0])) {
                $error = $body->response->errors[0];
                $message = "CODE : {$error->code}, message : $error->message";
            } else {
                $message = print_r($body, true);
            }

            $this->logger->addError($message);
        } else {
            $message = "CODE : {$exception->getCode()}, message : {$exception->getMessage()}";
            $this->logger->addError($message);
        }

        return $message;
    }
}
