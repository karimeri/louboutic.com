<?php
namespace Project\Rma\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Project\Rma\Model\ResourceModel\ImporterAddress as ImporterAddressResource;
/**
 * Class RmaAddress
 * @package Project\Rma\Model
 * @author Synolia <contact@synolia.com>
 */
class ImporterAddress extends AbstractModel
{
    /**
     * Entity Id
     */
    const ENTITY_ID = 'entity_id';

    /**
     * address
     */
    const ADDRESS_NAME = 'address_name';
    const ADDRESS_COMPANY = 'address_company';
    const ADDRESS_STREET = 'address_street';
    const ADDRESS_CODE_POSTAL = 'address_code_postal';
    const ADDRESS_CITY = 'address_city';
    const ADDRESS_COUNTRY = 'address_country';
    const ADDRESS_VAT = 'address_vat';





    /**
     * RmaAddress constructor.
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(ImporterAddressResource::class);
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return (int) $this->getData(self::ENTITY_ID);
    }

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->setData(self::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressName()
    {
        return (int) $this->getData(self::ADDRESS_NAME);
    }

    /**
     * @param string $AddressName
     * @return $this
     */
    public function setAddressName($AddressName)
    {
        $this->setData(self::ADDRESS_NAME, $AddressName);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCompany()
    {
        return (int) $this->getData(self::ADDRESS_COMPANY);
    }

    /**
     * @param string $AddressCompany
     * @return $this
     */
    public function setAddressCompany($AddressCompany)
    {
        $this->setData(self::ADDRESS_COMPANY, $AddressCompany);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressStreet()
    {
        return (int) $this->getData(self::ADDRESS_STREET);
    }

    /**
     * @param string $AddressStreet
     * @return $this
     */
    public function setAddressStreet($AddressStreet)
    {
        $this->setData(self::ADDRESS_STREET, $AddressStreet);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCodePostal()
    {
        return (int) $this->getData(self::ADDRESS_CODE_POSTAL);
    }

    /**
     * @param string $AddressCodePostal
     * @return $this
     */
    public function setAddressCodePostal($AddressCodePostal)
    {
        $this->setData(self::ADDRESS_CODE_POSTAL, $AddressCodePostal);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return (int) $this->getData(self::ADDRESS_CITY);
    }

    /**
     * @param string $AddressCity
     * @return $this
     */
    public function setAddressCity($AddressCity)
    {
        $this->setData(self::ADDRESS_City, $AddressCity);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCountry()
    {
        return (int) $this->getData(self::ADDRESS_COUNTRY);
    }

    /**
     * @param string $AddressCountry
     * @return $this
     */
    public function setAddressCountry($AddressCountry)
    {
        $this->setData(self::ADDRESS_COUNTRY, $AddressCountry);
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressVat()
    {
        return (int) $this->getData(self::ADDRESS_VAT);
    }

    /**
     * @param string $AddressVat
     * @return $this
     */
    public function setAddressVat($AddressVat)
    {
        $this->setData(self::ADDRESS_VAT, $AddressVat);
        return $this;
    }

    public function saveAddressData(ImporterAddress $rmaAddress, $data)
    {
        $rmaAddress
            ->setData($data);
    }


}
