<?php
namespace Project\Rma\Api\Data;

/**
 * Interface ReturnAddressInterface
 * @package Project\Rma\Api\Data
 * @author Synolia <contact@synolia.com>
 */
interface ReturnAddressInterface
{
    /**
     * Entity Id
     */
    const ENTITY_ID = 'entity_id';

    /**
     * Name
     */
    const NAME = 'name';

    /**
     * Region
     */
    const REGION = 'region';

    /**
     * Post code
     */
    const POSTCODE = 'postcode';

    /**
     * Street line 1
     */
    const STREET1 = 'street1';

    /**
     * Street line 2
     */
    const STREET2 = 'street2';

    /**
     * Street line 3
     */
    const STREET3 = 'street3';

    /**
     * City
     */
    const CITY = 'city';

    /**
     * Contact name
     */
    const CONTACT_NAME = 'contact_name';

    /**
     * Contact phone
     */
    const CONTACT_PHONE = 'contact_phone';

    /**
     * Country Id
     */
    const COUNTRY_ID = 'country_id';

    /**
     * Email template
     */
    const EMAIL_TEMPLATE = 'email_template';

    /**
     * Company
     */
    const COMPANY = 'company';

    /**
     * Establishment Code
     */
    const ESTABLISHMENT_CODE = 'establishment_code';

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getRegion();

    /**
     * @param string $region
     * @return $this
     */
    public function setRegion($region);

    /**
     * @return string
     */
    public function getPostcode();

    /**
     * @param string $postcode
     * @return $this
     */
    public function setPostcode($postcode);

    /**
     * @return string
     */
    public function getContactName();

    /**
     * @param string $contactName
     * @return $this
     */
    public function setContactName($contactName);

    /**
     * @return string
     */
    public function getContactPhone();

    /**
     * @param string $contactPhone
     * @return $this
     */
    public function setContactPhone($contactPhone);

    /**
     * @return string
     */
    public function getStreet1();

    /**
     * @param string $street1
     * @return $this
     */
    public function setStreet1($street1);

    /**
     * @return string
     */
    public function getStreet2();

    /**
     * @param string $street2
     * @return $this
     */
    public function setStreet2($street2);

    /**
     * @return string
     */
    public function getStreet3();

    /**
     * @param string $street3
     * @return $this
     */
    public function setStreet3($street3);

    /**
     * @return string
     */
    public function getCity();

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city);

    /**
     * @return string
     */
    public function getEmailTemplate();

    /**
     * @param string $emailTemplate
     * @return $this
     */
    public function setEmail($emailTemplate);

    /**
     * @return string
     */
    public function getCountryId();

    /**
     * @param string $countryId
     * @return $this
     */
    public function setCountryId($countryId);

    /**
     * @return string
     */
    public function getCompany();

    /**
     * @param string $company
     * @return $this
     */
    public function setCompany($company);

    /**
     * @return string
     */
    public function getEstablishmentCode();

    /**
     * @param string $establishmentCode
     * @return $this
     */
    public function setEstablishmentCode($establishmentCode);
}
