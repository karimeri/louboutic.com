<?php

namespace Project\Rma\Console\Command;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Registry;
use Magento\Rma\Api\Data\RmaSearchResultInterface;
use Magento\Rma\Model\ResourceModel\Item as ItemResource;
use Magento\Rma\Model\Item;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Rma\Helper\Pickup;
use Project\Rma\Manager\LabelGenerationManager;
use Project\Rma\Model\Magento\Rma\Rma\Source\Status;
use Project\Rma\Model\Rma;
use Magento\Rma\Model\Rma\Status\History;
use Magento\Rma\Model\Rma\Status\HistoryFactory;
use Magento\Rma\Model\RmaRepository;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Synolia\Logger\Model\Logger;
use Synolia\Logger\Model\LoggerFactory;
use \Magento\Framework\App\AreaList;

/**
 * Class DeleteEmailSent
 * @package Project\Rma\Console\Command
 * @author  Synolia <contact@synolia.com>
 */
class GenerateLabel extends Command
{
    const ERROR_FILENAME = 'rma_labels_generation.log';

    const CONTAINER = [
        'dhl' => 'N',
        'ups' => '00',
    ];

    /**
     * @var Rma
     */
    protected $rma;

    /**
     * @var State
     */
    protected $state;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var Item
     */
    protected $itemResource;

    /**
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var LoggerFactory
     */
    protected $loggerFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var History
     */
    protected $rmaHistory;

    /**
     * @var HistoryFactory
     */
    protected $rmaHistoryFactory;

    /**
     * @var Pickup
     */
    protected $pickupHelper;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var LabelGenerationManager
     */
    protected $labelGenerationManager;

    /**
     * @var AreaList
     */
    protected $areaList;

    /**
     * GenerateLabel constructor.
     * @param State $state
     * @param RmaRepository $rmaRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ResourceConnection $resource
     * @param ItemResource $itemResource
     * @param Registry $coreRegistry
     * @param StoreManagerInterface $storeManager
     * @param LoggerFactory $loggerFactory
     * @param DirectoryList $directoryList
     * @param HistoryFactory $rmaHistoryFactory
     * @param Pickup $pickupHelper
     * @param EnvironmentManager $environmentManager
     * @param LabelGenerationManager $labelGenerationManager
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        State $state,
        RmaRepository $rmaRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ResourceConnection $resource,
        ItemResource $itemResource,
        Registry $coreRegistry,
        StoreManagerInterface $storeManager,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        HistoryFactory $rmaHistoryFactory,
        Pickup $pickupHelper,
        EnvironmentManager $environmentManager,
        LabelGenerationManager $labelGenerationManager,
        AreaList $areaList
    ) {
        parent::__construct();
        $this->state = $state;
        $this->rmaRepository = $rmaRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->resource = $resource;
        $this->itemResource = $itemResource;
        $this->coreRegistry = $coreRegistry;
        $this->storeManager = $storeManager;
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log') . DIRECTORY_SEPARATOR . self::ERROR_FILENAME]
        );
        $this->rmaHistoryFactory = $rmaHistoryFactory;
        $this->pickupHelper = $pickupHelper;
        $this->environmentManager = $environmentManager;
        $this->labelGenerationManager = $labelGenerationManager;
        $this->areaList = $areaList;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:rmalabel:generate')
            ->setDescription('Generate RMA labels');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(Area::AREA_FRONTEND);
        } catch (\Throwable $throwable) {
        }

        $this->output = $output;
        $this->connection = $this->resource->getConnection();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * phpcs:disable Generic.Metrics.CyclomaticComplexity
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            // Get list of pending RMA
            $rmaList = $this->getPendingRma();
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');
            $this->logger->addError($exception->getMessage());

            return Cli::RETURN_FAILURE;
        }

        $currentEnv = $this->environmentManager->getEnvironment();

        /** @var Rma $rma */
        foreach ($rmaList as $rma) {
            $this->coreRegistry->register('current_rma', $rma);

            try {
                $this->connection->beginTransaction();

                // Set items to authorized
                $this->setItemsToAuthorized($rma);

                // Update RMA status
                $this->setRmaToAuthorized($rma);

                $area = $this->areaList->getArea($this->state->getAreaCode());
                $area->load(Area::PART_TRANSLATE);
                $store = $this->storeManager->getStore($rma->getStoreId());
                // On ne génère pas les labels pour la plateforme JP et pour le website hk de la plateforme ASIA
                if ($currentEnv !== Environment::JP &&
                    (
                        $currentEnv !== Environment::HK ||
                        $currentEnv === Environment::HK &&
                        \strpos($store->getCode(), 'hk') === false
                    )
                ) {
                    // Get RMA shipping
                    $method = $this->pickupHelper->getShippingMethod($rma);

                    if (\strpos($method->getCode(), 'error') !== false) {
                        throw new \Exception(
                            __('Error in shipping method, please verify if RMA address informations are corrects')
                        );
                    }

                    // Get package params
                    $params = $this->labelGenerationManager->getPackageParams($rma, $method);

                    // Label generation
                    $this->labelGenerationManager->generateLabel($rma, $params);

                    if ($this->environmentManager->getEnvironment() !== Environment::HK
                        && $rma->getPickupDate()
                        && !$rma->getPickupConfirmationNumber()
                    ) {
                        // Pickup
                        $this->labelGenerationManager->pickupRequest($rma, $method);
                    }

                    // Send email
                    $this->labelGenerationManager->sendRmaEmail($rma, true);
                } else {
                    $this->labelGenerationManager->sendRmaEmail($rma, false);
                }

                $this->connection->commit();
            } catch (\Exception $exception) {
                $this->connection->rollBack();

                // Set RMA items status to error
                $this->setItemsToError($rma);

                // Set RMA status to error
                $this->setRmaToError($rma);

                /** @var History $statusHistory */
                $statusHistory = $this->rmaHistoryFactory->create();
                $statusHistory->setRmaEntityId($rma->getEntityId());
                $statusHistory->saveComment($exception->getMessage(), false, true);

                $errorMsg = [
                    'RMA ID : ' . $rma->getId(),
                    'Message : ' . $exception->getMessage(),
                    'File : ' . $exception->getFile(),
                    'Line : ' . $exception->getLine(),
                ];

                $output->writeln('<error>' . \implode(' || ', $errorMsg) . '</error>');
                $this->logger->addError(\implode(' || ', $errorMsg));
            }

            $this->coreRegistry->unregister('current_rma');
        }

        return Cli::RETURN_SUCCESS;
    }

    /**
     * @return RmaSearchResultInterface
     */
    private function getPendingRma()
    {
        $this->searchCriteriaBuilder->addFilter(
            Rma::STATUS,
            Status::STATE_PENDING
        );
        $searchCriteria = $this->searchCriteriaBuilder->create();

        return $this->rmaRepository->getList($searchCriteria);
    }

    /**
     * @param Rma $rma
     */
    private function setItemsToAuthorized(Rma $rma)
    {
        $itemsCollection = $rma->getItemsForDisplay();
        $itemsIds = [];
        /** @var \Magento\Rma\Model\Item $item */
        foreach ($itemsCollection as $item) {
            $itemsIds[] = $item->getId();
        }

        $this->connection->update(
            $this->itemResource->getTable('magento_rma_item_entity'),
            [
                Item::QTY_AUTHORIZED => new \Zend_Db_Expr(Item::QTY_REQUESTED),
                Item::STATUS => Status::STATE_AUTHORIZED,
            ],
            [Item::ENTITY_ID . ' IN (?)' => $itemsIds]
        );
    }

    /**
     * @param Rma $rma
     */
    private function setItemsToError(Rma $rma)
    {
        $itemsCollection = $rma->getItemsForDisplay();
        $itemsIds = [];
        /** @var \Magento\Rma\Model\Item $item */
        foreach ($itemsCollection as $item) {
            $itemsIds[] = $item->getId();
        }

        $this->connection->update(
            $this->itemResource->getTable('magento_rma_item_entity'),
            [
                Item::STATUS => Status::STATE_ERROR,
            ],
            [Item::ENTITY_ID . ' IN (?)' => $itemsIds]
        );
    }

    /**
     * @param Rma $rma
     */
    private function setRmaToAuthorized(Rma $rma)
    {
        try {
        $rma->setStatus(Status::STATE_AUTHORIZED);
        $this->rmaRepository->save($rma);
        } catch (\Throwable $throwable) {
        }
    }

    /**
     * @param Rma $rma
     */
    private function setRmaToError(Rma $rma)
    {
        try {
            $rma->setStatus(Status::STATE_ERROR);
            $this->rmaRepository->save($rma);
        } catch (\Throwable $throwable) {
        }
    }
}
