<?php

namespace Project\Rma\Console\Command;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Console\Cli;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Rma\Api\Data\RmaSearchResultInterface;
use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Rma\Model\ResourceModel\Item as ItemResource;
use Magento\Rma\Model\ResourceModel\Rma as RmaResource;
use Magento\Rma\Model\Rma\Status\HistoryFactory;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Rma\Helper\Alerting as AlertingHelper;
use Project\Rma\Helper\Config as ConfigHelper;
use Project\Rma\Helper\Pickup as PickupHelper;
use Project\Rma\Manager\LabelGenerationManager;
use Project\Rma\Model\Magento\Rma\Rma\Source\Status;
use Project\Rma\Model\Rma;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Synolia\Logger\Model\Logger;
use Synolia\Logger\Model\LoggerFactory;

/**
 * Class RetryGenerateLabel
 * @package Project\Rma\Console\Command
 */
class RetryGenerateLabel extends Command
{
    const ERROR_FILENAME = 'rma_labels_retry.log';

    /**
     * @var State
     */
    protected $state;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * @var LoggerFactory
     */
    protected $loggerFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var RmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var RmaResource
     */
    protected $rmaResource;

    /**
     * @var ItemResource
     */
    protected $itemResource;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var AlertingHelper
     */
    protected $alertingHelper;

    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * @var PickupHelper
     */
    protected $pickupHelper;

    /**
     * @var LabelGenerationManager
     */
    protected $labelGenerationManager;

    /**
     * @var HistoryFactory
     */
    protected $rmaHistoryFactory;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * RetryGenerateLabel constructor.
     * @param State $state
     * @param LoggerFactory $loggerFactory
     * @param DirectoryList $directoryList
     * @param RmaRepositoryInterface $rmaRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ResourceConnection $resource
     * @param EnvironmentManager $environmentManager
     * @param Registry $coreRegistry
     * @param RmaResource $rmaResource
     * @param ItemResource $itemResource
     * @param StoreManagerInterface $storeManager
     * @param AlertingHelper $alertingHelper
     * @param ConfigHelper $configHelper
     * @param PickupHelper $pickupHelper
     * @param LabelGenerationManager $labelGenerationManager
     * @param HistoryFactory $historyFactory
     * @param TimezoneInterface $timezone
     * @param null $name
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        State $state,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        RmaRepositoryInterface $rmaRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ResourceConnection $resource,
        EnvironmentManager $environmentManager,
        Registry $coreRegistry,
        RmaResource $rmaResource,
        ItemResource $itemResource,
        StoreManagerInterface $storeManager,
        AlertingHelper $alertingHelper,
        ConfigHelper $configHelper,
        PickupHelper $pickupHelper,
        LabelGenerationManager $labelGenerationManager,
        HistoryFactory $historyFactory,
        TimezoneInterface $timezone,
        $name = null
    ) {
        parent::__construct($name);
        $this->state                    = $state;
        $this->rmaRepository            = $rmaRepository;
        $this->searchCriteriaBuilder    = $searchCriteriaBuilder;
        $this->resource                 = $resource;
        $this->environmentManager       = $environmentManager;
        $this->coreRegistry             = $coreRegistry;
        $this->rmaResource              = $rmaResource;
        $this->itemResource             = $itemResource;
        $this->storeManager             = $storeManager;
        $this->alertingHelper           = $alertingHelper;
        $this->configHelper             = $configHelper;
        $this->pickupHelper             = $pickupHelper;
        $this->labelGenerationManager   = $labelGenerationManager;
        $this->rmaHistoryFactory        = $historyFactory;
        $this->timezone                 = $timezone;
        $this->loggerFactory            = $loggerFactory;
        $this->logger                   = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log') . DIRECTORY_SEPARATOR . self::ERROR_FILENAME]
        );
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:rmalabel:retry')
            ->setDescription('Retry RMA labels generation');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(Area::AREA_GLOBAL);
        } catch (\Throwable $throwable) {
        }

        $this->output = $output;
        $this->connection = $this->resource->getConnection();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            // Get list of retry RMA
            $rmaList = $this->getRetryRma();
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');
            $this->logger->addError($exception->getMessage());

            return Cli::RETURN_FAILURE;
        }

        $currentEnv = $this->environmentManager->getEnvironment();

        /** @var Rma $rma */
        foreach ($rmaList as $rma) {
            $this->coreRegistry->register('current_rma', $rma);

            try {
                $this->connection->beginTransaction();

                // Set items to authorized
                $this->labelGenerationManager->setItemsToAuthorized($rma);

                // Update RMA status
                $this->labelGenerationManager->setRmaToAuthorized($rma);

                $store = $this->storeManager->getStore($rma->getStoreId());
                // On ne génère pas les labels pour la plateforme JP et pour le website hk de la plateforme ASIA
                if ($currentEnv !== Environment::JP &&
                    (
                        $currentEnv !== Environment::HK ||
                        $currentEnv === Environment::HK &&
                        \strpos($store->getCode(), 'hk') === false
                    )
                ) {
                    // Get RMA shipping
                    $method = $this->pickupHelper->getShippingMethod($rma);

                    if (\strpos($method->getCode(), 'error') !== false) {
                        throw new \Exception(
                            __('Error in shipping method, please verify if RMA address informations are corrects')
                        );
                    }

                    // Get package params
                    $params = $this->labelGenerationManager->getPackageParams($rma, $method);

                    // Label generation
                    $this->labelGenerationManager->generateLabel($rma, $params);

                    if ($this->environmentManager->getEnvironment() !== Environment::HK
                        && $rma->getPickupDate()
                        && !$rma->getPickupConfirmationNumber()
                    ) {
                        // Pickup
                        $this->labelGenerationManager->pickupRequest($rma, $method);
                    }

                    // Send email
                    $this->labelGenerationManager->sendRmaEmail($rma, true);
                } else {
                    $this->labelGenerationManager->sendRmaEmail($rma, false);
                }

                $this->connection->commit();
            } catch (\Exception $exception) {
                $this->connection->rollBack();

                // count errors
                $retryCount = $this->setRetryCount($rma);

                if ($retryCount === 3) {
                    $this->sendAlertEmail($rma, $exception);
                }

                /** @var Rma\Status\History $statusHistory */
                $statusHistory = $this->rmaHistoryFactory->create();
                $statusHistory->setRmaEntityId($rma->getEntityId());
                $statusHistory->saveComment($exception->getMessage(), false, true);

                $errorMsg = [
                    'RMA ID : ' . $rma->getId(),
                    'Message : ' . $exception->getMessage(),
                    'File : ' . $exception->getFile(),
                    'Line : ' . $exception->getLine(),
                ];

                $output->writeln('<error>' . \implode(' || ', $errorMsg) . '</error>');
                $this->logger->addError(\implode(' || ', $errorMsg));
            }

            $this->coreRegistry->unregister('current_rma');
        }

        return Cli::RETURN_SUCCESS;
    }

    /**
     * @return RmaSearchResultInterface
     */
    private function getRetryRma()
    {
        // filter RMAs with 'error' status
        $this->searchCriteriaBuilder->addFilter(
            Rma::STATUS,
            Status::STATE_ERROR
        );
        // filter RMAs with retry max limit
        $this->searchCriteriaBuilder->addFilter(
            Rma::RETRY_COUNT,
            $this->configHelper->getRmaRetryLimit(),
            'lt'
        );
        $searchCriteria = $this->searchCriteriaBuilder->create();

        return $this->rmaRepository->getList($searchCriteria);
    }

    /**
     * @param Rma $rma
     * @return int|mixed
     */
    private function setRetryCount(Rma $rma)
    {
        $retryCount = $rma->getData('retry_count') + 1;

        $this->connection->update(
            $this->rmaResource->getTable('magento_rma'),
            [
                Rma::RETRY_COUNT => $retryCount,
            ],
            [Rma::ENTITY_ID . ' = ?' => $rma->getEntityId()]
        );

        return $retryCount;
    }

    private function sendAlertEmail(Rma $rma, \Exception $exception)
    {
        if ($this->alertingHelper->isEnabled()) {
            $templateVars = [
                'environment' => $this->environmentManager->getEnvironment(),
                'rmaId' => $rma->getEntityId(),
                'errorMsg' => $exception->getMessage(),
                'dateTime' => $this->timezone->date()->format('Y-m-d H:i:s')
            ];

            $this->alertingHelper->alert($templateVars);
        }
    }
}
