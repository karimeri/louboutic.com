<?php
namespace Project\Rma\Controller\Magento\Sales\Guest;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Controller\Guest\View as ViewBase;
use Magento\Sales\Helper\Guest as GuestHelperBase;
use Project\Rma\Helper\Magento\Sales\Guest as GuestHelper;

/**
 * Class View
 * @package Project\Rma\Controller\Order
 * @author  Synolia <contact@synolia.com>
 */
class View extends ViewBase
{
    /**
     * @var GuestHelper
     */
    protected $projectGuestHelper;

    /**
     * View constructor.
     * @param Context         $context
     * @param GuestHelperBase $guestHelper
     * @param PageFactory     $resultPageFactory
     * @param GuestHelper     $projectGuestHelper
     */
    public function __construct(
        Context $context,
        GuestHelperBase $guestHelper,
        PageFactory $resultPageFactory,
        GuestHelper $projectGuestHelper
    ) {
        parent::__construct(
            $context,
            $guestHelper,
            $resultPageFactory
        );
        $this->projectGuestHelper = $projectGuestHelper;
    }

    /**
     * @return ResultInterface
     */
    public function execute()
    {
        $result = $this->projectGuestHelper->checkEmail($this->getRequest());

        if ($result instanceof ResultInterface) {
            return $result;
        }

        $result = $this->projectGuestHelper->loadValidOrder($this->getRequest());

        if ($result instanceof ResultInterface) {
            return $result;
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->projectGuestHelper->getBreadcrumbs($resultPage);
        return $resultPage;
    }
}
