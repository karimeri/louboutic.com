<?php
namespace Project\Rma\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\Rma\Helper\Magento\Sales\Guest as GuestHelper;

/**
 * Class Search
 * @package Project\Rma\Controller\Order
 * @author  Synolia <contact@synolia.com>
 */
class Search extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var GuestHelper
     */
    protected $guestHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Search constructor.
     * @param Context                                            $context
     * @param PageFactory                                        $resultPageFactory
     * @param GuestHelper                                        $guestHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        GuestHelper $guestHelper,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->guestHelper = $guestHelper;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @return Redirect|Page
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        if ($post) {
            $result = $this->guestHelper->checkEmail($this->getRequest());

            if ($result instanceof ResultInterface) {
                return $result;
            }

            $result = $this->guestHelper->loadValidOrder($this->getRequest());

            if ($result instanceof ResultInterface) {
                return $result;
            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(
            'Create return'
        );

        return $resultPage;
    }
}
