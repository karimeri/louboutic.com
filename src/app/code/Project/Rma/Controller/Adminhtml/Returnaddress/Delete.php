<?php
namespace Project\Rma\Controller\Adminhtml\Returnaddress;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Project\Rma\Model\ReturnAddressRepository;

/**
 * Class Delete
 * @package Project\Rma\Controller\Adminhtml\Returnaddress
 * @author Synolia <contact@synolia.com>
 */
class Delete extends Action
{
    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param ReturnAddressRepository $returnAddressRepository
     */
    public function __construct(
        Context $context,
        ReturnAddressRepository $returnAddressRepository
    ) {
        parent::__construct($context);
        $this->returnAddressRepository = $returnAddressRepository;
    }

    /**
     * Delete action
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $addressId = $this->getRequest()->getParam('entity_id');

        if ($addressId) {
            try {
                $this->returnAddressRepository->deleteById($addressId);
                $this->messageManager->addSuccessMessage(__('You deleted the return address.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $addressId]);
            }
        }

        $this->messageManager->addErrorMessage(__('We can\'t find a return address to delete.'));

        return $resultRedirect->setPath('*/*/');
    }
}
