<?php
namespace Project\Rma\Controller\Adminhtml\Returnaddress;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Project\Rma\Model\ReturnAddress\DataProvider;
use Project\Rma\Model\ReturnAddressFactory;
use Project\Rma\Model\ReturnAddressRepository;

/**
 * Class Save
 * @package Project\Rma\Controller\Adminhtml\Returnaddress
 * @author Synolia <contact@synolia.com>
 */
class Save extends Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * @var ReturnAddressFactory
     */
    protected $returnAddressFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param ReturnAddressFactory $returnAddressFactory
     * @param ReturnAddressRepository $returnAddressRepository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        ReturnAddressFactory $returnAddressFactory,
        ReturnAddressRepository $returnAddressRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->returnAddressFactory = $returnAddressFactory;
        $this->returnAddressRepository = $returnAddressRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            $model = $this->returnAddressFactory->create();

            if ($data['entity_id']) {
                try {
                    /** @var \Project\Rma\Model\ReturnAddress $model */
                    $model = $this->returnAddressRepository->getById($data['entity_id']);
                } catch (NoSuchEntityException $exception) {
                    $this->messageManager->addErrorMessage(__('This Return Address no longer exists.'));

                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->returnAddressRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the Return Address.'));
                $this->dataPersistor->clear(DataProvider::DATAPERSISTOR_NAME);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $exception) {
                $this->messageManager->addExceptionMessage($exception->getPrevious() ?: $exception);
            } catch (\Exception $exception) {
                $this->messageManager->addExceptionMessage(
                    $exception,
                    __('Something went wrong while saving the address.')
                );
            }

            $this->dataPersistor->set(DataProvider::DATAPERSISTOR_NAME, $data);

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
