<?php
namespace Project\Rma\Controller\Adminhtml\Returnaddress;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Project\Rma\Model\ReturnAddressFactory;
use Project\Rma\Model\ReturnAddressRepository;

/**
 * Class Edit
 * @package Project\Rma\Controller\Adminhtml\Returnaddress
 * @author Synolia <contact@synolia.com>
 */
class Edit extends Action
{
    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ReturnAddressFactory
     */
    protected $returnAddressFactory;

    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * Edit constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param ReturnAddressFactory $returnAddressFactory
     * @param ReturnAddressRepository $returnAddressRepository
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        ReturnAddressFactory $returnAddressFactory,
        ReturnAddressRepository $returnAddressRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
        $this->returnAddressFactory = $returnAddressFactory;
        $this->returnAddressRepository = $returnAddressRepository;
    }

    /**
     * Init actions
     * @return \Magento\Backend\Model\View\Result\Page
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable
     */
    protected function _initAction()
    {
        // phpcs:enable
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    /**
     * Edit Return Address
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->returnAddressFactory->create();

        // 2. Initial checking
        if ($id) {
            try {
                $model = $this->returnAddressRepository->getById($id);
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage(__('This Return Address no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->coreRegistry->register('rma_return_address', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Return Address') : __('New Return Address'),
            $id ? __('Edit Return Address') : __('New Return Address')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Return Addresses'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New Return Address'));

        return $resultPage;
    }
}
