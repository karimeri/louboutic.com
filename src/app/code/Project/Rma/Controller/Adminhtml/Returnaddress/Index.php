<?php
namespace Project\Rma\Controller\Adminhtml\Returnaddress;

use Magento\Backend\App\Action;

/**
 * Class Index
 * @package Project\Rma\Controller\Adminhtml\Returnaddress
 * @author Synolia <contact@synolia.com>
 */
class Index extends Action
{
    /**
     * Index action
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
