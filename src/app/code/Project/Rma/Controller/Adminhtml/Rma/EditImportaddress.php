<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Model\ImporterAddressRepository;

/**
 * Class EditAddress
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class EditImportaddress  extends \Magento\Backend\App\Action
{
    /**
     * @var ImporterAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * EditAddress constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param Carrier $carrierHelper
     * @param LabelService $labelService
     * @param RmaDataMapper $rmaDataMapper
     * @param ImporterAddressRepository $rmaAddressRepository
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        ImporterAddressRepository $rmaAddressRepository,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Edit rma address form
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $addressId = $this->getRequest()->getParam('entity_id');
        try {
            $address = $this->rmaAddressRepository->getById($addressId);
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage(__('This address no longer exists.'));
            return $this->resultRedirectFactory->create()->setPath('adminhtml/*/');
        }
        $this->_coreRegistry->register('rma_importer_address', $address);
        $resultPage = $this->resultPageFactory->create();

        return $resultPage;
    }
}
