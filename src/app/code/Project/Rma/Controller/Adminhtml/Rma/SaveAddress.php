<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Rma\Controller\Adminhtml\Rma;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Model\RmaAddressRepository;

/**
 * Class SaveAddress
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class SaveAddress extends Rma
{
    /**
     * @var RmaAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * SaveNew constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param Carrier $carrierHelper
     * @param LabelService $labelService
     * @param RmaDataMapper $rmaDataMapper
     * @param RmaAddressRepository $rmaAddressRepository
     * @param RegionFactory $regionFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        RmaAddressRepository $rmaAddressRepository,
        RegionFactory $regionFactory
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $filesystem,
            $carrierHelper,
            $labelService,
            $rmaDataMapper
        );
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->regionFactory = $regionFactory;
    }

    /**
     * Save RMA address
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_redirect('adminhtml/*/');
            return;
        }

        try {
            $data = $this->getRequest()->getPostValue();
            if(isset($data['entity_id']) && !empty($data['entity_id'])){
                $rmaAddress = $this->rmaAddressRepository->getById($data['entity_id']);
            }else {
                $address = json_encode($data);
                $this->messageManager->addSuccess(__('You updated the RMA address.'));
                $this->_redirect('adminhtml/rma/new', ['order_id' => $data['parent_id'],'address'=>$address]);
                return;
            }
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the RMA address right now.'));
            $this->_redirect('adminhtml/*/');
            return;
        }

        $data = $this->updateRegionData($data);
        $rmaAddress->addData($data);

        try {
            $this->rmaAddressRepository->save($rmaAddress);

            $this->_eventManager->dispatch(
                'admin_rma_address_update',
                [
                    'rma_id' => $rmaAddress->getParentId()
                ]
            );

            //get Rma Object
            $model = $this->_objectManager->create(\Magento\Rma\Model\Rma::class);
            $rmaId = $this->getRequest()->getParam('parent_id');
            $rma =  $model->load($rmaId);
            if (!$rma) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The wrong RMA was requested.'));
            }
            $this->_coreRegistry->register('current_rma', $rma);
            $rma->setRmaAddressChangedFlag(true);
            $this->_eventManager->dispatch('adminhtml_rma_return_address_save_after',
                [
                    'rma' => $rma,
                    'params' => $this->getRequest()->getParams()
                ]);

            $this->messageManager->addSuccess(__('You updated the RMA address.'));
            $this->_redirect('adminhtml/*/edit', ['id' => $rmaAddress->getParentId()]);
            return;
        } catch (LocalizedException $exception) {
            $this->messageManager->addError($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the order address right now.'));
        }

        $this->_redirect('adminhtml/*/address', ['address_id' => $rmaAddress->getId()]);
    }

    /**
     * Update region data
     * @param array $attributeValues
     * @return array
     */
    protected function updateRegionData($attributeValues)
    {
        if (!empty($attributeValues['region_id'])) {
            $newRegion = $this->regionFactory->create()->load($attributeValues['region_id']);
            $attributeValues['region_code'] = $newRegion->getCode();
            $attributeValues['region'] = $newRegion->getDefaultName();
        }

        return $attributeValues;
    }
}
