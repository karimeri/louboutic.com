<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Registry;
use Magento\Rma\Controller\Adminhtml\Rma;
use Magento\Rma\Model\GridFactory;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Model\ReturnAddressRepository;

/**
 * Class SaveReturnAddress
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author  Synolia <contact@synolia.com>
 */
class SaveReturnAddress extends Rma
{
    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * SaveReturnAddress constructor.
     * @param Context                 $context
     * @param Registry                $coreRegistry
     * @param FileFactory             $fileFactory
     * @param Filesystem              $filesystem
     * @param Carrier                 $carrierHelper
     * @param LabelService            $labelService
     * @param RmaDataMapper           $rmaDataMapper
     * @param ReturnAddressRepository $returnAddressRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        ReturnAddressRepository $returnAddressRepository
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $filesystem,
            $carrierHelper,
            $labelService,
            $rmaDataMapper
        );
        $this->returnAddressRepository = $returnAddressRepository;
    }

    /**
     * Save RMA Return Address
     * @throws LocalizedException
     * @return void
     */
    public function execute()
    {
        try {
            $this->_initModel();

            $rma = $this->_coreRegistry->registry('current_rma');
            if (!$rma) {
                throw new LocalizedException(__('Invalid RMA'));
            }

            $returnAddressId = $this->getRequest()->getPost('rma_return_address_id');

            try {
                $returnAddress = $this->returnAddressRepository->getById($returnAddressId);
            } catch (NoSuchEntityException $exception) {
                throw new LocalizedException(__('Please enter a valid return address.'));
            }

            $rma->setRmaReturnAddressId($returnAddress->getId());
            $rma->save();

            $this->_view->loadLayout();
            $response = $this->_view->getLayout()->getBlock('return_address')->toHtml();
            $this->_eventManager->dispatch('adminhtml_rma_return_address_save_after',
                [
                    'rma' => $rma,
                    'params' => $this->getRequest()->getParams()
                ]);
        } catch (LocalizedException $exception) {
            $response = ['error' => true, 'message' => $exception->getMessage()];
        } catch (\Exception $exception) {
            $response = ['error' => true, 'message' => __('We cannot save RMA Return Address.')];
        }

        if (\is_array($response)) {
            $this->getResponse()->representJson(
                $this->_objectManager->get(Data::class)->jsonEncode($response)
            );
        } else {
            $this->getResponse()->setBody($response);
        }
    }
}
