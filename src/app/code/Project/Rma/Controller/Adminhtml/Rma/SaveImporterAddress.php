<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Model\ImporterAddressRepository;

/**
 * Class SaveImporterAddress
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class SaveImporterAddress extends  \Magento\Backend\App\Action
{
    /**
     * @var ImporterAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * SaveNew constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param Carrier $carrierHelper
     * @param LabelService $labelService
     * @param RmaDataMapper $rmaDataMapper
     * @param ImporterAddressRepository $rmaAddressRepository
     * @param RegionFactory $regionFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        ImporterAddressRepository $rmaAddressRepository
    ) {
        parent::__construct($context);
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Save RMA Importer address
     */
    public function execute()
    {

        if (!$this->getRequest()->isPost()) {
            $this->_redirect('adminhtml/*/');
            return;
        }

        try {
            $data = $this->getRequest()->getPostValue();
            $rmaAddress = $this->rmaAddressRepository->getById($data['entity_id']);
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the RMA Importer address right now.'));
            $this->_redirect('adminhtml/*/');
            return;
        }

        $rmaAddress->saveAddressData($rmaAddress,$data);
        try {
            $this->rmaAddressRepository->save($rmaAddress);
            $this->messageManager->addSuccess(__('You updated the RMA Importer address.'));
            $this->_redirect('adminhtml/*/edit', ['id' => $data['rma_id']]);
            return;
        } catch (LocalizedException $exception) {
            $this->messageManager->addError($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the RMA Importer address right now.'));
        }

        $this->_redirect('adminhtml/*/edit', ['id' => $data['rma_id']]);
    }

}
