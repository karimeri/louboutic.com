<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Rma\Controller\Adminhtml\Rma;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Model\RmaAddressRepository;
use Project\Rma\Model\RmaAddressFactory;

/**
 * Class EditAddress
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class EditAddress extends Rma
{
    /**
     * @var RmaAddressRepository
     */
    protected $rmaAddressRepository;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * EditAddress constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param Carrier $carrierHelper
     * @param LabelService $labelService
     * @param RmaDataMapper $rmaDataMapper
     * @param RmaAddressRepository $rmaAddressRepository
     * @param PageFactory $resultPageFactory
     * @param RmaAddressFactory $RmaAddressFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        RmaAddressRepository $rmaAddressRepository,
        PageFactory $resultPageFactory,
        RmaAddressFactory $RmaAddressFactory
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $filesystem,
            $carrierHelper,
            $labelService,
            $rmaDataMapper
        );
        $this->RmaAddressFactory = $RmaAddressFactory;
        $this->rmaAddressRepository = $rmaAddressRepository;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Edit rma address form
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $addressId = $this->getRequest()->getParam('address_id');
        $address = $this->getRequest()->getParam('address');
        try {
            if(!$addressId){
                $address = (array)json_decode($address);
                $address_object = $this->RmaAddressFactory->create();
                $address_object->setData($address);
                $address = $address_object;
            }else {
                $address = $this->rmaAddressRepository->getById($addressId);
            }
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage(__('This address no longer exists.'));
            return $this->resultRedirectFactory->create()->setPath('adminhtml/*/');
        }

        $this->_coreRegistry->register('rma_address', $address);
        $resultPage = $this->resultPageFactory->create();

        return $resultPage;
    }
}
