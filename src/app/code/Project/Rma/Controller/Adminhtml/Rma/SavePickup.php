<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Registry;
use Magento\Rma\Controller\Adminhtml\Rma;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Rma\Status\HistoryFactory;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Helper\Pickup as PickupHelper;

/**
 * Class SavePickup
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author  Synolia <contact@synolia.com>
 */
class SavePickup extends Rma
{
    /**
     * @var GridFactory
     */
    protected $rmaGridFactory;
    /**
     * @var PickupHelper
     */
    protected $pickupHelper;
    /**
     * @var \Magento\Rma\Model\Rma\Status\HistoryFactory
     */
    protected $rmaHistoryFactory;

    /**
     * SavePickup constructor.
     * @param Context                                      $context
     * @param Registry                                     $coreRegistry
     * @param FileFactory                                  $fileFactory
     * @param Filesystem                                   $filesystem
     * @param Carrier                                      $carrierHelper
     * @param LabelService                                 $labelService
     * @param RmaDataMapper                                $rmaDataMapper
     * @param PickupHelper                                 $pickupHelper
     * @param \Magento\Rma\Model\Rma\Status\HistoryFactory $rmaHistoryFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        PickupHelper $pickupHelper,
        HistoryFactory $rmaHistoryFactory
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $filesystem,
            $carrierHelper,
            $labelService,
            $rmaDataMapper
        );
        $this->pickupHelper = $pickupHelper;
        $this->rmaHistoryFactory = $rmaHistoryFactory;
    }

    /**
     * Save Pickup date
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        try {
            $this->_initModel();

            /** @var \Project\Rma\Model\Rma $rma */
            $rma = $this->_coreRegistry->registry('current_rma');
            if (!$rma) {
                throw new LocalizedException(__('Invalid RMA'));
            }

            $pickupDate = $this->getRequest()->getPost('pickup_date');
            $allowedDates = $this->pickupHelper->getAllowedDates($rma->getStoreId());

            if ($pickupDate !== PickupHelper::ACCESS_POINT && !\in_array($pickupDate, $allowedDates)) {
                throw new LocalizedException(__('This pickup date is not allowed'));
            }

            $pickupDate = $pickupDate !== PickupHelper::ACCESS_POINT ? $pickupDate : null;

            $rma->setPickupDate($pickupDate);
            $shippingMethod = $this->pickupHelper->getShippingMethod($rma);
            $code = ($shippingMethod) ? \strtolower($shippingMethod->getCode()) : '';

            if ($rma->getPickupConfirmationNumber()) {
                switch (true) {
                    case strpos($code, 'ups') !== false:
                        $response = $this->pickupHelper->cancelUps($rma);
                        break;
                    default:
                        return;
                }

                if (!empty($response)) {
                    throw new LocalizedException(__($response));
                }

                $rma->setPickupConfirmationNumber(null);
            }

            if ($pickupDate && $rma->getStatus() === \Magento\Rma\Model\Rma\Source\Status::STATE_AUTHORIZED) {
                switch (true) {
                    case strpos($code, 'ups') !== false:
                        $confirmationNumber = $this->pickupHelper->requestUps($rma);
                        break;
                    default:
                        return;
                }

                if ($confirmationNumber) {
                    $rma->setPickupConfirmationNumber($confirmationNumber);

                    /** @var \Magento\Rma\Model\Rma\Status\History $statusHistory */
                    $statusHistory = $this->rmaHistoryFactory->create();
                    $statusHistory->setRmaEntityId($rma->getEntityId());
                    $statusHistory->saveComment(
                        __('Pickup confirmed with number "%1"', $confirmationNumber),
                        false,
                        true
                    );

                }
            }

            $this->_view->loadLayout();
            $response = $this->_view->getLayout()->getBlock('pickup_date')->toHtml();
            $rma->save();
        } catch (LocalizedException $exception) {
            $response = ['error' => true, 'message' => $exception->getMessage()];
        } catch (\Exception $exception) {
            $response = ['error' => true, 'message' => __('We cannot save Pickup date.'.$exception->getMessage())];
        }

        if (\is_array($response)) {
            $this->getResponse()->representJson(
                $this->_objectManager->get(Data::class)->jsonEncode($response)
            );
        } else {
            $this->getResponse()->setBody($response);
        }
    }
}
