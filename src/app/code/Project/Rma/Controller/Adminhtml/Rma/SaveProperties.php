<?php
namespace Project\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Registry;
use Magento\Rma\Controller\Adminhtml\Rma;
use Magento\Rma\Model\GridFactory;
use Magento\Rma\Model\Rma\RmaDataMapper;
use Magento\Rma\Model\Shipping\LabelService;
use Magento\Shipping\Helper\Carrier;
use Project\Rma\Model\Config\Source\Category;
use Project\Rma\Model\Config\Source\Priority;
use Project\Rma\Model\Config\Source\StockCode;

/**
 * Class SaveProperties
 * @package Project\Rma\Controller\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class SaveProperties extends Rma
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Priority
     */
    protected $priority;

    /**
     * @var StockCode
     */
    protected $stockCode;

    /**
     * @var GridFactory
     */
    protected $rmaGridFactory;

    /**
     * @var EventManager
     */
    protected $eventManager;

    /**
     * SaveProperties constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     * @param Carrier $carrierHelper
     * @param LabelService $labelService
     * @param RmaDataMapper $rmaDataMapper
     * @param Category $category
     * @param Priority $priority
     * @param StockCode $stockCode
     * @param GridFactory $rmaGridFactory
     * @param EventManager $eventManager
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        Carrier $carrierHelper,
        LabelService $labelService,
        RmaDataMapper $rmaDataMapper,
        Category $category,
        Priority $priority,
        StockCode $stockCode,
        GridFactory $rmaGridFactory,
        EventManager $eventManager
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $filesystem,
            $carrierHelper,
            $labelService,
            $rmaDataMapper
        );
        $this->category = $category;
        $this->priority = $priority;
        $this->stockCode = $stockCode;
        $this->rmaGridFactory = $rmaGridFactory;
        $this->eventManager = $eventManager;
    }

    /**
     * Save RMA properties
     * @throws LocalizedException
     * @return void
     */
    public function execute()
    {
        try {
            $this->_initModel();

            $rma = $this->_coreRegistry->registry('current_rma');
            if (!$rma) {
                throw new LocalizedException(__('Invalid RMA'));
            }

            $category = $this->getRequest()->getPost('rma_category');
            $priority = $this->getRequest()->getPost('rma_priority');
            $stockCode = $this->getRequest()->getPost('rma_stock_code');

            if (!$this->category->checkValue($category)) {
                throw new LocalizedException(__('Please enter a valid category.'));
            }

            if (!$this->priority->checkValue($priority)) {
                throw new LocalizedException(__('Please enter a valid priority.'));
            }

            if (!$this->stockCode->checkValue($stockCode)) {
                throw new LocalizedException(__('Please enter a valid stock code.'));
            }

            $this->eventManager->dispatch('adminhtml_rma_properties_save_before',
                [
                    'rma' => $rma,
                    'params' => $this->getRequest()->getParams()
                ]);

            $rma->setCategory($category);
            $rma->setPriority($priority);
            $rma->setStockCode($stockCode);
            $rma->save();

            $gridModel = $this->rmaGridFactory->create();
            $gridModel->addData($rma->getData());
            $gridModel->save();

            $this->_view->loadLayout();
            $response = $this->_view->getLayout()->getBlock('rma_properties')->toHtml();

            $this->eventManager->dispatch('adminhtml_rma_properties_save_after',
                [
                    'rma' => $rma,
                    'params' => $this->getRequest()->getParams()
                ]);
        } catch (LocalizedException $exception) {
            $response = ['error' => true, 'message' => $exception->getMessage()];
        } catch (\Exception $exception) {
            $response = ['error' => true, 'message' => __('We cannot save RMA properties.')];
        }

        if (\is_array($response)) {
            $this->getResponse()->representJson(
                $this->_objectManager->get(Data::class)->jsonEncode($response)
            );
        } else {
            $this->getResponse()->setBody($response);
        }
    }
}
