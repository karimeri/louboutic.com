<?php
namespace Project\Rma\Controller\Rmaaddress;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Base64Json;
use Magento\Rma\Controller\Returns;
use Magento\Rma\Helper\Data;
use Magento\Sales\Model\OrderRepository;

/**
 * Class Create
 * @package Project\Rma\Controller\Rmaaddress
 * @author  Synolia <contact@synolia.com>
 */
class Create extends Returns
{
    const ADDRESS_PARAM = 'Wn43W5wy';

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var Data
     */
    protected $rmaHelper;
    /**
     * @var \Magento\Framework\Serialize\Serializer\Base64Json
     */
    protected $base64Json;

    /**
     * Create constructor.
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Framework\Registry                        $coreRegistry
     * @param \Magento\Sales\Model\OrderRepository               $orderRepository
     * @param \Magento\Rma\Helper\Data                           $rmaHelper
     * @param \Magento\Framework\Serialize\Serializer\Base64Json $base64Json
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        OrderRepository $orderRepository,
        Data $rmaHelper,
        Base64Json $base64Json
    ) {
        parent::__construct($context, $coreRegistry);
        $this->orderRepository = $orderRepository;
        $this->rmaHelper       = $rmaHelper;
        $this->base64Json = $base64Json;
    }

    /**
     * Try to load valid collection of ordered items
     * @param int $orderId
     * @return bool
     */
    protected function loadOrderItems($orderId)
    {
        if ($this->rmaHelper->canCreateRma($orderId)) {
            return true;
        }

        $incrementId = $this->_coreRegistry->registry('current_order')->getIncrementId();
        $message = __('We can\'t create a return transaction for order #%1.', $incrementId);
        $this->messageManager->addErrorMessage($message);

        return false;
    }

    /**
     * Customer create new rma customer address
     * @return void
     */
    public function execute()
    {
        $orderId = (int) $this->getRequest()->getParam('order_id');
        /** @var $order \Magento\Sales\Model\Order */
        $order = $this->orderRepository->get($orderId);

        if (empty($orderId)) {
            $this->_redirect('sales/order/history');
            return;
        }

        $this->_coreRegistry->register('current_order', $order);

        if (!$this->loadOrderItems($orderId)) {
            $this->_redirect('sales/order/history');
            return;
        }

        if (!$this->_canViewOrder($order)) {
            $this->_redirect('sales/order/history');
            return;
        }

        $post = $this->getRequest()->getPostValue();

        if ($post) {
            $this->_redirect(
                'rma/returns/create',
                ['_secure' => true, 'order_id' => $orderId, $this::ADDRESS_PARAM => $this->base64Json->serialize($post)]
            );

            return;
        }

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
