define([
    'jquery'
], function (
    $
) {
    'use strict';

    $.widget('project.returnAddressSelect', {
        options : {
            optionsConfig: {},
            selectSelector: 'select',
            renderSelector: 'address',
            rmaStockCode: '#rma_stock_code',
            idMessages: '#messages',
            divMessages: '<div id="messages">',
            rmaMessage: '.rma-message',
            rmaConfirmationSubmit: '#rma_confirmation_submit, #save, .action-save',
            pickUpDate: '.pickup-date-details input',
            pickUpDateChecked: '.pickup-date-details input:checked'
        },

        $errorDate: '<div class="message message-error error rma-message">Please choose a pickup date</div>',
        $errorCode: '<div class="message message-error error rma-message">Please choose a valid stock code</div>',

        $select: '',
        $render: '',

        /**
         * Constructor
         * @private
         */
        _create: function () {
            var $idMessages = $(this.options.idMessages),
                $divMessages = $(this.options.divMessages);

            this.$select = this.element.find(this.options.selectSelector);
            this.$render = this.element.find(this.options.renderSelector);

            if(!$idMessages.length) {
                $divMessages.insertBefore('.page-columns');
                $divMessages.append('<div class="messages">');
            }

            $('.title:contains("Pickup date")').append('<span class="afterPickupDate">*</span>');

            if(!$('p:contains("Not applicable")').length) {
                this._enableSubmit();
                $(this.options.rmaStockCode).on('change', _.bind(this._enableSubmit, this));
                $(this.options.pickUpDate).on('click', _.bind(this._enableSubmit, this));
            } else {
                this._enableSubmitWithoutPickupDate();
                $(this.options.rmaStockCode).on('change', _.bind(this._enableSubmitWithoutPickupDate, this));
            }


            _.bindAll(this, '_change');

            this._bindEvents();
        },


        _enableSubmitWithoutPickupDate: function () {
            var $rmaStockCode = $(this.options.rmaStockCode),
                $rmaConfirmationSubmit = $(this.options.rmaConfirmationSubmit),
                $messages = $('.messages'),
                $rmaMessage = $(this.options.rmaMessage);

            $rmaMessage.remove();

            if($rmaStockCode.val()) {
                $rmaConfirmationSubmit.attr('disabled', false);
            } else {
                $rmaConfirmationSubmit.attr('disabled', true);
                $messages.append(this.$errorCode);
            }
        },

        _enableSubmit: function () {
            var $rmaStockCode = $(this.options.rmaStockCode),
                $rmaConfirmationSubmit = $(this.options.rmaConfirmationSubmit),
                $pickUpDateChecked = $(this.options.pickUpDateChecked),
                $messages = $('.messages'),
                $rmaMessage = $(this.options.rmaMessage);

            $rmaMessage.remove();

            if($rmaStockCode.val() && $pickUpDateChecked.length) {
                $rmaConfirmationSubmit.attr('disabled', false);
            } else if($rmaStockCode.val() && !$pickUpDateChecked.length) {
                $rmaConfirmationSubmit.attr('disabled', true);
                $messages.append(this.$errorDate);
            } else if (!$rmaStockCode.val() && $pickUpDateChecked.length) {
                $rmaConfirmationSubmit.attr('disabled', true);
                $messages.append(this.$errorCode);
            } else {
                $rmaConfirmationSubmit.attr('disabled', true);
                $messages.append(this.$errorDate);
                $messages.append(this.$errorCode);
            }
        },

        /**
         * Bind events
         * @private
         */
        _bindEvents: function() {
            this.$select.on('change', this._change);
        },

        /**
         * On select change
         * @private
         */
        _change: function () {
            this.$render.html(this.options.optionsConfig[this.$select.val()]);
        }
    });

    return $.project.returnAddressSelect;
});
