<?php

namespace Project\ProductFile\Controller\Adminhtml\Delete;

use Magento\Framework\App\Action\Action;

class Index extends Action
{
    /**
     * @var \Project\ProductFile\Model\Filelink
     */
    private $filelink;
    /**
     * @var
     */
    private $filelinkFactory;

    /**
     * Index constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Project\ProductFile\Model\Filelink $filelink
     * @param \Project\ProductFile\Model\FilelinkFactory $filelinkFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Project\ProductFile\Model\Filelink $filelink,
        \Project\ProductFile\Model\FilelinkFactory $filelinkFactory
    )
    {
        $this->filelink = $filelink;
        $this->filelinkFactory = $filelinkFactory;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($filelinkId = $this->getRequest()->getParam('file_id')) {
            try {
                /** @var \Project\ProductFile\Model\Filelink $filelink */
                $filelink = $this->filelinkFactory->create();
                $filelink->load($filelinkId);
                $productId = $filelink['product_id'];
                $filelink->delete();
                $this->messageManager->addSuccess(__('The file has been deleted'));
                $resultRedirect->setPath('catalog/product/edit/*', ['id' => $productId, 'active_tab' => 'filelink']);

                return $resultRedirect;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $resultRedirect->setPath('catalog/product/edit/', ['id' => $productId, 'active_tab' => 'filelink']);

                return $resultRedirect;
            }
        }

        $this->messageManager->addError(__('No file to delete'));

        $resultRedirect->setPath('catalog/product/index');

        return $resultRedirect;
    }
}
