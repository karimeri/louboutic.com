<?php

namespace Project\ProductFile\Block\Product;

use Magento\Framework\View\Element\Template;

class FileLink extends Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Project\ProductFile\Model\Filelink
     */
    private $filelink;

    /**
     * FileLink constructor.
     * @param Template\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Project\ProductFile\Model\Filelink $filelink
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Project\ProductFile\Model\Filelink $filelink
    )
    {
        $this->coreRegistry = $coreRegistry;
        $this->storeManager = $storeManager;
        $this->filelink = $filelink;

        parent::__construct($context);
    }

    /**
     * @return bool|string
     */
    public function getFileLinks()
    {
        return $this->filelink->getProductFileLinks($this->getProduct()->getId());
    }

    /**
     * @return mixed|null
     */
    public function getProduct()
    {
        return $this->coreRegistry->registry('product');
    }

    /**
     * @param $filelink
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductFileUrl($filelink)
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB) . $this->filelink->getProductFileUrl($filelink);
    }
}
