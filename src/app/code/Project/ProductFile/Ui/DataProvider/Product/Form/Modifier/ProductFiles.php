<?php

namespace Project\ProductFile\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form\Fieldset;
use Project\ProductFile\Model\Filelink;

class ProductFiles extends AbstractModifier
{
    /** @var \Project\ProductFile\Model\Filelink */
    private $filelink;

    /** @var \Magento\Framework\Registry */
    private $registry;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    private $storeManager;

    public function __construct(
        Filelink $filelink,
        Registry $registry,
        StoreManagerInterface $storeManager
    ) {
        $this->filelink     = $filelink;
        $this->registry     = $registry;
        $this->storeManager = $storeManager;
    }

    public function modifyMeta(array $meta)
    {
        $fields = $this->getFileloadFields();

        $meta[ 'file_fieldset_name' ] = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label'         => __('Product Files'),
                        'sortOrder'     => 20,
                        'collapsible'   => true,
                        'componentType' => Fieldset::NAME,
                    ]
                ]
            ],
            'children'  => $fields
        ];

        return $meta;
    }

    protected function getFileloadFields()
    {
        $fields   = [];
        $_product = $this->registry->registry('current_product');

        $filelinks = $this->filelink->getProductFileLinks($_product->getId());

            if (isset($filelinks[ 0 ])) {
                $fields[ 'data.product.remove_filelink[0]' ] = [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement'   => 'checkbox',
                                'componentType' => 'field',
                                'description'   => $filelinks[ 0 ][ 'upload_file' ],
                                'dataScope'     => 'data.product.remove_filelink][' . $filelinks[ 0 ][ 'file_id' ],
                                'checked'       => true,
                                'value'         => true,
                                'visible'       => 1,
                                'required'      => 0,
                                'label'         => __('PDF File'),
                                'comment'       => '<a href="' . $this->storeManager->getStore()->getBaseUrl() . $this->filelink->getProductFileUrl($filelinks[ 0 ]) . '" target="_blank">' . $filelinks[ 0 ][ 'upload_file' ] . '</a>'
                            ]
                        ]
                    ]
                ];
            } else {
                $fields[ 'louboutin.filelink[0]' ] = [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement'   => 'file',
                                'dataScope'     => 'louboutin.filelink[]',
                                'componentType' => 'field',
                                'visible'       => 1,
                                'required'      => 0,
                                'label'         => __('PDF File')
                            ]
                        ]
                    ]
                ];
            }

        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }
}
