<?php

namespace Project\ProductFile\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Filelink extends AbstractModel implements IdentityInterface
{
    /**
     * cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'louboutin_product_file';

    /**
     * @var string
     */
    private $uploadFolder = 'louboutin/productfile';

    /**
     * Filelink constructor.
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @param $filelink
     * @return string
     */
    public function getProductFileUrl($filelink)
    {
        return 'pub/media/' . $this->uploadFolder . $filelink['file_url'];
    }

    /**
     * @param $urlKey
     * @param $storeId
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function checkUrlKey($urlKey, $storeId)
    {
        return $this->_getResource()->checkUrlKey($urlKey, $storeId);
    }

    /**
     * Get FilesLinks
     *
     * @param $fileId
     *
     * @return bool|string
     */
    public function getProductFileLinks($fileId)
    {
        return $this->getResource()->getProductFileLinks($fileId);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Project\ProductFile\Model\Resource\Filelink');
    }
}
