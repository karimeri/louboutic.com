<?php

namespace Project\ProductFile\Model\Resource;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Filelink extends AbstractDb
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $dateTime;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime       $date
     * @param \Magento\Framework\Stdlib\DateTime                $dateTime
     * @param string|null                                       $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        $resourcePrefix = null)
    {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    /**
     * @param AbstractModel $object
     * @param mixed $value
     * @param null $field
     * @return Filelink
     */
    public function load(AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'product_id';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * @param $id
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProductFileLinks($id)
    {
        $adapter = $this->getConnection();
        $select = $adapter->select()->from($this->getMainTable())->where('product_id = :product_id');
        $binds = ['product_id' => (int) $id];

        return $adapter->fetchAll($select, $binds);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('louboutin_product_file', 'file_id');
    }

    /**
     * @param AbstractModel $object
     * @return Filelink
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        return parent::_beforeSave($object);
    }

    /**
     * @param AbstractModel $object
     * @return Filelink
     */
    protected function _beforeDelete(AbstractModel $object)
    {
        $condition = ['file_id = ?' => (int) $object->getId()];
        $this->getConnection()->delete($this->getTable('louboutin_product_file'), $condition);

        return parent::_beforeDelete($object);
    }
}
