<?php

namespace Project\ProductFile\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Upload
{

    /**
     * uploader factory
     *
     * @var \Magento\Framework\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var string
     */
    private $uploadPath;

    /**
     * @var string
     */
    private $uploadFolder = 'louboutin/productfile/';

    /**
     * Allowed file extensions to upload
     *
     * @var array
     */
    protected $allowedExtensions = ['pdf'];

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * constructor
     *
     * @param UploaderFactory      $uploaderFactory
     * @param Filesystem           $fileSystem
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        UploaderFactory $uploaderFactory,
        Filesystem $fileSystem,
        ScopeConfigInterface $scopeConfig)
    {
        $this->uploaderFactory = $uploaderFactory;
        $this->uploadPath = $fileSystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath($this->uploadFolder);
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param $file
     * @return array|bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function uploadFile($file)
    {
        try {
            $upload = $this->uploaderFactory->create(['fileId' => $file]);
            $upload->setAllowedExtensions($this->allowedExtensions);
            $upload->setAllowRenameFiles(false);
            $upload->setFilesDispersion(true);
            $upload->setAllowCreateFolders(true);

            return $upload->save($this->uploadPath);
        } catch (\Exception $e) {
            if (!$upload->checkAllowedExtension($upload->getFileExtension()) ||
                $e->getCode() != Uploader::TMP_NAME_EMPTY) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    new \Magento\Framework\Phrase('Invalid Pdf file type.')
                );
            }elseif (!$upload->save($this->uploadPath)){
                throw new \Magento\Framework\Exception\LocalizedException(
                    new \Magento\Framework\Phrase('File can not be saved.')
                );
            }
        }

        return false;
    }
}
