<?php

namespace Project\ProductFile\Observer\Product;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Registry;
use Project\ProductFile\Model\Upload;
use Project\ProductFile\Model\Filelink;
use Project\ProductFile\Model\FilelinkFactory;

class Save implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var Project\ProductFile\Model\Upload
     */
    private $upload;

    /**
     * @var Magento\Backend\App\Action\Context
     */
    private $context;

    /**
     * @var
     */
    private $file;

    /** @var \Project\ProductFile\Model\FilelinkFactory */
    private $filelinkFactory;

    /**
     * Save constructor.
     * @param Registry $coreRegistry
     * @param Upload $upload
     * @param Context $context
     * @param Filelink $file
     * @param FilelinkFactory $filelinkFactory
     */
    public function __construct(
        Registry $coreRegistry,
        Upload $upload,
        Context $context,
        Filelink $file,
        FilelinkFactory $filelinkFactory
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->upload       = $upload;
        $this->context      = $context;
        $this->file = $file;
        $this->filelinkFactory = $filelinkFactory;
    }

    /**
     * @param EventObserver $observer
     * @return $this|void
     */
    public function execute(EventObserver $observer)
    {
        $filelinks = $this->context->getRequest()->getFiles('filelink', -1);
        $post = $observer->getDataObject();

        $product = $this->coreRegistry->registry('product');

        $this->deleteFilesLinks($post, $product);

        if ($filelinks != '-1') {
            $this->addfileslinks($filelinks, $product);
        }

        return $this;
    }

    private function addfileslinks($files, $product)
    {
        $productId = $product->getId();

        foreach ($files as $file) {

            if ($file[ 'tmp_name' ] === "") {
                continue;
            }

            $uploadedFile = $this->upload->uploadFile($file);

            if ($uploadedFile) {
                $file = $this->file;
                $file->setProductId($productId);
                $file->setFileUrl($uploadedFile[ 'file' ]);
                $file->setUploadFile($uploadedFile[ 'name' ]);
                $file->setFileType($uploadedFile[ 'type' ]);
                $file->save();
            }
        }
    }

    private function deleteFilesLinks($post, $product)
    {
        if (isset($post['remove_filelink'])) {
            foreach ($post['remove_filelink'] as $key => $value) {

                if($value === "0") {
                    /** @var \Project\ProductFile\Model\Filelink $filelink */
                    $file = $this->filelinkFactory->create();
                    $file->load($key);
                    $file->delete();
                }
            }
        }
    }
}
