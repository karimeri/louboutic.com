<?php

namespace Project\ProductFile\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    const NAME_TABLE = 'louboutin_product_file';
    /**
     * Installs DB schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create table 'louboutin_product_file'
         */
        $setup->startSetup();
        $connection = $setup->getConnection();
        if (!$connection->isTableExists($setup->getTable(self::NAME_TABLE))) {
            $table = $connection->newTable(
                $setup->getTable(self::NAME_TABLE)
            )->addColumn(
                'file_id',
                Table::TYPE_INTEGER,
                null,
                 [
                     'identity' => true,
                     'unsigned' => true,
                     'nullable' => false,
                     'primary' => true
                 ],
                'File ID'
            )->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => '0'
                ],
                'Product ID'
            )->addColumn(
                'file_url',
                Table::TYPE_TEXT,
                255,
                [],
                'File Url'
            )->addColumn(
                'upload_file',
                Table::TYPE_TEXT,
                255,
                [],
                'Upload File'
            )->addColumn(
                'file_type',
                Table::TYPE_TEXT,
                20,
                [],
                'File Type'
            )->addIndex(
                $setup->getIdxName(self::NAME_TABLE, ['product_id']),
                ['product_id']
            )->setComment(
                'Product File table'
            );
            $connection->createTable($table);
        }
        $setup->endSetup();
    }
}
