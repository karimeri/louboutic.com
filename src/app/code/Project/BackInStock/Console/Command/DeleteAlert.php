<?php

namespace Project\BackInStock\Console\Command;

use Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory as StockCollectionFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Console\Cli;

/**
 * Class DeleteAlerte
 * @package Project\BackInStock\Console\Command
 */
class DeleteAlert extends Command
{
    /**
     * @var StockCollectionFactory
     */
    private $stockCollectionFactory;

    /**
     * DeleteAlerte constructor.
     * @param StockCollectionFactory $stockCollectionFactory
     */
    public function __construct(
        StockCollectionFactory $stockCollectionFactory
    ) {
        $this->stockCollectionFactory = $stockCollectionFactory;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:delete:alert')
            ->setDescription('Remove old alert');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $collection = $this->stockCollectionFactory->create();
            $collection->addFieldToFilter('status', ['eq' => 1]);
            $collection->walk('delete');
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');
            return Cli::RETURN_FAILURE;
        }
        return Cli::RETURN_SUCCESS;
    }
}
