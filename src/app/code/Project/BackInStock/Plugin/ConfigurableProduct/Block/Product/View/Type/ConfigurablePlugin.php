<?php

namespace Project\BackInStock\Plugin\ConfigurableProduct\Block\Product\View\Type;

use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Json\DecoderInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Helper\Product as ProductHelper;
use Magento\CatalogInventory\Api\StockRegistryInterface as StockRegistry;
use Magento\ProductAlert\Helper\Data as AlertStockHelper;
use Synolia\AdvancedStock\Plugin\ConfigurableProduct\Block\Product\View\Type\ConfigurablePlugin as AdvancedStockPlugin;
use Project\Catalog\Helper\Label as CatalogLabelHelper;

/**
 * Class ConfigurablePlugin
 * @package Project\BackInStock\Plugin\ConfigurableProduct\Block\Product\View\Type
 * @author Synolia <contact@synolia.com>
 */
class ConfigurablePlugin extends AdvancedStockPlugin
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;

    /**
     * ConfigurablePlugin constructor.
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Json\DecoderInterface $jsonDecoder
     * @param \Magento\ProductAlert\Helper\Data $alertStockHelper
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     */
    public function __construct(
        ProductHelper $productHelper,
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder,
        AlertStockHelper $alertStockHelper,
        StockRegistry $stockRegistry,
        ScopeConfigInterface $scopeConfig,
        JsonSerializer $jsonSerializer
    ) {
        parent::__construct(
            $productHelper,
            $jsonEncoder,
            $jsonDecoder,
            $alertStockHelper,
            $stockRegistry,
            $scopeConfig
        );

        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @param $subject
     * @return array
     */
    public function getOptionStocks($subject)
    {
        $stocks = parent::getOptionStocks($subject);

        foreach ($subject->getAllowProducts() as $product) {
            if (array_key_exists($product->getId(), $stocks)) {
                $backInStock = $product->getData(CatalogLabelHelper::PRODUCT_ATTRIBUTE_BACKINSTOCK) ?: 0;
                $stocks[$product->getId()]['is_back_in_stock'] = $backInStock;
            }
        }

        return $stocks;
    }

    /*
     * Reorder options by label
     */
    public function aroundGetJsonConfig($subject, \Closure $proceed)
    {
        $jsonConfig = parent::aroundGetJsonConfig($subject, $proceed);
        $config = $this->jsonDecoder->decode($jsonConfig);
        if(isset($config['attributes']) && \is_array($config['attributes']) && !empty($config['attributes'])) {
            foreach ($config['attributes'] as $atttribute) {
                if($atttribute['code'] != 'size_bracelets') {
                    $config['attributes'] = $this->orderOptionsByLabel($config['attributes']);
                }
            }
        }
        return $this->jsonEncoder->encode($config);
    }

    /**
     * @param array $attributes
     * @return array
     */
    protected function orderOptionsByLabel($attributes)
    {
        foreach ($attributes as &$attributeOptionsData) {
            usort($attributeOptionsData['options'], function ($attributeOption1, $attributeOption2) {
                if ($attributeOption1 == $attributeOption2) {
                    return 0;
                }
                return $attributeOption1['label'] < $attributeOption2['label'] ? -1 : 1;
            });
        }

        return $attributes;
    }
}
