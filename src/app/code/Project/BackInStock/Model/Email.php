<?php

namespace Project\BackInStock\Model;

use Magento\ProductAlert\Model\Email as BaseEmail;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Area;
use Magento\Catalog\Model\Product;

/**
 * Class Email
 * @package Project\BackInStock\Model
 */
class Email extends BaseEmail
{
    /***
     * @var string
     */
    protected $_storeId;

    protected $_alertStockId;

    /**
     * URL Model instance
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;

    /**
     * Current Store scope object
     *
     * @var \Magento\Store\Model\Store
     */
    protected $_store;

    /**
     * Email constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\ProductAlert\Helper\Data $productAlertData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Helper\View $customerHelper
     * @param \Magento\Store\Model\App\Emulation $appEmulation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\ProductAlert\Helper\Data $productAlertData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Helper\View $customerHelper,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_productAlertData = $productAlertData;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->_appEmulation = $appEmulation;
        $this->_transportBuilder = $transportBuilder;
        $this->_customerHelper = $customerHelper;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context, $registry, $productAlertData, $scopeConfig, $storeManager, $customerRepository,
            $customerHelper, $appEmulation, $transportBuilder, $resource, $resourceCollection, $data);
    }

    /**
     * @param $storeId
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = $storeId;
        return $this;
    }

    /**
     * @param $alertStockId
     * @return $this
     */
    public function setAlertStockId($alertStockId)
    {
        $this->_alertStockId = $alertStockId;
        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Magento\Framework\Exception\MailException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function send()
    {
        if ($this->_website === null || $this->_customer === null) {
            return false;
        }
        if ($this->_type == 'price' &&
            count($this->_priceProducts) == 0 ||
            $this->_type == 'stock' && count($this->_stockProducts) == 0
        ) {
            return false;
        }
        if (!$this->_website->getDefaultGroup() || !$this->_website->getDefaultGroup()->getDefaultStore()) {
            return false;
        }

        if ($this->_storeId > 0) {
            $store = $this->_storeManager->getStore($this->_storeId);
        } else {
            $store = $this->_website->getDefaultStore();
        }
        $storeId = $store->getId();

        if ($this->_type == 'price'
            && !$this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_PRICE_TEMPLATE,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            return false;
        } elseif ($this->_type == 'stock'
            && !$this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_STOCK_TEMPLATE,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            return false;
        }

        if ($this->_type != 'price' && $this->_type != 'stock') {
            return false;
        }

        $this->_appEmulation->startEnvironmentEmulation($storeId);

        if ($this->_type == 'price') {
            $this->_getPriceBlock()->setStore($store)->reset();
            foreach ($this->_priceProducts as $product) {
                $product->setCustomerGroupId($this->_customer->getGroupId());
                $this->_getPriceBlock()->addProduct($product);
            }
            $block = $this->_getPriceBlock();
            $templateId = $this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_PRICE_TEMPLATE,
                ScopeInterface::SCOPE_STORE,
                $storeId
            );
        } else {
            $this->_getStockBlock()->setStore($store)->reset();
            foreach ($this->_stockProducts as $key => $product) {
                $product->setCustomerGroupId($this->_customer->getGroupId());
                $this->_getStockBlock()->addProductWithAlertId($product, $key);
            }
            $block = $this->_getStockBlock();
            $templateId = $this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_STOCK_TEMPLATE,
                ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }

        $alertGrid = $this->_appState->emulateAreaCode(
            Area::AREA_FRONTEND,
            [$block, 'toHtml']
        );
        $this->_appEmulation->stopEnvironmentEmulation();

        $transport = $this->_transportBuilder->setTemplateIdentifier(
            $templateId
        )->setTemplateOptions(
            ['area' => Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            [
                'customerName' => $this->_customerHelper->getCustomerName($this->_customer),
                'alertGrid' => $alertGrid,
                'unsubscribeUrl' => $this->getUnsubscribeUrl($this->_alertStockId),
            ]
        )->setFrom(
            $this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_IDENTITY,
                ScopeInterface::SCOPE_STORE,
                $storeId
            )
        )->addTo(
            $this->_customer->getEmail(),
            $this->_customerHelper->getCustomerName($this->_customer)
        )->getTransport();

        $transport->sendMessage();

        return true;
    }

    /**
     * @param $to
     * @return bool
     * @throws \Exception
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendGuest($to)
    {
        if ($this->_website === null) {
            return false;
        }
        if (count($this->_stockProducts) == 0) {
            return false;
        }
        if (!$this->_website->getDefaultGroup() || !$this->_website->getDefaultGroup()->getDefaultStore()) {
            return false;
        }

        if ($this->_storeId > 0) {
            $store = $this->_storeManager->getStore($this->_storeId);
        } else {
            $store = $this->_website->getDefaultStore();
        }
        $storeId = $store->getId();

        if ($this->_type == 'stock'
            && !$this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_STOCK_TEMPLATE,
                ScopeInterface::SCOPE_STORE,
                $storeId
            )
        ) {
            return false;
        }

        if ($this->_type != 'stock') {
            return false;
        }

        $this->_appEmulation->startEnvironmentEmulation($storeId);

        $this->_getStockBlock()->setStore($store)->reset();
        foreach ($this->_stockProducts as $key => $product) {
            $this->_getStockBlock()->addProductWithAlertId($product, $key);
        }
        $block = $this->_getStockBlock();
        $templateId = $this->_scopeConfig->getValue(
            self::XML_PATH_EMAIL_STOCK_TEMPLATE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $alertGrid = $this->_appState->emulateAreaCode(
            Area::AREA_FRONTEND,
            [$block, 'toHtml']
        );
        $this->_appEmulation->stopEnvironmentEmulation();

        $transport = $this->_transportBuilder->setTemplateIdentifier(
            $templateId
        )->setTemplateOptions(
            ['area' => Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            [
                'customerName' => 'Bonjour',
                'alertGrid' => $alertGrid,
                'unsubscribeUrl' => $this->getUnsubscribeUrl($this->_alertStockId),
            ]
        )->setFrom(
            $this->_scopeConfig->getValue(
                self::XML_PATH_EMAIL_IDENTITY,
                ScopeInterface::SCOPE_STORE,
                $storeId
            )
        )->addTo(
            $to
        )->getTransport();

        $transport->sendMessage();

        return true;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param int $alertId
     * @return $this
     */
    public function addStockProductWithAlertId(Product $product, $alertId)
    {
        $this->_stockProducts[$alertId] = $product;
        return $this;
    }

    /**
     * @param $alertStockId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getUnsubscribeUrl($alertStockId)
    {
        $params = $this->_getUrlParams();
        $params['alert'] = $alertStockId;
        return $this->_urlInterface->getUrl('project_backInStock/unsubscribe/stockAll', $params);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _getUrlParams()
    {
        return ['_scope' => $this->getStore(), '_scope_to_url' => true];
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface|\Magento\Store\Model\Store
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStore()
    {
        if (isset($this->_storeId) && $this->_storeId > 0) {
            $this->_store = $this->_storeManager->getStore($this->_storeId);
        } else {
            $this->_store = $this->_website->getDefaultStore();
        }

        return $this->_store;
    }
}
