<?php

namespace Project\BackInStock\Model;

use Magento\ProductAlert\Model\Observer as BaseObserver;
use Magento\Catalog\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\ProductAlert\Model\ResourceModel\Price\CollectionFactory as PriceCollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory as StockCollectionFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\ProductAlert\Model\EmailFactory;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\ProductAlert\Model\Email;
use Magento\Store\Model\Website;

/**
 * Class Observer
 * @package Project\BackInStock\Model
 */
class Observer extends BaseObserver
{
    /**
     * @var RegisterRepository
     */
    private $registerRepository;

    /**
     * @var Website
     */
    private $websiteModel;

    /**
     * Observer constructor.
     * @param Data $catalogData
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param PriceCollectionFactory $priceColFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param ProductRepositoryInterface $productRepository
     * @param DateTimeFactory $dateFactory
     * @param StockCollectionFactory $stockColFactory
     * @param TransportBuilder $transportBuilder
     * @param EmailFactory $emailFactory
     * @param StateInterface $inlineTranslation
     * @param RegisterRepository $registerRepository
     * @param Website $websiteModel
     */
    public function __construct(
        Data $catalogData,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        PriceCollectionFactory $priceColFactory,
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepository,
        DateTimeFactory $dateFactory,
        StockCollectionFactory $stockColFactory,
        TransportBuilder $transportBuilder,
        EmailFactory $emailFactory,
        StateInterface $inlineTranslation,
        RegisterRepository $registerRepository,
        Website $websiteModel
    ) {
        $this->registerRepository = $registerRepository;
        $this->websiteModel = $websiteModel;

        parent::__construct(
            $catalogData,
            $scopeConfig,
            $storeManager,
            $priceColFactory,
            $customerRepository,
            $productRepository,
            $dateFactory,
            $stockColFactory,
            $transportBuilder,
            $emailFactory,
            $inlineTranslation
        );
    }

    /**
     * @param Email $email
     * @param null $productId
     * @return $this|Observer
     * @throws \Exception
     */
    protected function _processStock(\Magento\ProductAlert\Model\Email $email, $productId = null)
    {
        $email->setType('stock');

        foreach ($this->_getWebsites() as $website) {
            /* @var $website \Magento\Store\Model\Website */

            if (!$website->getDefaultGroup() || !$website->getDefaultGroup()->getDefaultStore()) {
                continue;
            }
            if (!$this->_scopeConfig->getValue(
                self::XML_PATH_STOCK_ALLOW,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $website->getDefaultGroup()->getDefaultStore()->getId()
            )
            ) {
                continue;
            }
            try {
                $collection = $this->_stockColFactory->create()->addWebsiteFilter(
                    $website->getId()
                )->addStatusFilter(
                    0
                )->addFieldToFilter(
                    'customer_id',
                    ['notnull' => true]
                );

                if (!is_null($productId)) {
                    $collection->addFieldToFilter(
                        'product_id',
                        ['eq' => $productId]
                    );
                }

                $collection->setCustomerOrder();

            } catch (\Exception $e) {
                $this->_errors[] = $e->getMessage();
                throw $e;
            }

            $previousCustomer = null;
            $email->setWebsite($website);

            if ($collection) {
                foreach ($collection as $alert) {
                    try {
                        if (!$previousCustomer || $previousCustomer->getId() != $alert->getCustomerId()) {
                            $customer = $this->customerRepository->getById($alert->getCustomerId());
                            if ($previousCustomer) {
                                $email->send();
                            }
                            if (!$customer) {
                                continue;
                            }
                            $previousCustomer = $customer;
                            $email->clean();
                            $email->setCustomerData($customer);
                            $email->setStoreId($alert->getStoreId());
                            $email->setAlertStockId($alert->getAlertStockId());
                        } else {
                            $customer = $previousCustomer;
                        }
                        $product = $this->productRepository->getById(
                            $alert->getProductId(),
                            false,
                            $website->getDefaultStore()->getId()
                        );

                        $product->setCustomerGroupId($customer->getGroupId());
                        if ($product->isSalable()) {
                            $email->addStockProductWithAlertId($product, $alert->getAlertStockId());

                            $alert->setSendDate($this->_dateFactory->create()->gmtDate());
                            $alert->setSendCount($alert->getSendCount() + 1);
                            $alert->setStatus(1);
                            $alert->save();
                        }
                    } catch (\Exception $e) {
                        $this->_errors[] = $e->getMessage();
                        throw $e;
                    }
                }

                if ($previousCustomer) {
                    try {
                        $email->send();
                    } catch (\Exception $e) {
                        $this->_errors[] = $e->getMessage();
                        throw $e;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param Email $email
     * @param null $productId
     * @return $this
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _processStockGuest(Email $email, $productId = null)
    {
        $email->setType('stock');

        if (is_null($productId))
            $condition = ['status' => 0, 'customer_id' => null];
        else {
            $condition = ['status' => 0, 'customer_id' => null, 'product_id' => $productId];
        }
        $collection =  $this->registerRepository->findBy($condition);

        $mailsToSend = [];
        if ($collection) {
            try {
                foreach ($collection as $key => $alert) {
                    $website = $this->websiteModel->load($alert->getWebsiteId());
                    $product = $this->productRepository->getById(
                        $alert->getProductId(),
                        false,
                        $website->getDefaultStore()->getId()
                    );

                    if ($product->isSalable()) {
                        $mailsToSend[$alert->getEmail()][$key]['website'] = $website;
                        $mailsToSend[$alert->getEmail()][$key]['alertId'] = $alert->getAlertStockId();
                        $mailsToSend[$alert->getEmail()][$key]['product'] = $product;
                        $mailsToSend[$alert->getEmail()][$key]['storeId'] = $alert->getStoreId();

                        $alert->setSendDate($this->_dateFactory->create()->gmtDate());
                        $alert->setStatus(1);
                        $alert->save();
                    }
                }

                foreach ($mailsToSend as $mail => $products) {
                    foreach ($products as $product) {
                        $email->setWebsite($product['website']);
                        $email->setStoreId($product['storeId']);
                        $email->setAlertStockId($product['alertId']);
                        $email->addStockProductWithAlertId($product['product'], $product['alertId']);
                    }
                    $email->sendGuest($mail);
                    $email->clean();
                }
            } catch (\Exception $exception) {
                $this->_errors[] = $exception->getMessage();
                throw $exception;
            }
        }

        return $this;
    }

    /**
     * @param null $productId
     * @return $this|Observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function process($productId = null)
    {
        /* @var $email \Magento\ProductAlert\Model\Email */
        $email = $this->_emailFactory->create();
        $this->_processPrice($email);
        $this->_processStock($email, $productId);
        $this->_processStockGuest($email, $productId);
        $this->_sendErrorEmail();

        return $this;
    }
}
