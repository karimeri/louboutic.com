<?php

namespace Project\BackInStock\Model\ResourceModel\Stock;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class Collection
 * @package Project\BackInStock\Model\ResourceModel\Stock
 */
class Collection extends \Magento\ProductAlert\Model\ResourceModel\Stock\Collection
{
    /**
     * Whether product name attribute value table is joined in select
     *
     * @var boolean
     */
    protected $_isProductNameJoined = true;

    /**
     * Add days in stock alert filter of product collection
     *
     * @var boolean
     */
    protected $_addDaysInStockAlert = true;

    /**
     * Product Ids array
     *
     * @var array
     */
    protected $_productIds = [];

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\ConfigFactory
     */
    protected $_catalogConfFactory;

    /**
     * @var \Magento\Catalog\Model\Entity\AttributeFactory
     */
    protected $_catalogAttrFactory;

    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    protected $metadataPool;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Catalog\Model\ResourceModel\ConfigFactory $catalogConfFactory
     * @param \Magento\Catalog\Model\Entity\AttributeFactory $catalogAttrFactory
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ResourceModel\ConfigFactory $catalogConfFactory,
        \Magento\Catalog\Model\Entity\AttributeFactory $catalogAttrFactory,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_date = $date;
        $this->_storeManager = $storeManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_stockRegistry = $stockRegistry;
        $this->_catalogConfFactory = $catalogConfFactory;
        $this->_catalogAttrFactory = $catalogAttrFactory;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * Get metadata pool object
     *
     * @return \Magento\Framework\EntityManager\MetadataPool
     * @since 100.1.0
     */
    protected function getMetadataPool()
    {
        if ($this->metadataPool == null) {
            $this->metadataPool = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Framework\EntityManager\MetadataPool::class);
        }
        return $this->metadataPool;
    }

    /**
     * After load processing
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();

        /**
         * Assign products
         */
        $this->_joinProductNameTable();
        $this->_assignProducts();
        $this->resetItemsDataChanged();

        $this->getPageSize();

        return $this;
    }

    /**
     * Add products to items and item options
     *
     * @return $this
     */
    protected function _assignProducts()
    {
        \Magento\Framework\Profiler::start(
            'STOCK_ALERT:' . __METHOD__,
            ['group' => 'STOCK_ALERT', 'method' => __METHOD__]
        );

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->_productCollectionFactory->create();

        $this->_productIds = $this->getColumnValues('product_id');

        $productCollection
            ->addIdFilter($this->_productIds)
            ->addAttributeToSelect(['name', 'size'])
            ->addOptionsToResult()
            ->addUrlRewrite();

        $this->_eventManager->dispatch(
            'stock_alert_collection_products_after_load',
            ['product_collection' => $productCollection]
        );

        foreach ($this as $item) {
            $product = $productCollection->getItemById($item->getProductId());
            $stockItem = $this->_stockRegistry->getStockItem($item->getProductId());
            if ($product) {
                $product->setCustomOptions([]);
                $item->setProduct($product);
                $item->setProductName($product->getName());
                $item->setName($product->getName());
                $item->setPrice($product->getPrice());
                $item->setIsInStock($stockItem->getIsInStock());
            } else {
                $this->removeItemByKey($item->getId());
            }
        }

        \Magento\Framework\Profiler::stop('STOCK_ALERT:' . __METHOD__);

        return $this;
    }

    /**
     * Joins product name attribute value to use it in WHERE and ORDER clauses
     *
     * @return $this
     */
    public function _joinProductNameTable()
    {
        if (!$this->_isProductNameJoined) {
            $entityTypeId = $this->_catalogConfFactory->create()->getEntityTypeId();
            /** @var \Magento\Catalog\Model\Entity\Attribute $attribute */
            $attribute = $this->_catalogAttrFactory->create()->loadByCode($entityTypeId, 'name');

            $storeId = $this->_storeManager->getStore(\Magento\Store\Model\Store::ADMIN_CODE)->getId();

            $entityMetadata = $this->getMetadataPool()->getMetadata(ProductInterface::class);

            $this->getSelect()->join(
                ['product_name_table' => $attribute->getBackendTable()],
                'product_name_table.' . $entityMetadata->getLinkField() . ' = main_table.product_id' .
                ' AND product_name_table.store_id = ' .
                $storeId .
                ' AND product_name_table.attribute_id = ' .
                $attribute->getId(),
                []
            );

            $this->_isProductNameJoined = true;
        }
        return $this;
    }

    /**
     * After load data
     *
     * @return $this
     */
    protected function _afterLoadData()
    {
        parent::_afterLoadData();

        if ($this->_addDaysInStockAlert) {
            $gmtOffset = (int)$this->_date->getGmtOffset();
            $nowTimestamp = $this->_date->timestamp();

            foreach ($this as $stockAlert) {
                $stockAlertTimestamp = $this->_date->timestamp($stockAlert->getAddDate());

                $stockAlert->setDaysInStockAlert(
                    (int)(($nowTimestamp - $gmtOffset - $stockAlertTimestamp) / 24 / 60 / 60)
                );
            }
        }

        return $this;
    }
}
