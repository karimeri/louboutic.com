<?php

namespace Project\BackInStock\Model;

use Magento\ProductAlert\Model\Stock;
use Magento\ProductAlert\Model\StockFactory;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Synolia\Standard\Model\Repository\AbstractRepository;

/**
 * Class RegisterRepository
 *
 * @package Project\BackInStock\Model
 */
class RegisterRepository extends AbstractRepository
{
    const TABLE_NAME = 'product_alert_stock';

    /**
     * @var StockFactory
     */
    protected $stockFactory;

    /**
     * RegisterRepository constructor.
     *
     * @param StockFactory $stockFactory
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(
        StockFactory $stockFactory,
        Context $context,
        $connectionName = null
    ) {
        $this->stockFactory = $stockFactory;

        parent::__construct($context, $connectionName);
    }

    /**
     * @return string
     */
    protected function getIdFieldName()
    {
        return 'alert_stock_id';
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return self::TABLE_NAME;
    }


    /**
     * @param Stock $stock
     * @return Stock
     */
    public function save(Stock $stock)
    {
        $this->persistToDb($stock);

        return $stock;
    }

    /**
     * @param $params
     * @return array
     */
    public function findBy($params)
    {
        $items = parent::findBy($params);

        return $this->castResults($items);
    }

    /**
     * @param array $params
     * @return array|Stock
     * @throws NoSuchEntityException
     * @throws \Synolia\Standard\Exception\NestedConnectionException
     */
    public function findOneBy($params)
    {
        $item = parent::findOneBy($params);

        if (! $item) {
            $value = reset($params);
            throw NoSuchEntityException::singleField(key($params), $value);
        }

        $object = $this->stockFactory->create();
        $object->setData($item);

        return $object;
    }

    /**
     * @param $items
     * @return array
     */
    private function castResults($items)
    {
        $results = [];

        foreach ($items as $item) {
            $object = $this->stockFactory->create();
            $object->setData($item);

            $results[] = $object;
        }

        return $results;
    }
}
