<?php

namespace Project\BackInStock\Block;

use Magento\Catalog\Block\Product\View as BaseView;

/**
 * Class Register
 * @package Project\BackInStock\Block
 */
class View extends BaseView
{
    /**
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('project_backInStock/register/new', ['_secure' => true]);
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}
