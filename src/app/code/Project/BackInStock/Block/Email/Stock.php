<?php

namespace Project\BackInStock\Block\Email;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\ProductAlert\Block\Email\Stock as BaseStock;

/**
 * Class Stock
 * @package Project\BackInStock\Block\Email
 */
class Stock extends BaseStock
{
    /**
     * @var Configurable
     */
    protected $configurableModel;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Stock constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Filter\Input\MaliciousCode $maliciousCode
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
     * @param Configurable $configurable
     * @param ProductRepositoryInterface $productRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Filter\Input\MaliciousCode $maliciousCode,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder,
        Configurable $configurable,
        ProductRepositoryInterface $productRepository,
        array $data = []
    )
    {
        parent::__construct($context, $maliciousCode, $priceCurrency, $imageBuilder, $data);
        $this->configurableModel = $configurable;
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param int $alertId
     */
    public function addProductWithAlertId(\Magento\Catalog\Model\Product $product, $alertId)
    {
        $this->_products[$alertId] = $product;
    }

    /**
     * @param int $alertId
     * @return string
     */
    public function getUnsubscribeAll($alertId)
    {
        $params = $this->_getUrlParams();
        $params['alert'] = $alertId;
        return $this->getUrl('project_backInStock/unsubscribe/stockAll', $params);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Api\Data\ProductInterface|\Magento\Catalog\Model\Product
     */
    public function getProduct(\Magento\Catalog\Model\Product $product)
    {
        if ($product->getTypeId() !== Configurable::TYPE_CODE) {
            $productId = $product->getId();
            $parentId = $this->configurableModel->getParentIdsByChild($productId);
            if (isset($parentId[0])) {
                try {
                    $parentProduct = $this->productRepository->getById($parentId[0]);
                    return $parentProduct;
                } catch (\Exception $e) {
                    $this->_logger->error($e->getMessage());
                }
            }
        }

        return $product;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getProductUrl(\Magento\Catalog\Model\Product $product)
    {
        $product = $this->getProduct($product);

        return $product->getProductUrl();
    }

    /**
     * Retrieve product image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array $attributes
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getImage($product, $imageId, $attributes = [])
    {
        $product = $this->getProduct($product);

        return $this->imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }
}
