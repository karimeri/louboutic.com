<?php

namespace Project\BackInStock\Block\Adminhtml\Alert;

/**
 * Class Grid
 * @package Project\BackInStock\Block\Adminhtml\Alert
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Stock Alert collection
     *
     * @var \Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Stock Alert model
     *
     * @var \Magento\ProductAlert\Model\StockFactory
     */
    protected $_stockFactory;

    protected $_filterVisibility = false;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory $collectionFactory
     * @param \Magento\ProductAlert\Model\StockFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory $collectionFactory,
        \Magento\ProductAlert\Model\StockFactory $stockFactory,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_stockFactory = $stockFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Initialize grid
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();

        $this->setId('stockAlertGrid');
        $this->setDefaultSort('add_date');
        $this->setDefaultDir('DESC');
    }

    protected function _prepareLayout()
    {
        return $this;
    }

    /**
     * Prepare related item collection
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        $this->_beforePrepareCollection();
        return parent::_prepareCollection();
    }

    /**
     * Configuring and setting collection
     *
     * @return $this
     */
    protected function _beforePrepareCollection()
    {
        if (!$this->getCollection()) {
            /** @var $collection \Magento\Rma\Model\ResourceModel\Rma\Grid\Collection */
            $collection = $this->_collectionFactory->create();
            $this->setCollection($collection);
        }
        return $this;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'alert_stock_id',
            [
                'header' => __('Stock Alert'),
                'index' => 'alert_stock_id',
                'type' => 'text',
                'sortable' => false,
                'filter' => false,
                'header_css_class' => 'col-number',
                'column_css_class' => 'col-number'
            ]
        );

        $this->addColumn(
            'product_name',
            [
                'header' => __('Product'),
                'index' => 'product_name',
                'sortable' => false,
                'filter' => false,
                'renderer' => \Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer\ProductName::class,
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        $this->addColumn(
            'product_status',
            [
                'header' => __('Product Status'),
                'index' => 'product_status',
                'sortable' => false,
                'filter' => false,
                'renderer' => \Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer\ProductStatus::class,
                'header_css_class' => 'col-status',
                'column_css_class' => 'col-status'
            ]
        );

        $this->addColumn(
            'product_stock_status',
            [
                'header' => __('Stock Status'),
                'index' => 'product_stock_status',
                'sortable' => false,
                'filter' => false,
                'renderer' => \Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer\StockStatus::class,
                'header_css_class' => 'col-status',
                'column_css_class' => 'col-status'
            ]
        );

        $this->addColumn(
            'add_locale',
            [
                'header' => __('Add Locale'),
                'index' => 'store_id',
                'type' => 'store',
                'sortable' => false,
                'filter' => false,
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'add_date',
            [
                'header' => __('Add Date'),
                'index' => 'add_date',
                'type' => 'datetime',
                'sortable' => false,
                'filter' => false,
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-period',
                'column_css_class' => 'col-period'
            ]
        );

        $this->addColumn(
            'days_in_stock_alert',
            [
                'header' => __('Days in stock alert'),
                'index' => 'days_in_stock_alert',
                'type' => 'number',
                'sortable' => false,
                'filter' => false,
                'html_decorators' => ['nobr'],
                'header_css_class' => 'col-number',
                'column_css_class' => 'col-number'
            ]
        );

        $this->addColumn(
            'notification',
            [
                'header' => __('Customer Notification'),
                'index' => 'status',
                'type' => 'options',
                'sortable' => false,
                'filter' => false,
                'header_css_class' => 'col-status',
                'column_css_class' => 'col-status',
                'options' => [1 => 'Yes', 0 => 'No']
            ]
        );

        return parent::_prepareColumns();
    }
}
