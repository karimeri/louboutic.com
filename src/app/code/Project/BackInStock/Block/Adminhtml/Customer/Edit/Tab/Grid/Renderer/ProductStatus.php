<?php

namespace Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer;

use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;

/**
 * Class ProductStatus
 * @package Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer
 */
class ProductStatus extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 2;

    /**
     * Renders item product name and its configuration
     *
     * @param \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface|\Magento\Framework\DataObject $item
     * @return string
     */
    public function render(DataObject $item)
    {
        $this->setItem($item);
        $product = $this->getProduct();
        return $this->getOptionText($product->getStatus());
    }

    /**
     * Returns product associated with this block
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->getItem()->getProduct();
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
