<?php

namespace Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer;

use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;

/**
 * Class ProductName
 * @package Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer
 */
class ProductName extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Renders item product name and its configuration
     *
     * @param \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface|\Magento\Framework\DataObject $item
     * @return string
     */
    public function render(DataObject $item)
    {
        $this->setItem($item);
        $product = $this->getProduct();
        return $product->getData('size') ? $this->_renderItemOptions($product) : $this->escapeHtml($product->getName());
    }

    /**
     * Returns product associated with this block
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->getItem()->getProduct();
    }

    /**
     * Render product item with size
     *
     * @param Product $product
     * @return string
     */
    protected function _renderItemOptions(Product $product)
    {
        $html = '<div class="product-title">' . $this->escapeHtml(
                $product->getName()
            ) . '</div>' . '<dl class="item-options">';
        $html .= '<dt>' . __('Size') . '</dt>';
        $html .= '<dd>' . $product->getData('size') . '</dd>';
        $html .= '</dl>';

        return $html;
    }
}
