<?php

namespace Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer;

use Magento\Framework\DataObject;

/**
 * Class StockStatus
 * @package Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab\Grid\Renderer
 */
class StockStatus extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    const STATUS_IN_STOCK = 1;

    const STATUS_OUT_OF_STOCK = 0;

    /**
     * Renders item product name and its configuration
     *
     * @param \Magento\Catalog\Model\Product\Configuration\Item\ItemInterface|\Magento\Framework\DataObject $item
     * @return string
     */
    public function render(DataObject $item)
    {
        $this->setItem($item);
        return $this->getOptionText($item->getIsInStock());
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return [self::STATUS_IN_STOCK => __('In Stock'), self::STATUS_OUT_OF_STOCK => __('Out of stock')];
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
