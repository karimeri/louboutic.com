<?php

namespace Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

/**
 * Class BackInStock
 * @package Project\BackInStock\Block\Adminhtml\Customer\Edit\Tab
 */
class BackInStock extends \Project\BackInStock\Block\Adminhtml\Alert\Grid implements TabInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory $collectionFactory,
        \Magento\ProductAlert\Model\StockFactory $stockFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->customerRepository = $customerRepository;
        parent::__construct($context, $backendHelper, $collectionFactory, $stockFactory, $data);
    }

    /**
     * Initialize customer edit tab back in stock
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('customer_edit_tab_back_in_stock');
        $this->setUseAjax(true);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Back in stock');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Back in stock');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID) !== null;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('project_backInStock/alert/customerGrid', ['_current' => true]);
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return '';
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return false;
    }

    /**
     * Defines after which tab, this tab should be rendered
     *
     * @return string
     */
    public function getAfter()
    {
        return 'wishlist';
    }

    /**
     * @return $this|\Project\BackInStock\Block\Adminhtml\Alert\Grid
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _beforePrepareCollection()
    {
        $customerId = null;
        $customer = $this->customerRepository->getById(
            $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID)
        );
        if ($customer && $customer->getId()) {
            $customerId = $customer->getId();
        } elseif ($this->getCustomerId()) {
            $customerId = $this->getCustomerId();
        }
        if ($customerId) {
            /** @var $collection \Magento\ProductAlert\Model\ResourceModel\Stock\Collection */
            $collection = $this->_collectionFactory->create()->addFieldToFilter('main_table.customer_id', $customerId);

            $this->setCollection($collection);
        }
        return $this;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended|void
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();
    }
}
