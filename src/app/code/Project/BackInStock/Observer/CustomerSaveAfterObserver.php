<?php
namespace Project\BackInStock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory as StockCollectionFactory;
use Magento\Framework\Event\Observer;

/**
 * Class CustomerSaveAfterObserver
 * @package Project\BackInStock\Observer
 */
class CustomerSaveAfterObserver implements ObserverInterface
{
    /**
     * @var StockCollectionFactory
     */
    protected $stockCollectionFactory;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * CustomerSaveAfterObserver constructor.
     * @param CustomerFactory $customerFactory
     * @param StockCollectionFactory $stockCollectionFactory
     */
    public function __construct(
        CustomerFactory $customerFactory,
        StockCollectionFactory $stockCollectionFactory
    ) {
        $this->customerFactory = $customerFactory;
        $this->stockCollectionFactory = $stockCollectionFactory;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getCustomer();

        $collection = $this->stockCollectionFactory->create();
        $collection->addFieldToFilter('email', ['eq' => $customer->getEmail()]);
        $collection->addFieldToFilter('customer_id', ['null' => true]);
        $collection->addFieldToFilter('status', ['eq' => 0]);

        foreach ($collection as $item) {
            $item->setCustomerId($customer->getId());
        }
        $collection->save();
    }
}
