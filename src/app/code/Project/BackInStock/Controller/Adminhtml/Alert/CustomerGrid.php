<?php

namespace Project\BackInStock\Controller\Adminhtml\Alert;

use Magento\Customer\Controller\RegistryConstants;

/**
 * Class CustomerGrid
 * @package Project\BackInStock\Controller\Adminhtml\Alert
 */
class CustomerGrid extends \Project\BackInStock\Controller\Adminhtml\Alert
{
    /**
     * Initialize customer by ID specified in request
     *
     * @return $this
     */
    protected function initCurrentCustomer()
    {
        $customerId = (int)$this->getRequest()->getParam('id');
        if ($customerId) {
            $this->_coreRegistry->register(RegistryConstants::CURRENT_CUSTOMER_ID, $customerId);
        }
        return $this;
    }

    /**
     * Customer billing agreements ajax action
     *
     * @return void
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
