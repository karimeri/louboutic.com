<?php

namespace Project\BackInStock\Controller\Unsubscribe;

use Magento\Framework\App\Action\Action;
use Magento\ProductAlert\Model\ResourceModel\Stock\CollectionFactory as StockCollectionFactory;
use Magento\ProductAlert\Model\StockFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Class StockAll
 * @package Project\BackInStock\Controller\Unsubscribe
 */
class StockAll extends Action
{
    /**
     * @var StockCollectionFactory
     */
    protected $stockCollectionFactory;

    /**
     * @var StockFactory
     */
    protected $stockFactory;

    /**
     * StockAll constructor.
     *
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param StockCollectionFactory $stockCollectionFactory
     * @param StockFactory $stockFactory
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        StockCollectionFactory $stockCollectionFactory,
        StockFactory $stockFactory
    ) {
        $this->stockCollectionFactory = $stockCollectionFactory;
        $this->stockFactory = $stockFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $alertId = (int)$this->getRequest()->getParam('alert');

        try {
            $alert = $this->stockFactory->create()->load($alertId);

            if (!is_null($alert->getCustomerId())) {
                $collection = $this->stockCollectionFactory->create();
                $collection->addFieldToFilter('customer_id', ['eq' => $alert->getCustomerId()]);
                $collection->walk('delete');
            } else {
                $collection = $this->stockCollectionFactory->create();
                $collection->addFieldToFilter('email', ['eq' => $alert->getEmail()]);
                $collection->walk('delete');
            }

            $this->messageManager->addSuccess(__('You will no longer receive stock alerts.'));
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Unable to update the alert subscription.'));
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('/');
    }
}
