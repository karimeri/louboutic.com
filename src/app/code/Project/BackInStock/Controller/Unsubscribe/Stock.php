<?php

namespace Project\BackInStock\Controller\Unsubscribe;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Action\Action;

/**
 * Class Stock
 * @package Project\BackInStock\Controller\Unsubscribe
 */
class Stock extends Action
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $alertId = (int)$this->getRequest()->getParam('product');

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl('/');
        if (!isset($alertId)) {
            return $resultRedirect;
        }

        try {
            $model = $this->_objectManager->create(\Magento\ProductAlert\Model\Stock::class)
                ->setAlertStockId($alertId)
                ->loadByParam();
            if ($model->getId()) {
                $model->delete();
            }
            $this->messageManager->addSuccess(__('You will no longer receive stock alerts for this product.'));
        } catch (NoSuchEntityException $noEntityException) {
            $this->messageManager->addError(__('This alert was not found.'));
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the alert subscription right now.'));
        }
        return $resultRedirect;
    }
}
