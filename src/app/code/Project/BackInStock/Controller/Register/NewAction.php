<?php

namespace Project\BackInStock\Controller\Register;

use Magento\Customer\Api\AccountManagementInterface as CustomerAccountManagement;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Exception\LocalizedException;
use Project\BackInStock\Model\RegisterRepository;
use Magento\ProductAlert\Model\StockFactory;

/**
 * Class NewAction
 * @package Project\BackInStock\Controller\Register
 */
class NewAction extends Action
{
    /**
     * @var CustomerAccountManagement
     */
    protected $customerAccountManagement;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var StockFactory
     */
    protected $registerFactory;

    /**
     * @var RegisterRepository
     */
    private $registerRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * NewAction constructor.
     *
     * @param Context $context
     * @param StockFactory $registerFactory
     * @param RegisterRepository $registerRepository
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     * @param CustomerAccountManagement $customerAccountManagement
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        StockFactory $registerFactory,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        CustomerAccountManagement $customerAccountManagement,
        JsonFactory $resultJsonFactory,
        RegisterRepository $registerRepository
    ) {
        $this->customerAccountManagement = $customerAccountManagement;
        $this->storeManager = $storeManager;
        $this->registerFactory = $registerFactory;
        $this->customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->registerRepository = $registerRepository;
        parent::__construct($context);
    }

    /**
     * @param $email
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function validateEmailAvailable($email)
    {
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        if ($this->customerSession->isLoggedIn() && $this->customerSession->getCustomer()->getEmail() !== $email) {
            if (!$this->customerAccountManagement->isEmailAvailable($email, $websiteId)) {
                throw new LocalizedException(
                    __('Please log in to your customer account before signing in to the back in stock alert.')
                );
            }
        }
    }

    /**
     * @param $email
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    protected function validateEmailFormat($email)
    {
        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            throw new LocalizedException(__('Please enter a valid email address.'));
        }
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data['error'] = true;
        $data['message'] = __('Please enter your email address.');

        if ($this->getRequest()->isAjax()
            && $this->getRequest()->getMethod() == 'POST'
            && $this->getRequest()->isXmlHttpRequest()
            && $this->getRequest()->getPost('email')
        ) {
            $email = (string) $this->getRequest()->getPost('email');
            try {
                $this->validateEmailFormat($email);
                $this->validateEmailAvailable($email);

                $productId = $this->getRequest()->getPost('product');
                if (!empty($this->getRequest()->getPost('selected_configurable_option'))) {
                    $productId = $this->getRequest()->getPost('selected_configurable_option');
                }

                $register = $this->registerFactory->create();
                $register->setEmail($email)
                    ->setPhoneNumber($this->getRequest()->getPost('phone_number'))
                    ->setCustomerId($this->customerSession->getCustomerId())
                    ->setProductId($productId)
                    ->setWebsiteId(
                        $this->_objectManager->get(StoreManagerInterface::class)
                            ->getStore()
                            ->getWebsiteId()
                    )
                    ->setStoreId($this->storeManager->getStore()->getId());

                $status = $this->registerRepository->save($register);

                if ($status) {
                    $data['error'] = false;
                    $data['message'] = __('Thank you for your subscription.');
                }
            } catch (LocalizedException $exception) {
                $data['message'] = __($exception->getMessage());
            } catch (\Exception $exception) {
                $data['message'] = __('Something went wrong with the subscription. Are you already register ? ');
            }
        }

        $result = $this->resultJsonFactory->create();

        return $result->setData($data);
    }
}
