<?php

namespace Project\BackInStock\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Project\Catalog\Helper\Label;

/**
 * Class BackInStockHelper
 * @package Project\BackInStock\Helper
 */
class BackInStockHelper extends AbstractHelper
{
    const XML_PATH_URL_TRACKING_PARAMETERS = 'louboutin_backinstock/general/tracking_parameters';

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var Label
     */
    private $catalogHelper;

    /**
     * BackInStockHelper constructor.
     *
     * @param Context $context
     * @param Session $customerSession
     * @param Registry $registry
     * @param Label $catalogHelper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Registry $registry,
        Label $catalogHelper
    ) {
        $this->customerSession = $customerSession;
        $this->registry = $registry;
        $this->catalogHelper = $catalogHelper;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * @return Product|mixed
     * @throws LocalizedException
     */
    public function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }

    /**
     * @return bool|int
     * @throws LocalizedException
     */
    public function isBackInStock()
    {
        if (!is_null($this->getProduct())) {
            return (int) $this->getProduct()->getData($this->catalogHelper::PRODUCT_ATTRIBUTE_BACKINSTOCK);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getTrackingParameters()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_URL_TRACKING_PARAMETERS);
    }
}
