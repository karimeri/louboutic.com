define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'Magento_Customer/js/customer-data',
    'mage/mage',
    'jquery/ui'
], function ($, modal, customerData) {
    'use strict';
    $.widget('project.processPopupBackInStock', {
        options: {
            modalForm: '#popup-backInStock',
            modalButton: '#register-popup',
            addToCartForm: '#product_addtocart_form'
        },
        customer: customerData.get('customer'),

        _create: function() {
            this.options.modalOption = this._getModalOptions();
            this._bind();
            this._getMore()
        },
        _getModalOptions: function() {
            var popup_backInStock_options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                buttons: false,
                modalClass : 'popup-backInStock'
            };

            return popup_backInStock_options;
        },
        _bind: function() {
            var modalOption = this.options.modalOption;
            var modalForm = this.options.modalForm;
            var addToCartForm = this.options.addToCartForm;
            var self = this;

            $(document).on('click', this.options.modalButton,  function(){
                // Set selected_configurable_option
                var selectSimpleProduct = $(addToCartForm).find('[name="selected_configurable_option"]').val();
                $(modalForm).find('[name="selected_configurable_option"]').val(selectSimpleProduct);

                // Set customer email if logged in
                $(modalForm).find('#backInStock_email').val(self._getEmail());

                // Set customer telephone if logged in
                $(modalForm).find('#backInStock_number').val(self._getTelephone());

                // Create modal
                $(modalForm).modal(modalOption);

                // Open modal
                $(modalForm).trigger('openModal');
            });

            $(modalForm).find('form').submit(function() {
                self._submitHandler($(this), modalForm);
                return false;
            });
        },
        _submitHandler: function($form, modalForm) {
            if ($form.validation('isValid')) {
                $.ajax({
                    url: $form.attr('action'),
                    cache: true,
                    data: $form.serialize(),
                    dataType: 'json',
                    type: 'POST',
                    showLoader: true
                }).done(function (data) {
                    $(modalForm).find('.messages .message div').html(data.message);
                    if (data.error) {
                        $(modalForm).find('.messages .message').addClass('message-error error');
                    } else {
                        $(modalForm).find('.messages .message').addClass('message-success success');
                        setTimeout(function() {
                            $(modalForm).modal('closeModal');
                        }, 1000);

                        //dataLayer
                        if (window.dataLayer) {
                            window.dataLayer.push({
                                'event': 'gtm.ext.event',
                                'eventTmp': 'stockAlert',
                                'eventCategory': 'stockAlert',
                                'eventAction': 'alertSubscription',
                                'eventLabel':  $('[data-ui-id="page-title-wrapper"]').text()
                            });
                        }
                    }
                    $(modalForm).find('.messages').show();
                    setTimeout(function() {
                        $(modalForm).find('.messages').hide();
                    }, 5000);
                });
            }
        },
        _getEmail: function () {
            return this.customer().email;
        },
        _getTelephone: function () {
            return this.customer().telephone;
        },
        _getMore: function () {
            $(document).on('click', '#moreLink',  function() {
                if ($('#dots').css('display') === 'none') {
                    $('#dots').css("display", "inline");
                    $('#moreLink').html($.mage.__("Read more »"));
                    $('#more').css("display", "none");
                } else {
                    $('#dots').css("display", 'none');
                    $('#moreLink').html($.mage.__("« Read less"));
                    $('#more').css("display", "inline");
                }
            });
        }
    });

    return $.project.processPopupBackInStock;
});
