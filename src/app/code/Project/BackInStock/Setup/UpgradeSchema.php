<?php

namespace Project\BackInStock\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class UpgradeSchema
 * @package Project\BackInStock\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const VERSIONS = [
        '1.0.1' => '101',
        '1.0.2' => '102'
    ];

    /**
     * @var ConsoleOutput
     */
    protected $output;

    /**
     * @var SchemaSetupInterface
     */
    protected $setup;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * UpgradeSchema constructor.
     *
     * @param ConsoleOutput $output
     * @param ObjectManagerInterface $objectManager
     *
     * @throws \RuntimeException
     */
    public function __construct(
        ConsoleOutput $output,
        ObjectManagerInterface $objectManager
    ) {
        $this->output        = $output;
        $this->objectManager = $objectManager;
    }

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $setup->startSetup();

        $this->output->writeln(''); // new line in console

        foreach (self::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Project BackInStock setup : $version");

                $currentSetup = $this->getObjectManager()
                    ->create('Project\BackInStock\Setup\UpgradeSchema\UpgradeSchema'.$fileData);
                $currentSetup->upgrade($setup, $context);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return SchemaSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
