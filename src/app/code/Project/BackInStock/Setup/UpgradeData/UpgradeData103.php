<?php

namespace Project\BackInStock\Setup\UpgradeData;

use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class UpgradeData103
 * @package Project\BackInStock\Setup\UpgradeData
 */
class UpgradeData103
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * UpgradeData103 constructor.
     * @param ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }

    public function run()
    {
        $installer = $this->setup;
        $installer->startSetup();

        $table = $installer->getTable('product_alert_stock');
        $sql = "UPDATE " . $table . " SET customer_id = NULL where customer_id = 0;";
        $installer->run($sql);

        $installer->endSetup();
    }
}
