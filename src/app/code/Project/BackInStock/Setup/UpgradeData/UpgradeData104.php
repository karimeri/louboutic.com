<?php

namespace Project\BackInStock\Setup\UpgradeData;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Model\BlockFactory;
use Psr\Log\LoggerInterface;

/**
 * Class UpgradeData104
 * @package Project\BackInStock\Setup\UpgradeData
 */
class UpgradeData104
{
    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var BlockRepositoryInterface
     */
    protected $blockRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * UpgradeData104 constructor.
     * @param BlockFactory $blockFactory
     * @param BlockRepositoryInterface $blockRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        BlockFactory $blockFactory,
        BlockRepositoryInterface $blockRepository,
        LoggerInterface $logger
    ) {
        $this->blockFactory     = $blockFactory;
        $this->blockRepository  = $blockRepository;
        $this->logger           = $logger;
    }

    public function run()
    {
        try {
            /** create backinstock block - version FR */
            $backInStockBlockFRData = [
                'title' => 'product size backinstock',
                'identifier' => 'product-size-backinstock',
                'content' => "<p>Votre pointure n’est pas disponible ?<br />Inscrivez vous à nos back in stock !</p>",
                'is_active' => 1,
                'stores' => [6,24,33,39,54],
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($backInStockBlockFRData)->save();

            /** create backinstock block - version EN **/
            $backInStockBlockENData = [
                'title' => 'product size backinstock',
                'identifier' => 'product-size-backinstock',
                'content' => "<p>Size not available?<br />Subscribe to our Back in Stock!</p>",
                'is_active' => 1,
                'stores' => [9,12,15,18,21,27,30,36,42,45,48,51,57],
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($backInStockBlockENData)->save();
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable->getMessage());
        }
    }
}