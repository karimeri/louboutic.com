<?php

namespace Project\BackInStock\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\BackInStock\Model\RegisterRepository;

/**
 * Class UpgradeSchema101
 * @package Project\BackInStock\Setup\UpgradeSchema
 */
class UpgradeSchema101 implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable(RegisterRepository::TABLE_NAME);

        $setup->getConnection()
            ->addColumn(
                $tableName,
                'store_id',
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'size'      => null,
                    'unsigned'  => true,
                    'nullable'  => false,
                    'default'   => '0',
                    'comment'   => 'Store id'
                ]
            );

        $setup->endSetup();
    }
}