<?php

namespace Project\BackInStock\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Project\BackInStock\Model\RegisterRepository;

/**
 * Class UpgradeSchema102
 * @package Project\BackInStock\Setup\UpgradeSchema
 */
class UpgradeSchema102 implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable(RegisterRepository::TABLE_NAME);

        $setup->getConnection()
            ->modifyColumn(
                $tableName,
                'customer_id',
                [
                    'type'      => Table::TYPE_INTEGER,
                    'unsigned'  => true,
                    'nullable'  => true,
                    'default'   => null
                ]
            );

        $setup->endSetup();
    }
}