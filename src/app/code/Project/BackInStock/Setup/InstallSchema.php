<?php

namespace Project\BackInStock\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Project\BackInStock\Model\RegisterRepository;

/**
* Class InstallSchema
*
* @package Project\EmailSent\Setup
*/
class InstallSchema implements InstallSchemaInterface
{
    /**
    * @param SchemaSetupInterface $setup
    * @param ModuleContextInterface $context
    */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table = $setup->getTable(RegisterRepository::TABLE_NAME);

        $setup->getConnection()->addColumn(
            $table,
            'email',
            [
                'type'     => Table::TYPE_TEXT,
                'length' => 255,
                'comment'  => 'email',
            ]
        );

        $setup->getConnection()->addColumn(
            $table,
            'phone_number',
            [
                'type'     => Table::TYPE_TEXT,
                'length' => 255,
                'comment'  => 'phone number',
            ]
        );

        $setup->getConnection()->modifyColumn(
            $table,
            'customer_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'null' => false,
                'unsigned' => true,
                'comment'  => 'customer id',
            ]
        );

        $setup->getConnection()->addIndex(
            $table,
            $setup->getIdxName(
                $table,
                ['email', 'product_id'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['email', 'product_id'],
            AdapterInterface::INDEX_TYPE_UNIQUE
        );

        $setup->endSetup();
    }
}
