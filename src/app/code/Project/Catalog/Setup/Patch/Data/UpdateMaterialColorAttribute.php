<?php

namespace Project\Catalog\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class UpdateMaterialColorAttribute
 * @package Project\Catalog\Setup\Patch\Data
 */
class UpdateMaterialColorAttribute implements DataPatchInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * UpdateMaterialColorAttribute constructor.
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     */
    public function __construct(
        \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $attribute = [
            'type' => \Magento\Catalog\Model\Product::ENTITY,
            'code' => 'material_color',
            'data' => [
                'used_in_product_listing' => 1,
            ]
        ];

        foreach ($attribute['data'] as $field => $value) {
            $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.1';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
