<?php

namespace Project\Catalog\Model;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogUrlRewrite\Model\Product\CanonicalUrlRewriteGenerator;
use Magento\Framework\Model\ResourceModel\Iterator;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Psr\Log\LoggerInterface;

/**
 * Class DataCheckManager
 * @package Project\Catalog\Model
 */
class DataCheckManager
{
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var ProductResourceModel
     */
    protected $productResource;

    /**
     * @var CanonicalUrlRewriteGenerator
     */
    protected $canonicalUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Iterator
     */
    protected $iterator;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * DataCheckManager constructor.
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductFactory $productFactory
     * @param ProductResourceModel $productResource
     * @param CanonicalUrlRewriteGenerator $canonicalUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param StoreManagerInterface $storeManager
     * @param Iterator $iterator
     * @param LoggerInterface $logger
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        ProductFactory $productFactory,
        ProductResourceModel $productResource,
        CanonicalUrlRewriteGenerator $canonicalUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        StoreManagerInterface $storeManager,
        Iterator $iterator,
        LoggerInterface $logger
    ) {
        $this->productCollectionFactory     = $productCollectionFactory;
        $this->productFactory               = $productFactory;
        $this->productResource              = $productResource;
        $this->canonicalUrlRewriteGenerator = $canonicalUrlRewriteGenerator;
        $this->urlPersist                   = $urlPersist;
        $this->storeManager                 = $storeManager;
        $this->iterator                     = $iterator;
        $this->logger                       = $logger;
    }

    public function checkUrlKeys() {
        try {
            $this->iterator->walk(
                $this->getProductCollection()->getSelect(),
                [[$this, 'callback']],
                [
                    'product' => $this->productFactory->create()
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param $args
     */
    public function callback($args)
    {
        try {
            /** @var Product $product */
            $product = clone $args['product'];
            $product->setData($args['row']);

            $stores = $this->storeManager->getStores();
            foreach ($stores as $store) {
                $urlKey = $this->productResource->getAttributeRawValue(
                    $product->getId(),
                    'url_key',
                    $store->getId()
                );
                // If url key contains 1 uppercase letter
                if (preg_match('/[A-Z]/', $urlKey)) {
                    // Change url key
                    $product->addAttributeUpdate('url_key', strtolower($urlKey), $store->getId());
                    $product->setStoreId($store->getId())->load($product->getId());
                    // Rewrite url
                    $urls = $this->canonicalUrlRewriteGenerator->generate($store->getId(), $product);
                    $this->urlPersist->replace($urls);
                }
            }
            // Change url key for default store id (admin)
            $urlKey = $this->productResource->getAttributeRawValue(
                $product->getId(),
                'url_key',
                Store::DEFAULT_STORE_ID
            );
            $product->addAttributeUpdate('url_key', strtolower($urlKey), Store::DEFAULT_STORE_ID);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private function getProductCollection()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToSelect('url_key', 'left');
        return $productCollection;
    }
}
