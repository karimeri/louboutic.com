<?php

namespace Project\Catalog\Model\ResourceModel\CategoryProduct;

use Magento\Catalog\Model\ResourceModel\CategoryProduct as CategoryProductResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Catalog\Model\CategoryProduct;

/**
 * Class Collection
 * @package Project\Catalog\Model\ResourceModel\CategoryProduct
 */
class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(CategoryProduct::class, CategoryProductResourceModel::class);
    }
}
