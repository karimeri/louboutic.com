<?php

namespace Project\Catalog\Model\Magento\Catalog;

/**
 * Class Category
 * @package Project\Catalog\Model\Magento\Catalog
 */
class Category extends \Magento\Catalog\Model\Category
{
    /**
     * Save current attribute with code $code and assign new value
     *
     * @param string $code Attribute code
     * @param mixed $value New attribute value
     * @param int $store Store ID
     * @return void
     */
    public function addAttributeUpdate($code, $value, $store)
    {
        $oldValue = $this->getData($code);
        $oldStore = $this->getStoreId();

        $this->setData($code, $value);
        $this->setStoreId($store);
        $this->getResource()->saveAttribute($this, $code);

        $this->setData($code, $oldValue);
        $this->setStoreId($oldStore);
    }
}
