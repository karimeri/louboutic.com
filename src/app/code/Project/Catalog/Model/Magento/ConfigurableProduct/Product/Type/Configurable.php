<?php

namespace Project\Catalog\Model\Magento\ConfigurableProduct\Product\Type;

use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Option;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory as EavAttributeFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Collection\SalableProcessor;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableBase;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ConfigurableResource;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable\Attribute\CollectionFactory
    as AttributeCollectionFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable\Product\CollectionFactory
    as ProductCollectionFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\ConfigurableFactory;
use Magento\Customer\Model\Session;
use Magento\Eav\Model\Config;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Cache\FrontendInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Psr\Log\LoggerInterface;

/**
 * Class Configurable
 * @package Project\Catalog\Model\Magento\ConfigurableProduct\Product\Type
 * @author Synolia <contact@synolia.com>
 */
class Configurable extends ConfigurableBase
{
    /**
     * @var SalableProcessor|null
     */
    protected $salableProcessorFix;

    /**
     * Initialized products
     * @var array
     */
    protected $products = [];

    /**
     * Configurable constructor.
     * @param Option $catalogProductOption
     * @param Config $eavConfig
     * @param Type $catalogProductType
     * @param ManagerInterface $eventManager
     * @param Database $fileStorageDb
     * @param Filesystem $filesystem
     * @param Registry $coreRegistry
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param ConfigurableFactory $typeConfigurableFactory
     * @param EavAttributeFactory $eavAttributeFactory
     * @param ConfigurableBase\AttributeFactory $configurableAttributeFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param ConfigurableResource $catalogProductTypeConfigurable
     * @param ScopeConfigInterface $scopeConfig
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param FrontendInterface|null $cache
     * @param Session|null $customerSession
     * @param Json|null $serializer
     * @param ProductInterfaceFactory|null $productFactory
     * @param SalableProcessor|null $salableProcessor
     */
    public function __construct(
        Option $catalogProductOption,
        Config $eavConfig,
        Type $catalogProductType,
        ManagerInterface $eventManager,
        Database $fileStorageDb,
        Filesystem $filesystem,
        Registry $coreRegistry,
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        ConfigurableFactory $typeConfigurableFactory,
        EavAttributeFactory $eavAttributeFactory,
        ConfigurableBase\AttributeFactory $configurableAttributeFactory,
        ProductCollectionFactory $productCollectionFactory,
        AttributeCollectionFactory $attributeCollectionFactory,
        ConfigurableResource $catalogProductTypeConfigurable,
        ScopeConfigInterface $scopeConfig,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        FrontendInterface $cache = null,
        Session $customerSession = null,
        Json $serializer = null,
        ProductInterfaceFactory $productFactory = null,
        SalableProcessor $salableProcessor = null
    ) {
        parent::__construct(
            $catalogProductOption,
            $eavConfig,
            $catalogProductType,
            $eventManager,
            $fileStorageDb,
            $filesystem,
            $coreRegistry,
            $logger,
            $productRepository,
            $typeConfigurableFactory,
            $eavAttributeFactory,
            $configurableAttributeFactory,
            $productCollectionFactory,
            $attributeCollectionFactory,
            $catalogProductTypeConfigurable,
            $scopeConfig,
            $extensionAttributesJoinProcessor,
            $cache,
            $customerSession,
            $serializer,
            $productFactory,
            $salableProcessor
        );

        // salableProcessor is private in parent
        $this->salableProcessorFix = $salableProcessor ?: ObjectManager::getInstance()->get(SalableProcessor::class);
    }

    /**
     * Set number of simple products in the collection that have quantity > 0
     * @param Product $product
     */
    public function setSimpleProductsCount($product)
    {
        if (isset($this->products[$product->getId()])) {
            return;
        }

        $collection = $this->getUsedProductCollection($product);
        $collection->addStoreFilter($this->getStoreFilter($product));
        $collection = $this->salableProcessorFix->process($collection);

        $product->setSimpleProductsCount($collection->getSize());

        $this->products[$product->getId()] = true;
    }
}
