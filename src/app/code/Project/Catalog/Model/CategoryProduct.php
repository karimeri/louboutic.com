<?php

namespace Project\Catalog\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class CategoryProduct
 * @package Project\Catalog\Model
 */
class CategoryProduct extends AbstractModel
{
    /**
     * @var \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory
     */
    protected $categoryProductCollectionFactory;

    /**
     * CategoryProduct constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->categoryProductCollectionFactory = $categoryProductCollectionFactory;
    }

    protected function _construct()
    {
        $this->_init(\Magento\Catalog\Model\ResourceModel\CategoryProduct::class);
    }

    /**
     * @param int $categoryId
     * @param int $productId
     * @return \Magento\Framework\DataObject
     */
    public function loadByCategoryIdAndProductId($categoryId, $productId)
    {
        $categoryProductCollection = $this->getCategoryProductCollection(
            [
                'category_id' => $categoryId,
                'product_id' => $productId
            ]
        );
        return $categoryProductCollection->getFirstItem();
    }

    /**
     * @param int $categoryId
     * @return array
     */
    public function getProductsByCategoryId($categoryId)
    {
        $categoryProductCollection = $this->getCategoryProductCollection(
            [
                'category_id' => $categoryId
            ]
        );
        return $categoryProductCollection->getColumnValues('product_id');
    }

    /**
     * @param int $productId
     * @return array
     */
    public function getCategoriesByProductId($productId)
    {
        $categoryProductCollection = $this->getCategoryProductCollection(
            [
                'product_id' => $productId
            ]
        );
        return $categoryProductCollection->getColumnValues('category_id');
    }

    /**
     * @param array $filters
     * @return \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection
     */
    protected function getCategoryProductCollection(array $filters = [])
    {
        /** @var \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection $categoryProductCollection */
        $categoryProductCollection = $this->categoryProductCollectionFactory->create();
        foreach ($filters as $field => $condition) {
            $categoryProductCollection->addFieldToFilter($field, $condition);
        }
        return $categoryProductCollection->load();
    }
}
