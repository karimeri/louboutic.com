<?php
namespace Project\Catalog\Plugin\Magento\Framework\View\Page;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Layer\Category\FilterableAttributeList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Page\Config;

/**
 * Class ConfigPlugin
 * @package Project\Catalog\Plugin\Magento\Framework\View\Page
 * @author Synolia <contact@synolia.com>
 */
class ConfigPlugin
{
    const NONINDEXABLE_ATTRIBUTES = [
        'size',
        'heel_height_map',
    ];

    const NONINDEXABLE_VALUE = 'NOINDEX,FOLLOW';

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var FilterableAttributeList
     */
    protected $filterableAttributeList;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * ConfigPlugin constructor.
     * @param RequestInterface $request
     * @param FilterableAttributeList $filterableAttributeList
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        RequestInterface $request,
        FilterableAttributeList $filterableAttributeList,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->request = $request;
        $this->filterableAttributeList = $filterableAttributeList;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Set value defined in constant when category has "left_menu_expands" activated or
     * when specific filters have been set or when there is two or more filters
     * @param Config $config
     * @param $result
     * @return string
     */
    public function afterGetRobots(Config $config, $result)
    {
        if ($this->request->getFullActionName() !== 'catalog_category_view') {
            return $result;
        }

        $params = $this->request->getParams();

        $category = $this->categoryRepository->get($params['id']);

        if ($category->getData('left_menu_expands')) {
            return $this::NONINDEXABLE_VALUE;
        }

        $attributes = $this->filterableAttributeList->getList();
        $attributesCodes = [];

        foreach ($attributes as $items) {
            $attributesCodes[] = $items->getAttributeCode();
        }

        $intersect = (array) array_intersect($attributesCodes, array_keys($params));

        if (($intersect && count($intersect) === 1 && in_array(reset($intersect), $this::NONINDEXABLE_ATTRIBUTES))
            || ($intersect && count($intersect) >= 2)
        ) {
            return $this::NONINDEXABLE_VALUE;
        }

        return $result;
    }
}
