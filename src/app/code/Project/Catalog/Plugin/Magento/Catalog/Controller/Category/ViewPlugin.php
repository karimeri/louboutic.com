<?php

namespace Project\Catalog\Plugin\Magento\Catalog\Controller\Category;

use Magento\Catalog\Controller\Category\View;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Category;

/**
 * Class ViewPlugin
 * @package Project\Catalog\Plugin\Magento\Catalog\Controller\Category
 * @author Synolia <contact@synolia.com>
 */
class ViewPlugin
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * ViewPlugin constructor.
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param \Magento\Catalog\Controller\Category\View $subject
     * @param callable $proceed
     * @return mixed
     */
    public function aroundExecute(
        View $subject,
        callable $proceed
    ) {
        $result = $proceed();

        if (! $result instanceof \Magento\Framework\View\Result\Page) {
             return $result;
        }

        /** @var $currentCategory Category */
        $currentCategory = $this->registry->registry('current_category');

        if ($currentCategory
            && $currentCategory->getId()
            && $currentCategory->getDisplayMode() == Category::DM_PAGE) {
            $result->getConfig()->addBodyClass('cms-only');
        }

        return $result;
    }
}
