<?php

namespace Project\Catalog\Plugin\Magento\Catalog\Model;

use Magento\Catalog\Model\Product;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ProductPlugin
 * @package Project\Catalog\Plugin\Magento\Catalog\Model
 * @author Synolia <contact@synolia.com>
 */
class ProductPlugin
{
    /**
     * @var array
     */
    protected $attributes = [
        'name',
        'meta_description'
    ];

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ProductPlugin constructor.
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(StoreManagerInterface $storeManager)
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @param Product $subject
     * @param $result
     * @return string
     */
    public function afterGetName(Product $subject, $result)
    {
        return $this->processName($subject, $result);
    }

    /**
     * @param Product $subject
     * @param \Closure $proceed
     * @param string $key
     * @param null $index
     * @return mixed
     */
    public function aroundGetData(Product $subject, \Closure $proceed, $key = '', $index = null)
    {
        if (!in_array($key, $this->attributes)) {
            return $proceed($key, $index);
        }
        $value   = $proceed($key, $index);
        $method  = $this->getMethodName($key);
        return $this->$method($value, $subject);
    }

    /**
     * @param $metaDescription
     * @return string
     */
    public function getCustomMetaDescription($metaDescription)
    {
        $storeCode = $this->storeManager->getStore()->getCode();
        $replace   = __("meta_description_store_$storeCode");
        return str_replace('{{country}}', $replace, $metaDescription);
    }

    /**
     * @param Product $subject
     * @param $name
     * @return string
     */
    public function getCustomName($name, Product $subject)
    {
        return $this->processName($subject, $name);
    }

    /**
     * @param $value
     * @return string
     */
    public function getMethodName($value)
    {
        return 'getCustom'.str_replace('_', '', ucwords($value, '_'));
    }

    /**
     * @param Product $subject
     * @param $name
     * @return string
     */
    protected function processName(Product $subject, $name)
    {
        $value = $subject->getFormatName();
        return (bool)$value ? ucwords(
            strtolower($name),
            " \t\r\n\f\v/"
        ) : $name;
    }
}
