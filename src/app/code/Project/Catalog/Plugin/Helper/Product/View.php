<?php
namespace Project\Catalog\Plugin\Helper\Product;

use Magento\Framework\App\Helper\Context;

class View extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface
     */
    protected $_attributeSetRepository;

    /**
     *
     * @var \Magento\Framework\Filter\FilterManager
     */
    protected $_filterManager;

   /**
    *
    * @param Context $context
    * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet
    * @param \Magento\Framework\Filter\FilterManager $filterManager
    */
    public function __construct(
        Context $context,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet,
        \Magento\Framework\Filter\FilterManager $filterManager
    )
    {
        parent::__construct($context);
        $this->_attributeSetRepository = $attributeSet;
        $this->_filterManager = $filterManager;
    }


    /**
     *
     * @param \Magento\Catalog\Helper\Product\View $view
     * @param \Magento\Framework\View\Result\Page $resultPage
     * @param \Magento\Catalog\Model\Product $product
     * @param array $params
     * @return \Magento\Framework\View\Result\Page[]|\Magento\Catalog\Model\Product[]|string[]|\Magento\Framework\DataObject[]
     */
    public function beforeInitProductLayout(
        \Magento\Catalog\Helper\Product\View $view,
        \Magento\Framework\View\Result\Page $resultPage,
        \Magento\Catalog\Model\Product $product, $params = null
    ){
        if (! $params) {
            $params = new \Magento\Framework\DataObject();
        }
        $afterHandles = $params->getAfterHandles();
        if (! $afterHandles) {
            $afterHandles = array();
        }

        $attributeSetId = $product->getAttributeSetId();
        $attributeSet = $this->_attributeSetRepository->get($attributeSetId);

        $afterHandles[] = 'catalog_product_view_attributeset_'.$this->_filterManager->translitUrl($attributeSet->getAttributeSetName());
        $params->setAfterHandles($afterHandles);

        return [
            $resultPage,
            $product,
            $params
        ];
    }
}

?>
