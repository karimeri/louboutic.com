<?php

namespace Project\Catalog\Block;

/**
 * Class Breadcrumbs
 * @package Project\Catalog\Block
 * @author Synolia <contact@synolia.com>
 */
class Breadcrumbs extends \Magento\Catalog\Block\Breadcrumbs
{
    /**
     * @return $this
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _prepareLayout()
    {
        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $title = [];
            $path = $this->_catalogData->getBreadcrumbPath();

            foreach ($path as $name => $breadcrumb) {
                $breadcrumbsBlock->addCrumb($name, $breadcrumb);
                $title[] = $breadcrumb['label'];
            }

            $this->pageConfig->getTitle()->set(join($this->getTitleSeparator(), array_reverse($title)));
        }

        return $this;
    }
}
