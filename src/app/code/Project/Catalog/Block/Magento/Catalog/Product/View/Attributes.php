<?php
namespace Project\Catalog\Block\Magento\Catalog\Product\View;

use Magento\Catalog\Block\Product\View\Attributes as AttributesBase;
use Magento\Framework\Phrase;
use Magento\Catalog\Api\AttributeSetRepositoryInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Project\Catalog\Helper\Config;

/**
 * Class Attributes
 * @package Project\Catalog\Block\Magento\Catalog\Product\View
 * @author Synolia <contact@synolia.com>
 */
class Attributes extends AttributesBase
{
    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSet;

    /**
     * @var Config
     */
    protected $_attributeConfig;

    /**
     * Attributes constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param AttributeSetRepositoryInterface $attributeSet
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        PriceCurrencyInterface $priceCurrency,
        AttributeSetRepositoryInterface $attributeSet,
        array $data = [],
        Config $attributeConfig
    ){
        $this->attributeSet = $attributeSet;
        $this->_attributeConfig = $attributeConfig;

        parent::__construct($context, $registry, $priceCurrency, $data);
    }
    /**
     * @param array $excludeAttr
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getAdditionalData(array $excludeAttr = [])
    {
        $data = [];
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode()) || (string) $value == '') {
                    continue;
                }

                if ($attribute->getFrontendInput() === 'price' && is_string($value)) {
                    $value = $this->priceCurrency->convertAndFormat($value);
                }
                if ($attribute->getAttributeCode() === 'height' ||
                    $attribute->getAttributeCode() === 'length' ||
                    $attribute->getAttributeCode() === 'width'
                ) {
                    if($this->isBeltProduct() && !$this->_attributeConfig->getDisplayDimensions()) {
                        continue;
                    } else {
                        if (!isset($dataDimensions)) {
                            $dataDimensions = [
                                'label' => __('Dimensions'),
                                'value' => '',
                                'code' => 'dimensions',
                            ];
                        }
                        $value = $value * 10;
                        $dataDimensions['values'][] = (int)$value <> 0 ? $value . 'mm' : 0;
                        $data['dimensions'] = $dataDimensions;
                        if (count($dataDimensions['values']) == 3) {
                            $tmp = $dataDimensions['values'][1];
                            $dataDimensions['values'][1] = $dataDimensions['values'][2];
                            $dataDimensions['values'][2] = $tmp;
                        }
                        $data['dimensions']['value'] = (int)$value <> 0 ? \implode(' x ', $dataDimensions['values']) : 0;
                        continue;
                    }
                }

                if ($value instanceof Phrase || (is_string($value) && strlen($value))) {
                    $data[$attribute->getAttributeCode()] = [
                        'label' => __($attribute->getStoreLabel()),
                        'value' => $value,
                        'code' => $attribute->getAttributeCode(),
                    ];
                }
            }
        }
        return $data;
    }

    /**
     * @return mixed
     */
    public function getAttributeSetName() {
        $product = $this->getProduct();
        $attributeSetRepository = $this->attributeSet->get($product->getAttributeSetId());
        return $attributeSetRepository->getAttributeSetName();
    }

    /**
     * @return bool
     */
    public function isBeautyProduct() {
        if(strpos($this->getAttributeSetName(),"Beauty") === false){
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function isBeltProduct() {
        if(strpos($this->getAttributeSetName(),"Belts") === false){
            return false;
        }
        return true;
    }
}
