<?php
namespace Project\Catalog\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Upsell
 * @package Project\Catalog\Helper
 */
class Upsell extends AbstractHelper
{
    /**
     * @param $array
     * @param $attribute
     * @return |null
     */
    public function sortArrayByAttribute($array, $attribute)
    {
        try {
            foreach ($array as $key => $value) {
                $sortArray[$array[$key]->getData($attribute)] = $array[$key];
                unset($array[$key]);
            }
            ksort($sortArray);
            return $sortArray;
        } catch (\Exception $e) {
            return null;
        }
    }
}
