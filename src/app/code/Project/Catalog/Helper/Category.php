<?php

namespace Project\Catalog\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Model\Category as CategoryModel;

/**
 * Class Category
 * @package Project\Catalog\Helper
 */
class Category extends AbstractHelper
{
    protected $registry;

    /**
     * Category constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * @param CategoryModel $category
     * @return bool
     */
    public function isCategoryCmsOnlyView($category)
    {
        return $category->getDisplayMode() == CategoryModel::DM_PAGE;
    }

    /**
     * @param $categoryId
     * @param $productCollection
     * @return bool
     */
    public function hasProductCollection($categoryId, $productCollection)
    {
        $productCollection->addCategoriesFilter(['eq' => [$categoryId]])
                          ->addAttributeToSelect(['entity_id','name', 'visibility', 'status'])
                          ->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                          ->addAttributeToFilter('visibility', ['neq' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE]);

        $productCollection->getSelect()
            ->joinLeft(
                ['ccp' => 'catalog_category_product'],
                'ccp.product_id = cat_index.product_id AND ccp.category_id = cat_index.category_id',
                'to_merch'
            )->where(
                'ccp.to_merch = ?',
                0
            );

        $result = $productCollection->getData();

        if (!empty($result))
            return true;

        return false;
    }

    /**
     * @return mixed
     */
    public function getCurrentCategory()
    {
        return $this->registry->registry('current_category');
    }

    /**
     * @param $level
     * @return mixed|null
     */
    public function getLevelCategory($level){
        if($this->getCurrentCategory()){
            if($this->getCurrentCategory()->getParentCategories()){
                foreach ($this->getCurrentCategory()->getParentCategories() as $parent) {
                    if ($parent->getLevel() == $level) {
                        return $parent;
                    }
                }
            }
        }
        return null;
    }
}
