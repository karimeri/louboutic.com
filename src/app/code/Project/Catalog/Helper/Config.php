<?php

namespace Project\Catalog\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package Project\Catalog\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_CATALOG_LABEL_DISPLAYSOLDOUT = 'louboutin_catalog/label/display_sold_out';
    const XML_PATH_CATALOG_NAVIGATION_MAX_DEPTH = 'catalog/navigation/max_depth';

    const XML_PATH_BELTS_DISPLAY_DIMENSIONS = 'louboutin_belts/general/display_dimensions';

    /**
     * Get config of displaying sold out label or not
     * @param null $storeId
     * @return mixed
     */
    public function getCatalogDisplaySoldOutLabel($storeId = null)
    {
        return $this->scopeConfig->getValue(
            $this::XML_PATH_CATALOG_LABEL_DISPLAYSOLDOUT,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @return mixed
     */
    public function getMenuMaxDepth()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_CATALOG_NAVIGATION_MAX_DEPTH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function getDisplayDimensions()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_BELTS_DISPLAY_DIMENSIONS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
