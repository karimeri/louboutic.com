<?php

namespace Project\Catalog\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Repository;
use Project\Catalog\Model\Magento\ConfigurableProduct\Product\Type\Configurable;
use Project\PreOrder\Helper\Product as ProductHelper;

/**
 * Class Label
 * @package Project\Catalog\Helper
 * @author Synolia <contact@synolia.com>
 */
class Label extends AbstractHelper
{
    const PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE = 'online_exclusive';
    const PRODUCT_ATTRIBUTE_BACKINSTOCK      = 'back_in_stock';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Repository
     */
    protected $attributeRepository;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    protected $backInStockAttribute;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    protected $onlineExclusiveAttribute;

    /**
     * @var \Project\PreOrder\Helper\Product
     */
    protected $productHelper;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $stockState;

    /**
     * Label constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Project\Catalog\Helper\Config $config
     * @param \Magento\Catalog\Model\Product\Attribute\Repository $attributeRepository
     * @param \Project\PreOrder\Helper\Product $productHelper
     */
    public function __construct(
        Context $context,
        Config $config,
        Repository $attributeRepository,
        ProductHelper $productHelper,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState
    ) {
        parent::__construct($context);

        $this->config = $config;
        $this->attributeRepository = $attributeRepository;
        $this->productHelper = $productHelper;
        $this->stockState = $stockState;
    }

    /**
     * Get product label
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Framework\Phrase|null|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getLabel(Product $product)
    {

        return $this->getPreOrderLabel($product) ??
            $this->getBackInStockLabel($product) ??
            $this->getSoldOutLabel($product) ??
            $this->getOnlineExclusiveLabel($product);
    }

    /**
     * Get PreOrder product label
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Framework\Phrase|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function getPreOrderLabel(Product $product)
    {
        if ($this->productHelper->isPreOrderByType($product) && !$product->isSaleable()) {
            return __('Pre-order');
        }

        return null;
    }

    /**
     * Get Online Exclusive product label
     * @param Product $product
     * @return string
     */
    public function getOnlineExclusiveLabel(Product $product)
    {
        if ($product->getData($this::PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE) === '1') {
            $onlineExclusiveAttribute = $this->getOnlineExclusiveAttribute();

            if (!$onlineExclusiveAttribute) {
                return null;
            }

            return $onlineExclusiveAttribute->getStoreLabel();
        }

        return null;
    }

    /**
     * Get Sold Out product label
     * @param Product $product
     * @return string|null
     */
    public function getSoldOutLabel(Product $product)
    {
        if ($this->config->getCatalogDisplaySoldOutLabel() === '1' &&
            !$this->getProductQty($product)
        ) {
            return __('Sold out');
        }

        return null;
    }

    /**
     * Get Back in Stock product label
     * @param Product $product
     * @return string|null
     */
    public function getBackInStockLabel(Product $product)
    {
        if ($product->getData($this::PRODUCT_ATTRIBUTE_BACKINSTOCK) === '1' &&
            !$product->getStockStatus() &&
            !$this->getProductQty($product)
        ) {
            $backInStockAttribute = $this->getBackInStockAttribute();

            if (!$backInStockAttribute) {
                return null;
            }
            /**
             * Temporary fix, waiting to get the appropriate label for the store view FR
             */
            //return $backInStockAttribute->getStoreLabel();
            return __('Back in Stock');
        }

        return null;
    }

    /**
     * @param Product $product
     * @return float
     */
    protected function getProductQty($product)
    {
        if ($product->getTypeId() == Configurable::TYPE_CODE) {
            $product->getTypeInstance()->setSimpleProductsCount($product);
            $stockQty = $product->getSimpleProductsCount();
        } else {
            $stockQty = floatVal($this->stockState->getStockQty($product->getId()));
        }

        return $stockQty;
    }

    /**
     * @return \Magento\Eav\Model\Entity\Attribute|null
     */
    public function getBackInStockAttribute()
    {
        if (!$this->backInStockAttribute) {
            try {
                $this->backInStockAttribute = $this->attributeRepository->get($this::PRODUCT_ATTRIBUTE_BACKINSTOCK);
            } catch (NoSuchEntityException $exception) {
                return null;
            }
        }

        return $this->backInStockAttribute;
    }

    /**
     * @return \Magento\Eav\Model\Entity\Attribute|null
     */
    public function getOnlineExclusiveAttribute()
    {
        if (!$this->onlineExclusiveAttribute) {
            try {
                $this->onlineExclusiveAttribute =
                    $this->attributeRepository->get($this::PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE);
            } catch (NoSuchEntityException $exception) {
                return null;
            }
        }

        return $this->onlineExclusiveAttribute;
    }
}
