<?php

namespace Project\Catalog\Console\Command;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateProductStatus
 * @package Project\Catalog\Console\Command
 */
class UpdateProductStatus extends Command
{
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * UpdateProductStatus constructor.
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param StoreManagerInterface $storeManager
     * @param string|null $name
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('project:catalog:update-product-status')->setDescription('Update product status');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln("<info>Update product status ...</info>");
            /** @var ProductCollection $collection */
            $collection = $this->productCollectionFactory->create();
            /** @var \Magento\Catalog\Model\Product $item */
            foreach ($collection->getItems() as $item) {
                $sku = $item->getData('sku');
                $storeIds = [];
                $websiteIds = $item->getWebsiteIds();
                foreach ($websiteIds as $websiteId) {
                    $storeIds = array_merge($storeIds, $this->storeManager->getStoreByWebsiteId($websiteId));
                }
                $disableProduct = true;
                foreach ($storeIds as $storeId) {
                    $product = $this->productRepository->get($sku, false, $storeId);
                    if ($product->getStatus() == Status::STATUS_ENABLED) {
                        $disableProduct = false;
                    }
                }
                if ($disableProduct) {
                    // disable product globally if disabled on all stores
                    $item->addAttributeUpdate('status', Status::STATUS_DISABLED, Store::DEFAULT_STORE_ID);
                }
            }
            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
