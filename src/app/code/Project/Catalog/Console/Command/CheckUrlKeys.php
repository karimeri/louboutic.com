<?php

namespace Project\Catalog\Console\Command;

use Project\Catalog\Model\DataCheckManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckUrlKeys
 * @package Project\Catalog\Console\Command
 */
class CheckUrlKeys extends Command
{
    /**
     * @var DataCheckManager
     */
    protected $dataCheckManager;

    /**
     * CheckCustomerAddresses constructor.
     * @param DataCheckManager $dataCheckManager
     * @param string|null $name
     */
    public function __construct(
        DataCheckManager $dataCheckManager,
        ?string $name = null
    ) {
        $this->dataCheckManager = $dataCheckManager;
        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('project:catalog:check-url-keys')->setDescription('Check url keys');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln("<info>Check url keys ...</info>");

            $this->dataCheckManager->checkUrlKeys();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
