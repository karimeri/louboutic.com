<?php

namespace Project\Catalog\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\CatalogInventory\Model\ResourceModel\Stock\StatusFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

/**
 * Class AddStockInfoToProductCollection
 * @package Project\Catalog\Observer
 * @author Synolia <contact@synolia.com>
 */
class AddStockInfoToProductCollection implements ObserverInterface
{
    /**
     * @var StatusFactory
     */
    protected $stockStatusFactory;

    /**
     * @param StatusFactory $stockStatusFactory
     */
    public function __construct(StatusFactory $stockStatusFactory)
    {
        $this->stockStatusFactory = $stockStatusFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var ProductCollection $productCollection */
        $productCollection = $observer->getEvent()->getCollection();

        if ($productCollection instanceof ProductCollection
            && !$productCollection->hasFlag('has_stock_status_filter')) {
            $productCollection->getSelect()->columns('stock_status_index.qty');
        }

        return $this;
    }
}
