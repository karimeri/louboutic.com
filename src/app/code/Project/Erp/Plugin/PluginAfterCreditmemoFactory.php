<?php

namespace Project\Erp\Plugin;

use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\CreditmemoFactory;

/**
 * Class PluginAfterCreditmemoFactory
 *
 * @package Project\Erp\Plugin
 * @author  Synolia <contact@synolia.com>
 */
class PluginAfterCreditmemoFactory
{
    /**
     * @param CreditmemoFactory $subject
     * @param Creditmemo        $creditmemo
     *
     * @return Creditmemo
     */
    public function afterCreateByOrder(CreditmemoFactory $subject, $creditmemo)
    {
        return $this->setLineNumber($creditmemo);
    }

    /**
     * @param CreditmemoFactory $subject
     * @param Creditmemo        $creditmemo
     *
     * @return Creditmemo
     */
    public function afterCreateByInvoice(CreditmemoFactory $subject, $creditmemo)
    {
        return $this->setLineNumber($creditmemo);
    }

    /**
     * @param Creditmemo $creditmemo
     *
     * @return Creditmemo
     */
    protected function setLineNumber($creditmemo)
    {
        $lineNumber = 1;

        foreach ($creditmemo->getAllItems() as $item) {
            $item->setLineNumber($lineNumber);

            $lineNumber++;
        }

        return $creditmemo;
    }
}
