<?php

namespace Project\Erp\Plugin;

use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Service\InvoiceService;

/**
 * Class PluginAfterInvoiceService
 *
 * @package Project\Erp\Plugin
 * @author  Synolia <contact@synolia.com>
 */
class PluginAfterInvoiceService
{
    /**
     * @param InvoiceService $subject
     * @param Invoice        $invoice
     *
     * @return Invoice
     */
    public function afterPrepareInvoice(InvoiceService $subject, $invoice)
    {
        $lineNumber = 1;

        foreach ($invoice->getAllItems() as $item) {
            $item->setLineNumber($lineNumber);

            $lineNumber++;
        }

        return $invoice;
    }
}
