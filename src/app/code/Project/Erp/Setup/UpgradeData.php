<?php

namespace Project\Erp\Setup;

use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Project\Core\Manager\EnvironmentManager;

/**
 * Class UpgradeData
 *
 * @package Project\Erp\Setup
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeData implements UpgradeDataInterface
{
    const VERSIONS = [
        '1.0.0' => '100',
        '1.0.2' => '102',
    ];

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * UpgradeData constructor.
     *
     * @param ConsoleOutput      $output
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        ConsoleOutput $output,
        EnvironmentManager $environmentManager
    ) {
        $this->output        = $output;
        $this->objectManager = ObjectManager::getInstance();
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $setup->startSetup();

        $this->output->writeln(''); // new line in console

        foreach (self::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Erp setup : $version");

                $currentSetup = $this->getObjectManager()->create('Project\Erp\Setup\UpgradeData\Upgrade'.$fileData);
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ModuleDataSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return string
     * @throws \RuntimeException
     */
    public function getEnvironment()
    {
        return $this->environmentManager->getEnvironment();
    }
}
