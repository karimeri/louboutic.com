<?php

namespace Project\Erp\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class Upgrade100
 *
 * @package Project\Erp\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade100
{
    const TABLE_INVOICE     = 'sales_invoice';
    const TABLE_CREDIT_MEMO = 'sales_creditmemo';

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $columnExists = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_INVOICE),
            'exported'
        );

        if (!$columnExists) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_INVOICE),
                'exported',
                [
                    'type'     => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment'  => 'Exported',
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_CREDIT_MEMO),
                'exported',
                [
                    'type'     => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment'  => 'Exported',
                ]
            );
        }
    }
}
