<?php

namespace Project\Erp\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class Upgrade101
 *
 * @package Project\Erp\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade101
{
    const TABLE_INVOICE       = 'sales_invoice_item';
    const TABLE_CREDIT_MEMO   = 'sales_creditmemo_item';
    const COLUMN_LINE_NUMBER  = 'line_number';
    const COMMENT_LINE_NUMBER = 'Line Number';

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $columnExists = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_INVOICE),
            self::COLUMN_LINE_NUMBER
        );

        if (!$columnExists) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_INVOICE),
                self::COLUMN_LINE_NUMBER,
                [
                    'type'     => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment'  => self::COMMENT_LINE_NUMBER,
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_CREDIT_MEMO),
                self::COLUMN_LINE_NUMBER,
                [
                    'type'     => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment'  => self::COMMENT_LINE_NUMBER,
                ]
            );
        }
    }
}
