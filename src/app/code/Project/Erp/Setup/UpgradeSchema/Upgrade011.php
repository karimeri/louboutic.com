<?php

namespace Project\Erp\Setup\UpgradeSchema;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

use Project\Erp\Model\ResourceModel\ErpCustomerId;
use Project\Erp\Model\Repository\ErpCustomerIdGeneratedId;

/**
 * Class Upgrade011
 *
 * @package Project\Erp\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade011
{
    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->getConnection()->isTableExists($installer->getTable(ErpCustomerId::TABLE_NAME))) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable(ErpCustomerId::TABLE_NAME)
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Email'
            )->addColumn(
                'generated_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true, 'unsigned' => true],
                'Generated Id'
            );

            $installer->getConnection()->createTable($table);
        }

        if (!$installer->getConnection()->isTableExists($installer->getTable(ErpCustomerIdGeneratedId::TABLE_NAME))) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable(ErpCustomerIdGeneratedId::TABLE_NAME)
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            );

            $installer->getConnection()->createTable($table);
        }
    }
}
