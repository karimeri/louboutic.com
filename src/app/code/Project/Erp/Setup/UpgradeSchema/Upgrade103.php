<?php

namespace Project\Erp\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Upgrade102
 * @package Project\Erp\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class Upgrade103
{
    const TABLE_ORDER = 'sales_order';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $columnExists = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_ORDER),
            'address_exported'
        );

        if (!$columnExists) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_ORDER),
                'address_exported',
                [
                    'type' => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment' => 'Address Exported',
                ]
            );
        }
    }
}
