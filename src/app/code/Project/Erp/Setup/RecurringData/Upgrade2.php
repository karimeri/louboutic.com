<?php

namespace Project\Erp\Setup\RecurringData;

use Project\Erp\Block\Adminhtml\System\Config\ColumbusCivilityMappingArray;
use Project\Erp\Block\Adminhtml\System\Config\Y2TitlesMappingArray;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Upgrade2
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade2 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Upgrade19 constructor.
     *
     * @param ConfigSetup $configSetup
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        ConfigSetup $configSetup,
        SerializerInterface $serializer
    ) {
        $this->configSetup = $configSetup;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig(
            'synchronizations_columbus/customer/civility_mapping',
            $this->serializer->serialize(
                [
                    "Mr." =>
                        [
                            ColumbusCivilityMappingArray::CIVILITY => 'Mr.',
                            ColumbusCivilityMappingArray::CODE => '60010',
                        ],
                    "Mrs." =>
                        [
                            ColumbusCivilityMappingArray::CIVILITY => 'Mrs.',
                            ColumbusCivilityMappingArray::CODE => '70010',
                        ],
                    "Miss" =>
                        [
                            ColumbusCivilityMappingArray::CIVILITY => 'Miss',
                            ColumbusCivilityMappingArray::CODE => '200010',
                        ],
                ]
            )
        );

        $this->configSetup->saveConfig(
            'synchronizations_addresses/mappings/y2_titles',
            $this->serializer->serialize(
                [
                    "Mr." =>
                        [
                            Y2TitlesMappingArray::MAGENTO_CODE => 'Mr.',
                            Y2TitlesMappingArray::ERP_CODE => '001',
                            Y2TitlesMappingArray::GENDER_CODE => 'M',
                        ],
                    "Mrs." =>
                        [
                            Y2TitlesMappingArray::MAGENTO_CODE => 'Mrs.',
                            Y2TitlesMappingArray::ERP_CODE => 'MRS',
                            Y2TitlesMappingArray::GENDER_CODE => 'F',
                        ],
                    "Miss" =>
                        [
                            Y2TitlesMappingArray::MAGENTO_CODE => 'Miss',
                            Y2TitlesMappingArray::ERP_CODE => '005',
                            Y2TitlesMappingArray::GENDER_CODE => 'F',
                        ],
                ]
            )
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Config for columbus flows';
    }
}
