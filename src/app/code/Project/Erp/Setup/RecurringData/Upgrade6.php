<?php

namespace Project\Erp\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Framework\App\ResourceConnection;
use Project\Sales\Helper\Config;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class Upgrade6
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade6 implements UpgradeDataSetupInterface
{
    const TABLE_CREDITMEMO = 'sales_creditmemo';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade6 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_CREDITMEMO),
            Config::FIELD_AJUSTMENT_TAX_RATE,
            [
                'type'     => Table::TYPE_DECIMAL,
                'length'   => '12,4',
                'nullable' => true,
                'comment'  => 'Adjustment tax rate',
            ]
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Add adjustment tax rate column to creditmemo';
    }
}
