<?php

namespace Project\Erp\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class Upgrade4
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade4 implements UpgradeDataSetupInterface
{
    const TABLE_INVOICE = 'sales_invoice';
    const TABLE_CREDIT_MEMO = 'sales_creditmemo';

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * Upgrade4 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnExists = $this->resource->getConnection()->tableColumnExists(
            $this->resource->getTableName(self::TABLE_INVOICE),
            'payment_exported'
        );

        if (!$columnExists) {
            $this->resource->getConnection()->addColumn(
                $this->resource->getTableName(self::TABLE_INVOICE),
                'payment_exported',
                [
                    'type' => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment' => 'Exported',
                ]
            );

            $this->resource->getConnection()->addColumn(
                $this->resource->getTableName(self::TABLE_CREDIT_MEMO),
                'payment_exported',
                [
                    'type' => Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'comment' => 'Exported',
                ]
            );
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Y2 export payment column';
    }
}
