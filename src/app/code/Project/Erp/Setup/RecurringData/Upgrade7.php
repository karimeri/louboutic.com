<?php

namespace Project\Erp\Setup\RecurringData;

use Project\Erp\Model\Action\SetInvoiceCreditExportedData;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class Upgrade7
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade7 implements UpgradeDataSetupInterface
{

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade7 constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(SetInvoiceCreditExportedData::TABLE_CREDITMEMO),
            'customer_exported',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => true,
                'comment' => 'Customer Exported',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(SetInvoiceCreditExportedData::TABLE_CREDITMEMO),
            'address_exported',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => true,
                'comment' => 'Address Exported',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(SetInvoiceCreditExportedData::TABLE_INVOICE),
            'customer_exported',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => true,
                'comment' => 'Customer Exported',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(SetInvoiceCreditExportedData::TABLE_INVOICE),
            'address_exported',
            [
                'type' => Table::TYPE_SMALLINT,
                'nullable' => true,
                'comment' => 'Address Exported',
            ]
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Add customer_exported and address_exported to creditmemo and invoice';
    }
}
