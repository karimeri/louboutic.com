<?php

namespace Project\Erp\Setup\RecurringData;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Tax\Model\Config;

/**
 * Class Upgrade5
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade5 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Upgrade5 constructor.
     *
     * @param ConfigSetup $configSetup
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ConfigSetup $configSetup,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->configSetup = $configSetup;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig(
            \Project\Erp\Helper\Config::XML_PATH_CREDITMEMO_ADJUST_TAX_CLASS,
            $this->scopeConfig->getValue(Config::CONFIG_XML_PATH_SHIPPING_TAX_CLASS)
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Config for credit memo tax in y2 flows';
    }
}
