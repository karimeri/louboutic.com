<?php

namespace Project\Erp\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Upgrade1
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Upgrade19 constructor.
     *
     * @param ConfigSetup $configSetup
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        ConfigSetup $configSetup,
        SerializerInterface $serializer
    ) {
        $this->configSetup = $configSetup;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig(
            'synchronizations_columbus/sales_ticket/invoice_type_mapping',
            $this->serializer->serialize([
                [
                    'amount_min' => '0',
                    'amount_max' => '0',
                    'code' => 'HK',
                ],
                [
                    'amount_min' => '0',
                    'amount_max' => '0',
                    'code' => 'TW',
                ],
                [
                    'amount_min' => '0',
                    'amount_max' => '0',
                    'code' => 'SG',
                ],
                [
                    'amount_min' => '0',
                    'amount_max' => '0',
                    'code' => 'MY',
                ],
                [
                    'amount_min' => '0',
                    'amount_max' => '0',
                    'code' => 'JP',
                ],
                [
                    'amount_min' => '0',
                    'amount_max' => '999',
                    'code' => 'AUD',
                ],
                [
                    'amount_min' => '1000',
                    'amount_max' => '0',
                    'code' => 'A1',
                ],
            ])
        );
        $this->configSetup->saveConfig(
            'synchronizations_columbus/sales_ticket/price_code_mapping',
            $this->serializer->serialize([
                [
                    'currency_code' => 'HKD',
                    'code' => 'RDHK',
                ],
                [
                    'currency_code' => 'TWD',
                    'code' => 'RDTW',
                ],
                [
                    'currency_code' => 'SGD',
                    'code' => 'RDSG',
                ],
                [
                    'currency_code' => 'MYR',
                    'code' => 'RDMY',
                ],
                [
                    'currency_code' => 'JPY',
                    'code' => 'RDJP',
                ],
                [
                    'currency_code' => 'AUD',
                    'code' => 'RDAU',
                ],
            ])
        );
        $this->configSetup->saveConfig(
            'synchronizations_columbus/customer/civility_mapping',
            $this->serializer->serialize([
                [
                    'civility' => 'MR',
                    'code' => '60010',
                ],
                [
                    'civility' => 'MRS',
                    'code' => '70010',
                ],
                [
                    'civility' => 'MISS',
                    'code' => '20010',
                ],
            ])
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Config for columbus flows';
    }
}
