<?php

namespace Project\Erp\Setup\RecurringData;

use Magento\Store\Model\WebsiteRepository;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Upgrade3
 * @package Project\Erp\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    const HK_VALUES = [
        [
            'amount_min' => '0',
            'amount_max' => '0',
            'code' => 'HK',
        ],
    ];

    const TW_VALUES = [
        [
            'amount_min' => '0',
            'amount_max' => '0',
            'code' => 'TW',
        ],
    ];

    const SG_VALUES = [
        [
            'amount_min' => '0',
            'amount_max' => '0',
            'code' => 'SG',
        ],
    ];

    const MY_VALUES = [
        [
            'amount_min' => '0',
            'amount_max' => '0',
            'code' => 'MY',
        ],
    ];

    const JP_VALUES = [
        [
            'amount_min' => '0',
            'amount_max' => '0',
            'code' => 'JP',
        ],
    ];

    const AU_VALUES = [
        [
            'amount_min' => '0',
            'amount_max' => '999',
            'code' => 'AUD',
        ],
        [
            'amount_min' => '1000',
            'amount_max' => '0',
            'code' => 'A1',
        ],
    ];

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Store\Model\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * Upgrade3 constructor.
     *
     * @param ConfigSetup $configSetup
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Store\Model\WebsiteRepository $websiteRepository
     */
    public function __construct(
        ConfigSetup $configSetup,
        SerializerInterface $serializer,
        EnvironmentManager $environmentManager,
        WebsiteRepository $websiteRepository
    ) {
        $this->configSetup = $configSetup;
        $this->serializer = $serializer;
        $this->environmentManager = $environmentManager;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() === Environment::HK) {
            $this->configSetup->saveConfig(
                'synchronizations_columbus/sales_ticket/invoice_type_mapping',
                $this->serializer->serialize(self::AU_VALUES),
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $this->websiteRepository->get('au')->getId()
            );
            $this->configSetup->saveConfig(
                'synchronizations_columbus/sales_ticket/invoice_type_mapping',
                $this->serializer->serialize(self::MY_VALUES),
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $this->websiteRepository->get('my')->getId()
            );
            $this->configSetup->saveConfig(
                'synchronizations_columbus/sales_ticket/invoice_type_mapping',
                $this->serializer->serialize(self::SG_VALUES),
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $this->websiteRepository->get('sg')->getId()
            );
            $this->configSetup->saveConfig(
                'synchronizations_columbus/sales_ticket/invoice_type_mapping',
                $this->serializer->serialize(self::TW_VALUES),
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $this->websiteRepository->get('tw')->getId()
            );
            $this->configSetup->saveConfig(
                'synchronizations_columbus/sales_ticket/invoice_type_mapping',
                $this->serializer->serialize(self::HK_VALUES),
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $this->websiteRepository->get('hk')->getId()
            );
        } elseif ($this->environmentManager->getEnvironment() === Environment::JP) {
            $this->configSetup->saveConfig(
                'synchronizations_columbus/sales_ticket/invoice_type_mapping',
                $this->serializer->serialize(self::JP_VALUES),
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $this->websiteRepository->get('jp')->getId()
            );
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Config for columbus flows';
    }
}
