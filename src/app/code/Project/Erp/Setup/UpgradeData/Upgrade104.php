<?php

namespace Project\Erp\Setup\UpgradeData;

use Magento\Framework\Serialize\SerializerInterface;
use Project\Erp\Block\Adminhtml\System\Config\ColumbusCivilityMappingArray;
use Project\Erp\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Eav\ConfigSetupFactory;

/**
 * Class Upgrade102
 * @package Project\Erp\Setup\UpgradeData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade104
{
    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * Upgrade104 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetupFactory $configSetupFactory
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        ConfigSetupFactory $configSetupFactory,
        SerializerInterface $serializer
    ) {
        $this->configSetupFactory = $configSetupFactory;
        $this->serializer = $serializer;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     *
     * @throws \RuntimeException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        /** @var ConfigSetup $configSetup */
        $configSetup = $this->configSetupFactory->create();

        // Columbus Civility Mapping Config
        $configSetup->saveConfig(
            'synchronizations_columbus/customer/civility_mapping',
            $this->serializer->serialize(
                [
                    "MR" =>
                        [
                            ColumbusCivilityMappingArray::CIVILITY => 'MR',
                            ColumbusCivilityMappingArray::CODE => '60010',
                        ],
                    "MRS" =>
                        [
                            ColumbusCivilityMappingArray::CIVILITY => 'MRS',
                            ColumbusCivilityMappingArray::CODE => '70010',
                        ],
                    "MISS" =>
                        [
                            ColumbusCivilityMappingArray::CIVILITY => 'MISS',
                            ColumbusCivilityMappingArray::CODE => '200010',
                        ],
                ]
            )
        );
    }
}
