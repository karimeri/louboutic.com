<?php

namespace Project\Erp\Setup\UpgradeData;

use Magento\Framework\Serialize\SerializerInterface;
use Project\Erp\Block\Adminhtml\System\Config\Y2TitlesMappingArray;
use Project\Erp\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Eav\ConfigSetupFactory;

/**
 * Class Upgrade102
 * @package Project\Erp\Setup\UpgradeData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade102
{
    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * Upgrade102 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetupFactory $configSetupFactory
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        ConfigSetupFactory $configSetupFactory,
        SerializerInterface $serializer
    ) {
        $this->configSetupFactory = $configSetupFactory;
        $this->serializer = $serializer;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     *
     * @throws \RuntimeException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        /** @var ConfigSetup $configSetup */
        $configSetup = $this->configSetupFactory->create();

        // Y2 Title and Gender Mapping Config
        $configSetup->saveConfig(
            'synchronizations_addresses/mappings/y2_titles',
            $this->serializer->serialize(
                [
                    "MR" =>
                        [
                            Y2TitlesMappingArray::MAGENTO_CODE => 'MR',
                            Y2TitlesMappingArray::ERP_CODE => '001',
                            Y2TitlesMappingArray::GENDER_CODE => 'M',
                        ],
                    "MRS" =>
                        [
                            Y2TitlesMappingArray::MAGENTO_CODE => 'MRS',
                            Y2TitlesMappingArray::ERP_CODE => 'MRS',
                            Y2TitlesMappingArray::GENDER_CODE => 'F',
                        ],
                    "MS" =>
                        [
                            Y2TitlesMappingArray::MAGENTO_CODE => 'MS',
                            Y2TitlesMappingArray::ERP_CODE => '005',
                            Y2TitlesMappingArray::GENDER_CODE => 'F',
                        ],
                ]
            )
        );
    }
}
