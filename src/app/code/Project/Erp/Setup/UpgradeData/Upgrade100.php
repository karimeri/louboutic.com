<?php

namespace Project\Erp\Setup\UpgradeData;

use Project\Erp\Setup\UpgradeData;

use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Eav\ConfigSetupFactory;

/**
 * Class Upgrade100
 *
 * @package Project\Erp\Setup\UpgradeData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade100
{
    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    public function __construct(
        ConfigSetupFactory $configSetupFactory
    ) {
        $this->configSetupFactory = $configSetupFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     *
     * @throws \RuntimeException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        if ($upgradeDataObject->getEnvironment() === 'hk') {
            $societyValue = 'CLHK';
            $vendorValue  = 'HK999';
        } elseif ($upgradeDataObject->getEnvironment() === 'jp') {
            $societyValue = 'CLJP';
            $vendorValue  = 'JP999';
        } else {
            return;
        }

        /** @var ConfigSetup $configSetup */
        $configSetup = $this->configSetupFactory->create();

        $configSetup->saveConfig(
            'synchronizations_columbus/general/society_code',
            $societyValue
        );

        $configSetup->saveConfig(
            'synchronizations_columbus/general/vendor_code',
            $vendorValue
        );
    }
}
