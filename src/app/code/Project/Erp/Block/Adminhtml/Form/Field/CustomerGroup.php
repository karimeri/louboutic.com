<?php

namespace Project\Erp\Block\Adminhtml\Form\Field;

use Magento\Customer\Model\Config\Source\Group;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class PaymentMethods
 * @package HappyTech\Sync\Block\Adminhtml\Form\Field
 */
class CustomerGroup extends Select
{
    const GUEST_GROUP_ID = 'guest';
    /**
     * @var \Magento\Customer\Model\Config\Source\Group
     */
    protected $groupSource;

    /**
     * CustomerGroup constructor.
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Customer\Model\Config\Source\Group $group
     * @param array $data
     */
    public function __construct(
        Context $context,
        Group $group,
        array $data = []
    ) {
        $this->groupSource = $group;
        parent::__construct($context, $data);
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    //phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    public function _beforeToHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->groupSource->toOptionArray());
            $this->addOption(self::GUEST_GROUP_ID, __('Guest'));
        }

        return parent::_beforeToHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
