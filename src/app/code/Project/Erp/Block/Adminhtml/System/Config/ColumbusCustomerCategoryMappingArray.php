<?php

namespace Project\Erp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Project\Erp\Block\Adminhtml\Form\Field\CustomerGroup;

/**
 * Class AbstractFieldMapping
 * @package HappyTech\Sync\Block\Adminhtml\System\Config\Form\Field
 * @author Synolia <contact@synolia.com>
 */
class ColumbusCustomerCategoryMappingArray extends AbstractFieldArray
{
    const GROUP_ID = 'groupId';
    const CODE = 'code';
    /**
     * @var \Magento\Framework\View\Element\Html\Select
     */
    protected $renderer;

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    //phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    protected function _prepareToRender()
    {
        $this->addColumn(
            self::GROUP_ID,
            [
                'label' => __('Customer Group'),
                'renderer' => $this->getRenderer(),
            ]
        );
        $this->addColumn(self::CODE, ['label' => __('Code')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @return \Magento\Framework\View\Element\Html\Select
     */
    protected function getRenderer()
    {
        if (!$this->renderer) {
            $this->renderer = $this->_layout->createBlock(
                CustomerGroup::class,
                '',
                [
                    'data' => [
                        'is_render_to_js_template' => true,
                    ],
                ]
            );
        }

        return $this->renderer;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    //phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    protected function _prepareArrayRow(DataObject $row)
    {
        $options = [];

        if ($groupId = $row->getData(self::GROUP_ID)) {
            $options['option_' . $this->getRenderer()->calcOptionHash($groupId)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);
    }
}
