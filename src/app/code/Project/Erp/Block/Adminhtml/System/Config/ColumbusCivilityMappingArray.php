<?php

namespace Project\Erp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect;

/**
 * Class ColumbusCivilityMappingArray
 * @package Project\Erp\Block\Adminhtml\System\Config
 * @author Synolia <contact@synolia.com>
 */
class ColumbusCivilityMappingArray extends AbstractFieldArray
{
    const CIVILITY = 'civility';
    const CODE = 'code';

    /**
     * @inheritdoc
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _prepareToRender()
    {
        $this->addColumn('civility', ['label' => __('Civility'), 'renderer' => Textselect::class]);
        $this->addColumn('code', ['label' => __('Code'), 'renderer' => Textselect::class]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}
