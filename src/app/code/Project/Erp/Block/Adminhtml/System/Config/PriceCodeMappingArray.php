<?php

namespace Project\Erp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect;

/**
 * Class PriceCodeMappingArray
 *
 * @package Project\Erp\Block\Adminhtml\System\Config
 * @author  Synolia <contact@synolia.com>
 */
class PriceCodeMappingArray extends AbstractFieldArray
{
    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _prepareToRender()
    {
        $this->addColumn('currency_code', ['label' => __('Currency Code'), 'renderer' => Textselect::class]);
        $this->addColumn('code', ['label' => __('Code'), 'renderer' => Textselect::class]);

        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');
    }
}
