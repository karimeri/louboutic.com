<?php

namespace Project\Erp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect;

/**
 * Class InvoiceTypeMappingArray
 *
 * @package Project\Erp\Block\Adminhtml\System\Config
 * @author  Synolia <contact@synolia.com>
 */
class InvoiceTypeMappingArray extends AbstractFieldArray
{
    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _prepareToRender()
    {
        $this->addColumn('amount_min', ['label' => __('Amount min'), 'renderer' => Textselect::class]);
        $this->addColumn('amount_max', ['label' => __('Amount max'), 'renderer' => Textselect::class]);
        $this->addColumn('code', ['label' => __('Code'), 'renderer' => Textselect::class]);

        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');
    }
}
