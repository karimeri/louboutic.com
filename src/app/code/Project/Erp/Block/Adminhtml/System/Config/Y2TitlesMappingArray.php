<?php

namespace Project\Erp\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect;

/**
 * Class GenderMappingArray
 *
 * @package Project\Erp\Block\Adminhtml\System\Config
 * @author Synolia <contact@synolia.com>
 */
class Y2TitlesMappingArray extends AbstractFieldArray
{
    const MAGENTO_CODE = 'magento_code';
    const ERP_CODE = 'erp_code';
    const GENDER_CODE = 'gender_code';

    /**
     * @inheritdoc
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _prepareToRender()
    {
        $this->addColumn(self::MAGENTO_CODE, ['label' => __('Magento Code'), 'renderer' => Textselect::class]);
        $this->addColumn(self::ERP_CODE, ['label' => __('ERP Code'), 'renderer' => Textselect::class]);
        $this->addColumn(self::GENDER_CODE, ['label' => __('Gender Code'), 'renderer' => Textselect::class]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}
