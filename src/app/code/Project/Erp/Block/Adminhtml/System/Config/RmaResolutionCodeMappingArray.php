<?php

namespace Project\Erp\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\Factory;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Helper\Eav;

/**
 * Class RmaResolutionCodeMappingArray
 * @package Project\Erp\Block\Adminhtml\System\Config
 * @author  Synolia <contact@synolia.com>
 */
class RmaResolutionCodeMappingArray extends AbstractFieldArray
{
    const ATTRIBUTE_RESOLUTION = 'resolution';

    /**
     * @var Eav
     */
    protected $rmaHelper;

    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Rma\Helper\Eav $rmaHelper
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        Eav $rmaHelper,
        Factory $elementFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->rmaHelper = $rmaHelper;
        $this->elementFactory = $elementFactory;
    }

    /**
     * Initialise form fields
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore
    protected function _construct()
    {
        $this->addColumn('resolution', ['label' => __('Resolution')]);
        $this->addColumn('code', ['label' => __('Code')]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * @param string $columnName
     *
     * @return string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName === 'resolution' && isset($this->_columns[$columnName])) {
            $options = $this->getOptions();

            $element = $this->elementFactory->create('select');
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                $options
            );

            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        $options = [];

        foreach ($this->rmaHelper->getAttributeOptionValues(self::ATTRIBUTE_RESOLUTION) as $key => $value) {
            $options[$key] = $value;
        }

        return $options;
    }
}
