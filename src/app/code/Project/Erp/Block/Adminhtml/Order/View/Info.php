<?php

namespace Project\Erp\Block\Adminhtml\Order\View;

use Magento\Sales\Block\Adminhtml\Order\View\Info as BaseInfo;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Sales\Helper\Admin;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Model\Metadata\ElementFactory;
use Magento\Sales\Model\Order\Address\Renderer;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;

/**
 * Class Info
 * @package Project\Erp\Block\Adminhtml\Order\View
 */
class Info extends BaseInfo
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var WmsHelper
     */
    protected $wmsHelper;

    /**
     * Info constructor.
     * @param Helper $helper
     * @param WmsHelper $wmsHelper
     * @param Context $context
     * @param Registry $registry
     * @param Admin $adminHelper
     * @param GroupRepositoryInterface $groupRepository
     * @param CustomerMetadataInterface $metadata
     * @param ElementFactory $elementFactory
     * @param Renderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        Context $context,
        Registry $registry,
        Admin $adminHelper,
        GroupRepositoryInterface $groupRepository,
        CustomerMetadataInterface $metadata,
        ElementFactory $elementFactory,
        Renderer $addressRenderer,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $adminHelper,
            $groupRepository,
            $metadata,
            $elementFactory,
            $addressRenderer,
            $data
        );

        $this->helper = $helper;
        $this->wmsHelper = $wmsHelper;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getIdentifierErp()
    {
        $identifier = "";
        if ($this->getOrder()) {
            $identifier = $this->helper->getCustomerIdentifierCode(
                $this->getOrder()->getCustomerEmail(),
                $this->wmsHelper->getEstablishmentCode($this->getOrder()->getStore()->getWebsiteId())
            );
        }

        return $identifier;
    }
}
