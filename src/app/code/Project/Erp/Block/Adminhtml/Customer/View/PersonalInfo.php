<?php

namespace Project\Erp\Block\Adminhtml\Customer\View;

use Magento\Customer\Block\Adminhtml\Edit\Tab\View\PersonalInfo as BasePersonalInfo;
use Magento\Backend\Block\Template\Context;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Helper\Address;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Registry;
use Magento\Customer\Model\Address\Mapper;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Customer\Model\Logger;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;

/**
 * Class PersonalInfo
 * @package Project\Erp\Block\Adminhtml\Customer\View
 */
class PersonalInfo extends BasePersonalInfo
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var WmsHelper
     */
    protected $wmsHelper;

    /**
     * PersonalInfo constructor.
     * @param Helper $helper
     * @param WmsHelper $wmsHelper
     * @param Context $context
     * @param AccountManagementInterface $accountManagement
     * @param GroupRepositoryInterface $groupRepository
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param Address $addressHelper
     * @param DateTime $dateTime
     * @param Registry $registry
     * @param Mapper $addressMapper
     * @param DataObjectHelper $dataObjectHelper
     * @param Logger $customerLogger
     * @param array $data
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        Context $context,
        AccountManagementInterface $accountManagement,
        GroupRepositoryInterface $groupRepository,
        CustomerInterfaceFactory $customerDataFactory,
        Address $addressHelper,
        DateTime $dateTime,
        Registry $registry,
        Mapper $addressMapper,
        DataObjectHelper $dataObjectHelper,
        Logger $customerLogger,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $accountManagement,
            $groupRepository,
            $customerDataFactory,
            $addressHelper,
            $dateTime,
            $registry,
            $addressMapper,
            $dataObjectHelper,
            $customerLogger,
            $data
        );
        $this->helper = $helper;
        $this->wmsHelper = $wmsHelper;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getIdentifierErp()
    {
        $identifier = "";
        if ($this->getCustomer()) {
            $identifier = $this->helper->getCustomerIdentifierCode(
                $this->getCustomer()->getEmail(),
                $this->wmsHelper->getEstablishmentCode($this->getCustomer()->getWebsiteId())
            );
        }

        return $identifier;
    }
}
