<?php

namespace Project\Erp\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class TaxRegimeCodeType
 *
 * @package Project\Erp\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class TaxRegimeCodeType implements ArrayInterface
{
    const COUNTRY_NAME   = 'country_name';
    const STATE_CODE     = 'state_code';
    const COUNTRY_CODE_1 = 'country_code_1';
    const SPECIFIC_VALUE = 'specific_value';
    const EMPTY_VALUE    = 'empty_value';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::COUNTRY_NAME, 'label' => __('Country Name')],
            ['value' => self::STATE_CODE, 'label' => __('State Code')],
            ['value' => self::COUNTRY_CODE_1, 'label' => __('Country Code 1')],
            ['value' => self::SPECIFIC_VALUE, 'label' => __('Specific Value')],
            ['value' => self::EMPTY_VALUE, 'label' => __('Empty Value')],
        ];
    }
}
