<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Store\Model\StoreRepository;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\ObjectManager\ConfigLoaderInterface;

use Project\Erp\Helper\Config;
use Project\Erp\Helper\Config\Report\Sales as HelperReportSales;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class SendExportReportMail
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class SendExportReportMail implements ActionInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var Config
     */
    protected $helperConfig;

    /**
     * @var HelperReportSales
     */
    protected $helperReportSales;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * SendExportReportEmail constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param ObjectManagerInterface $objectManager
     * @param ConfigLoaderInterface $configLoader
     * @param State $state
     * @param Config $helperConfig
     * @param HelperReportSales $helperReportSales
     * @param StoreRepository $storeRepository
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        ObjectManagerInterface $objectManager,
        ConfigLoaderInterface $configLoader,
        State $state,
        Config $helperConfig,
        HelperReportSales $helperReportSales,
        StoreRepository $storeRepository
    ) {
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->helperConfig = $helperConfig;
        $this->helperReportSales = $helperReportSales;
        $this->storeRepository = $storeRepository;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
        $objectManager->configure($configLoader->load('adminhtml'));
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\MailException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $templateOptions = ['area' => Area::AREA_ADMINHTML, 'store' => $this->storeManager->getStore()->getId()];
        $templateVars = [
            'invoiceData' => $data[$params['invoiceData']],
            'creditmemoData' => $data[$params['creditmemoData']],
            'helperConfig' => $this->helperConfig,
            'helperReportSales' => $this->helperReportSales,
        ];

        $from = $this->helperReportSales->getExportSender();

        $to = $this->getTo();

        $this->inlineTranslation->suspend();

        $transport = $this->transportBuilder->setTemplateIdentifier($params['template_identifier'])
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom($from)
            ->addTo($to)
            ->getTransport();
        $transport->sendMessage();

        $this->inlineTranslation->resume();

        return $data;
    }

    protected function getTo()
    {
        $to = [];
        foreach ($this->storeRepository->getList() as $store) {
            $to = array_merge($to, $this->helperReportSales->getExportMails($store->getId()));
        }

        return array_unique($to);
    }
}
