<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ErpAction
 * @package Project\Erp\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class ErpAction
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var WmsHelper
     */
    protected $wmsHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * GetY2ExportCustomerData constructor.
     *
     * @param \Project\Erp\Helper\ErpCustomerId $helper
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone
    ) {
        $this->helper = $helper;
        $this->wmsHelper = $wmsHelper;
        $this->timezone = $timezone;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     */
    protected function getCustomerIdentifierCode($order)
    {
        try {
            return $this->helper->getCustomerIdentifierCode(
                $order->getCustomerEmail(),
                $this->wmsHelper->getEstablishmentCode($order->getStore()->getWebsiteId())
            );
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param string $string
     * @param int $length
     *
     * @return string
     */
    protected function upperTrunc($string, $length)
    {
        return mb_strtoupper(mb_substr($string, 0, $length));
    }

    /**
     * @param string $timestamp
     * @param string $format
     * @param int|string $storeId
     *
     * @return string
     */
    protected function convertDateTime($timestamp, $format, $storeId)
    {
        $storeTimezone = $this->timezone->getConfigTimezone(
            ScopeInterface::SCOPE_STORES,
            $storeId
        );
        $dateTime = new \DateTime($timestamp, new \DateTimeZone($this->timezone->getDefaultTimezone()));
        $dateTime->setTimezone(new \DateTimeZone($storeTimezone));

        return $dateTime->format($format);
    }
}
