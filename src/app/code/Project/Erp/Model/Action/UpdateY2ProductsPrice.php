<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Erp\Helper\Config;
use Psr\Log\LoggerInterface;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Project\Erp\Model\Repository\ProductPriceRepository;
use Magento\Eav\Model\AttributeRepository;
use Magento\Catalog\Model\Product;

/**
 * Class UpdateY2ProductsPrice
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class UpdateY2ProductsPrice implements ActionInterface {

    const AUTHORIZED_INSTANCES = [Environment::US, Environment::EU];

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var int
     */
    protected $styleCodeKey = 0;

    /**
     * @var \Project\Erp\Model\Repository\ProductPriceRepository
     */
    protected $productPriceRepository;

    /**
     * @var \Magento\Eav\Model\AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var \Project\Erp\Helper\Config
     */
    protected $helper;

    /**
     * @param State $state
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Erp\Model\Repository\ProductPriceRepository $productPriceRepository
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     * @param \Project\Erp\Helper\Config $helper
     */
    public function __construct(
            State $state,
            StoreManagerInterface $storeManager,
            LoggerInterface $logger,
            EnvironmentManager $environmentManager,
            ProductPriceRepository $productPriceRepository,
            AttributeRepository $attributeRepository,
            Config $helper
    ) {
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->environmentManager = $environmentManager;
        $this->productPriceRepository = $productPriceRepository;
        $this->attributeRepository = $attributeRepository;
        $this->helper = $helper;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
            array $params,
            array $data,
            $flowCode,
            ConsoleOutput $consoleOutput
    ) {
        $environment = $this->environmentManager->getEnvironment();
        if (!in_array($environment, self::AUTHORIZED_INSTANCES, true)) {
            throw new \RuntimeException('Environment ' . $environment . ' is not supported');
        }

        $fileData = $data[$params['key']];
        $attributes = array_column($fileData, $this->styleCodeKey);
        $attributeStyleCode = $this->attributeRepository->get(Product::ENTITY, 'style_code');
        $attributeStatus = $this->attributeRepository->get(Product::ENTITY, 'status');
        $attributeSearchableSkuId = $this->attributeRepository->get(
                Product::ENTITY,
                'searchable_sku'
        );
        $attributePrice = $this->attributeRepository->get(Product::ENTITY, 'price');
        $rowIds = $this->productPriceRepository->getRowIdsByStyleCodes(
                $attributes,
                $attributeStyleCode->getAttributeId(),
                $attributeStatus->getAttributeId()
        );

        $data['products'] = [];
        $data['mailData'] = [];
        $mailData = [];
        $insertData = [];

        $defaultStoreCode = $this->helper->getBaseCurrency();
        $defaultStoreId = $this->storeManager->getStore('admin')->getId();

        $insertCounter = 0;
        $stores = $this->storeManager->getStores();
        $totalstores = count($stores);
        $countstore = 0;
        $mem_usage = memory_get_usage(true);
        echo round($mem_usage / 1048576, 2) . " MB START SCRIPT \n";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $connection->getTableName("catalog_product_entity");
        $tableNameRef = $connection->getTableName("catalog_product_entity_varchar");
        $tablePriceName = $connection->getTableName("catalog_product_entity_decimal");
        $tableProductRelation = $connection->getTableName("catalog_product_relation");

        $productMissing = 0;
        $totalProduct = count($fileData);
        $products = array();
        $currentProductCount = 0;
        foreach ($fileData as $productData) {
            $currentProductCount++;
            if (count($productData) != 7) {
                echo "error ligne mal formated \n";
                continue;
            }
            $product['SKU'] = $productData[0];
            $product['EUR'] = $productData[2];
            $product['CHF'] = $productData[4];
            $product['GBP'] = $productData[6];

            $query = 'SELECT entity_id, type_id  FROM ' . $tableName . " where sku like '%" . $product['SKU'] . "%'";

            $query = 'SELECT * FROM ' . $tableNameRef . ' where attribute_id = '.$attributeSearchableSkuId->getAttributeId().' and value like "%' . $product['SKU'] . '%";';
            $productsId = $connection->fetchAll($query);

            if (!$productsId) {
                $productMissing++;
                echo "Product missing from database M2 [" . $currentProductCount . " / $totalProduct ]: SKU [" . $product['SKU'] . "] \n";
                continue;
            }
            foreach ($productsId as $productId) {
                /*
                 * Rule = update simple products from configurable
                  if ($productId['type_id'] == "configurable") {
                  $querySimpleProducts = 'SELECT * FROM ' . $tableProductRelation . " where parent_id = " . $productId['row_id'];
                  $simpleProducts = $connection->fetchAll($querySimpleProducts);
                  foreach ($simpleProducts as $simpleProduct) {
                  $product['ENTITY_ID'] = (int) $simpleProduct['child_id'];
                  $products[] = $product;
                  }
                  } */
                $product['ENTITY_ID'] = (int) $productId['row_id'];
                $products[] = $product;
            }
        }


        $totalProduct = count($products);
        echo "products found ->" . ((int) $currentProductCount - (int) $productMissing) . " / " . $totalProduct . " ->  Configurables + Declinaisons =" . $totalProduct . " \n";
        echo "$productMissing products missing \n";
        sleep(2);
        $currentProductCount = 0;
        $sql2insert = "";

        foreach ($products as $key => $product) {
            $mem_usage = memory_get_usage(true);
            $req = $connection->prepare("update  " . $tablePriceName . " set value = :price "
                    . " WHERE attribute_id = :attributePrice "
                    . " AND row_id = :entity_id "
                    . " AND store_id = :storeid ");
            echo round($mem_usage / 1048576, 2) . " Memory - productId -> [" . $product['ENTITY_ID'] . "] [" . ($currentProductCount++) . "/" . ($totalProduct - 1) . "] " . $product['SKU'] . " \n";
            $firstStore = 0;
            foreach ($stores as $store) {
                $storeCode = $store->getCurrentCurrency()->getCode();
                if (!isset($product[$storeCode])) {
                    echo "DEVISE MISSING FROM FILE";
                    continue;
                }
                $price = $product[$storeCode];
                //(p_store_id int,p_row_id int,p_attribute_id int,p_price decimal)
                $sql = "call update_prices(" . $store->getId() . "," . $product['ENTITY_ID'] . "," . $attributePrice->getAttributeId() . "," . $price . ");";
                $connection->query($sql);
                if ($firstStore == 0) {
                    $sql = "call update_prices(0," . $product['ENTITY_ID'] . "," . $attributePrice->getAttributeId() . "," . $price . ");";
                    $connection->query($sql);
                    $firstStore = 1;
                }
            }
        }
        return $data;


        /*
         * Il ne faut pas supprimer l'ancien code ci-dessous tant que nous n'avons pas valider le code ci-dessus sur la prod
         * validation à faire pas thomas basbous
         */





        foreach ($stores as $store) {
            $storedebug = " stores $totalstores / " . $countstore++;
            echo round($mem_usage / 1048576, 2) . " MB   foreach stores \n";
            echo "working on store " . $store->getName() . " -> " . $storedebug;
            echo "\n";
            $countproducts = 0;
            $totalproducts = count($fileData);
            ;
            foreach ($fileData as $productData) {
                $productdebug = "$storedebug - $totalproducts / " . $countproducts++ . " \n";
                echo round($mem_usage / 1048576, 2) . " MB  $productdebug \n";
                if (!isset($rowIds[$productData[$this->styleCodeKey]])) {
                    continue;
                }

                $currencyKey = array_keys($productData, $store->getCurrentCurrency()->getCode());

                if ($currencyKey) {
                    $key = $currencyKey[0];
                    $countrows = count($rowIds[$productData[$this->styleCodeKey]]);
                    foreach ($rowIds[$productData[$this->styleCodeKey]] as $entry) {
                        $productdebug = "$storedebug - $totalproducts / " . $countproducts . " \n";
                        echo round($mem_usage / 1048576, 2) . " MB foreach $productdebug \n";
                        $value = $productData[$key + 1];
                        $storeCode = $store->getCurrentCurrency()->getCode();

                        if ($storeCode === $defaultStoreCode) {
                            $insertData[] = [
                                'row_id' => $entry['row_id'],
                                'store_id' => $defaultStoreId,
                                'attribute_id' => $attributePrice->getAttributeId(),
                                'value' => $value,
                            ];
                        }

                        $insertData[] = [
                            'row_id' => $entry['row_id'],
                            'store_id' => $store->getId(),
                            'attribute_id' => $attributePrice->getAttributeId(),
                            'value' => $value,
                        ];

                        if (in_array($value, ['0.00', '', null]) && $entry['value'] == '1') {
                            $searchableSku = $this->productPriceRepository->getSearchableSkuByRowId(
                                    $entry['row_id'],
                                    $attributeSearchableSkuId->getAttributeId()
                            );

                            if (isset($searchableSku['value'])) {
                                $searchableSku = $searchableSku['value'];
                                $storeCode = $store->getCurrentCurrency()->getCode();

                                if (!isset($mailData[$searchableSku . $storeCode])) {
                                    $mailData[$searchableSku . $storeCode] = [
                                        'searchable_sku' => $searchableSku,
                                        'price' => '0.00',
                                        'currency' => $storeCode,
                                    ];
                                }
                            }
                        }

                        $data['products'][] = [
                            'row_id' => $entry['row_id'],
                        ];
                    }
                    $insertCounter++;
                    if ($insertCounter > 10) {
                        echo round($mem_usage / 1048576, 2) . " MB b4 insert \n";
                        $this->productPriceRepository->insertPriceData($insertData);
                        echo round($mem_usage / 1048576, 2) . " MB after insert \n";

                        $insertData = [];
                        $insertCounter = 0;
                    }
                }
            }
        }

        $this->productPriceRepository->insertPriceData($insertData);
        $data['mailData'] = $mailData;

        return $data;
    }

}
