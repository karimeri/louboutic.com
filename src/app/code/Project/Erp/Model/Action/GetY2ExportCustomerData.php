<?php

namespace Project\Erp\Model\Action;

use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Project\Erp\Helper\Config\Address as HelperConfig;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class GetY2CustomerExportData
 * @package Project\Erp\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class GetY2ExportCustomerData extends ErpAction implements ActionInterface
{
    /**
     * @var HelperConfig
     */
    protected $helperConfig;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * GetY2ExportCustomerData constructor.
     *
     * @param \Project\Erp\Helper\ErpCustomerId $helper
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Project\Erp\Helper\Config\Address $helperConfig
     * @param \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        HelperConfig $helperConfig,
        CustomerRepository $customerRepository,
        State $state
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone
        );

        $this->helperConfig = $helperConfig;
        $this->customerRepository = $customerRepository;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $fileData = [];

        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $data[GetErpExportOrderCollection::ORDER_COLLECTION_KEY];

        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orderCollection->getItems() as $order) {
            $address = $order->getBillingAddress();
            $websiteId = $order->getStore()->getWebsiteId();
            $establishmentCode = $this->wmsHelper->getEstablishmentCode($websiteId);

            $line = [
                // Interface Identifier
                'MCLC1 ',
                // Customer Identifier Code
                $this->getCustomerIdentifierCode($order),
                // Customer Title
                $this->helperConfig->getY2Title($address->getPrefix()),
                // Customer Name
                $this->upperTrunc($address->getLastname(), 35),
                // Customer First Name
                $this->upperTrunc($address->getFirstname(), 35),
                // Address Line 1
                $this->upperTrunc($this->getAddressLine($address, 0), 35),
                // Address Line 2
                $this->upperTrunc($this->getAddressLine($address, 1), 35),
                // State Information
                $this->upperTrunc(
                    $this->helperConfig->getUseStateCodes($websiteId) ? $address->getRegion() : '',
                    35
                ),
                // Postal Code
                $this->upperTrunc($address->getPostcode(), 9),
                // City
                $this->upperTrunc($address->getCity(), 35),
                // State Code
                $this->upperTrunc(
                    $this->helperConfig->getUseStateCodes($websiteId) ? $address->getRegionCode() : '',
                    3
                ),
                // Country Code
                $this->upperTrunc($address->getCountryId(), 3),
                // No longer live here
                'N',
                // Gender
                $this->helperConfig->getY2Gender($address->getPrefix()),
                // Date of Birth (DAY)
                '',
                // Date of Birth (MONTH)
                '',
                // Date of Birth (YEAR)
                '',
                // Customer Email Address
                $order->getCustomerEmail(),
                // Second Email Address
                '',
                // Customer Phone Number
                $address->getTelephone(),
                // Customer Phone Number #2
                $address->getFax(),
                // Customer Phone Number #3
                '',
                // Is the customer account still active?
                'N',
                // Establishment code
                $establishmentCode,
                // Establishment creation code
                $establishmentCode,
                // Guest Customer
                $order->getCustomerIsGuest() ? 'Y' : 'N',
                // Account Creation Date (DDMMYYYYHHMMSS)
                $this->getAccountCreationDate($order),
            ];

            $fileData[] = $line;
        }

        $data['fileData'] = $fileData;

        return $data;
    }

    /**
     * Get Address line
     *
     * @param \Magento\Sales\Api\Data\OrderAddressInterface $address
     * @param int $index
     *
     * @return string
     */
    protected function getAddressLine($address, $index = 0)
    {
        $streetArray = $address->getStreet();

        return $streetArray[$index] ?? '';
    }

    /**
     * get Account creation date from Order
     * Return Customer created_at or order_created_t if customer is guest
     * Format DDMMYYYYHHMMSS
     *
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getAccountCreationDate($order)
    {
        $timestamp = $order->getCreatedAt();
        if (!$order->getCustomerIsGuest() && $order->getCustomerId()) {
            try {
                $customer = $this->customerRepository->getById($order->getCustomerId());
                $timestamp = $customer->getCreatedAt();
            } catch (\Exception $exception) {
                throw new LocalizedException(__('Could not find customer for Order ID %1', $order->getId()));
            }
        }

        return $this->convertDateTime($timestamp, 'dmYHis', $order->getStoreId());
    }
}
