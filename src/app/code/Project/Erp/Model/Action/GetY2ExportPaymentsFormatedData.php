<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Sales\Model\Order\Creditmemo\Item as CreditmemoItem;
use Psr\Log\LoggerInterface;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Project\Erp\Helper\Config\Y2\Sales as HelperY2Sales;
use Project\Wms\Helper\Config as WmsHelper;
use Project\Core\Model\Environment;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Core\Manager\EnvironmentManager;
use Magento\Rma\Model\RmaRepository;

/**
 * Class GetY2ExportPaymentsFormatedData
 *
 * @package Project\Erp\Model\Action\ANSI
 * @author  Synolia <contact@synolia.com>
 */
class GetY2ExportPaymentsFormatedData extends ErpAction implements ActionInterface
{
    const AUTHORIZED_INSTANCES = [Environment::US, Environment::EU];

    const PAYMENT_CODES = [
        // dinners
        'DN' => [
            'US' => 'US3',
            'CA' => 'CA3',
            'FR' => '',
            'UK' => '',
            'IT' => '',
            'DE' => '',
            'ES' => '',
            'CH' => '',
            'NL' => '',
            'LU' => '',
            'BE' => '',
            'AT' => '',
            'IE' => '',
            'PT' => '',
            'MC' => '',
            'GR' => '',
        ],
        // amex
        'AE' => [
            'US' => 'US2',
            'CA' => 'CA2',
            'FR' => 'EU2',
            'UK' => 'GB2',
            'IT' => 'EU2',
            'DE' => 'EU2',
            'ES' => 'EU2',
            'CH' => 'CH2',
            'NL' => 'EU2',
            'LU' => 'EU2',
            'BE' => 'EU2',
            'AT' => 'EU2',
            'IE' => 'EU2',
            'PT' => 'EU2',
            'MC' => 'EU2',
            'GR' => 'EU2',
        ],
        // japan card
        'JCB' => [
            'US' => 'USG',
            'CA' => 'CAG',
            'FR' => '',
            'UK' => '',
            'IT' => '',
            'DE' => '',
            'ES' => '',
            'CH' => '',
            'NL' => '',
            'LU' => '',
            'BE' => '',
            'AT' => '',
            'IE' => '',
            'PT' => '',
            'MC' => '',
            'GR' => '',
        ],
        // mastercard
        'MC' => [
            'US' => 'US4',
            'CA' => 'CA4',
            'FR' => 'EUH',
            'UK' => 'GBH',
            'IT' => 'EUH',
            'DE' => 'EUH',
            'ES' => 'EUH',
            'CH' => 'CH4',
            'NL' => 'EUH',
            'LU' => 'EUH',
            'BE' => 'EUH',
            'AT' => 'EUH',
            'IE' => 'EUH',
            'PT' => 'EUH',
            'MC' => 'EUH',
            'GR' => 'EUH',
        ],
        // visa
        'VI' => [
            'US' => 'USI',
            'CA' => 'CAI',
            'FR' => 'EUE',
            'UK' => 'GBE',
            'IT' => 'EUE',
            'DE' => 'EUE',
            'ES' => 'EUE',
            'CH' => 'CH3',
            'NL' => 'EUE',
            'LU' => 'EUE',
            'BE' => 'EUE',
            'AT' => 'EUE',
            'IE' => 'EUE',
            'PT' => 'EUE',
            'MC' => 'EUE',
            'GR' => 'EUE',
        ],
        // Maestro --- @TODO CODE MAESTRO FOR US/CANADA
        'MI' => [
            'US' => '',
            'CA' => '',
            'FR' => 'EUI',
            'UK' => 'GBH',
            'IT' => 'EUI',
            'DE' => 'EUI',
            'ES' => 'EUI',
            'CH' => 'CH5',
            'NL' => 'EUI',
            'LU' => 'EUI',
            'BE' => 'EUI',
            'AT' => 'EUI',
            'IE' => 'EUI',
            'PT' => 'EUI',
            'MC' => 'EUI',
            'GR' => 'EUI',
        ],
        // paypal
        'PP' => [
            'US' => 'USL',
            'CA' => 'CAL',
            'FR' => 'EUP',
            'UK' => 'GBL',
            'IT' => 'EUP',
            'DE' => 'EUP',
            'ES' => 'EUP',
            'CH' => 'CHL',
            'NL' => 'EUP',
            'LU' => 'EUP',
            'BE' => 'EUP',
            'AT' => 'EUP',
            'IE' => 'EUP',
            'PT' => 'EUP',
            'MC' => 'EUP',
            'GR' => 'EUP',
        ],
        // discover
        'DI' => [
            'US' => 'USH',
            'CA' => 'CAH',
            'FR' => '',
            'UK' => '',
            'IT' => '',
            'DE' => '',
            'ES' => '',
            'CH' => '',
            'NL' => '',
            'LU' => '',
            'BE' => '',
            'AT' => '',
            'IE' => '',
            'PT' => '',
            'MC' => '',
            'GR' => '',
        ],
        'GAP' => [
            'US' => 'USK',
            'CA' => 'USK',
            'UK' => 'EUS',
            'CH' => 'EUS',
            'FR' => 'EUS',
            'IT' => 'EUS',
            'DE' => 'EUS',
            'ES' => 'EUS',
            'NL' => 'EUS',
            'LU' => 'EUS',
            'BE' => 'EUS',
            'AT' => 'EUS',
            'IE' => 'EUS',
            'PT' => 'EUS',
            'MC' => 'EUS',
            'GR' => 'EUS',
        ],
        'free' => [
            'US' => 'US9',
            'CA' => 'CA9',
            'UK' => 'GB3',
            'CH' => 'CH3',
            'FR' => 'EU3',
            'IT' => 'EU3',
            'DE' => 'EU3',
            'ES' => 'EU3',
            'NL' => 'EU3',
            'LU' => 'EU3',
            'BE' => 'EU3',
            'AT' => 'EU3',
            'IE' => 'EU3',
            'PT' => 'EU3',
            'MC' => 'EU3',
            'GR' => 'EU3',
        ],
    ];

    const PAYPAL_METHODS = [
        'adyen_hpp' => 'adyen_hpp',
    ];

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var HelperY2Sales
     */
    protected $helperY2Sales;

    /**
     * @var bool
     */
    protected $gapLine = false;

    /**
     * @var float
     */
    protected $gap = 0;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * ArrayToCsv constructor.
     *
     * @param Helper $helper
     * @param WmsHelper $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param EnvironmentManager $environmentManager
     * @param HelperY2Sales $helperY2Sales
     * @param State $state
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        EnvironmentManager $environmentManager,
        HelperY2Sales $helperY2Sales,
        State $state,
        LoggerInterface $logger,
        RmaRepository $rmaRepository
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone
        );

        $this->environmentManager = $environmentManager;
        $this->helperY2Sales = $helperY2Sales;
        $this->logger = $logger;
        $this->rmaRepository = $rmaRepository;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $environment = $this->environmentManager->getEnvironment();
        if (!in_array($environment, self::AUTHORIZED_INSTANCES, true)) {
            throw new \RuntimeException('Environment ' . $environment . ' is not supported');
        }

        $formatedData = [];

        foreach ($data as $type => $collection) {
            /** @var Creditmemo|Invoice $entry */
            foreach ($collection as $entry) {
                try {
                    $formatedData[] = [
                        // interface identifier
                        'MREC1 ',
                        // establishment code
                        $this->upperTrunc(
                            $this->wmsHelper->getEstablishmentCode($entry->getStore()->getWebsiteId()),
                            3
                        ),
                        // file creation date
                        $this->upperTrunc(
                            $this->convertDateTime($entry->getCreatedAt(), 'dmY', $entry->getStoreId()),
                            20
                        ),
                        // payment number
                        '1',
                        // payment method code
                        $this->getPaymentMethodCode($entry),
                        // total paid amount
                        $this->getTotalPaidAmount($entry, $type),
                        // currency code
                        $this->upperTrunc($entry->getOrderCurrencyCode(), 3),
                        // currency change rate
                        '1',
                        // sales ticket number
                        $this->upperTrunc($entry->getIncrementId(), 20),
                        // file creation date
                        $this->upperTrunc(
                            $this->convertDateTime($entry->getCreatedAt(), 'dmYHis', $entry->getStoreId()),
                            20
                        ),
                    ];

                    if ($this->gapLine) {
                        $formatedData[] = [
                            // interface identifier
                            'MREC1 ',
                            // establishment code
                            $this->upperTrunc(
                                $this->wmsHelper->getEstablishmentCode($entry->getStore()->getWebsiteId()),
                                3
                            ),
                            // file creation date
                            $this->upperTrunc(
                                $this->convertDateTime($entry->getCreatedAt(), 'dmY', $entry->getStoreId()),
                                20
                            ),
                            // payment number
                            '2',
                            // payment method code
                            $this->getPaymentMethodCode($entry),
                            // total paid amount
                            $this->getTotalPaidAmount($entry, $type),
                            // currency code
                            $this->upperTrunc($entry->getOrderCurrencyCode(), 3),
                            // currency change rate
                            '1',
                            // sales ticket number
                            $this->upperTrunc($entry->getIncrementId(), 20),
                            // file creation date
                            $this->upperTrunc(
                                $this->convertDateTime($entry->getCreatedAt(), 'dmYHis', $entry->getStoreId()),
                                20
                            ),
                        ];

                        $this->gap = 0;
                        $this->gapLine = false;
                    }
                } catch (\Exception $e) {
                    //do nothing
                    $this->logger->error('Y2 export payments error line', ['message' => $e->getMessage()]);
                }
            }
        }

        $data['fileData'] = $formatedData;

        return $data;
    }

    /**
     * @param Creditmemo|Invoice $entry
     *
     * @return string
     */
    protected function getPaymentMethodCode($entry)
    {
        $storeCode = $this->upperTrunc($entry->getStore()->getCode(), 2);

        if ($this->gapLine) {
            return self::PAYMENT_CODES['GAP'][$storeCode];
        }

        $payment = $entry->getOrder()->getPayment();

        // Get the payment method of the original order
        if($entry->getOrder()->getPayment()->getMethod() == 'exchange_offline') {
            $rmaId = $entry->getOrder()->getRmaId();
            $rma   = $this->rmaRepository->get($rmaId);
            $order = $rma->getOrder();
            $payment = $order->getPayment();
        }

        try {
            if (isset(self::PAYMENT_CODES[$payment->getMethod()])) {
                return self::PAYMENT_CODES[$payment->getMethod()][$storeCode];
            }

            if (isset(self::PAYPAL_METHODS[$payment->getMethod()])) {
                return self::PAYMENT_CODES['PP'][$storeCode];
            }

            $ccType = $payment->getCcType();
            return self::PAYMENT_CODES[$ccType][$storeCode];


            // @todo veritrans
        } catch (\Exception $e) {
            // same as default
        }

        return 'EQ';
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return string
     */
    protected function getTotalPaidAmount($entry, $type)
    {
        if ($this->gapLine) {
            $amount = $this->gap;
        } else {
            $amount = 0;
            /** @var CreditmemoItem|InvoiceItem $item */
            foreach ($entry->getAllItems() as $item) {
                $amount += $item->getPriceInclTax() * $this->getQty($item);
            }

            $amount += $entry->getShippingInclTax();

            // adjustments
            if ($type === 'credit_memos') {
                $adjustments = $entry->getAdjustmentShippingFees()
                    - $entry->getAdjustmentRepairingFees()
                    + $entry->getAdjustmentCreditNote()
                    - $entry->getAdjustmentReturnCharge();

                $amount += $adjustments;
            }

            if ($amount != $entry->getGrandTotal()) {
                $this->gap = $entry->getGrandTotal() - $amount;
                $this->gapLine = true;
            }
        }

        if ($type === 'invoices') {
            return number_format($amount, 2, '.', '');
        }

        // creditmemos
        return number_format($amount * -1, 2, '.', '');
    }

    /**
     * @param InvoiceItem|CreditmemoItem $item
     *
     * @return int
     */
    protected function getQty($item)
    {
        if (in_array($item->getSku(), $this->helperY2Sales->getY2VirtualReferences(), false)) {
            return 1;
        }

        return number_format($item->getQty());
    }

}
