<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Store\Model\StoreRepository;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\ObjectManager\ConfigLoaderInterface;

use Project\Erp\Helper\Config\Report\Sales as HelperReportSales;
use Project\Erp\Helper\Config\Report\Sales;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class SendAlertImportPriceMail
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class SendAlertImportPriceMail implements ActionInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var HelperReportSales
     */
    protected $helperReportSales;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * SendExportReportEmail constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param ObjectManagerInterface $objectManager
     * @param ConfigLoaderInterface $configLoader
     * @param State $state
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Project\Erp\Helper\Config\Report\Sales $helperReportSales
     * @param StoreRepository $storeRepository
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        ObjectManagerInterface $objectManager,
        ConfigLoaderInterface $configLoader,
        State $state,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        HelperReportSales $helperReportSales,
        StoreRepository $storeRepository
    ) {
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->storeRepository = $storeRepository;
        $this->date = $date;
        $this->helperReportSales = $helperReportSales;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
        $objectManager->configure($configLoader->load('adminhtml'));
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\MailException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        if (count($data[$params['mailData']])) {
            $templateOptions = ['area' => Area::AREA_ADMINHTML, 'store' => $this->storeManager->getStore()->getId()];
            $templateVars = [
                'productData' => $data[$params['mailData']],
                'date' => $this->date->gmtDate('Y-m-d'),
            ];

            $from = ['email' => $this->helperReportSales->getFromMail0(), 'name' => 'Alert Import Prices'];

            $to = $this->getTo();

            $this->inlineTranslation->suspend();

            $transport = $this->transportBuilder->setTemplateIdentifier('alert_import_prices')
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($to)
                ->getTransport();
            $transport->sendMessage();

            $this->inlineTranslation->resume();
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getTo()
    {
        $to = [];
        foreach ($this->storeRepository->getList() as $store) {
            $to = array_merge($to, $this->helperReportSales->getImportPricesMails($store->getId()));
        }

        return array_unique($to);
    }
}
