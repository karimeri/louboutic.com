<?php

namespace Project\Erp\Model\Action\ANSI;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class ArrayToTXT
 *
 * @package Project\Erp\Model\Action\ANSI
 * @author  Synolia <contact@synolia.com>
 */
class ArrayToTXT implements ActionInterface
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * ArrayToTXT constructor.
     *
     * @param Filesystem $filesystem
     * @param State $state
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     */
    public function __construct(
        Filesystem $filesystem,
        State $state,
        TimezoneInterface $timezone
    ) {
        $this->filesystem = $filesystem;
        $this->timezone = $timezone;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array         $params
     * @param array         $data
     * @param string        $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $fileContent = '';

        $fileData = $data['fileData'];

        /** @var Order $order */
        foreach ($fileData as $line) {
            $fileContent .= \implode("\t", $line) . "\r\n";
        }

        // don't export if no values
        if ($fileContent === '') {
            return $data;
        }

        // replace accentuated caracters by non accentuated ones (incompatible with ANSI)
        $transliterator = \Transliterator::create(
            'NFD; [:Nonspacing Mark:] Remove; NFC;'
        );

        $this->saveFile(
            $params,
            iconv(
                "UTF-8//TRANSLIT",
                "WINDOWS-1252//TRANSLIT",
                $transliterator->transliterate($fileContent)
            )
        );

        return $data;
    }

    /**
     * @param array  $params
     * @param string $content
     *
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function saveFile($params, $content)
    {
        $fileName = sprintf('%s_%s.TXT', $params['prefix'], $this->timezone->date()->format('YmdHis'));
        $writer   = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $path     = $writer->getRelativePath(
            sprintf('%s%s', $params['filepath'], $fileName)
        );

        $stream = $writer->openFile($path, 'w');
        $stream->write($content);
        $stream->close();
    }
}
