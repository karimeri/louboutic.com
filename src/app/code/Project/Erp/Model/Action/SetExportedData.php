<?php

namespace Project\Erp\Model\Action;

use Magento\Sales\Model\ResourceModel\Collection\AbstractCollection;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class SetExportedData
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class SetExportedData implements ActionInterface
{
    const DEFAULT_STATUS_COLUMN = 'exported';

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $statusColumn = $params['statusColumn'] ?? self::DEFAULT_STATUS_COLUMN;

        foreach ($data as $collection) {
            if (!$collection instanceof AbstractCollection) {
                continue;
            }

            $entityIds = $collection->getAllIds();

            $collection->getConnection()
                ->update(
                    $collection->getMainTable(),
                    [$statusColumn => 1],
                    ['entity_id IN (?)' => $entityIds]
                );
        }

        return $data;
    }
}
