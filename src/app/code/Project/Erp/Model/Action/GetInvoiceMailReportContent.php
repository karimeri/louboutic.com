<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

use Project\Erp\Helper\ExportSalesTickets as HelperExportSalesTickets;
use Project\Erp\Helper\Config\Y2\Sales as HelperY2Sales;
use Project\Core\Helper\AttributeSet;

/**
 * Class GetInvoiceMailReportContent
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class GetInvoiceMailReportContent implements ActionInterface
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * @var HelperY2Sales
     */
    protected $helperY2Sales;

    /**
     * @var HelperExportSalesTickets
     */
    protected $helperExportSalesTickets;

    /**
     * @var array
     */
    protected $invoiceData;

    /**
     * @param ProductRepository $productRepository
     * @param AttributeSet $attributeSetHelper
     * @param HelperY2Sales $helperY2Sales
     * @param HelperExportSalesTickets $helperExportSalesTickets
     */
    public function __construct(
        ProductRepository $productRepository,
        AttributeSet $attributeSetHelper,
        HelperY2Sales $helperY2Sales,
        HelperExportSalesTickets $helperExportSalesTickets
    ) {
        $this->productRepository = $productRepository;
        $this->attributeSetHelper = $attributeSetHelper;
        $this->helperY2Sales = $helperY2Sales;
        $this->helperExportSalesTickets = $helperExportSalesTickets;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $this->processInvoices($data['invoices'],$params);
        $data['invoiceData'] = $this->invoiceData;

        return $data;
    }

    /**
     * @param InvoiceCollection $invoices
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    // phpcs:disable
    protected function processInvoices($invoices,$params)
    {
        $currencies = $this->helperExportSalesTickets->getCurrencies();
        $invoiceDate = date('d/m') ;
        if(count($params) != 0 && (isset($params["replay_start_date"]) || isset($params["replay_end_date"]))){
            $invoiceDate = '';
            $invoiceDate .= (isset($params["replay_start_date"]) && !empty($params["replay_start_date"])) ? "From " . $params["replay_start_date"] : "";
            $invoiceDate .= (isset($params["replay_end_date"]) && !empty($params["replay_end_date"])) ? " To " . $params["replay_end_date"] : "";
        }
        $initArray = $this->helperExportSalesTickets->initArray($currencies);
        $this->invoiceData = [
            'date' => $invoiceDate,
            'paypalAmount' => \array_merge($initArray, ['header' => '']),
            'invoicesCount' => $initArray,
            'servicesInvoicesCount' => $initArray,
            'exchangeInvoicesCount' => $initArray,
            'omnichannelInvoicesCount' => $initArray,
            'productsCount' => $initArray,
            'womenShoesCount' => $initArray,
            'menShoesCount' => $initArray,
            'womenBagsCount' => $initArray,
            'menBagsCount' => $initArray,
            'accessoriesCount' => $initArray,
            'beltsCount' => $initArray,
            'braceletsCount' => $initArray,
            'nailsCount' => $initArray,
            'lipsCount' => $initArray,
            'fragrancesCount' => $initArray,
            'eyesCount' => $initArray,
            'otherCount' => $initArray,
            'otherSkus' => [],
            'exchangeInvoices' => $initArray,
            'bankAmount' => $initArray,
            'storeCreditAmount' => $initArray,
            'invoicedServices' => $initArray,
            'exchangeInvoicedAmount' => $initArray,
            'omnichannelInvoicedAmount' => $initArray,
            'taxTotal' => $initArray,
            'netTotal' => $initArray,
            'netTotalShoesBagsAccessories' => $initArray,
            'netTotalBeauty' => $initArray,
            'shippingTotal' => $initArray,
            'brutTotal' => $initArray,
        ];

        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            // invoices count
            $this->invoiceData['invoicesCount']['total']++;
            // invoices currency count
            $this->invoiceData['invoicesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['invoicesCount'],
                $invoice->getOrderCurrencyCode()
            );
            // paypal
            if ($this->helperExportSalesTickets->isPaypal($invoice->getOrder()->getPayment())) {
                $this->invoiceData['paypalAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->invoiceData['paypalAmount'],
                    $invoice->getOrderCurrencyCode(),
                    $invoice->getGrandTotal()
                );
            }
            // services invoices
            if ($this->isService($invoice)) {
                $this->invoiceData['servicesInvoicesCount']['total']++;
                $this->invoiceData['servicesInvoicesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->invoiceData['servicesInvoicesCount'],
                    $invoice->getOrderCurrencyCode()
                );
                // invoiced services
                $this->invoiceData['invoicedServices'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->invoiceData['invoicedServices'],
                    $invoice->getOrderCurrencyCode(),
                    $invoice->getGrandTotal()
                );
            }
            // exchange invoices count
            $orderIncrementId = $invoice->getOrder()->getIncrementId();
            if (preg_match('/-1/', $orderIncrementId)) {
                $this->invoiceData['exchangeInvoicesCount']['total']++;
                $this->invoiceData['exchangeInvoicesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->invoiceData['exchangeInvoicesCount'],
                    $invoice->getOrderCurrencyCode()
                );
                // exchanged invoice amount
                $this->invoiceData['exchangeInvoicedAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->invoiceData['exchangeInvoicedAmount'],
                    $invoice->getOrderCurrencyCode(),
                    $invoice->getGrandTotal()
                );
            }
            // omnichannel invoices count always 0 in lot 0
            // products count
            $this->invoiceData['productsCount']['total'] += (int) $invoice->getOrder()->getTotalQtyOrdered();
            $this->invoiceData['productsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['productsCount'],
                $invoice->getOrderCurrencyCode(),
                (int) $invoice->getOrder()->getTotalQtyOrdered()
            );
            // product type count
            /** @var Invoice\Item $item */
            foreach ($invoice->getAllItems() as $item) {
                try {
                    // if simple product
                    if ($item->getOrderItem()->getParentItemId()) {
                        continue;
                    }

                    try {
                        $product = $this->productRepository->getById($item->getProductId());
                    } catch (\Exception $e) {
                        // do nothing
                        continue;
                    }

                    if ($this->attributeSetHelper->isShoes($product)) {
                        if (strtoupper($product->getResource()->getAttribute('gender')->getFrontend()->getValue($product)) === 'MEN') {
                            $this->invoiceData['menShoesCount']['total'] += $item->getQty();
                            $this->invoiceData['menShoesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                                $this->invoiceData['menShoesCount'],
                                $invoice->getOrderCurrencyCode(),
                                $item->getQty()
                            );
                        } else {
                            $this->invoiceData['womenShoesCount']['total'] += $item->getQty();
                            $this->invoiceData['womenShoesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                                $this->invoiceData['womenShoesCount'],
                                $invoice->getOrderCurrencyCode(),
                                $item->getQty()
                            );
                        }
                        // net total shoes, bags, accessories
                        $this->invoiceData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalShoesBagsAccessories'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isBag($product)) {
                        if ($product->getResource()->getAttribute('gender')->getFrontend()->getValue($product) === 'MEN') {
                            $this->invoiceData['menBagsCount']['total'] += $item->getQty();
                            $this->invoiceData['menBagsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                                $this->invoiceData['menBagsCount'],
                                $invoice->getOrderCurrencyCode(),
                                $item->getQty()
                            );
                        } else {
                            $this->invoiceData['womenBagsCount']['total'] += $item->getQty();
                            $this->invoiceData['womenBagsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                                $this->invoiceData['womenBagsCount'],
                                $invoice->getOrderCurrencyCode(),
                                $item->getQty()
                            );
                        }
                        // net total shoes, bags, accessories
                        $this->invoiceData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalShoesBagsAccessories'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isAccessories($product)) {
                        $this->invoiceData['accessoriesCount']['total'] += $item->getQty();
                        $this->invoiceData['accessoriesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['accessoriesCount'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                        // net total shoes, bags, accessories
                        $this->invoiceData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalShoesBagsAccessories'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isBelts($product) || $this->attributeSetHelper->isBracelets($product)) {
                        if ($this->attributeSetHelper->isBelts($product)) {
                            $this->invoiceData['beltsCount']['total'] += $item->getQty();
                            $this->invoiceData['beltsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                                $this->invoiceData['beltsCount'],
                                $invoice->getOrderCurrencyCode(),
                                $item->getQty()
                            );
                        }elseif ($this->attributeSetHelper->isBracelets($product)) {
                            $this->invoiceData['braceletsCount']['total'] += $item->getQty();
                            $this->invoiceData['braceletsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                                $this->invoiceData['braceletsCount'],
                                $invoice->getOrderCurrencyCode(),
                                $item->getQty()
                            );
                        }
                        // net total shoes, bags, accessories
                        $this->invoiceData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalShoesBagsAccessories'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isNails($product)) {
                        $this->invoiceData['nailsCount']['total'] += $item->getQty();
                        $this->invoiceData['nailsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['nailsCount'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                        // net total beauty
                        $this->invoiceData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalBeauty'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isLips($product)) {
                        $this->invoiceData['lipsCount']['total'] += $item->getQty();
                        $this->invoiceData['lipsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['lipsCount'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                        // net total beauty
                        $this->invoiceData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalBeauty'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isPerfumes($product)) {
                        $this->invoiceData['fragrancesCount']['total'] += $item->getQty();
                        $this->invoiceData['fragrancesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['fragrancesCount'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                        // net total beauty
                        $this->invoiceData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalBeauty'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } elseif ($this->attributeSetHelper->isEyes($product)) {
                        $this->invoiceData['eyesCount']['total'] += $item->getQty();
                        $this->invoiceData['eyesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['eyesCount'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                        // net total beauty
                        $this->invoiceData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['netTotalBeauty'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                    } else {
                        $this->invoiceData['otherCount']['total'] += $item->getQty();
                        $this->invoiceData['otherCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->invoiceData['otherCount'],
                            $invoice->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                        $otherSkus[] = $product->getSku();
                    }
                } catch (NoSuchEntityException $e) {
                    // product does not exist
                }
            }

            $this->invoiceData['bankAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['bankAmount'],
                $invoice->getOrderCurrencyCode(),
                $invoice->getGrandTotal()
            );
            // store credit amount (always 0)
            $this->invoiceData['storeCreditAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['storeCreditAmount'],
                $invoice->getOrderCurrencyCode(),
                $invoice->getData('customer_balance_amount')
            );
            // omnichannel invoiced amount (always 0)
            // tax total
            $this->invoiceData['taxTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['taxTotal'],
                $invoice->getOrderCurrencyCode(),
                $invoice->getTaxAmount()
            );
            // net total
            $this->invoiceData['netTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['netTotal'],
                $invoice->getOrderCurrencyCode(),
                $invoice->getSubtotal()
            );
            // shipping total
            $this->invoiceData['shippingTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['shippingTotal'],
                $invoice->getOrderCurrencyCode(),
                $invoice->getShippingAmount()
            );
            // brut total
            $this->invoiceData['brutTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->invoiceData['brutTotal'],
                $invoice->getOrderCurrencyCode(),
                $invoice->getGrandTotal()
            );
        }
        // phpcs:enable
        $this->invoiceData['currencies'] = $currencies;
        $this->invoiceData['paypalAmount']['header'] = $this->helperExportSalesTickets->getPaypalHeader(
            $currencies,
            $this->invoiceData['paypalAmount']
        );
    }

    /**
     * @param Invoice $invoice
     *
     * @return bool
     */
    protected function isService($invoice)
    {
        /** @var InvoiceItem $item */
        foreach ($invoice->getAllItems() as $item) {
            if (!$this->helperY2Sales->isY2VirtualReference($item->getSku())) {
                return false;
            }
        }

        return true;
    }
}
