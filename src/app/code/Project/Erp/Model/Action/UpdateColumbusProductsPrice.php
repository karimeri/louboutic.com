<?php

namespace Project\Erp\Model\Action;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\AttributeRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Erp\Helper\Config;
use Project\Erp\Model\Repository\ProductPriceRepository;
use Psr\Log\LoggerInterface;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class UpdateColumbusProductsPrice
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class UpdateColumbusProductsPrice implements ActionInterface
{
    const AUTHORIZED_INSTANCES = [Environment::HK, Environment::JP];

    const AUTHORIZED_CURRENCIES = [
        'JPY',
        'AUD',
        'HKD',
        'SGD',
        'MYR',
        'TWD',
    ];

    const HK_FIELDS = [
        0 => 'product_code',
        6 => 'HKD',
        7 => 'SGD',
        8 => 'MYR',
        9 => 'TWD',
        10 => 'AUD',
    ];

    const JP_FIELDS = [
        0 => 'product_code',
        1 => 'JPY',
    ];

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var int
     */
    protected $styleCodeKey = 0;

    /**
     * @var \Project\Erp\Model\Repository\ProductPriceRepository
     */
    protected $productPriceRepository;

    /**
     * @var \Magento\Eav\Model\AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var \Project\Erp\Helper\Config
     */
    protected $helper;

    /**
     * @param State $state
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Erp\Model\Repository\ProductPriceRepository $productPriceRepository
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     * @param \Project\Erp\Helper\Config $helper
     */
    public function __construct(
        State $state,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        EnvironmentManager $environmentManager,
        ProductPriceRepository $productPriceRepository,
        AttributeRepository $attributeRepository,
        Config $helper
    ) {
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->environmentManager = $environmentManager;
        $this->productPriceRepository = $productPriceRepository;
        $this->attributeRepository = $attributeRepository;
        $this->helper = $helper;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $environment = $this->environmentManager->getEnvironment();
        if (!in_array($environment, self::AUTHORIZED_INSTANCES, true)) {
            throw new \RuntimeException('Environment ' . $environment . ' is not supported');
        }

        $fileData = $data[$params['key']];
        $attributes = array_column($fileData, $this->styleCodeKey);
        $attributeStyleCode = $this->attributeRepository->get(Product::ENTITY, 'style_code');
        $attributeStatus = $this->attributeRepository->get(Product::ENTITY, 'status');
        $attributeSearchableSkuId = $this->attributeRepository->get(
            Product::ENTITY,
            'searchable_sku'
        );
        $attributePrice = $this->attributeRepository->get(Product::ENTITY, 'price');
        $rowIds = $this->productPriceRepository->getRowIdsByStyleCodes(
            $attributes,
            $attributeStyleCode->getAttributeId(),
            $attributeStatus->getAttributeId()
        );

        $data['products'] = [];
        $data['mailData'] = [];
        $mailData = [];
        $insertData = [];

        $defaultStoreCode = $this->helper->getBaseCurrency();
        $defaultStoreId = $this->storeManager->getStore('admin')->getId();

        $insertCounter = 0;

        foreach ($this->storeManager->getStores() as $store) {
            if (!in_array(
                $store->getCurrentCurrency()->getCode(),
                self::AUTHORIZED_CURRENCIES
            )) {
                continue;
            }

            foreach ($fileData as $productData) {
                if (!isset($rowIds[$productData[$this->styleCodeKey]])) {
                    continue;
                }

                $currencyKey = $this->getCurrencyKey($productData, $store);

                if ($currencyKey) {
                    $key = $currencyKey[0];

                    foreach ($rowIds[$productData[$this->styleCodeKey]] as $entry) {
                        $value = $productData[$key];
                        $storeCode = $store->getCurrentCurrency()->getCode();

                        if ($storeCode === $defaultStoreCode) {
                            $insertData[] = [
                                'row_id' => $entry['row_id'],
                                'store_id' => $defaultStoreId,
                                'attribute_id' => $attributePrice->getAttributeId(),
                                'value' => $value,
                            ];
                        }

                        $insertData[] = [
                            'row_id' => $entry['row_id'],
                            'store_id' => $store->getId(),
                            'attribute_id' => $attributePrice->getAttributeId(),
                            'value' => $value,
                        ];

                        if (in_array($value, ['0.00', '', null]) && $entry['value'] == '1') {
                            $searchableSku = $this->productPriceRepository->getSearchableSkuByRowId(
                                $entry['row_id'],
                                $attributeSearchableSkuId->getAttributeId()
                            );

                            if (isset($searchableSku['value'])) {
                                $searchableSku = $searchableSku['value'];
                                $storeCode = $store->getCurrentCurrency()->getCode();

                                if (!isset($mailData[$searchableSku.$storeCode])) {
                                    $mailData[$searchableSku.$storeCode] = [
                                        'searchable_sku' => $searchableSku,
                                        'price' => '0.00',
                                        'currency' => $storeCode,
                                    ];
                                }
                            }
                        }

                        $data['products'][] = [
                            'row_id' => $entry['row_id'],
                        ];
                    }

                    $insertCounter++;
                    if ($insertCounter > 99) {
                        $this->productPriceRepository->insertPriceData($insertData);
                        $insertData = [];
                        $insertCounter = 0;
                    }
                }
            }
        }

        $this->productPriceRepository->insertPriceData($insertData);
        $data['mailData'] = $mailData;

        return $data;
    }

    /**
     * @param array $productData
     * @param \Magento\Store\Api\Data\StoreInterface $store
     *
     * @return array|null
     */
    protected function getCurrencyKey($productData, $store)
    {
        $currencyKey = null;

        if (isset($productData[3]) && $this->environmentManager->getEnvironment() === Environment::HK) {
            // hk
            $currencyKey = array_keys(self::HK_FIELDS, $store->getCurrentCurrency()->getCode());
        } elseif ($this->environmentManager->getEnvironment() === Environment::JP) {
            // jp
            $currencyKey = array_keys(self::JP_FIELDS, $store->getCurrentCurrency()->getCode());
        }

        return $currencyKey;
    }
}
