<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection as CreditmemoCollection;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory as CreditmemoCollectionFactory;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\ProcessedRepository;
use Synolia\Sync\Model\Action\ActionInterface;
use Synolia\Sync\Exception\NothingToProcessException;

/**
 * Class GetExportTVCollection
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class GetExportTVCollection implements ActionInterface
{
    /**
     * @var InvoiceCollectionFactory
     */
    protected $invoiceCollectionFactory;

    /**
     * @var CreditmemoCollectionFactory
     */
    protected $creditmemoCollectionFactory;

    /**
     * @var ProcessedRepository
     */
    protected $processedRepository;

    /**
     * GetColumbusInvoiceExportData constructor.
     *
     * @param InvoiceCollectionFactory    $invoiceCollectionFactory
     * @param CreditmemoCollectionFactory $creditmemoCollectionFactory
     * @param ProcessedRepository         $processedRepository
     * @param State                       $state
     */
    public function __construct(
        InvoiceCollectionFactory $invoiceCollectionFactory,
        CreditmemoCollectionFactory $creditmemoCollectionFactory,
        ProcessedRepository $processedRepository,
        State $state
    ) {
        $this->invoiceCollectionFactory    = $invoiceCollectionFactory;
        $this->creditmemoCollectionFactory = $creditmemoCollectionFactory;
        $this->processedRepository         = $processedRepository;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array         $params
     * @param array         $data
     * @param string        $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \RuntimeException
     * @throws NothingToProcessException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $data['invoices']     = $this->getInvoices($params);
        $data['credit_memos'] = $this->getCreditMemos($params);

        return $data;
    }

    /**
     * @param array $params
     *
     * @return InvoiceCollection
     */
    protected function getInvoices($params)
    {
        /** @var InvoiceCollection $invoiceCollection */
        $invoiceCollection = $this->invoiceCollectionFactory->create();
        $invoiceCollection = $this->filterCollection($invoiceCollection, $params);

        return $invoiceCollection->load();
    }

    /**
     * @param array $params
     *
     * @return CreditmemoCollection
     */
    protected function getCreditMemos($params)
    {
        /** @var CreditmemoCollection $creditmemoCollection */
        $creditmemoCollection = $this->creditmemoCollectionFactory->create();
        $creditmemoCollection = $this->filterCollection($creditmemoCollection, $params);

        return $creditmemoCollection->load();
    }

    /**
     * @param InvoiceCollection|CreditmemoCollection $collection
     * @param array                                  $params
     *
     * @return CreditmemoCollection|InvoiceCollection
     */
    protected function filterCollection($collection, $params)
    {
        $statusColumn = $params['statusColumn'] ?? SetExportedData::DEFAULT_STATUS_COLUMN;

        if (isset($params['replay_start_date'], $params['replay_end_date'])) {
            $date      = new \DateTime($params['replay_start_date']);
            $dateStart = $date->format('Y-m-d H:i:s');
            $date      = new \DateTime($params['replay_end_date']);
            $dateEnd   = $date->format('Y-m-d H:i:s');

            $collection->addFieldToFilter('created_at', ['gteq' => $dateStart])
                ->addFieldToFilter('created_at', ['lteq' => $dateEnd]);
        } else {
            $collection->addFieldToFilter($statusColumn, ['null' => true]);
        }

        return $collection;
    }
}
