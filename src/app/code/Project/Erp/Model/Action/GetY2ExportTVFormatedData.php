<?php

namespace Project\Erp\Model\Action;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Catalog\Model\Product;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Directory\Model\RegionFactory;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Directory\Model\CountryFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\Order\Creditmemo\Item as CreditmemoItem;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Directory\Model\ResourceModel\Region;
use Magento\Sales\Model\ResourceModel\Order\Tax\Item as TaxItem;

use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;
use Psr\Log\LoggerInterface;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

use Project\Erp\Helper\Config\Y2\Sales as HelperY2Sales;
use Project\Erp\Helper\Config\Tax\General as HelperTax;
use Project\Wms\Helper\Config as WmsHelper;
use Project\Core\Model\Environment;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Rma\Manager\EavValueManager;
use Project\Core\Manager\EnvironmentManager;
use Project\Erp\Model\Config\Source\TaxRegimeCodeType;
use Project\Sales\Helper\Config as SalesConfig;
use Synolia\DutyCalculator\Helper\TaxData;

/**
 * Class GetY2ExportTVFormatedData
 *
 * @package Project\Erp\Model\Action\ANSI
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class GetY2ExportTVFormatedData extends ErpAction implements ActionInterface
{
    const AUTHORIZED_INSTANCES = [Environment::US, Environment::EU];

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var HelperY2Sales
     */
    protected $helperY2Sales;

    /**
     * @var HelperTax
     */
    protected $helperTax;

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var EavValueManager
     */
    protected $eavValueManager;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var TaxItem
     */
    protected $taxItem;

    /**
     * @var Region
     */
    protected $region;

    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * @var TaxData
     */
    protected $taxDataHelper;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    protected $logger;

    /**
     * GetY2ExportTVFormatedData constructor.
     *
     * @param \Project\Erp\Helper\ErpCustomerId $helper
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Erp\Helper\Config\Y2\Sales $helperY2Sales
     * @param \Project\Erp\Helper\Config\Tax\General $helperTax
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Rma\Model\RmaRepository $rmaRepository
     * @param \Project\Rma\Manager\EavValueManager $eavValueManager
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Sales\Model\ResourceModel\Order\Tax\Item $taxItem
     * @param \Magento\Directory\Model\ResourceModel\Region $region
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Synolia\DutyCalculator\Helper\TaxData $taxDataHelper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        EnvironmentManager $environmentManager,
        HelperY2Sales $helperY2Sales,
        HelperTax $helperTax,
        State $state,
        RmaRepository $rmaRepository,
        EavValueManager $eavValueManager,
        CountryFactory $countryFactory,
        StoreManagerInterface $storeManager,
        ProductRepository $productRepository,
        TaxItem $taxItem,
        Region $region,
        RegionFactory $regionFactory,
        TaxData $taxDataHelper,
        LoggerInterface $logger
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone
        );

        $this->environmentManager = $environmentManager;
        $this->helperY2Sales = $helperY2Sales;
        $this->helperTax = $helperTax;
        $this->rmaRepository = $rmaRepository;
        $this->eavValueManager = $eavValueManager;
        $this->countryFactory = $countryFactory;
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->taxItem = $taxItem;
        $this->region = $region;
        $this->regionFactory = $regionFactory;
        $this->taxDataHelper = $taxDataHelper;
        $this->logger = $logger;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @phpcs:disable
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $environment = $this->environmentManager->getEnvironment();
        if (!in_array($environment, self::AUTHORIZED_INSTANCES, true)) {
            throw new \RuntimeException('Environment ' . $environment . ' is not supported');
        }

        $formatedData = [];

        foreach ($data as $type => $collection) {
            /** @var Creditmemo|Invoice $entry */
            foreach ($collection as $entry) {
                /** @var \Magento\Sales\Model\Order $order */
                $order = $entry->getOrder();
                /** @var InvoiceItem|CreditmemoItem $item */
                foreach ($entry->getItems() as $item) {
                    if ($item->getPrice() == 0) {
                        $this->logger->error(
                            'Y2 export payments product price = 0',
                            ['message' => sprintf('type : %s id : %s', $type, $item->getId())]
                        );
                        continue;
                    }

                    if ($item->getOrderItem()->getParentItem()) {
                        continue;
                    }

                    try {
                        $product = $this->productRepository->getById($item->getProductId());
                    } catch (\Exception $e) {
                        //do nothing
                        $this->logger->error(
                            'Y2 export payments product load',
                            ['message' => sprintf('type : %s id : %s', $type, $item->getProductId())]
                        );
                        continue;
                    }

                    $formatedData[] = [
                        // interface identifier
                        'MVEC1 ',
                        // sales ticket number
                        $this->upperTrunc($entry->getIncrementId(), 20),
                        // establishment code
                        $this->upperTrunc(
                            $this->wmsHelper->getEstablishmentCode($entry->getStore()->getWebsiteId()),
                            3
                        ),
                        // stock code
                        $this->upperTrunc($this->getStockCode($entry, $type), 3),
                        // customer code
                        $this->upperTrunc($this->getCustomerIdentifierCode($order), 17),
                        // document creation date
                        $this->upperTrunc(
                            $this->convertDateTime($entry->getCreatedAt(), 'dmY', $entry->getStoreId()),
                            10
                        ),
                        // product barcode
                        $this->upperTrunc($item->getSku(), 18),
                        // product quantity
                        $this->upperTrunc($this->getQty($item), 12),
                        // income/refund
                        $this->upperTrunc($this->getIncomeRefund($entry), 1),
                        // tax amount
                        $this->upperTrunc(
                            number_format(
                                $this->getTaxAmount($entry, $item),
                                2,
                                '.',
                                ''
                            ),
                            12
                        ),
                        // item price (with tax)
                        $this->upperTrunc(
                            number_format($item->getPriceInclTax(), 2, '.', ''),
                            12
                        ),
                        // item price (without tax)
                        $this->upperTrunc(
                            number_format($item->getPrice(), 2, '.', ''),
                            12
                        ),
                        // currency code
                        $this->upperTrunc($entry->getOrderCurrencyCode(), 3),
                        // tax code
                        $this->upperTrunc($order->getShippingAddress()->getCountryId(), 3),
                        // tax regime code
                        $this->upperTrunc($this->getTaxRegimeCode($entry), 3),
                        // tax family code
                        $this->upperTrunc($this->getTaxFamilyCode($product), 3),
                        // tax rate
                        $this->upperTrunc($this->getTaxRate($entry, $item), 6),
                        // reduced price reason code (always empty)
                        '',
                        // reduced price percentage (always empty)
                        '',
                        // return reason code
                        $this->upperTrunc($this->getReturnReasonCode($entry, $type), 3),
                        // file creation date
                        $this->upperTrunc(
                            $this->convertDateTime($entry->getCreatedAt(), 'dmY', $entry->getStoreId()),
                            20
                        ),
                        // magento order number
                        $this->upperTrunc($order->getIncrementId(), 35),
                        // source of the order
                        $this->getSourceOfTheOrder($order),
                        // local/tourist
                        'OCLLOCAL',
                        // invoice/cm creation datetime
                        $this->upperTrunc(
                            $this->convertDateTime($entry->getCreatedAt(), 'dmYHis', $entry->getStoreId()),
                            14
                        ),
                        // sales associate code
                        '',
                        // tax family code 2
                        $this->upperTrunc($this->getTaxFamilyCode2($entry, $product), 3),
                        // tax rate 2
                        $this->upperTrunc($this->getTaxRate2($entry, $item), 6),
                        // product total tax amount 2
                        $this->upperTrunc($this->getProductTotalTaxAmount2($entry, $item), 12),
                    ];

                    $entry->setDuty(
                        $entry->getDuty() + (double)$this->taxDataHelper->getDutyByOrderItemId($item->getId())
                    );
                }

                // shipping
                if ($type === 'invoices' && $entry->getShippingAmount()) {
                    $taxRate = $this->getTaxRate($entry, null, $entry->getShippingAmount());

                    $formatedData[] = $this->getVirtualProductData(
                        OrderInterface::SHIPPING_AMOUNT,
                        $entry,
                        $type,
                        '+',
                        [
                            'tax_amount' => number_format(
                                $entry->getShippingAmount() * ((100 + $taxRate) / 100)
                                - $entry->getShippingAmount(),
                                3,
                                '.',
                                ''
                            ),
                            'price_without_tax' => $entry->getShippingAmount(),
                            'price_with_tax' => $entry->getShippingInclTax(),
                            'tax_rate' => $taxRate,
                        ]
                    );
                }

                if ($type === 'credit_memos') {
                    if ($this->isCanada($entry)) {
                        $taxRate = $this->getTaxRateByRegex(
                            $entry,
                            null,
                            $entry->getShippingAmount(),
                            '(GST)'
                        );
                    } else {
                        $taxRate = $entry->getAdjustmentTaxRate();
                    }

                    // shipping
                    if ($entry->getAdjustmentShippingFees()
                        && $entry->getAdjustmentShippingFees() != 0) {
                        $taxAmount = $entry->getAdjustmentShippingFees() - $entry->getBaseAdjustmentShippingFees();

                        $formatedData[] = $this->getVirtualProductData(
                            SalesConfig::FIELD_AJUSTMENT_SHIPPING_FEES,
                            $entry,
                            $type,
                            '-',
                            [
                                'tax_amount' => $taxAmount,
                                'tax_rate' => number_format($taxRate, 3, '.', ''),
                            ]
                        );
                    }

                    // repair
                    if ($entry->getAdjustmentRepairingFees()
                        && $entry->getAdjustmentRepairingFees() != 0) {
                        $taxAmount = $entry->getAdjustmentRepairingFees() - $entry->getBaseAdjustmentRepairingFees();

                        $formatedData[] = $this->getVirtualProductData(
                            SalesConfig::FIELD_AJUSTMENT_REPAIRING_FEES,
                            $entry,
                            $type,
                            '+',
                            [
                                'tax_amount' => $taxAmount,
                                'tax_rate' => number_format($taxRate, 3, '.', ''),
                            ]
                        );
                    }

                    // credit note
                    if ($entry->getAdjustmentCreditNote()
                        && $entry->getAdjustmentCreditNote() != 0) {
                        $taxAmount = $entry->getAdjustmentCreditNote() - $entry->getBaseAdjustmentCreditNote();

                        $formatedData[] = $this->getVirtualProductData(
                            SalesConfig::FIELD_AJUSTMENT_CREDIT_NOTE,
                            $entry,
                            $type,
                            '-',
                            [
                                'tax_amount' => $taxAmount,
                                'tax_rate' => number_format($taxRate, 3, '.', ''),
                            ]
                        );
                    }

                    // charge return
                    if ($entry->getAdjustmentReturnCharge()
                        && $entry->getAdjustmentReturnCharge() != 0) {
                        $taxAmount = $entry->getAdjustmentReturnCharge() - $entry->getBaseAdjustmentReturnCharge();

                        $formatedData[] = $this->getVirtualProductData(
                            SalesConfig::FIELD_AJUSTMENT_RETURN_CHARGE,
                            $entry,
                            $type,
                            '+',
                            [
                                'tax_amount' => $taxAmount,
                                'tax_rate' => number_format($taxRate, 3, '.', ''),
                            ]
                        );
                    }
                }
            }
        }

        $data['fileData'] = $formatedData;

        return $data;
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return string
     */
    protected function getStockCode($entry, $type)
    {
        if ($type === 'invoices') {
            return $this->wmsHelper->getStockCode($entry->getStore()->getWebsiteId());
        }

        // creditmemos
        if ($entry->getRmaId()) {
            return $this->rmaRepository->get($entry->getRmaId())->getStockCode();
        } else {
            return $this->wmsHelper->getStockCode(
                $entry->getOrder()->getStore()->getWebsiteId()
            );
        }
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function getReturnReasonCode($entry, $type)
    {
        if ($type !== 'credit_memos' || !$entry->getRmaId()) {
            return '';
        }

        $rma = $this->rmaRepository->get($entry->getRmaId());
        if ($rma->getId() === null) {
            return '';
        }

        //phpcs:ignore
        if(is_object($rma->getItems(true))){
            $item = $rma->getItems(true)->setPage(1, 1)->getFirstItem();
        }elseif(is_array($rma->getItems(true))) {
            $item = current($rma->getItems(true));
        }else {
            return '';
        }
        $attributeId = $this->eavValueManager->getOptionIdByAttributeNameAndItem('resolution', $item);

        return $this->helperY2Sales->getY2RmaResolutionCodeMappingCode($attributeId);
    }

    /**
     * @param Invoice|Creditmemo $entry
     *
     * @return string
     */
    protected function getIncomeRefund($entry)
    {
        return $entry instanceof Invoice ? '+' : '-';
    }

    /**
     * @param Invoice|Creditmemo $entry
     *
     * @return string
     */
    protected function getTaxRegimeCode($entry)
    {
        $scopeCode = $entry->getOrder()->getStoreId();
        $codeType = $this->helperTax->getTaxRegimeCodeType($scopeCode);

        if ($codeType === TaxRegimeCodeType::COUNTRY_NAME) {
            $store = $this->storeManager->getStore($scopeCode);
            $country = $this->countryFactory->create()->loadByCode(substr($store->getCode(), 0, 2));

            return $country->getName();
        } elseif ($codeType === TaxRegimeCodeType::STATE_CODE) {
            $region = $this->regionFactory->create();
            $this->region->loadByName(
                $region,
                $entry->getOrder()->getShippingAddress()->getRegion(),
                $entry->getOrder()->getShippingAddress()->getCountryId()
            );

            return $region->getCode();
        } elseif ($codeType === TaxRegimeCodeType::COUNTRY_CODE_1) {
            return mb_strtoupper(sprintf(
                '%s1',
                substr($this->storeManager->getStore($scopeCode)->getCode(), 0, 2)
            ));
        } elseif ($codeType === TaxRegimeCodeType::SPECIFIC_VALUE) {
            return $this->helperTax->getTaxRegimeCodeValue($scopeCode);
        }

        return '';
    }

    /**
     * @param Order $order
     *
     * @return string
     */
    protected function getSourceOfTheOrder($order)
    {
        return $order->getSourceCode() ?: '';
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool $shipping
     *
     * @return string
     */
    protected function getTaxAmount($entry, $item = null, $shipping = false)
    {
        if ($this->isCanada($entry)) {
            return $this->getTaxItemsByRegex($entry, $item, $shipping, '(GST)');
        }

        return $item->getTaxAmount();
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool|string|double|float $shipping
     *
     * @return string
     */
    protected function getTaxRate($entry, $item = null, $shipping = false)
    {
        if ($this->isCanada($entry)) {
            return number_format(
                $this->getTaxRateByRegex($entry, $item, $shipping, '(GST)'),
                3,
                '.',
                ''
            );
        }

        return number_format(
            $this->getItemTaxRate($entry, $item, $shipping),
            3,
            '.',
            ''
        );
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool|string|double|float $shipping
     *
     * @return string
     */
    protected function getTaxRate2($entry, $item = null, $shipping = false)
    {
        if ($this->isCanada($entry)) {
            return number_format(
                $this->getTaxRateByRegex($entry, $item, $shipping),
                3,
                '.',
                ''
            );
        }

        return '';
    }

    /**
     * @param Product $product
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function getTaxFamilyCode($product)
    {
        return $this->helperTax->getTaxFamilyCodeMappingCode($product->getAttributeSetId());
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param Product $product
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function getTaxFamilyCode2($entry, $product)
    {
        if ($this->isCanada($entry)) {
            return $this->helperTax->getTaxFamilyCodeMappingCode($product->getAttributeSetId());
        }

        return '';
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool $shipping
     *
     * @return string
     */
    protected function getProductTotalTaxAmount2($entry, $item = null, $shipping = false)
    {
        if ($this->isCanada($entry)) {
            return number_format(
                $this->getTaxItemsByRegex($entry, $item, $shipping),
                2,
                '.',
                ''
            );
        }

        return '';
    }

    /**
     * @param Invoice|Creditmemo $entry
     *
     * @return bool
     */
    protected function isCanada($entry)
    {
        return 0 === strpos($entry->getStore()->getCode(), 'ca');
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool $shipping
     * @param string $pattern
     *
     * @return int
     */
    protected function getTaxItemsByRegex($entry, $item = null, $shipping = false, $pattern = '(HST|QST|PST)')
    {
        $total = 0;

        foreach ($this->taxItem->getTaxItemsByOrderId($entry->getOrder()->getId()) as $tax) {
            if (preg_match($pattern, $tax['title']) === 1) {
                if (($shipping && $tax['taxable_item_type'] === CommonTaxCollector::ITEM_TYPE_SHIPPING)
                    || ($item && $tax['item_id'] === $item->getOrderItemId())
                ) {
                    $total += $tax['real_amount'];
                }
            }
        }

        return $total;
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool $shipping
     * @param string $pattern
     *
     * @return int
     */
    protected function getTaxRateByRegex($entry, $item = null, $shipping = false, $pattern = '(HST|QST|PST)')
    {
        $taxRate = 0;

        foreach ($this->taxItem->getTaxItemsByOrderId($entry->getOrder()->getId()) as $tax) {
            if (preg_match($pattern, $tax['title']) === 1) {
                if (($shipping && $tax['taxable_item_type'] === CommonTaxCollector::ITEM_TYPE_SHIPPING)
                    || ($item && $tax['item_id'] === $item->getOrderItemId())
                ) {
                    $taxRate += $tax['tax_percent'];
                }
            }
        }

        return $taxRate;
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param InvoiceItem|CreditmemoItem $item
     * @param bool $shipping
     *
     * @return int
     */
    protected function getItemTaxRate($entry, $item = null, $shipping = false)
    {
        $taxRate = 0;

        foreach ($this->taxItem->getTaxItemsByOrderId($entry->getOrder()->getId()) as $tax) {
            if (($shipping && $tax['taxable_item_type'] === CommonTaxCollector::ITEM_TYPE_SHIPPING)
                || ($item && $tax['item_id'] === $item->getOrderItemId())
            ) {
                $taxRate += $tax['tax_percent'];
            }
        }

        return $taxRate;
    }

    /**
     * @param InvoiceItem|CreditmemoItem $item
     *
     * @return int
     */
    protected function getQty($item)
    {
        if (in_array($item->getSku(), $this->helperY2Sales->getY2VirtualReferences(), false)) {
            return 1;
        }

        return number_format($item->getQty());
    }

    /**
     * @param string $field
     * @param Invoice|Creditmemo $entry
     * @param string $type
     * @param string $incomeRefund
     * @param array $amounts
     *
     * @return array
     */
    protected function getVirtualProductData($field, $entry, $type, $incomeRefund, $amounts)
    {
        $barcode = $this->upperTrunc(
            $this->helperY2Sales->getY2VirtualReference($field),
            18
        );

        if (!isset($amounts['price_without_tax'], $amounts['price_with_tax'])) {
            $amounts['price_without_tax'] = $entry->getData(sprintf('base_%s', $field));
            $amounts['price_with_tax'] = $entry->getData($field);

            $taxRate2 = (float)$this->getTaxRateByRegex(
                $entry,
                null,
                true
            );

            if ($this->isCanada($entry)) {
                $taxAmount2 = number_format(
                    $amounts['price_without_tax'] * ($taxRate2 / 100),
                    2,
                    '.',
                    ''
                );

                $amounts['tax_amount'] -= $taxAmount2;
            } else {
                $taxAmount2 = '';
            }
        } else {
            $taxAmount2 = $this->getProductTotalTaxAmount2($entry, null, true);

            $taxAmount2 = $taxAmount2 ? number_format(
                $taxAmount2,
                2,
                '.',
                ''
            ) : '';
        }

        return [
            // interface identifier
            'MVEC1 ',
            // sales ticket number
            $this->upperTrunc($entry->getIncrementId(), 20),
            // establishment code
            $this->upperTrunc(
                $this->wmsHelper->getEstablishmentCode($entry->getStore()->getWebsiteId()),
                3
            ),
            // stock code
            $this->upperTrunc($this->getStockCode($entry, $type), 3),
            // customer code
            $this->upperTrunc($this->getCustomerIdentifierCode($entry->getOrder()), 17),
            // document creation date
            $this->upperTrunc(
                $this->convertDateTime($entry->getCreatedAt(), 'dmY', $entry->getStoreId()),
                10
            ),
            // product barcode
            $barcode,
            // product quantity
            '1',
            // income/refund
            $incomeRefund,
            // tax amount
            $this->upperTrunc(
                number_format($amounts['tax_amount'], 2, '.', ''),
                12
            ),
            // item price (with tax)
            $this->upperTrunc(
                number_format($amounts['price_with_tax'], 2, '.', ''),
                12
            ),
            // item price (without tax)
            $this->upperTrunc(
                number_format($amounts['price_without_tax'], 2, '.', ''),
                12
            ),
            // currency code
            $this->upperTrunc($entry->getOrderCurrencyCode(), 3),
            // tax code
            $this->upperTrunc($entry->getOrder()->getShippingAddress()->getCountryId(), 3),
            // tax regime code
            $this->upperTrunc($this->getTaxRegimeCode($entry), 3),
            // tax family code
            'CT3',
            // tax rate
            $this->upperTrunc($amounts['tax_rate'], 6),
            // reduced price reason code (always empty)
            '',
            // reduced price percentage (always empty)
            '',
            // return reason code
            '',
            // file creation date
            $this->upperTrunc(
                $this->convertDateTime($entry->getCreatedAt(), 'dmY', $entry->getStoreId()),
                20
            ),
            // magento order number
            $this->upperTrunc($entry->getOrder()->getIncrementId(), 35),
            // source of the order
            $this->getSourceOfTheOrder($entry->getOrder()),
            // local/tourist
            'OCLLOCAL',
            // invoice/cm creation datetime
            $this->upperTrunc(
                $this->convertDateTime($entry->getCreatedAt(), 'dmYHis', $entry->getStoreId()),
                14
            ),
            // sales associate code
            '',
            // tax family code 2
            $this->isCanada($entry) ? 'CT3' : '',
            // tax rate 2
            $this->upperTrunc($this->getTaxRate2($entry, null, true), 6),
            // product total tax amount 2
            $this->upperTrunc($taxAmount2, 12),
        ];
    }
}
