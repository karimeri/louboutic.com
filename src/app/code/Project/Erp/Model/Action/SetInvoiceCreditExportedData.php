<?php

namespace Project\Erp\Model\Action;

use Magento\Sales\Model\ResourceModel\Collection\AbstractCollection;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class SetInvoiceCreditExportedData
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class SetInvoiceCreditExportedData implements ActionInterface
{
    const DEFAULT_STATUS_COLUMN = 'exported';
    const TABLE_CREDITMEMO = 'sales_creditmemo';
    const TABLE_INVOICE = 'sales_invoice';

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $statusColumn = $params['statusColumn'] ?? self::DEFAULT_STATUS_COLUMN;

        foreach ($data as $collection) {
            if (!$collection instanceof AbstractCollection) {
                continue;
            }

            $invoiceIds = $collection->getColumnValues('invoice_id');
            $creditmemoIds = $collection->getColumnValues('creditmemo_id');

            $collection->getConnection()
                ->update(
                    self::TABLE_INVOICE,
                    [$statusColumn => 1],
                    ['entity_id IN (?)' => $invoiceIds]
                );

            $collection->getConnection()
                ->update(
                    self::TABLE_CREDITMEMO,
                    [$statusColumn => 1],
                    ['entity_id IN (?)' => $creditmemoIds]
                );
        }

        return $data;
    }
}
