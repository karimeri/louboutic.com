<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection as CreditmemoCollection;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory as CreditmemoCollectionFactory;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class GetExportTVMailCollection
 *
 * @package Project\Erp\Model\Action
 */
class GetExportTVMailCollection implements ActionInterface
{
    /**
     * @var InvoiceCollectionFactory
     */
    protected $invoiceCollectionFactory;

    /**
     * @var CreditmemoCollectionFactory
     */
    protected $creditmemoCollectionFactory;

    /**
     * @var State
     */
    protected $state;

    /**
     * GetExportTVMailCollection constructor.
     * @param InvoiceCollectionFactory $invoiceCollectionFactory
     * @param CreditmemoCollectionFactory $creditmemoCollectionFactory
     * @param State $state
     */
    public function __construct(
        InvoiceCollectionFactory $invoiceCollectionFactory,
        CreditmemoCollectionFactory $creditmemoCollectionFactory,
        State $state
    )
    {
        $this->invoiceCollectionFactory     = $invoiceCollectionFactory;
        $this->creditmemoCollectionFactory  = $creditmemoCollectionFactory;
        $this->state                        = $state;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array
     * @throws \Exception
     */
    public function execute(array $params, array $data, $flowCode, ConsoleOutput $consoleOutput)
    {
        $data['invoices']     = $this->getInvoices($params);
        $data['credit_memos'] = $this->getCreditMemos($params);

        return $data;
    }

    /**
     * @param $params
     * @return InvoiceCollection
     * @throws \Exception
     */
    protected function getInvoices($params)
    {
        /** @var InvoiceCollection $invoiceCollection */
        $invoiceCollection = $this->invoiceCollectionFactory->create();
        $invoiceCollection = $this->filterCollection($invoiceCollection, $params);

        return $invoiceCollection->load();
    }

    /**
     * @param $params
     * @return CreditmemoCollection
     * @throws \Exception
     */
    protected function getCreditMemos($params)
    {
        /** @var CreditmemoCollection $creditmemoCollection */
        $creditmemoCollection = $this->creditmemoCollectionFactory->create();
        $creditmemoCollection = $this->filterCollection($creditmemoCollection, $params);

        return $creditmemoCollection->load();
    }

    /**
     * @param $collection
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function filterCollection($collection, $params)
    {
        if (!empty($params['replay_start_date']) && !empty($params['replay_end_date'])) {
            $date      = new \DateTime($params['replay_start_date']);
            $dateStart = $date->format('Y-m-d H:i:s');
            $date      = new \DateTime($params['replay_end_date']);
            $dateEnd   = $date->format('Y-m-d H:i:s');

            $collection->addFieldToFilter('created_at', ['gteq' => $dateStart])
                ->addFieldToFilter('created_at', ['lteq' => $dateEnd]);
        } else {
            $date      = new \DateTime('today');
            $dateStart = $date->format('Y-m-d H:i:s');

            $collection->addFieldtoFilter('created_at', ['gteq' => $dateStart]);
        }

        return $collection;
    }
}
