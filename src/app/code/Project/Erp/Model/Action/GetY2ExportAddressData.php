<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Project\Erp\Helper\Config\Address as HelperConfig;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class GetY2ExportAddressData
 * @package Project\Erp\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class GetY2ExportAddressData extends ErpAction implements ActionInterface
{
    /**
     * @var HelperConfig
     */
    protected $helperConfig;

    /**
     * GetY2ExportAddressData constructor.
     *
     * @param \Project\Erp\Helper\ErpCustomerId $helper
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Project\Erp\Helper\Config\Address $helperConfig
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        HelperConfig $helperConfig,
        State $state
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone
        );

        $this->helperConfig = $helperConfig;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $fileData = [];

        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $data[GetErpExportOrderCollection::ORDER_COLLECTION_KEY];

        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orderCollection->getItems() as $order) {
            $address = $order->getShippingAddress();
            $websiteId = $order->getStore()->getWebsiteId();

            // Interface Identifier
            $line = [
                "MLIC1 ",
                // Address Id
                $address->getEntityId(),
                // Code Tiers (Customer Identifier Code)
                $this->getCustomerIdentifierCode($order),
                // Address intern #
                1,
                // Customer Title
                $this->helperConfig->getY2Title($address->getPrefix()),
                // Customer Name
                $this->upperTrunc($address->getLastname(), 35),
                // Customer First Name
                $this->upperTrunc($address->getFirstname(), 35),
                // Address Line 1
                $this->upperTrunc($this->getAddressLine($address, 0), 38),
                // Address Line 2
                $this->upperTrunc($this->getAddressLine($address, 1), 38),
                // State Information
                $this->upperTrunc(
                    $this->helperConfig->getUseStateCodes($websiteId) ? $address->getRegion() : '',
                    38
                ),
                // Postal Code
                $this->upperTrunc($address->getPostcode(), 38),
                // State Code
                $this->upperTrunc(
                    $this->helperConfig->getUseStateCodes($websiteId) ? $address->getRegionCode() : '',
                    3
                ),
                // City
                $this->upperTrunc($address->getCity(), 38),
                // Customer Phone Number
                $address->getTelephone(),
                // Address Creation Date
                $this->getOrderCreationDate($order),
                // Customer Email Address
                $order->getCustomerEmail(),
                // Country Code
                $address->getCountryId(),
            ];

            $fileData[] = $line;
        }

        $data['fileData'] = $fileData;

        return $data;
    }

    /**
     * Get Address line
     *
     * @param \Magento\Sales\Api\Data\OrderAddressInterface $address
     * @param int $index
     *
     * @return string
     */
    protected function getAddressLine($address, $index = 0)
    {
        $streetArray = $address->getStreet();

        return $streetArray[$index] ?? "";
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     */
    protected function getOrderCreationDate($order)
    {
        return $this->convertDateTime($order->getCreatedAt(), 'dmY', $order->getStoreId());
    }
}
