<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Csv;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class GetImportPriceData
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class GetImportPriceData implements ActionInterface
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Framework\File\Csv
     */
    private $csvProcessor;

    /**
     * @param Filesystem $filesystem
     * @param State $state
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        Filesystem $filesystem,
        State $state,
        Csv $csvProcessor
    ) {
        $this->filesystem = $filesystem;
        $this->csvProcessor = $csvProcessor;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Exception
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $delimiter = $params['delimiter'] ?? "\t";
        $fileData = [];
        $reader = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        foreach ($reader->read($params['path']) as $filePath) {
            $fileData = array_merge($fileData, $this->getData($reader->getAbsolutePath($filePath), $delimiter));
        }

        $data['import_price'] = $fileData;

        return $data;
    }

    /**
     * @param string $filePath
     * @param string $delimiter
     *
     * @return array
     * @throws \Exception
     */
    protected function getData($filePath, $delimiter)
    {
        $csv = $this->csvProcessor->setDelimiter($delimiter);

        return $csv->getData($filePath);
    }
}
