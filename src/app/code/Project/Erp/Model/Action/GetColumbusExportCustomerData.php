<?php

namespace Project\Erp\Model\Action;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Project\Erp\Block\Adminhtml\Form\Field\CustomerGroup;
use Project\Erp\Helper\Config\Columbus\Customer as HelperConfig;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class GetColumbusExportCustomerData
 * @package Project\Erp\Model\Action
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class GetColumbusExportCustomerData extends ErpAction implements ActionInterface
{
    /**
     * @var \Project\Erp\Helper\Config\Columbus\Customer
     */
    protected $helperConfig;

    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * GetY2ExportCustomerData constructor.
     *
     * @param \Project\Erp\Helper\ErpCustomerId $helper
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Project\Erp\Helper\Config\Columbus\Customer $helperConfig
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        HelperConfig $helperConfig,
        CustomerRegistry $customerRegistry,
        State $state
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone
        );

        $this->helperConfig = $helperConfig;
        $this->customerRegistry = $customerRegistry;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $fileData = [];

        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $data[GetErpExportOrderCollection::ORDER_COLLECTION_KEY];

        /** @var \Magento\Sales\Model\Order $order */
        foreach ($orderCollection->getItems() as $order) {
            try {
                $customer = $this->customerRegistry->retrieve($order->getCustomerId());
            } catch (NoSuchEntityException $exception) {
                $customer = null;
            }
            $fileData[] = $this->getCustomerLine($order, $customer);
            $fileData[] = $this->getClassification1Line($order, $customer);
            $fileData[] = $this->getClassification3Line($order, $customer);
            $fileData[] = $this->getCustomerFieldLine($order, $customer);
        }

        $data['fileData'] = $fileData;

        return $data;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return array
     */
    protected function getCustomerLine($order, $customer)
    {
        $shippingAddress = $order->getShippingAddress();

        // Line Type
        $line = [
            'CD',
            // Customer Identifier Code
            $this->getCustomerIdentifierCode($order),
            // Customer Name
            $this->upperTrunc($shippingAddress->getLastname(), 30),
            // Customer Middle Name
            '',
            // Customer First Name
            $this->upperTrunc($shippingAddress->getFirstname(), 60),
            // Date of Birth (DAY)
            '',
            // Date of Birth (MONTH)
            '',
            // Date of Birth (YEAR)
            '',
            // Customer Sex
            $this->getGender($customer),
            // Age Start
            '',
            // Age End
            '',
            // Label
            '',
            // Address Line 1
            $this->upperTrunc($this->getAddressLine($shippingAddress, 0), 38),
            // Address Line 2
            $this->upperTrunc($this->getAddressLine($shippingAddress, 1), 38),
            // Address Line 3
            $this->upperTrunc($this->getAddressLine($shippingAddress, 2), 38),
            // Address Line 4
            $this->upperTrunc($this->getAddressLine($shippingAddress, 3), 38),
            // City
            $this->upperTrunc($shippingAddress->getCity(), 50),
            // Postal Code
            $this->upperTrunc($shippingAddress->getPostcode(), 10),
            // City
            $this->upperTrunc($shippingAddress->getCity(), 32),
            // Country Code
            $this->upperTrunc($shippingAddress->getCountryId(), 3),
            // Customer Phone Number #1
            $shippingAddress->getFax(),
            // Customer Phone Number #2
            '',
            // Customer Phone Number #3
            $shippingAddress->getTelephone(),
            // Customer Phone Number #4
            '',
            // Customer Phone Number #5
            '',
            // Customer Email Address
            $this->upperTrunc($order->getCustomerEmail(), 80),
            // Customer Civility
            $this->getCivility($customer),
            // Mother Code
            '',
            // Fidelity Card
            '',
            // Proprietary Establishment
            '',
            // Creation Date
            '',
            // Invoice Mode
            '',
        ];

        return $line;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return array
     */
    protected function getClassification1Line($order, $customer)
    {
        $line = [
            // Line Type
            'CL',
            // Customer Identifier Code
            $this->getCustomerIdentifierCode($order),
            // Classification Level
            1,
            // Class Code
            $this->helperConfig->getColumbusCustomerCategoryCode($this->getGroupId($customer)),
        ];

        return $line;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return array
     */
    protected function getClassification3Line($order, $customer)
    {
        $line = [
            // Line Type
            'CL',
            // Customer Identifier Code
            $this->getCustomerIdentifierCode($order),
            // Classification Level
            3,
            // Class Code
            $this->helperConfig->getColumbusCustomerCategoryEcomCode($this->getGroupId($customer)),
        ];

        return $line;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return array
     */
    protected function getCustomerFieldLine($order, $customer)
    {
        $line = [
            // Line Type
            'CU',
            // Customer Identifier Code
            $this->getCustomerIdentifierCode($order),
            // Customer Field Number
            165,
            // Entity ID
            $customer ? $customer->getId() : '',
        ];

        return $line;
    }

    /**
     * Get Address line
     *
     * @param \Magento\Sales\Api\Data\OrderAddressInterface $address
     * @param int $index
     *
     * @return string
     */
    protected function getAddressLine($address, $index = 0)
    {
        $street = $address->getStreet();

        if (!\is_array($street)) {
            return '';
        }

        $streetArray = $this->getAddressArray(\implode(' ', $street), 35);

        return $streetArray[$index] ?? '';
    }

    /**
     * Return "true" if male, "false" if female
     *
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return string
     */
    protected function getGender($customer)
    {
        if ($customer instanceof Customer) {
            $gender = $this->getCustomerGender($customer);

            if ($gender) {
                switch ($gender) {
                    case 'Male':
                        return 'true';
                    case 'Female':
                        return 'false';
                }
            }

            $prefix = $customer->getPrefix();
            switch ($prefix) {
                case 'Mr.':
                    return 'true';
                case 'Mrs.':
                case 'Miss':
                    return 'false';
            }
        }

        return '';
    }

    /**
     * @param Customer $customer
     *
     * @return null|string
     */
    protected function getCustomerGender($customer)
    {
        $gender = $customer->getGender();

        if ($gender !== null && $gender !== 0) {
            $gender = $customer->getResource()->getAttribute('gender')->getSource()->getOptionText($gender);
        }

        return $gender;
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return string
     */
    protected function getCivility($customer)
    {
        if ($customer instanceof Customer) {
            $civility = $customer->getPrefix();

            if ($civility === null) {
                $gender = $this->getCustomerGender($customer);

                if ($gender) {
                    switch ($gender) {
                        case 'Male':
                            $civility = 'Mr.';
                            break;
                        case 'Female':
                            $civility = 'Mrs.';
                            break;
                    }
                }
            }

            return $this->helperConfig->getColumbusCivilityCode($civility);
        }

        return '';
    }

    /**
     * @param string $street
     * @param int $length
     *
     * @return array
     */
    protected function getAddressArray($street, $length)
    {
        return \explode("\n", \wordwrap($street, $length, "\n"));
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     *
     * @return int|null|string
     */
    protected function getGroupId($customer)
    {
        $groupId = CustomerGroup::GUEST_GROUP_ID;
        if ($customer instanceof Customer) {
            $groupId = $customer->getGroupId();
        }

        return $groupId;
    }
}
