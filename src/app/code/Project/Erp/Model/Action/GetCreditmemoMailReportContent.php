<?php

namespace Project\Erp\Model\Action;

use Magento\Rma\Api\RmaRepositoryInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Catalog\Model\ProductRepository;
use Magento\Rma\Helper\Eav as RmaEavHelper;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Collection as CreditmemoCollection;

use Project\Rma\Model\Rma;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

use Project\Erp\Helper\ExportSalesTickets as HelperExportSalesTickets;
use Project\Erp\Helper\Config;
use Project\Erp\Helper\Config\Y2\Sales as HelperY2Sales;
use Project\Core\Helper\AttributeSet;
use Project\ExchangeOffline\Model\Order\CreditmemoFactory;

/**
 * Class GetCreditmemoMailReportContent
 *
 * @package Project\Erp\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class GetCreditmemoMailReportContent implements ActionInterface
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * @var Config
     */
    protected $helperConfig;

    /**
     * @var HelperY2Sales
     */
    protected $helperY2Sales;

    /**
     * @var HelperExportSalesTickets
     */
    protected $helperExportSalesTickets;

    /**
     * @var array
     */
    protected $invoiceData;

    /**
     * @var array
     */
    protected $creditmemoData;

    /**
     * @var RmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var RmaEavHelper
     */
    protected $rmaEavHelper;

    /**
     * @param ProductRepository $productRepository
     * @param AttributeSet $attributeSetHelper
     * @param Config $helperConfig
     * @param HelperY2Sales $helperY2Sales
     * @param HelperExportSalesTickets $helperExportSalesTickets
     * @param RmaRepositoryInterface $rmaRepository
     * @param RmaEavHelper $rmaEavHelper
     */
    public function __construct(
        ProductRepository $productRepository,
        AttributeSet $attributeSetHelper,
        Config $helperConfig,
        HelperY2Sales $helperY2Sales,
        HelperExportSalesTickets $helperExportSalesTickets,
        RmaRepositoryInterface $rmaRepository,
        RmaEavHelper $rmaEavHelper
    ) {
        $this->productRepository = $productRepository;
        $this->attributeSetHelper = $attributeSetHelper;
        $this->helperConfig = $helperConfig;
        $this->helperY2Sales = $helperY2Sales;
        $this->helperExportSalesTickets = $helperExportSalesTickets;
        $this->rmaRepository = $rmaRepository;
        $this->rmaEavHelper = $rmaEavHelper;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $this->invoiceData = $data['invoiceData'];
        $this->processCreditmemos($data['credit_memos'],$params);
        $data['creditmemoData'] = $this->creditmemoData;

        return $data;
    }

    /**
     * @param CreditmemoCollection $creditmemos
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    // phpcs:disable
    protected function processCreditmemos($creditmemos,$params)
    {
        $currencies = $this->helperExportSalesTickets->getCurrencies();
        $initArray = $this->helperExportSalesTickets->initArray($currencies);
        $creditMemoDate = date('d/m') ;
        if(count($params) != 0 && (isset($params["replay_start_date"]) || isset($params["replay_end_date"]))){
            $creditMemoDate = '';
            $creditMemoDate .= (isset($params["replay_start_date"]) && !empty($params["replay_start_date"])) ? "From " . $params["replay_start_date"] : "";
            $creditMemoDate .= (isset($params["replay_end_date"]) && !empty($params["replay_end_date"])) ? " To " . $params["replay_end_date"] : "";
        }
        $this->creditmemoData = [
            'date' =>$creditMemoDate,
            'paypalAmount' => \array_merge($initArray, ['header' => '']),
            'creditmemosCount' => $initArray,
            'exchangeCreditmemoCount' => $initArray,
            'productsCount' => $initArray,
            'womenShoesCount' => $initArray,
            'menShoesCount' => $initArray,
            'womenBagsCount' => $initArray,
            'menBagsCount' => $initArray,
            'accessoriesCount' => $initArray,
            'beltsCount' => $initArray,
            'braceletsCount' => $initArray,
            'nailsCount' => $initArray,
            'lipsCount' => $initArray,
            'fragrancesCount' => $initArray,
            'eyesCount' => $initArray,
            'otherCount' => $initArray,
            'otherSkus' => [],
            'repairFees' => $initArray,
            'shippingFees' => $initArray,
            'bankAmount' => $initArray,
            'storeCreditAmount' => $initArray,
            'exchangeCreditmemoAmount' => $initArray,
            'omnichannelCreditmemoAmount' => $initArray,
            'taxTotal' => $initArray,
            'netTotal' => $initArray,
            'netTotalShoesBagsAccessories' => $initArray,
            'netTotalBeauty' => $initArray,
            'shippingTotal' => $initArray,
            'brutTotal' => $initArray
        ];

        /** @var Creditmemo $creditmemo */
        foreach ($creditmemos as $creditmemo) {
            // invoices count
            $this->creditmemoData['creditmemosCount']['total']++;
            // invoices currency count
            $this->creditmemoData['creditmemosCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['creditmemosCount'],
                $creditmemo->getOrderCurrencyCode()
            );
            // paypal
            if ($this->helperExportSalesTickets->isPaypal($creditmemo->getOrder()->getPayment())) {
                $this->creditmemoData['paypalAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->creditmemoData['paypalAmount'],
                    $creditmemo->getOrderCurrencyCode(),
                    $creditmemo->getGrandTotal()
                );
            }

            $rmaId = $creditmemo->getData('rma_id');
            if ($rmaId) {
                /** @var Rma $rma */
                $rma = $this->rmaRepository->get($rmaId);
                // phpcs:ignore Ecg.Performance.GetFirstItem
                $rmaItem = $rma->getItemsForDisplay()->getFirstItem();
                $rmaResolutions = $this->rmaEavHelper->getAttributeOptionValues('resolution', 0);
                // exchange creditmemos count
                if (strpos($rmaResolutions[$rmaItem->getResolution()], 'Exchange') !== false) {
                    $this->creditmemoData['exchangeCreditmemoCount']['total']++;
                    $this->creditmemoData['exchangeCreditmemoCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['exchangeCreditmemoCount'],
                        $creditmemo->getOrderCurrencyCode()
                    );
                }
                // exchanged invoice amount
                if (strpos($rmaResolutions[$rmaItem->getResolution()], 'Exchange') !== false) {
                    $this->creditmemoData['exchangeCreditmemoAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['exchangeCreditmemoAmount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $creditmemo->getGrandTotal()
                    );
                }
            }
            // omnichannel creditmemos count always 0 in lot 0
            // product type count
            foreach ($creditmemo->getAllItems() as $item) {
                // if simple product
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }

                try {
                    $product = $this->productRepository->getById($item->getProductId());
                } catch (\Exception $e) {
                    // do nothing
                    continue;
                }

                if ($this->attributeSetHelper->isShoes($product)) {
                    if (strtoupper($product->getResource()->getAttribute('gender')->getFrontend()->getValue($product)) === 'MEN') {
                        $this->creditmemoData['menShoesCount']['total'] += $item->getQty();
                        $this->creditmemoData['menShoesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->creditmemoData['menShoesCount'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                    } else {
                        $this->creditmemoData['womenShoesCount']['total'] += $item->getQty();
                        $this->creditmemoData['womenShoesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->creditmemoData['womenShoesCount'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                    }
                    // net total shoes, bags, accessories
                    $this->creditmemoData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets
                        ->addToCountCurrency(
                            $this->creditmemoData['netTotalShoesBagsAccessories'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                } elseif ($this->attributeSetHelper->isBag($product)) {
                    if ($product->getResource()->getAttribute('gender')->getFrontend()->getValue($product) === 'MEN') {
                        $this->creditmemoData['menBagsCount']['total'] += $item->getQty();
                        $this->creditmemoData['menBagsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->creditmemoData['menBagsCount'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                    } else {
                        $this->creditmemoData['womenBagsCount']['total'] += $item->getQty();
                        $this->creditmemoData['womenBagsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->creditmemoData['womenBagsCount'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                    }
                    // net total shoes, bags, accessories
                    $this->creditmemoData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets
                        ->addToCountCurrency(
                            $this->creditmemoData['netTotalShoesBagsAccessories'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                } elseif ($this->attributeSetHelper->isAccessories($product)) {
                    $this->creditmemoData['accessoriesCount']['total'] += $item->getQty();
                    $this->creditmemoData['accessoriesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['accessoriesCount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getQty()
                    );
                    // net total shoes, bags, accessories
                    $this->creditmemoData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets
                        ->addToCountCurrency(
                            $this->creditmemoData['netTotalShoesBagsAccessories'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                }  elseif ($this->attributeSetHelper->isBelts($product) || $this->attributeSetHelper->isBracelets($product)) {
                    if ($this->attributeSetHelper->isBelts($product)) {
                        $this->creditmemoData['beltsCount']['total'] += $item->getQty();
                        $this->creditmemoData['beltsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->creditmemoData['beltsCount'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                    }elseif ($this->attributeSetHelper->isBracelets($product)) {
                        $this->creditmemoData['braceletsCount']['total'] += $item->getQty();
                        $this->creditmemoData['braceletsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                            $this->creditmemoData['braceletsCount'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getQty()
                        );
                    }
                    // net total shoes, bags, accessories
                    $this->creditmemoData['netTotalShoesBagsAccessories'] = $this->helperExportSalesTickets
                        ->addToCountCurrency(
                            $this->creditmemoData['netTotalShoesBagsAccessories'],
                            $creditmemo->getOrderCurrencyCode(),
                            $item->getPrice()
                        );
                } elseif ($this->attributeSetHelper->isNails($product)) {
                    $this->creditmemoData['nailsCount']['total'] += $item->getQty();
                    $this->creditmemoData['nailsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['nailsCount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getQty()
                    );
                    // net total beauty
                    $this->creditmemoData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['netTotalBeauty'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getPrice()
                    );
                } elseif ($this->attributeSetHelper->isLips($product)) {
                    $this->creditmemoData['lipsCount']['total'] += $item->getQty();
                    $this->creditmemoData['lipsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['lipsCount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getQty()
                    );
                    // net total beauty
                    $this->creditmemoData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['netTotalBeauty'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getPrice()
                    );
                } elseif ($this->attributeSetHelper->isPerfumes($product)) {
                    $this->creditmemoData['fragrancesCount']['total'] += $item->getQty();
                    $this->creditmemoData['fragrancesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['fragrancesCount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getQty()
                    );
                    // net total beauty
                    $this->creditmemoData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['netTotalBeauty'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getPrice()
                    );
                } elseif ($this->attributeSetHelper->isEyes($product)) {
                    $this->creditmemoData['eyesCount']['total'] += $item->getQty();
                    $this->creditmemoData['eyesCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['eyesCount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getQty()
                    );
                    // net total beauty
                    $this->creditmemoData['netTotalBeauty'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['netTotalBeauty'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getPrice()
                    );
                } else {
                    $this->creditmemoData['otherCount']['total'] += $item->getQty();
                    $this->creditmemoData['otherCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                        $this->creditmemoData['otherCount'],
                        $creditmemo->getOrderCurrencyCode(),
                        $item->getQty()
                    );
                    $otherSkus[] = $product->getSku();
                }

                // products count
                $this->creditmemoData['productsCount']['total'] += $item->getQty();
                $this->creditmemoData['productsCount'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->creditmemoData['productsCount'],
                    $creditmemo->getOrderCurrencyCode(),
                    $item->getQty()
                );
            }

            if ($creditmemo->getAdjustmentRepairingFees() && $creditmemo->getAdjustmentRepairingFees() > 0) {
                // repair fees
                $this->creditmemoData['repairFees']['total']++;
                $this->creditmemoData['repairFees'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->creditmemoData['repairFees'],
                    $creditmemo->getOrderCurrencyCode(),
                    1
                );
            }

            if ($creditmemo->getAdjustmentShippingFees() && $creditmemo->getAdjustmentShippingFees() > 0) {
                // shipping fees
                $this->creditmemoData['shippingFees']['total']++;
                $this->creditmemoData['shippingFees'] = $this->helperExportSalesTickets->addToCountCurrency(
                    $this->creditmemoData['shippingFees'],
                    $creditmemo->getOrderCurrencyCode(),
                    1
                );
            }

            $this->creditmemoData['bankAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['bankAmount'],
                $creditmemo->getOrderCurrencyCode(),
                $creditmemo->getGrandTotal()
            );
            // store credit amount (always 0)
            $this->creditmemoData['storeCreditAmount'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['storeCreditAmount'],
                $creditmemo->getOrderCurrencyCode(),
                $creditmemo->getData('customer_balance_amount')
            );
            // omnichannel creditmemo amount (always 0)
            // tax total
            $this->creditmemoData['taxTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['taxTotal'],
                $creditmemo->getOrderCurrencyCode(),
                $creditmemo->getTaxAmount()
            );
            // net total
            $this->creditmemoData['netTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['netTotal'],
                $creditmemo->getOrderCurrencyCode(),
                $creditmemo->getSubtotal()
            );
            // shipping total
            $this->creditmemoData['shippingTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['shippingTotal'],
                $creditmemo->getOrderCurrencyCode(),
                $creditmemo->getShippingAmount()
            );
            // brut total
            $this->creditmemoData['brutTotal'] = $this->helperExportSalesTickets->addToCountCurrency(
                $this->creditmemoData['brutTotal'],
                $creditmemo->getOrderCurrencyCode(),
                $creditmemo->getGrandTotal()
            );
        }
        // phpcs:enable
        $this->creditmemoData['currencies'] = $currencies;
        $this->creditmemoData['paypalAmount']['header'] = $this->helperExportSalesTickets->getPaypalHeader(
            $currencies,
            $this->creditmemoData['paypalAmount']
        );

        $this->creditmemoData['footer'] = $this->getFooter();
    }

    /**
     * @return string
     */
    public function getFooter()
    {
        $footers = [];
        $currencies = array_unique(array_merge(
            $this->creditmemoData['currencies'],
            $this->invoiceData['currencies']
        ));

        foreach ($currencies as $currency) {
            $total = 0;

            if (isset($this->invoiceData['brutTotal'][$currency])) {
                $total += $this->invoiceData['brutTotal'][$currency];
            }

            if (isset($this->creditmemoData['brutTotal'][$currency])) {
                $total -= $this->creditmemoData['brutTotal'][$currency];
            }

            $footers[] = sprintf('%s %s', $currency, $total);
        }

        return implode('; ', $footers);
    }
}
