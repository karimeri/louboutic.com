<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Sales\Model\Order\Creditmemo\Item as CreditmemoItem;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

use Project\Erp\Helper\Config as HelperConfig;
use Project\Erp\Helper\Config\Columbus\General as HelperColumbus;
use Project\Erp\Helper\Config\Columbus\SalesTicket as HelperColumbusSalesTickets;
use Project\Wms\Helper\Config as WmsHelper;
use Project\Core\Model\Environment;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Rma\Manager\EavValueManager;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class GetColumbusExportTVFormatedData
 *
 * @package Project\Erp\Model\Action\ANSI
 * @author  Synolia <contact@synolia.com>
 */
class GetColumbusExportTVFormatedData extends ErpAction implements ActionInterface
{
    const AUTHORIZED_INSTANCES = [Environment::HK, Environment::JP];
    const INTERFACE_IDENTIFIERS = [
        'invoices' => [
            'hk' => 'VHK07',
            'jp' => 'VJP77',
        ],
        'credit_memos' => [
            'hk' => 'AHK07',
            'jp' => 'AJP77',
        ],
    ];

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var HelperConfig
     */
    protected $helperConfig;

    /**
     * @var HelperColumbus
     */
    protected $helperColumbus;

    /**
     * @var HelperColumbusSalesTickets
     */
    protected $helperColumbusSalesTickets;

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var EavValueManager
     */
    protected $eavValueManager;

    /**
     * ArrayToCsv constructor.
     *
     * @param Helper $helper
     * @param WmsHelper $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param EnvironmentManager $environmentManager
     * @param HelperConfig $helperConfig
     * @param HelperColumbus $helperColumbus
     * @param HelperColumbusSalesTickets $helperColumbusSalesTickets
     * @param State $state
     * @param RmaRepository $rmaRepository
     * @param EavValueManager $eavValueManager
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        EnvironmentManager $environmentManager,
        HelperConfig $helperConfig,
        HelperColumbus $helperColumbus,
        HelperColumbusSalesTickets $helperColumbusSalesTickets,
        State $state,
        RmaRepository $rmaRepository,
        EavValueManager $eavValueManager
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone
        );

        $this->environmentManager = $environmentManager;
        $this->helperConfig = $helperConfig;
        $this->helperColumbus = $helperColumbus;
        $this->helperColumbusSalesTickets = $helperColumbusSalesTickets;
        $this->rmaRepository = $rmaRepository;
        $this->eavValueManager = $eavValueManager;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $environment = $this->environmentManager->getEnvironment();
        if (!in_array($environment, self::AUTHORIZED_INSTANCES, true)) {
            throw new \RuntimeException('Environment ' . $environment . ' is not supported');
        }

        $formatedData = [];

        foreach ($data as $type => $collection) {
            /** @var Creditmemo|Invoice $entry */
            foreach ($collection as $entry) {
                $formatedData[] = [
                    // line type
                    'TV',
                    // piece type
                    $this->getInterfaceIdentifier($type),
                    // piece number
                    $this->getEntryNumber($entry->getIncrementId()),
                    // society
                    $this->helperColumbus->getSocietyCode(),
                    // line subtype
                    'ET',
                    // ticket date
                    $this->convertDateTime($entry->getCreatedAt(), 'd/m/y', $entry->getStoreId()),
                    // stock code
                    $this->getStockCode($entry, $type),
                    // client code
                    $this->getCustomerIdentifierCode($entry->getOrder()),
                    // currency code
                    $entry->getOrder()->getOrderCurrencyCode(),
                    // conversion rate 1
                    '<?>',
                    // conversion rate 2
                    '<?>',
                    // conversion type 2
                    '<?>',
                    // document category
                    'VE',
                    // facturation regime
                    $this->helperColumbusSalesTickets->getInvoiceTypeMappingCode(
                        $entry->getGrandTotal(),
                        $entry->getStore()->getCode()
                    ),
                    // vendor code
                    $this->helperColumbus->getVendorCode(),
                    // season code
                    '',
                    // price code
                    $this->helperColumbusSalesTickets->getPriceCodeMappingCode(
                        $entry->getStore(),
                        $entry->getStore()->getId()
                    ),
                    // promotional event
                    'PB',
                    // validation hour
                    sprintf('%s', time() % 86400),
                    // document label
                    sprintf('(%s)', $entry->getOrder()->getIncrementId()) . "\t",
                ];

                $tvLineContent = $this->getTVLineContent($entry, $type);

                if ($tvLineContent) {
                    $formatedData = array_merge($formatedData, $tvLineContent);
                }

                $formatedData[] = $this->getPaymentMethodContent($entry, $type);
                $formatedData[] = $this->getFinancialMovement($entry, $type);
            }
        }

        $header = [
            iconv(
                'UTF-8',
                'WINDOWS-1252',
                sprintf(
                    '<ENTETE>EXP  007.000%s%s%s %s         ',
                    date('d/m/Y'),
                    date('H:i:s'),
                    $this->getPlatform($environment),
                    $this->getLocale($environment)
                )
            ),
        ];

        $data['fileData'] = array_merge([$header], $formatedData);

        return $data;
    }

    /**
     * @param string $type
     *
     * @return string
     * @throws \RuntimeException
     */
    protected function getInterfaceIdentifier($type)
    {
        return self::INTERFACE_IDENTIFIERS[$type][$this->environmentManager->getEnvironment()];
    }

    /**
     * @param string $id
     *
     * @return string
     */
    protected function getEntryNumber($id)
    {
        return sprintf(
            '%s%s%s',
            substr($id, 0, 1),
            substr($id, 3, 2),
            substr($id, 6, 9)
        );
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return string
     */
    protected function getStockCode($entry, $type)
    {
        if ($type === 'invoices') {
            return $this->wmsHelper->getStockCode($entry->getStore()->getWebsiteId());
        }

        // creditmemos
        if ($entry->getRmaId()) {
            return $this->rmaRepository->get($entry->getRmaId())->getStockCode();
        } else {
            return $this->wmsHelper->getStockCode(
                $entry->getOrder()->getStore()->getWebsiteId()
            );
        }
    }

    /**
     * @param string $environment
     *
     * @return string
     */
    protected function getPlatform($environment)
    {
        return $environment === 'hk' ? 'HKN0' : 'JPI0';
    }

    /**
     * @param string $environment
     *
     * @return string
     */
    protected function getLocale($environment)
    {
        return $environment === 'hk' ? 'FRHK' : 'FRJP';
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return array|null
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    protected function getTVLineContent($entry, $type)
    {
        $formatedData = null;

        /** @var InvoiceItem|CreditmemoItem $item */
        foreach ($entry->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            $formatedData[] = [
                // line type
                'TV',
                // piece type
                $this->getInterfaceIdentifier($type),
                // piece number
                $this->getEntryNumber($entry->getIncrementId()),
                // society
                $this->helperColumbus->getSocietyCode(),
                // line subtype
                'LT',
                // line number
                $item->getLineNumber(),
                // vendor code
                $this->helperColumbus->getVendorCode(),
                // sku code
                '',
                // barcode
                $item->getSku(),
                // sold quantity
                $this->getQty($item),
                // sold quantity
                number_format($item->getPriceInclTax(), 2, '.', ''),
                // discount amount
                '',
                // discount reason code
                '',
                // tva code
                '',
                // tva rate
                '',
                // promotional event
                '',
                // origin price
                '',
                // return reason code
                $this->getReturnReasonCode($entry, $type) . "\t",
            ];
        }

        return $formatedData;
    }

    /**
     * @param InvoiceItem|CreditmemoItem $item
     *
     * @return string
     */
    protected function getQty($item)
    {
        if ($item instanceof CreditmemoItem) {
            return sprintf('-%s', (int)$item->getQty());
        }

        return (string)(int)$item->getQty();
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function getReturnReasonCode($entry, $type)
    {
        if ($type !== 'credit_memos' || !$entry->getRmaId()) {
            return '';
        }

        $rma = $this->rmaRepository->get($entry->getRmaId());
        if ($rma->getId() === null) {
            return '';
        }

        //phpcs:ignore
        $item = $rma->getItems(true)->setPage(1, 1)->getFirstItem();
        $attributeId = $this->eavValueManager->getOptionIdByAttributeNameAndItem('resolution', $item);

        return $this->helperColumbusSalesTickets->getColumbusRmaResolutionCodeMappingCode($attributeId);
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    protected function getPaymentMethodContent($entry, $type)
    {

        return [
            // line type
            'TV',
            // piece type
            $this->getInterfaceIdentifier($type),
            // piece number
            $this->getEntryNumber($entry->getIncrementId()),
            // society
            $this->helperColumbus->getSocietyCode(),
            // line subtype
            'LM',
            // payment method code
            $this->helperConfig->getPaymentCode($entry->getOrder()->getPayment()),
            // currency code
            $entry->getOrder()->getOrderCurrencyCode(),
            // conversion rate 1
            '<?>',
            // conversion rate 2
            '<?>',
            // conversion type 2
            '<?>',
            // expiration code
            '',
            // expiration date
            '',
            // payment amount
            number_format($this->getGrandTotal($entry), 2, '.', ''),
            // payment received
            "True\t",
        ];
    }

    /**
     * @param Invoice|Creditmemo $entry
     *
     * @return string
     */
    protected function getGrandTotal($entry)
    {
        $grandTotal = $entry->getOrder()->getGrandTotal();

        if ($entry instanceof Creditmemo) {
            $grandTotal = '-' . $grandTotal;
        }

        return $grandTotal;
    }

    /**
     * @param Invoice|Creditmemo $entry
     * @param string $type
     *
     * @return array
     * @throws \RuntimeException
     */
    protected function getFinancialMovement($entry, $type)
    {
        return [
            // line type
            'TV',
            // piece type
            $this->getInterfaceIdentifier($type),
            // piece number
            $this->getEntryNumber($entry->getIncrementId()),
            // society
            $this->helperColumbus->getSocietyCode(),
            // line subtype
            'LF',
            // financial movement code
            'SH',
            // amount
            $this->getShippingAmount($entry),
            // vat code
            '00',
            // vat rate
            $this->getTaxRate($entry) . "\t",
        ];
    }

    /**
     * @param Invoice|Creditmemo $entry
     *
     * @return string
     */
    protected function getShippingAmount($entry)
    {
        $shippingAmount = $entry->getShippingAmount();

        if ($entry instanceof Creditmemo) {
            $shippingAmount = '-' . $shippingAmount;
        }

        return $shippingAmount;
    }

    /**
     * @param Invoice|Creditmemo $entry
     *
     * @return string
     */
    protected function getTaxRate($entry)
    {
        return ($entry->getBaseSubtotalInclTax() * 100) / $entry->getBaseSubtotal() - 100;
    }
}
