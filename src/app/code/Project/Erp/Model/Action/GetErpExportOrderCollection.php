<?php

namespace Project\Erp\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class GetY2ExportOrderCollection
 * @package Project\Erp\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class GetErpExportOrderCollection implements ActionInterface
{
    const ORDER_COLLECTION_KEY = 'orders';

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * GetColumbusInvoiceExportData constructor.
     *
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        State $state
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     *
     * @return array
     * @throws \RuntimeException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $data[self::ORDER_COLLECTION_KEY] = $this->getOrderCollection($params);

        return $data;
    }

    /**
     * @param array $params
     *
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected function getOrderCollection($params)
    {
        $orderCollection = $this->filterCollection($this->orderCollectionFactory->create(), $params);

        return $orderCollection->load();
    }


    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\Collection $collection
     * @param array $params
     *
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected function filterCollection($collection, $params)
    {
        $statusColumn = $params['statusColumn'] ?? SetExportedData::DEFAULT_STATUS_COLUMN;

        if (isset($params['replay_start_date'], $params['replay_end_date'])) {
            $date = new \DateTime($params['replay_start_date']);
            $dateStart = $date->format('Y-m-d H:i:s');
            $date = new \DateTime($params['replay_end_date']);
            $dateEnd = $date->format('Y-m-d H:i:s');

            $condition = sprintf(
                'WHERE created_at <= "%s" AND created_at >= "%s"',
                $dateEnd,
                $dateStart
            );
        } else {
            $condition = sprintf(
                'WHERE %s IS NULL',
                $statusColumn
            );
        }
        $subqueryInvoice = new \Zend_Db_Expr(
            sprintf(
                '(SELECT DISTINCT order_id, entity_id, created_at, %s from sales_invoice %s)',
                $statusColumn,
                $condition
            )
        );
        $subqueryCreditmemo = new \Zend_Db_Expr(
            sprintf(
                '(SELECT DISTINCT order_id, entity_id, created_at, %s from sales_creditmemo %s)',
                $statusColumn,
                $condition
            )
        );

        $collection->getSelect()->joinLeft(
            ['i' => $subqueryInvoice],
            'main_table.entity_id = i.order_id',
            [
                'invoice_id' => 'entity_id',
                'invoice_created_at' => 'created_at'
            ]
        );

        $collection->getSelect()->joinLeft(
            ['c' => $subqueryCreditmemo],
            'main_table.entity_id = c.order_id',
            [
                'creditmemo_id' => 'entity_id',
                'creditmemo_created_at' => 'created_at'
            ]
        );

        $collection->addFieldToFilter(
            ['i.entity_id', 'c.entity_id'],
            [
                ['notnull' => true],
                ['notnull' => true],
            ]
        );

        $collection->getSelect()->group('entity_id');

        return $collection;
    }
}
