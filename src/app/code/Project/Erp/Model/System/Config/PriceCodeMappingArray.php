<?php

namespace Project\Erp\Model\System\Config;

use Magento\Config\Model\Config\Backend\Serialized\ArraySerialized;

/**
 * Class PriceCodeMappingArray
 *
 * @package Project\Erp\Model\System\Config
 * @author  Synolia <contact@synolia.com>
 */
class PriceCodeMappingArray extends ArraySerialized
{
    /**
     * @return ArraySerialized
     */
    // @codingStandardsIgnoreLine
    public function beforeSave()
    {
        // For value validations
        $exceptions = $this->getValue();

        // Validations
        $this->setValue($exceptions);

        return parent::beforeSave();
    }
}
