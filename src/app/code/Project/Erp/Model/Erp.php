<?php
namespace Project\Erp\Model;

use Magento\Framework\Option\ArrayInterface;

use Project\Erp\Helper\Config;

/**
 * Class Erp
 *
 * @package Project\Erp\Model
 * @author  Synolia <contact@synolia.com>
 */
class Erp implements ArrayInterface
{
    public function toOptionArray()
    {
        $options = [
            [
                'label' => Config::VALUE_Y2,
                'value' => Config::VALUE_Y2,
            ],
            [
                'label' => Config::VALUE_COLUMBUS,
                'value' => Config::VALUE_COLUMBUS,
            ],
        ];

        return $options;
    }
}
