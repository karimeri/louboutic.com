<?php

namespace Project\Erp\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class ErpCustomerId
 *
 * @package Synolia\Slider\Model
 * @author  Synolia <contact@synolia.com>
 */
class ErpCustomerId extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('Project\Erp\Model\ResourceModel\ErpCustomerId');
    }

    /**
     * @param string|bool $establishmentCode
     *
     * @return string|null
     */
    public function getGeneratedId($establishmentCode = false)
    {
        $generatedId = $this->getData('generated_id');

        if ($establishmentCode) {
            // adding 0s in front of the generated id if length is shorter than 10 chars
            $generatedId = $establishmentCode.sprintf("%010d", $generatedId);
        }

        return $generatedId;
    }
}
