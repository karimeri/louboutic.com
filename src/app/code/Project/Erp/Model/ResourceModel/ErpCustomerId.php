<?php

namespace Project\Erp\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class ErpCustomerId
 *
 * @author Synolia <contact@synolia.com>
 */
class ErpCustomerId extends AbstractDb
{
    const TABLE_NAME = 'erp_customer_id';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, 'id');
    }
}
