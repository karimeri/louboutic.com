<?php

namespace Project\Erp\Model\ResourceModel\ErpCustomerId;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

use Project\Erp\Model\ErpCustomerId;
use Project\Erp\Model\ResourceModel\ErpCustomerId as ResourceModel;

/**
 * Class Collection
 *
 * @package Synolia\Slider\Model\ResourceModel\ErpCustomerId
 * @author  Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ErpCustomerId::class, ResourceModel::class);
    }
}
