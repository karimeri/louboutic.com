<?php

namespace Project\Erp\Model\Repository;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Synolia\Standard\Exception\NestedConnectionException;
use Synolia\Standard\Model\Repository\AbstractRepository;

use Project\Erp\Model\ErpCustomerIdFactory;
use Project\Erp\Model\ErpCustomerId as Model;

/**
 * Class ErpCustomerId
 *
 * @package Project\Erp\Model\Repository
 * @author  Synolia <contact@synolia.com>
 */
class ErpCustomerId extends AbstractRepository
{
    const ID_FIELD_NAME = 'id';
    const TABLE_NAME    = 'erp_customer_id';

    /**
     * @var ErpCustomerIdFactory
     */
    protected $factory;

    /**
     * AbstractRepository constructor.
     *
     * @param Context              $context
     * @param ErpCustomerIdFactory $factory
     * @param null                 $connectionName
     */
    public function __construct(
        Context $context,
        ErpCustomerIdFactory $factory,
        $connectionName = null
    ) {
        $this->factory = $factory;

        parent::__construct($context, $connectionName);
    }

    /**
     * @return string
     */
    protected function getIdFieldName()
    {
        return self::ID_FIELD_NAME;
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return self::TABLE_NAME;
    }

    /**
     * @param array $params
     *
     * @return Model|null
     * @throws NestedConnectionException
     * @throws NoSuchEntityException
     */
    public function findOneBy($params)
    {
        $data = parent::findOneBy($params);

        if (isset($data['id'])) {
            $erpCustomerId = $this->factory->create();
            $erpCustomerId->setData($data);

            return $erpCustomerId;
        }

        throw new NoSuchEntityException(__('No Erp Customer Id found with these parameters.'));
    }

    /**
     * @param \Magento\Framework\DataObject $erpCustomerId
     *
     * @return AbstractRepository
     */
    public function save($erpCustomerId)
    {
        return parent::persistToDb($erpCustomerId);
    }
}
