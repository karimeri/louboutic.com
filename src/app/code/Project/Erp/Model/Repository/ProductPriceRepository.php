<?php

namespace Project\Erp\Model\Repository;

use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Store\Model\Store;

/**
 * Class ProductPriceRepository
 * @package Project\Erp\Model\Repository
 * @author  Synolia <contact@synolia.com>
 */
class ProductPriceRepository
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $productResourceModel;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResourceModel
     */
    public function __construct(
        ProductResourceModel $productResourceModel
    ) {
        $this->productResourceModel = $productResourceModel;
        $this->connection = $this->productResourceModel->getConnection();
    }

    /**
     * @param array $data
     */
    public function insertPriceData($data)
    {
        if (!empty($data)) {
            $this->connection->insertOnDuplicate(
                'catalog_product_entity_decimal',
                $data,
                ['value']
            );
        }
    }

    /**
     * @param array $skus
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getRowIdsByskus($skus)
    {
        $select = $this->connection->select()
            ->from(
                $this->productResourceModel->getEntityTable(),
                ['sku', 'row_id']
            )
            ->where('sku IN (?)', $skus);

        //phpcs:ignore
        return $this->connection->query($select)->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    /**
     * @param array $styleCodes
     * @param int $attributeId
     * @param int $attributeStatusId
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getRowIdsByStyleCodes($styleCodes, $attributeId, $attributeStatusId)
    {
        $select = $this->connection->select()
            ->from(
                ['v' => 'catalog_product_entity_varchar'],
                ['v.value', 'v.row_id']
            )
            ->joinLeft(
                ['i' => 'catalog_product_entity_int'],
                'i.row_id = v.row_id',
                ['value']
            )
            ->where('v.value IN (?)', $styleCodes)
            ->where('v.attribute_id = (?)', $attributeId)
            ->where('i.attribute_id = (?)', $attributeStatusId)
            ->where('v.store_id = (?)', Store::DEFAULT_STORE_ID);

        //phpcs:ignore
        return $this->connection->query($select)->fetchAll(\PDO::FETCH_GROUP);
    }

    /**
     * @param int|string $rowId
     * @param int|string $attributeId
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getSearchableSkuByRowId($rowId, $attributeId)
    {
        $select = $this->connection->select()
            ->from(
                ['v' => 'catalog_product_entity_varchar'],
                ['v.value']
            )
            ->where('v.row_id = (?)', $rowId)
            ->where('v.attribute_id = (?)', $attributeId)
            ->where('v.store_id = (?)', Store::DEFAULT_STORE_ID);

        //phpcs:ignore
        return $this->connection->query($select)->fetch();
    }
}
