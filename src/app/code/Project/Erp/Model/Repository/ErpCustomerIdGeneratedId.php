<?php

namespace Project\Erp\Model\Repository;

use Synolia\Standard\Model\Repository\AbstractRepository;

use Project\Erp\Model\ErpCustomerIdFactory;

/**
 * Class ErpCustomerIdGeneratedId
 *
 * @package Project\Erp\Model\Repository
 * @author  Synolia <contact@synolia.com>
 */
class ErpCustomerIdGeneratedId extends AbstractRepository
{
    const ID_FIELD_NAME = 'id';
    const TABLE_NAME    = 'erp_customer_id_increment';

    /**
     * @return string
     */
    protected function getIdFieldName()
    {
        return self::ID_FIELD_NAME;
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return self::TABLE_NAME;
    }

    /**
     * @return string
     */
    public function getNextGeneratedId()
    {
        $connection = $this->getConnection();

        $connection->insert(self::TABLE_NAME, ['id' => null]);

        return $connection->lastInsertId(self::TABLE_NAME);
    }
}
