<?php

namespace Project\Erp\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Directory\Model\CountryFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Config
 *
 * @package Project\Erp\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_ERP_TO_USE = 'synchronizations_general/erp/use';
    const XML_PATH_BASE_CURRENCY = 'currency/options/base';
    const XML_PATH_CREDITMEMO_ADJUST_TAX_CLASS = 'tax/classes/cm_adjust_tax_class';

    const VALUE_Y2 = 'Y2';
    const VALUE_COLUMBUS = 'Columbus';

    const DEFAULT_PAYMENT_METHOD_CODE = 'EQ';
    const PAYMENT_METHOD_MAPPING = [
        // Diners
        'DN' => 'DIN',
        // AMEX / American Express
        'AE' => 'AME',
        'American Express' => 'AME',
        // Debit Card
        'DC' => 'DC',
        // Japan Card
        'JCB' => 'JCB',
        // Mastercard
        'MC' => 'MAS',
        'MasterCard' => 'MAS',
        // Visa
        'VI' => 'VIS',
        // Store Credit
        'storecredit' => 'AV4',
        // Cash On Delivery
        'cashondelivery' => 'CON',
    ];

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Config constructor.
     *
     * @param Context $context
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;

        parent::__construct($context);
    }

    /**
     * @param             $configName
     * @param string|null $scopeCode
     *
     * @return null|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getErpToUse($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_ERP_TO_USE, $scopeCode);
    }

    /**
     * @return bool|string
     */
    public function getBaseCurrency()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_BASE_CURRENCY, 'default');
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getCreditmemoAdjustTaxClass($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_CREDITMEMO_ADJUST_TAX_CLASS, $scopeCode);
    }

    /**
     * @param OrderPaymentInterface $payment
     *
     * @return string
     */
    public function getPaymentCode($payment)
    {
        if (isset(self::PAYMENT_METHOD_MAPPING[$payment->getCcType()])) {
            return self::PAYMENT_METHOD_MAPPING[$payment->getCcType()];
        } elseif (isset(self::PAYMENT_METHOD_MAPPING[$payment->getMethod()])) {
            return self::PAYMENT_METHOD_MAPPING[$payment->getMethod()];
        }

        return self::DEFAULT_PAYMENT_METHOD_CODE;
    }
}
