<?php

namespace Project\Erp\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory;
use Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory;
use Magento\Tax\Model\Calculation;
use Adyen\Payment\Observer\AdyenHppDataAssignObserver;
use CyberSource\PayPal\Model\Payment as PaypalPayment;
use Synolia\Tax\Model\Tax\CalculationServiceFactory;
use Project\Core\Manager\EnvironmentManager;
use Project\Erp\Helper\Config as HelperConfig;

/**
 * Class ExportSalesTickets
 *
 * @package Project\Erp\Helper
 * @author Synolia <contact@synolia.com>
 */
class ExportSalesTickets extends AbstractHelper
{
    const PAYPAL_CODES = [PaypalPayment::CODE];

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Synolia\Tax\Model\Tax\CalculationServiceFactory
     */
    protected $calculationServiceFactory;

    /**
     * Quote details factory
     *
     * @var \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory
     */
    protected $quoteDetailsFactory;

    /**
     * Quote details item factory
     *
     * @var \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory
     */
    protected $quoteDetailsItemFactory;

    /**
     * @var HelperConfig
     */
    protected $helperConfig;

    /**
     * @var \Magento\Tax\Model\Calculation
     */
    protected $calculationTool;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ExportSalesTickets constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Synolia\Tax\Model\Tax\CalculationServiceFactory $calculationServiceFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemFactory
     * @param \Project\Erp\Helper\Config $helperConfig
     * @param \Magento\Tax\Model\Calculation $calculationTool
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        EnvironmentManager $environmentManager,
        CalculationServiceFactory $calculationServiceFactory,
        QuoteDetailsInterfaceFactory $quoteDetailsFactory,
        QuoteDetailsItemInterfaceFactory $quoteDetailsItemFactory,
        HelperConfig $helperConfig,
        Calculation $calculationTool,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);

        $this->environmentManager = $environmentManager;
        $this->calculationServiceFactory = $calculationServiceFactory;
        $this->quoteDetailsFactory = $quoteDetailsFactory;
        $this->quoteDetailsItemFactory = $quoteDetailsItemFactory;
        $this->helperConfig = $helperConfig;
        $this->calculationTool = $calculationTool;
        $this->storeManager = $storeManager;
    }

    /**
     * @param array $countCurrency
     * @param string $currency
     *
     * @param int $qty
     *
     * @return array
     */
    public function addToCountCurrency($countCurrency, $currency, $qty = 1)
    {
        if (isset($countCurrency[$currency])) {
            $countCurrency[$currency] += $qty;
        } else {
            $countCurrency[$currency] = $qty;
        }

        return $countCurrency;
    }

    /**
     * @return array
     */
    public function getCurrencies()
    {
        $currencies = [];
        foreach ($this->storeManager->getStores() as $store) {
            $currencies[$store->getCurrentCurrency()->getCode()] = 1;
        }

        return array_keys($currencies);
    }

    /**
     * @param array $currencies
     *
     * @return array
     */
    public function initArray($currencies)
    {
        $array = \array_fill_keys($currencies, 0);
        $array['total'] = 0;

        return $array;
    }

    /**
     * @param array $currencies
     * @param array $amounts
     *
     * @return array
     */
    public function getPaypalHeader($currencies, $amounts)
    {
        $headers = [];
        foreach ($currencies as $currency) {
            $headers[] = sprintf('%s %s', $currency, $amounts[$currency]);
        }

        return implode('; ', $headers);
    }

    /**
     * @param Payment $payment
     *
     * @return bool
     */
    public function isPaypal($payment)
    {
        $brandCode = $payment->getAdditionalInformation(
            AdyenHppDataAssignObserver::BRAND_CODE
        );

        return ($brandCode === 'paypal' || in_array($payment->getMethod(), self::PAYPAL_CODES, true));
    }

    /**
     * @param Order $order
     * @param string $sku
     *
     * @return int|float
     */
    public function getTaxRate($order, $sku = null)
    {
        if ($sku) {
            //avalara
            $item = $this->quoteDetailsItemFactory->create();
            $item->setQuantity(1)
                ->setCode($sku)
                ->setTaxClassId($this->helperConfig->getCreditmemoAdjustTaxClass($order->getStoreId()))
                ->setIsTaxIncluded(true)
                ->setType('product')
                ->setUnitPrice(1);

            $quoteDetails = $this->quoteDetailsFactory->create();
            $quoteDetails->setShippingAddress($order->getShippingAddress()->getDataModel())
                ->setBillingAddress($order->getBillingAddress()->getDataModel())
                ->setCustomerTaxClassKey($order->getCustomerTaxClassId())
                ->setItems([$item])
                ->setCustomerId($order->getCustomerId());

            $avalara = $this->calculationServiceFactory->create(\Synolia\Avalara\Helper\Tax::SERVICE_NAME);
            $taxes = $avalara->calculateTax(
                $quoteDetails,
                $order->getStoreId()
            );

            return isset($taxes->getData()['items'][$sku])
                ? $taxes->getData()['items'][$sku]->getTaxPercent()
                : 0;
        }

        $rateRequest = $this->calculationTool->getRateRequest(
            $order->getShippingAddress(),
            $order->getBillingAddress(),
            $order->getCustomerTaxClassId(),
            $order->getStoreId(),
            $order->getCustomerId()
        )->setProductClassId($this->helperConfig->getCreditmemoAdjustTaxClass($order->getStoreId()));

        return $this->calculationTool->getRate($rateRequest);
    }
}
