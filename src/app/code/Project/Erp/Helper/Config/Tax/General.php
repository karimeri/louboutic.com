<?php

namespace Project\Erp\Helper\Config\Tax;

use Project\Erp\Helper\Config;

/**
 * Class General
 *
 * @package Project\Erp\Helper\Config\Tax
 * @author Synolia <contact@synolia.com>
 */
class General extends Config
{
    const XML_PATH_TAX_FAMILY_CODE_MAPPING = 'synchronizations_tax/general/tax_family_codes';
    const XML_PATH_TAX_REGIME_CODE_TYPE = 'synchronizations_tax/general/tax_regime_code_type';
    const XML_PATH_TAX_REGIME_CODE_VALUE = 'synchronizations_tax/general/tax_regime_code_value';

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getTaxFamilyCodeMapping($scopeCode = null)
    {
        $taxFamilyCodeMapping = $this->getConfig(self::XML_PATH_TAX_FAMILY_CODE_MAPPING, $scopeCode);

        if ($taxFamilyCodeMapping) {
            return $this->serializer->unserialize($taxFamilyCodeMapping);
        }

        return $taxFamilyCodeMapping;
    }

    /**
     * @param int $attributeId
     * @param string|null $scopeCode
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getTaxFamilyCodeMappingCode($attributeId, $scopeCode = null)
    {
        $taxFamilyCodeMapping = $this->getTaxFamilyCodeMapping($scopeCode);

        if (is_array($taxFamilyCodeMapping)) {
            foreach ($taxFamilyCodeMapping as $entry) {
                if ($attributeId === $entry['attribute']) {
                    return $entry['code'];
                }
            }
        }

        return '';
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getTaxRegimeCodeType($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_TAX_REGIME_CODE_TYPE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getTaxRegimeCodeValue($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_TAX_REGIME_CODE_VALUE, $scopeCode);
    }
}
