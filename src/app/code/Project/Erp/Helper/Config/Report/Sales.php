<?php

namespace Project\Erp\Helper\Config\Report;

use Project\Erp\Helper\Config;

/**
 * Class Sales
 *
 * @package Project\Erp\Helper\Config\Report
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class Sales extends Config
{
    //phpcs:disable
    const XML_PATH_REPORT_SALES_EXPORT_SENDER = 'synchronizations_report/sales/sender';
    const XML_PATH_REPORT_SALES_EXPORT_MAILS = 'synchronizations_report/sales/mails';
    const XML_PATH_REPORT_IMPORT_PRICES_MAILS = 'synchronizations_report/prices/mails';
    const XML_PATH_REPORT_IMPORT_PRICES_FROM_MAIL_0 = 'synchronizations_report/prices/mail_from_0';
    const XML_PATH_REPORT_SALES_INVOICED_SERVICES = 'synchronizations_report/sales/invoiced_services';
    const XML_PATH_REPORT_SALES_EXCHANGED_INVOICED_AMOUNT = 'synchronizations_report/sales/exchanged_invoiced_amount';
    const XML_PATH_REPORT_SALES_EXCHANGED_CREDITMEMO_AMOUNT = 'synchronizations_report/sales/exchanged_creditmemo_amount';
    const XML_PATH_REPORT_SALES_OMNICHANNEL_INVOICED_AMOUNT = 'synchronizations_report/sales/omnichannel_invoiced_amount';
    const XML_PATH_REPORT_SALES_OMNICHANNEL_CREDITMEMO_AMOUNT = 'synchronizations_report/sales/omnichannel_creditmemo_amount';
    const XML_PATH_REPORT_SALES_TAX_TOTAL = 'synchronizations_report/sales/tax_total';
    const XML_PATH_REPORT_SALES_NET_TOTAL = 'synchronizations_report/sales/net_total';
    const XML_PATH_REPORT_SALES_NET_TOTAL_SHOES = 'synchronizations_report/sales/net_total_shoes';
    const XML_PATH_REPORT_SALES_NET_TOTAL_BEAUTY = 'synchronizations_report/sales/net_total_beauty';
    const XML_PATH_REPORT_SALES_SHIPPING_TOTAL = 'synchronizations_report/sales/shipping_total';
    const XML_PATH_REPORT_SALES_GRAND_TOTAL = 'synchronizations_report/sales/grand_total';
    //phpcs:enable

    /**
     * @param string|int|null $scopeCode
     * @return string|null
     */
    public function getExportSender($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_REPORT_SALES_EXPORT_SENDER, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return array
     */
    public function getExportMails($scopeCode = null)
    {
        $mails = $this->getConfig(self::XML_PATH_REPORT_SALES_EXPORT_MAILS, $scopeCode);

        return $mails ? array_map('trim', explode(';', $mails)) : [];
    }

    /**
     * @param string|int|null $scopeCode
     *
     * @return string|null
     */
    public function getFromMail0($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_REPORT_IMPORT_PRICES_FROM_MAIL_0, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return array
     */
    public function getImportPricesMails($scopeCode = null)
    {
        $mails = $this->getConfig(self::XML_PATH_REPORT_IMPORT_PRICES_MAILS, $scopeCode);

        return $mails ? explode(';', $mails) : [];
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayInvoicedServicesRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_INVOICED_SERVICES, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayExchangedInvoicedAmountRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_EXCHANGED_INVOICED_AMOUNT, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayExchangedCreditmemoAmountRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_EXCHANGED_CREDITMEMO_AMOUNT, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayOmnichannelInvoicedAmountRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_OMNICHANNEL_INVOICED_AMOUNT, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayOmnichannelCreditmemoAmountRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_OMNICHANNEL_CREDITMEMO_AMOUNT, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayTaxTotalRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_TAX_TOTAL, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayNetTotalRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_NET_TOTAL, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayNetTotalShoesRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_NET_TOTAL_SHOES, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayNetTotalBeautyRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_NET_TOTAL_BEAUTY, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayShippingTotalRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_SHIPPING_TOTAL, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function displayGrandTotalRow($scopeCode = null)
    {
        return (bool) $this->getConfig(self::XML_PATH_REPORT_SALES_GRAND_TOTAL, $scopeCode);
    }
}
