<?php

namespace Project\Erp\Helper\Config\Columbus;

use Project\Erp\Block\Adminhtml\System\Config\ColumbusCivilityMappingArray;
use Project\Erp\Block\Adminhtml\System\Config\ColumbusCustomerCategoryMappingArray;
use Project\Erp\Helper\Config;

/**
 * Class Customer
 * @package Project\Erp\Helper\Config\Columbus
 * @author Synolia <contact@synolia.com>
 */
class Customer extends Config
{
    const XML_PATH_COLUMBUS_CIVILITY_MAPPING = 'synchronizations_columbus/customer/civility_mapping';
    const XML_PATH_COLUMBUS_CATEGORY_MAPPING = 'synchronizations_columbus/customer/customer_category_mapping';
    const XML_PATH_COLUMBUS_CATEGORY_ECOM_MAPPING = 'synchronizations_columbus/customer/customer_category_ecom_mapping';

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getColumbusCivilityMapping($scopeCode = null)
    {
        $civilityMapping = $this->getConfig(self::XML_PATH_COLUMBUS_CIVILITY_MAPPING, $scopeCode);

        if ($civilityMapping) {
            return $this->serializer->unserialize($civilityMapping);
        }

        return $civilityMapping;
    }

    /**
     * @param string $civility
     * @param string|null $scopeCode
     *
     * @return string
     */
    public function getColumbusCivilityCode($civility, $scopeCode = null)
    {
        $civilityMapping = $this->getColumbusCivilityMapping($scopeCode);

        if (is_array($civilityMapping)) {
            foreach ($civilityMapping as $entry) {
                if (\strtolower($civility) === \strtolower($entry[ColumbusCivilityMappingArray::CIVILITY])) {
                    return $entry[ColumbusCivilityMappingArray::CODE];
                }
            }
        }

        return '';
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getColumbusCustomerCategoryMapping($scopeCode = null)
    {
        $categoryMapping = $this->getConfig(self::XML_PATH_COLUMBUS_CATEGORY_MAPPING, $scopeCode);

        if ($categoryMapping) {
            return $this->serializer->unserialize($categoryMapping);
        }

        return $categoryMapping;
    }

    /**
     * @param int $group
     * @param string|null $scopeCode
     *
     * @return string
     */
    public function getColumbusCustomerCategoryCode($group, $scopeCode = null)
    {
        $categoryMapping = $this->getColumbusCustomerCategoryMapping($scopeCode);

        if (is_array($categoryMapping)) {
            foreach ($categoryMapping as $entry) {
                if ($group == $entry[ColumbusCustomerCategoryMappingArray::GROUP_ID]) {
                    return $entry[ColumbusCustomerCategoryMappingArray::CODE];
                }
            }
        }

        return '';
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getColumbusCustomerCategoryEcomMapping($scopeCode = null)
    {
        $categoryMapping = $this->getConfig(self::XML_PATH_COLUMBUS_CATEGORY_ECOM_MAPPING, $scopeCode);

        if ($categoryMapping) {
            return $this->serializer->unserialize($categoryMapping);
        }

        return $categoryMapping;
    }

    /**
     * @param int $group
     * @param string|null $scopeCode
     *
     * @return string
     */
    public function getColumbusCustomerCategoryEcomCode($group, $scopeCode = null)
    {
        $categoryMapping = $this->getColumbusCustomerCategoryEcomMapping($scopeCode);

        if (is_array($categoryMapping)) {
            foreach ($categoryMapping as $entry) {
                if ($group == $entry[ColumbusCustomerCategoryMappingArray::GROUP_ID]) {
                    return $entry[ColumbusCustomerCategoryMappingArray::CODE];
                }
            }
        }

        return '';
    }
}
