<?php

namespace Project\Erp\Helper\Config\Columbus;

use Project\Erp\Helper\Config;

/**
 * Class General
 *
 * @package Project\Erp\Helper\Config\Columbus
 * @author Synolia <contact@synolia.com>
 */
class General extends Config
{
    const XML_PATH_SOCIETY_CODE = 'synchronizations_columbus/general/society_code';
    const XML_PATH_VENDOR_CODE = 'synchronizations_columbus/general/vendor_code';

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getSocietyCode($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_SOCIETY_CODE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getVendorCode($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_VENDOR_CODE, $scopeCode);
    }
}
