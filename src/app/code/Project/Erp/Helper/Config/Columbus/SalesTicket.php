<?php

namespace Project\Erp\Helper\Config\Columbus;

use Project\Erp\Helper\Config;

/**
 * Class SalesTicket
 *
 * @package Project\Erp\Helper\Config\Columbus
 * @author Synolia <contact@synolia.com>
 */
class SalesTicket extends Config
{
    //phpcs:disable
    const XML_PATH_INVOICE_TYPE_MAPPING = 'synchronizations_columbus/sales_ticket/invoice_type_mapping';
    const XML_PATH_PRICE_CODE_MAPPING = 'synchronizations_columbus/sales_ticket/price_code_mapping';
    const XML_PATH_COLUMBUS_RMA_RESOLUTION_CODE_MAPPING = 'synchronizations_columbus/sales_ticket/rma_resolution_code_mapping';
    //phpcs:enable

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getInvoiceTypeMapping($scopeCode = null)
    {
        $invoiceTypeMapping = $this->getConfig(self::XML_PATH_INVOICE_TYPE_MAPPING, $scopeCode);

        if ($invoiceTypeMapping) {
            return $this->serializer->unserialize($invoiceTypeMapping);
        }

        return $invoiceTypeMapping;
    }

    /**
     * @param string $amount
     * @param string|null $scopeCode
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getInvoiceTypeMappingCode($amount, $scopeCode = null)
    {
        $invoiceTypeMapping = $this->getInvoiceTypeMapping($scopeCode);

        if (is_array($invoiceTypeMapping)) {
            foreach ($invoiceTypeMapping as $entry) {
                if ($amount >= $entry['amount_min']
                    && ($entry['amount_max'] === '0' || $entry['amount_max'] >= $amount)
                ) {
                    return $entry['code'];
                }
            }
        }

        return '';
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getPriceCodeMapping($scopeCode = null)
    {
        $priceCodeMapping = $this->getConfig(self::XML_PATH_PRICE_CODE_MAPPING, $scopeCode);

        if ($priceCodeMapping) {
            return $this->serializer->unserialize($priceCodeMapping);
        }

        return $priceCodeMapping;
    }

    /**
     * @param Store $store
     * @param string|null $scopeCode
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getPriceCodeMappingCode($store, $scopeCode = null)
    {
        $priceCodeMapping = $this->getPriceCodeMapping($scopeCode);

        if (is_array($priceCodeMapping)) {
            foreach ($priceCodeMapping as $entry) {
                if ($store->getCurrentCurrencyCode() === $entry['currency_code']) {
                    return $entry['code'];
                }
            }
        }

        return '';
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getColumbusRmaResolutionCodeMapping($scopeCode = null)
    {
        $rmaResolutionCodeMapping = $this->getConfig(self::XML_PATH_COLUMBUS_RMA_RESOLUTION_CODE_MAPPING, $scopeCode);

        if ($rmaResolutionCodeMapping) {
            return $this->serializer->unserialize($rmaResolutionCodeMapping);
        }

        return $rmaResolutionCodeMapping;
    }

    /**
     * @param int $attributeId
     * @param string|null $scopeCode
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getColumbusRmaResolutionCodeMappingCode($attributeId, $scopeCode = null)
    {
        $rmaResolutionCodeMapping = $this->getColumbusRmaResolutionCodeMapping($scopeCode);

        if (is_array($rmaResolutionCodeMapping)) {
            foreach ($rmaResolutionCodeMapping as $entry) {
                if ($attributeId === $entry['resolution']) {
                    return $entry['code'];
                }
            }
        }

        return '';
    }
}
