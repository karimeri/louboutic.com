<?php

namespace Project\Erp\Helper\Config\Y2;

use Magento\Sales\Api\Data\OrderInterface;
use Project\Erp\Helper\Config;
use Project\Sales\Helper\Config as SalesConfig;

/**
 * Class Sales
 *
 * @package Project\Erp\Helper\Config\Y2
 * @author Synolia <contact@synolia.com>
 */
class Sales extends Config
{
    //phpcs:disable
    const XML_PATH_Y2_RMA_RESOLUTION_CODE_MAPPING = 'synchronizations_y2/sales_ticket/rma_resolution_code_mapping';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_TTC = 'synchronizations_y2/sales/virtual_references_ttc';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_SHIPPING = 'synchronizations_y2/sales/virtual_reference_shipping';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_REPAIR = 'synchronizations_y2/sales/virtual_reference_repair';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_DUTY = 'synchronizations_y2/sales/virtual_reference_duty';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_CREDITNOTE = 'synchronizations_y2/sales/virtual_reference_creditnote';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_CHARGERETURN = 'synchronizations_y2/sales/virtual_reference_chargereturn';
    const XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_PROCESSINGFEE = 'synchronizations_y2/sales/virtual_reference_processingfee';
    //phpcs:enable

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getY2RmaResolutionCodeMapping($scopeCode = null)
    {
        $rmaResolutionCodeMapping = $this->getConfig(self::XML_PATH_Y2_RMA_RESOLUTION_CODE_MAPPING, $scopeCode);

        if ($rmaResolutionCodeMapping) {
            return $this->serializer->unserialize($rmaResolutionCodeMapping);
        }

        return $rmaResolutionCodeMapping;
    }

    /**
     * @param int         $attributeId
     * @param string|null $scopeCode
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getY2RmaResolutionCodeMappingCode($attributeId, $scopeCode = null)
    {
        $rmaResolutionCodeMapping = $this->getY2RmaResolutionCodeMapping($scopeCode);

        if (is_array($rmaResolutionCodeMapping)) {
            foreach ($rmaResolutionCodeMapping as $entry) {
                if ($attributeId === $entry['resolution']) {
                    return $entry['code'];
                }
            }
        }

        return '';
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function isY2VirtualReferenceTtc($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_TTC, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getY2VirtualReferenceShipping($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_SHIPPING, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getY2VirtualReferenceRepair($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_REPAIR, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getY2VirtualReferenceDuty($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_DUTY, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getY2VirtualReferenceCreditnote($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_CREDITNOTE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getY2VirtualReferenceChargereturn($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_CHARGERETURN, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getY2VirtualReferenceProcessingfee($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_Y2_SALES_VIRTUAL_REFERENCE_PROCESSINGFEE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return array|null
     */
    public function getY2VirtualReferences($scopeCode = null)
    {
        return [
            OrderInterface::SHIPPING_AMOUNT => $this->getY2VirtualReferenceShipping($scopeCode),
            SalesConfig::FIELD_AJUSTMENT_SHIPPING_FEES => $this->getY2VirtualReferenceShipping($scopeCode),
            SalesConfig::FIELD_AJUSTMENT_REPAIRING_FEES => $this->getY2VirtualReferenceRepair($scopeCode),
            'duty' => $this->getY2VirtualReferenceDuty($scopeCode),
            SalesConfig::FIELD_AJUSTMENT_CREDIT_NOTE => $this->getY2VirtualReferenceCreditnote($scopeCode),
            SalesConfig::FIELD_AJUSTMENT_RETURN_CHARGE => $this->getY2VirtualReferenceChargereturn($scopeCode),
            'processing_fee' => $this->getY2VirtualReferenceProcessingfee($scopeCode),
        ];
    }

    /**
     * @param string      $sku
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function isY2VirtualReference($sku, $scopeCode = null)
    {
        return \in_array($sku, $this->getY2VirtualReferences($scopeCode), false);
    }

    /**
     * @param $field
     * @param null $scopeCode
     *
     * @return bool|mixed
     */
    public function getY2VirtualReference($field, $scopeCode = null)
    {
        $virtualReferences = $this->getY2VirtualReferences($scopeCode);

        return isset($virtualReferences[$field]) ? $virtualReferences[$field] : false;
    }
}
