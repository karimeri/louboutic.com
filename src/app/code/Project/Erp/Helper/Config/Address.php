<?php

namespace Project\Erp\Helper\Config;

use Magento\Store\Model\ScopeInterface;
use Project\Erp\Block\Adminhtml\System\Config\Y2TitlesMappingArray;
use Project\Erp\Helper\Config;

/**
 * Class Address
 * @package Project\Erp\Helper\Config
 * @author Synolia <contact@synolia.com>
 */
class Address extends Config
{

    const XML_PATH_USE_STATE_CODES = 'synchronizations_addresses/general/use_state_codes';

    const XML_PATH_Y2_TITLES_MAPPING = 'synchronizations_addresses/mappings/y2_titles';

    /**
     * @param null $website
     *
     * @return  bool|string
     */
    public function getUseStateCodes($website = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_USE_STATE_CODES,
            ScopeInterface::SCOPE_WEBSITE,
            $website
        );
    }


    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getY2TitlesMapping($scopeCode = null)
    {
        $y2TitlesMapping = $this->getConfig(self::XML_PATH_Y2_TITLES_MAPPING, $scopeCode);

        if ($y2TitlesMapping) {
            return $this->serializer->unserialize($y2TitlesMapping);
        }

        return $y2TitlesMapping;
    }

    /**
     * @param string $magentoCode
     * @param string|null $scopeCode
     *
     * @return string
     */
    public function getY2Title($magentoCode, $scopeCode = null)
    {
        return $this->getY2MappingValue($magentoCode, Y2TitlesMappingArray::ERP_CODE, $scopeCode);
    }

    /**
     * @param string $magentoCode
     * @param string|null $scopeCode
     *
     * @return string
     */
    public function getY2Gender($magentoCode, $scopeCode = null)
    {
        return $this->getY2MappingValue($magentoCode, Y2TitlesMappingArray::GENDER_CODE, $scopeCode);
    }

    /**
     * @param string $magentoCode
     * @param string $key
     * @param string|null $scopeCode
     *
     * @return string
     */
    protected function getY2MappingValue($magentoCode, $key, $scopeCode = null)
    {
        $y2TitlesMapping = $this->getY2TitlesMapping($scopeCode);

        if (is_array($y2TitlesMapping)) {
            foreach ($y2TitlesMapping as $entry) {
                if (\strtolower($magentoCode) === \strtolower($entry[Y2TitlesMappingArray::MAGENTO_CODE])) {
                    return $entry[$key];
                }
            }
        }

        return '';
    }
}
