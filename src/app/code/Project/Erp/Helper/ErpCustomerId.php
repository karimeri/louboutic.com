<?php

namespace Project\Erp\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\NoSuchEntityException;

use Project\Erp\Model\ErpCustomerId as ErpCustomerIdModel;
use Project\Erp\Model\ErpCustomerIdFactory;
use Project\Erp\Model\Repository\ErpCustomerId as Repository;
use Project\Erp\Model\Repository\ErpCustomerIdGeneratedId as GeneratedIdRepository;

/**
 * Class ErpCustomerId
 *
 * @package Project\Erp\Helper
 * @author  Synolia <contact@synolia.com>
 */
class ErpCustomerId extends AbstractHelper
{
    /**
     * @var Repository
     */
    protected $erpCustomerIdRepository;

    /**
     * @var GeneratedIdRepository
     */
    protected $generatedIdRepository;

    /**
     * @var ErpCustomerIdFactory
     */
    protected $erpCustomerIdFactory;

    /**
     * Config constructor.
     *
     * @param Context               $context
     * @param Repository            $erpCustomerIdRepository
     * @param GeneratedIdRepository $generatedIdRepository
     * @param ErpCustomerIdFactory  $erpCustomerIdFactory
     */
    public function __construct(
        Context $context,
        Repository $erpCustomerIdRepository,
        GeneratedIdRepository $generatedIdRepository,
        ErpCustomerIdFactory $erpCustomerIdFactory
    ) {
        $this->erpCustomerIdRepository = $erpCustomerIdRepository;
        $this->generatedIdRepository   = $generatedIdRepository;
        $this->erpCustomerIdFactory    = $erpCustomerIdFactory;

        parent::__construct($context);
    }

    /**
     * @param string $email
     *
     * @return ErpCustomerIdModel
     * @throws NoSuchEntityException
     */
    public function getByEmail($email)
    {
        try {
            $erpCustomerId = $this->erpCustomerIdRepository->findOneBy(['email' => $email]);

            return $erpCustomerId;
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * @param string $email
     * @param string $establishmentCode
     * @return string
     * @throws NoSuchEntityException
     */
    public function getCustomerIdentifierCode($email, $establishmentCode)
    {
        if (!is_null($this->getByEmail($email))) {
            return sprintf(
                '%s%s',
                $establishmentCode,
                str_pad($this->getByEmail($email)->getGeneratedId(), 10, 0, STR_PAD_LEFT)
            );
        }

        return '';
    }

    /**
     * @param string $email
     *
     * @return ErpCustomerIdModel $erpCustomerId
     */
    public function createWithEmail($email)
    {
        $erpCustomerId = $this->erpCustomerIdFactory->create();
        $erpCustomerId->setEmail($email);

        return $erpCustomerId;
    }

    /**
     * insert with integrity check
     * insert first then get the generated id and update
     *
     * @param ErpCustomerIdModel $erpCustomerId
     */
    public function insert($erpCustomerId)
    {
        $nextId = $this->generatedIdRepository->getNextGeneratedId();

        $erpCustomerId->setGeneratedId($nextId);
        $this->erpCustomerIdRepository->save($erpCustomerId);
    }
}
