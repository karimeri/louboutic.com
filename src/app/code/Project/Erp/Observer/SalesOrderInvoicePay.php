<?php

namespace Project\Erp\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

use Project\Erp\Helper\ErpCustomerId;

/**
 * Class SalesOrderInvoicePay
 *
 * @package Project\Erp\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SalesOrderInvoicePay implements ObserverInterface
{
    protected $helper;

    public function __construct(
        ErpCustomerId $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Create ErpCustomerId if non-existant
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        $order         = $observer->getEvent()->getInvoice()->getOrder();
        $email         = $order->getCustomerEmail();
        $erpCustomerId = $this->helper->getByEmail($email);

        if (!$erpCustomerId) {
            $erpCustomerId = $this->helper->createWithEmail($email);
            $this->helper->insert($erpCustomerId);
        }
    }
}
