<?php

namespace Project\GiftMessage\Setup\RecurringData;

use Magento\GiftCard\Model\Giftcard;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade1
 * @package Project\GiftMessage\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    const TABLE_CORE_CONFIG_DATA = 'core_config_data';

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * Upgrade1 constructor.
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    ) {
        $this->configWriter = $configWriter;
    }

    /**
     * Run setup
     * @param Upgrade $upgradeObject
     * @return void
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configWriter->save(
            Giftcard::XML_PATH_MESSAGE_MAX_LENGTH,
            400,
            \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            0
        );
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return 'Add gift message max length';
    }
}
