<?php

namespace Project\GiftMessage\Plugin;

use Magento\GiftCard\Model\Giftcard;

/**
 * Class GiftMessageConfigProviderPlugin
 * @package Project\GiftMessage\Plugin
 * @author Synolia <contact@synolia.com>
 */
class GiftMessageConfigProviderPlugin
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * GiftMessageConfigProviderPlugin constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\GiftMessage\Model\GiftMessageConfigProvider $subject
     * @param $result
     * @return array
     */
    public function afterGetConfig(\Magento\GiftMessage\Model\GiftMessageConfigProvider $subject, $result)
    {
        //Add messageMaxLength to window.giftOptionsConfig.giftMessage for validation
        $result['giftMessage']['messageMaxLength'] = $this->scopeConfig->getValue(
            Giftcard::XML_PATH_MESSAGE_MAX_LENGTH
        );

        return $result;
    }
}
