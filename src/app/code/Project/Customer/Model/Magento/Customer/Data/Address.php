<?php

namespace Project\Customer\Model\Magento\Customer\Data;

/**
 * Class Address
 * @package Project\Customer\Model\Magento\Customer\Data
 * @author Synolia <contact@synolia.com>
 */
class Address extends \Magento\Customer\Model\Data\Address
{
    const FIRSTNAME_EN = 'firstname_en';
    const LASTNAME_EN = 'lastname_en';
    const CONTACT_PHONE = 'contact_phone';

    /**
     * @return string|null
     */
    public function getFirstnameEn()
    {
        return $this->_get(self::FIRSTNAME_EN);
    }

    /**
     * @return string|null
     */
    public function getLastnameEn()
    {
        return $this->_get(self::LASTNAME_EN);
    }

    /**
     * @return string|null
     */
    public function getContactPhone()
    {
        return $this->_get(self::CONTACT_PHONE);
    }

    /**
     * @param $firstnameEn
     * @return \Project\Customer\Model\Magento\Customer\Data\Address
     */
    public function setFirstnameEn($firstnameEn)
    {
        return $this->setData(self::FIRSTNAME_EN, $firstnameEn);
    }

    /**
     * @param $lastnameEn
     * @return \Project\Customer\Model\Magento\Customer\Data\Address
     */
    public function setLastnameEn($lastnameEn)
    {
        return $this->setData(self::LASTNAME_EN, $lastnameEn);
    }

    /**
     * @param $contactPhone
     * @return \Project\Customer\Model\Magento\Customer\Data\Address
     */
    public function setContactPhone($contactPhone)
    {
        return $this->setData(self::CONTACT_PHONE, $contactPhone);
    }
}
