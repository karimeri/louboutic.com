<?php

namespace Project\Customer\Model\Magento\Customer\Address;

use Magento\Customer\Model\Address\CustomerAddressDataFormatter;
use Magento\Directory\Model\AllowedCountries;
use Magento\Framework\App\ObjectManager;

/**
 * Class CustomerAddressDataProvider
 * @package Project\Customer\Model\Magento\Customer\Address
 */
class CustomerAddressDataProvider
{
    /**
     * Customer addresses.
     *
     * @var array
     */
    private $customerAddresses = [];

    /**
     * @var CustomerAddressDataFormatter
     */
    private $customerAddressDataFormatter;

    /**
     * @var AllowedCountries
     */
    private $allowedCountryReader;

    /**
     * CustomerAddressDataProvider constructor.
     * @param CustomerAddressDataFormatter $customerAddressDataFormatter
     * @param AllowedCountries|null $allowedCountryReader
     */
    public function __construct(
        CustomerAddressDataFormatter $customerAddressDataFormatter,
        AllowedCountries $allowedCountryReader = null
    ) {
        $this->customerAddressDataFormatter = $customerAddressDataFormatter;
        $this->allowedCountryReader = $allowedCountryReader ?: ObjectManager::getInstance()
            ->get(AllowedCountries::class);
    }

    /**
     * Get addresses for customer.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAddressDataByCustomer(
        \Magento\Customer\Api\Data\CustomerInterface $customer
    ): array {
        if (!empty($this->customerAddresses)) {
            return $this->customerAddresses;
        }

        $customerOriginAddresses = $customer->getAddresses();
        if (!$customerOriginAddresses) {
            return [];
        }

        $customerAddresses = [];
        foreach ($customerOriginAddresses as $address) {
            // Louboutin custom : check if address country is allowed
            if (in_array($address->getCountryId(), $this->allowedCountryReader->getAllowedCountries())) {
                $customerAddresses[$address->getId()] = $this->customerAddressDataFormatter->prepareAddress($address);
            }
        }

        $this->customerAddresses = $customerAddresses;

        return $this->customerAddresses;
    }
}
