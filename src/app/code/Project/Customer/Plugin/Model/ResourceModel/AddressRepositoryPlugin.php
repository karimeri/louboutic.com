<?php

namespace Project\Customer\Plugin\Model\ResourceModel;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Model\ResourceModel\AddressRepository;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Customer\Model\Magento\Customer\Data\Address;

/**
 * Class AddressRepositoryPlugin
 * @package Project\Customer\Plugin\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class AddressRepositoryPlugin
{
    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * AddressRepositoryPlugin constructor.
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        EnvironmentManager $environmentManager
    ) {
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param \Magento\Customer\Model\ResourceModel\AddressRepository $subject
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     */
    public function beforeSave(
        AddressRepository $subject,
        AddressInterface $address
    ) {
        if ($this->environmentManager->getEnvironment() == Environment::JP) {
            $this->setCustomAttribute($address, Address::FIRSTNAME_EN, 'getFirstnameEn');
            $this->setCustomAttribute($address, Address::LASTNAME_EN, 'getLastnameEn');
        }
        if ($this->environmentManager->getEnvironment() == Environment::EU) {
            $this->setCustomAttribute($address, Address::CONTACT_PHONE, 'getContactPhone');
        }
    }

    /**
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     * @param string $customAttributeName
     * @param string $getMethodName
     */
    protected function setCustomAttribute(
        AddressInterface $address,
        string $customAttributeName,
        string $getMethodName
    ) {
        $customAttribute = $address->getCustomAttribute($customAttributeName);
        if (!$customAttribute || is_null($customAttribute->getValue())) {
            $address->setCustomAttribute($customAttributeName, $address->$getMethodName());
        }
    }
}
