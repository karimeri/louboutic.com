<?php

namespace Project\Customer\Plugin\CustomerData;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Customer
 * @package Project\Customer\Plugin\CustomerData
 */
class Customer
{
    /**
     * @var CurrentCustomer
     */
    private $currentCustomer;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Customer constructor.
     * @param CurrentCustomer $currentCustomer
     * @param AddressRepositoryInterface $addressRepository
     * @param StoreManagerInterface $storeManager
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        CurrentCustomer $currentCustomer,
        AddressRepositoryInterface $addressRepository,
        StoreManagerInterface $storeManager,
        OrderCollectionFactory $orderCollectionFactory,
        LoggerInterface $logger
    ) {
        $this->currentCustomer = $currentCustomer;
        $this->addressRepository = $addressRepository;
        $this->storeManager = $storeManager;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Customer\CustomerData\Customer $subject
     * @param $result
     * @return array
     */
    public function afterGetSectionData(\Magento\Customer\CustomerData\Customer $subject, $result)
    {
        try {
            if ($this->currentCustomer->getCustomerId()) {
                $customer = $this->currentCustomer->getCustomer();
                $result['email'] = $customer->getEmail();
                $addressId = $customer->getDefaultShipping();
                $result['telephone'] = $addressId ? $this->addressRepository->getById($addressId)->getTelephone() : null;
                $result['id'] = $customer->getId();
                $result['lastSubscriptionDate'] = date("Y-m-d", strtotime($customer->getCreatedAt()));
                $result['lastTransactionDate'] = $this->getLastTransactionDate($customer->getId());
                $result['userCountry'] = $this->storeManager->getWebsite($customer->getWebsiteId())->getName();
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $result;
    }

    /**
     * @param int $customerId
     * @return false|string
     */
    private function getLastTransactionDate(int $customerId)
    {
        $orderCollection = $this->orderCollectionFactory->create();
        $orderCollection->addFieldToSelect('*');
        $orderCollection->addFieldToFilter('customer_id', $customerId);
        $orderCollection->setOrder('created_at', 'desc');
        $orderCollection->setPageSize(1)->setCurPage(1);
        $lastTransactionDate = '';
        if ($orderCollection->count() > 0) {
            $lastOrder = $orderCollection->getFirstItem();
            $lastTransactionDate = date("Y-m-d", strtotime($lastOrder->getCreatedAt()));
        }
        return $lastTransactionDate;
    }
}
