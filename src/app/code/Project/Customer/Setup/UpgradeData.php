<?php

namespace Project\Customer\Setup;

use Magento\Customer\Model\Customer;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;
use Psr\Log\LoggerInterface;

/**
 * Class UpgradeData
 * @package Project\Customer\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * @var ConfigInterface
     */
    protected $configResource;

    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * UpgradeData constructor.
     * @param EavSetup $eavSetup
     * @param ConfigInterface $configResource
     * @param AttributeRepositoryInterface $attributeRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        EavSetup $eavSetup,
        ConfigInterface $configResource,
        AttributeRepositoryInterface $attributeRepository,
        LoggerInterface $logger
    )
    {
        $this->eavSetup = $eavSetup;
        $this->configResource = $configResource;
        $this->attributeRepository = $attributeRepository;
        $this->logger = $logger;
    }

    /**
     * Update Customer Configuration
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->updateRequireEmailsConfirmationConfig();
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->addCountryAttribute();
        }

        $setup->endSetup();
    }

    private function updateRequireEmailsConfirmationConfig()
    {
        $this->configResource->saveConfig(
            'customer/create_account/confirm',
            0,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
    }

    private function addCountryAttribute()
    {
        try {
            $attribute = [
                'type' => Customer::ENTITY,
                'code' => 'country',
                'entity_type_id' => Customer::ENTITY,
                'data' => [
                    'backend_type' => 'varchar',
                    'frontend_label' => 'Country',
                    'frontend_input' => 'select',
                    'source_model' => \Project\Customer\Model\ResourceModel\Attribute\Source\Country::class,
                    'is_required' => false,
                    'visible' => true,
                    'is_user_defined' => true,
                    'is_system' => 0,
                    'sort_order' => 120,
                    'used_in_forms' => ['customer_account_create'],
                ],
            ];

            if ($this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->removeAttribute($attribute['type'], $attribute['code']);
            }

            if (!$this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->addAttribute($attribute['entity_type_id'], $attribute['code'], array());
            }

            $eavAttribute = $this->attributeRepository->get($attribute['type'], $attribute['code']);
            foreach ($attribute['data'] as $field => $value) {
                $eavAttribute->setData($field, $value);
            }
            $eavAttribute->save();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
