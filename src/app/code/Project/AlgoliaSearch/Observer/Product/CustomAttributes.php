<?php

namespace Project\AlgoliaSearch\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CustomAttributes
 * @package Project\AlgoliaSearch\Observer\Product
 */
class CustomAttributes implements ObserverInterface
{
    /**
     * @var \Project\Core\Helper\AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * CustomAttributes constructor.
     * @param \Project\Core\Helper\AttributeSet $attributeSetHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Project\Core\Helper\AttributeSet $attributeSetHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->attributeSetHelper = $attributeSetHelper;
        $this->_productFactory = $productFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Framework\DataObject $record */
        $record = $observer->getData('custom_data');
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getData('productObject');

        $record['priceExclTax'] = $product->getPriceInfo()->getPrice('final_price')->getAmount()->getBaseAmount();
        $record['category_analytics'] = $this->getGtmCategoryCode($product);
        $observer->setData('custom_data', $record);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    private function getGtmCategoryCode($product)
    {
        $productType = '';

        if ($this->attributeSetHelper->isBeauty($product)) {
            $productType = 'beauty';
            if ($this->attributeSetHelper->isEyes($product)) $productType .= ' eyes';
            elseif ($this->attributeSetHelper->isLips($product)) $productType .= ' lips';
            elseif ($this->attributeSetHelper->isNails($product)) $productType .= ' nails';
            elseif ($this->attributeSetHelper->isPerfumes($product)) $productType .= ' perfumes';
        } else {
            $gender = ($product->getGender())? $product->getResource()->getAttribute('gender')->getSource()->getOptionText($product->getGender()) . ' ' : '';
            if ($this->attributeSetHelper->isBelts($product)) $productType .= $gender . 'belts';
            elseif ($this->attributeSetHelper->isBracelets($product)) $productType .= $gender . 'bracelets';
            elseif ($this->attributeSetHelper->isShoes($product)) $productType .= $gender . 'shoes';
            elseif ($this->attributeSetHelper->isBag($product)) $productType .= $gender . 'bag';
            elseif ($this->attributeSetHelper->isAccessories($product)) $productType .= $gender . 'accessories';
        }

        return strtolower($productType);
    }
}
