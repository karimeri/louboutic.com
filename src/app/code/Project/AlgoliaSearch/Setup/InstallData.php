<?php

namespace Project\AlgoliaSearch\Setup;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Synolia\Standard\Setup\ConfigSetup;
use Magento\Store\Model\Store;

/**
 * Class InstallData
 * @package Project\AlgoliaSearch\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var array
     */
    protected $defaultConfigData = [
        'algoliasearch_credentials/credentials/debug'                              => 1,
        'algoliasearch_credentials/credentials/index_prefix'                       => 'magento2_',
        'algoliasearch_credentials/credentials/is_instant_enabled'                 => 1,
        'algoliasearch_autocomplete/autocomplete/nb_of_products_suggestions'       => '999999',
        'algoliasearch_autocomplete/autocomplete/nb_of_queries_suggestions'        => 0,
        'algoliasearch_autocomplete/autocomplete/min_popularity'                   => 1000,
        'algoliasearch_autocomplete/autocomplete/min_number_of_results'            => 2,
        'algoliasearch_instant/instant/replace_categories'                         => 0,
        'algoliasearch_instant/instant/add_to_cart_enable'                         => 1,
        'algoliasearch_categories/categories/show_cats_not_included_in_navigation' => 0,
        'algoliasearch_images/image/width'                                         => '496',
        'algoliasearch_images/image/height'                                        => '496',
        'algoliasearch_queue/queue/active'                                         => 1,
        'algoliasearch_queue/queue/number_of_job_to_run'                           => 100,
        'algoliasearch_queue/queue/number_of_retries'                              => 100,
        'algoliasearch_synonyms/synonyms_group/enable_synonyms'                    => 1,
        'algoliasearch_advanced/advanced/remove_branding'                          => 1,
    ];

    /**
     * @var array
     */
    protected $deleteConfigData = [
        'algoliasearch_categories/categories/custom_ranking_category_attributes'
    ];

    /**
     * @var array
     */
    protected $defaultArrayConfig = [
        'algoliasearch_products/products/product_additional_attributes'      => [
            [
                'attribute'   => 'name',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'path',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'categories',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'color',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'sku',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'price',
                'searchable'  => 2,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'ordered_qty',
                'searchable'  => 2,
                'retrievable' => 2,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'stock_qty',
                'searchable'  => 2,
                'retrievable' => 2,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'rating_summary',
                'searchable'  => 2,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'heel_height',
                'searchable'  => 2,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'additional_1',
                'searchable'  => 2,
                'retrievable' => 2,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'material_product_page',
                'searchable'  => 2,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'heel_height_map',
                'searchable'  => 2,
                'retrievable' => 2,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'meta_keyword',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'material_color',
                'searchable'  => 2,
                'retrievable' => 1,
                'order'       => 'unordered',
            ]
        ],
        'algoliasearch_categories/categories/category_additional_attributes' => [
            [
                'attribute'   => 'name',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'path',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'description',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'unordered',
            ],
            [
                'attribute'   => 'meta_title',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'meta_keywords',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
            [
                'attribute'   => 'meta_description',
                'searchable'  => 1,
                'retrievable' => 1,
                'order'       => 'unordered',
            ],
            [
                'attribute'   => 'product_count',
                'searchable'  => 2,
                'retrievable' => 1,
                'order'       => 'ordered',
            ],
        ],
        'algoliasearch_instant/instant/facets'                               => [],
        'algoliasearch_instant/instant/sorts'                                => []
    ];

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * InstallData constructor.
     * @param ConfigSetupFactory $configSetupFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ConfigSetupFactory $configSetupFactory,
        SerializerInterface $serializer
    ) {
        $this->configSetupFactory = $configSetupFactory;
        $this->serializer         = $serializer;
    }

    /**
     * @SuppressWarnings(PHPMD)
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);
        $this->updateConfig();
        $this->deleteConfig($setup);

        $setup->endSetup();
    }

    /**
     * Update config
     */
    protected function updateConfig()
    {
        foreach ($this->defaultConfigData as $configPath => $value) {
            $this->configSetup->saveConfig($configPath, $value);
        }
        foreach ($this->defaultArrayConfig as $configPath => $value) {
            $this->configSetup->saveConfig($configPath, $this->serializer->serialize($value));
        }
    }

    /**
     * Delete config
     * @param ModuleDataSetupInterface $setup
     */
    protected function deleteConfig(ModuleDataSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $connection->delete(
            $connection->getTableName('core_config_data'),
            [
                'path IN (?)' => $this->deleteConfigData
            ]
        );
    }
}
