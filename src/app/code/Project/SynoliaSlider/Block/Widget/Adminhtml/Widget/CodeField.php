<?php

namespace Project\SynoliaSlider\Block\Widget\Adminhtml\Widget;
/**
 * Class CodeField
 * @package Project\SynoliaSlider\Block\Widget\Adminhtml\Widget
 */
class CodeField extends \Magento\Backend\Block\Template
{
    /**
     * CodeField constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
    /**
     * @return string
     */
    protected function _toHtml()
    {
        $sliderUrl = $this->getSliderUrl() ? ' <a href="' . $this->getSliderUrl() . '" target="_blank">'
                            . __('Edit Slider') . '</a>' : '';

        return $sliderUrl;
    }
}