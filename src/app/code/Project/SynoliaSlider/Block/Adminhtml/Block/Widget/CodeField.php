<?php

namespace Project\SynoliaSlider\Block\Adminhtml\Block\Widget;
/**
 * Class CodeField
 * @package Project\SynoliaSlider\Block\Adminhtml\Block\Widget
 */
Class CodeField extends \Magento\Backend\Block\Template{

    const URL_PATH_EDIT = 'synolia_slider/slider/edit';
    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $_elementFactory;
    /**
     * @var \Synolia\Slider\Model\SliderFactory
     */
    protected $sliderFactory;

    /**
     * CodeField constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     * @param \Synolia\Slider\Model\SliderFactory $sliderFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        array $data = [],
        \Synolia\Slider\Model\SliderFactory  $sliderFactory
    ) {
        $this->_elementFactory = $elementFactory;
        $this->sliderFactory = $sliderFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepareElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $inputField = $this->_elementFactory->create("text", ['data' => $element->getData()]);
        $inputField->setId($element->getId());
        $inputField->setLabel('Code');
        $inputField->setForm($element->getForm());

        $block = $this->getLayout()->createBlock(
            \Project\SynoliaSlider\Block\Widget\Adminhtml\Widget\CodeField::class
        );

        if ($element->getValue()) {
            $slider = $this->sliderFactory->create()->load($element->getValue(), 'identifier');
            if ($slider && $slider->getId()) {
                $block->setSliderUrl(
                    $this->_urlBuilder->getUrl(
                        static::URL_PATH_EDIT,
                        [
                            'slider_id' => $slider->getId(),
                        ]
                    )
                );
            }
        }
        $element->setData(
            'after_element_html',
            $inputField->getElementHtml() . $block->toHtml() . $this->_getAfterElementHtml()
        );
        return $element;
    }

    /**
     * @return string
     */
    protected function _getAfterElementHtml()
    {
        $html = <<<HTML
            <style>
                .admin__field-control.control .control-value {
                    display: none !important;
                }
                input{ width: 30%; height: 2.5em }
                a{ display: block; margin-top: 1em }
            </style>
HTML;

        return $html;
    }
}