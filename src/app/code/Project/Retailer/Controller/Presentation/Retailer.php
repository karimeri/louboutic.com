<?php

namespace Project\Retailer\Controller\Presentation;

use Synolia\Retailer\Model\ResourceModel\RetailerTranslation;

/**
 * Class Retailer
 * @package Project\Retailer\Controller\Presentation
 */
class Retailer extends \Synolia\Retailer\Controller\Presentation\Retailer
{
    /**
     * @inheritdoc
     */
    public function execute()
    {
        $this->_view->loadLayout();

        $retailer = [];
        $retailerName = $this->getRequest()->getParam('retailerName');
        $storeId = $this->storeManager->getStore()->getId();
        $collection = $this->retailerCollectionFactory->create();
        $select = $collection->getSelect();
        $select->columns([
            'name' => new \Zend_Db_Expr("CASE WHEN translationFront.name = '' OR translationFront.name IS NULL
                            THEN translation.name
                            ELSE translationFront.name
                            END"),
            'information' => new \Zend_Db_Expr("CASE WHEN translationFront.information = '' 
                            OR translationFront.information IS NULL
                            THEN translation.information
                            ELSE translationFront.information
                            END"),
            'street' => new \Zend_Db_Expr("CASE WHEN translationFront.street = '' 
                            OR translationFront.street IS NULL
                            THEN translation.street
                            ELSE translationFront.street
                            END"),
            'street_bis' => new \Zend_Db_Expr("CASE WHEN translationFront.street_bis = '' 
                            OR translationFront.street_bis IS NULL
                            THEN translation.street_bis
                            ELSE translationFront.street_bis
                            END")
        ])
            ->joinLeft(
                ['translationFront' => RetailerTranslation::TABLE_NAME],
                'main_table.retailer_id = translationFront.retailer_id AND translationFront.store_id = ' . $storeId,
                []
            )
            ->where('url_key = ?', $retailerName);

        if ($collection->count() === 0) {
            return $this->_redirect('storelocator');
        }

        foreach ($collection->getItems() as $retailer) {
            $schedules = [];
            foreach ($retailer->getSchedules() as $item) {
                $schedule['day'] = $this->formatData->getDay($item->getDay());
                $schedule['start_time'] = $item->getStartTime();
                $schedule['end_time'] = $item->getEndTime();
                $schedules[($item->getDay() == 0) ? 99 : $item->getDay() ][] = $schedule;
            }
            $formatSchedules = $this->formatData->formatSchedules($schedules);
            $data = [];
            foreach ($formatSchedules as $day => $daySchedule) {
                $item = [];
                $item['day'] = $daySchedule[0]['day'];
                $startTime = $daySchedule[0]['start_time'] . 'AM';
                $item['start_time'] = date('H:i', strtotime($startTime));
                $endTime =  $daySchedule[0]['end_time'] . 'AM';
                if (array_key_exists(1, $daySchedule)) {
                    $endTime =  $daySchedule[1]['end_time'] . 'PM';
                }
                $item['end_time'] = date('H:i', strtotime($endTime));
                $data[$day] = $item;
            }
            $retailer['schedule_data'] = $data;

            $image = $retailer->getImage();
            if (isset($image)) {
                $image = $this->formatData->getImagePath() . $image;
                $retailer->setImage($image);
            }
        }

        $this->prepareBreadcrumbs($retailer);

        $this->prepareMeta($retailer);

        $this->coreRegistry->register('retailer_country', $this->getCountryName($retailer->getCountry()));
        $this->coreRegistry->register('retailer_continent', $retailer->getContinent());
        $this->coreRegistry->register('individual_retailer', $retailer);

        $otherRetailers = $this->getOtherRetailers($retailer);
        $this->coreRegistry->register('other_retailers', $otherRetailers);

        $this->_view->renderLayout();
    }

    /**
     * @param $countryCode
     * @return string
     */
    private function getCountryName($countryCode)
    {
        $country = $this->countryFactory->create()->loadByCode($countryCode);

        return $country->getName();
    }

    /**
     * @inheritdoc
     */
    private function prepareMeta($retailer)
    {
        $metaRetailer = $this->seoConfig->getMetaRetailer();
        $storeName = strtolower($this->storeManager->getWebsite()->getName());

        $metaRetailer = str_replace('{store}', $storeName, $metaRetailer);
        $metaRetailer = str_replace('{retailer}', $retailer->getName(), $metaRetailer);

        $this->_view->getPage()->getConfig()->getTitle()->set($metaRetailer['title']);
        $this->_view->getPage()->getConfig()->setDescription($metaRetailer['description']);
    }

    /**
     * @param $retailer
     * @return \Magento\Framework\DataObject[]
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getOtherRetailers($retailer) {
        $city = $retailer['city'];
        $id = $retailer['retailer_id'];
        $storeId = $this->storeManager->getStore()->getId();

        /** @var \Synolia\Retailer\Model\ResourceModel\Retailer\Collection $collection */
        $collection = $this->retailerCollectionFactory->create();
        $collection->addFieldToFilter('main_table.city', $city);
        $collection->addFieldToFilter('main_table.retailer_id', ['neq' => $id]);
        $select = $collection->getSelect();
        $select->columns([
            'name' => new \Zend_Db_Expr("CASE WHEN translationFront.name = '' OR translationFront.name IS NULL
                            THEN translation.name
                            ELSE translationFront.name
                            END"),
            'information' => new \Zend_Db_Expr("CASE WHEN translationFront.information = '' 
                            OR translationFront.information IS NULL
                            THEN translation.information
                            ELSE translationFront.information
                            END"),
            'street' => new \Zend_Db_Expr("CASE WHEN translationFront.street = '' 
                            OR translationFront.street IS NULL
                            THEN translation.street
                            ELSE translationFront.street
                            END"),
            'street_bis' => new \Zend_Db_Expr("CASE WHEN translationFront.street_bis = '' 
                            OR translationFront.street_bis IS NULL
                            THEN translation.street_bis
                            ELSE translationFront.street_bis
                            END")
        ])
            ->joinLeft(
                ['translationFront' => RetailerTranslation::TABLE_NAME],
                'main_table.retailer_id = translationFront.retailer_id AND translationFront.store_id = ' . $storeId,
                []
            );

        return $collection->getItems();
    }
}
