<?php

namespace Project\Retailer\Controller\Presentation;

/**
 * Class Listing
 * @package Project\Retailer\Controller\Presentation
 */
class Listing extends \Synolia\Retailer\Controller\Presentation\Listing
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface|\Synolia\Retailer\Controller\Presentation\ActionInterface|\Synolia\Retailer\Controller\Presentation\ResponseInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Date_Exception
     * @throws \Zend_Json_Exception
     */
    public function execute()
    {
        $retailers = [];
        $continent = $this->getRequest()->getParam('continent');
        $countryName = $this->getRequest()->getParam('country');
        $countryCode = $this->getCountryCode($countryName);

        $city = $this->getRequest()->getParam('city', null);
        if (!is_null($city)) {
            $city = str_replace('-', ' ', $city);
        }

        $retailerCity = $this->retailerRepository->getRetailerByCountryCodeAndCity($countryCode, $city);

        if ($retailerCity->count() == 1) {
            // @codingStandardsIgnoreLine
            $this->_forward('retailer', null, null, ['retailer' => $retailerCity->getFirstItem()]);
        }

        /** @var \Synolia\Retailer\Model\Retailer $retailer */
        foreach ($retailerCity->getItems() as $retailer) {
            if ($retailerCity->getSize() == 1) {
                $retailerRedirectUrl = $this->url->getUrl('store/'. $retailer->getData('url_key') .'/');
                $this->responseFactory->create()->setRedirect($retailerRedirectUrl)->sendResponse();
            } else {
                $schedules = [];
                foreach ($retailer->getSchedules() as $item) {
                    $schedule['day'] = $this->formatData->getDay((int)$item->getDay());
                    $schedule['start_time'] = $item->getStartTime();
                    $schedule['end_time'] = $item->getEndTime();
                    $schedules[($item->getDay() == 0) ? 99 : $item->getDay() ][] = $schedule;
                }
                $retailer['schedule_data'] = $this->formatData->formatSchedules($schedules);

                $image = $retailer->getImage();
                if (isset($image)) {
                    $image = $this->formatData->getImagePath() . $image;
                    $retailer->setImage($image);
                }

                $retailers[$retailer->getCity()][] = $retailer->getData();
                ksort($retailers);
            }
        }

        $this->_view->loadLayout();

        $this->coreRegistry->register('retailer_country', $countryName);
        $this->coreRegistry->register('retailer_continent', $continent);
        $this->coreRegistry->register('retailer_country_city', $retailers);

        $this->prepareBreadcrumbs($continent, $countryName, $city);

        $this->prepareMeta($countryName, $city);

        $this->_view->renderLayout();
    }

    /**
     * @param $label
     * @return mixed|string
     */
    private function getCountryCode($label)
    {
        $countryCode = "";
        $countries = $this->countryCollection->create()->load()->toOptionArray();
        foreach ($countries as $country) {
            if ($this->formatData->formatUrlKey($country['label']) == $label) {
                $countryCode = $country['value'];
            }
        }

        return $countryCode;
    }

    /**
     * @param string $countryName
     * @param null $city
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function prepareMeta($countryName, $city = null)
    {
        if (!is_null($city)) {
            $meta = $this->seoConfig->getMetaCity();
        } else {
            $meta = $this->seoConfig->getMetaCountry();
        }

        $metaListing = $this->getMetaCountryAndCity($meta, $countryName, $city);

        $this->_view->getPage()->getConfig()->getTitle()->set($metaListing['title']);
        $this->_view->getPage()->getConfig()->setDescription($metaListing['description']);
    }

    /**
     * @param $meta
     * @param $countryName
     * @param null $city
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getMetaCountryAndCity($meta, $countryName, $city = null)
    {
        $storeName = strtolower($this->storeManager->getWebsite()->getName());

        $meta = str_replace('{store}', $storeName, $meta);
        $meta = str_replace('{country}', $countryName, $meta);

        if (!is_null($city)) {
            $meta = str_replace('{city}', $city, $meta);
        }

        return $meta;
    }
}
