<?php

namespace Project\Retailer\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\Helper\Context;

/**
 * Class AuthConfig
 * @package Synolia\Retailer\Helper
 */
class AuthConfig extends AbstractHelper
{
    const XML_PATH_RETAILER_CONFIG = 'synolia_retailer';
    const XML_PATH_AUTHENTICATION = 'authentication';

    const XML_PATH_BASE_URL = 'url_';
    const XML_PATH_USERNAME = 'username_';
    const XML_PATH_PASSWORD = 'password_';

    protected $encryptor;

    /**
     * AuthConfig constructor.
     * @param EncryptorInterface $encryptor
     * @param Context $context
     */
    public function __construct(
        EncryptorInterface $encryptor,
        Context $context
    ) {
        parent::__construct($context);
        $this->encryptor = $encryptor;
    }

    /**
     * @param $code
     * @return int|string
     */
    public function getBaseUrl($code)
    {
        return $this->getConfig(self::XML_PATH_BASE_URL.$code);
    }

    /**
     * @param $code
     * @return int|string
     */
    public function getUsername($code)
    {
        return $this->getConfig(self::XML_PATH_USERNAME.$code);
    }

    /**
     * @param $code
     * @return int|string
     */
    public function getPassword($code)
    {
        return $this->encryptor->decrypt($this->getConfig(self::XML_PATH_PASSWORD.$code));
    }

    /**
     * @param $configName
     * @param string $configGroup
     * @return string|int
     */
    protected function getConfig($configName, $configGroup = 'authentication')
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_RETAILER_CONFIG . "/$configGroup/$configName"
        );
    }
}
