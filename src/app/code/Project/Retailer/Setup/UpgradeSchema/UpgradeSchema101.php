<?php

namespace Project\Retailer\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Synolia\Retailer\Api\Data\RetailerPermissionInterface;
use Synolia\Retailer\Model\ResourceModel\RetailerPermission;

/**
 * Class UpgradeSchema101
 * @package Project\Retailer\Setup\UpgradeSchema
 */
class UpgradeSchema101 implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable(RetailerPermission::TABLE_NAME);

        $setup->getConnection()
            ->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_SCHEDULE,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_schedule'
                ]
            )->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_EMAIL,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_email'
                ]
            )->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_PHONE,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_phone'
                ]
            )->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_STREET,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_street'
                ]
            )->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_STREET_BIS,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_street_bis'
                ]
            )->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_ZIP_CODE,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_zip_code'
                ]
            )->modifyColumn(
                $tableName,
                RetailerPermissionInterface::DISPLAY_CITY,
                [
                    'type'      => Table::TYPE_SMALLINT,
                    'default'   => true,
                    'unsigned'  => true,
                    'comment'   => 'display_city'
                ]
            );

        $setup->endSetup();
    }
}