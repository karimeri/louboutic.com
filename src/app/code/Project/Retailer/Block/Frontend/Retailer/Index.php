<?php

namespace Project\Retailer\Block\Frontend\Retailer;

/**
 * Class Index
 * @package Project\Retailer\Block\Frontend\Retailer
 */
class Index extends \Synolia\Retailer\Block\Frontend\Retailer\Index
{
    /**
     * @inheritdoc
     */
    public function getRetailersForMap()
    {
        $retailers = $this->getRetailer();

        $retailersData = [];
        foreach ($retailers as $retailer) {
            $schedules = [];
            if (!empty($retailer->getSchedules())) {
                foreach ($retailer->getSchedules() as $item) {
                    $schedule['day'] = $this->formatData->getDay((int)$item->getDay());
                    $schedule['start_time'] = $item->getStartTime();
                    $schedule['end_time'] = $item->getEndTime();
                    $schedules[($item->getDay() == 0) ? 99 : $item->getDay() ][] = $schedule;
                }
            }

            $formatSchedules = $this->formatData->formatSchedules($schedules);
            $data = [];
            foreach ($formatSchedules as $day => $daySchedule) {
                $item = [];
                $item['day'] = $daySchedule[0]['day'];
                $startTime = $daySchedule[0]['start_time'];
                $item['start_time'] = date('H:i', strtotime($startTime));
                $endTime =  $daySchedule[0]['end_time'] . 'AM';
                if (array_key_exists(1, $daySchedule)) {
                    $endTime =  $daySchedule[1]['end_time'] . 'PM';
                }
                $item['end_time'] = date('H:i', strtotime($endTime));
                $data[$day] = $item;
            }
            $retailer['schedule_data'] = $data;

            $retailer['country_name'] = $this->getCountryName($retailer->getCountry());

            $image = $retailer->getImage();
            if (isset($image)) {
                $image = $this->formatData->getImagePath() . $image;
                $retailer->setImage($image);
            }
//            $retailer['schedule_data'] = $this->formatData->formatSchedules($schedules);
            $retailersData[] = $retailer->getData();
        }

        return $retailersData;
    }

    /**
     * @param $countryCode
     * @return string
     */
    private function getCountryName($countryCode)
    {
        $country = $this->countryFactory->create()->loadByCode($countryCode);

        return $country->getName();
    }
}
