<?php

namespace Project\Retailer\Block\Frontend\Presentation;

/**
 * Class Retailer
 * @package Project\Retailer\Block\Frontend\Presentation
 */
class Retailer extends \Synolia\Retailer\Block\Frontend\Presentation\Retailer
{
    public function getOtherRetailers()
    {
        return $this->coreRegistry->registry('other_retailers');
    }
}
