<?php

namespace Project\Retailer\Block\Frontend\Presentation;

use Magento\Framework\App\ResourceConnection;
use Synolia\Retailer\Api\Data\RetailerInterface;
use Synolia\Retailer\Model\ResourceModel\Retailer;

/**
 * Class Index
 * @package Project\Retailer\Block\Frontend\Presentation
 */
class Index extends \Synolia\Retailer\Block\Frontend\Presentation\Index
{
    /**
     * @return array|\Magento\Framework\DataObject[]
     */
    public function getCountryContinent()
    {
        $connection = $this->resources->getConnection(ResourceConnection::DEFAULT_CONNECTION);
        $select = $connection->select()
            ->from(
                Retailer::TABLE_NAME,
                ['continent', 'country']
            )
            ->where("enabled = ?", RetailerInterface::STATUS_ENABLED)
            ->group(['continent', 'country'])
            ->order('continent ASC')
            ->order('country ASC');
        $datas = $connection->fetchAll($select);
        $datas = $this->formatCountry($datas);

        return $datas;
    }

    /**
     * @param array $datas
     * @return array
     */
    public function formatCountry($datas)
    {
        $listing = [];
        if (!empty($datas)) {
            foreach ($datas as $data) {
                $countryName = $this->countryFactory->create()->loadByCode($data['country']);
                $contient = strtolower($data['continent']);
                if($contient != null) {
                    $listing[$contient][$data['country']] = $countryName->getName();
                    asort($listing[$contient]);
                }
            }
        }

        return $listing;
    }
}
