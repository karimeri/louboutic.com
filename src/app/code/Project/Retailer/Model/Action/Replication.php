<?php

namespace Project\Retailer\Model\Action;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Synolia\Retailer\Model\ResourceModel\Retailer\CollectionFactory as RetailerCollectionFactory;
use Project\Retailer\Model\Api\Connector;
use Project\Core\Model\Environment;

/**
 * Class Replication
 * @package Project\Retailer\Model\Action\Retailer
 * @author  Synolia <contact@synolia.com>
 */
class Replication implements ActionInterface
{
    /**
     * @var RetailerCollectionFactory
     */
    protected $retailerCollectionFactory;

    /**
     * @var Connector
     */
    protected $connector;

    /**
     * @var Environment
     */
    protected $environment;

    /**
     * Replication constructor.
     * @param RetailerCollectionFactory $retailerCollectionFactory
     * @param Connector $connector
     * @param Environment $environment
     */
    public function __construct(
        RetailerCollectionFactory $retailerCollectionFactory,
        Connector $connector,
        Environment $environment
    ) {
        $this->retailerCollectionFactory = $retailerCollectionFactory;
        $this->connector = $connector;
        $this->environment = $environment;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $response = false;

        $collection = $this->retailerCollectionFactory->create();
        $collection->load();

        foreach ($collection->getItems() as $key => $retailer) {
            $data[$key] = [
                // Retailer
                'code' => $retailer->getCode(),
                'position' => $retailer->getPosition(),
                'email' => $retailer->getEmail(),
                'latitude' => $retailer->getLatitude(),
                'longitude' => $retailer->getLongitude(),
                'continent' => $retailer->getContinent(),
                'image' => $retailer->getImage(),
                'url_key' => $retailer->getUrlKey(),
                'city' => $retailer->getCity(),
                'zip_code' => $retailer->getZipCode(),
                'phone_number' => $retailer->getPhoneNumber(),
                'country' => $retailer->getCountry(),
                'enabled' => $retailer->getEnabled(),
                'keyword' => $retailer->getKeyword(),
                // Permission
                'display_schedule' => $retailer->getData('display_schedule'),
                'display_email' => $retailer->getData('display_email'),
                'display_phone' => $retailer->getData('display_phone'),
                'display_street' => $retailer->getData('display_street'),
                'display_street_bis' => $retailer->getData('display_street_bis'),
                'display_zip_code' => $retailer->getData('display_zip_code'),
                'display_city' => $retailer->getData('display_city'),
                // Translation
                'name' => $retailer->getData('name'),
                'information' => $retailer->getData('information'),
                'street' => $retailer->getData('street'),
                'street_bis' => $retailer->getData('street_bis'),
            ];

            // Schedules
            $schedules = [];
            foreach ($retailer->getSchedules() as $schedule) {
                $schedules[] = [
                    'day' => $schedule->getDay(),
                    'start_time' => $schedule->getStartTime(),
                    'end_time' => $schedule->getEndTime(),
                ];
            }

            $data[$key]['schedules'] = $schedules;
        }

        foreach ($this->environment->getAll() as $environment) {
            if ($environment != $this->environment::US) {
                $params['environment'] = $environment;

                $jsonData = \GuzzleHttp\json_encode($data);
                $response = $this->connector->request($params, $jsonData);
            }
        }

        return $response;
    }
}
