<?php

namespace Project\Retailer\Model\Api;

use Magento\Framework\Filesystem\DirectoryList;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Logger\Model\Logger;
use Project\Retailer\Helper\AuthConfig;

/**
 * Class Connector
 * @package Project\Wms\Model\Api
 * @author  Synolia <contact@synolia.com>
 */
class Connector
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Authentication
     */
    protected $authentication;

    /**
     * @var AuthConfig
     */
    protected $config;

    /**
     * Connector constructor.
     * @param Client        $client
     * @param LoggerFactory $loggerFactory
     * @param DirectoryList $directoryList
     * @param Authentication $authentication
     * @param AuthConfig $config
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Client $client,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        Authentication $authentication,
        AuthConfig $config
    ) {
        $this->client = $client;
        $this->authentication = $authentication;
        $this->config = $config;

        $logPath      = $directoryList->getPath('log');
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath.DIRECTORY_SEPARATOR.'retailer.log']
        );
    }

    /**
     * @param array $params
     * @param string $data
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($params, $data)
    {
        try {
            $response = $this->client->request(
                'POST',
                $this->config->getBaseUrl($params['environment']) . $params['api_path'],
                [
                    'json' => $data,
                    'headers' => [
                        'Authorization' => 'Bearer '.$this->authentication->getToken($params['environment']),
                        'content-type' => 'application/json',
                    ],
                ]
            );
            $this->logger->addInfo("message :" . $response->getBody()->getContents());
            return true;
        } catch (RequestException $exception) {
            $this->processException($exception);
            return false;
        }
    }

    /**
     * @param RequestException $exception
     */
    protected function processException(RequestException $exception)
    {
        if ($exception->hasResponse()) {
            $body = \GuzzleHttp\json_decode($exception->getResponse()->getBody()->getContents());
            $this->logger->addError("CODE : {$exception->getCode()}, message : $body->message");
        } else {
            $this->logger->addError("CODE : {$exception->getCode()}, message : {$exception->getMessage()}");
        }
    }
}
