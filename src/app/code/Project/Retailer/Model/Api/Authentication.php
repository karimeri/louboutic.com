<?php

namespace Project\Retailer\Model\Api;

use Magento\Framework\Filesystem\DirectoryList;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Logger\Model\Logger;
use Project\Retailer\Helper\AuthConfig;

/**
 * Class Authentication
 * @package Project\MagentoRest\Service
 * @author  Synolia <contact@synolia.com>
 */
class Authentication
{
    const PATH_TOKEN = '/rest/V1/integration/admin/token';

    /**
     * @var string
     */
    protected $token;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var AuthConfig
     */
    protected $config;

    /**
     * Token constructor.
     * @param Client        $client
     * @param LoggerFactory $loggerFactory
     * @param DirectoryList $directoryList
     * @param AuthConfig $config
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Client $client,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        AuthConfig $config
    ) {
        $this->client = $client;
        $this->config = $config;

        $logPath      = $directoryList->getPath('log');
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath.DIRECTORY_SEPARATOR."authentication.log"]
        );
    }

    /**
     * @param string $environment
     * @return string|bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken($environment)
    {
        $param = $this->getAccess($environment);
        try {
            $response = $this->client->request(
                'POST',
                $param['url'].self::PATH_TOKEN,
                [
                    'json' => $param['body'],
                    'headers' => [
                        'content-type' => 'application/json'
                    ]
                ]
            );

            $content  = \GuzzleHttp\json_decode($response->getBody()->getContents());

            return $content;
        } catch (RequestException $exception) {
            $this->processException($exception);
            throw new \Exception("Invalid login or password");
        }
    }

    /**
     * @param $environment
     * @return array
     */
    protected function getAccess($environment)
    {
        $param = [];
        $param['url'] = $this->config->getBaseUrl($environment);

        $user = $this->config->getUsername($environment);
        $password = $this->config->getPassword($environment);

        $param['auth'] = [$user, $password];
        $param['body'] = ['username' => $user, 'password' => $password];

        return $param;
    }

    /**
     * @param RequestException $exception
     */
    protected function processException(RequestException $exception)
    {
        if ($exception->hasResponse()) {
            $body = \GuzzleHttp\json_decode($exception->getResponse()->getBody()->getContents());
            $this->logger->addError("CODE : {$exception->getCode()}, message : $body->message");
        } else {
            $this->logger->addError("CODE : {$exception->getCode()}, message : {$exception->getMessage()}");
        }
    }
}
