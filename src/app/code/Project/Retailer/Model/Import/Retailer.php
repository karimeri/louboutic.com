<?php

namespace Project\Retailer\Model\Import;

use Carbon\Carbon;

/**
 * Class Retailer
 * @package Project\Retailer\Model\Import
 */
class Retailer extends \Synolia\Retailer\Model\Import\Retailer
{

    /**
     * Overrides $scheduleDays because of an error with the day's number:
     * -- Sunday should be 0 and not 7
     *
     * @var array
     */
    protected $scheduleDays = [
        Carbon::MONDAY => 'mo',
        Carbon::TUESDAY => 'tu',
        Carbon::WEDNESDAY => 'we',
        Carbon::THURSDAY => 'th',
        Carbon::FRIDAY => 'fr',
        Carbon::SATURDAY => 'sa',
        Carbon::SUNDAY => 'su',
    ];

}