<?php

namespace Project\Retailer\Plugin\Synolia\Retailer\Model;

use Synolia\Retailer\Model\DefaultConfigProvider;
use Synolia\Retailer\Block\Frontend\Retailer\Config as ConfigHelper;

/**
 * Class DefaultConfigProviderPlugin
 * @package Project\Retailer\Model
 * @author Synolia <contact@synolia.com>
 * @comment Plugin for Synolia\Retailer\Block\Frontend\Retailer\Config class (alter default config)
 */
class DefaultConfigProviderPlugin
{
    /**
     * @var ConfigHelper
     */
    protected $configHelper;

    /**
     * DefaultConfigProviderPlugin constructor.
     * @param \Synolia\Retailer\Block\Frontend\Retailer\Config $configHelper
     * @return void
     */
    public function __construct(ConfigHelper $configHelper)
    {
        $this->configHelper = $configHelper;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param DefaultConfigProvider $subject
     * @param $result
     * @return array
     */
    public function afterGetConfig(DefaultConfigProvider $subject, $result)
    {
        return array_merge(
            $result,
            [
                'mapTypeControl' => false,
                'lazyLoading'    => false,
                'trainImage'     => $this->configHelper->getViewFileUrl('Synolia_Retailer::images/train.gif'),
                'planeImage'     => $this->configHelper->getViewFileUrl('Synolia_Retailer::images/plane.gif')
            ]
        );
    }
}
