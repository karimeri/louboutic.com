<?php

namespace Project\Share\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Helper\Image as CatalogHelper;

/**
 * Class Share
 * @package Project\Share\Block\Widget
 * @author Synolia <contact@synolia.com>
 */
class Share extends Template implements \Magento\Widget\Block\BlockInterface
{
    const DEFAULT_TEMPLATE = 'Project_Share::share.phtml';

    const XML_PATH_FACEBOOK_SHARE_URL    = 'project_share/general/facebook_share_url';
    const XML_PATH_TWITTER_SHARE_URL     = 'project_share/general/twitter_share_url';
    const XML_PATH_TWITTER_HASHTAG       = 'project_share/general/twitter_hashtag';
    const XML_PATH_PINTEREST_SHARE_URL   = 'project_share/general/pinterest_share_url';
    const XML_PATH_PINTEREST_DEFAULT_IMG = 'project_share/general/pinterest_default_img';
    const XML_PATH_LINE_ENABLED          = 'project_share/general/enable_line';
    const XML_PATH_LINE_SCRIPT_URL       = 'project_share/general/line_script_url';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;


    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $catalogHelper;


    /**
     * Share constructor.
     * @param Template\Context $context
     * @param Registry $registry
     * @param CatalogHelper $catalogHelper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        CatalogHelper $catalogHelper,
        array $data
    ) {
        parent::__construct($context, $data);

        $this->coreRegistry  = $registry;
        $this->catalogHelper = $catalogHelper;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->getUrl(
            '*/*/*',
            [
                '_current' => true,
                '_use_rewrite' => true
            ]
        );
    }

    /**
     * @return string
     */
    public function getFacebookShareUrl()
    {
        $facebookBaseUrl = $this->_scopeConfig->getValue(
            self::XML_PATH_FACEBOOK_SHARE_URL,
            ScopeInterface::SCOPE_STORE
        );

        $product = $this->getProduct();

        if ($product) {
            $queryArgs = [
                'u'       => $product->getUrl(),
                'display' => 'popup',
                'ref'     => 'plugin',
                'src'     => 'share_button'
            ];
        } else {
            $queryArgs = [
                'u'       => $this->getCurrentUrl(),
                'display' => 'popup',
                'ref'     => 'plugin',
                'src'     => 'share_button'
            ];
        }

        return $facebookBaseUrl . '?' . http_build_query($queryArgs);
    }

    /**
     * @return string
     */
    public function getTwitterShareUrl()
    {
        $twitterBaseUrl = $this->_scopeConfig->getValue(
            self::XML_PATH_TWITTER_SHARE_URL,
            ScopeInterface::SCOPE_STORE
        );

        $product = $this->getProduct();

        if ($product) {
            $queryArgs = [
                'text' => (string)__('Discover product %1', $product->getName()),
                'url'  => $product->getProductUrl()
            ];
        } else {
            $queryArgs = [
                'text' => (string)__('Discover this website'),
                'url'  => $this->getCurrentUrl()
            ];
        }

        $twitterHashtag = $this->_scopeConfig->getValue(
            self::XML_PATH_TWITTER_HASHTAG,
            ScopeInterface::SCOPE_STORE
        );

        if ($twitterHashtag) {
            $queryArgs['hashtags'] = $twitterHashtag;
        }

        return $twitterBaseUrl . '?' . http_build_query($queryArgs);
    }

    /**
     * @return string
     */
    public function getPinterestShareUrl()
    {
        $product = $this->getProduct();

        if ($product) {
            $queryArgs = [
                'url'         => $product->getProductUrl(),
                'media'       => $this->catalogHelper->init($product, 'image')->getUrl(),
                'description' => $product->getName()
            ];
        } else {
            $pinterestDefaultImgPath = $this->_scopeConfig->getValue(
                self::XML_PATH_PINTEREST_DEFAULT_IMG,
                ScopeInterface::SCOPE_STORE
            );

            $pinterestDefaultImg = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                .trim($pinterestDefaultImgPath, '/');

            $queryArgs = [
                'url'         => $this->getCurrentUrl(),
                'media'       => $pinterestDefaultImg,
                'description' => __('Discover this website')
            ];
        }

        $pinterestBaseUrl = $this->_scopeConfig->getValue(
            self::XML_PATH_PINTEREST_SHARE_URL,
            ScopeInterface::SCOPE_STORE
        );

        return $pinterestBaseUrl . '?' . http_build_query($queryArgs);
    }

    /**
     * @return bool
     */
    public function isLineEnabled()
    {
        return (bool) $this->_scopeConfig->getValue(
            self::XML_PATH_LINE_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getLineScriptUrl()
    {
        return $this->_scopeConfig->getValue(
            self::XML_PATH_LINE_SCRIPT_URL,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        if (!parent::getTemplate()) {
            $this->setTemplate(self::DEFAULT_TEMPLATE);
        }

        return parent::getTemplate();
    }
}
