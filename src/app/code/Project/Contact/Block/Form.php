<?php

namespace Project\Contact\Block;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Project\Contact\Helper\Data as ContactHelper;
use Project\Contact\Model\ResourceModel\Topic\CollectionFactory as TopicCollectionFactory;
use Project\Contact\Model\Topic;

/**
 * Class Form
 * @package Project\Contact\Block
 */
class Form extends Template
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * @var ContactHelper
     */
    protected $contactHelper;

    /**
     * @var TopicCollectionFactory
     */
    protected $topicCollectionFactory;

    /**
     * @var DataObjectFactory
     */
    protected $objectFactory;

    /**
     * Form constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param JsonSerializer $jsonSerializer
     * @param ContactHelper $contactHelper
     * @param TopicCollectionFactory $topicCollectionFactory
     * @param DataObjectFactory $objectFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        JsonSerializer $jsonSerializer,
        ContactHelper $contactHelper,
        TopicCollectionFactory $topicCollectionFactory,
        DataObjectFactory $objectFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->jsonSerializer = $jsonSerializer;
        $this->contactHelper = $contactHelper;
        $this->topicCollectionFactory = $topicCollectionFactory;
        $this->objectFactory = $objectFactory;
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('contact/index/post', ['_secure' => true]);
    }

    /**
     * Retrieve form data
     *
     * @return mixed
     */
    public function getFormData()
    {
        $data = $this->getData('form_data');
        if ($data === null) {
            $formData = $this->customerSession->getCustomerFormData(true);
            $data = $this->objectFactory->create();
            if ($formData) {
                $data->addData($formData);
                $data->setCustomerData(1);
            }
            $this->setData('form_data', $data);
        }
        return $data;
    }

    /**
     * @return bool|false|string
     */
    public function getTopicGroups()
    {
        $storeId = $this->_storeManager->getStore()->getId();

        $data = [];
        $topicGroups = $this->contactHelper->getTopicGroups(false);
        foreach ($topicGroups as $groupId => $group) {
            $data[$groupId] = $group;
            $topicsCollection = $this->topicCollectionFactory->create()
                ->addFieldToFilter('parent_topic_id', 0)
                ->addFieldToFilter('group_id', $groupId)
                ->setOrder('sort_order', 'ASC')
                ->addStoreFilter($storeId);

            /** @var Topic $topic */
            $topicArray = [];
            foreach ($topicsCollection as $topic) {
                $topicToAdd = [
                    'id' => $topic->getId(),
                    'title' => $topic->getTitle()
                ];

                $subTopicsCollection = $this->topicCollectionFactory->create()
                    ->addFieldToFilter('parent_topic_id', $topic->getId())
                    ->addFieldToFilter('group_id', $groupId)
                    ->setOrder('sort_order', 'ASC')
                    ->addStoreFilter($storeId);

                /** @var Topic $subTopic */
                $subtopicArray = [];
                foreach ($subTopicsCollection as $subTopic) {
                    $subtopicToAdd = [
                        'id' => $subTopic->getId(),
                        'title' => $subTopic->getTitle()
                    ];

                    if ($subTopic->hasAdditionalFields()) {
                        $selectedFields = $subTopic->getAdditionalFields();
                        $fields = [];
                        foreach ($selectedFields as $field) {
                            $fieldConfig = $this->contactHelper->getContactUsAdditionalField($field);
                            $fields[$field]['code'] = $field;
                            $fields[$field]['title'] = $fieldConfig['label'];
                        };
                        $subtopicToAdd['additional_fields'] = $fields;
                    }
                    $subtopicArray[] = $subtopicToAdd;
                }

                if (!empty($subtopicArray)) {
                    $topicToAdd['subTopics'] = $subtopicArray;
                }
                $topicArray[] = $topicToAdd;
            }

            $data[$groupId]['topics'] = $topicArray;
        }

        return $this->jsonSerializer->serialize($data);
    }

    /**
     * @return bool|false|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCmsBlocks()
    {
        $cmsBlocks = [];
        $topicGroups = $this->contactHelper->getTopicGroups(false);
        foreach ($topicGroups as $group) {
            $blockId = 'contact_block' . '-' . $group['code'];
            $cmsBlocks[$group['code']] = $this->getLayout()->createBlock(
                \Magento\Cms\Block\Block::class
            )->setBlockId(
                $blockId
            )->toHtml();
        }
        return $this->jsonSerializer->serialize($cmsBlocks);
    }
}
