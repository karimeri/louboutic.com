<?php

namespace Project\Contact\Block\Adminhtml\Topic\Edit\Buttons;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Project\Contact\Api\TopicRepositoryInterface;

/**
 * Class Generic
 * @package Project\Contact\Block\Adminhtml\Topic\Edit\Buttons
 */
class Generic
{

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var TopicRepositoryInterface
     */
    protected $topicRepository;

    /**
     * @param Context $context
     * @param TopicRepositoryInterface $topicRepository
     */
    public function __construct(
        Context $context,
        TopicRepositoryInterface $topicRepository
    ) {
        $this->context = $context;
        $this->topicRepository = $topicRepository;
    }

    /**
     * Return topic page ID
     *
     * @return int|null
     */
    public function getTopicId()
    {
        try {
            return $this->topicRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
