<?php

namespace Project\Contact\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Project\Contact\Api\Data\TopicInterface;

/**
 * Interface TopicRepositoryInterface
 * @package Project\Contact\Api
 */
interface TopicRepositoryInterface
{
    /**
     * @param TopicInterface $topic
     * @return TopicInterface
     */
    public function save(TopicInterface $topic);

    /**
     * @param int $topicId
     * @return TopicInterface
     */
    public function getById($topicId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Project\Contact\Api\Data\TopicSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param TopicInterface $topic
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(TopicInterface $topic);

    /**
     * @param int $topicId
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($topicId);
}
