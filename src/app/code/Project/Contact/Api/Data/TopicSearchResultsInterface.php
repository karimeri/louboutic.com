<?php

namespace Project\Contact\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface TopicSearchResultsInterface
 * @package Project\Contact\Api\Data
 */
interface TopicSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Project\Contact\Api\Data\TopicInterface[]
     */
    public function getItems();

    /**
     * @param \Project\Contact\Api\Data\TopicInterface[] $items
     * @return TopicSearchResultsInterface
     */
    public function setItems(array $items);
}
