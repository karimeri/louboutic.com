<?php

namespace Project\Contact\Api\Data;

/**
 * Interface TopicInterface
 * @package Project\Contact\Api\Data
 */
interface TopicInterface
{
    const TOPIC_ID                  = 'topic_id';
    const PARENT_TOPIC_ID           = 'parent_topic_id';
    const TITLE                     = 'title';
    const STORES                    = 'stores';
    const CUSTOM_NAME               = 'custom_name';
    const GROUP_ID                  = 'group_id';
    const SORT_ORDER                = 'sort_order';
    const CATEGORY_NAME             = 'category_name';
    const CATEGORY_VALUE            = 'category_value';
    const SUBCATEGORY_NAME          = 'subcategory_name';
    const SUBCATEGORY_VALUE         = 'subcategory_value';
    const SF_ORIGIN                 = 'sf_origin';
    const SF_CASE_RECORD_TYPE_NAME  = 'sf_case_record_type_name';
    const SF_CASE_RECORD_TYPE_VALUE = 'sf_case_record_type_value';
    const SF_OTHER                  = 'sf_other';
    const HEEL_HEIGHT               = 'heel_height';
    const COLOR                     = 'color';
    const SIZE                      = 'size';
    const STYLE                     = 'style';
    const ADDITIONAL_FIELDS         = [self::HEEL_HEIGHT, self::COLOR, self::SIZE, self::STYLE];

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $topicId
     * @return TopicInterface
     */
    public function setId($topicId);

    /**
     * @return int|null
     */
    public function getParentTopicId();

    /**
     * @param int $parentTopicId
     * @return TopicInterface
     */
    public function setParentTopicId($parentTopicId);

    /**
     * @return string|null
     */
    public function getTitle();

    /**
     * @param string $title
     * @return TopicInterface
     */
    public function setTitle($title);

    /**
     * @return string|null
     */
    public function getStores();

    /**
     * @param string $stores
     * @return TopicInterface
     */
    public function setStores($stores);

    /**
     * @return string|null
     */
    public function getCustomName();

    /**
     * @param string $customName
     * @return TopicInterface
     */
    public function setCustomName($customName);

    /**
     * @return int|null
     */
    public function getGroupId();

    /**
     * @param int $groupId
     * @return TopicInterface
     */
    public function setGroupId($groupId);

    /**
     * @return int|null
     */
    public function getSortOrder();

    /**
     * @param int $sortOrder
     * @return TopicInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * @return string|null
     */
    public function getCategoryName();

    /**
     * @param string $categoryName
     * @return TopicInterface
     */
    public function setCategoryName($categoryName);

    /**
     * @return string|null
     */
    public function getCategoryValue();

    /**
     * @param string $categoryValue
     * @return TopicInterface
     */
    public function setCategoryValue($categoryValue);

    /**
     * @return string|null
     */
    public function getSubcategoryName();

    /**
     * @param string $subcategoryName
     * @return TopicInterface
     */
    public function setSubcategoryName($subcategoryName);

    /**
     * @return string|null
     */
    public function getSubcategoryValue();

    /**
     * @param string $subcategoryValue
     * @return TopicInterface
     */
    public function setSubcategoryValue($subcategoryValue);

    /**
     * @return string|null
     */
    public function getSfOrigin();

    /**
     * @param string $sfOrigin
     * @return TopicInterface
     */
    public function setSfOrigin($sfOrigin);

    /**
     * @return string|null
     */
    public function getSfCaseRecordTypeName();

    /**
     * @param string $sfCaseRecordTypeName
     * @return TopicInterface
     */
    public function setSfCaseRecordTypeName($sfCaseRecordTypeName);

    /**
     * @return string|null
     */
    public function getSfCaseRecordTypeValue();

    /**
     * @param string $sfCaseRecordTypeValue
     * @return TopicInterface
     */
    public function setSfCaseRecordTypeValue($sfCaseRecordTypeValue);

    /**
     * @return string|null
     */
    public function getSfOther();

    /**
     * @param string $sfOther
     * @return TopicInterface
     */
    public function setSfOther($sfOther);

    /**
     * @return bool|null
     */
    public function getHeelHeight();

    /**
     * @param bool $heelHeight
     * @return TopicInterface
     */
    public function setHeelHeight($heelHeight);

    /**
     * @return bool|null
     */
    public function getColor();

    /**
     * @param bool $color
     * @return TopicInterface
     */
    public function setColor($color);

    /**
     * @return bool|null
     */
    public function getSize();

    /**
     * @param bool $size
     * @return TopicInterface
     */
    public function setSize($size);

    /**
     * @return bool|null
     */
    public function getStyle();

    /**
     * @param bool $style
     * @return TopicInterface
     */
    public function setStyle($style);

    /**
     * @return bool
     */
    public function hasAdditionalFields();

    /**
     * @return array
     */
    public function getAdditionalFields();
}
