<?php

namespace Project\Contact\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObjectFactory;

/**
 * Class Data
 * @package Project\Contact\Helper
 */
class Data extends AbstractHelper
{
    const XML_PATH_SALESFORCE_WEBTOCASE_ENABLED     = 'louboutin_contact/salesforce_configuration/enabled';
    const XML_PATH_SALESFORCE_WEBTOCASE_FORM_ACTION = 'louboutin_contact/salesforce_configuration/form_action';
    const XML_PATH_SALESFORCE_WEBTOCASE_ORG_ID      = 'louboutin_contact/salesforce_configuration/org_id';
    const XML_PATH_SALESFORCE_WEBTOCASE_SEND_EMAIL  = 'louboutin_contact/salesforce_configuration/send_email';

    const XML_PATH_EMAIL_ONLINE_PRESS_EMAIL         = 'louboutin_contact/email/online_press';
    const XML_PATH_EMAIL_ONLINE_PRESS_EMAIL_COPY    = 'louboutin_contact/email/online_press_copy';
    const XML_PATH_EMAIL_RECRUITMENT_EMAIL          = 'louboutin_contact/email/recruitment';
    const XML_PATH_EMAIL_RECRUITMENT_EMAIL_COPY     = 'louboutin_contact/email/recruitment_copy';
    const XML_PATH_EMAIL_ONLINE_INQUIRIES           = 'louboutin_contact/email/online_inquiries';
    const XML_PATH_EMAIL_ONLINE_INQUIRIES_COPY      = 'louboutin_contact/email/online_inquiries_copy';
    const XML_PATH_EMAIL_CLIENT_SERVICES            = 'louboutin_contact/email/client_services';
    const XML_PATH_EMAIL_CLIENT_SERVICES_COPY       = 'louboutin_contact/email/client_services_copy';

    const CODE_CLIENT_SERVICES   = 'client_services';
    const LABEL_CLIENT_SERVICES  = 'Client services';
    const CODE_ONLINE_INQUIRIES  = 'online_inquiries';
    const LABEL_ONLINE_INQUIRIES = 'Online inquiries';
    const CODE_OTHER_INQUIRIES   = 'other_inquiries';
    const LABEL_OTHER_INQUIRIES  = 'Other inquiries';

    protected $objectFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param DataObjectFactory $objectFactory
     */
    public function __construct(
        Context $context,
        DataObjectFactory $objectFactory
    ) {
        parent::__construct($context);
        $this->objectFactory = $objectFactory;
    }

    /**
     * @var array
     */
    protected $additionalFields = [];

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_SALESFORCE_WEBTOCASE_ENABLED);
    }

    /**
     * @param bool $asOptions
     * @return array
     */
    public function getTopicGroups($asOptions = true)
    {
        $topicGroups = [
            self::CODE_ONLINE_INQUIRIES => [
                'label' => __(self::LABEL_ONLINE_INQUIRIES),
                'code'  => self::CODE_ONLINE_INQUIRIES,
                'mail'  => self::XML_PATH_EMAIL_ONLINE_INQUIRIES,
                'bcc'   => self::XML_PATH_EMAIL_ONLINE_INQUIRIES_COPY
            ],
            self::CODE_CLIENT_SERVICES => [
                'label' => __(self::LABEL_CLIENT_SERVICES),
                'code'  => self::CODE_CLIENT_SERVICES,
                'mail'  => self::XML_PATH_EMAIL_CLIENT_SERVICES,
                'bcc'   => self::XML_PATH_EMAIL_CLIENT_SERVICES_COPY
            ],
        ];

        if ($this->getOtherMailsList()) {
            $topicGroups[self::CODE_OTHER_INQUIRIES] = [
                'label' => __(self::LABEL_OTHER_INQUIRIES),
                'code'  => self::CODE_OTHER_INQUIRIES
            ];
        }

        if ($asOptions) {
            foreach ($topicGroups as $key => $groupConfig) {
                $topicGroups[$key] = $groupConfig['label'];
            }
        }

        return $topicGroups;
    }

    /**
     * @param int $groupId
     * @return string
     */
    public function getTopicGroupLabel($groupId)
    {
        $topicGroups = $this->getTopicGroups();
        return $topicGroups[$groupId];
    }

    /**
     * @return array
     */
    protected function getContactUsAdditionalFieldsConfig()
    {
        return [
            'heel_height' => [
                'label'       => __('Heel Height'),
            ],
            'color' => [
                'label'       => __('Color'),
            ],
            'size' => [
                'label'       => __('Size'),
            ],
            'style' => [
                'label'       => __('Style'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function getContactUsAdditionalFields()
    {
        if (empty($this->additionalFields)) {
            foreach ($this->getContactUsAdditionalFieldsConfig() as $fieldCode => $fieldData) {
                $field = $this->objectFactory->create();
                $field->setCode($fieldCode);
                foreach ($fieldData as $key => $value) {
                    $field->setData($key, $value);
                }
                $this->additionalFields[$fieldCode] = $field;
            }
        }
        return $this->additionalFields;
    }

    /**
     * @param $code
     * @return bool|mixed
     */
    public function getContactUsAdditionalField($code)
    {
        $fields = $this->getContactUsAdditionalFields();
        if (isset($fields[$code])) {
            return $fields[$code];
        }
        return false;
    }

    /**
     * @return array
     */
    public function getOtherMailsList()
    {
        $mails = [
            'online_press' => [
                'label' => __('Online Press'),
                'mail' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_ONLINE_PRESS_EMAIL),
            ],
            'recruitment' => [
                'label' => __('Recruitment'),
                'mail' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECRUITMENT_EMAIL),
            ],
        ];

        foreach ($mails as $key => $mail) {
            if (!$mail['mail']) {
                unset($mails[$key]);
            }
        }

        return $mails;
    }
}
