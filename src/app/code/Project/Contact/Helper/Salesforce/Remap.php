<?php

namespace Project\Contact\Helper\Salesforce;

use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Api\TopicRepositoryInterface;
use Project\Contact\Helper\Data as ContactHelper;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\StoreSwitcher\Helper\Data as StoreSwitcherHelper;
use Synolia\StoreSwitcher\Model\LocalisationServiceFactory;
use Magento\Directory\Model\CountryFactory;

/**
 * Class Remap
 * @package Project\Contact\Helper\Salesforce
 */
class Remap extends AbstractHelper
{
    /**
     * @var array
     */
    protected $topics = [];

    /**
     * @var TopicRepositoryInterface
     */
    protected $topicRepository;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var StoreSwitcherHelper
     */
    protected $storeSwitcherHelper;

    /**
     * @var LocalisationServiceFactory
     */
    protected $localisationServiceFactory;

    /**
     * @var CountryInformationAcquirerInterface
     */
    protected $countryInformationAcquirer;

    /**
     * @var ContactHelper
     */
    protected $contactHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $countryFactory;

    /**
     * Remap constructor.
     * @param Context $context
     * @param TopicRepositoryInterface $topicRepository
     * @param EnvironmentManager $environmentManager
     * @param StoreSwitcherHelper $storeSwitcherHelper
     * @param LocalisationServiceFactory $localisationServiceFactory
     * @param CountryInformationAcquirerInterface $countryInformationAcquirer
     * @param ContactHelper $contactHelper
     * @param CountryFactory $countryFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        TopicRepositoryInterface $topicRepository,
        EnvironmentManager $environmentManager,
        StoreSwitcherHelper $storeSwitcherHelper,
        LocalisationServiceFactory $localisationServiceFactory,
        CountryInformationAcquirerInterface $countryInformationAcquirer,
        ContactHelper $contactHelper,
        CountryFactory $countryFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->topicRepository = $topicRepository;
        $this->environmentManager = $environmentManager;
        $this->storeSwitcherHelper = $storeSwitcherHelper;
        $this->localisationServiceFactory = $localisationServiceFactory;
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->contactHelper = $contactHelper;
        $this->countryFactory = $countryFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param array $post
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function remapFields($post)
    {
        $topic = $this->getTopic((int)$post['subject']);

        $fields['orgid'] = $this->scopeConfig->getValue(ContactHelper::XML_PATH_SALESFORCE_WEBTOCASE_ORG_ID);
        if ($this->environmentManager->getEnvironment() === Environment::EU) {
            $fields['origin'] = $topic->getSfOrigin();
        } else {
            $countryCode = $this->getCountryCode();
            $fields['origin'] = $topic->getSfOrigin() . ' ' . $countryCode;
        }
        $fields['name'] = $this->getNameFromPost($post);
        $fields['email'] = isset($post['email']) ? $post['email'] : '';
        $fields['phone'] = isset($post['telephone']) ? $post['telephone'] : '';
        $fields['description'] = $this->prepareDescription($post);
        $fields['subject'] = $this->getSubjectFromPost($post);

        $categoryName = $topic->getCategoryName();
        $categoryValue = $topic->getCategoryValue();

        if (!empty($categoryName)) {
            $fields[$categoryName] = empty($categoryValue) ? $topic->getTitle() : $categoryValue;
        }

        $subCategoryName = $topic->getSubcategoryName();
        $subCategoryValue = $topic->getSubcategoryValue();

        if (!empty($subCategoryName) && !empty($subCategoryValue)) {
            $fields[$subCategoryName] = $subCategoryValue;
        }

        $sfCaseRecordTypeName = $topic->getSfCaseRecordTypeName();
        $sfCaseRecordTypeValue = $topic->getSfCaseRecordTypeValue();

        if ($sfCaseRecordTypeName) {
            $fields[$sfCaseRecordTypeName] = $sfCaseRecordTypeValue;
        }

        return $fields;
    }

    /**
     * @param int $id
     * @return TopicInterface
     */
    protected function getTopic($id)
    {
        if (!isset($this->topics[$id])) {
            $this->topics[$id] = $this->topicRepository->getById($id);
        }

        return $this->topics[$id];
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCountryCode()
    {
        $websiteId = $this->storeManager->getWebsite()->getId();
        $countryCode = $this->scopeConfig->getValue(
            DirectoryHelper::XML_PATH_DEFAULT_COUNTRY,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );

        return $countryCode;
    }

    /**
     * @param array $post
     * @return string
     */
    protected function getNameFromPost($post)
    {
        $firstName = isset($post['firstname']) ? $post['firstname'] : '';
        $lastName = isset($post['lastname']) ? $post['lastname'] : '';
        $name = $firstName . ' ' . $lastName;

        return $name;
    }

    /**
     * @param array $post
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function prepareDescription($post)
    {
        if (isset($post['style']) && $post['style'] != '') {
            $message = "Product information:\r\n";
            $message .= "Style: " . $post['style'] . "\r\n";
            $size = isset($post['size']) && $post['size'] != '' ? $post['size'] : '';
            $message .= "Size: " . $size . "\r\n";
            $color = isset($post['color']) && $post['color'] != '' ? $post['color'] : '';
            $message .= "Color: " . $color . "\r\n";
            $heelHeight = isset($post['heel_height']) && $post['heel_height'] != '' ? $post['heel_height'] : '';
            $message .= "Heel height: " . $heelHeight . "\r\n\r\n";
            $message .= "Message: \r\n";
            $message .= $post['comment'];
            return $message;
        } else {
            return isset($post['comment']) ? $post['comment'] : '';
        }
    }

    /**
     * @param $post
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getSubjectFromPost($post)
    {
        $serviceName = $this->storeSwitcherHelper->getLocalisationServiceName();
        /** @var \Synolia\GeoIp\Model\Service\GeoIpService $localisationService */
        $localisationService  = $this->localisationServiceFactory->create($serviceName);
        $countryCode = $localisationService->getCountryByIp();
        if (empty($countryCode)) {
            $countryCode = $this->getCountryCode();
        }
        $countryName = $this->getCountryName($countryCode);
        $name = "[$countryName]";

        $topicLabel = $this->getTopicLabel($post);
        if (!empty($topicLabel)) {
            $name .= "[$topicLabel]";
        }

        $topicId = (int)$post['subject'];
        $topicName = $this->getTopic($topicId)->getTitle();
        if (!empty($topicName)) {
            $name .= " - " . $topicName;
        }

        return $name;
    }

    /**
     * @param $countryCode
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCountryName($countryCode)
    {
        if ($countryCode) {
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            return $country->getName();
        }
        return '';
    }

    /**
     * @param array $post
     * @return string
     */
    protected function getTopicLabel($post)
    {
        $topicGroup = isset($post['option-type']) ? $post['option-type'] : '';
        $topicOptions = $this->contactHelper->getTopicGroups();

        return isset($topicOptions[$topicGroup]) ? $topicOptions[$topicGroup] : '';
    }
}
