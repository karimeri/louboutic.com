<?php

namespace Project\Contact\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Email
 * @package Project\Contact\Helper
 */
class Email extends AbstractHelper
{
    const XML_PATH_EMAIL_COPY_METHOD = 'louboutin_contact/email/copy_method';
    const XML_PATH_EMAIL_TEMPLATE = 'louboutin_contact/email/template';
    const XML_PATH_LEGAL_SERVICE_EMAIL = 'louboutin_contact/legal_service/email';

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * Email constructor.
     * @param Context $context
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param StateInterface $inlineTranslation
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        StateInterface $inlineTranslation
    ) {
        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
    }

    /**
     * @param $sender
     * @param $recipientPath
     * @param $copyPath
     * @param $template
     * @param $templateVars
     * @return $this
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendEmail($sender, $recipientPath, $copyPath, $template, $templateVars)
    {
        $this->inlineTranslation->suspend();

        $storeId = $this->storeManager->getStore()->getId();

        $sendFrom = [
            'email' => $sender['email'],
            'name' => $sender['name']
        ];

        $copyTo = $this->getEmails($copyPath, $storeId);
        $copyMethod = $this->scopeConfig->getValue(
            self::XML_PATH_EMAIL_COPY_METHOD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
        $bcc = [];
        if ($copyTo && $copyMethod == 'bcc') {
            $bcc = $copyTo;
        }

        $recipient = $this->scopeConfig->getValue(
            $recipientPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
        $sendTo = [
            [
                'email' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $recipient . '/email',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $storeId
                ),
                'name' => $this->scopeConfig->getValue(
                    'trans_email/ident_' . $recipient . '/name',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
                    $storeId
                ),
            ]
        ];

        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $sendTo[] = ['email' => $email, 'name' => null];
            }
        }

        foreach ($sendTo as $recipient) {
            $transport = $this->transportBuilder->setTemplateIdentifier(
                $template
            )->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => Store::DEFAULT_STORE_ID
                ]
            )->setTemplateVars(
                $templateVars
            )->setReplyTo(
                $sendFrom['email'],
                $sendFrom['name']
            )->setFromByScope(
                $sendFrom,
                $storeId
            )->addTo(
                $recipient['email'],
                $recipient['name']
            )->addBcc(
                $bcc
            )->getTransport();

            $transport->sendMessage();
        }

        $this->inlineTranslation->resume();

        return $this;
    }

    /**
     * @param string $configPath
     * @param null|string|bool|int|Store $storeId
     * @return array|false
     */
    protected function getEmails($configPath, $storeId)
    {
        $data = $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

    /**
     * @param $configName
     * @return mixed
     */
    public function getConfigValue($pathConfig)
    {
        return $this->scopeConfig->getValue($pathConfig, ScopeInterface::SCOPE_STORE, $this->getStoreId());
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getStoreId(){
        return $this->storeManager->getStore()->getId();
    }
}
