<?php

namespace Project\Contact\Controller\Adminhtml\Topic;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Controller\Adminhtml\Manager;

/**
 * Class Save
 * @package Project\Contact\Controller\Adminhtml\Topic
 */
class Save extends Manager
{

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Project\Contact\Api\Data\TopicInterface $topic */
        $topic = null;
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['topic_id']) ? $data['topic_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            if ($id) {
                $topic = $this->topicRepository->getById((int)$id);
            } else {
                unset($data['topic_id']);
                $topic = $this->topicFactory->create();
            }

            $this->dataObjectHelper->populateWithArray($topic, $data, TopicInterface::class);
            $this->topicRepository->save($topic);
            $this->messageManager->addSuccessMessage(__('You saved the topic'));
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('contact/topic/edit', ['id' => $topic->getId()]);
            } else {
                $resultRedirect->setPath('contact/topic');
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            if ($topic != null) {
                $this->storeTopicDataToSession(
                    $this->dataObjectProcessor->buildOutputDataArray(
                        $topic,
                        TopicInterface::class
                    )
                );
            }
            $resultRedirect->setPath('contact/topic/edit', ['id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the topic'));
            if ($topic != null) {
                $this->storeTopicDataToSession(
                    $this->dataObjectProcessor->buildOutputDataArray(
                        $topic,
                        TopicInterface::class
                    )
                );
            }
            $resultRedirect->setPath('contact/topic/edit', ['id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param $topicData
     */
    protected function storeTopicDataToSession($topicData)
    {
        $this->_getSession()->setProjectContactTopicData($topicData);
    }
}
