<?php

namespace Project\Contact\Controller\Adminhtml\Topic;

use Magento\Framework\App\ResponseInterface;
use Project\Contact\Controller\Adminhtml\Manager;

/**
 * Class NewAction
 * @package Project\Contact\Controller\Adminhtml\Topic
 */
class NewAction extends Manager
{

    /**
     * @return \Magento\Backend\Model\View\Result\Forward|ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        $resultForward->forward('edit');
        return $resultForward;
    }
}
