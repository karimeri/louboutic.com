<?php

namespace Project\Contact\Controller\Adminhtml\Topic;

use Magento\Framework\App\ResponseInterface;
use Project\Contact\Controller\Adminhtml\Manager;

/**
 * Class MassDelete
 * @package Project\Contact\Controller\Adminhtml\Topic
 */
class MassDelete extends Manager
{

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/index');

        $selected = $this->getRequest()->getParam(\Magento\Ui\Component\MassAction\Filter::SELECTED_PARAM, []);
        $excluded = $this->getRequest()->getParam(\Magento\Ui\Component\MassAction\Filter::EXCLUDED_PARAM, []);
        if ($excluded === 'false') {
            $excluded = [];
        }

        $topicIds = $this->filter->getTopicIds($this->topicRepository, $selected, $excluded);
        $requestedItemsCount = count($topicIds);
        $deletedItemsCount = 0;

        foreach ($topicIds as $topicId) {
            try {
                $this->topicRepository->deleteById($topicId);
                $deletedItemsCount++;
            } catch (\Exception $e) {
                $message = __('Topic %1 cannot be deleted: %2', $topicId, $e->getMessage());
                $this->messageManager->addExceptionMessage($e, $message);
            }
        }

        $resultMessage = __('A total of %1 record(s) have been deleted.', $deletedItemsCount);
        if ($requestedItemsCount !== $deletedItemsCount) {
            $this->messageManager->addWarningMessage($resultMessage);
            $errorMessage = 'An error occurred while deleting topics.';
            $errorMessage.= ' Please see the log files for more detailed information.';
            $this->messageManager->addErrorMessage(__($errorMessage));
        } else {
            $this->messageManager->addSuccessMessage($resultMessage);
        }

        return $resultRedirect;
    }
}
