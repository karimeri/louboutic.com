<?php

namespace Project\Contact\Controller\Adminhtml\Topic;

use Magento\Framework\App\ResponseInterface;
use Project\Contact\Controller\Adminhtml\Manager;
use Project\Contact\Controller\RegistryConstants;

/**
 * Class Edit
 * @package Project\Contact\Controller\Adminhtml\Topic
 */
class Edit extends Manager
{
    /**
     * Initialize current topic and set it in the registry.
     *
     * @return int
     */
    protected function initTopic()
    {
        $topicId = $this->getRequest()->getParam('id');
        $this->coreRegistry->register(RegistryConstants::CURRENT_TOPIC_ID, $topicId);

        return $topicId;
    }

    /**
     * Edit or create topic
     *
     * @return \Magento\Backend\Model\View\Result\Page|ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $topicId = $this->initTopic();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Project_Contact::manager');
        $resultPage->getConfig()->getTitle()->prepend(__('Topics'));
        $resultPage->addBreadcrumb(__('Topics'), __('Topics'), $this->getUrl('contact/topic'));

        if ($topicId === null) {
            $resultPage->addBreadcrumb(__('New Topic'), __('New Topic'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Topic'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Topic'), __('Edit Topic'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->topicRepository->getById($topicId)->getTitle()
            );
        }
        return $resultPage;
    }
}
