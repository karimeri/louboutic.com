<?php

namespace Project\Contact\Controller\Adminhtml\Topic;

use Magento\Framework\App\ResponseInterface;
use Project\Contact\Controller\Adminhtml\Manager;

/**
 * Class Index
 * @package Project\Contact\Controller\Adminhtml\Topic
 */
class Index extends Manager
{

    /**
     * @return \Magento\Backend\Model\View\Result\Page|ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Project_Contact::manager');
        $resultPage->getConfig()->getTitle()->prepend(__('Topic Manager'));
        $resultPage->addBreadcrumb(__('Topic Manager'), __('Topic Manager'));
        return $resultPage;
    }
}
