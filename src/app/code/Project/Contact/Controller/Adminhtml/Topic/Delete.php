<?php

namespace Project\Contact\Controller\Adminhtml\Topic;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Project\Contact\Controller\Adminhtml\Manager;

/**
 * Class Delete
 * @package Project\Contact\Controller\Adminhtml\Topic
 */
class Delete extends Manager
{

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->topicRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The topic has been deleted.'));
                $resultRedirect->setPath('contact/*/');
                return $resultRedirect;
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The topic no longer exists.'));
                return $resultRedirect->setPath('contact/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('contact/topic/edit', ['id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the topic'));
                return $resultRedirect->setPath('contact/topic/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a topic to delete.'));
        $resultRedirect->setPath('contact/*/');

        return $resultRedirect;
    }
}
