<?php

namespace Project\Contact\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Project\Contact\Api\Data\TopicInterfaceFactory;
use Project\Contact\Api\TopicRepositoryInterface;
use Project\Contact\Ui\Component\MassAction\Filter;

/**
 * Class Manager
 * @package Project\Contact\Controller\Adminhtml
 */
abstract class Manager extends Action
{
    /**
     * @var string
     */
    const ACTION_RESOURCE = 'Project_Contact::manager';

    /**
     * @var TopicRepositoryInterface
     */
    protected $topicRepository;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var TopicInterfaceFactory
     */
    protected $topicFactory;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;


    /**
     * @param Registry $registry
     * @param TopicRepositoryInterface $topicRepository
     * @param Filter $filter
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param TopicInterfaceFactory $topicFactory
     * @param DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        TopicRepositoryInterface $topicRepository,
        Filter $filter,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        TopicInterfaceFactory $topicFactory,
        DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        Context $context
    ) {
        parent::__construct($context);
        $this->coreRegistry         = $registry;
        $this->topicRepository      = $topicRepository;
        $this->filter               = $filter;
        $this->resultPageFactory    = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->topicFactory         = $topicFactory;
        $this->dataObjectProcessor  = $dataObjectProcessor;
        $this->dataObjectHelper     = $dataObjectHelper;
    }
}
