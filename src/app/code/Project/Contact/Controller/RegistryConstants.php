<?php

namespace Project\Contact\Controller;

/**
 * Class RegistryConstants
 * @package Project\Contact\Controller
 */
class RegistryConstants
{

    /**
     * Registry key where current topic ID is stored
     */
    const CURRENT_TOPIC_ID = 'current_topic_id';
}
