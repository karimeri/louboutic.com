<?php

namespace Project\Contact\Controller\Form;

use Magento\Contact\Model\ConfigInterface;
use Magento\Contact\Model\MailInterface;
use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Api\TopicRepositoryInterface;
use Project\Contact\Helper\Data as ContactHelper;
use Project\Contact\Helper\Email as ContactEmailHelper;
use Project\Contact\Helper\Salesforce\Remap as RemapHelper;
use Psr\Log\LoggerInterface;
use Synolia\StoreSwitcher\Helper\Data as StoreSwitcherHelper;
use Synolia\StoreSwitcher\Model\LocalisationServiceFactory;
use Magento\Directory\Model\CountryFactory;

/**
 * Class Post
 * @package Project\Contact\Controller\Form
 */
class Post extends \Magento\Contact\Controller\Index\Post
{
    /**
     * @var TopicInterface
     */
    protected $topic;

    /**
     * @var TopicInterface
     */
    protected $subTopic;

    /**
     * @var ContactHelper
     */
    protected $contactHelper;

    /**
     * @var ContactEmailHelper
     */
    protected $contactEmailHelper;

    /**
     * @var RemapHelper
     */
    protected $remapHelper;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var MessageManagerInterface
     */
    protected $messageManager;

    /**
     * @var TopicRepositoryInterface
     */
    protected $topicRepository;

    /**
     * @var CountryInformationAcquirerInterface
     */
    protected $countryInformationAcquirer;

    /**
     * @var StoreSwitcherHelper
     */
    protected $storeSwitcherHelper;

    /**
     * @var LocalisationServiceFactory
     */
    protected $localisationServiceFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var DataObjectFactory
     */
    protected $objectFactory;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $countryFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Post constructor.
     * @param Context $context
     * @param ConfigInterface $contactsConfig
     * @param MailInterface $mail
     * @param DataPersistorInterface $dataPersistor
     * @param ContactHelper $contactHelper
     * @param ContactEmailHelper $contactEmailHelper
     * @param RemapHelper $remapHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param MessageManagerInterface $messageManager
     * @param TopicRepositoryInterface $topicRepository
     * @param CountryInformationAcquirerInterface $countryInformationAcquirer
     * @param StoreSwitcherHelper $storeSwitcherHelper
     * @param LocalisationServiceFactory $localisationServiceFactory
     * @param StoreManagerInterface $storeManager
     * @param DataObjectFactory $objectFactory
     * @param CountryFactory $countryFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig,
        MailInterface $mail,
        DataPersistorInterface $dataPersistor,
        ContactHelper $contactHelper,
        ContactEmailHelper $contactEmailHelper,
        RemapHelper $remapHelper,
        ScopeConfigInterface $scopeConfig,
        MessageManagerInterface $messageManager,
        TopicRepositoryInterface $topicRepository,
        CountryInformationAcquirerInterface $countryInformationAcquirer,
        StoreSwitcherHelper $storeSwitcherHelper,
        LocalisationServiceFactory $localisationServiceFactory,
        StoreManagerInterface $storeManager,
        DataObjectFactory $objectFactory,
        CountryFactory $countryFactory,
        LoggerInterface $logger = null
    ) {
        parent::__construct($context, $contactsConfig, $mail, $dataPersistor, $logger);
        $this->contactHelper = $contactHelper;
        $this->contactEmailHelper = $contactEmailHelper;
        $this->remapHelper = $remapHelper;
        $this->scopeConfig = $scopeConfig;
        $this->messageManager = $messageManager;
        $this->topicRepository = $topicRepository;
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->storeSwitcherHelper = $storeSwitcherHelper;
        $this->localisationServiceFactory = $localisationServiceFactory;
        $this->storeManager = $storeManager;
        $this->objectFactory = $objectFactory;
        $this->countryFactory = $countryFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|void
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $post = $this->getRequest()->getParams();

        if ($post) {
            try {
                $postObject = $this->objectFactory->create();
                $postObject->setData($post);

                $error = $this->checkFields($post);
                // if contact form fields invalid, (bool)$error = true
                if ($error) {
                    throw new \Exception();
                }

                if (strlen($post['comment']) != strlen(strip_tags($post['comment']))) {
                    $this->messageManager->addErrorMessage(__('Message has not been sent, please verify content.'));
                    $this->_redirect('*/*/');
                    return;
                }

                $this->sendMails($postObject);

                $this->_eventManager->dispatch(
                    'contact_form_post_success',
                    ['option-type' => $postObject->getData('option-type')]
                );

                if ($this->contactHelper->isEnabled()) {
                    $returnAttribute = ['modal_message' => true];
                    $attr = $this->remapHelper->remapFields($post);
                    $refererUrl = $this->_redirect->getRefererUrl();
                    $retUrl = (strstr($refererUrl, 'modal_message') != false)
                        ? $retUrl = $refererUrl : $refererUrl . '?' . http_build_query($returnAttribute);
                    $attr['retURL'] = $retUrl;

                    $url = $this->scopeConfig->getValue(ContactHelper::XML_PATH_SALESFORCE_WEBTOCASE_FORM_ACTION);
                    $url .= '&' . http_build_query($attr);

                    $this->_redirect($url);
                } else {
                    $this->_redirect('*/*/', array(
                        'modal_message' => true
                    ));
                }
                return;
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Unable to submit your request. Please, try again later.'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    /**
     * @param $postObject
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function sendMails($postObject)
    {
        $groups = $this->contactHelper->getTopicGroups(false);
        $groupId = $postObject->getData('option-type');

        $subject = $this->getSubject($postObject);

        $recipientPath = null;
        $copyToPath = null;

        $sender = $this->getSender($postObject);

        if (isset($groups[$groupId])) {
            if ($groups[$groupId]['mail']) {
                $recipientPath = $groups[$groupId]['mail'];
            }
            if ($groups[$groupId]['bcc']) {
                $copyToPath = $groups[$groupId]['bcc'];
            }
            $subject = $groups[$groupId]['label'] . ' - ' . $subject;
        }

        if (!$recipientPath) {
            $recipientPath = ContactHelper::XML_PATH_EMAIL_ONLINE_INQUIRIES;
        }

        $websiteId = $this->storeManager->getWebsite()->getId();
        $countryCode = $this->scopeConfig->getValue(
            'general/country/default',
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
        $storeId = $this->storeManager->getStore()->getId();
        $localeCode = $this->scopeConfig->getValue(
            'general/locale/code',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            $storeId
        );

        $templateVars = [
            'data'              => $postObject,
            'country'           => $this->getCountryName($countryCode),
            'countryByIp'       => $this->getCountryByIp(),
            'ip'                => $this->getCustomerIp(),
            'language'          => $localeCode,
            'additionalFields'  => $this->getAdditionalFieldsAsText($postObject),
            'subject'           => $subject,
            'subsubject'        => $this->getSubTopic($postObject, true),
            'image'             => null
        ];

        if (!$this->contactHelper->isEnabled() || ($this->contactHelper->isEnabled() &&
                $this->scopeConfig->getValue(ContactHelper::XML_PATH_SALESFORCE_WEBTOCASE_SEND_EMAIL))) {
            $postObject->setData('comment', nl2br($postObject->getData('comment')));
            $postObject->setData('name', $this->getCustomerName($postObject));
            $template = $this->scopeConfig->getValue(parent::XML_PATH_EMAIL_TEMPLATE);

            $this->contactEmailHelper->sendEmail($sender, $recipientPath, $copyToPath, $template, $templateVars);
        }
    }

    /**
     * @param $post
     * @return bool
     * @throws \Zend_Validate_Exception
     */
    private function checkFields($post)
    {
        $error = false;

        if (!\Zend_Validate::is(trim($post['firstname']), 'NotEmpty')) {
            $error = true;
        }
        if (!\Zend_Validate::is(trim($post['lastname']), 'NotEmpty')) {
            $error = true;
        }
        if (!\Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
            $error = true;
        }
        if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
            $error = true;
        }
        if (\Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
            $error = true;
        }

        return $error;
    }

    /**
     * @param $postObject
     * @return string|null
     */
    protected function getSubject($postObject)
    {
        return $this->getTopic($postObject)->getTitle();
    }

    /**
     * @param $postObject
     * @return \Project\Contact\Api\Data\TopicInterface
     */
    protected function getTopic($postObject)
    {
        if (is_null($this->topic)) {
            $this->topic = $this->topicRepository->getById($postObject->getSubject());
        }

        return $this->topic;
    }

    /**
     * @param $postObject
     * @param bool $asText
     * @return bool|\Project\Contact\Api\Data\TopicInterface|string|null
     */
    protected function getSubTopic($postObject, $asText = false)
    {
        if (is_null($this->subTopic)) {
            if ($postObject->getSubsubject()) {
                $subTopic = $this->topicRepository->getById($postObject->getSubsubject());

                $topic = $this->getTopic($postObject);
                if ($subTopic->getParentTopicId() == $topic->getId()) {
                    $this->subTopic = $subTopic;
                } else {
                    $this->subTopic = false;
                }
            } else {
                $this->subTopic = false;
            }
        }
        if ($asText) {
            if ($this->subTopic) {
                return $this->subTopic->getTitle();
            }
        } else {
            return $this->subTopic;
        }
    }

    /**
     * @param $postObject
     * @return array
     */
    protected function getSender($postObject)
    {
        $data = [
            'email' => $postObject->getData('email'),
            'name' => $this->getCustomerName($postObject)
        ];

        return $data;
    }

    /**
     * @param $postObject
     * @return string
     */
    protected function getCustomerName($postObject)
    {
        $firstName = $postObject->getData('firstname');
        $lastName = $postObject->getData('lastname');
        $name = $firstName . ' ' . $lastName;

        return $name;
    }

    /**
     * @param $postObject
     * @return string
     */
    protected function getAdditionalFieldsAsText($postObject)
    {
        if ($this->getSubTopic($postObject)) {
            $topic = $this->getSubTopic($postObject);
        } else {
            $topic = $this->getTopic($postObject);
        }

        $resultArray = [];
        $value = '';

        $topicFields = $topic->getAdditionalFields();
        foreach ($topicFields as $fieldName) {
            $additionalFieldConfig = $this->contactHelper->getContactUsAdditionalField($fieldName);
            if ($additionalFieldConfig) {
                if (isset($postObject[$fieldName])) {
                    $value = $postObject[$fieldName];
                }
                $resultArray[] = $additionalFieldConfig['label'] . ': ' . $value.', ';
            }
        }
        return implode("\n", $resultArray);
    }

    /**
     * @param $countryCode
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCountryName($countryCode)
    {
        return !empty($countryCode)
            ? $this->countryInformationAcquirer->getCountryInfo($countryCode)->getFullNameEnglish()
            : '';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCountryByIp()
    {
        $countryCode = $this->getLocalisationService()->getCountryByIp();
        if ($countryCode) {
            $country = $this->countryFactory->create()->loadByCode($countryCode);
            return $country->getName();
        }
        return '';
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCustomerIp()
    {
        return $this->getLocalisationService()->getIp();
    }

    /**
     * @return \Synolia\GeoIp\Model\Service\GeoIpService
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getLocalisationService()
    {
        $serviceName = $this->storeSwitcherHelper->getLocalisationServiceName();
        /** @var \Synolia\GeoIp\Model\Service\GeoIpService $localisationService */
        $localisationService  = $this->localisationServiceFactory->create($serviceName);

        return $localisationService;
    }
}
