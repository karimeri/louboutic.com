define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Ui/js/modal/modal'
    ], function ($, Component, ko, modal) {
        'use strict';
        return Component.extend({
            initialize: function (config) {
                let self = this;

                // display modal if modal_message=1 parameter
                let urlParams = new URL(window.location.href).searchParams;
                let modal_message = urlParams.get('modal_message');
                if (modal_message == true) {
                    self.showModal();
                }

                // CMS block
                this.cmsBlocks = config.cmsBlocks;
                this.cmsBlock = ko.observable('');

                this.availableGroups = ko.observableArray();
                this.availableTopics = ko.observableArray();
                this.availableSubTopics = ko.observableArray();
                this.additionalFields = ko.observableArray();
                this.selectedGroup = ko.observable();
                this.selectedTopic = ko.observable();
                this.selectedSubTopic = ko.observable();

                let groups = config.topicGroups;
                Object.keys(groups).forEach(function (group) {
                    // populate group list
                    self.availableGroups.push(groups[group]);
                });

                this.selectedGroup.subscribe(function (data) {
                    self.populateTopics(data.topics);
                    self.populateCmsBlock();
                });
                this.selectedTopic.subscribe(function (data) {
                    self.populateSubTopics(data);
                });
                this.selectedSubTopic.subscribe(function (data) {
                    self.displayAdditionalFields(data);
                });

                // Init group
                if (this.availableGroups().length > 0) {
                    this.selectedGroup(groups[Object.keys(groups)[0]]);
                }

                this._super();
            },

            populateCmsBlock: function () {
                let code = this.selectedGroup().code;
                this.cmsBlock(this.cmsBlocks[code]);
            },

            getTopic: function (id) {
                let self = this;
                var topic = null;
                Object.keys(self.availableTopics()).forEach(function (availableTopic) {
                    if (self.availableTopics()[availableTopic].id === id) {
                        topic = self.availableTopics()[availableTopic];
                    }
                });
                return topic;
            },

            getSubTopic: function (id) {
                let self = this;
                var subTopic = null;
                Object.keys(self.availableSubTopics()).forEach(function (availableSubTopic) {
                    if (self.availableSubTopics()[availableSubTopic].id === id) {
                        subTopic = self.availableSubTopics()[availableSubTopic];
                    }
                });
                return subTopic;
            },

            populateTopics: function (data) {
                let self = this;
                // clear topics list
                self.availableTopics([]);
                if (data) {
                    Object.keys(data).forEach(function (topic) {
                        // populate topics list
                        self.availableTopics.push(data[topic]);
                    });
                }
            },

            populateSubTopics: function (topicId) {
                let self = this;
                let topic = self.getTopic(topicId);
                // clear sub topics list
                self.availableSubTopics([]);
                if (topic && topic.hasOwnProperty('subTopics')) {
                    Object.keys(topic.subTopics).forEach(function (subTopic) {
                        // populate sub topics list
                        self.availableSubTopics.push(topic.subTopics[subTopic]);
                    });
                }
            },

            displayAdditionalFields: function (subTopicId) {
                let self = this;
                let subTopic = self.getSubTopic(subTopicId);
                // clear additional fields list
                self.additionalFields([]);
                if (subTopic && subTopic.hasOwnProperty('additional_fields')) {
                    Object.keys(subTopic.additional_fields).forEach(function (field) {
                        // populate additional fields
                        self.additionalFields.push(subTopic.additional_fields[field]);
                    });
                }
            },

            showModal: function () {
                let options = {
                    modalClass: 'contact-modal',
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    // title: '',
                    buttons: [{
                        text: $.mage.__('Continue'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };

                modal(options, $('#popup-modal'));

                $('#popup-modal').modal('openModal');
            }

        });

    }

);
