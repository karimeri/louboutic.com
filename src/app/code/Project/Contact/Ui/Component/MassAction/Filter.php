<?php

namespace Project\Contact\Ui\Component\MassAction;

use Magento\Framework\Api\SearchCriteriaInterface;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Api\TopicRepositoryInterface;

/**
 * Class Filter
 * @package Project\Contact\Ui\Component\MassAction
 */
class Filter
{
    /**
     * @var SearchCriteriaInterface
     */
    protected $searchCriteria;

    /**
     * Filter constructor.
     * @param SearchCriteriaInterface $searchCriteria
     */
    public function __construct(
        SearchCriteriaInterface $searchCriteria
    ) {
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * Obtain the list of selected topics.
     *
     * @param TopicRepositoryInterface $topicRepository
     * @param string[] $selected
     * @param string|string[] $excluded
     * @return string[]
     */
    public function getTopicIds(TopicRepositoryInterface $topicRepository, array $selected, array $excluded)
    {
        if (!empty($selected)) {
            return $selected;
        }

        // read all ids from repo
        $topics = $topicRepository->getList($this->searchCriteria)->getItems();
        $selected = array_map(function (TopicInterface $topic) {
            return $topic->getId();
        }, $topics);
        // remove $excluded from ids
        $selected = array_diff($selected, $excluded);

        return $selected;
    }
}
