<?php

namespace Project\Contact\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Project\Contact\Api\TopicRepositoryInterface;

/**
 * Class ParentTopic
 * @package Project\Contact\Ui\Component\Listing\Column
 */
class ParentTopic extends Column
{
    /**
     * @var TopicRepositoryInterface
     */
    protected $topicRepository;

    /**
     * ParentTopic constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param TopicRepositoryInterface $topicRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        TopicRepositoryInterface $topicRepository,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->topicRepository = $topicRepository;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {
                $parentTopicId = $items['parent_topic_id'];
                if ($parentTopicId == 0) {
                    $label = 'ROOT';
                } else {
                    $topic = $this->topicRepository->getById($parentTopicId);
                    $label = $topic->getCustomName() ? $topic->getCustomName() : $topic->getTitle();
                }
                $items['parent_topic_id'] = $label;
            }
        }
        return $dataSource;
    }
}
