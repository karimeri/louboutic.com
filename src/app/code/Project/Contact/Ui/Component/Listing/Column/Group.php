<?php

namespace Project\Contact\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Project\Contact\Helper\Data as ContactHelper;

/**
 * Class Group
 * @package Project\Contact\Ui\Component\Listing\Column
 */
class Group extends Column
{
    /**
     * @var ContactHelper
     */
    protected $contactHelper;

    /**
     * Group constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ContactHelper $contactHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ContactHelper $contactHelper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->contactHelper = $contactHelper;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {
                $groupId = $items['group_id'];
                $items['group_id'] = $this->contactHelper->getTopicGroupLabel($groupId);
            }
        }
        return $dataSource;
    }
}
