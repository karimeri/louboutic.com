<?php

namespace Project\Contact\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Model\ResourceModel\Topic;

/**
 * Class InstallSchema
 * @package Project\Contact\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $this->createTableTopic($setup);

        $installer->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function createTableTopic(SchemaSetupInterface $setup)
    {
        $tableTopic = $setup->getConnection()->newTable($setup->getTable(Topic::TABLE_NAME));

        $tableTopic->addColumn(
            TopicInterface::TOPIC_ID,
            Table::TYPE_SMALLINT,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true
            ],
            'Topic ID'
        )->addColumn(
            TopicInterface::PARENT_TOPIC_ID,
            Table::TYPE_SMALLINT,
            null,
            [
                'nullable' => false
            ],
            'Parent Topic ID'
        )->addColumn(
            TopicInterface::TITLE,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Title'
        )->addColumn(
            TopicInterface::STORES,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => 0
            ],
            'Stores'
        )->addColumn(
            TopicInterface::CUSTOM_NAME,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Custom Name'
        )->addColumn(
            TopicInterface::GROUP_ID,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
            ],
            'Group ID'
        )->addColumn(
            TopicInterface::SORT_ORDER,
            Table::TYPE_SMALLINT,
            null,
            [
                'nullable' => true,
                'default' => 0
            ],
            'Sort Order'
        )->addColumn(
            TopicInterface::CATEGORY_NAME,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => 'type'
            ],
            'Category Name'
        )->addColumn(
            TopicInterface::CATEGORY_VALUE,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Category Value'
        )->addColumn(
            TopicInterface::SUBCATEGORY_NAME,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => 'Sub_Type__c'
            ],
            'Subcategory Name'
        )->addColumn(
            TopicInterface::SUBCATEGORY_VALUE,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Subcategory Value'
        )->addColumn(
            TopicInterface::SF_ORIGIN,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Sf Origin'
        )->addColumn(
            TopicInterface::SF_CASE_RECORD_TYPE_NAME,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Sf Case Record Type Name'
        )->addColumn(
            TopicInterface::SF_CASE_RECORD_TYPE_VALUE,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Sf Case Record Type Value'
        )->addColumn(
            TopicInterface::SF_OTHER,
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => true
            ],
            'Sf Other'
        )->addColumn(
            TopicInterface::HEEL_HEIGHT,
            Table::TYPE_BOOLEAN,
            null,
            [
                'nullable' => true,
                'default' => 0
            ],
            'Heel Height'
        )->addColumn(
            TopicInterface::COLOR,
            Table::TYPE_BOOLEAN,
            null,
            [
                'nullable' => true,
                'default' => 0
            ],
            'Color'
        )->addColumn(
            TopicInterface::SIZE,
            Table::TYPE_BOOLEAN,
            null,
            [
                'nullable' => true,
                'default' => 0
            ],
            'Size'
        )->addColumn(
            TopicInterface::STYLE,
            Table::TYPE_BOOLEAN,
            null,
            [
                'nullable' => true,
                'default' => 0
            ],
            'Style'
        );

        $setup->getConnection()->createTable($tableTopic);

        $setup->getConnection()->addIndex(
            Topic::TABLE_NAME,
            $setup->getConnection()->getIndexName(
                Topic::TABLE_NAME,
                TopicInterface::PARENT_TOPIC_ID,
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            TopicInterface::PARENT_TOPIC_ID
        );

        $setup->getConnection()->addIndex(
            Topic::TABLE_NAME,
            $setup->getConnection()->getIndexName(
                Topic::TABLE_NAME,
                TopicInterface::STORES,
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            TopicInterface::STORES
        );

        $setup->getConnection()->addIndex(
            Topic::TABLE_NAME,
            $setup->getConnection()->getIndexName(
                Topic::TABLE_NAME,
                TopicInterface::GROUP_ID,
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            TopicInterface::GROUP_ID
        );
    }
}
