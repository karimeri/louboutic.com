<?php


namespace Project\Contact\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;


/**
 * Class InstallData
 * @package Project\Contact\Setup
 */

class UpgradeData implements UpgradeDataInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */

    public function upgrade(ModuleDataSetupInterface $setup , ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        // add new topic
        $this->addNewTopic($setup,$context);
        //update the order of topic
        $this->TopicsReoder($setup,$context);
        $installer->endSetup();
    }


    public function addNewTopic(ModuleDataSetupInterface $setup , ModuleContextInterface $context){

        $conn = $setup->getConnection();
        $tableName = $setup->getTable('project_contact_topics');
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $data = [
                'parent_topic_id' => 0,
                'title' => 'Account issues',
                'stores' => '9,12,15,18,21,27,30,36,42,45,48,51,57',
                'custom_name' => 'Account issues - Online inquiries',
                'group_id' => 'online_inquiries',
                'sort_order' => 4,
                'sf_origin' => 'Web to case',
                'sf_case_record_type_name' => 'recordType',
                'sf_case_record_type_value' => '012o0000000nN29',
                'sf_other' => 'status=1.+New'
            ];

            // Insert data to project_contact_topics
            $conn->insert($tableName, $data);

        }

    }

    public function TopicsReoder(ModuleDataSetupInterface $setup , ModuleContextInterface $context){

        $installer = $setup;
        $installer->startSetup();
        $tableName = $setup->getTable('project_contact_topics');
        $sql = "UPDATE " . $tableName . " SET sort_order = 5 where title='Other' and group_id='online_inquiries'" ;
        $installer->run($sql);

    }
}
