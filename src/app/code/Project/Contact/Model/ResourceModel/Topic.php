<?php

namespace Project\Contact\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Contact\Api\Data\TopicInterface;

/**
 * Class Topic
 * @package Project\Contact\Model\ResourceModel
 */
class Topic extends AbstractDb
{
    const TABLE_NAME = 'project_contact_topics';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, TopicInterface::TOPIC_ID);
    }
}
