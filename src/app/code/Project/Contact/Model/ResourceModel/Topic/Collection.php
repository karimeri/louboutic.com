<?php

namespace Project\Contact\Model\ResourceModel\Topic;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Contact\Model\Topic;
use Project\Contact\Model\ResourceModel\Topic as TopicResourceModel;

/**
 * Class Collection
 * @package Project\Contact\Model\ResourceModel\Topic
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(Topic::class, TopicResourceModel::class);
    }

    public function addStoreFilter($storeId)
    {
        $result = [];

        $topics = $this->getItems();
        foreach ($topics as $topic)
        {
            $topicStores = $topic->getStores();
            if (count(array_intersect([$storeId, 0], $topicStores)) > 0) {
                $result[] = $topic;
            }
        }

        return $result;
    }
}
