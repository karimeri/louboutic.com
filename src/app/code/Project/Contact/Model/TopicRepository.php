<?php

namespace Project\Contact\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Api\Data\TopicInterfaceFactory;
use Project\Contact\Api\Data\TopicSearchResultsInterfaceFactory;
use Project\Contact\Api\TopicRepositoryInterface;
use Project\Contact\Model\ResourceModel\Topic as TopicResource;
use Project\Contact\Model\ResourceModel\Topic\Collection as TopicCollection;
use Project\Contact\Model\ResourceModel\Topic\CollectionFactory as TopicCollectionFactory;

/**
 * Class TopicRepository
 * @package Project\Contact\Model
 */
class TopicRepository implements TopicRepositoryInterface
{
    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @var TopicResource
     */
    protected $resource;

    /**
     * @var TopicInterfaceFactory
     */
    protected $topicInterfaceFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var TopicSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var TopicCollectionFactory
     */
    protected $topicCollectionFactory;

    /**
     * TopicRepository constructor.
     * @param TopicResource $resource
     * @param TopicInterfaceFactory $topicInterfaceFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param TopicSearchResultsInterfaceFactory $topicSearchResultsInterfaceFactory
     * @param TopicCollectionFactory $topicCollectionFactory
     */
    public function __construct(
        TopicResource $resource,
        TopicInterfaceFactory $topicInterfaceFactory,
        DataObjectHelper $dataObjectHelper,
        TopicSearchResultsInterfaceFactory $topicSearchResultsInterfaceFactory,
        TopicCollectionFactory $topicCollectionFactory
    ) {
        $this->resource = $resource;
        $this->topicInterfaceFactory = $topicInterfaceFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->searchResultsFactory = $topicSearchResultsInterfaceFactory;
        $this->topicCollectionFactory = $topicCollectionFactory;
    }

    /**
     * @param TopicInterface $topic
     * @return \Magento\Framework\Model\AbstractModel|TopicInterface
     * @throws CouldNotSaveException
     */
    public function save(TopicInterface $topic)
    {
        /** @var TopicInterface|\Magento\Framework\Model\AbstractModel $topic */
        try {
            $this->resource->save($topic);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the topic: %1',
                $exception->getMessage()
            ));
        }
        return $topic;
    }

    /**
     * @param int $topicId
     * @return TopicInterface
     * @throws NoSuchEntityException
     */
    public function getById($topicId)
    {
        if (!isset($this->instances[$topicId])) {
            /** @var \Project\Contact\Api\Data\TopicInterface|\Magento\Framework\Model\AbstractModel $topic */
            $topic = $this->topicInterfaceFactory->create();
            $this->resource->load($topic, $topicId);
            if (!$topic->getId()) {
                throw new NoSuchEntityException(__('Requested topic doesn\'t exist'));
            }
            $this->instances[$topicId] = $topic;
        }
        return $this->instances[$topicId];
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Project\Contact\Api\Data\TopicSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {

        /** @var \Project\Contact\Api\Data\TopicSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Project\Contact\Model\ResourceModel\Topic\Collection $collection */
        $collection = $this->topicCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'topic_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Project\Contact\Api\Data\TopicInterface[] $topics */
        $topics = [];
        /** @var \Project\Contact\Model\Topic $topic */
        foreach ($collection as $topic) {
            /** @var \Project\Contact\Api\Data\TopicInterface $topicDataObject */
            $topicDataObject = $this->topicInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray($topicDataObject, $topic->getData(), TopicInterface::class);
            $topics[] = $topicDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($topics);
    }

    /**
     * @param TopicInterface $topic
     * @return bool
     * @throws StateException
     */
    public function delete(TopicInterface $topic)
    {
        /** @var \Project\Contact\Api\Data\TopicInterface|\Magento\Framework\Model\AbstractModel $topic */
        $id = $topic->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($topic);
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove topic %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * @param int $topicId
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById($topicId)
    {
        $topic = $this->getById($topicId);
        return $this->delete($topic);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param FilterGroup $filterGroup
     * @param TopicCollection $collection
     * @return $this
     */
    protected function addFilterGroupToCollection(FilterGroup $filterGroup, TopicCollection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
