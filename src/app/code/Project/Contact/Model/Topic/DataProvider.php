<?php

namespace Project\Contact\Model\Topic;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Project\Contact\Model\ResourceModel\Topic\Collection;
use Project\Contact\Model\ResourceModel\Topic\CollectionFactory;

/**
 * Class DataProvider
 * @package Project\Contact\Model\Topic
 */
class DataProvider extends AbstractDataProvider
{
    const DATA_PERSISTOR_NAME = 'project_contact_topic';

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $topicCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $topicCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $topicCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        /** @var \Project\Contact\Model\Topic[] $items */
        $items = $this->collection->getItems();

        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
        }

        $data = $this->dataPersistor->get(self::DATA_PERSISTOR_NAME);

        if (!empty($data)) {
            /** @var \Project\Contact\Model\Topic $model */
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear(self::DATA_PERSISTOR_NAME);
        }

        return $this->loadedData;
    }
}
