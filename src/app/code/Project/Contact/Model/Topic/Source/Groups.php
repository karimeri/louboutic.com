<?php

namespace Project\Contact\Model\Topic\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Project\Contact\Helper\Data as ContactHelper;

/**
 * Class Groups
 * @package Project\Contact\Model\Topic\Source
 */
class Groups implements OptionSourceInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @var ContactHelper
     */
    protected $contactHelper;

    /**
     * Groups constructor.
     * @param ContactHelper $contactHelper
     */
    public function __construct(
        ContactHelper $contactHelper
    ) {
        $this->contactHelper = $contactHelper;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $topicGroups = $this->contactHelper->getTopicGroups();
        $options = [];
        foreach ($topicGroups as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        $this->options = $options;

        return $this->options;
    }
}
