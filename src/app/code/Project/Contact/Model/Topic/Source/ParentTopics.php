<?php

namespace Project\Contact\Model\Topic\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Project\Contact\Model\ResourceModel\Topic\Collection as TopicCollection;
use Project\Contact\Model\ResourceModel\Topic\CollectionFactory as TopicCollectionFactory;

/**
 * Class ParentTopics
 * @package Project\Contact\Model\Topic\Source
 */
class ParentTopics implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $parentTopics = [];

    /**
     * @var TopicCollection
     */
    protected $collection;

    /**
     * ParentTopics constructor.
     * @param TopicCollectionFactory $topicCollectionFactory
     */
    public function __construct(
        TopicCollectionFactory $topicCollectionFactory
    ) {
        $this->collection = $topicCollectionFactory->create();
    }

    /**
     * Return array of options as value-label pairs
     *
     * @param int $topicId
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray($topicId = 0)
    {
        $parentTopics = $this->getParentTopics($topicId);

        $options = array();

        foreach ($parentTopics as $id => $title) {
            $options[] = [
                'value'     => $id,
                'label'     => __($title),
            ];
        }
        return $options;
    }

    /**
     * @param null|int $currentTopic
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getParentTopics($currentTopic = null)
    {
        if ($currentTopic && $currentTopic->getId()) {
            $topicId = $currentTopic->getId();
            $currentSubTopics = $currentTopic->getSubTopics();
            $currentParentId  = $currentTopic->getParentTopicId();
        } else {
            $topicId = 0;
            $currentSubTopics = [];
            $currentParentId = 0;
        }

        if (!isset($this->_parentTopics[$topicId])) {
            $this->parentTopics[$topicId] = array();

            if (empty($currentSubTopics)) {
                $topicsCollection = $this->collection;
            } else {
                $topicsCollection = [];
            }

            $this->parentTopics[$topicId][-1] = __('NO EXISTS');
            $this->parentTopics[$topicId][0] = __('ROOT');
            $currentParentExists = false;

            foreach ($topicsCollection as $topic) {
                if ($topic->getId() != $topicId && !$topic->getParentTopicId()) {
                    $this->parentTopics[$topicId][$topic->getId()] =
                        $topic->getCustomName() ? $topic->getCustomName() : $topic->getTitle();
                    if ($currentParentId == $topic->getId()) {
                        $currentParentExists = true;
                    }
                }
            }

            if ($currentTopic && (!$currentParentId || $currentParentExists)) {
                unset($this->parentTopics[$topicId][-1]);
            }
        }
        return $this->parentTopics[$topicId];
    }
}
