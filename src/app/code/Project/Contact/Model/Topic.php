<?php

namespace Project\Contact\Model;

use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Model\AbstractModel;
use Project\Contact\Api\Data\TopicInterface;
use Project\Contact\Model\ResourceModel\Topic as TopicResourceModel;

/**
 * Class Topic
 * @package Project\Contact\Model
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Topic extends AbstractModel implements TopicInterface, ExtensibleDataInterface
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'project_contact_topic';

    // phpcs:disable
    /**
     * @var string
     */
    protected $_cacheTag = 'project_contact_topic';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'project_contact_topic';
    // phpcs:enable

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Topic constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->storeManager = $storeManager;
    }

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        $this->_init(TopicResourceModel::class);
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->getData(TopicInterface::TOPIC_ID);
    }

    /**
     * @param string $topicId
     * @return TopicInterface
     */
    public function setId($topicId)
    {
        return $this->setData(TopicInterface::TOPIC_ID, $topicId);
    }

    /**
     * @return int|null
     */
    public function getParentTopicId()
    {
        return $this->getData(TopicInterface::PARENT_TOPIC_ID);
    }

    /**
     * @param int $parentTopicId
     * @return TopicInterface
     */
    public function setParentTopicId($parentTopicId)
    {
        return $this->setData(TopicInterface::PARENT_TOPIC_ID, $parentTopicId);
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->getData(TopicInterface::TITLE);
    }

    /**
     * @param string $title
     * @return TopicInterface
     */
    public function setTitle($title)
    {
        return $this->setData(TopicInterface::TITLE, $title);
    }

    /**
     * @return array|string|null
     */
    public function getStores()
    {
        if ($this->getData(TopicInterface::STORES)) {
            return explode(',', $this->getData(TopicInterface::STORES));
        }
        return [0];
    }

    /**
     * @param array|string $stores
     * @return TopicInterface
     */
    public function setStores($stores)
    {
        return $this->setData(TopicInterface::STORES, implode(',', $stores));
    }

    /**
     * @return string|null
     */
    public function getCustomName()
    {
        return $this->getData(TopicInterface::CUSTOM_NAME);
    }

    /**
     * @param string $customName
     * @return TopicInterface
     */
    public function setCustomName($customName)
    {
        return $this->setData(TopicInterface::CUSTOM_NAME, $customName);
    }

    /**
     * @return int|null
     */
    public function getGroupId()
    {
        return $this->getData(TopicInterface::GROUP_ID);
    }

    /**
     * @param int $groupId
     * @return TopicInterface
     */
    public function setGroupId($groupId)
    {
        return $this->setData(TopicInterface::GROUP_ID, $groupId);
    }

    /**
     * @return int|null
     */
    public function getSortOrder()
    {
        return $this->getData(TopicInterface::SORT_ORDER);
    }

    /**
     * @param int $sortOrder
     * @return TopicInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(TopicInterface::SORT_ORDER, $sortOrder);
    }

    /**
     * @return string|null
     */
    public function getCategoryName()
    {
        return $this->getData(TopicInterface::CATEGORY_NAME);
    }

    /**
     * @param string $categoryName
     * @return TopicInterface
     */
    public function setCategoryName($categoryName)
    {
        return $this->setData(TopicInterface::CATEGORY_NAME, $categoryName);
    }

    /**
     * @return string|null
     */
    public function getCategoryValue()
    {
        return $this->getData(TopicInterface::CATEGORY_VALUE);
    }

    /**
     * @param string $categoryValue
     * @return TopicInterface
     */
    public function setCategoryValue($categoryValue)
    {
        return $this->setData(TopicInterface::CATEGORY_VALUE, $categoryValue);
    }

    /**
     * @return string|null
     */
    public function getSubcategoryName()
    {
        return $this->getData(TopicInterface::SUBCATEGORY_NAME);
    }

    /**
     * @param string $subcategoryName
     * @return TopicInterface
     */
    public function setSubcategoryName($subcategoryName)
    {
        return $this->setData(TopicInterface::SUBCATEGORY_NAME, $subcategoryName);
    }

    /**
     * @return string|null
     */
    public function getSubcategoryValue()
    {
        return $this->getData(TopicInterface::SUBCATEGORY_VALUE);
    }

    /**
     * @param string $subcategoryValue
     * @return TopicInterface
     */
    public function setSubcategoryValue($subcategoryValue)
    {
        return $this->setData(TopicInterface::SUBCATEGORY_VALUE, $subcategoryValue);
    }

    /**
     * @return string|null
     */
    public function getSfOrigin()
    {
        return $this->getData(TopicInterface::SF_ORIGIN);
    }

    /**
     * @param string $sfOrigin
     * @return TopicInterface
     */
    public function setSfOrigin($sfOrigin)
    {
        return $this->setData(TopicInterface::SF_ORIGIN, $sfOrigin);
    }

    /**
     * @return string|null
     */
    public function getSfCaseRecordTypeName()
    {
        return $this->getData(TopicInterface::SF_CASE_RECORD_TYPE_NAME);
    }

    /**
     * @param string $sfCaseRecordTypeName
     * @return TopicInterface
     */
    public function setSfCaseRecordTypeName($sfCaseRecordTypeName)
    {
        return $this->setData(TopicInterface::SF_CASE_RECORD_TYPE_NAME, $sfCaseRecordTypeName);
    }

    /**
     * @return string|null
     */
    public function getSfCaseRecordTypeValue()
    {
        return $this->getData(TopicInterface::SF_CASE_RECORD_TYPE_VALUE);
    }

    /**
     * @param string $sfCaseRecordTypeValue
     * @return TopicInterface
     */
    public function setSfCaseRecordTypeValue($sfCaseRecordTypeValue)
    {
        return $this->setData(TopicInterface::SF_CASE_RECORD_TYPE_VALUE, $sfCaseRecordTypeValue);
    }

    /**
     * @return string|null
     */
    public function getSfOther()
    {
        return $this->getData(TopicInterface::SF_OTHER);
    }

    /**
     * @param string $sfOther
     * @return TopicInterface
     */
    public function setSfOther($sfOther)
    {
        return $this->setData(TopicInterface::SF_OTHER, $sfOther);
    }

    /**
     * @return bool|null
     */
    public function getHeelHeight()
    {
        return $this->getData(TopicInterface::HEEL_HEIGHT);
    }

    /**
     * @param bool $heelHeight
     * @return TopicInterface
     */
    public function setHeelHeight($heelHeight)
    {
        return $this->setData(TopicInterface::HEEL_HEIGHT, $heelHeight);
    }

    /**
     * @return bool|null
     */
    public function getColor()
    {
        return $this->getData(TopicInterface::COLOR);
    }

    /**
     * @param bool $color
     * @return TopicInterface
     */
    public function setColor($color)
    {
        return $this->setData(TopicInterface::COLOR, $color);
    }

    /**
     * @return bool|null
     */
    public function getSize()
    {
        return $this->getData(TopicInterface::SIZE);
    }

    /**
     * @param bool $size
     * @return TopicInterface
     */
    public function setSize($size)
    {
        return $this->setData(TopicInterface::SIZE, $size);
    }

    /**
     * @return bool|null
     */
    public function getStyle()
    {
        return $this->getData(TopicInterface::STYLE);
    }

    /**
     * @param bool $style
     * @return TopicInterface
     */
    public function setStyle($style)
    {
        return $this->setData(TopicInterface::STYLE, $style);
    }

    /**
     * @return bool
     */
    public function hasAdditionalFields()
    {
        if ($this->getHeelHeight() || $this->getColor() || $this->getSize() || $this->getStyle()) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getAdditionalFields()
    {
        $data = [];
        foreach (TopicInterface::ADDITIONAL_FIELDS as $field) {
            if ($this->getDataByKey($field) == true) {
                array_push($data, $field);
            }
        }
        return $data;
    }

    /**
     * @return bool
     */
    public function canDisplay()
    {
        $stores = $this->getStores();
        if (in_array(0, $stores) || in_array($this->storeManager->getStore()->getId(), $stores)) {
            return true;
        }

        return false;
    }
}
