<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\Checkout\Controller\Cart;

use Magento\Checkout\Model\Cart\RequestQuantityProcessor;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Post update shopping cart.
 */
class UpdatePost extends \Magento\Checkout\Controller\Cart\UpdatePost
{
    /**
     * @var RequestQuantityProcessor
     */
    private $quantityProcessor;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */
    protected $stockState;

    /**
     * @var \Psr\Log\LoggerInterface gi
     */
    protected $_logger;

    /**
     * UpdatePost constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param \Magento\Checkout\Model\Cart $cart
     * @param RequestQuantityProcessor|null $quantityProcessor
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockState
     * @param ProductRepositoryInterface $productRepository
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Checkout\Model\Cart $cart,
        RequestQuantityProcessor $quantityProcessor = null,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        ProductRepositoryInterface $productRepository,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $quantityProcessor
        );

        $this->quantityProcessor = $quantityProcessor ?: $this->_objectManager->get(RequestQuantityProcessor::class);
        $this->stockState = $stockState;
        $this->productRepository = $productRepository;
        $this->_logger = $logger;
    }

    /**
     * Empty customer's shopping cart
     *
     * @return void
     */
    protected function _emptyShoppingCart()
    {
        try {
            $this->cart->truncate()->save();
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addExceptionMessage($exception, __('We can\'t update the shopping cart.'));
        }
    }

    /**
     * Update customer's shopping cart
     *
     * @return void
     */
    protected function _updateShoppingCart()
    {
        try {
            $cartData = $this->getRequest()->getParam('cart');
            if (is_array($cartData) & !empty($cartData)) {
                if (!$this->cart->getCustomerSession()->getCustomerId() && $this->cart->getQuote()->getCustomerId()) {
                    $this->cart->getQuote()->setCustomerId(null);
                }
                $storeId = $this->_storeManager->getStore()->getId();
                $FinalcartData = array();
                if (!isset($cartData[0])) {
                    if (!(is_array($cartData) && array_key_exists('is_preorder', $cartData))) {
                        $message = __(
                            'The size you are interested in is no longer available. Your item has recently sold. We apologize for any inconvenience. Please make another selection.');
                        foreach ($cartData as $key_ => $items) {
                            $qty = 0;
                            foreach ($items as $key => $item) {
                                $qty = $qty + $item['qty'];
                                $option_id = $item['option_id'];
                                if ($option_id == 0) {
                                    $option_id = $key_;
                                    $product = $this->productRepository->getById($option_id, false, $storeId);
                                    $message = __(
                                        "We don't have as many %1 as you requested.", $product->getName());
                                }
                                $stock_qty = (int)$this->stockState->getStockQty($option_id);
                                if ($item['qty'] > $stock_qty) {
                                    $this->messageManager->addErrorMessage(
                                        $message
                                    );
                                    return $this->_goBack();
                                }
                                unset($item['option_id']);
                                $FinalcartData[$key] = $item;
                            }
                            if ($qty > 2) {
                                $message = __(
                                    'You have exceeded the maximum number of items per order. We apologize for the inconvenience and invite you to reduce the quantity ordered.');
                                $this->messageManager->addErrorMessage(
                                    $message
                                );
                                return $this->_goBack();
                            }
                        }
                    } else {
                        unset($cartData['is_preorder']);
                        foreach ($cartData as $key_ => $items) {
                            foreach ($items as $key => $item) {
                                unset($item['option_id']);
                                $FinalcartData[$key] = $item;
                            }
                        }
                    }
                    $cartData = $this->quantityProcessor->process($FinalcartData);
                    $cartData = $this->cart->suggestItemsQty($cartData);
                    $this->cart->updateItems($cartData)->save();
                }
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage(
                $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
            );
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('We can\'t update the shopping cart.'));
            $this->_logger->critical($e);
        }
    }

    /**
     * Update shopping cart data action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

        switch ($updateAction) {
            case 'empty_cart':
                $this->_emptyShoppingCart();
                break;
            case 'update_qty':
                $this->_updateShoppingCart();
                break;
            default:
                $this->_updateShoppingCart();
        }

        return $this->_goBack();
    }
}
