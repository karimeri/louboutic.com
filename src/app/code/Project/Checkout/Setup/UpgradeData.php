<?php
namespace Project\Checkout\Setup;

use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;

/**
 * Class UpgradeData
 * @package Project\Checkout\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    const XML_PATH_GUEST_CHECKOUT = 'checkout/options/guest_checkout';

    /**
     * Configuration value of whether to display billing address on payment method or payment page
     */
    const DISPLAY_BILLING_ADDRESS_ON_CONFIG_PATH = 'checkout/options/display_billing_address_on';

    const CHECKMO_PAYMENT_METHOD_CONFIG_PATH = 'payment/checkmo/active';

    /**
     * @var ConfigInterface
     */
    protected $configResource;

    /**
     * UpgradeData constructor.
     * @param ConfigInterface $configResource
     */
    public function __construct(
        EavSetup $eavSetup,
        ConfigInterface $configResource
    )
    {
        $this->eavSetup = $eavSetup;
        $this->configResource = $configResource;
    }

    /**
     * Update Customer Configuration
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->updateRequireEmailsConfirmationConfig();
        }
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->updateDisplayBillingAddressOnPaymentMethodOrPageConfig();
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $this->updateCheckmoPaymentMethodOrConfig();
        }

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    private function updateRequireEmailsConfirmationConfig()
    {
        $this->configResource->saveConfig(
            self::XML_PATH_GUEST_CHECKOUT,
            0,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
    }

    /**
     * Display BillingAddress On Payment Method
     */
    private function updateDisplayBillingAddressOnPaymentMethodOrPageConfig()
    {
        $this->configResource->saveConfig(
            self::DISPLAY_BILLING_ADDRESS_ON_CONFIG_PATH,
            0,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
    }

    /**
     * Disable Checkmo Payment Method
     */
    private function updateCheckmoPaymentMethodOrConfig()
    {
        $this->configResource->saveConfig(
            self::CHECKMO_PAYMENT_METHOD_CONFIG_PATH,
            0,
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            \Magento\Store\Model\Store::DEFAULT_STORE_ID
        );
    }
}




