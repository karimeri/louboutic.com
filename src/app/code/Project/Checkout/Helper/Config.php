<?php

namespace Project\Checkout\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 *
 * @package Project\Sales\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_CHECKOUT_TAX_SHIPPING_ESTIMATION_ENABLE = 'louboutin_checkout/tax_shipping_estimation/enable';

    /**
     * @param int|null $scopeCode
     *
     * @return bool
     */
    public function displayShippingTaxFees($scopeCode = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_CHECKOUT_TAX_SHIPPING_ESTIMATION_ENABLE,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }
}
