<?php
namespace Project\Checkout\Block\Cart;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Checkout\Block\Cart\Shipping as BaseShipping;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Checkout\Model\CompositeConfigProvider;
use Magento\Framework\View\Element\Template\Context;

use Project\Checkout\Helper\Config;

/**
 * Class Shipping
 *
 * @package Project\Checkout\Block\Cart
 * @author  Synolia <contact@synolia.com>
 */
class Shipping extends BaseShipping
{
    /**
     * @var Config
     */
    protected $helper;

    /**
     * @param Context                 $context
     * @param Session                 $customerSession
     * @param CheckoutSession         $checkoutSession
     * @param CompositeConfigProvider $configProvider
     * @param CompositeConfigProvider $configProvider
     * @param Config                  $helper
     * @param array                   $layoutProcessors
     * @param array                   $data
     * @param Json|null               $serializer
     *
     * @throws \RuntimeException
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        CompositeConfigProvider $configProvider,
        Config $helper,
        array $layoutProcessors = [],
        array $data = [],
        Json $serializer = null
    ) {
        $this->helper = $helper;

        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $configProvider,
            $layoutProcessors,
            $data,
            $serializer
        );
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->helper->displayShippingTaxFees();
    }
}
