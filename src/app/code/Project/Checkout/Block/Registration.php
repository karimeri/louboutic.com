<?php

namespace Project\Checkout\Block;

/**
 * Class Registration
 * @package Project\Checkout\Block
 */
class Registration extends \Magento\Checkout\Block\Registration
{
    /**
     * Retrieve account creation url
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getCreateAccountUrl()
    {
        return $this->getUrl('checkout/account/create');
    }
}
