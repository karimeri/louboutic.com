<?php
namespace Project\Checkout\Block\Checkout;

use Magento\Checkout\Block\Checkout\AttributeMerger;
use Magento\Checkout\Helper\Data;
use Magento\Customer\Model\AttributeMetadataDataProvider;
use Magento\Ui\Component\Form\AttributeMapper;

/**
 * Class LayoutProcessor
 * @package Project\Checkout\Block\Checkout
 * @author Synolia <contact@synolia.com>
 */
class LayoutProcessor implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{
    /**
     * @var \Magento\Customer\Model\AttributeMetadataDataProvider
     */
    protected $dataProvider;

    /**
     * @var \Magento\Ui\Component\Form\AttributeMapper
     */
    protected $attributeMapper;

    /**
     * @var \Magento\Checkout\Block\Checkout\AttributeMerger
     */
    protected $merger;

    /**
     * @var Data
     */
    protected $checkoutDataHelper;

    /**
     * LayoutProcessor constructor.
     * @param AttributeMetadataDataProvider $dataProvider
     * @param AttributeMapper $attributeMapper
     * @param AttributeMerger $merger
     * @param Data|null $checkoutDataHelper
     */
    public function __construct(
        AttributeMetadataDataProvider $dataProvider,
        AttributeMapper $attributeMapper,
        AttributeMerger $merger,
        Data $checkoutDataHelper = null
    ) {
        $this->dataProvider = $dataProvider;
        $this->attributeMapper = $attributeMapper;
        $this->merger = $merger;
        $this->checkoutDataHelper = $checkoutDataHelper ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(Data::class);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getAddressAttributes()
    {
        /** @var \Magento\Eav\Api\Data\AttributeInterface[] $attributes */
        $attributes = $this->dataProvider->loadAttributesCollection(
            'customer_address',
            'customer_register_address'
        );

        $elements = [];
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            if ($attribute->getIsUserDefined()) {
                continue;
            }
            $elements[$code] = $this->attributeMapper->map($attribute);
            if (isset($elements[$code]['label'])) {
                $label = $elements[$code]['label'];
                $elements[$code]['label'] = __($label);
            }
        }
        return $elements;
    }

    /**
     * @param array $jsLayout
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function process($jsLayout)
    {
        $elements      = $this->getAddressAttributes();

        // The if billing address should be displayed on Payment method or page
        if ($this->checkoutDataHelper->isDisplayBillingOnPaymentMethodAvailable()) {
            $configuration = $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children'];
            if (!isset($configuration['payments-list']['children'])) {
                $configuration['payments-list']['children'] = [];
            }
        } else {
            $configuration = $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']['payments-list']['children'];
        }

        if (isset($configuration)) {
             foreach ($configuration as $paymentGroup => $groupConfig) {
                if (isset($groupConfig['component']) &&
                        $groupConfig['component'] === 'Magento_Checkout/js/view/billing-address') {
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children']
                        [$paymentGroup]['children']['form-fields']['children'] =
                            $this->merger->merge(
                                $elements,
                                'checkoutProvider',
                                'billingAddress' . $paymentGroup,
                                [
                                    'telephone' => [
                                        'config' => [
                                            'tooltip' => false,
                                        ],
                                    ],
                                    'street' => [
                                        'children' => [
                                            0 => [
                                                'config' => [
                                                    'tooltip' => [
                                                        // phpcs:ignore
                                                        'description' => __('Please ensure that this address matches the card holder details and in particular the address held by the card issuer.'),
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            );
                }
             }
        }
        return $jsLayout;
    }
}
