<?php

namespace Project\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderRepository;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Sales\Model\Magento\Sales\Order;
use Magento\Framework\Event\Observer;

/**
 * Class OnepageSuccess
 * @package Project\Checkout\Observer
 * @author  Synolia <contact@synolia.com>
 */
class OnepageSuccess implements ObserverInterface
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * OnepageSuccess constructor.
     * @param \Magento\Sales\Model\OrderRepository     $orderRepository
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        OrderRepository $orderRepository,
        EnvironmentManager $environmentManager
    ) {
        $this->orderRepository    = $orderRepository;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param Observer $observer
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $orderId = $observer->getOrderIds()[0];
        /**@var $order Order */
        $order = $this->orderRepository->get($orderId);

        $payment         = $order->getPayment();
        $additionnalInfo = $payment->getAdditionalInformation();

        if (isset($additionnalInfo['method']) && $additionnalInfo['method'] == 'adyen_cc') {
            $orderState = $order->getState();
            switch ($orderState) {
                case Order::STATE_PROCESSING:
                case Order::STATE_PENDING_PAYMENT:
                    if ($this->environmentManager->getEnvironment() == Environment::US) {
                        $amount = '$1.00';
                    } else {
                        $amount = round($order->getGrandTotal(), 2);
                    }
                    $message = 'Preauthorized amount of '.$amount.'. Transaction ID = '
                        .$additionnalInfo['last_trans_id'];
                    $this->addHistoryComment($order, $message);
                    break;
                case Order::STATE_PAYMENT_REVIEW:
                    $message = 'Authorizing amount of '.round($order->getGrandTotal(), 2).
                        ' is pending approval on gateway. Transaction ID = '
                        .$additionnalInfo['last_trans_id'];
                    $this->addHistoryComment($order, $message);
                    break;
            }
        }
    }

    /**
     * @param \Project\Sales\Model\Magento\Sales\Order $order
     * @param string                                   $message
     */
    private function addHistoryComment(Order $order, $message)
    {
        $order->addStatusHistoryComment($message)
            ->setIsCustomerNotified(false);
        $this->orderRepository->save($order);
    }
}
