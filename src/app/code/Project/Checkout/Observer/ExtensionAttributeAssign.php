<?php

namespace Project\Checkout\Observer;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

/**
 * Class ExtensionAttributeAssign
 *
 * @package Project\Checkout\Observer
 * @author Synolia <contact@synolia.com>
 */
class ExtensionAttributeAssign implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        $billingAddress = $quote->getBillingAddress();
        $shippingAddress = $quote->getShippingAddress();

        foreach ((array)$billingAddress->getExtensionAttributes() as $code => $value) {
            $billingAddress->setCustomAttribute($code, $value);
        }

        foreach ((array)$shippingAddress->getExtensionAttributes() as $code => $value) {
            $shippingAddress->setCustomAttribute($code, $value);
        }

        $quote->setBillingAddress($billingAddress);
        $quote->setShippingAddress($shippingAddress);
    }
}
