var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-billing-address': {
                'Project_Checkout/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'Project_Checkout/js/action/set-billing-address-mixin': true
            }
        }
    }
};
