define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setBillingAddressAction) {
        return wrapper.wrap(setBillingAddressAction, function (originalAction) {
            var billingAddress = quote.billingAddress();

            if (billingAddress && billingAddress.customAttributes) {
                if (billingAddress['extension_attributes'] === undefined) {
                    billingAddress['extension_attributes'] = {};
                }

                if (billingAddress.customAttributes['contact_telephone']) {
                    var contactTelephone = billingAddress.customAttributes['contact_telephone'];

                    if ($.isPlainObject(contactTelephone)) {
                        contactTelephone = contactTelephone['value'];
                    }

                    billingAddress['extension_attributes']['contact_telephone'] = contactTelephone;
                }

                if (billingAddress.customAttributes['firstname_en']) {
                    var firstnameEn = billingAddress.customAttributes['firstname_en'];

                    if ($.isPlainObject(firstnameEn)) {
                        firstnameEn = firstnameEn['value'];
                    }

                    billingAddress['extension_attributes']['firstname_en'] = firstnameEn;
                }

                if (billingAddress.customAttributes['lastname_en']) {
                    var lastnameEn = billingAddress.customAttributes['lastname_en'];

                    if ($.isPlainObject(lastnameEn)) {
                        lastnameEn = lastnameEn['value'];
                    }

                    billingAddress['extension_attributes']['lastname_en'] = lastnameEn;
                }

            }

            return originalAction();
        });
    };
});
