<?php

namespace Project\Checkout\Plugin\CustomerData;

use Magento\Quote\Model\Quote\Item;

/**
 * Class DefaultItemPlugin
 * @package Project\Checkout\Plugin\CustomerData
 * @author Synolia <contact@synolia.com>
 */
class DefaultItemPlugin
{
    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * DefaultItemPlugin constructor.
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     */
    public function __construct(
        \Magento\Checkout\Helper\Data $checkoutHelper
    ) {
        $this->checkoutHelper = $checkoutHelper;
    }

    /**
     * @param \Magento\Checkout\CustomerData\AbstractItem $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        \Closure $proceed,
        Item $item
    ) {
        $parentResult = $proceed($item);

        $return = array_merge(
            $parentResult,
            [
                'product_is_preorder' => !is_null($item->getIsPreorder()),
            ]
        );

        return $return;
    }
}
