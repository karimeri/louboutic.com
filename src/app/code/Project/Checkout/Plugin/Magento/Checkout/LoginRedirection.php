<?php

namespace Project\Checkout\Plugin\Magento\Checkout;
/**
 * Class LoginRedirection
 * @package Project\Checkout\Plugin\Magento\Checkout
 */
class LoginRedirection
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * LoginRedirection constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->storeManager = $storeManager;
        $this->customerSession = $customerSession;
    }

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterLoadCustomerQuote(
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        if (count($checkoutSession->getQuote()->getAllVisibleItems()) > 0) {
            $this->customerSession
                ->setBeforeAuthUrl($this->storeManager->getStore()->getUrl('checkout/cart/'));
        }
    }
}
