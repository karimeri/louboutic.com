<?php

namespace Project\Checkout\Plugin\Magento\Checkout\Helper;

/**
 * Class CartPlugin
 *
 * @package Project\Checkout\Plugin\Magento\Checkout\Helper
 * @author Synolia <contact@synolia.com>
 */
class CartPlugin
{
    /**
     * @var \Project\Core\Helper\AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @param \Project\Core\Helper\AttributeSet $attributeSetHelper
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Project\Core\Helper\AttributeSet $attributeSetHelper,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->attributeSetHelper = $attributeSetHelper;
        $this->quoteRepository = $quoteRepository;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\Helper\Cart $subject
     * @param \Magento\Checkout\Model\Cart $result
     *
     * @return \Magento\Checkout\Model\Cart
     */
    public function afterGetCart(\Magento\Checkout\Helper\Cart $subject, $result)
    {
        $gift = false;
        foreach ($result->getQuote()->getAllVisibleItems() as $item) {
            if (!$this->attributeSetHelper->isBeauty($item->getProduct())) {
                $gift = true;
                break;
            }
        }

        if (!$gift) {
            $this->resetAllGiftInformations();
            $this->quoteRepository->save($this->checkoutSession->getQuote());
            $result->setQuote($this->checkoutSession->getQuote());
        }

        return $result;
    }

    /**
     * Reset all gif informations if only beauty product
     */
    private function resetAllGiftInformations()
    {
        $commonData = [
            'gift_cards' => null,
            'gift_cards_amount' => 0.0000,
            'base_gift_cards_amount' => 0.0000,
            'gift_message_id' => null,
            'gw_id' => null,
            'gw_allow_gift_receipt' => null,
            'gw_add_card' => null,
            'gw_base_price' => 0.0000,
            'gw_price' => 0.0000,
            'gw_items_base_price' => 0.0000,
            'gw_items_price' => 0.0000,
            'gw_card_base_price' => 0.0000,
            'gw_card_price' => 0.0000,
            'gw_base_tax_amount' => null,
            'gw_tax_amount' => null,
            'gw_items_base_tax_amount' => null,
            'gw_items_tax_amount' => null,
            'gw_card_base_tax_amount' => null,
            'gw_card_tax_amount' => null,
            'gw_base_price_incl_tax' => null,
            'gw_price_incl_tax' => null,
            'gw_items_base_price_incl_tax' => null,
            'gw_items_price_incl_tax' => null,
            'gw_card_base_price_incl_tax' => null,
            'gw_card_price_incl_tax' => null
        ];

        $quoteData = array_merge($commonData, [
            'gift_cards_amount_used' => 0.0000,
            'base_gift_cards_amount_used' => 0.0000
        ]);

        $this->checkoutSession->getQuote()->addData($quoteData);

        $this->checkoutSession->getQuote()->getShippingAddress()->addData($commonData);
    }
}
