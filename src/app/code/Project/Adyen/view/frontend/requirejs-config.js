var config = {
    map: {
        '*': {
            'Adyen_Payment/template/payment/cc-form.html':
                'Project_Adyen/template/payment/cc-form.html',
            'Adyen_Payment/template/payment/hpp-form.html':
                'Project_Adyen/template/payment/hpp-form.html'
        }
    }
};
