<?php

namespace Project\Adyen\Console\Command;

use Adyen\Payment\Model\Notification;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class AdyenRescue
 * @package Project\Adyen\Console\Command
 */
class AdyenRescue extends Command
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Adyen\Payment\Model\ResourceModel\Notification\CollectionFactory
     */
    protected $_notificationFactory;

    /**
     * Adyen Rescue constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Adyen\Payment\Model\ResourceModel\Notification\CollectionFactory $notificationFactory
     */
    public function __construct(
        State $state,
        \Adyen\Payment\Model\ResourceModel\Notification\CollectionFactory $notificationFactory,
        \Adyen\Payment\Logger\AdyenLogger $adyenLogger,
        $name = null
    ) {
        parent::__construct($name);
        $this->_notificationFactory = $notificationFactory;
        $this->state = $state;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('louboutin:adyen:rescue')->setDescription('Adyen Notification Rescue');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $output->writeln("<info>Adyen Notification Rescue Start ...</info>");

            $notifications = $this->_notificationFactory->create();
            //get notifications collection
            $notifications->adyenRescueFilter();

            // Loop thorugh notifications to reset  to processing false if notifiaction is blocked
            foreach ($notifications as $notification) {
                // Check if notification should be processed or not
                if ($this->shouldSkipProcessingNotification($notification)) {
                    // Remove notification from collection which will be processed
                    $notifications->removeItemByKey($notification->getId());
                    continue;
                }
                // reset notification processing
                $this->_resetNotification($notification, false);
                $output->writeln("<info>Adyen Notification : Transaction ".$notification->getPspreference()." reset...</info>");
            }

            $output->writeln("<info>Adyen Notification Rescue End ...</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }

    /**
     * @param $notification
     * @return bool
     */
    protected function shouldSkipProcessingNotification($notification)
    {
        // OFFER_CLOSED notifications needs to be at least 10 minutes old to be processed
        $offerClosedMinDate = new \DateTime();
        $offerClosedMinDate->modify('-10 minutes');

        // Remove OFFER_CLOSED notifications arrived in the last 10 minutes from the list to process to ensure it
        // won't close any order which has an AUTHORISED notification arrived a bit later than the OFFER_CLOSED one.
        $createdAt = \DateTime::createFromFormat('Y-m-d H:i:s', $notification['created_at']);
        // To get the difference between $offerClosedMinDate and $createdAt, $offerClosedMinDate time in seconds is
        // deducted from $createdAt time in seconds, divided by 60 and rounded down to integer
        $minutesUntilProcessing = floor(($createdAt->getTimestamp() - $offerClosedMinDate->getTimestamp()) / 60);
        if ($notification['event_code'] == Notification::OFFER_CLOSED && $minutesUntilProcessing > 0) {
            return true;
        }

        return false;
    }


        /**
         * @param $notification
         * @param $processing
         * @param $done
         */
        protected function _resetNotification($notification, $processing)
        {
            $notification->setProcessing($processing);
            $notification->setUpdatedAt(new \DateTime());
            $notification->save();
        }
}
