<?php

namespace Project\Adyen\Gateway\Request;

/**
 * Payment Data Builder
 */
class PaymentDataBuilder extends \Adyen\Payment\Gateway\Request\PaymentDataBuilder
{
    /**
     * @var \Adyen\Payment\Helper\Requests
     */
    private $adyenRequestsHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * @var \Magento\Sales\Model\AdminOrder\Create
     */
    protected $_backendOrder;

    /**
     * PaymentDataBuilder constructor.
     *
     * @param \Adyen\Payment\Helper\Requests $adyenRequestsHelper
     */
    public function __construct(
        \Adyen\Payment\Helper\Requests $adyenRequestsHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\State $state,
        \Magento\Sales\Model\AdminOrder\Create $backendOrder
    )
    {
        $this->adyenRequestsHelper = $adyenRequestsHelper;
        $this->_checkoutSession = $checkoutSession;
        $this->_state = $state;
        $this->_backendOrder = $backendOrder;
    }

    /**
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject)
    {
        /** @var \Magento\Payment\Gateway\Data\PaymentDataObject $paymentDataObject */
        $paymentDataObject = \Magento\Payment\Gateway\Helper\SubjectReader::readPayment($buildSubject);

        $order = $paymentDataObject->getOrder();
        $payment = $paymentDataObject->getPayment();
        $fullOrder = $payment->getOrder();

        $currencyCode = $fullOrder->getOrderCurrencyCode();
        $amount = $fullOrder->getGrandTotal();
        $reference = $order->getOrderIncrementId();
        $paymentMethod = $payment->getMethod();

        if ($this->isPreorder()) {
            $amount = 1;
        }

        $request['body'] = $this->adyenRequestsHelper->buildPaymentData(
            $amount,
            $currencyCode,
            $reference,
            $paymentMethod,
            []
        );

        return $request;
    }

    /**
     * @return \Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuote()
    {
        if ('adminhtml' === $this->getArea()) {
            return $quote = $this->_backendOrder->getQuote();
        }
        return $this->_checkoutSession->getQuote();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getArea()
    {
        return $this->_state->getAreaCode();
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isPreorder()
    {
        $quote = $this->getQuote();
        if ($quote->getItemsCount() > 0) {
            $i = 1;
            foreach ($quote->getAllVisibleItems() as $item) {
                /** @var \Magento\Quote\Model\Quote\Item $item */
                if ($item->getIsPreorder()) {
                    return true;
                }
            }
        }

        return false;
    }
}
