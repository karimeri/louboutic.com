<?php
namespace Project\Adyen\Gateway\Http\Client;

use Magento\Payment\Gateway\Http\ClientInterface;
use Adyen\Payment\Model\ApplicationInfo;

/**
 * Class TransactionPayment
 */
class TransactionPayment implements ClientInterface
{
    const SHOPPER_INTERACTION = "Ecommerce";
    const RECURRING_PROCESSING_MODEL = "UnscheduledCardOnFile";

    /**
     * @var \Adyen\Payment\Helper\Data
     */
    private $adyenHelper;

    /**
     * @var ApplicationInfo
     */
    private $applicationInfo;

    /**
     * @var \Adyen\Payment\Logger\AdyenLogger
     */
    protected $_adyenLogger;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var \Magento\GiftMessage\Model\MessageFactory
     */
    protected $_messageFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orders;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * @var \Magento\Sales\Model\AdminOrder\Create
     */
    protected $_backendOrder;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Magento\Framework\HTTP\Header
     */
    protected $httpHeader;

    /**
     * TransactionPayment constructor.
     * @param \Adyen\Payment\Helper\Data $adyenHelper
     * @param ApplicationInfo $applicationInfo
     * @param \Adyen\Payment\Logger\AdyenLogger $adyenLogger
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\GiftMessage\Model\MessageFactory $messageFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Sales\Model\AdminOrder\Create $backendOrder
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Framework\HTTP\Header $httpHeader
     */
    public function __construct(
        \Adyen\Payment\Helper\Data $adyenHelper,
        \Adyen\Payment\Model\ApplicationInfo $applicationInfo,
        \Adyen\Payment\Logger\AdyenLogger $adyenLogger,
        \Magento\Sales\Model\Order $order,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\GiftMessage\Model\MessageFactory $messageFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\State $state,
        \Magento\Sales\Model\AdminOrder\Create $backendOrder,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\HTTP\Header $httpHeader
    ) {
        $this->adyenHelper = $adyenHelper;
        $this->applicationInfo = $applicationInfo;
        $this->_adyenLogger = $adyenLogger;
        $this->_order = $order;
        $this->_cart = $cart;
        $this->_customerSession = $customerSession;
        $this->_messageFactory = $messageFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_state = $state;
        $this->_backendOrder = $backendOrder;
        $this->_categoryFactory = $categoryFactory;
        $this->httpHeader = $httpHeader;
    }

    /**
     * @param \Magento\Payment\Gateway\Http\TransferInterface $transferObject
     * @return array|mixed|string
     * @throws \Adyen\AdyenException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function placeRequest(\Magento\Payment\Gateway\Http\TransferInterface $transferObject)
    {
        $request = $transferObject->getBody();
        $headers = $transferObject->getHeaders();

        if(!isset($request["additionalData"])){
            $request["additionalData"] = [];
        }

        $area = $this->getArea();

        $giftMessage = $this->_messageFactory->create()
            ->load($this->getQuote()->getGiftMessageId())
            ->getMessage();

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->getQuote();

        $billingAddress = $quote->getBillingAddress();
        $shippingAddress = $quote->getShippingAddress();

        $paymentMethod = false;
        if($quote->getPayment()){
            $paymentMethod = $quote->getPayment()->getMethod();
        }

        //browser version
        $userAgent = $this->httpHeader->getHttpUserAgent();
        $browserVersion = $this->_chromeVersion($userAgent);

        $risksData = [
            'riskdata.ecomareafobo' => ('adminhtml' == $area) ? 'BO' : 'FO',
            'riskdata.customertype' => ($this->getCustomer()->getId() || 'adminhtml' === $area) ? 'Registered' : 'Guest',
            'riskdata.giftmessage' => ($giftMessage) ? $giftMessage : '',
            'riskdata.customerprevrefunded' => ($this->getRefundsByCustomer()->count() > 0) ? 'Yes' : 'No',
            'riskdata.billingname' => $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname(),
            'riskdata.shippingmethod' => $shippingAddress->getShippingMethod(),
            'riskdata.promotionCode' => $quote->getCouponCode() ? $quote->getCouponCode() : 'null',
            'riskdata.promotiondiscountamount' => $this->getDiscountAmountOrder() ? $this->getDiscountAmountOrder() : 'null',
            'riskdata.browserversion' => (!is_null($browserVersion)) ? $browserVersion :'null'
        ];

        $request['additionalData'] = $request['additionalData'] + $risksData;

        $isPreorder = false;
        if($quote->getItemsCount() > 0) {
            $i=1;
            foreach ($quote->getAllVisibleItems() as $item) {
                /** @var \Magento\Quote\Model\Quote\Item $item */
                if ($item->getIsPreorder()) {
                    $isPreorder = true;
                }
                $cat_name = [];
                foreach ($item->getProduct()->getCategoryIds() as $category_id) {
                    $category = $this->_categoryFactory->create()->load($category_id);
                    if(isset($category)){
                        $cat_name[] = $category->getName();
                    }
                }
                $request["additionalData"]["riskdata.basket.item".$i.".amountPerItem"] = $item->getProduct()->getPrice();
                $request["additionalData"]["riskdata.basket.item".$i.".quantity"] = $item->getQty();
                $request["additionalData"]["riskdata.basket.item".$i.".sku"] = $item->getSku();
                $request["additionalData"]["riskdata.basket.item".$i.".category"] = (!empty($cat_name)) ? implode(', ',$cat_name) : '';
                $i++;
            }
        }

        if($paymentMethod && $paymentMethod == "adyen_hpp"){
            $risksPaypalData = [
                'riskdata.paypalfirstname' => $billingAddress->getFirstname(),
                'riskdata.paypallastname' => $billingAddress->getLastname(),
                'riskdata.paypaladdressstreet' => $billingAddress->getStreet(),
                'riskdata.paypaladdresscity' => $billingAddress->getCity(),
                'riskdata.paypaladdresspostcode' => $billingAddress->getPostcode(),
                'riskdata.paypalmerchantprotection' => ''
            ];

            $request["additionalData"] = $request["additionalData"] + $risksPaypalData;
        }

        $this->_adyenLogger->info("additionalData : ",$request["additionalData"]);

        // If the payments call is already done return the request
        if (!empty($request['resultCode'])) {
            //Initiate has already a response
            return $request;
        }

        $client = $this->adyenHelper->initializeAdyenClient();

        $service = $this->adyenHelper->createAdyenCheckoutService($client);

        $requestOptions = [];

        if (!empty($headers['idempotencyKey'])) {
            $requestOptions['idempotencyKey'] = $headers['idempotencyKey'];
        }

        $request = $this->applicationInfo->addMerchantApplicationIntoRequest($request);

        if ($isPreorder) {
            $request["shopperInteraction"] = self::SHOPPER_INTERACTION;
            $request["recurringProcessingModel"] = self::RECURRING_PROCESSING_MODEL;
            $request["shopperReference"] = $billingAddress->getEmail()."_".$quote->getEntityId();
            $request["storePaymentMethod"] = true;
        }

        try {
            $response = $service->payments($request, $requestOptions);
        } catch (\Adyen\AdyenException $e) {
            $response['error'] = $e->getMessage();
        }

        return $response;
    }

    /**
     * @param $useragent
     * @return null|int
     */
    protected function _chromeVersion($useragent)
    {
        $regex = "/Chrom[^ \/]+\/(\d+)[\.\d]* /";
        $matched = array();
        if (preg_match($regex, $useragent, $matched)) {
            $version = (int)$matched[1];
            return $version;
        }
        return null;
    }

    /**
     * @return \Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getQuote()
    {
        if('adminhtml' === $this->getArea()){
            return $quote = $this->_backendOrder->getQuote();
        }
        return $this->_checkoutSession->getQuote();
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_checkoutSession->getLastRealOrder();
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        return $this->_customerSession->getCustomer();
    }

    /**
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getRefundsByCustomer()
    {
        if (!$this->_orders) {
            $this->_orders = $this->_orderCollectionFactory->create()
                ->addFieldToSelect(
                '*'
                )->addFieldToFilter(
                    'customer_id',
                    $this->getCustomer()->getId()
                )->addFieldToFilter(
                    'status',
                    "creditmemo_refunded"
                );
        }
        return $this->_orders;
    }

    /**
     * @return float|int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getDiscountAmountOrder()
    {
        $orderDiscountAmount = null;
        try {
            $quote = $this->getQuote();

            if($quote->getItemsCount() > 0){
                foreach ($quote->getAllItems() as $item) {
                    if ($item->getDiscountAmount()) {
                        $orderDiscountAmount = abs($item->getDiscountAmount());
                    }
                }
            }
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $orderDiscountAmount;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getArea()
    {
        return $this->_state->getAreaCode();
    }
}
