<?php

namespace Project\Adyen\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Recurring
 * @package Project\Adyen\Model
 * @author Marwen JELLOUL
 */
class Recurring extends AbstractModel
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Project\Adyen\Model\ResourceModel\Recurring::class);
    }

}
