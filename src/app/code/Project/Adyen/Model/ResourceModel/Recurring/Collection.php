<?php

namespace Project\Adyen\Model\ResourceModel\Recurring;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Project\Adyen\Model\ResourceModel\Recurring
 * @author Marwen JELLOUL
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Project\Adyen\Model\Recurring',
            'Project\Adyen\Model\ResourceModel\Recurring'
        );
    }
}
