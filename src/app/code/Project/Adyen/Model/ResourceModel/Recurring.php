<?php

namespace Project\Adyen\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Recurring
 * @package Project\Adyen\Model\ResourceModel
 * @author Marwen JELLOUL
 */
class Recurring extends AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('adyen_recurring', 'entity_id');
    }
}