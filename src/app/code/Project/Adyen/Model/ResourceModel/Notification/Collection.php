<?php
/**
 * Author: BONI Jean-Michel
 */

namespace Project\Adyen\Model\ResourceModel\Notification;

class Collection extends \Adyen\Payment\Model\ResourceModel\Notification\Collection
{

    /**
     * Filter the notifications table to get locked notification in processing status
     * 1 hour but not older than 7 days
     *
     */
    public function adyenRescueFilter()
    {
        $dateStart = new \DateTime();
        $dateStart->modify('-7 day');
        $dateEnd = new \DateTime();
        $dateEnd->modify('-1 hour');
        $dateRange = ['from' => $dateStart, 'to' => $dateEnd, 'datetime' => true];

        $this->addFieldToFilter('done', 0);
        $this->addFieldToFilter('processing', 1);
        $this->addFieldToFilter('created_at', $dateRange);
        $this->addFieldToFilter('error_count', ['lt' => \Adyen\Payment\Model\Notification::MAX_ERROR_COUNT]);

        // Process the notifications in ascending order by creation date and event_code
        $this->getSelect()->order('created_at ASC')->order('event_code ASC');
        return $this;
    }

}
