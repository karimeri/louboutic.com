<?php

namespace Project\Adyen\Model;

use Adyen\Payment\Helper\Vault;
use Adyen\Payment\Model\Ui\AdyenCcConfigProvider;
use Adyen\Payment\Model\Ui\AdyenHppConfigProvider;
use Adyen\Payment\Model\Notification;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\AreaList;
use Magento\Framework\Webapi\Exception;
use Magento\Sales\Api\Data\TransactionInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Project\Sales\Model\Magento\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderRepository;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Vault\Model\PaymentTokenManagement;
use DateInterval;
use DateTime;
use DateTimeZone;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class Cron
 * @package Project\Adyen\Model
 * @author Marwen JELLOUL
 */
class Cron extends \Adyen\Payment\Model\Cron
{
    const ACTION_FLAG_CAPTURE_CANCEL = 'capture_cancel';
    const STATUS_FRAUD_CANCEL = 'fraud_canceled';
    const STATE_AUTORIZATION_ERROR = 'authorization_error';
    const STATE_SENT_TO_LOGISTIC = 'sent_to_logistic';
    const STATE_CANCELED = 'canceled';

    const RECURRING_PROCESSING_MODEL = "UnscheduledCardOnFile";

    const STATE_CREDITMEMO_REFUNDED = 'creditmemo_refunded';
    const MANUAL_REVIEW_REJECT_REASON = 'expire';

    /**
     * @var \Magento\Sales\Model\Order\Payment\Transaction\Builder
     */
    private $transactionBuilder;

    /**
     * @var Recurring
     */
    protected $_recurring;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var \Project\Wms\Model\Api\Connector
     */
    protected $_wmsConnector;

    /**
     * @var \Magento\Sales\Api\OrderPaymentRepositoryInterface
     */
    private $orderPaymentRepository;

    /**
     * @var \Magento\Sales\Api\TransactionRepositoryInterface
     */
    private $transactionRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var PaymentTokenManagement
     */
    private $paymentTokenManagement;

    /**
     * @var PaymentTokenFactoryInterface
     */
    protected $paymentTokenFactory;

    /**
     * @var PaymentTokenRepositoryInterface
     */
    protected $paymentTokenRepository;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;



    /**
     * Application Event Dispatcher
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Cron constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Adyen\Payment\Logger\AdyenLogger $adyenLogger
     * @param \Adyen\Payment\Model\ResourceModel\Notification\CollectionFactory $notificationFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Adyen\Payment\Helper\Data $adyenHelper
     * @param OrderSender $orderSender
     * @param InvoiceSender $invoiceSender
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     * @param \Adyen\Payment\Model\Billing\AgreementFactory $billingAgreementFactory
     * @param \Adyen\Payment\Model\ResourceModel\Billing\Agreement\CollectionFactory $billingAgreementCollectionFactory
     * @param \Adyen\Payment\Model\Api\PaymentRequest $paymentRequest
     * @param \Adyen\Payment\Model\Order\PaymentFactory $adyenOrderPaymentFactory
     * @param \Adyen\Payment\Model\ResourceModel\Order\Payment\CollectionFactory $adyenOrderPaymentCollectionFactory
     * @param \Adyen\Payment\Model\InvoiceFactory $adyenInvoiceFactory
     * @param AreaList $areaList
     * @param \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $orderStatusCollection
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param OrderRepository $orderRepository
     * @param \Adyen\Payment\Model\ResourceModel\Billing\Agreement $agreementResourceModel
     * @param \Magento\Sales\Model\Order\Payment\Transaction\Builder $transactionBuilder
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param \Magento\Framework\Notification\NotifierInterface $notifierPool
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param \Magento\Framework\Notification\NotifierInterface $notifierPool
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Adyen\Payment\Helper\Config $configHelper
     * @param PaymentTokenManagement $paymentTokenManagement
     * @param PaymentTokenFactoryInterface $paymentTokenFactory
     * @param PaymentTokenRepositoryInterface $paymentTokenRepository
     * @param EncryptorInterface $encryptor
     * @param Recurring $recurring
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     * @param \Magento\Sales\Api\OrderPaymentRepositoryInterface $orderPaymentRepository
     * @param \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository
     * @param \Magento\Framework\Model\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Adyen\Payment\Logger\AdyenLogger $adyenLogger,
        \Adyen\Payment\Model\ResourceModel\Notification\CollectionFactory $notificationFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Adyen\Payment\Helper\Data $adyenHelper,
        OrderSender $orderSender,
        InvoiceSender $invoiceSender,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Adyen\Payment\Model\Billing\AgreementFactory $billingAgreementFactory,
        \Adyen\Payment\Model\ResourceModel\Billing\Agreement\CollectionFactory $billingAgreementCollectionFactory,
        \Adyen\Payment\Model\Api\PaymentRequest $paymentRequest,
        \Adyen\Payment\Model\Order\PaymentFactory $adyenOrderPaymentFactory,
        \Adyen\Payment\Model\ResourceModel\Order\Payment\CollectionFactory $adyenOrderPaymentCollectionFactory,
        \Adyen\Payment\Model\InvoiceFactory $adyenInvoiceFactory,
        AreaList $areaList,
        \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $orderStatusCollection,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderRepository $orderRepository,
        \Adyen\Payment\Model\ResourceModel\Billing\Agreement $agreementResourceModel,
        \Magento\Sales\Model\Order\Payment\Transaction\Builder $transactionBuilder,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\Notification\NotifierInterface $notifierPool,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Adyen\Payment\Helper\Config $configHelper,
        PaymentTokenManagement  $paymentTokenManagement,
        PaymentTokenFactoryInterface $paymentTokenFactory,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        EncryptorInterface $encryptor,
        \Project\Adyen\Model\Recurring $recurring,
        \Project\Wms\Model\Api\Connector $wmsConnector,
        \Magento\Sales\Api\OrderPaymentRepositoryInterface $orderPaymentRepository,
        \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository,
        \Magento\Framework\Model\Context $context
    )
    {
        parent::__construct(
            $scopeConfig,
            $adyenLogger,
            $notificationFactory,
            $orderFactory,
            $adyenHelper,
            $orderSender,
            $invoiceSender,
            $transactionFactory,
            $billingAgreementFactory,
            $billingAgreementCollectionFactory,
            $paymentRequest,
            $adyenOrderPaymentFactory,
            $adyenOrderPaymentCollectionFactory,
            $adyenInvoiceFactory,
            $areaList,
            $orderStatusCollection,
            $searchCriteriaBuilder,
            $orderRepository,
            $agreementResourceModel,
            $transactionBuilder,
            $serializer,
            $notifierPool,
            $timezone,
            $configHelper,
            $paymentTokenManagement,
            $paymentTokenFactory,
            $paymentTokenRepository,
            $encryptor
        );

        $this->_recurring = $recurring;
        $this->_wmsConnector = $wmsConnector;
        $this->transactionBuilder = $transactionBuilder;
        $this->orderRepository = $orderRepository;
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->transactionRepository = $transactionRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->paymentTokenRepository = $paymentTokenRepository;
        $this->encryptor = $encryptor;
        $this->_eventManager = $context->getEventDispatcher();
    }

    /**
     * Process the Notification
     */
    protected function _processNotification()
    {

        $this->_adyenLogger->addAdyenNotificationCronjob('Processing the notification');
        $_paymentCode = $this->_paymentMethodCode();

        switch ($this->_eventCode) {
            case Notification::REFUND_FAILED:
                //Trigger admin notice for REFUND_FAILED notifications
                $this->addRefundFailedNotice();
                break;
            case Notification::REFUND:
                $ignoreRefundNotification = $this->_getConfigData(
                    'ignore_refund_notification',
                    'adyen_abstract',
                    $this->_order->getStoreId()
                );
                if ($ignoreRefundNotification != true) {
                    $this->_refundOrder();
                    //refund completed
                    $this->_setRefundAuthorized();
                } else {
                    $this->_adyenLogger->addAdyenNotificationCronjob(
                        'Setting to ignore refund notification is enabled so ignore this notification'
                    );
                }

                // check if success is false
                if (strcmp($this->_success, 'false') == 0) {
                    $this->_order->setState(Order::STATE_REFUND_ERROR);
                    $this->_order->setStatus(Order::STATE_REFUND_ERROR);
                } else {
                    //keep Status in REFUND / RMA WorkFlow
                    $this->_order->setState($this->_order->getState());
                    $this->_order->setStatus($this->_order->getStatus());
                }
                break;
            case Notification::PENDING:
                if ($this->_getConfigData(
                    'send_email_bank_sepa_on_pending',
                    'adyen_abstract',
                    $this->_order->getStoreId()
                )
                ) {
                    // Check if payment is banktransfer or sepa if true then send out order confirmation email
                    $isBankTransfer = $this->_isBankTransfer();
                    if ($isBankTransfer || $this->_paymentMethod == 'sepadirectdebit') {
                        if (!$this->_order->getEmailSent()) {
                            $this->_sendOrderMail();
                        }
                    }
                }
                break;
            case Notification::HANDLED_EXTERNALLY:
            case Notification::AUTHORISATION:
                //If pre order
                $isPreorder = $this->isPreorder($this->_merchantReference);
                $messageCheckpreorder = __(
                    'preorder AUTHORISATION LOG for %1 : Value : %2 and is preorder: %3 and Success : %4 PSPRef : %5',
                    $this->_merchantReference,
                    $this->_value,
                    $isPreorder,
                    $this->_success,
                    $this->_pspReference
                );
                $this->_adyenLogger->addAdyenNotificationCronjob(
                    $messageCheckpreorder
                );
                if ($isPreorder && $this->_value > 1) {
                    $order = $this->_order;
                    // check if success is false
                    if (strcmp($this->_success, 'false') == 0) {

                        //success -> false
                        $retryFullAuto = $order->getRetryFullAuto();
                        if ($retryFullAuto == 0) {
                            $order->setRetryFullAuto(1);
                            $state = self::STATE_AUTORIZATION_ERROR;
                        } elseif ($retryFullAuto == 1) {
                            $order->setRetryFullAuto(2);
                            $state = self::STATE_AUTORIZATION_ERROR;
                        } else {
                            $state = self::STATE_CANCELED;
                        }
                        $message = __(
                            'There is an error in processing the payment.
                            Please try again or contact us. Transaction ID = %1',
                            $this->_pspReference
                        );
                        // process response
                        $this->_processResponse($order, $message, $state);
                        $this->_adyenLogger->addAdyenNotificationCronjob($message);
                    } else {
                        //success -> true
                        // set transaction when payment authorized
                        try {
                            $this->_updatePaymentInfo($order, $this->_pspReference);
                            //sent to logistic
                            $state = self::STATE_SENT_TO_LOGISTIC;
                            $message = __(
                                'Amount %1 has been authorized. Transaction ID = %2',
                                round($order->getGrandTotal(), 2),
                                $this->_pspReference
                            );

                            $websiteId = $order->getStore()->getWebsite()->getWebsiteId();
                            $wmsId = $order->getWmsId();
                            $request = ['state' => 'SENTTOLOGISTIC'];
                            $wmsResponse = $this->_wmsConnector->request(
                                '/api/web_orders/'.$wmsId.'/state',
                                $websiteId,
                                $request,
                                'PUT'
                            );

                            if (!$wmsResponse || isset($wmsResponse->error)) {
                                $message = $wmsResponse->error->message ?? 'See log for more information';
                            }

                            $this->_adyenLogger->addAdyenNotificationCronjob($message);
                            $this->_adyenLogger->addAdyenDebug('Change order status');
                            $order = $this->_processResponse($order, $message, $state);
                            $this->_adyenLogger->addAdyenDebug(sprintf('Order status : %s', $order->getStatus()));

                        }catch (\Exception $exception) {
                            $this->_adyenLogger->addAdyenNotificationCronjob($exception->getMessage());
                        }
                    }

                } else {
                    $this->_authorizePayment();
                }
                break;
            case Notification::MANUAL_REVIEW_REJECT:
                // don't do anything it will send a CANCEL_OR_REFUND notification when this payment is captured
                if(strcmp($this->_reason, self::MANUAL_REVIEW_REJECT_REASON) == 0)
                {
                    // Notification result =>
                    // CANCEL or REFUND
                        $this->_holdCancelOrder(true);
                    //WARNING NB all payment method Authorisation
                    //$this->_refundOrder();
                    //refund completed
                    //$this->_setRefundAuthorized();
                }
                break;
            case Notification::MANUAL_REVIEW_ACCEPT:
                //CL Custom
                $this->_setPaymentAuthorized(false);
                break;
            case Notification::CAPTURE:
                // check if success is true of false
                if (strcmp($this->_success, 'true') == 0) {
                    /*
                     * ignore capture if you are on auto capture
                     * this could be called if manual review is enabled and you have a capture delay
                     */
                    if (!$this->_isAutoCapture()) {
                        $this->_setPaymentAuthorized(false, true, true);

                        /*
                         * Add invoice in the adyen_invoice table
                         */
                        $invoiceCollection = $this->_order->getInvoiceCollection();
                        foreach ($invoiceCollection as $invoice) {
                            if ($invoice->getTransactionId() == $this->_pspReference) {
                                $this->_adyenInvoiceFactory->create()
                                    ->setInvoiceId($invoice->getEntityId())
                                    ->setPspreference($this->_pspReference)
                                    ->setOriginalReference($this->_originalReference)
                                    ->setAcquirerReference($this->_acquirerReference)
                                    ->save();
                                $this->_adyenLogger->addAdyenNotificationCronjob('Created invoice entry in the Adyen table');
                            }
                        }
                    }
                } else {
                    $this->_captureCancel(true);
                }
                break;
            case Notification::OFFER_CLOSED:
                $previousSuccess = $this->_order->getData('adyen_notification_event_code_success');

                // Order is already Authorised
                if (!empty($previousSuccess)) {
                    $this->_adyenLogger->addAdyenNotificationCronjob("Order is already authorised, skipping OFFER_CLOSED");
                    break;
                }

                // Order is already Cancelled
                if ($this->_order->isCanceled()) {
                    $this->_adyenLogger->addAdyenNotificationCronjob("Order is already cancelled, skipping OFFER_CLOSED");
                    break;
                }

                /*
                 * For cards, it can be 'visa', 'maestro',...
                 * For alternatives, it can be 'ideal', 'directEbanking',...
                 */
                $notificationPaymentMethod = $this->_paymentMethod;

                /*
                * For cards, it can be 'VI', 'MI',...
                * For alternatives, it can be 'ideal', 'directEbanking',...
                */
                $orderPaymentMethod = $this->_order->getPayment()->getCcType();

                $isOrderCc = strcmp($this->_paymentMethodCode(),
                        'adyen_cc') == 0 || strcmp($this->_paymentMethodCode(), 'adyen_oneclick') == 0;

                /*
                 * If the order was made with an Alternative payment method,
                 * continue with the cancellation only if the payment method of
                 * the notification matches the payment method of the order.
                 */
                if (!$isOrderCc && strcmp($notificationPaymentMethod, $orderPaymentMethod) !== 0) {
                    $this->_adyenLogger->addAdyenNotificationCronjob("Order is not a credit card,
                    or the payment method in the notification does not match the payment method of the order,
                    skipping OFFER_CLOSED");
                    break;
                }

                if (!$this->_order->canCancel()) {
                    // Move the order from PAYMENT_REVIEW to NEW, so that can be cancelled
                    $this->_order->setState(\Magento\Sales\Model\Order::STATE_NEW);
                }
                $this->_holdCancelOrder(true);
                break;
            case Notification::CAPTURE_FAILED:
                $this->_captureCancel(true);
                break;
            case Notification::CANCELLATION:
            case Notification::CANCELLED:
                $this->_holdCancelOrder(true);
                break;
            case Notification::CANCEL_OR_REFUND:
                if (isset($this->_modificationResult) && $this->_modificationResult != "") {
                    if ($this->_modificationResult == "cancel") {
                        $this->_holdCancelOrder(true);
                    } elseif ($this->_modificationResult == "refund") {
                        $this->_refundOrder();
                        //refund completed
                        $this->_setRefundAuthorized();
                    }
                } else {
                    if ($this->_order->isCanceled() ||
                        $this->_order->getState() === \Magento\Sales\Model\Order::STATE_HOLDED
                    ) {
                        $this->_adyenLogger->addAdyenNotificationCronjob(
                            'Order is already cancelled or holded so do nothing'
                        );
                    } else {
                        if ($this->_order->canCancel() || $this->_order->canHold()) {
                            $this->_adyenLogger->addAdyenNotificationCronjob('try to cancel the order');
                            $this->_holdCancelOrder(true);
                        } else {
                            $this->_adyenLogger->addAdyenNotificationCronjob('try to refund the order');
                            // refund
                            $this->_refundOrder();
                            //refund completed
                            $this->_setRefundAuthorized();
                        }
                    }
                }
                break;

            case Notification::RECURRING_CONTRACT:
                    try {
                        $recurringData = [
                            'increment_id' =>$this->_order->getIncrementId(),
                            'order_id' => $this->_order->getId(),
                            'adyen_token' => $this->_pspReference,
                            'shopper_reference' => $this->_order->getBillingAddress()->getEmail()."_".$this->_order->getQuoteId(),
                            'ProcessingModel' => self::RECURRING_PROCESSING_MODEL
                        ];

                        $this->_recurring->setData($recurringData);
                        $this->_recurring->save();

                        $message = 'Token saved for order '.$this->_order->getIncrementId();
                        $this->_adyenLogger->addAdyenNotificationCronjob($message);
                        $comment = $this->_order->addStatusHistoryComment($message);
                        $this->_order->addRelatedObject($comment);
                    } catch (\Exception $exception) {
                        $message = $exception->getMessage();
                        $this->_adyenLogger->addAdyenNotificationCronjob($message);
                    }
                break;
            default:
                $this->_adyenLogger->addAdyenNotificationCronjob(
                    sprintf('This notification event: %s is not supported so will be ignored', $this->_eventCode)
                );
                break;
        }
    }


    /**
     * @param $ignoreHasInvoice
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _holdCancelOrder($ignoreHasInvoice)
    {
        $orderStatus = $this->_getConfigData(
            'payment_cancelled',
            'adyen_abstract',
            $this->_order->getStoreId()
        );

        // check if order has in invoice only cancel/hold if this is not the case
        if ($ignoreHasInvoice || !$this->_order->hasInvoices()) {
            if ($orderStatus == \Magento\Sales\Model\Order::STATE_HOLDED) {
                // Allow magento to hold order
                $this->_order->setActionFlag(\Magento\Sales\Model\Order::ACTION_FLAG_HOLD, true);

                if ($this->_order->canHold()) {
                    $this->_order->hold();
                } else {
                    $this->_adyenLogger->addAdyenNotificationCronjob('Order can not hold or is already on Hold');
                    return;
                }
            } else {
                // Allow magento to cancel order
                $this->_order->setActionFlag(\Magento\Sales\Model\Order::ACTION_FLAG_CANCEL, true);

                if ($this->_order->canCancel()) {
                    $this->_order->cancel();
                    // Change order status
                    $this->_order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
                    $this->_order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                } elseif ($this->_order->isPaymentReview())
                {
                    if($this->_order->getStatus() == \Magento\Sales\Model\Order::STATUS_FRAUD)
                    {
                        $this->_order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
                        $this->_order->setStatus(self::STATUS_FRAUD_CANCEL);
                        // add event for synchronization with WMS
                        $this->_eventManager->dispatch('order_cancel_after', ['order' => $this->_order]);
                        $message = __('Order canceled');
                        $this->_order->addStatusHistoryComment($message,self::STATUS_FRAUD_CANCEL );
                        $this->_adyenLogger->addAdyenNotificationCronjob('Order canceled after OnHold or Waiting Status');
                    }
                    else
                    {
                        $this->_order->setState(\Magento\Sales\Model\Order::STATE_CANCELED);
                        $this->_order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                        // add event for synchronization with WMS
                        $this->_eventManager->dispatch('order_cancel_after', ['order' => $this->_order]);
                        $message = __('Order canceled');
                        $this->_order->addStatusHistoryComment($message,\Magento\Sales\Model\Order::STATE_CANCELED);
                        $this->_adyenLogger->addAdyenNotificationCronjob('Order canceled after Suspected Fraud Status');
                    }

                }
                else {
                    $this->_adyenLogger->addAdyenNotificationCronjob('Order can not be canceled');
                    return;
                }
            }
        } else {
            $this->_adyenLogger->addAdyenNotificationCronjob('Order has already an invoice so cannot be canceled');
        }
    }


    /**
     * Send order Mail
     *
     * @return void
     */
    private function _sendOrderMail()
    {
        try {
            $this->_orderSender->send($this->_order);
            $this->_adyenLogger->addAdyenNotificationCronjob('Send orderconfirmation email to shopper');
        } catch (\Exception $exception) {
            $this->_adyenLogger->addAdyenNotificationCronjob(
                "Exception in Send Mail in Magento. This is an issue in the the core of Magento" .
                $exception->getMessage()
            );
        }
    }

    /**
     * @param $order
     */
    protected function _setRefundAuthorized()
    {
        $this->_adyenLogger->addAdyenNotificationCronjob(
            'Status update to default status or refund_authorized status if this is set'
        );
        $status = \Project\Sales\Model\Magento\Sales\Order::STATE_CREDITMEMO_REFUNDED;
        $this->_order->addStatusHistoryComment(__('Adyen Refund Successfully completed'), $status);
    }

    /**
     * Set status on authorisation
     *
     * @return void
     */
    private function _setPrePaymentAuthorized()
    {
        $status = $this->_getConfigData(
            'payment_pre_authorized',
            'adyen_abstract',
            $this->_order->getStoreId()
        );

        // only do this if status in configuration is set
        if (!empty($status)) {
            $this->_order->addStatusHistoryComment(__('Payment is authorised waiting for capture'), $status);
            $this->_setState($status);

            $this->_adyenLogger->addAdyenNotificationCronjob(
                'Order status is changed to Pre-authorised status, status is ' . $status
            );
        } else {
            $this->_adyenLogger->addAdyenNotificationCronjob('No pre-authorised status is used so ignore');
        }
    }

    /**
     * @param $ignoreHasInvoice
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _captureCancel($ignoreHasInvoice)
    {
        $orderStatus = $this->_getConfigData(
            'payment_cancelled',
            'adyen_abstract',
            $this->_order->getStoreId()
        );

        if ($ignoreHasInvoice || !$this->_order->hasInvoices()) {
            if ($orderStatus == \Magento\Sales\Model\Order::STATE_HOLDED) {
                // Allow magento to hold order
                $this->_order->setActionFlag(\Magento\Sales\Model\Order::ACTION_FLAG_HOLD, true);

                if ($this->_order->canHold()) {
                    $this->_order->hold();
                } else {
                    $this->_adyenLogger->addAdyenNotificationCronjob('Order can not hold or is already on Hold');
                    return;
                }
            } else {
                // Allow magento to cancel order
                $this->_order->setActionFlag(self::ACTION_FLAG_CAPTURE_CANCEL, true);
                $this->_adyenLogger->addAdyenNotificationCronjob('Capture canceled');

                if ($this->_order->canCancel()) {
                    $this->_order->cancel();
                } else {
                    $this->_adyenLogger->addAdyenNotificationCronjob('Order can not be canceled');
                    return;
                }
            }
        } else {
            $this->_adyenLogger->addAdyenNotificationCronjob('Order has already an invoice so cannot be canceled');
        }
    }

    /**
     * @param bool $manualReviewComment
     * @param bool $createInvoice
     * @throws Exception
     */
    protected function _setPaymentAuthorized($manualReviewComment = true, $createInvoice = false, $capture = false)
    {
        $this->_adyenLogger->addAdyenNotificationCronjob('Set order to authorised');

        //review state
        $review = false;
        // if full amount is captured create invoice
        $currency = $this->_order->getOrderCurrencyCode();
        $amount = $this->_value;
        $orderAmount = (int)$this->_adyenHelper->formatAmount($this->_order->getGrandTotal(), $currency);

        // create invoice for the capture notification if you are on manual capture
        if ($createInvoice == true && $amount == $orderAmount) {
            $this->_adyenLogger->addAdyenNotificationCronjob(
                'amount notification:' . $amount . ' amount order:' . $orderAmount
            );
            $this->_createInvoice();
        }

        $status = $this->_getConfigData(
            'payment_authorized',
            'adyen_abstract',
            $this->_order->getStoreId()
        );

        // virtual order can have different status
        if ($this->_order->getIsVirtual()) {
            $this->_adyenLogger->addAdyenNotificationCronjob('Product is a virtual product');
            $virtualStatus = $this->_getConfigData(
                'payment_authorized_virtual',
                'adyen_abstract',
                $this->_order->getStoreId()
            );
            if ($virtualStatus != "") {
                $status = $virtualStatus;
            }
        }

        // check for boleto if payment is totally paid
        if ($this->_paymentMethodCode() == "adyen_boleto") {
            // check if paid amount is the same as orginal amount
            $orginalAmount = $this->_boletoOriginalAmount;
            $paidAmount = $this->_boletoPaidAmount;

            if ($orginalAmount != $paidAmount) {
                // not the full amount is paid. Check if it is underpaid or overpaid
                // strip the  BRL of the string
                $orginalAmount = str_replace("BRL", "", $orginalAmount);
                $orginalAmount = floatval(trim($orginalAmount));

                $paidAmount = str_replace("BRL", "", $paidAmount);
                $paidAmount = floatval(trim($paidAmount));

                if ($paidAmount > $orginalAmount) {
                    $overpaidStatus = $this->_getConfigData(
                        'order_overpaid_status',
                        'adyen_boleto',
                        $this->_order->getStoreId()
                    );
                    // check if there is selected a status if not fall back to the default
                    $status = (!empty($overpaidStatus)) ? $overpaidStatus : $status;
                } else {
                    $underpaidStatus = $this->_getConfigData(
                        'order_underpaid_status',
                        'adyen_boleto',
                        $this->_order->getStoreId()
                    );
                    // check if there is selected a status if not fall back to the default
                    $status = (!empty($underpaidStatus)) ? $underpaidStatus : $status;
                }
            }
        }

        $comment = __(
            'The customer successfully returned from Adyen. Reason Code: %1 Request was processed successfully. <br /> Amount %2 has been authorized. Transaction ID %3',
            $this->_reason,
            $amount,
            $this->_pspReference
        );

        // if manual review is true use the manual review status if this is set
        if ($manualReviewComment == true && $this->_fraudManualReview) {
            // check if different status is selected
            $fraudManualReviewStatus = $this->_getFraudManualReviewStatus();
            if ($fraudManualReviewStatus != "") {
                $status = $fraudManualReviewStatus;
                $review = true;
                $comment = __("Adyen Payment is in Manual Review check the Adyen platform");
            }
        }
        if ($capture) {
            $status = $this->_order->getStatus();
        } else {
            $status = (!empty($status)) ? $status : $this->_order->getStatus();
        }
        $this->_order->addStatusHistoryComment(__($comment), $status);
        $this->_setState($status, $review);

        $this->_adyenLogger->addAdyenNotificationCronjob(
            'Order status is changed to authorised status, status is ' . $status
        );
    }


    /**
     * Set State from Status
     */

    protected function _setState($status, $review = false)
    {
        $statusObject = $this->_orderStatusCollection->create()
            ->addFieldToFilter('main_table.status', $status)
            ->addFieldToFilter('state_table.is_default', true)
            ->joinStates()
            ->getFirstItem();

        //TO manage state new to payment_review state
        if($review)
        {
            $this->_order->setState($this->_getFraudManualReviewStatus());
            $this->_adyenLogger->addAdyenNotificationCronjob('State is changed to  ' . $this->_getFraudManualReviewStatus());
        }
        else{
            $this->_order->setState($statusObject->getState());
            $this->_adyenLogger->addAdyenNotificationCronjob('State is changed to  ' . $statusObject->getState());
        }
    }

    /**
     * @return string
     * return custom CL status payment_review
     */

    protected function _getFraudManualReviewStatus()
    {
        return \Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param $message
     * @param $state
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _processResponse($order, $message, $state)
    {
        // set order status
        $order->setState($state);
        $order->setStatus($state);
        $order->addCommentToStatusHistory($message, $state, false);

        return $this->orderRepository->save($order);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param $pspReference
     */
    protected function _updatePaymentInfo($order, $pspReference)
    {
        // Prepare transaction
        try {
            $paymentObj = $order->getPayment();

            // pre auth psp reference
            $previousReference = $paymentObj->getLastTransId();

            // set pspReference as transactionId
            $paymentObj->setCcTransId($pspReference);
            $paymentObj->setLastTransId($pspReference);
            //Add this for capture issue
            $paymentObj->setAdyenPspReference($pspReference);
            $paymentObj->setAdditionalInformation('pspReference', $pspReference);

            // set transaction
            $paymentObj->setTransactionId($pspReference);

            // save payment
            $this->orderPaymentRepository->save($paymentObj);

            // retrieve pre auth transaction
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('order_id', $order->getId())
                ->addFilter('txn_id', $previousReference)
                ->addFilter('payment_id', $paymentObj->getEntityId())
                ->addFilter('txn_type', 'authorization')
                ->create();
            /** @var TransactionInterface $transaction */
            $transaction = $this->transactionRepository->getList($searchCriteria)->getFirstItem();

            // update transaction with full auth psp reference
            $transaction->setTxnId($pspReference);

            // save transaction
            $this->transactionRepository->save($transaction);
        } catch (\Exception $exception) {
            $this->_adyenLogger->addAdyenNotificationCronjob($exception->getMessage());
        }
    }

    /**
     * @param $orderIncrementId
     * @return bool
     */
    protected function isPreorder($orderIncrementId)
    {
        $collection = $this->_recurring->getCollection()
            ->addFieldToFilter('increment_id', $orderIncrementId);

        if($collection->getSize() > 0){
            return true;
        }

        return false;
    }

    /**
     * @desc order comments or history
     * @param type $order
     */
    protected function _addStatusHistoryComment()
    {
        $successResult = (strcmp($this->_success, 'true') == 0 ||
            strcmp($this->_success, '1') == 0) ? 'true' : 'false';
        $success = (!empty($this->_reason)) ? "$successResult <br />reason:$this->_reason" : $successResult;

        if ($this->_eventCode == Notification::REFUND || $this->_eventCode == Notification::CAPTURE) {
            $currency = $this->_order->getOrderCurrencyCode();

            // check if it is a full or partial refund
            $amount = $this->_value;
            $orderAmount = (int)$this->_adyenHelper->formatAmount($this->_order->getGrandTotal(), $currency);

            $this->_adyenLogger->addAdyenNotificationCronjob(
                'amount notification:' . $amount . ' amount order:' . $orderAmount
            );

            if ($amount == $orderAmount) {
                $this->_order->setData(
                    'adyen_notification_event_code',
                    $this->_eventCode . " : " . strtoupper($successResult)
                );
            } else {
                $this->_order->setData(
                    'adyen_notification_event_code',
                    "(PARTIAL) " .
                    $this->_eventCode . " : " . strtoupper($successResult)
                );
            }
        } else {
            $this->_order->setData(
                'adyen_notification_event_code',
                $this->_eventCode . " : " . strtoupper($successResult)
            );
        }

        // if payment method is klarna, ratepay or openinvoice/afterpay show the reservartion number
        if ($this->_adyenHelper->isPaymentMethodOpenInvoiceMethod($this->_paymentMethod) && !empty($this->_klarnaReservationNumber)) {
            $klarnaReservationNumberText = "<br /> reservationNumber: " . $this->_klarnaReservationNumber;
        } else {
            $klarnaReservationNumberText = "";
        }

        if ($this->_boletoPaidAmount != null && $this->_boletoPaidAmount != "") {
            $boletoPaidAmountText = "<br /> Paid amount: " . $this->_boletoPaidAmount;
        } else {
            $boletoPaidAmountText = "";
        }

        $type = 'Adyen HTTP Notification(s):';
        $comment = __(
            '%1 <br /> eventCode: %2 <br /> pspReference: %3 <br /> paymentMethod: %4 <br />' .
            ' success: %5 %6 %7',
            $type,
            $this->_eventCode,
            $this->_pspReference,
            $this->_paymentMethod,
            $success,
            $klarnaReservationNumberText,
            $boletoPaidAmountText
        );

        // If notification is pending status and pending status is set add the status change to the comment history
        if ($this->_eventCode == Notification::PENDING) {
            $pendingStatus = $this->_getConfigData(
                'pending_status',
                'adyen_abstract',
                $this->_order->getStoreId()
            );
            if ($pendingStatus != "") {
                $this->_order->addStatusHistoryComment($comment, $pendingStatus);
                $this->_adyenLogger->addAdyenNotificationCronjob(
                    'Created comment history for this notification with status change to: ' . $pendingStatus
                );
                return;
            }
        }

        // if manual review is accepted and a status is selected. Change the status through this comment history item
        if ($this->_eventCode == Notification::MANUAL_REVIEW_ACCEPT
            && $this->_getFraudManualReviewAcceptStatus() != ""
        ) {
            $manualReviewAcceptStatus = $this->_getFraudManualReviewAcceptStatus();
            $this->_order->addStatusHistoryComment($comment, $manualReviewAcceptStatus);
            $this->_adyenLogger->addAdyenNotificationCronjob('Created comment history for this notification with status change to: ' . $manualReviewAcceptStatus);
            if (!$this->_order->getEmailSent()) {
                $this->_sendOrderMail();
            }
            return;
        }

        if ($this->_eventCode == Notification::REFUND) {
            if(strcmp($this->_success, 'true') == 0 || strcmp($this->_success, '1') == 0){
                $this->_order->addStatusHistoryComment($comment, self::STATE_CREDITMEMO_REFUNDED);
            }
        }

        if ($this->_eventCode == Notification::RECURRING_CONTRACT) {
            $comment = $comment . __('<br /> Authorized amount of 1.00 %1. Transaction ID: "%2".', $this->_order->getOrderCurrencyCode(), $this->_pspReference);
        }

        $this->_order->addStatusHistoryComment($comment);
        $this->_adyenLogger->addAdyenNotificationCronjob('Created comment history for this notification');
    }
}
