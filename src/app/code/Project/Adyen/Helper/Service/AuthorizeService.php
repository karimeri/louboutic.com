<?php

namespace Project\Adyen\Helper\Service;

use Adyen\Payment\Model\ApplicationInfo;
use Adyen\Payment\Helper\Data as AdyenData;
use Adyen\Payment\Logger\AdyenLogger;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Project\Adyen\Model\Recurring;
use Adyen\Payment\Model\Order\PaymentFactory;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order\Payment\Transaction\Builder;

/**
 * Class AuthorizeService
 * @package Project\Adyen\Helper\Service
 * @author Marwen JELLOUL
 */
class AuthorizeService
{
    const SHOPPER_INTERACTION = "ContAuth";
    const RECURRING_PROCESSING_MODEL = "UnscheduledCardOnFile";
    const RESULT_CODE_AUTHORISED = "Authorised";
    const RESULT_CODE_REFUSED = "Refused";
    const RESULT_CODE_ERROR = "Error";
    const RESULT_CODE_CANCEL = "Cancelled";
    const ERROR_RESULT_CODE = array(
        self::RESULT_CODE_REFUSED,
        self::RESULT_CODE_ERROR,
        self::RESULT_CODE_CANCEL
    );

    /**
     * @var \Adyen\Payment\Helper\Data
     */
    private $adyenHelper;

    /**
     * @var ApplicationInfo
     */
    private $applicationInfo;

    /**
     * @var AdyenLogger
     */
    protected $_adyenLogger;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var Recurring
     */
    protected $_recurring;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var PaymentFactory
     */
    protected $_adyenOrderPaymentFactory;

    /**
     * @var Builder
     */
    private $transactionBuilder;

    /**
     * AuthorizeService constructor.
     * @param AdyenData $adyenHelper
     * @param ApplicationInfo $applicationInfo
     * @param AdyenLogger $adyenLogger
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param Recurring $recurring
     * @param OrderRepository $orderRepository
     * @param PaymentFactory $adyenOrderPaymentFactory
     * @param Builder $transactionBuilder
     */
    public function __construct(
        AdyenData $adyenHelper,
        ApplicationInfo $applicationInfo,
        AdyenLogger $adyenLogger,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        Recurring $recurring,
        OrderRepository $orderRepository,
        \Adyen\Payment\Model\Order\PaymentFactory $adyenOrderPaymentFactory,
        Builder $transactionBuilder
    ) {
        $this->adyenHelper = $adyenHelper;
        $this->applicationInfo = $applicationInfo;
        $this->_adyenLogger = $adyenLogger;
        $this->storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_recurring = $recurring;
        $this->orderRepository = $orderRepository;
        $this->_adyenOrderPaymentFactory = $adyenOrderPaymentFactory;
        $this->transactionBuilder = $transactionBuilder;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return mixed
     * @throws \Adyen\AdyenException
     */
    public function fullAuthorize($order, $retry = false)
    {
        $client = $this->adyenHelper->initializeAdyenClient($order->getStoreId());

        $service = new \Adyen\Service\Checkout($client);

        $requestOptions = [];
        $request = [];
        $commentStatus ="";

        $request = $this->applicationInfo->addMerchantApplicationIntoRequest($request);

        /** REQUEST DATA */
        $orderIncrementId = $order->getIncrementId();
        $recurringDetailReference = false;
        $shopperReference = false;

        if($this->getRecurringData($orderIncrementId)){
            /** @var \Project\Adyen\Model\Recurring $recurring */
            $recurring = $this->getRecurringData($orderIncrementId);
            $recurringDetailReference = $recurring->getAdyenToken();
            $shopperReference = $recurring->getShopperReference();
        }

        $orderCurrencyCode = $order->getOrderCurrencyCode();
        $amount = $order->getTotalDue();
        $type = ($order->getPayment()->getMethod() == 'adyen_hpp')?'paypal':'scheme';

        $requestData = [
            'amount' => [
                'currency' => $orderCurrencyCode,
                'value' => $this->formatAmount($amount,$orderCurrencyCode),
            ],
            'reference' => $order->getIncrementId(), //Order Increment ID
            'paymentMethod' => [
                'type' => $type,
                'recurringDetailReference' => $recurringDetailReference ?? null //Adyen Token
            ],
            'shopperReference' => $shopperReference ?? null,
            'returnUrl' => $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK) . 'adyen/process/result',
            'merchantAccount' => $this->getAdyenAbstractConfigData("merchant_account", $order->getStoreId()),
            'shopperInteraction' => self::SHOPPER_INTERACTION,
            'recurringProcessingModel' => self::RECURRING_PROCESSING_MODEL,
            'additionalData' => [
                'riskdata.riskProfileReference' => $this->getAdyenAbstractConfigData("case_management_contauth", $order->getStoreId())
            ]
        ];

        $request = $request + $requestData;

        try {
            $result = $service->payments($request, $requestOptions);
            /** Response Management */
            if (empty($result)) {
                $commentStatus = __("No result for full authorization. See log for more information");
                throw new \Exception('No result for full authorization. See log for more information');
            }

            if(isset($result['resultCode'])) {
                if ($result['resultCode'] == self::RESULT_CODE_AUTHORISED) {
                    if ($retry) {
                        $commentStatus = __("Retry Payment Full Authorize request sent to Adyen. Waiting for Adyen Notification on %1", $result['resultCode']);
                    } else {
                        $commentStatus = __("Payment Full Authorize request sent to Adyen. Waiting for Adyen Notification on %1", $result['resultCode']);
                    }
                } elseif (in_array($result['resultCode'], self::ERROR_RESULT_CODE)) {
                    $commentStatus = __("An error during the payment Full Authorize request sent to Adyen. the reason is :  %1", $result['refusalReason']);
                }
                $order->addStatusHistoryComment($commentStatus)->setIsCustomerNotified(false);
                $order->setSendFullAuth(1);
                $this->orderRepository->save($order);
            }

        } catch (\Adyen\AdyenException $e) {
            $result['error'] = $e->getMessage();
        }

        return true;
    }

    /**
     * @param $orderIncrementId
     * @return bool|\Project\Adyen\Model\Recurring
     */
    protected function getRecurringData($orderIncrementId)
    {
        $collection = $this->_recurring->getCollection()
            ->addFieldToFilter('increment_id', $orderIncrementId);

        $recurring = false;
        if($collection->getSize() > 0){
            /** @var \Project\Adyen\Model\Recurring $recurring */
            $recurring = $collection->getFirstItem();
        }

        return $recurring;
    }

    /**
     * Return the formatted amount. Adyen accepts the currency in multiple formats.
     * @param $amount
     * @param $currency
     * @return int
     */
    public function formatAmount($amount, $currency)
    {
        return (int)number_format($amount, $this->adyenHelper->decimalNumbers($currency), '', '');
    }

    /**
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getAdyenAbstractConfigData($field, $storeId = null)
    {
        return $this->getConfigData($field, 'adyen_abstract', $storeId);
    }

    /**
     * @param $field
     * @param $paymentMethodCode
     * @param $storeId
     * @param bool|false $flag
     * @return bool|mixed
     */
    public function getConfigData($field, $paymentMethodCode, $storeId, $flag = false)
    {
        $path = 'payment/' . $paymentMethodCode . '/' . $field;

        if (!$flag) {
            return $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        } else {
            return $this->_scopeConfig->isSetFlag($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        }
    }
}
