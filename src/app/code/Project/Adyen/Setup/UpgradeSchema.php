<?php

namespace Project\Adyen\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 * @package Project\Adyen\Setup
 * @author Marwen JELLOUL
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Adyen Recurring Table Name
     */
    const ADYEN_RECURRING_TABLE = 'adyen_recurring';

    /**
     * Adyen Recurring Table Fields
     */
    const ADYEN_RECURRING_ID_FIELD = 'entity_id';
    const ADYEN_RECURRING_ORDER_INCREMENT_ID_FIELD = 'increment_id';
    const ADYEN_RECURRING_ORDER_ID_FIELD = 'order_id';
    const ADYEN_RECURRING_ADYEN_TOKEN_FIELD = 'adyen_token';
    const ADYEN_RECURRING_SHOPPER_REFERENCE_FIELD = 'shopper_reference';
    const ADYEN_RECURRING_PROCESSING_MODEL_FIELD = 'processing_model';


    /**
     * Adyen Recurring Processing Model Field default value
     */
    const ADYEN_RECURRING_PROCESSING_MODEL_DEFAULT_VALUE = 'UnscheduledCardOnFile';

    /**
     * Custom order column : retry_full_auto
     */
    const ORDER_RETRY_FIELD = 'retry_full_auto';

    /**
     * Custom order column : send_full_auto
     */
    const ORDER_SENT_FULL_AUTH_FIELD = 'send_full_auth';

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable(self::ADYEN_RECURRING_TABLE))
                ->addColumn(
                    self::ADYEN_RECURRING_ID_FIELD,
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Entity ID'
                )
                ->addColumn(
                    self::ADYEN_RECURRING_ORDER_INCREMENT_ID_FIELD,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => ''],
                    'Increment ID'
                )
                ->addColumn(
                    self::ADYEN_RECURRING_ORDER_ID_FIELD,
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Order ID'
                )
                ->addColumn(
                    self::ADYEN_RECURRING_ADYEN_TOKEN_FIELD,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => ''],
                    'Adyen Token'
                )
                ->addColumn(
                    self::ADYEN_RECURRING_SHOPPER_REFERENCE_FIELD,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => ''],
                    'Shopper Reference'
                )
                ->addColumn(
                    self::ADYEN_RECURRING_PROCESSING_MODEL_FIELD,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => self::ADYEN_RECURRING_PROCESSING_MODEL_DEFAULT_VALUE],
                    'Processing Model'
                )->setComment("Adyen Recurring Table");
            $setup->getConnection()->createTable($table);
        }

        //Add a new column retry_full_auto to sales_order table
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                self::ORDER_RETRY_FIELD,
                [
                    'type' => Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Custom order column : Retry Full Authorization'
                ]
            );
        }


        //Add a new column retry_full_auto to sales_order table
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                self::ORDER_SENT_FULL_AUTH_FIELD,
                [
                    'type' => Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Custom order column : Send Full Auth'
                ]
            );
        }

        $setup->endSetup();
    }
}
