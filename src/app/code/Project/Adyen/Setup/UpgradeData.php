<?php
/**
 * @author BONI Jean-Michel
 * @copyright CL Team
 */

namespace Project\Adyen\Setup;

use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Model\Order\Status;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

/**
 * Class UpgradeData
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Capture cancel Order-State code
     */
    const ORDER_STATE_CAPTURE_CODE = 'canceled';

    /**
     * Capture cancel Order-Status code
     */
    const ORDER_STATUS_CAPTURE_CODE = 'fraud_canceled';

    /**
     * Capture cancel Order-Status label
     */
    const ORDER_STATUS_CAPTURE_LABEL = 'Fraud Canceled';

    /**
     * 'Waiting for Adyen' Order-Status code
     */
    const ORDER_STATUS_WAITING_ADYEN_CODE = 'waiting_adyen';

    /**
     * 'Waiting for Adyen' Order-Status label
     */
    const ORDER_STATUS_WAITING_ADYEN_LABEL = 'Waiting for Adyen';

    /**
     * @var StatusFactory
     */
    protected $statusFactory;

    /**
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;

    /**
     * InstallData constructor.
     * @param StatusFactory $statusFactory
     * @param StatusResourceFactory $statusResourceFactory
     */
    public function __construct(
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory
    ){
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            /** @var StatusResource $statusResource */
            $statusResource = $this->statusResourceFactory->create();
            /** @var Status $status */
            $status = $this->statusFactory->create();
            $status->setData([
                'status' => self::ORDER_STATUS_CAPTURE_CODE,
                'label' => self::ORDER_STATUS_CAPTURE_LABEL,
            ]);

            try {
                $statusResource->save($status);
            } catch (AlreadyExistsException $exception) {
                return;
            }

            $status->assignState(self::ORDER_STATE_CAPTURE_CODE, true, true);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            /** @var StatusResource $statusResource */
            $statusResource = $this->statusResourceFactory->create();
            /** @var Status $status */
            $status = $this->statusFactory->create();
            $status->setData([
                'status' => self::ORDER_STATUS_WAITING_ADYEN_CODE,
                'label' => self::ORDER_STATUS_WAITING_ADYEN_LABEL,
            ]);

            try {
                $statusResource->save($status);
            } catch (AlreadyExistsException $exception) {
                return;
            }

            $status->assignState(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW, true, true);
        }

        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            /** @var Status $status */
            $status = $this->statusFactory->create();
            $status->setData([
                'status' => self::ORDER_STATUS_CAPTURE_CODE,
                'label' => self::ORDER_STATUS_CAPTURE_LABEL,
            ]);
            $status->assignState(self::ORDER_STATE_CAPTURE_CODE, false, true);
        }
    }
}
