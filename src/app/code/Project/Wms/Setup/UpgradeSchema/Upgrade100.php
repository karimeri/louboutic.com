<?php

namespace Project\Wms\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Project\Wms\Api\Data\WmsErrorInterface;
use Project\Wms\Model\ResourceModel\WmsError as WmsErrorResource;
use Synolia\Standard\Setup\AbstractSetup;

/**
 * Class Upgrade100
 * @package Project\Wms\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade100 extends AbstractSetup
{
    const TABLE_ORDER = 'sales_order';

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->createWmsErrorTable($setup);

        $columnToAdd = [
            [
                'name' => 'wms_id',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'Order id in WMS'
                ]
            ]
        ];

        foreach ($columnToAdd as $column) {
            $this->addColumn(
                self::TABLE_ORDER,
                $column['name'],
                $column['definition']
            );
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function createWmsErrorTable(SchemaSetupInterface $setup)
    {
        if (!$setup->getConnection()->isTableExists($setup->getTable(WmsErrorResource::TABLE_NAME))) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable(WmsErrorResource::TABLE_NAME)
            )->addColumn(
                WmsErrorInterface::ERROR_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Error Id'
            )->addColumn(
                WmsErrorInterface::ENTITY_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Entity Id'
            )->addColumn(
                WmsErrorInterface::WMS_ID,
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => true,
                    'unsigned' => true
                ],
                'Wms Id'
            )->addColumn(
                WmsErrorInterface::NB_ERROR_WMS,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true],
                'Nb error Wms'
            )->addColumn(
                WmsErrorInterface::LAST_ERROR_MSG_WMS,
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Last error message in Wms'
            )->addColumn(
                WmsErrorInterface::ENTITY_TYPE,
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Entity Type'
            )->addColumn(
                WmsErrorInterface::API_METHOD,
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Api method'
            )->addColumn(
                WmsErrorInterface::MAIL_SENT,
                Table::TYPE_BOOLEAN,
                null,
                ['nullable' => true],
                'Mail sent'
            );

            $setup->getConnection()->createTable($table);
        }
    }
}
