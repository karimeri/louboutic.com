<?php

namespace Project\Wms\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade1
 * @package Project\Wms\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnToAdd = [
            [
                'name' => 'wms_id',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'RMA id in WMS'
                ]
            ],
            [
                'name' => 'delivery_date',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_DATE,
                    'comment' => 'Date of delivery'
                ]
            ]
        ];

        foreach ($columnToAdd as $column) {
            $this->abstractSetup->addColumn(
                self::TABLE_RMA,
                $column['name'],
                $column['definition']
            );
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add wms_id column to rma table';
    }
}
