<?php

namespace Project\Wms\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade3
 * @package Project\Wms\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';
    const TABLE_RMA_GRID = 'magento_rma_grid';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->abstractSetup->addColumn(
            self::TABLE_RMA,
            'received_date',
            [
                'nullable' => true,
                'type' => Table::TYPE_DATE,
                'comment' => 'Date of received'
            ]
        );

        $this->abstractSetup->addColumn(
            self::TABLE_RMA_GRID,
            'received_date',
            [
                'nullable' => true,
                'type' => Table::TYPE_DATE,
                'comment' => 'Date of received'
            ]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add received_date column to rma table';
    }
}
