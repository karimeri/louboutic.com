<?php

namespace Project\Wms\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade2
 * @package Project\Wms\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade2 implements UpgradeDataSetupInterface
{
    const TABLE_RMA = 'magento_rma';
    const TABLE_RMA_GRID = 'magento_rma_grid';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->abstractSetup->addColumn(
            self::TABLE_RMA,
            'has_images',
            [
                'nullable' => false,
                'default' => 0,
                'type' => Table::TYPE_BOOLEAN,
                'comment' => 'Has images'
            ]
        );

        $this->abstractSetup->addColumn(
            self::TABLE_RMA_GRID,
            'has_images',
            [
                'nullable' => false,
                'default' => 0,
                'type' => Table::TYPE_BOOLEAN,
                'comment' => 'Has images'
            ]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add has_images column to rma table';
    }
}
