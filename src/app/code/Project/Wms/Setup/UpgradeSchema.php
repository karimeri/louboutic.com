<?php

namespace Project\Wms\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class UpgradeSchema
 * @package Project\Wms\Setup
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const VERSIONS = [
        '1.0.0' => '100'
    ];

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var SchemaSetupInterface
     */
    protected $setup;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * UpgradeData constructor.
     *
     * @param ConsoleOutput $output
     *
     * @throws \RuntimeException
     */
    public function __construct(
        ConsoleOutput $output,
        ObjectManagerInterface $objectManager
    ) {
        $this->output        = $output;
        $this->objectManager = $objectManager;
    }

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $setup->startSetup();

        $this->output->writeln(''); // new line in console

        foreach (self::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Project Wms setup : $version");

                $currentSetup = $this->getObjectManager()
                    ->create('Project\Wms\Setup\UpgradeSchema\Upgrade'.$fileData);
                $currentSetup->upgrade($setup, $context);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return SchemaSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
