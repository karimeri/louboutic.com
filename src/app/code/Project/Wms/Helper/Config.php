<?php

namespace Project\Wms\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 *
 * @package Project\Wms\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_ESTABLISHMENT_CODE = 'louboutin_wms/general/establishment_code';
    const XML_PATH_STOCK_CODE = 'louboutin_wms/general/stock_code';
    const XML_PATH_STOCK_CODE_DEFECTIVE = 'louboutin_wms/general/stock_code_defective';
    const XML_PATH_BASE_URL = 'louboutin_wms/api/url';
    const XML_PATH_USERNAME = 'louboutin_wms/api/username';
    const XML_PATH_PASSWORD = 'louboutin_wms/api/password';
    const XML_PATH_WMS_SFTP_HOST = 'louboutin_wms/wms_sftp/host';
    const XML_PATH_WMS_SFTP_USERNAME = 'louboutin_wms/wms_sftp/username';
    const XML_PATH_WMS_SFTP_PASSWORD = 'louboutin_wms/wms_sftp/password';
    const XML_PATH_WMS_SFTP_PATH = 'louboutin_wms/wms_sftp/path';

    const XML_PATH_IS_MASTER = 'louboutin_wms/retailer_import/isMaster';

    /**
     * @param string      $configName
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_WEBSITE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getEstablishmentCode($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_ESTABLISHMENT_CODE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getStockCode($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_STOCK_CODE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getStockCodeDefective($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_STOCK_CODE_DEFECTIVE, $scopeCode);
    }

    /**
     * @param int|null $scopeCode
     * @return null|string
     */
    public function getBaseUrl($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_BASE_URL, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getUsername($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_USERNAME, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getPassword($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_PASSWORD, $scopeCode);
    }

    /**
     * @return bool|int
     */
    public function isMaster()
    {
        return $this->getConfig(self::XML_PATH_IS_MASTER);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getWmsSftpHost($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_WMS_SFTP_HOST, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getWmsSftpUsername($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_WMS_SFTP_USERNAME, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getWmsSftpPassword($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_WMS_SFTP_PASSWORD, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getWmsSftpPath($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_WMS_SFTP_PATH, $scopeCode);
    }
}
