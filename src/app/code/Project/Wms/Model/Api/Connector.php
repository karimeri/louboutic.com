<?php

namespace Project\Wms\Model\Api;

use GuzzleHttp\Client;
use Magento\Store\Model\StoreManager;
use Project\Wms\Helper\Config;
use Project\Wms\Model\WmsErrorRepository;
use Zend\Http\Request;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Logger\Model\Logger;
use Magento\Framework\Filesystem\DirectoryList;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Connector
 * @package Project\Wms\Model\Api
 * @author  Synolia <contact@synolia.com>
 */
class Connector
{
    const PATH_TOKEN = '/api/login_check';

    /**
     * @var string
     */
    protected $token;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Connector constructor.
     * @param \Project\Wms\Helper\Config $config
     * @param \GuzzleHttp\Client $client
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        Config $config,
        Client $client,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        StoreManager $storeManager
    ) {
        $this->config = $config;
        $this->client = $client;

        $logPath = $directoryList->getPath('log');
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath.DIRECTORY_SEPARATOR.WmsErrorRepository::WMS_ERROR_LOG_FILENAME]
        );

        $this->storeManager = $storeManager;
    }

    /**
     * @param $websiteId
     * @return string|bool
     */
    protected function getToken($websiteId)
    {
        try {
            $response = $this->client->request(
                'POST',
                $this->getUrl(self::PATH_TOKEN, $websiteId),
                [
                    'auth' => [$this->config->getUsername($websiteId), $this->config->getPassword($websiteId)],
                    'json' => $this->getBodyToken($websiteId),
                    'headers' => [
                        'content-type' => 'application/json'
                    ]
                ]
            );
            $content = \GuzzleHttp\json_decode($response->getBody()->getContents());

            return $content->token;
        } catch (RequestException $exception) {
            $this->processException($exception);
            return false;
        }
    }

    /**
     * @param $path
     * @param $website
     * @param $data
     * @param string $method
     * @return mixed
     * @throws \Exception
     */
    public function request($path, $website, $data, $method = Request::METHOD_POST)
    {
        try {
            $websiteId = null;
            if (!is_null($website)) {
                $websiteEntity = $this->storeManager->getWebsite($website);
                $websiteId = $websiteEntity->getId();
            }

            $response = $this->client->request(
                $method,
                $this->getUrl($path, $websiteId),
                [
                    'json' => $data,
                    'headers' => [
                        'Authorization' => 'Bearer '.$this->getToken($websiteId),
                        'content-type' => 'application/json',
                        'accept' => 'application/json'
                    ],
                ]
            );

            return \GuzzleHttp\json_decode($response->getBody()->getContents());
        } catch (RequestException $exception) {
            $message = $this->processException($exception);
            throw new \Exception($message);
        }
    }

    /**
     * @param $path
     * @param $website
     * @param $data
     * @return bool|mixed
     */
    public function getRequest($path, $website, $data)
    {
        try {
            $websiteId = null;
            if (!is_null($website)) {
                $websiteEntity = $this->storeManager->getWebsite($website);
                $websiteId = $websiteEntity->getId();
            }
            $response = $this->client->request(
                Request::METHOD_GET,
                $this->getUrl($path, $websiteId).'?'.\GuzzleHttp\Psr7\build_query($data),
                [
                    'headers' => [
                        'Authorization' => 'Bearer '.$this->getToken($websiteId),
                        'accept' => 'application/json',
                    ]
                ]
            );

            return \GuzzleHttp\json_decode($response->getBody()->getContents());
        } catch (RequestException $exception) {
            $this->processException($exception);
            return false;
        }
    }

    /**
     * @param RequestException $exception
     * @return string
     */
    protected function processException(RequestException $exception)
    {
        if ($exception->hasResponse()) {
            $body = \GuzzleHttp\json_decode($exception->getResponse()->getBody()->getContents());
            $bodyMsg = isset($body->detail)? $body->detail : (isset($body->error->message)? $body->error->message : '');
            $message = "CODE : {$exception->getCode()}, message : $bodyMsg -> ".$exception->getResponse()->getBody()->getContents();
            $this->logger->addError($message);
        } else {
            $message = "CODE : {$exception->getCode()}, message : {$exception->getMessage()} ";
            $this->logger->addError($message);
        }

        return $message;
    }

    /**
     * @param $path
     * @return string
     */
    protected function getUrl($path, $websiteId)
    {
        return $this->config->getBaseUrl($websiteId).$path;
    }

    /**
     * @param $websiteId
     * @return array
     */
    protected function getBodyToken($websiteId)
    {
        return [
            'username' => $this->config->getUsername($websiteId),
            'password' => $this->config->getPassword($websiteId)
        ];
    }
}
