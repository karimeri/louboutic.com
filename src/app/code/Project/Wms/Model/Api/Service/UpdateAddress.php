<?php

namespace Project\Wms\Model\Api\Service;

use Magento\Sales\Model\Order\Address;
use Project\Wms\Model\Api\Connector;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollectionFactory;

/**
 * Class UpdateAddress
 * @package Project\Wms\Model\Api\Service
 * @author  Synolia <contact@synolia.com>
 */
class UpdateAddress
{
    const BASE_API_PATH = '/api/web_orders/';
    
    /**
     * @var RegionCollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @var Connector
     */
    protected $wmsConnector;

    /**
     * UpdateAddress constructor.
     * @param RegionCollectionFactory $regionCollectionFactory
     * @param Connector $wmsConnector
     */
    public function __construct(
        Connector $wmsConnector,
        RegionCollectionFactory $regionCollectionFactory
    ) {
        $this->wmsConnector = $wmsConnector;
        $this->regionCollectionFactory = $regionCollectionFactory;
    }

    /**
     * @param array $addresses
     * @param $websiteId
     * @param $wmsId
     * @return mixed
     * @throws \Exception
     */
    public function updateAddress(array $addresses, $websiteId, $wmsId)
    {
        $request  = $this->getRequest($addresses);

        $fullApiPath = self::BASE_API_PATH.$wmsId.'/address';

        $response = $this->wmsConnector->request($fullApiPath, $websiteId, $request, 'PUT');

        return $response;
    }

    /**
     * @param array $addresses
     * @return array
     */
    protected function getRequest(array $addresses)
    {
        /** @var Address $shippingAddress */
        $shippingAddress = $addresses['shippingAddress'];
        $billingAddress  = $addresses['billingAddress'];
        $company_shipping = $shippingAddress->getCompany() ? $shippingAddress->getCompany() : '';
        $company_billing = $billingAddress->getCompany() ? $billingAddress->getCompany() : '';
        $regionArray = $this->getAllRegion();
        $data = [
            'shippingAddress' => [
                'firstname' => $shippingAddress->getFirstname(),
                'lastname' => $shippingAddress->getLastname(),
                'company'=>   $company_shipping,
                'email' => $shippingAddress->getEmail(),
                'address1' => $shippingAddress->getStreetLine(1),
                'address2' => $shippingAddress->getStreetLine(2),
                'address3' => $shippingAddress->getStreetLine(3),
                'telephone' => $shippingAddress->getTelephone(),
                'postcode' => $shippingAddress->getPostcode(),
                'city' => $shippingAddress->getCity(),
                'region' => $regionArray[$shippingAddress->getRegionId()] ?? '',
                'country' => $shippingAddress->getCountryId(),
            ],
            'billingAddress' => [
                'firstname' => $billingAddress->getFirstname(),
                'lastname' => $billingAddress->getLastname(),
                'company'=>   $company_billing,
                'email' => $billingAddress->getEmail(),
                'address1' => $billingAddress->getStreetLine(1),
                'address2' => $billingAddress->getStreetLine(2),
                'address3' => $billingAddress->getStreetLine(3),
                'telephone' => $billingAddress->getTelephone(),
                'postcode' => $billingAddress->getPostcode(),
                'city' => $billingAddress->getCity(),
                'region' => $regionArray[$billingAddress->getRegionId()] ?? '',
                'country' => $billingAddress->getCountryId(),
            ],
        ];
        return $data;
    }

    /**
     * @return mixed
     */
    protected function getAllRegion()
    {
        $regionCollection = $this->regionCollectionFactory->create();
        $regionCollection->load();

        foreach ($regionCollection->getItems() as $region) {
            $regionArray[$region->getId()] = $region->getCode();
        }

        return $regionArray;
    }

    /**
     * Split address line to respect limit
     * @param string $street
     * @return string
     */
    public function splitAddress(string $street)
    {
        $streetExploded = explode('\n', $street);
        $limit = 15;
        $format = false;
        //check if a line length exceeds limit
        foreach ($streetExploded as $line) {
            if (strlen($line) > $limit) {
                $format = true;
                break;
            }
        }

        if ($format) {
            $i = 0;
            $streets[0] = '';
            $word = strtok(str_replace("\n", "", $street), " ");
            while ($word !== false) {
                if ((strlen($streets[$i]) + strlen($word)) < $limit) {
                    $streets[$i] .= $word . " ";
                } else {
                    $i++;
                    //max lines reached
                    if ($i == 3) break;

                    $streets[$i] = $word . " ";
                }
                $word = strtok(" ");

            }
            return implode('\n', $streets);
        }
        return $street;
    }

    /**
     * format phone number to digits
     * @param string $telephone
     * @return mixed|null|string|string[]
     */
    public function sanitizeTelephone(string $telephone)
    {
        $telephone = str_replace("+", "00", $telephone);
        $telephone = preg_replace('/[^\d+]/', '', $telephone);
        return $telephone;
    }
}
