<?php

namespace Project\Wms\Model;

use Magento\Framework\Model\AbstractModel;
use Project\Wms\Api\Data\WmsErrorInterface;
use Project\Wms\Model\ResourceModel\WmsError as WmsErrorResource;

/**
 * Class WmsError
 * @package Project\Wms\Model
 * @author  Synolia <contact@synolia.com>
 */
class WmsError extends AbstractModel implements WmsErrorInterface
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    public function _construct()
    {
        $this->_init(WmsErrorResource::class);
    }

    /**
     * @return int
     */
    public function getErrorId()
    {
        return (int) $this->getData(WmsErrorInterface::ERROR_ID);
    }

    /**
     * @param int $errorId
     * @return $this
     */
    public function setErrorId($errorId)
    {
        $this->setData(WmsErrorInterface::ERROR_ID, $errorId);
        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return (int) $this->getData(WmsErrorInterface::ENTITY_ID);
    }

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->setData(WmsErrorInterface::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * @return int
     */
    public function getWmsId()
    {
        return (int) $this->getData(WmsErrorInterface::WMS_ID);
    }

    /**
     * @param int $wmsId
     * @return $this
     */
    public function setWmsId($wmsId)
    {
        $this->setData(WmsErrorInterface::WMS_ID, $wmsId);
        return $this;
    }

    /**
     * @return int
     */
    public function getNbErrorWms()
    {
        return (int) $this->getData(WmsErrorInterface::NB_ERROR_WMS);
    }

    /**
     * @param int $nbErrorWms
     * @return $this
     */
    public function setNbErrorWms($nbErrorWms)
    {
        $this->setData(WmsErrorInterface::NB_ERROR_WMS, $nbErrorWms);
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getLastErrorMsgWms()
    {
        return $this->getData(WmsErrorInterface::LAST_ERROR_MSG_WMS);
    }

    /**
     * @param string $lastErrorMsgWms
     * @return $this
     */
    public function setLastErrorMsgWms($lastErrorMsgWms)
    {
        $this->setData(WmsErrorInterface::LAST_ERROR_MSG_WMS, $lastErrorMsgWms);
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getEntityType()
    {
        return $this->getData(WmsErrorInterface::ENTITY_TYPE);
    }

    /**
     * @param string $entityType
     * @return $this
     */
    public function setEntityType($entityType)
    {
        $this->setData(WmsErrorInterface::ENTITY_TYPE, $entityType);
        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getApiMethod()
    {
        return $this->getData(WmsErrorInterface::API_METHOD);
    }

    /**
     * @param string $apiMethod
     * @return $this
     */
    public function setApiMethod($apiMethod)
    {
        $this->setData(WmsErrorInterface::API_METHOD, $apiMethod);
        return $this;
    }

    /**
     * @return mixed|boolean
     */
    public function isMailSent()
    {
        return (bool) $this->getData(WmsErrorInterface::MAIL_SENT);
    }

    /**
     * @param bool $mailSent
     * @return $this
     */
    public function setMailSent($mailSent)
    {
        $this->setData(WmsErrorInterface::MAIL_SENT, $mailSent);
        return $this;
    }
}
