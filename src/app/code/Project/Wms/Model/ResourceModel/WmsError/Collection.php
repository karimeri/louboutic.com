<?php
namespace Project\Wms\Model\ResourceModel\WmsError;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Wms\Model\ResourceModel\WmsError as WmsErrorResource;
use Project\Wms\Model\WmsError;

/**
 * Class Collection
 * @package Project\Wms\Model\ResourceModel\WmsError
 * @author  Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init(WmsError::class, WmsErrorResource::class);
    }
}
