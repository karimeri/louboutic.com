<?php
namespace Project\Wms\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Wms\Model\WmsError as WmsErrorModel;

/**
 * Class WmsError
 * @package Project\Wms\Model\ResourceModel
 * @author  Synolia <contact@synolia.com>
 */
class WmsError extends AbstractDb
{
    const TABLE_NAME = 'wms_error';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     */
    protected function _construct()
    {
        $this->_init($this::TABLE_NAME, WmsErrorModel::ERROR_ID);
    }
}
