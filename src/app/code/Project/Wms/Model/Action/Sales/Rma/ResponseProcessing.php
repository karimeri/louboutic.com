<?php

namespace Project\Wms\Model\Action\Sales\Rma;

use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Project\Wms\Model\WmsError;

/**
 * Class ResponseProcessing
 * @package Project\Wms\Model\Action\Sales\Rma
 * @author  Synolia <contact@synolia.com>
 */
class ResponseProcessing extends \Project\Wms\Model\Action\ResponseProcessing
{
    /**
     * @param array $success
     * @return array
     */
    protected function processSuccess(array $success)
    {
        $connection = $this->resourceConnection->getConnection();

        $conditions = [];
        foreach ($success as $data) {
            $rmaEntityId[]     = $data->entity_id;
            $case              = $connection->quoteInto('?', $data->entity_id);
            $result            = $connection->quoteInto('?', $data->wms_id);
            $conditions[$case] = $result;
        }

        $value = $connection->getCaseSql('entity_id', $conditions);
        $where = ['entity_id IN (?)' => $rmaEntityId];

        try {
            $connection->beginTransaction();
            $connection->update(
                $connection->getTableName('magento_rma'),
                [
                    'wms_id' => $value
                ],
                $where
            );
            $connection->commit();
        } catch (\Exception $e) {
            $rmaEntityId = implode(',', $rmaEntityId);

            $errorMsg[] = 'RMA entity_id : '.$rmaEntityId;
            $errorMsg[] = 'Type : '.WmsError::RMA_TYPE;
            $errorMsg[] = 'API Method : '.WmsError::CREATE_RMA_API_METHOD;
            $errorMsg[] = 'Message : '.$e->getMessage();

            $this->logger->addError(implode(' ', $errorMsg));
            $connection->rollBack();
        }
    }

    /**
     * @param array $error
     * @param array $params
     * @throws \Magento\Framework\Exception\MailException
     */
    protected function processError(array $error, array $params)
    {
        $connection = $this->resourceConnection->getConnection();

        foreach ($error as $responseLine) {
            $rmaEntityId[$responseLine->entity_id] = [
                'increment_id' => $responseLine->increment_id,
                'message' => $responseLine->message
            ];
        }

        $sql = $connection->select()
            ->from('magento_rma', ['order_id', 'entity_id'])
            ->where('entity_id IN (?)', array_keys($rmaEntityId));

        //phpcs:ignore Ecg.Performance.FetchAll.Found
        $result = $connection->fetchAll($sql);

        $errorMapped = [];
        foreach ($result as $line) {
            $errorMapped[] = (object)[
                'entity_id' => $line['order_id'],
                'increment_id' => $rmaEntityId[$line['entity_id']]['increment_id'],
                'message' => $rmaEntityId[$line['entity_id']]['message']
            ];
        }

        parent::processError($errorMapped, $params);
    }
}
