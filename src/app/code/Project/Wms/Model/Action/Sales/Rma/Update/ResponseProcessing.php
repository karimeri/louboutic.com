<?php

namespace Project\Wms\Model\Action\Sales\Rma\Update;

use Project\Wms\Model\WmsError;

/**
 * Class ResponseProcessing
 * @package Project\Wms\Model\Action\Sales\Rma
 * @author  Synolia <contact@synolia.com>
 */
class ResponseProcessing extends \Project\Wms\Model\Action\Sales\Rma\ResponseProcessing
{
    /**
     * @param array $success
     * @return array|void
     */
    protected function processSuccess(array $success)
    {
        $connection = $this->resourceConnection->getConnection();

        foreach ($success as $data) {
            $rmaEntityId[]     = $data->entity_id;
        }

        $where = ['entity_id IN (?)' => $rmaEntityId];

        try {
            $connection->beginTransaction();
            $connection->update(
                $connection->getTableName('magento_rma'),
                [
                    'wms_exported' => 1
                ],
                $where
            );
            $connection->commit();
        } catch (\Exception $e) {
            $rmaEntityId = implode(',', $rmaEntityId);

            $errorMsg[] = 'RMA entity_id : '.$rmaEntityId;
            $errorMsg[] = 'Type : '.WmsError::RMA_TYPE;
            $errorMsg[] = 'API Method : '.WmsError::CREATE_RMA_API_METHOD;
            $errorMsg[] = 'Message : '.$e->getMessage();

            $this->logger->addError(implode(' ', $errorMsg));
            $connection->rollBack();
        }
    }
}
