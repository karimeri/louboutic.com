<?php

namespace Project\Wms\Model\Action\Sales\Preorder;

use Project\Sales\Model\Magento\Sales\Order;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;

/**
 * Class Export
 * @package Project\Wms\Model\Action\Sales\Preorder
 * @author Synolia <contact@synolia.com>
 */
class Export implements ActionInterface
{
    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;



    /**
     * Export constructor.
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param State $state
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        State $state
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }

    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $orderCollection = $this->orderCollectionFactory->create();
        $orderCollection->addFieldToFilter('status', Order::STATE_PREORDER_ENTERING_STOCK);
        $orderCollection->addFieldToFilter('send_full_auth', 0);
        $orderCollection->load();

        return $orderCollection;
    }
}
