<?php

namespace Project\Wms\Model\Action\Sales\Preorder;

use Project\Adyen\Helper\Service\AuthorizeService;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class FullAuthorize
 * @package Project\Wms\Model\Action\Sales\Preorder
 * @author Synolia <contact@synolia.com>
 */
class FullAuthorize implements ActionInterface
{
    /**
     * @var AuthorizeService
     */
    protected $_authorizeService;

    /**
     * FullAuthorize constructor.
     * @param AuthorizeService $authorizeService
     */
    public function __construct(
        AuthorizeService $authorizeService
    ) {
        $this->_authorizeService = $authorizeService;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|void
     * @throws \Adyen\AdyenException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        foreach ($data[0]->getItems() as $order) {
            // ADYEN AUTHORIZATION
            $this->_authorizeService->fullAuthorize($order);
        }
    }
}
