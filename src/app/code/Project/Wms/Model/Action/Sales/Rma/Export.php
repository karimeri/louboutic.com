<?php

namespace Project\Wms\Model\Action\Sales\Rma;

use Magento\Rma\Model\ResourceModel\Shipping\CollectionFactory;
use Magento\Rma\Model\Rma\Source\Status;
use Magento\Store\Model\StoreManager;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Rma\Model\ResourceModel\Rma\CollectionFactory as RmaCollectionFactory;
use Synolia\Sync\Console\ConsoleOutput;

/**
 * Class Export
 * @package Project\Wms\Model\Action\Sales\Rma
 * @author  Synolia <contact@synolia.com>
 */
class Export implements ActionInterface
{
    /**
     * @var RmaCollectionFactory
     */
    protected $rmaCollectionFactory;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var \Magento\Rma\Model\ResourceModel\Shipping\CollectionFactory
     */
    protected $shippingCollectionFactory;

    /**
     * Export constructor.
     * @param \Magento\Rma\Model\ResourceModel\Rma\CollectionFactory $rmaCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        RmaCollectionFactory $rmaCollectionFactory,
        State $state,
        StoreManager $storeManager,
        CollectionFactory $shippingCollectionFactory
    ) {
        $this->rmaCollectionFactory = $rmaCollectionFactory;
        $this->storeManager = $storeManager;
        $this->shippingCollectionFactory = $shippingCollectionFactory;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param                                     $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $rmaCollection = $this->rmaCollectionFactory->create();
        $rmaCollection->addFieldToFilter('status', Status::STATE_AUTHORIZED);
        $rmaCollection->addFieldToFilter('wms_id', ['null' => true]);

        if (isset($params['website'])) {
            $website = $this->storeManager->getWebsite($params['website']);
            $stores = $this->storeManager->getStoreByWebsiteId($website->getId());

            $rmaCollection->addFieldToFilter('store_id', ['in' => $stores]);
        }

        $rmaCollection->load();

        $dataToExport = [];
        foreach ($rmaCollection->getItems() as $rma) {
            $itemsArray = [];
            foreach ($rma->getItems(true) as $item) {
                $itemsArray[] = [
                    'entity_id' => (int)$item->getEntityId(),
                    'qty_requested' => (int)$item->getQtyRequested(),
                    'order_item_id' => (int)$item->getOrderItemId(),
                    'product_name' => $item->getProductName(),
                    'product_sku' => $item->getProductSku(),
                ];
            }

            $shippingCollection = $this->shippingCollectionFactory->create();
            //phpcs:disable Ecg.Performance.GetFirstItem.Found
            //phpcs:disable Ecg.Performance.Loop.DataLoad
            $shipping = $shippingCollection->addFieldToFilter(
                'rma_entity_id',
                $rma->getId()
            )->addFieldToFilter(
                'is_admin',
                ["neq" => \Magento\Rma\Model\Shipping::IS_ADMIN_STATUS_ADMIN_LABEL]
            )->setPageSize(1)
                ->getFirstItem();

            $dataToExport[] = [
                'entity_id' => (int)$rma->getEntityId(),
                'increment_id' => $rma->getIncrementId(),
                'date_requested' => $rma->getDateRequested(),
                'order_id' => (int)$rma->getOrderId(),
                'order_increment_id' => $rma->getOrderIncrementId(),
                'tracking_number' => $shipping->getTrackNumber(),
                'items' => $itemsArray
            ];
        }

        return $dataToExport;
    }
}
