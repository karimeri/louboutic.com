<?php

namespace Project\Wms\Model\Action\Sales\Shipping;

use Eu\Core\Setup\UpgradeData;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\Order\ShipmentRepository;
use Magento\Sales\Model\OrderRepository;
use Magento\Store\Model\StoreManager;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\CustomInvoice;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\Invoice;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\CustomInvoiceFactory;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\InvoiceFactory;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\CustomReturnProformaInvoice;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\CustomReturnProformaInvoiceFactory;
use Project\Sales\Model\Magento\Sales\Order;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Shipping\Model\ShipmentNotifierFactory;
use Magento\Sales\Model\Convert\OrderFactory as ConvertOrderFactory;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Filesystem\Io\Sftp as SFTP;
use Project\Wms\Helper\Config;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class Export
 * @package Project\Wms\Model\Action\Sales\Shipping
 * @author  Synolia <contact@synolia.com>
 */
class Export implements ActionInterface
{
    const PDF_PATH_CUSTOM_RETURN_PROFORMA_INVOICE = 'var/export/pdf/custom_return_proforma_invoice/';
    const PDF_PATH_CUSTOM_INVOICE = 'var/export/pdf/custom_invoice/';
    const PDF_PATH_INVOICE = 'var/export/pdf/invoice/';
    const CONNECTION_TIMEOUT = 3600;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var ConvertOrderFactory $convertOrderFactory
     */
    protected $convertOrderFactory;

    /**
     * @var ShipmentNotifierFactory $shipmentNotifierFactory
     */
    protected $shipmentNotifierFactory;

    /**
     * @var ShipmentRepository $shipmentRepository
     */
    protected $shipmentRepository;

    /**
     * @var OrderRepository $orderRepository
     */
    protected $orderRepository;

    /**
     * @var CustomReturnProformaInvoice $customReturnProformaInvoice
     */
    protected $customReturnProformaInvoice;

    /**
     * @var CustomInvoice $customInvoice
     */
    protected $customInvoice;

    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var File
     */
    protected $ioFile;

    /**
     * @var SFTP
     */
    private $sftp;

    /**
     * @var Config
     */
    protected $wmsHelper;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * Export constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\App\State $state
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory
     * @param \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Shipping\Model\ShipmentNotifierFactory $shipmentNotifierFactory
     * @param CustomReturnProformaInvoiceFactory $customReturnProformaInvoiceFactory
     * @param \Project\Pdf\Model\Magento\Sales\Order\Pdf\CustomInvoiceFactory $customInvoiceFactory
     * @param \Project\Pdf\Model\Magento\Sales\Order\Pdf\InvoiceFactory $invoiceFactory
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory
     * @param \Magento\Framework\Filesystem\Io\File $ioFile
     * @param SFTP $sftp
     * @param Config $wmsHelper
     * @param EncryptorInterface $encryptor
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        ResourceConnection $resourceConnection,
        State $state,
        EnvironmentManager $environmentManager,
        ConvertOrderFactory $convertOrderFactory,
        ShipmentRepository $shipmentRepository,
        OrderRepository $orderRepository,
        ShipmentNotifierFactory $shipmentNotifierFactory,
        CustomReturnProformaInvoiceFactory $customReturnProformaInvoiceFactory,
        CustomInvoiceFactory $customInvoiceFactory,
        InvoiceFactory $invoiceFactory,
        FileFactory $fileFactory,
        DateTimeFactory $dateTimeFactory,
        File $ioFile,
        SFTP $sftp,
        Config $wmsHelper,
        EncryptorInterface $encryptor,
        StoreManager $storeManager
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->resourceConnection = $resourceConnection;
        $this->environmentManager = $environmentManager;
        $this->convertOrderFactory = $convertOrderFactory;
        $this->shipmentRepository = $shipmentRepository;
        $this->orderRepository = $orderRepository;
        $this->shipmentNotifierFactory = $shipmentNotifierFactory;
        $this->customReturnProformaInvoice = $customReturnProformaInvoiceFactory->create();
        $this->customInvoice = $customInvoiceFactory->create();
        $this->invoice = $invoiceFactory->create();
        $this->fileFactory = $fileFactory;
        $this->dateTime = $dateTimeFactory->create();
        $this->ioFile = $ioFile;
        $this->sftp = $sftp;
        $this->wmsHelper = $wmsHelper;
        $this->encryptor = $encryptor;
        $this->storeManager = $storeManager;

        try {
            $state->setAreaCode(Area::AREA_GLOBAL);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $dataToExport = [];

        $orderCollection = $this->orderCollectionFactory->create();
        //export orders with shipping status
        $orderCollection->addFieldToFilter('status', Order::STATE_SHIPPING);

        $connection = $this->resourceConnection->getConnection();
        $bind = [
            'flow_name' => $flowCode,
            'code' => 'increment_id'
        ];
        $select = $connection->select()
            ->from('synolia_sync_processed', 'id')
            ->where('flow_name = :flow_name')
            ->where('code = :code');

        // @codingStandardsIgnoreLine
        $processedId = $connection->fetchAll($select, $bind, \Zend_Db::FETCH_COLUMN);

        if ($processedId) {
            $orderCollection->addFieldToFilter('increment_id', ['nin' => $processedId]);
        }

        if (isset($params['website'])) {
            $website = $this->storeManager->getWebsite($params['website']);
            $stores = $this->storeManager->getStoreByWebsiteId($website->getId());

            $orderCollection->addFieldToFilter('store_id', ['in' => $stores]);
        }

        $orderCollection->load();

        foreach ($orderCollection->getItems() as $order) {
            //check if order has invoiced
            if(!$order->hasInvoices()){
                continue;
            }
            if ($this->environmentManager->getEnvironment() === Environment::HK) {
                $dataToExport[] = $this->generatePdf($order, false, true, true);
            } elseif ($this->environmentManager->getEnvironment() === Environment::EU &&
                (
                    $order->getStore()->getCode() === UpgradeData::STORE_CODE_CH_EN ||
                    $order->getStore()->getCode() === UpgradeData::STORE_CODE_CH_FR ||
                    $order->getStore()->getCode() === UpgradeData::STORE_CODE_UK_EN
                )
            ) {
                $dataToExport[] = $this->generatePdf($order, true, false, true);
            } elseif ($this->environmentManager->getEnvironment() === Environment::EU) {
                $dataToExport[] = $this->generatePdf($order, true, false, false);
            } else {
                $dataToExport[] = $this->generatePdf($order, false, false, false);
            }
        }

        return $dataToExport;
    }

    /**
     * @param Order $order
     * @param bool $invoice
     * @param bool $customInvoice
     * @param bool $customReturn
     * @return array
     * @throws LocalizedException
     * @throws \Zend_Pdf_Exception
     */
    public function generatePdf(Order $order, $invoice = true, $customInvoice = false, $customReturn = false)
    {
        $invoices = $order->getInvoiceCollection();
        $date = $this->dateTime->date('Y-m-d_H-i-s');

        $this->ioFile->mkdir(self::PDF_PATH_INVOICE, 0775);
        $this->ioFile->mkdir(self::PDF_PATH_CUSTOM_INVOICE, 0775);
        $this->ioFile->mkdir(self::PDF_PATH_CUSTOM_RETURN_PROFORMA_INVOICE, 0775);

        $data = [];
        $data['increment_id'] = $order->getIncrementId();
        if ($invoice) {
            $invoicePdfContent = $this->invoice->getPdf($invoices);
            $filenameInvoice = 'invoice'.$date.'_'.$order->getIncrementId().'.pdf';
            $this->writePdf(self::PDF_PATH_INVOICE, $filenameInvoice, $invoicePdfContent->render());
            $invoicePath = str_replace(
                'var/export/pdf/',
                '',
                self::PDF_PATH_INVOICE
            );
            $data['invoice'] = $invoicePath.$filenameInvoice;

            // Upload file to remote server.
            $remoteDir = $this->wmsHelper->getWmsSftpPath() . '/invoice';
            $filepath = self::PDF_PATH_INVOICE. $filenameInvoice;

            $this->upload($filenameInvoice, $filepath, $remoteDir);
        }
        if ($customInvoice) {
            $customInvoicePdfContent = $this->customInvoice->getPdf($invoices);
            $filenameCustomInvoice = 'custom_invoice'.$date.'_'.$order->getIncrementId().'.pdf';
            $this->writePdf(self::PDF_PATH_CUSTOM_INVOICE, $filenameCustomInvoice, $customInvoicePdfContent->render());
            $customInvoicePath = str_replace(
                'var/export/pdf/',
                '',
                self::PDF_PATH_CUSTOM_INVOICE
            );
            $data['custom_invoice'] = $customInvoicePath.$filenameCustomInvoice;

            // Upload file to remote server.
            $remoteDir = $this->wmsHelper->getWmsSftpPath() . '/custom_invoice';
            $filepath = self::PDF_PATH_CUSTOM_INVOICE. $filenameCustomInvoice;

            $this->upload($filenameCustomInvoice, $filepath, $remoteDir);
        }
        if ($customReturn) {
            $customReturnProformaPdfContent = $this->customReturnProformaInvoice->getPdf($invoices);
            $filenameCustomReturn = 'custom_return_proforma_invoice'.$date.'_'.$order->getIncrementId().'.pdf';
            $this->writePdf(self::PDF_PATH_CUSTOM_RETURN_PROFORMA_INVOICE, $filenameCustomReturn, $customReturnProformaPdfContent->render());
            $customReturnPath = str_replace(
                'var/export/pdf/',
                '',
                self::PDF_PATH_CUSTOM_RETURN_PROFORMA_INVOICE
            );
            $data['custom_return_proforma'] = $customReturnPath.$filenameCustomReturn;

            // Upload file to remote server.
            $remoteDir = $this->wmsHelper->getWmsSftpPath() . '/custom_return_proforma_invoice';
            $filepath = self::PDF_PATH_CUSTOM_RETURN_PROFORMA_INVOICE. $filenameCustomReturn;

            $this->upload($filenameCustomReturn, $filepath, $remoteDir);
        }

        return $data;
    }

    /**
     * @param string $path
     * @param string $filename
     * @param string $content
     * @throws \Exception
     */
    protected function writePdf($path, $filename, $content)
    {
        $this->ioFile->open(
            [
                'path' => $path
            ]
        );

        $result = $this->ioFile->write($filename, $content, 0666);

        if (!$result) {
            throw new \Exception('Error when trying to save pdf');
        }
    }

    /**
     * Upload file to remote server.
     * @param string $filename
     * @param string $filepath
     * @param string $remoteDir
     */
    public function upload($filename, $filepath, $remoteDir)
    {
        $this->connect();
        $this->sftp->cd($remoteDir);
        $this->sftp->write($filename, $filepath);
    }

    /**
     * {@inheritdoc}
     */
    public function connect()
    {
        $password = $this->encryptor->decrypt($this->wmsHelper->getWmsSftpPassword());
        $args = [
            'host'     => $this->wmsHelper->getWmsSftpHost(),
            'username' => $this->wmsHelper->getWmsSftpUsername(),
            'password' => $password,
            'timeout'  => self::CONNECTION_TIMEOUT,
        ];

        $this->sftp->open($args);
    }
}
