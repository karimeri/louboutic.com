<?php

namespace Project\Wms\Model\Action\Sales\Order;

use Magento\Customer\Model\ResourceModel\GroupRepository;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\ObjectManager\ConfigLoaderInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\GiftMessage\Model\MessageFactory as MessageModelFactory;
use Magento\GiftMessage\Model\ResourceModel\MessageFactory;
use Magento\Sales\Model\Order\Address;
use Magento\Store\Model\StoreManager;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Sales\Model\Magento\Sales\Order;
use Project\Wms\Helper\Config;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollectionFactory;

/**
 * Class Export
 * @package Project\Wms\Model\Action\Sales\Order
 * @author  Synolia <contact@synolia.com>
 */
class Export implements ActionInterface
{
    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var RegionCollectionFactory
     */
    protected $regionCollectionFactory;

    /**
     * @var MessageFactory
     */
    protected $giftMessageFactory;

    /**
     * @var Config
     */
    protected $wmsHelper;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var MessageModelFactory
     */
    protected $messageModelFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ConfigLoaderInterface
     */
    protected $configLoader;

    /**
     * @var \Magento\Customer\Model\ResourceModel\GroupRepository
     */
    protected $customerGroupRepository;

    /**
     * Export constructor.
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param RegionCollectionFactory $regionCollectionFactory
     * @param MessageFactory $giftMessageFactory
     * @param Config $wmsHelper
     * @param State $state
     * @param EnvironmentManager $environmentManager
     * @param StoreManager $storeManager
     * @param MessageModelFactory $messageModelFactory
     * @param ObjectManagerInterface $objectManager
     * @param ConfigLoaderInterface $configLoader
     * @param GroupRepository $customerGroupRepository
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        RegionCollectionFactory $regionCollectionFactory,
        MessageFactory $giftMessageFactory,
        Config $wmsHelper,
        State $state,
        EnvironmentManager $environmentManager,
        StoreManager $storeManager,
        MessageModelFactory $messageModelFactory,
        ObjectManagerInterface $objectManager,
        ConfigLoaderInterface $configLoader,
        GroupRepository $customerGroupRepository
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->regionCollectionFactory = $regionCollectionFactory;
        $this->giftMessageFactory = $giftMessageFactory;
        $this->wmsHelper = $wmsHelper;
        $this->environmentManager = $environmentManager;
        $this->storeManager = $storeManager;
        $this->messageModelFactory = $messageModelFactory;
        $this->objectManager = $objectManager;
        $this->configLoader = $configLoader;
        $this->customerGroupRepository = $customerGroupRepository;

        //In order to prevent fatal error on debugHintsPath param
        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $orderCollection = $this->orderCollectionFactory->create();
        $select = $orderCollection->getSelect();
        $select->where(
            '(status IN (?)) OR (status = "'.Order::STATE_PAYMENT_REVIEW.'" AND wms_id IS NULL)',
            [
                Order::STATE_PROCESSING,
                Order::STATE_PENDING_PAYMENT
            ]
        );

        if (isset($params['website'])) {
            $website = $this->storeManager->getWebsite($params['website']);
            $stores = $this->storeManager->getStoreByWebsiteId($website->getId());

            $orderCollection->addFieldToFilter('store_id', ['in' => $stores]);
        }

        $orderCollection->load();
        $regionArray = $this->getAllRegion();

        $dataToExport = [];
        foreach ($orderCollection->getItems() as $order) {
            $isPreorder = false;

            /**@var Order $order */
            /**@var Address $shippingAddress */
            $shippingAddress = $order->getShippingAddress();
            /**@var Address $billingAddress */
            $billingAddress = $order->getBillingAddress();

            if (is_null($shippingAddress) || is_null($billingAddress)) {
                continue;
            }

            $giftMessageOrder = '';
            if ($order->getGiftMessageId()) {
                $giftMessageModel = $this->messageModelFactory->create();
                //phpcs:ignore Ecg.Performance.Loop.ModelLSD -- I assume, small volume because export on every minute
                $this->giftMessageFactory->create()->load($giftMessageModel, $order->getGiftMessageId());
                $giftMessageOrder = $giftMessageModel->getMessage();
            }

            $warehouse = $this->wmsHelper->getEstablishmentCode($order->getStore()->getWebsiteId());

            $itemsArray = [];
            foreach ($order->getAllVisibleItems() as $item) {
                if ($item->getIsPreorder()) {
                    $isPreorder = true;
                }
                $giftMessageItem = '';
                if ($item->getGiftMessageId()) {
                    $giftMessageModel = $this->messageModelFactory->create();
                    //phpcs:ignore Ecg.Performance.Loop.ModelLSD
                    $this->giftMessageFactory->create()->load($giftMessageModel, $item->getGiftMessageId());
                    $giftMessageItem = $giftMessageModel->getMessage();
                }

                $metadataItem = [];
                if ($this->environmentManager->getEnvironment() == Environment::JP) {
                    $metadataItem = [
                        'tax' => $item->getTaxAmount(),
                        'subtotal' => $item->getRowTotal()
                    ];
                }

                $itemsArray[] = [
                    'item_id' => (int)$item->getItemId(),
                    'sku' => $item->getSku(),
                    'qty' => (int)$item->getQtyOrdered(),
                    'grand_total' => $item->getRowTotalInclTax(),
                    'gift_wrap' => $item->getGwId() ? true : false,
                    'gift_message' => $giftMessageItem,
                    'meta_data' => $metadataItem
                ];
            }

            $customerGroupId = $order->getCustomerGroupId();
            $customerGroup = $this->customerGroupRepository->getById($customerGroupId);
            $customerGroupCode = $customerGroup->getCode();

            $metadata = [
                'client_code' => $order->getCustomerId(),
                'client_type' => $order->getCustomerIsGuest() ? 'Guest' : 'Regular',
                'customer_priority' => $customerGroupCode,
                'billing_type' => $order->getPayment()->getMethod(),
                'storeview_lang' => $order->getStore()->getCode(),
                'placed_from' => $order->getRemoteIp() ? 'FO' : 'BO'
            ];

            if ($this->environmentManager->getEnvironment() == Environment::JP) {
                $dataJapan = [
                    'desired_delivery_date' => $order->getJapanShippingDay(),
                    'desired_delivery_time' => $order->getJapanShippingHour(),
                    'cod_fee' => $order->getCodAmount()
                ];

                $metadata = array_merge($metadata, $dataJapan);
            }
            $company_shipping = $shippingAddress->getCompany() ? $shippingAddress->getCompany() : '';
            $company_billing = $billingAddress->getCompany() ? $billingAddress->getCompany() : '';
            //Format street & telephone for UPS
            $billingAddress->setStreet($this->splitAddress($billingAddress->getStreet()));
            $billingAddress->setTelephone($this->sanitizeTelephone($billingAddress->getTelephone()));
            $shippingAddress->setStreet($this->splitAddress($shippingAddress->getStreet()));
            $shippingAddress->setTelephone($this->sanitizeTelephone($shippingAddress->getTelephone()));

            $data = [
                'entity_id' => (int)$order->getId(),
                'increment_id' => $order->getIncrementId(),
                'status' => $order->getStatus(),
                'shipping_address' => [
                    'firstname' => $shippingAddress->getFirstname(),
                    'lastname' => $shippingAddress->getLastname(),
                    'company'=>   $company_shipping,
                    'email' => $shippingAddress->getEmail(),
                    'address1' => $shippingAddress->getStreetLine(1),
                    'address2' => $shippingAddress->getStreetLine(2),
                    'address3' => $shippingAddress->getStreetLine(3),
                    'telephone' => $shippingAddress->getTelephone(),
                    'postcode' => $shippingAddress->getPostcode(),
                    'city' => $shippingAddress->getCity(),
                    'region' => $regionArray[$shippingAddress->getRegionId()] ?? '',
                    'country' => $shippingAddress->getCountryId(),
                ],
                'billing_address' => [
                    'firstname' => $billingAddress->getFirstname(),
                    'lastname' => $billingAddress->getLastname(),
                    'company'=>   $company_billing,
                    'email' => $billingAddress->getEmail(),
                    'address1' => $billingAddress->getStreetLine(1),
                    'address2' => $billingAddress->getStreetLine(2),
                    'address3' => $billingAddress->getStreetLine(3),
                    'telephone' => $billingAddress->getTelephone(),
                    'postcode' => $billingAddress->getPostcode(),
                    'city' => $billingAddress->getCity(),
                    'region' => $regionArray[$billingAddress->getRegionId()] ?? '',
                    'country' => $billingAddress->getCountryId(),
                ],
                'meta_data' => $metadata,
                'grand_total' => $order->getGrandTotal(),
                'shipping_fee' => $order->getShippingInclTax(),
                'shipping_method' => $order->getShippingMethod(),
                'gift_wrap' => $order->getGwId() ? true : false,
                'gift_message' => $giftMessageOrder,
                'is_preordered' => (bool)$isPreorder,
                'warehouse' => $warehouse,
                'ordered_at' => $order->getCreatedAt(),
                'items' => $itemsArray,
                'method_of_payment' => $order->getPayment()->getMethod(),
                'tax' => $order->getTaxAmount()
            ];

            $dataToExport[] = $data;
        }

        return $dataToExport;
    }

    /**
     * @return mixed
     */
    private function getAllRegion()
    {
        $regionCollection = $this->regionCollectionFactory->create();
        $regionCollection->load();

        foreach ($regionCollection->getItems() as $region) {
            $regionArray[$region->getId()] = $region->getCode();
        }

        return $regionArray;
    }

    /**
     * Split street lines to not exceed limit
     * @param array $orgStreet
     * @return array
     */
    public function splitAddress(array $orgStreet)
    {
        $limit = 35;
        $format = false;
        //check if a line length exceeds limit
        foreach ($orgStreet as $line) {
            if (strlen($line) > $limit) {
                $format = true;
                break;
            }
        }

        if ($format) {
            $i = 0;
            $streets[0] = '';
            $word = strtok(implode(" ", $orgStreet), " ");
            while ($word !== false) {
                if ((strlen($streets[$i]) + strlen($word)) < $limit) {
                    $streets[$i] .= $word . " ";
                } else {
                    $i++;
                    //max lines reached
                    if ($i == 3) break;

                    $streets[$i] = $word . " ";
                }
                $word = strtok(" ");

            }
            return ($streets);
        }
        return ($orgStreet);
    }

    /**
     * Format phone to digits only
     * @param string $telephone
     * @return mixed|null|string|string[]
     */
    public function sanitizeTelephone(string $telephone)
    {
        $telephone = str_replace("+", "00", $telephone);
        $telephone = preg_replace('/[^\d+]/', '', $telephone);
        return $telephone;
    }

}
