<?php

namespace Project\Wms\Model\Action\Sales\Order;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Sales\Model\Magento\Sales\Order;
use Project\Sales\Model\Magento\Sales\OrderRepository;
use Project\Wms\Model\Action\AbstractResponseProcessing;
use Project\Wms\Model\Api\Connector;
use Project\Wms\Model\ResourceModel\WmsError\CollectionFactory as WmsErrorCollectionFactory;
use Project\Wms\Model\Service\Mail;
use Project\Wms\Model\WmsError;
use Project\Wms\Model\WmsErrorRepository;
use Synolia\Logger\Model\Logger;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Helper\Config as SyncConfigHelper;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\OrderRepository as MagentoOrderRepository;

/**
 * Class ResponseProcessing
 * @package Project\Wms\Model\Action\Sales\Order
 * @author  Synolia <contact@synolia.com>
 */
class ResponseProcessing extends AbstractResponseProcessing implements ActionInterface
{
    const NB_MAX_ERROR_WMS = 3;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var WmsErrorCollectionFactory
     */
    protected $wmsErrorCollectionFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var MagentoOrderRepository
     */
    protected $magentoOrderRepository;

    /**
     * @var Connector
     */
    protected $wmsConnector;

    /**
     * ResponseProcessing constructor.
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Synolia\Sync\Helper\Config $syncConfigHelper
     * @param \Project\Wms\Model\WmsErrorRepository $wmsErrorRepository
     * @param \Project\Sales\Model\Magento\Sales\OrderRepository $orderRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
     * @param \Project\Wms\Model\Service\Mail $mailService
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Project\Wms\Model\ResourceModel\WmsError\CollectionFactory $wmsErrorCollectionFactory
     * @param \Synolia\Logger\Model\LoggerFactory $loggerFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Sales\Model\OrderRepository $magentoOrderRepository
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        SyncConfigHelper $syncConfigHelper,
        WmsErrorRepository $wmsErrorRepository,
        OrderRepository $orderRepository,
        DateTimeFactory $dateFactory,
        Mail $mailService,
        ResourceConnection $resourceConnection,
        WmsErrorCollectionFactory $wmsErrorCollectionFactory,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        EnvironmentManager $environmentManager,
        OrderFactory $orderFactory,
        MagentoOrderRepository $magentoOrderRepository,
        Connector $wmsConnector
    ) {
        parent::__construct(
            $transportBuilder,
            $syncConfigHelper,
            $wmsErrorRepository,
            $orderRepository,
            $dateFactory,
            $mailService
        );

        $this->resourceConnection        = $resourceConnection;
        $this->wmsErrorCollectionFactory = $wmsErrorCollectionFactory;
        $this->environmentManager        = $environmentManager;
        $this->orderFactory              = $orderFactory;
        $this->magentoOrderRepository    = $magentoOrderRepository;
        $this->wmsConnector              = $wmsConnector;

        $logPath      = $directoryList->getPath('log');
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath.DIRECTORY_SEPARATOR.WmsErrorRepository::WMS_ERROR_LOG_FILENAME]
        );
    }

    /**
     * @param array         $params
     * @param array         $data
     * @param string        $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $apiResponse = $data[0];
        if ($apiResponse) {
            if (isset($apiResponse->success) && count($apiResponse->success)) {
                $this->processSuccess($apiResponse->success);
            }
            if (isset($apiResponse->error) && count($apiResponse->error)) {
                $this->processError($apiResponse->error, $params);
            }
        }
    }

    /**
     * @param array $success
     */
    protected function processSuccess(array $success)
    {
        $connection = $this->resourceConnection->getConnection();
        $conditions = [];
        $orderEntityIdSentToLogistic = [];
        $orderEntityIdPaymentReview = [];
        foreach ($success as $data) {
            //phpcs:ignore Ecg.Performance.Loop.ModelLSD
            $order = $this->orderFactory->create()->load($data->entity_id);
            if ($order->getStatus() == Order::STATE_PAYMENT_REVIEW) {
                $orderEntityIdPaymentReview[$data->entity_id] = $data->id_wms;
            } else {
                $orderEntityIdSentToLogistic[$data->entity_id] = $data->id_wms;
            }
            $case              = $connection->quoteInto('?', $data->entity_id);
            $result            = $connection->quoteInto('?', $data->id_wms);
            $conditions[$case] = $result;
        }

        $value = $connection->getCaseSql('entity_id', $conditions);
        $orderEntityId = $orderEntityIdSentToLogistic + $orderEntityIdPaymentReview;
        $whereForWmsIdUpdate = ['entity_id IN (?)' => array_keys($orderEntityId)];
        $connection->beginTransaction();
        $connection->update(
            $connection->getTableName('sales_order'),
            ['wms_id' => $value],
            $whereForWmsIdUpdate
        );
        $connection->commit();

        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $this->usOrderProcess($orderEntityId);
        } else {
            $this->otherInstanceOrderProcess($connection, $orderEntityId, $orderEntityIdSentToLogistic);
        }
    }

    /**
     * @param array $orderEntityId
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function usOrderProcess(array $orderEntityId)
    {
        foreach ($orderEntityId as $entityId => $wmsId) {
            //phpcs:ignore Ecg.Performance.Loop.ModelLSD
            $order   = $this->orderFactory->create()->load($entityId);

            $isPreorder = false;
            foreach ($order->getAllVisibleItems() as $item) {
                if ($item->getIsPreorder()) {
                    $isPreorder = true;
                    break;
                }
            }

            if (!$isPreorder) {
                try {
                    if ($order->getPayment()->getMethod() == 'chcybersource'
                        && $order->getStatus() != Order::STATE_PAYMENT_REVIEW
                    ) {
                        // TODO_ADYEN check Edyen Method here
                       // $this->authorizeService->fullAuthorize($order);
                    }
                } catch (\Throwable $e) {
                    $message = __(
                        'Error during full authorization, see log for more information : %1',
                        $e->getMessage()
                    );
                    $order->setState(Order::STATE_AUTORIZATION_ERROR);
                    $order->setStatus(Order::STATE_AUTORIZATION_ERROR);
                    $order->addStatusHistoryComment($message)
                        ->setIsCustomerNotified(false);

                    //phpcs:ignore Ecg.Performance.Loop.ModelLSD
                    $this->magentoOrderRepository->save($order);
                    continue;
                }
                if ($order->getStatus() == Order::STATE_PENDING_PAYMENT ||
                    $order->getStatus() == Order::STATE_PROCESSING ||
                    $order->getStatus() == Order::STATE_SENT_TO_LOGISTIC) {
                    $this->sendToLogistic($order, $wmsId);
                }
            }
        }
    }

    /**
     * @param $order
     * @param $wmsId
     */
    private function sendToLogistic($order, $wmsId)
    {
        try {
            $saveOrder = false;
            if ($order->getStatus() != Order::STATE_SENT_TO_LOGISTIC) {
                $order->setState(Order::STATE_SENT_TO_LOGISTIC);
                $order->setStatus(Order::STATE_SENT_TO_LOGISTIC);
                $saveOrder = true;
            }

            $websiteId = $order->getStore()->getWebsite()->getWebsiteId();
            $request = ['state' => 'SENTTOLOGISTIC'];
            $wmsResponse = $this->wmsConnector->request(
                '/api/web_orders/'.$wmsId.'/state',
                $websiteId,
                $request,
                'PUT'
            );

            if (!$wmsResponse || isset($wmsResponse->error)) {
                $message = $wmsResponse->error->message ?? 'See log for more information';
                $order->addStatusHistoryComment($message)
                    ->setIsCustomerNotified(false);
                $saveOrder = true;
            }

            if ($saveOrder) {
                //phpcs:ignore Ecg.Performance.Loop.ModelLSD
                $this->magentoOrderRepository->save($order);
            }
        } catch (\Throwable $e) {
            $message = __('Error during send informations to wms : %1', $e->getMessage());
            $order->addStatusHistoryComment($message)
                ->setIsCustomerNotified(false);
            //phpcs:ignore Ecg.Performance.Loop.ModelLSD
            $this->magentoOrderRepository->save($order);
        }
    }

    /**
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
     * @param array $orderEntityId
     * @param array $orderEntityIdSentToLogistic
     */
    private function otherInstanceOrderProcess(
        AdapterInterface $connection,
        array $orderEntityId,
        array $orderEntityIdSentToLogistic
    ) {
        try {
            foreach ($orderEntityIdSentToLogistic as $entityId => $wmsId) {
                $order = $this->orderFactory->create()->load($entityId);
                $is_preorder = false;
                if ($this->environmentManager->getEnvironment() == Environment::EU) {
                    foreach ($order->getAllVisibleItems() as $item) {
                        if ($item->getIsPreorder()) {
                            $is_preorder = true;
                            break;
                        }
                    }
                }
                if(!$is_preorder){
                $order->setState(Order::STATE_SENT_TO_LOGISTIC);
                $order->setStatus(Order::STATE_SENT_TO_LOGISTIC);
                $order->addStatusHistoryComment('Sent to logistic')
                    ->setIsCustomerNotified(false);

                $this->magentoOrderRepository->save($order);
                }
            }

            $this->wmsErrorRepository->deleteMultiple(
                array_keys($orderEntityId),
                WmsError::ORDER_TYPE,
                WmsError::CREATE_ORDER_API_METHOD
            );
        } catch (\Exception $e) {
            $orderEntityId = implode(',', array_keys($orderEntityId));

            $errorMsg[] = 'Order entity_id : '.$orderEntityId;
            $errorMsg[] = 'Type : '.WmsError::ORDER_TYPE;
            $errorMsg[] = 'API Method : '.WmsError::CREATE_ORDER_API_METHOD;
            $errorMsg[] = 'Message : '.$e->getMessage();

            $this->logger->addError(implode(' ', $errorMsg));
            $connection->rollBack();
        }
    }

    /**
     * @param array $error
     * @param array $params
     * @return mixed|void
     * @throws \Magento\Framework\Exception\MailException
     */
    protected function processError(array $error, array $params)
    {
        foreach ($error as $responseLine) {
            $arrayOrderInError[$responseLine->entity_id] = $responseLine->message;
        }

        $wmsErrorCollectionFactory = $this->wmsErrorCollectionFactory->create();
        $wmsErrorCollectionFactory->addFieldToFilter('entity_id', ['in' => array_keys($arrayOrderInError)]);
        $wmsErrorCollectionFactory->addFieldToFilter('entity_type', WmsError::ORDER_TYPE);
        $wmsErrorCollectionFactory->addFieldToFilter('api_method', WmsError::CREATE_ORDER_API_METHOD);
        $wmsErrorCollectionFactory->load();

        $entityIdToIncrementNbError = [];
        $entityIdToSendMail         = [];
        foreach ($wmsErrorCollectionFactory->getItems() as $wmsError) {
            $orderEntityId = $wmsError->getEntityId();

            $nbError = $wmsError->getNbErrorWms();
            if ($nbError >= self::NB_MAX_ERROR_WMS && !$wmsError->isMailSent()) {
                $entityIdToSendMail[$orderEntityId] = 'ID : '.$orderEntityId.' => '.$wmsError->getLastErrorMsgWms();
            } else {
                $entityIdToIncrementNbError[$orderEntityId] = [
                    'last_error_msg_wms' => $arrayOrderInError[$orderEntityId],
                    'nb_error_wms' => $nbError
                ];
            }
        }

        $orderIdFirstError = array_diff_key($arrayOrderInError, $entityIdToIncrementNbError, $entityIdToSendMail);

        if (count($entityIdToSendMail) > 0) {
            $this->sendEmail($entityIdToSendMail, WmsError::ORDER_TYPE, WmsError::CREATE_ORDER_API_METHOD);

            $this->addErrorOnOrder($entityIdToSendMail);

            $bind = ['mail_sent' => 1];
            $this->wmsErrorRepository->update(
                array_keys($entityIdToSendMail),
                $bind,
                WmsError::ORDER_TYPE,
                WmsError::CREATE_ORDER_API_METHOD
            );
        }

        if (count($orderIdFirstError) > 0) {
            $this->saveErrorInDb($orderIdFirstError, WmsError::ORDER_TYPE, WmsError::CREATE_ORDER_API_METHOD, null);
        }

        if (count($entityIdToIncrementNbError) > 0) {
            $this->incrementNbError(
                $entityIdToIncrementNbError,
                WmsError::ORDER_TYPE,
                WmsError::CREATE_ORDER_API_METHOD
            );

            foreach ($entityIdToIncrementNbError as $orderEntityId => $data) {
                $entityIdToAddComment[$orderEntityId] = 'ID : '.$orderEntityId.' => '.$data['last_error_msg_wms'];
            }

            $this->addErrorOnOrder($entityIdToAddComment);
        }
    }

    /**
     * @param array $orderEntityId
     */
    public function addErrorOnOrder(array $orderEntityId)
    {
        $dateTime = $this->dateFactory->create();
        foreach ($orderEntityId as $entityId => $data) {
            //phpcs:ignore Ecg.Performance.Loop.ModelLSD
            $order   = $this->orderFactory->create()->load($entityId);
            $comments[] = [
                'comment' => __('An error occurred when sending order in the WMS: %1', $data),
                'parent_id' => $entityId,
                'is_customer_notified' => null,
                'is_visible_on_front' => 0,
                'status' => $order->getStatus(),
                'created_at' => $dateTime->gmtDate(),
                'entity_name' => 'order'
            ];
        }

        $this->addCommentOnOrder($comments);
    }
}
