<?php

namespace Project\Wms\Model\Action\Sales\Rma\Update;

use Magento\Rma\Model\Rma\Source\Status;
use Magento\Store\Model\StoreManager;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Rma\Model\ResourceModel\Rma\CollectionFactory as RmaCollectionFactory;
use Synolia\Sync\Console\ConsoleOutput;

/**
 * Class ExportUpdateInfo
 * @package Project\Wms\Model\Action\Sales\Rma
 * @author Synolia <contact@synolia.com>
 */
class Export implements ActionInterface
{
    /**
     * @var RmaCollectionFactory
     */
    protected $rmaCollectionFactory;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Export constructor.
     * @param \Magento\Rma\Model\ResourceModel\Rma\CollectionFactory $rmaCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        RmaCollectionFactory $rmaCollectionFactory,
        State $state,
        StoreManager $storeManager
    ) {
        $this->rmaCollectionFactory = $rmaCollectionFactory;
        $this->storeManager = $storeManager;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param                                     $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $status = [
            Status::STATE_APPROVED,
            Status::STATE_PROCESSED_CLOSED,
            Status::STATE_CLOSED,
            Status::STATE_REJECTED
        ];

        $rmaCollection = $this->rmaCollectionFactory->create();
        $rmaCollection->addFieldToFilter('status', ['in', $status]);
        $rmaCollection->addFieldToFilter('wms_exported', '0');

        if (isset($params['website'])) {
            $website = $this->storeManager->getWebsite($params['website']);
            $stores = $this->storeManager->getStoreByWebsiteId($website->getId());

            $rmaCollection->addFieldToFilter('store_id', ['in' => $stores]);
        }

        $rmaCollection->load();

        $dataToExport = [];
        foreach ($rmaCollection->getItems() as $rma) {
            $data = [
                'id_wms' => (int)$rma->getWmsId(),
                'order_status' => $rma->getOrder()->getStatus(),
                'rma_status' => $rma->getStatus(),
                'allocation' => $rma->getStockCode()
            ];

            if ($rma->getStatus() == Status::STATE_PROCESSED_CLOSED) {
                $data['back_to_stock'] = $rma->getBackToStock();
            }

            $dataToExport[] = $data;
        }

        return $dataToExport;
    }
}
