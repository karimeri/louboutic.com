<?php

namespace Project\Wms\Model\Action;

use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Project\Sales\Model\Magento\Sales\OrderRepository;
use Project\Wms\Api\Data\WmsErrorInterface;
use Project\Wms\Model\Service\Mail;
use Project\Wms\Model\WmsErrorRepository;
use Synolia\Sync\Helper\Config as SyncConfigHelper;

/**
 * Class ResponseProcessing
 * @package Project\Wms\Model\Action\Sales\Order
 * @author  Synolia <contact@synolia.com>
 */
abstract class AbstractResponseProcessing
{
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var SyncConfigHelper
     */
    protected $syncConfigHelper;

    /**
     * @var WmsErrorRepository
     */
    protected $wmsErrorRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var DateTimeFactory
     */
    protected $dateFactory;

    /**
     * @var Mail
     */
    protected $mailService;

    /**
     * AbstractResponseProcessing constructor.
     * @param TransportBuilder   $transportBuilder
     * @param SyncConfigHelper   $syncConfigHelper
     * @param WmsErrorRepository $wmsErrorRepository
     * @param OrderRepository    $orderRepository
     * @param DateTimeFactory    $dateFactory
     * @param Mail               $mailService
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        SyncConfigHelper $syncConfigHelper,
        WmsErrorRepository $wmsErrorRepository,
        OrderRepository $orderRepository,
        DateTimeFactory $dateFactory,
        Mail $mailService
    ) {
        $this->transportBuilder   = $transportBuilder;
        $this->syncConfigHelper   = $syncConfigHelper;
        $this->wmsErrorRepository = $wmsErrorRepository;
        $this->orderRepository    = $orderRepository;
        $this->dateFactory        = $dateFactory;
        $this->mailService        = $mailService;
    }

    /**
     * @param array $success
     */
    abstract protected function processSuccess(array $success);

    /**
     * @param array $error
     * @param array $params
     * @return mixed
     */
    abstract protected function processError(array $error, array $params);

    /**
     * @param array $comments
     */
    protected function addCommentOnOrder(array $comments)
    {
        $this->orderRepository->addCommentOnMultipleOrder($comments);
    }

    /**
     * @param array $entityInError
     * @param       $entityType
     * @param       $apiMethod
     * @throws \Magento\Framework\Exception\MailException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function sendEmail(array $entityInError, $entityType, $apiMethod)
    {
        $this->mailService->sendEmail($entityInError, $entityType, $apiMethod);
    }

    /**
     * @param array  $entityInError
     * @param string $entityType
     * @param string $apiMethod
     */
    protected function incrementNbError(array $entityInError, $entityType, $apiMethod)
    {
        foreach ($entityInError as $entityId => $data) {
            $wmsError[$entityId] = [
                'nb_error_wms' => $data['nb_error_wms'] + 1,
                'last_error_msg_wms' => $data['last_error_msg_wms']
            ];
        }

        $this->wmsErrorRepository->updateMultiple($wmsError, $entityType, $apiMethod);
    }

    /**
     * @param array  $entityInError
     * @param string $entityType
     * @param string $apiMethod
     * @param string $wmsId
     */
    protected function saveErrorInDb(array $entityInError, $entityType, $apiMethod, $wmsId)
    {
        foreach ($entityInError as $entityId => $message) {
            $wmsError[] = [
                WmsErrorInterface::ENTITY_ID => $entityId,
                WmsErrorInterface::WMS_ID => $wmsId,
                WmsErrorInterface::NB_ERROR_WMS => 1,
                WmsErrorInterface::LAST_ERROR_MSG_WMS => $message,
                WmsErrorInterface::ENTITY_TYPE => $entityType,
                WmsErrorInterface::API_METHOD => $apiMethod,
            ];
        }

        $this->wmsErrorRepository->saveMultiple($wmsError);
    }
}
