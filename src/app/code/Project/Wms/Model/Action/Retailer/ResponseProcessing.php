<?php

namespace Project\Wms\Model\Action\Retailer;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Store\Model\Store;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Synolia\Sync\Model\Action\ActionInterface;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Helper\Config as SyncConfigHelper;
use Synolia\Retailer\Model\RetailerFactory;
use Synolia\Retailer\Api\RetailerRepositoryInterface;
use Synolia\Retailer\Model\RetailerPermissionFactory;
use Synolia\Retailer\Api\RetailerPermissionRepositoryInterface;
use Synolia\Retailer\Model\RetailerScheduleFactory;
use Synolia\Retailer\Api\RetailerScheduleRepositoryInterface;
use Synolia\Retailer\Model\RetailerTranslationFactory;
use Synolia\Retailer\Api\RetailerTranslationRepositoryInterface;
use Synolia\Retailer\Model\ResourceModel\RetailerSchedule\CollectionFactory as CollectionScheduleFactory;
use Project\Wms\Model\Action\AbstractResponseProcessing;
use Project\Sales\Model\Magento\Sales\OrderRepository;
use Project\Wms\Model\Service\Mail;
use Project\Wms\Model\WmsErrorRepository;

/**
 * Class ResponseProcessing
 * @package Project\Wms\Model\Action\Retailer
 * @author  Synolia <contact@synolia.com>
 */
class ResponseProcessing extends AbstractResponseProcessing implements ActionInterface
{
    /**
     * @var RetailerFactory
     */
    protected $retailerFactory;

    /**
     * @var RetailerRepositoryInterface
     */
    protected $retailerRepository;

    /**
     * @var RetailerPermissionRepositoryInterface
     */
    protected $retailerPermissionRepository;

    /**
     * @var RetailerPermissionFactory
     */
    protected $retailerPermissionFactory;

    /**
     * @var RetailerScheduleRepositoryInterface
     */
    protected $retailerScheduleRepository;

    /**
     * @var RetailerScheduleFactory
     */
    protected $retailerScheduleFactory;

    /**
     * @var RetailerTranslationRepositoryInterface
     */
    protected $retailerTranslationRepository;

    /**
     * @var RetailerTranslationFactory
     */
    protected $retailerTranslationFactory;

    /**
     * @var CollectionScheduleFactory
     */
    protected $collectionScheduleFactory;

    /**
     * @var array
     */
    protected $messages = [];

    /**
     * ResponseProcessing constructor.
     * @param TransportBuilder $transportBuilder
     * @param SyncConfigHelper $syncConfigHelper
     * @param WmsErrorRepository $wmsErrorRepository
     * @param OrderRepository $orderRepository
     * @param DateTimeFactory $dateFactory
     * @param Mail $mailService
     * @param RetailerFactory $retailerFactory
     * @param RetailerRepositoryInterface $retailerRepository
     * @param RetailerPermissionRepositoryInterface $retailerPermissionRepository
     * @param RetailerPermissionFactory $retailerPermissionFactory
     * @param RetailerScheduleRepositoryInterface $retailerScheduleRepository
     * @param RetailerScheduleFactory $retailerScheduleFactory
     * @param CollectionScheduleFactory $collectionScheduleFactory
     * @param RetailerTranslationRepositoryInterface $retailerTranslationRepository
     * @param RetailerTranslationFactory $retailerTranslationFactory
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        SyncConfigHelper $syncConfigHelper,
        WmsErrorRepository $wmsErrorRepository,
        OrderRepository $orderRepository,
        DateTimeFactory $dateFactory,
        Mail $mailService,
        RetailerFactory $retailerFactory,
        RetailerRepositoryInterface $retailerRepository,
        RetailerPermissionRepositoryInterface $retailerPermissionRepository,
        RetailerPermissionFactory $retailerPermissionFactory,
        RetailerScheduleRepositoryInterface $retailerScheduleRepository,
        RetailerScheduleFactory $retailerScheduleFactory,
        CollectionScheduleFactory $collectionScheduleFactory,
        RetailerTranslationRepositoryInterface $retailerTranslationRepository,
        RetailerTranslationFactory $retailerTranslationFactory,
        State $state
    ) {
        parent::__construct(
            $transportBuilder,
            $syncConfigHelper,
            $wmsErrorRepository,
            $orderRepository,
            $dateFactory,
            $mailService
        );

        $this->retailerFactory = $retailerFactory;
        $this->retailerRepository = $retailerRepository;
        $this->retailerPermissionRepository = $retailerPermissionRepository;
        $this->retailerPermissionFactory = $retailerPermissionFactory;
        $this->retailerTranslationRepository = $retailerTranslationRepository;
        $this->retailerTranslationFactory = $retailerTranslationFactory;
        $this->collectionScheduleFactory = $collectionScheduleFactory;
        $this->retailerScheduleFactory = $retailerScheduleFactory;
        $this->retailerScheduleRepository = $retailerScheduleRepository;

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|void
     * @throws \Exception
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {

        if (!empty($data) && $data[0] != false) {
            $this->processSuccess($data);
        } else {
            $this->processError($data, $params);
        }
    }

    /**
     * @param array $success
     */
    protected function processSuccess(array $success)
    {
        $data = [];
        foreach ($success as $retailer) {
            $data[] = [
                'code'      => $retailer->codeEstablishment,
                'email'     => $retailer->address->email,
                'continent' => mb_strtolower($retailer->address->country->continent),
                'city'      => $retailer->address->city,
                'zip_code'  => $retailer->address->postcode,
                'country'   => strtoupper($retailer->address->country->iso2),
                'position'  => 0,
                'enabled'   => 0,
                // Permission
                'display_schedule'   => 1,
                'display_email'      => 1,
                'display_phone'      => 1,
                'display_street'     => 1,
                'display_street_bis' => 1,
                'display_zip_code'   => 1,
                'display_city'       => 1,
                // Translation
                'name'       => $retailer->name,
                'street'     => $retailer->address->address1,
                'street_bis' => $retailer->address->address2 . " " . $retailer->address->address3,
            ];
        }

        $this->save($data);
    }

    /**
     * @param array $error
     * @param array $param
     * @return mixed|void
     * @throws \Exception
     */
    protected function processError(array $error, array $param)
    {
        throw new \Exception("Nothing to import, if you think it's an error please contact your administrator");
    }

    /**
     * @param $data
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     */
    protected function save($data)
    {
        foreach ($data as $retailer) {
            $code = $retailer['code'];
            $storeId = Store::DEFAULT_STORE_ID;
            // Save Retailer
            try {
                $this->retailerRepository->getByCode($code);
                continue;
            } catch (NoSuchEntityException $exception) {
                $model = $this->retailerFactory->create();
            }

            if (!is_null($model->getData())) {
                $retailer['retailer_id'] = $model->getRetailerId();
            }

            $model->setData($retailer);
            try {
                $this->retailerRepository->save($model);

                if (is_null($retailer['retailer_id'])) {
                    $retailer['retailer_id'] = $model->getId();
                }

                $this->setPermission($retailer['retailer_id'], $retailer);
                $this->setSchedule($retailer['retailer_id'], $retailer);
                $this->setTranslation($retailer['retailer_id'], $storeId, $retailer);

                $this->addMessage('Success retailer ' . $retailer['retailer_id']);
            } catch (LocalizedException $localizedException) {
                $response = $localizedException->getMessage();
                $this->addMessage(json_encode($response) . ' retailer ' . $retailer['retailer_id']);
            } catch (\Exception $exception) {
                $response = $exception->getMessage();
                $this->addMessage(json_encode($response) . ' retailer ' . $retailer['retailer_id']);
            }
        }
    }

    /**
     * @param $retailerId
     * @param $data
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function setPermission($retailerId, $data)
    {
        try {
            $permission = $this->retailerPermissionRepository->getByRetailerId($retailerId);
            $data['permission_id'] = $permission->getPermissionId();
        } catch (NoSuchEntityException $exception) {
            $permission = $this->retailerPermissionFactory->create();
        }

        $permission->setData($data);
        $this->retailerPermissionRepository->save($permission);
    }

    /**
     * @param $retailerId
     * @param $storeId
     * @param $data
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function setTranslation($retailerId, $storeId, $data)
    {
        try {
            $translation = $this->retailerTranslationRepository
                ->getByRetailerIdAndStoreId($retailerId, $storeId);
        } catch (NoSuchEntityException $exception) {
            $translation = $this->retailerTranslationFactory->create();
            $translation->setRetailerId($retailerId);
        }

        $translation->addData($data);
        $this->retailerTranslationRepository->save($translation);
    }

    /**
     * @param $retailerId
     * @param $data
     * @throws \Magento\Framework\Exception\LocalizedException
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     */
    private function setSchedule($retailerId, $data)
    {
        $this->collectionScheduleFactory->create()
            ->addFieldToFilter('retailer_id', ['eq' => $retailerId])
            ->walk('delete');

        if (!empty($data['schedules'])) {
            foreach ($data['schedules'] as $schedules) {
                $schedule = $this->retailerScheduleFactory->create();
                $schedule->setRetailerId($retailerId);
                $schedule->addData($schedules);
                $this->retailerScheduleRepository->save($schedule);
            }
        }
    }

    private function addMessage($message)
    {
        $this->messages[] = $message;
        return $this;
    }
}
