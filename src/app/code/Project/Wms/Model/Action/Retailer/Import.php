<?php

namespace Project\Wms\Model\Action\Retailer;

use Project\Wms\Helper\Config;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class Import
 * @package Project\Wms\Model\Action\Retailer
 * @author  Synolia <contact@synolia.com>
 */
class Import implements ActionInterface
{
    /**
     * @var Config
     */
    protected $wmsHelper;

    /**
    * Import constructor.
    * @param Config $wmsHelper
    */
    public function __construct(
        Config $wmsHelper
    ) {
        $this->wmsHelper = $wmsHelper;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array
     * @throws \Exception
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {

        if ($this->wmsHelper->isMaster()) {
            $data = [
                'type' => 'SHOP',
                '_pagination' => 'false',
            ];
        } else {
            throw new \Exception("Not authorized on this environment");
        }

        return $data;
    }
}
