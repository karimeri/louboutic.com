<?php

namespace Project\Wms\Model\Action;

use Zend\Http\Request;
use Project\Wms\Model\Api\Connector;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class CallApi
 * @package Project\Wms\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class CallApi implements ActionInterface
{
    /**
     * @var Connector
     */
    protected $wmsConnector;

    /**
     * CallApi constructor.
     * @param Connector $wmsConnector
     */
    public function __construct(
        Connector $wmsConnector
    ) {
        $this->wmsConnector = $wmsConnector;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $response = false;
        $method = Request::METHOD_POST;

        if (isset($params['method'])) {
            $method = $params['method'];
        }

        $website = null;
        if (isset($params['website'])) {
            $website = $params['website'];
        }

        if (count($data)) {
            if ($method == Request::METHOD_GET) {
                $response = $this->wmsConnector->getRequest($params['api_path'], $website, $data);
            } else {
                $response = $this->wmsConnector->request($params['api_path'], $website, $data, $method);
            }
        }

        return $response;
    }
}
