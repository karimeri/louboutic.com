<?php

namespace Project\Wms\Model\Action;

use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Project\Sales\Model\Magento\Sales\OrderRepository;
use Project\Wms\Model\ResourceModel\WmsError\CollectionFactory as WmsErrorCollectionFactory;
use Project\Wms\Model\Service\Mail;
use Project\Wms\Model\WmsErrorRepository;
use Synolia\Logger\Model\Logger;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Helper\Config as SyncConfigHelper;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class ResponseProcessing
 * @package Project\Wms\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class ResponseProcessing extends AbstractResponseProcessing implements ActionInterface
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var WmsErrorCollectionFactory
     */
    protected $wmsErrorCollectionFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var OrderInterfaceFactory
     */
    protected $orderFactory;

    /**
     * ResponseProcessing constructor.
     * @param TransportBuilder $transportBuilder
     * @param SyncConfigHelper $syncConfigHelper
     * @param WmsErrorRepository $wmsErrorRepository
     * @param OrderRepository $orderRepository
     * @param DateTimeFactory $dateFactory
     * @param Mail $mailService
     * @param ResourceConnection $resourceConnection
     * @param WmsErrorCollectionFactory $wmsErrorCollectionFactory
     * @param LoggerFactory $loggerFactory
     * @param DirectoryList $directoryList
     * @param State $state
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        SyncConfigHelper $syncConfigHelper,
        WmsErrorRepository $wmsErrorRepository,
        OrderRepository $orderRepository,
        DateTimeFactory $dateFactory,
        Mail $mailService,
        ResourceConnection $resourceConnection,
        WmsErrorCollectionFactory $wmsErrorCollectionFactory,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        State $state
    ) {
        parent::__construct(
            $transportBuilder,
            $syncConfigHelper,
            $wmsErrorRepository,
            $orderRepository,
            $dateFactory,
            $mailService
        );

        $this->resourceConnection        = $resourceConnection;
        $this->wmsErrorCollectionFactory = $wmsErrorCollectionFactory;

        $logPath      = $directoryList->getPath('log');
        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $logPath.DIRECTORY_SEPARATOR.WmsErrorRepository::WMS_ERROR_LOG_FILENAME]
        );

        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }
    }

    /**
     * @param array         $params
     * @param array         $data
     * @param string        $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $apiResponse             = $data[0];
        $orderIncrementIdSuccess = [];
        if ($apiResponse) {
            if (isset($apiResponse->success) && count($apiResponse->success)) {
                $orderIncrementIdSuccess = $this->processSuccess($apiResponse->success);
            }
            if (isset($apiResponse->error) && count($apiResponse->error)) {
                $this->processError($apiResponse->error, $params);
            }
        }

        return $orderIncrementIdSuccess;
    }

    /**
     * @param array $success
     * @return array
     */
    protected function processSuccess(array $success)
    {
        foreach ($success as $responseLine) {
            $incrementId = $responseLine->increment_id;
            $orderIncrementId[] = [
                'increment_id' => $incrementId
            ];
        }

        return $orderIncrementId;
    }

    /**
     * @param array $error
     * @param array $params
     * @throws \Magento\Framework\Exception\MailException
     */
    protected function processError(array $error, array $params)
    {
        $entityIdToSendMail = null;

        foreach ($error as $responseLine) {
            $message = 'ID : '.$responseLine->increment_id.' => '.$responseLine->message;
            $entityIdToSendMail[$responseLine->entity_id] = $message;
        }

        if (!is_null($entityIdToSendMail)) {
            $this->sendEmail($entityIdToSendMail, $params['entity_type'], $params['api_method']);

            $dateTime = $this->dateFactory->create();
            foreach ($entityIdToSendMail as $entityId => $data) {
                $comments[] = [
                    'comment' => __('An error occurred when sending informations in the WMS: %1', $data),
                    'parent_id' => $entityId,
                    'is_customer_notified' => null,
                    'is_visible_on_front' => 0,
                    'status' => $params['order_status'],
                    'created_at' => $dateTime->gmtDate(),
                    'entity_name' => 'order'
                ];
            }

            $this->addCommentOnOrder($comments);
        }
    }
}
