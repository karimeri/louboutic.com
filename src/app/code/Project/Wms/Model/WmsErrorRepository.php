<?php

namespace Project\Wms\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\DirectoryList;
use Project\Wms\Model\ResourceModel\WmsError as WmsErrorResource;
use Synolia\Logger\Model\Logger;
use Synolia\Logger\Model\LoggerFactory;

/**
 * Class WmsErrorRepository
 * @package Project\Wms\Model
 * @author  Synolia <contact@synolia.com>
 */
class WmsErrorRepository
{
    const WMS_ERROR_LOG_FILENAME = 'wms_api.log';
    /**
     * @var WmsErrorResource
     */
    protected $wmsErrorResource;

    /**
     * @var WmsErrorFactory
     */
    protected $wmsErrorFactory;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * WmsErrorRepository constructor.
     * @param WmsErrorResource   $wmsErrorResource
     * @param WmsErrorFactory    $wmsErrorFactory
     * @param ResourceConnection $resourceConnection
     * @param LoggerFactory      $loggerFactory
     * @param DirectoryList      $directoryList
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(
        WmsErrorResource $wmsErrorResource,
        WmsErrorFactory $wmsErrorFactory,
        ResourceConnection $resourceConnection,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList
    ) {
        $this->wmsErrorResource   = $wmsErrorResource;
        $this->wmsErrorFactory    = $wmsErrorFactory;
        $this->resourceConnection = $resourceConnection;

        $this->logger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log').DIRECTORY_SEPARATOR.self::WMS_ERROR_LOG_FILENAME]
        );
    }

    /**
     * @param WmsError $wmsError
     */
    public function save(WmsError $wmsError)
    {
        $this->wmsErrorResource->save($wmsError);
    }

    /**
     * @param WmsError $wmsError
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(WmsError $wmsError)
    {
        try {
            $this->wmsErrorResource->delete($wmsError);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the wms error: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * @param int $wmsErrorId
     * @return bool
     */
    public function deleteById($wmsErrorId)
    {
        return $this->delete($this->getById($wmsErrorId));
    }

    /**
     * @param int $id
     * @return WmsError
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $wmsError = $this->wmsErrorFactory->create();
        $this->wmsErrorResource->load($wmsError, $id);
        if (!$wmsError->getErrorId()) {
            throw new NoSuchEntityException(__('Wms error with id %1 does not exist.', $id));
        }

        return $wmsError;
    }

    /**
     * @param array  $wmsError
     * @param array  $bind
     * @param string $entityType
     * @param string $apiMethod
     */
    public function update(array $wmsError, array $bind, $entityType, $apiMethod)
    {
        $where = [
            'entity_id IN (?)' => $wmsError,
            'entity_type = ?' => $entityType,
            'api_method = ?' => $apiMethod
        ];

        $this->resourceConnection->getConnection()->update(WmsErrorResource::TABLE_NAME, $bind, $where);
    }

    /**
     * @param array  $wmsError
     * @param string $entityType
     * @param string $apiMethod
     */
    public function deleteMultiple(array $wmsError, $entityType, $apiMethod)
    {
        $where = [
            'entity_id IN (?)' => $wmsError,
            'entity_type = ?' => $entityType,
            'api_method = ?' => $apiMethod
        ];

        $this->resourceConnection->getConnection()->delete(WmsErrorResource::TABLE_NAME, $where);
    }

    /**
     * @param array $wmsError
     */
    public function saveMultiple(array $wmsError)
    {
        $this->resourceConnection->getConnection()->insertMultiple(WmsErrorResource::TABLE_NAME, $wmsError);
    }

    /**
     * @param array  $wmsError
     * @param string $entityType
     * @param string $apiMethod
     */
    public function updateMultiple(array $wmsError, $entityType, $apiMethod)
    {
        $connection = $this->resourceConnection->getConnection();

        $nbErrorConditions = $this->getCaseConditions($wmsError, 'nb_error_wms');
        $messageConditions = $this->getCaseConditions($wmsError, 'last_error_msg_wms');

        $valueNbError = $connection->getCaseSql('entity_id', $nbErrorConditions, 'nb_error_wms');
        $valueMessage = $connection->getCaseSql('entity_id', $messageConditions, 'last_error_msg_wms');

        $where = [
            'entity_id IN (?)' => array_keys($wmsError),
            'entity_type = ?' => $entityType,
            'api_method = ?' => $apiMethod
        ];

        try {
            $connection->beginTransaction();
            $connection->update(
                WmsErrorResource::TABLE_NAME,
                [
                    'nb_error_wms' => $valueNbError,
                    'last_error_msg_wms' => $valueMessage
                ],
                $where
            );
            $connection->commit();
        } catch (\Exception $e) {
            $entityId = implode(',', array_keys($wmsError));

            $errorMsg[] = 'Entity_id : '.$entityId;
            $errorMsg[] = 'Type : '.$entityType;
            $errorMsg[] = 'API Method : '.$apiMethod;
            $errorMsg[] = 'Message : '.$e->getMessage();

            $this->logger->addError(implode(' ', $errorMsg));

            $connection->rollBack();
        }
    }

    /**
     * @param array  $wmsError
     * @param string $field
     * @return array
     */
    private function getCaseConditions(array $wmsError, $field)
    {
        $connection = $this->resourceConnection->getConnection();

        $conditions = [];
        foreach ($wmsError as $entityId => $data) {
            $case              = $connection->quoteInto('?', $entityId);
            $result            = $connection->quoteInto('?', $data[$field]);
            $conditions[$case] = $result;
        }

        return $conditions;
    }
}
