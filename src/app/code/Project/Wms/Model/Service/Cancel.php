<?php

namespace Project\Wms\Model\Service;

use Magento\Sales\Model\Order;
use Project\Wms\Model\Api\Connector;
use Project\Wms\Model\WmsError;

/**
 * Class Cancel
 * @package Project\Wms\Model\Service
 * @author Synolia <contact@synolia.com>
 */
class Cancel
{
    const BASE_API_PATH = '/api/web_orders/';

    /**
     * @var \Project\Wms\Model\Api\Connector
     */
    protected $wmsConnector;

    /**
     * @var \Project\Wms\Model\Service\Log
     */
    protected $logService;

    /**
     * Cancel constructor.
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     * @param \Project\Wms\Model\Service\Log $logService
     */
    public function __construct(
        Connector $wmsConnector,
        Log $logService
    ) {
        $this->wmsConnector = $wmsConnector;
        $this->logService = $logService;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @throws \Magento\Framework\Exception\MailException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function sendCancellationToWms(Order $order)
    {
        $request = [
            'state' => 'CANCELLED'
        ];

        $wmsId = $order->getWmsId();
        $fullApiPath = self::BASE_API_PATH.$wmsId.'/state';

        $websiteId = $order->getStore()->getWebsite()->getWebsiteId();
        $wmsResponse = $this->wmsConnector->request($fullApiPath, $websiteId, $request, 'PUT');

        if (!$wmsResponse || isset($wmsResponse->error)) {
            $message = $wmsResponse->error->message ?? 'See log for more information';
            $this->logService->logError($message, $order, WmsError::CANCEL_ORDER_API_METHOD);
        }
    }
}
