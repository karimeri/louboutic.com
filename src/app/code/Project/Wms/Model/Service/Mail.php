<?php

namespace Project\Wms\Model\Service;

use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Mail\Template\TransportBuilder;
use Project\Sales\Model\Magento\Sales\OrderRepository;
use Project\Wms\Model\WmsErrorRepository;
use Synolia\Sync\Helper\Config as SyncConfigHelper;

/**
 * Class Mail
 * @package Project\Wms\Model\Service
 * @author  Synolia <contact@synolia.com>
 */
class Mail
{
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var SyncConfigHelper
     */
    protected $syncConfigHelper;

    /**
     * @var WmsErrorRepository
     */
    protected $wmsErrorRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var DateTimeFactory
     */
    protected $dateFactory;

    /**
     * AbstractResponseProcessing constructor.
     * @param TransportBuilder   $transportBuilder
     * @param SyncConfigHelper   $syncConfigHelper
     * @param WmsErrorRepository $wmsErrorRepository
     * @param OrderRepository    $orderRepository
     * @param DateTimeFactory    $dateFactory
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        SyncConfigHelper $syncConfigHelper,
        WmsErrorRepository $wmsErrorRepository,
        OrderRepository $orderRepository,
        DateTimeFactory $dateFactory
    ) {
        $this->transportBuilder   = $transportBuilder;
        $this->syncConfigHelper   = $syncConfigHelper;
        $this->wmsErrorRepository = $wmsErrorRepository;
        $this->orderRepository    = $orderRepository;
        $this->dateFactory        = $dateFactory;
    }

    /**
     * @param array $entityInError
     * @param       $entityType
     * @param       $apiMethod
     * @throws \Magento\Framework\Exception\MailException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function sendEmail(array $entityInError, $entityType, $apiMethod)
    {
        $transport = $this->transportBuilder->setTemplateIdentifier($this->syncConfigHelper->getEmailErrorTemplate())
            ->setTemplateOptions(['area' => 'adminhtml', 'store' => '0'])
            ->setTemplateVars([
                'exception_message' => __(
                    'API method : '.$apiMethod
                    .' Synchronization with the WMS failed for the following '.$entityType.'(s) : %1',
                    implode(', ', $entityInError)
                ),
                'exception_class' => ''
            ])
            ->setFrom([
                'email' => $this->syncConfigHelper->getEmailFrom(),
                'name' => $this->syncConfigHelper->getEmailFromName()
            ]);

        $emails = $this->syncConfigHelper->getEmailList();
        $transport->addTo(explode(',', $emails['error_api_wms']));

        $transport->getTransport()->sendMessage();
    }
}
