<?php

namespace Project\Wms\Model\Service;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Project\Wms\Model\WmsError;

/**
 * Class Log
 * @package Project\Wms\Observer
 * @author Synolia <contact@synolia.com>
 */
class Log
{
    /**
     * @var Mail
     */
    protected $mailService;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var OrderStatusHistoryRepositoryInterface
     */
    private $historyRepository;


    /**
     * Log constructor.
     * @param \Project\Wms\Model\Service\Mail $mailService
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param OrderStatusHistoryRepositoryInterface $historyRepository
     */
    public function __construct(
        Mail $mailService,
        OrderRepository $orderRepository,
        OrderStatusHistoryRepositoryInterface $historyRepository
    ) {
        $this->mailService     = $mailService;
        $this->orderRepository = $orderRepository;
        $this->historyRepository = $historyRepository;
    }

    /**
     * @param string $message
     * @param \Magento\Sales\Model\Order $order
     * @param  string $apiMethod
     * @throws \Magento\Framework\Exception\MailException
     */
    public function logError($message, Order $order, $apiMethod)
    {
        $orderId                      = $order->getId();
        $entityIdToSendMail[$orderId] = 'ID : '.$order->getIncrementId().' => '.$message;
        $this->mailService->sendEmail(
            $entityIdToSendMail,
            WmsError::ORDER_TYPE,
            $apiMethod
        );

        $comment = $order->addCommentToStatusHistory(
            __('An error occurred when sending informations to the WMS: %1', $message)
        )
            ->setIsCustomerNotified(false);
        $this->historyRepository->save($comment);
    }
}
