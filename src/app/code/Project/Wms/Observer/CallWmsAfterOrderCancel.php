<?php

namespace Project\Wms\Observer;

use Magento\Framework\App\Area;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;
use Project\EmailSent\Helper\Config;
use Project\Sales\Model\Magento\Sales\Order;
use Project\Wms\Model\Service\Log;
use Project\Wms\Model\Api\Connector;
use Project\Wms\Model\Service\Cancel;
use Project\Wms\Model\WmsError;
use Magento\Framework\Mail\Template\TransportBuilder;

/**
 * Class CallWmsAfterOrderCancel
 * @package Project\Wms\Observer
 * @author  Synolia <contact@synolia.com>
 */
class CallWmsAfterOrderCancel implements ObserverInterface
{
    /**
     * @var Connector
     */
    protected $wmsConnector;

    /**
     * @var Config
     */
    protected $emailConfigHelper;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Project\Wms\Model\Service\Log
     */
    protected $logService;

    /**
     * @var \Project\Wms\Model\Service\Cancel
     */
    protected $cancelService;

    /**
     * CallWmsAfterOrderCancel constructor.
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     * @param \Project\EmailSent\Helper\Config $emailConfigHelper
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Project\Wms\Model\Service\Log $logService
     * @param \Project\Wms\Model\Service\Cancel $cancelService
     */
    public function __construct(
        Connector $wmsConnector,
        Config $emailConfigHelper,
        TransportBuilder $transportBuilder,
        Log $logService,
        Cancel $cancelService
    ) {
        $this->wmsConnector = $wmsConnector;
        $this->emailConfigHelper = $emailConfigHelper;
        $this->transportBuilder = $transportBuilder;
        $this->logService = $logService;
        $this->cancelService = $cancelService;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var Order $order */
            $order = $observer->getOrder();
            $storeId = $order->getStoreId();
            $sendEmail = $this->emailConfigHelper->getSendEmailOrderManualCancel(
                ScopeInterface::SCOPE_STORE,
                $storeId
            );

            if ($sendEmail) {
                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $this->emailConfigHelper->getEmailTemplateOrderManualCancel(
                        ScopeInterface::SCOPE_STORE,
                        $storeId
                    )
                )
                    ->setTemplateOptions(
                        [
                            'area' => Area::AREA_FRONTEND,
                            'store' => $storeId
                        ]
                    )
                    ->setTemplateVars(['order' => $order])
                    ->setFrom(
                        $this->emailConfigHelper->getEmailIdentityOrderManualCancel(
                            ScopeInterface::SCOPE_STORE,
                            $storeId
                        )
                    );
                $transport->addTo($order->getCustomerEmail());
                $transport->getTransport()->sendMessage();
            }

            //add Condition to send Cancel notification to WMS
            if(!is_null($order->getWmsId()))
            {
                $this->cancelService->sendCancellationToWms($order);
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->logService->logError($message, $order, WmsError::CANCEL_ORDER_API_METHOD);
        }
    }
}
