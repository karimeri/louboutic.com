<?php

namespace Project\Wms\Observer;

use stdClass;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\OrderRepository;
use Project\Sales\Model\Magento\Sales\Order;
use Project\Wms\Model\Service\Log;
use Project\Wms\Model\Api\Connector;
use Project\Wms\Model\Api\Service\UpdateAddress;
use Project\Wms\Model\WmsError;

/**
 * Class CallWmsAfterAddressUpdate
 * @package Project\Wms\Observer
 * @author  Synolia <contact@synolia.com>
 */
class CallWmsAfterAddressUpdate implements ObserverInterface
{
    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var UpdateAddress
     */
    protected $wmsUpdateAddressService;

    /**
     * @var Connector
     */
    protected $wmsConnector;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Project\Wms\Model\Service\Log
     */
    protected $logService;

    const STATE_READY_TO_RESHIP_LABEL = "Ready to Reship";

    /**
     * CallWmsAfterAddressUpdate constructor.
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Project\Wms\Model\Api\Service\UpdateAddress $wmsUpdateAddressService
     * @param \Project\Wms\Model\Service\Log $logService
     */
    public function __construct(
        OrderRepository $orderRepository,
        Connector $wmsConnector,
        OrderFactory $orderFactory,
        UpdateAddress $wmsUpdateAddressService,
        Log $logService
    ) {
        $this->orderFactory = $orderFactory;
        $this->wmsUpdateAddressService = $wmsUpdateAddressService;
        $this->wmsConnector = $wmsConnector;
        $this->orderRepository = $orderRepository;
        $this->logService = $logService;
    }

    /**
     * @param Observer $observer
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(Observer $observer)
    {
        try {
            $orderId = $observer->getOrderId();

            /** @var Order $order */
            $order = $this->orderFactory->create()->load($orderId);

            $billingAddress = $order->getBillingAddress();
            $shippingAddress = $order->getShippingAddress();

            //Format street & telephone for UPS
            $billingAddress->setStreet($this->splitAddress($billingAddress->getStreet()));
            $billingAddress->setTelephone($this->sanitizeTelephone($billingAddress->getTelephone()));
            $shippingAddress->setStreet($this->splitAddress($shippingAddress->getStreet()));
            $shippingAddress->setTelephone($this->sanitizeTelephone($shippingAddress->getTelephone()));

            $addresses = [
                'billingAddress' => $billingAddress,
                'shippingAddress' => $shippingAddress
            ];

            $wmsId = $order->getWmsId();

            $websiteId = $order->getStore()->getWebsite()->getWebsiteId();
            try{
                $wmsResponse = $this->wmsUpdateAddressService->updateAddress($addresses, $websiteId, $wmsId);
            } catch (\Exception $e) {
                $this->logService->logError($e->getMessage(), $order, WmsError::UPDATE_ADDRESS_API_METHOD);
                return;
            }


            if (!$wmsResponse || isset($wmsResponse->error)) {
                $message = $wmsResponse->error->message ?? 'See log for more information';
                $this->logService->logError($message, $order, WmsError::UPDATE_ADDRESS_API_METHOD);
            }

            if ($order->getStatus() == Order::STATE_NEED_TO_RESHIP) {
                $order->setState(Order::STATE_READY_TO_RESHIP);
                $order->setStatus(Order::STATE_READY_TO_RESHIP);
                $comment = self::STATE_READY_TO_RESHIP_LABEL;
                $order->addCommentToStatusHistory($comment,Order::STATE_READY_TO_RESHIP);
                $this->orderRepository->save($order);
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->logService->logError($message, $order, WmsError::UPDATE_ADDRESS_API_METHOD);
        }
    }

    /**
     * Split street lines to not exceed limit
     * @param array $orgStreet
     * @return array
     */
    public function splitAddress(array $orgStreet)
    {
        $limit = 35;
        $format = false;
        //check if a line length exceeds limit
        foreach ($orgStreet as $line) {
            if (strlen($line) > $limit) {
                $format = true;
                break;
            }
        }

        if ($format) {
            $i = 0;
            $streets[0] = '';
            $word = strtok(implode(" ", $orgStreet), " ");
            while ($word !== false) {
                if ((strlen($streets[$i]) + strlen($word)) < $limit) {
                    $streets[$i] .= $word . " ";
                } else {
                    $i++;
                    //max lines reached
                    if ($i == 3) break;

                    $streets[$i] = $word . " ";
                }
                $word = strtok(" ");

            }
            return ($streets);
        }
        return ($orgStreet);
    }

    /**
     * Format phone to digits only
     * @param string $telephone
     * @return mixed|null|string|string[]
     */
    public function sanitizeTelephone(string $telephone)
    {
        $telephone = str_replace("+", "00", $telephone);
        $telephone = preg_replace('/[^\d+]/', '', $telephone);
        return $telephone;
    }
}
