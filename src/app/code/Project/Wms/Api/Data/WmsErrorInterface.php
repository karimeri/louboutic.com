<?php

namespace Project\Wms\Api\Data;

/**
 * Interface WmsErrorInterface
 * @package Project\Wms\Api\Data
 */
interface WmsErrorInterface
{
    /**
     * Error Id
     */
    const ERROR_ID = 'error_id';

    /**
     * Entity Id
     */
    const ENTITY_ID = 'entity_id';

    /**
     * Id wms
     */
    const WMS_ID = 'wms_id';

    /**
     * Nb error in WMS
     */
    const NB_ERROR_WMS = 'nb_error_wms';

    /**
     * Last error message in WMS
     */
    const LAST_ERROR_MSG_WMS = 'last_error_msg_wms';

    /**
     * Entity type
     */
    const ENTITY_TYPE = 'entity_type';

    /**
     * API method
     */
    const API_METHOD = 'api_method';

    /**
     * Mail sent
     */
    const MAIL_SENT = 'mail_sent';

    /**
     * Order type
     */
    const ORDER_TYPE = 'order';

    /**
     * Rma Type
     */
    const RMA_TYPE = 'rma';

    /**
     * Create order api method
     */
    const CREATE_ORDER_API_METHOD = 'create_order';

    /**
     * Update address api method
     */
    const UPDATE_ADDRESS_API_METHOD = 'web_orders';

    /**
     * Cancel order api method
     */
    const CANCEL_ORDER_API_METHOD = 'orders/status';

    /**
     * Cancel order api method
     */
    const CREATE_RMA_API_METHOD = 'returns';

    /**
     * @return int
     */
    public function getErrorId();

    /**
     * @param int $errorId
     * @return $this
     */
    public function setErrorId($errorId);

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return int
     */
    public function getWmsId();

    /**
     * @param int $wmsId
     * @return $this
     */
    public function setWmsId($wmsId);

    /**
     * @return int
     */
    public function getNbErrorWms();

    /**
     * @param int $nbErrorWms
     * @return $this
     */
    public function setNbErrorWms($nbErrorWms);

    /**
     * @return string
     */
    public function getLastErrorMsgWms();

    /**
     * @param string $lastErrorMsgWms
     * @return $this
     */
    public function setLastErrorMsgWms($lastErrorMsgWms);

    /**
     * @return string
     */
    public function getEntityType();

    /**
     * @param string $entityType
     * @return $this
     */
    public function setEntityType($entityType);

    /**
     * @return string
     */
    public function getApiMethod();

    /**
     * @param string $apiMethod
     * @return $this
     */
    public function setApiMethod($apiMethod);

    /**
     * @return boolean
     */
    public function isMailSent();

    /**
     * @param boolean $mailSent
     * @return $this
     */
    public function setMailSent($mailSent);
}
