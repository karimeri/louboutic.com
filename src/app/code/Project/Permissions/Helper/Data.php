<?php

namespace Project\Permissions\Helper;

/**
 * Class Data
 * @package Project\Permissions\Helper
 * @author Synolia <contact@synolia.com>
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Path to edit_product_quantity node in ACL
     * Used to check if admin has permission to edit product quantity
     */
    const EDIT_PRODUCT_QTY_ACL_PATH = 'Project_Permissions::edit_product_qty';

    /**
     * Authorization interface
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $authorization;

    /**
     * @var \Magento\Authorization\Model\Acl\AclRetriever
     */
    protected $aclRetriever;

    /**
     * Constructor
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Magento\Authorization\Model\Acl\AclRetriever $aclRetriever
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Authorization\Model\Acl\AclRetriever $aclRetriever
    ) {
        parent::__construct($context);
        $this->authorization = $authorization;
        $this->aclRetriever = $aclRetriever;
    }

    /**
     * Check if admin has permissions to edit product quantity
     * @param int $roleId
     * @return boolean
     */
    public function canAdminEditProductQty($roleId)
    {
        $resources = $this->aclRetriever->getAllowedResourcesByRole($roleId);
        $return = \in_array(self::EDIT_PRODUCT_QTY_ACL_PATH, $resources) || \in_array(
            'Magento_Backend::all',
            $resources
        );

        return $return;
    }
}
