<?php

namespace Project\Permissions\Ui\DataProvider\Product\Form\Modifier;

use Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\ConfigurablePanel;

/**
 * Class ConfigurableQty
 * @package Project\Permissions\Ui\DataProvider\Product\Form\Modifier
 * @author Synolia <contact@synolia.com>
 */
class ConfigurableQty extends \Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\ConfigurableQty
{
    /**
     * Backend authorization session
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * Helper data
     * @var \Project\Permissions\Helper\Data
     */
    protected $helperData;

    /**
     * Constructor
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Project\Permissions\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Backend\Model\Auth\Session $authSession,
        \Project\Permissions\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
        $this->authSession = $authSession;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        if ($groupCode = $this->getGroupCodeByField($meta, self::CODE_QTY_CONTAINER)) {
            $parentChildren = &$meta[$groupCode]['children'];
            if (!empty($parentChildren[self::CODE_QTY_CONTAINER])) {
                // synolia start
                if ($this->authSession->isLoggedIn()
                    && $this->authSession->getUser()->getRole()
                    && !$this->helperData->canAdminEditProductQty($this->authSession->getUser()->getRole()->getId())
                ) {
                    $disabled = true;
                } else {
                    $disabled = '!ns = ${ $.ns }, index = ' . ConfigurablePanel::CONFIGURABLE_MATRIX . ':isEmpty';
                }
                // synolia end

                $parentChildren[self::CODE_QTY_CONTAINER] = array_replace_recursive(
                    $parentChildren[self::CODE_QTY_CONTAINER],
                    [
                        'children' => [
                            self::CODE_QUANTITY => [
                                'arguments' => [
                                    'data' => [
                                        'config' => [
                                            'imports' => [
                                                'disabled' => $disabled,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]
                );
            }
        }

        return $meta;
    }
}
