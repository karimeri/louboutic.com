<?php

namespace Project\Permissions\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Controller\Adminhtml\Product\Initialization\StockDataFilter;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\Serialize\JsonValidator;
use Magento\Framework\App\ObjectManager;

/**
 * Class AdvancedInventory
 * @package Project\Permissions\Ui\DataProvider\Product\Form\Modifier
 * @author Synolia <contact@synolia.com>
 */
class AdvancedInventory extends \Magento\CatalogInventory\Ui\DataProvider\Product\Form\Modifier\AdvancedInventory
{
    const STOCK_DATA_FIELDS = 'stock_data';

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var StockConfigurationInterface
     */
    private $stockConfiguration;

    /**
     * @var array
     */
    private $meta = [];

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var JsonValidator
     */
    private $jsonValidator;

    /**
     * Backend authorization session
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * Helper data
     * @var \Project\Permissions\Helper\Data
     */
    protected $helperData;

    /**
     * Constructor
     * @param LocatorInterface $locator
     * @param StockRegistryInterface $stockRegistry
     * @param ArrayManager $arrayManager
     * @param StockConfigurationInterface $stockConfiguration
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Project\Permissions\Helper\Data $helperData
     * @param Json|null $serializer
     * @param JsonValidator|null $jsonValidator
     */
    public function __construct(
        LocatorInterface $locator,
        StockRegistryInterface $stockRegistry,
        ArrayManager $arrayManager,
        StockConfigurationInterface $stockConfiguration,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Project\Permissions\Helper\Data $helperData,
        Json $serializer = null,
        JsonValidator $jsonValidator = null
    ) {
        parent::__construct(
            $locator,
            $stockRegistry,
            $arrayManager,
            $stockConfiguration,
            $serializer,
            $jsonValidator
        );

        $this->locator = $locator;
        $this->stockRegistry = $stockRegistry;
        $this->arrayManager = $arrayManager;
        $this->stockConfiguration = $stockConfiguration;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
        $this->jsonValidator = $jsonValidator ?: ObjectManager::getInstance()->get(JsonValidator::class);
        // start synolia
        $this->helperData = $helperData;
        $this->authSession = $authSession;
        // end synolia
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;

        $this->prepareMeta();

        return $this->meta;
    }

    /**
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function prepareMeta()
    {
        $fieldCode = 'quantity_and_stock_status';
        $pathField = $this->arrayManager->findPath($fieldCode, $this->meta, null, 'children');

        if ($pathField) {
            $labelField = $this->arrayManager->get(
                $this->arrayManager->slicePath($pathField, 0, -2) . '/arguments/data/config/label',
                $this->meta
            );
            $fieldsetPath = $this->arrayManager->slicePath($pathField, 0, -4);

            $this->meta = $this->arrayManager->merge(
                $pathField . '/arguments/data/config',
                $this->meta,
                [
                    'label' => __('Stock Status'),
                    'value' => '1',
                    'dataScope' => $fieldCode . '.is_in_stock',
                    'scopeLabel' => '[GLOBAL]',
                    'imports' => [
                        'visible' => '${$.provider}:data.product.stock_data.manage_stock',
                    ],
                ]
            );
            $this->meta = $this->arrayManager->merge(
                $this->arrayManager->slicePath($pathField, 0, -2) . '/arguments/data/config',
                $this->meta,
                [
                    'label' => __('Stock Status'),
                    'scopeLabel' => '[GLOBAL]',
                ]
            );

            $container['arguments']['data']['config'] = [
                'formElement' => 'container',
                'componentType' => 'container',
                'component' => "Magento_Ui/js/form/components/group",
                'label' => $labelField,
                'breakLine' => false,
                'dataScope' => $fieldCode,
                'scopeLabel' => '[GLOBAL]',
                'source' => 'product_details',
                'sortOrder' => (int) $this->arrayManager->get(
                    $this->arrayManager->slicePath(
                        $pathField,
                        0,
                        -2
                    ) . '/arguments/data/config/sortOrder',
                    $this->meta
                ) - 1,
            ];
            $qty['arguments']['data']['config'] = [
                'component' => 'Magento_CatalogInventory/js/components/qty-validator-changer',
                'dataType' => 'number',
                'formElement' => 'input',
                'componentType' => 'field',
                'visible' => '1',
                'require' => '0',
                'additionalClasses' => 'admin__field-small',
                'label' => __('Quantity'),
                'scopeLabel' => '[GLOBAL]',
                'dataScope' => 'qty',
                'validation' => [
                    'validate-number' => true,
                    'less-than-equals-to' => StockDataFilter::MAX_QTY_VALUE,
                ],
                'imports' => [
                    'handleChanges' => '${$.provider}:data.product.stock_data.is_qty_decimal',
                ],
                'sortOrder' => 10,
            ];

            // start synolia
            if ($this->authSession->isLoggedIn()
                && $this->authSession->getUser()->getRole()
                && !$this->helperData->canAdminEditProductQty($this->authSession->getUser()->getRole()->getId())
            ) {
                $qty['arguments']['data']['config']['disabled'] = true;
            } else {
                $advancedInventoryButton['arguments']['data']['config'] = [
                    'displayAsLink' => true,
                    'formElement' => 'container',
                    'componentType' => 'container',
                    'component' => 'Magento_Ui/js/form/components/button',
                    'template' => 'ui/form/components/button/container',
                    'actions' => [
                        [
                            'targetName' => 'product_form.product_form.advanced_inventory_modal',
                            'actionName' => 'toggleModal',
                        ],
                    ],
                    'title' => __('Advanced Inventory'),
                    'provider' => false,
                    'additionalForGroup' => true,
                    'source' => 'product_details',
                    'sortOrder' => 20,
                ];

                $container['children']['advanced_inventory_button'] = $advancedInventoryButton;
            }

            $container['children']['qty'] = $qty;
            // end synolia

            $this->meta = $this->arrayManager->merge(
                $fieldsetPath . '/children',
                $this->meta,
                ['quantity_and_stock_status_qty' => $container]
            );
        }
    }
}
