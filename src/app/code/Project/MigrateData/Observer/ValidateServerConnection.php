<?php

namespace Project\MigrateData\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\Manager;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Service\ServerService;

/**
 * Observer is responsible for changing scope for all price attributes in system
 * depending on 'Catalog Price Scope' configuration parameter
 */
class ValidateServerConnection implements ObserverInterface
{
    /**
     * @var \Project\MigrateData\Helper\ServerHelper
     */
    private $serverHelper;
    /**
     * @var \Project\MigrateData\Service\ServerService
     */
    private $serverService;
    /**
     * @var \Magento\Framework\Message\Manager
     */
    private $messageManager;

    /**
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param \Project\MigrateData\Service\ServerService $serverService
     * @param \Magento\Framework\Message\Manager $messageManager
     */
    public function __construct(
        ServerHelper $serverHelper,
        ServerService $serverService,
        Manager $messageManager
    ) {
        $this->serverHelper   = $serverHelper;
        $this->serverService  = $serverService;
        $this->messageManager = $messageManager;
    }

    /**
     * @param EventObserver $observer
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(EventObserver $observer)
    {
        try {
            $host = $this->serverService->getServerInstance();

            if ($this->serverHelper->isSshEnabled()) {
                $directory = $this->serverHelper->getValue(ServerHelper::SSH_DIR);
                $this->serverService->testSSHConnection($host, $directory);
                $this->messageManager->addSuccessMessage(__('SSH connection works'));
            }

            $this->serverService->testDBConnection($host);
            $this->messageManager->addSuccessMessage(__('DB connection works'));
        } catch (\Throwable $throwable) {
            $this->messageManager->addErrorMessage($throwable->getMessage());
        }
    }
}
