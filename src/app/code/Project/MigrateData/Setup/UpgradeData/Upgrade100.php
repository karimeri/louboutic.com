<?php

namespace Project\MigrateData\Setup\UpgradeData;

use Project\MigrateData\Setup\UpgradeData;

use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Eav\ConfigSetupFactory;
use Project\Core\Model\Environment;


/**
 * Class Upgrade100
 *
 * @package Project\Erp\Setup\UpgradeData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade100
{
    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    public function __construct(
        ConfigSetupFactory $configSetupFactory
    ) {
        $this->configSetupFactory = $configSetupFactory;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     *
     * @throws \RuntimeException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        if ($upgradeDataObject->getEnvironment() === Environment::EU) {
            $ssh_key_path_value = 'migratedata/ssh/key';
        } else {
            return;
        }
        /** @var ConfigSetup $configSetup */
        $configSetup = $this->configSetupFactory->create();

        $configSetup->saveConfig(
            'synchronizations_migratedata/migratedata_server/ssh_key_path',
            $ssh_key_path_value
        );
    }
}
