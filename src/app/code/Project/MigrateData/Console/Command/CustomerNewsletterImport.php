<?php

namespace Project\MigrateData\Console\Command;

use Project\MigrateData\Model\DataCheckManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Framework\App\ResourceConnection;
use Symfony\Component\Console\Input\InputOption;
use Magento\Newsletter\Model\Subscriber;

/**
 * Class CustomerNewsletterImport
 * @package Project\MigrateData\Console\Command
 */
class CustomerNewsletterImport extends Command {

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * @var Magento\Newsletter\Model\SubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * @var Magento\Framework\App\ResourceConnection
     */
    protected $_connexion;
    private $conn;
    private $storesM1ByCode = array();
    private $storesM1ByWebsiteId = array();
    private $storesM1ByStoreId = array();
    private $storesM2ByCode = array();
    private $storesM2ByWebsiteId = array();
    private $storesM2ByStoreId = array();

    /**
     * CheckCustomerAddresses constructor.
     * @param ResourceConnection $connexion
     * @param SubscriberFactory $subscriberFactory
     */
    public function __construct(
            ResourceConnection $connexion,
            SubscriberFactory $subscriberFactory
    ) {
        $this->subscriberFactory = $subscriberFactory;
        $this->_connexion = $connexion;


        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure() {
        $this->setName('synolia:import:newsletter:v2')->setDescription('Check customer addresses');


        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $startTime = microtime(true);

        try {
            $output->writeln("<info>Start importNewsletters " . $this->debug() . " </info>");
            try {
                $this->conn = new \PDO("mysql:host=127.0.0.1;port=3306;dbname=clb_prod_eu", "clb_prod_eu", "VAhBaitCDlTp");
                $output->writeln("<info>Connected successfully </info>");
            } catch (PDOException $e) {
                $output->writeln("<info>Connection failed: " . $e->getMessage() . " </info>");
            }

            $this->initM2Stores();
            $this->initM1Stores();
            $newsLetters = $this->getNewsLetterFromM1();
            $connexion = $this->_connexion->getConnection();
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            $date = date('Y-m-d H:i:s');
            $output->writeln("<info>Total newsletters to Import " . count($newsLetters) . " </info>");
            $counter = 0;
            $nlInserted = 0;
            $newsLetterOnError = array();
            $totalNlToImport = count($newsLetters);
            $inserted = 0;
            $reqsql = "";
            $connexion->query("SET FOREIGN_KEY_CHECKS = 0; ");
            foreach ($newsLetters as $key => $newsLetter) {

                /*
                 * sudo -u www-data bin/magento  synolia:import:newsletter:v2
                 * recupération de la correspondance du store
                 */
                if (!isset($newsLetter['store_id'])) {
                    $newsLetterOnError[] = $newsLetter;
                    continue;
                }
                $store_infos = $this->getStoreCodeM1toM2($this->storesM1ByStoreId[$newsLetter['store_id']]['code']);

                /*
                 * Preparation de la data client
                 */
                $d = array(
                    'store_id' => $store_infos['store_id'],
                    'subscriber_email' => strtolower($newsLetter["subscriber_email"]),
                    'subscriber_status' => $newsLetter["subscriber_status"],
                    'subscriber_confirm_code' => $newsLetter["subscriber_confirm_code"],
                    "change_status_at" => $date
                );
                /* préparer la requête */
                $req = $connexion->prepare('INSERT INTO  newsletter_subscriber
                                               ( store_id ,
                                                subscriber_email ,
                                                subscriber_status ,
                                                subscriber_confirm_code, 
                                                change_status_at
                                                ) VALUE (
                                                    :store_id, 
                                                    :subscriber_email,
                                                    :subscriber_status ,
                                                    :subscriber_confirm_code,
                                                    :change_status_at
                                                    )
                                              ');


                //used to make a sql file 
                $reqsql .= "INSERT INTO  newsletter_subscriber( store_id , subscriber_email , subscriber_status , subscriber_confirm_code,  change_status_at ) VALUE (
                " . $store_infos['store_id'] . " , 
                \"" . strtolower($newsLetter["subscriber_email"]) . "\" ,
                " . $newsLetter["subscriber_status"] . "  ,
                \"" . $newsLetter["subscriber_confirm_code"] . "\" ,
                \"" . $date . "\"
                ); 
                "; /*
                  execution de la query */

                if ($req->execute($d)) {
                    $inserted++;
                }
                unset($req);
                unset($newsLetters[$key]);
                $nlInserted++;
                $counter++;
                if ($counter == 100) {
                    $endTime = microtime(true);
                    $diff = round($endTime - $startTime);
                    $minutes = floor($diff / 60); //only minutes
                    $seconds = $diff % 60; //remaining seconds, using modulo operator
                    $output->writeln("<info>Total newsletters to Import $nlInserted / " . $totalNlToImport . " in minutes:$minutes, seconds:$seconds (" . $this->debug() . ") </info>");
                    $counter = 0;
                }
            }
            $connexion->query("SET FOREIGN_KEY_CHECKS = 1; ");

            //used to make a sql file 
            //$this->generateSqlFile($reqsql); 

            $output->writeln("<info> Total inserted ->" . $inserted . "... done</info>");
            $output->writeln("<info> Count Customers with store ID = 0 " . count($newsLetterOnError) . "... done</info>");

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }

    public function generateSqlFile($content) {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $om->get('Magento\Framework\Filesystem');
        $directoryList = $om->get('Magento\Framework\App\Filesystem\DirectoryList');

        $writer = $filesystem->getDirectoryWrite($directoryList::VAR_DIR);
        $file = $writer->openFile("log/" . "newsletter.sql", 'w');
        try {
            $file->lock();
            try {
                $file->write($content);
            } finally {
                $file->unlock();
            }
        } finally {
            $file->close();
        }
    }

    public function getNewsLetterFromM1() {
        $tableName = "newsletter_subscriber";
        $query = "SELECT * FROM $tableName ";
        return (array) ($this->conn->query($query, \PDO::FETCH_ASSOC)->fetchAll());
    }

    private function debug() {
        /* Currently used memory */
        $mem_usage = memory_get_usage();
        /* Peak memory usage */
        $mem_peak = memory_get_peak_usage();
        return " :  " . round($mem_usage / 1048576, 2) . " MO";
    }

    private function initM2Stores() {

        $tableName = $this->_connexion->getTableName('store'); //gives table name with prefix 
        $connexion = $this->_connexion->getConnection();
        $stores = $connexion->fetchAll("SELECT * FROM $tableName");

        foreach ($stores as $store) {

            $this->storesM2ByCode[$store['code']] = array(
                'code' => $store['code'],
                'store_id' => $store['store_id'],
                'website_id' => $store['website_id'],
                'group_id' => $store['group_id'],
                'name' => $store['name'],
                'sort_order' => $store['sort_order'],
                'is_active' => $store['is_active'],
            );
            $this->storesM2ByWebsiteId[$store['website_id']] = array(
                'code' => $store['code'],
                'store_id' => $store['store_id'],
                'website_id' => $store['website_id'],
                'group_id' => $store['group_id'],
                'name' => $store['name'],
                'sort_order' => $store['sort_order'],
                'is_active' => $store['is_active'],
            );
            $this->storesM2ByStoreId[$store['store_id']] = array(
                'code' => $store['code'],
                'store_id' => $store['store_id'],
                'website_id' => $store['website_id'],
                'group_id' => $store['group_id'],
                'name' => $store['name'],
                'sort_order' => $store['sort_order'],
                'is_active' => $store['is_active'],
            );
        }
    }

    private function getStoreCodeM1toM2($code) {
        if ($code == "fr_en") {
            return $this->storesM2ByCode["fr_fr"];
        }
        return $this->storesM2ByCode[$code];
    }

    private function initM1Stores() {

        $stores = ($this->conn->query("SELECT * from core_store", \PDO::FETCH_ASSOC)->fetchAll());
        foreach ($stores as $store) {
            $this->storesM1ByCode[$store['code']] = array(
                'code' => $store['code'],
                'store_id' => $store['store_id'],
                'website_id' => $store['website_id'],
                'group_id' => $store['group_id'],
                'name' => $store['name'],
                'sort_order' => $store['sort_order'],
                'is_active' => $store['is_active'],
            );
            $this->storesM1ByWebsiteId[$store['website_id']] = array(
                'code' => $store['code'],
                'store_id' => $store['store_id'],
                'website_id' => $store['website_id'],
                'group_id' => $store['group_id'],
                'name' => $store['name'],
                'sort_order' => $store['sort_order'],
                'is_active' => $store['is_active'],
            );
            $this->storesM1ByStoreId[$store['store_id']] = array(
                'code' => $store['code'],
                'store_id' => $store['store_id'],
                'website_id' => $store['website_id'],
                'group_id' => $store['group_id'],
                'name' => $store['name'],
                'sort_order' => $store['sort_order'],
                'is_active' => $store['is_active'],
            );
        }
    }

}
