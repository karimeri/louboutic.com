<?php

namespace Project\MigrateData\Console\Command;

use Project\MigrateData\Model\DataCheckManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckCustomerAddresses
 * @package Project\MigrateData\Console\Command
 */
class CheckCustomerAddresses extends Command
{
    /**
     * @var DataCheckManager
     */
    protected $dataCheckManager;

    /**
     * CheckCustomerAddresses constructor.
     * @param DataCheckManager $dataCheckManager
     * @param string|null $name
     */
    public function __construct(
        DataCheckManager $dataCheckManager,
        ?string $name = null
    ) {
        $this->dataCheckManager = $dataCheckManager;
        parent::__construct($name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('project:migrate-data:check-customer-addresses')->setDescription('Check customer addresses');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln("<info>Check customer addresses ...</info>");

            $this->dataCheckManager->checkCustomerAddresses();

            $output->writeln("<info>... done</info>");
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }
}
