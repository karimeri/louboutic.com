<?php

namespace Project\MigrateData\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Project\MigrateData\Helper\ServerHelper;

/**
 * Class PublicKey
 * @package Project\MigrateData\Block\Adminhtml\System\Config
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class PublicKey extends Field
{
    /**
     * @var string
     */
    //phpcs:ignore PSR2.Classes.PropertyDeclaration.Underscore
    protected $_template = 'Project_MigrateData::system/config/publickey.phtml';

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Project\MigrateData\Helper\ServerHelper
     */
    protected $serverHelper;

    /**
     * PublicKey constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Filesystem $filesystem,
        ServerHelper $serverHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->filesystem = $filesystem;
        $this->serverHelper = $serverHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function render(AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getPublicKey()
    {
        $reader = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $path = $reader->getRelativePath($this->serverHelper->getValue(ServerHelper::SSH_KEY_PATH) . '.pub');

        if ($reader->isFile($path) && $reader->isReadable($path)) {
            try {
                return $reader->readFile($path);
            } catch (FileSystemException $e) {
                return \__('Error when reading public key file');
            }
        }

        return \__('Public Key not generated yet');
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
