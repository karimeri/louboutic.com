<?php

namespace Project\MigrateData\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class KeygenButton
 *
 * @package Project\MigrateData\Block\Adminhtml\System\Config
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class KeygenButton extends Field
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var string
     */
    //phpcs:ignore PSR2.Classes.PropertyDeclaration.Underscore
    protected $_template = 'Project_MigrateData::system/config/keygenbutton.phtml';

    /**
     * KeygenButton constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        parent::__construct($context, $data);
    }

    /**
     * Generate button html
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'migratedata_keygen_result_button',
                'label' => __('Generate Key file'),
                'onclick' => 'javascript:migrateDataGenerateKeyFile();',
            ]
        );

        return $button->toHtml();
    }

    /**
     * Get url for ajax request on button click
     *
     * @return string
     */
    public function getAdminUrl()
    {
        return $this->getUrl('migratedata/key/generate', ['store' => $this->_request->getParam('store')]);
    }

    /**
     * {@inheritdoc}
     */
    public function render(AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
