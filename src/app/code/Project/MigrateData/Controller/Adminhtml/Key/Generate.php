<?php

namespace Project\MigrateData\Controller\Adminhtml\Key;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use phpseclib\Crypt\RSA;

/**
 * Class Generate
 *
 * @package HappyTech\SmtpEmail\Controller\Adminhtml\Test
 * @author Synolia <contact@synolia.com>
 */
class Generate extends Action
{
    /**
     * @var \phpseclib\Crypt\RSA
     */
    protected $cryptRSA;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * Generate constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \phpseclib\Crypt\RSA $cryptRSA
     * @param \Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        RSA $cryptRSA,
        Filesystem $filesystem
    ) {
        parent::__construct($context);
        $this->cryptRSA = $cryptRSA;
        $this->filesystem = $filesystem;
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $this->getRequest();

        /** @var \Magento\Framework\App\Response\Http $response */
        $response = $this->getResponse();

        $path = $request->getParam('path', null);
        $passPhrase = $request->getPost('passPhrase');

        $this->cryptRSA->setPublicKeyFormat(RSA::PUBLIC_FORMAT_OPENSSH);
        if (!empty($passPhrase)) {
            $this->cryptRSA->setPassword($passPhrase);
        }

        $keys = $this->cryptRSA->createKey(4096);

        $this->saveKeys($path, $keys['privatekey'], $keys['publickey']);

        $response->setBody($keys['publickey']);

        return $response;
    }

    protected function saveKeys($path, $privateKey, $publicKey)
    {
        $this->saveFile($path, $privateKey);
        $this->saveFile($path . '.pub', $publicKey);
    }

    protected function saveFile($path, $content)
    {
        $writer = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $path = $writer->getRelativePath($path);

        $stream = $writer->openFile($path, 'w');
        $stream->write($content);
        $writer->changePermissions($path, 0700);
        $stream->close();
    }
}
