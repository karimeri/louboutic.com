<?php

namespace Project\MigrateData\Model\Import\ErrorProcessing;

use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregator as ProcessingErrorAggregatorBase;

/**
 * Class ProcessingErrorAgregator
 * @package Project\MigrateData\Model
 */
class ProcessingErrorAggregator extends ProcessingErrorAggregatorBase
{
    /**
    * @return string $validationStrategy
    */
    public function getValidationStrategy()
    {
        return $this->validationStrategy;
    }

    /**
     * @return bool
     */
    public function hasToBeTerminated()
    {
        return $this->isErrorLimitExceeded();
    }
}
