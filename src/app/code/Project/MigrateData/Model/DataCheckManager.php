<?php

namespace Project\MigrateData\Model;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\Address;
use Magento\Customer\Model\AddressFactory;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory as CustomerAddressCollectionFactory;
use Magento\Framework\Model\ResourceModel\Iterator;
use Project\MigrateData\Manager\AddressCsvManager;
use Psr\Log\LoggerInterface;

/**
 * Class DataCheckManager
 * @package Project\MigrateData\Model
 */
class DataCheckManager
{
    protected $addressesToDelete = [];

    /**
     * @var CustomerAddressCollectionFactory
     */
    protected $_customerAddressCollectionFactory;

    /**
     * @var Iterator
     */
    protected $iterator;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var AddressFactory
     */
    protected $addressFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * DataCheckManager constructor.
     * @param CustomerAddressCollectionFactory $customerAddressCollectionFactory
     * @param Iterator $iterator
     * @param AddressRepositoryInterface $addressRepository
     * @param AddressFactory $addressFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        CustomerAddressCollectionFactory $customerAddressCollectionFactory,
        Iterator $iterator,
        AddressRepositoryInterface $addressRepository,
        AddressFactory $addressFactory,
        LoggerInterface $logger
    ) {
        $this->_customerAddressCollectionFactory = $customerAddressCollectionFactory;
        $this->iterator = $iterator;
        $this->addressRepository = $addressRepository;
        $this->addressFactory = $addressFactory;
        $this->logger = $logger;
    }

    public function checkCustomerAddresses() {
        try {
            $this->iterator->walk(
                $this->getCustomerAddressCollection()->getSelect(),
                [[$this, 'callback']]
            );

            echo "Total addresses to delete : " . count($this->addressesToDelete) . " **************" . PHP_EOL;
            foreach ($this->addressesToDelete as $addressId) {
                $this->addressRepository->deleteById($addressId);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param $args
     */
    public function callback($args)
    {
        try {
            $addressDataModel = $this
                ->addressRepository
                ->getById($args['row']['entity_id']);

            /** @var Address $address */
            $address = $this->addressFactory->create();
            $address->updateData($addressDataModel);


            if (strlen($address->getStreetFull()) > AddressCsvManager::ADDRESS_STREET_LIMIT || strlen($address->getName()) > AddressCsvManager::ADDRESS_STREET_LIMIT) {
                $this->addressesToDelete[] = $address->getEntityId();
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @return \Magento\Customer\Model\ResourceModel\Address\Collection
     */
    private function getCustomerAddressCollection()
    {
        return $this->_customerAddressCollectionFactory->create();
    }
}
