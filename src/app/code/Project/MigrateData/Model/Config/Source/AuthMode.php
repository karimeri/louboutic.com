<?php

namespace Project\MigrateData\Model\Config\Source;

/**
 * Class AuthMode
 * @package Project\MigrateData\Model\Config\Source
 * @author Synolia <contact@synolia.com>
 */
class AuthMode implements \Magento\Framework\Option\ArrayInterface
{
    const PASSWORD = 'password';
    const KEY_FILE = 'key_file';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => static::PASSWORD, 'label' => __('Password')],
            ['value' => static::KEY_FILE, 'label' => __('Key File')],
        ];
    }
}
