<?php

namespace Project\MigrateData\Model;

use Magento\ImportExport\Model\Import as ImportBase;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

/**
 * Class Import
 * @package Project\MigrateData\Model
 */
class Import extends ImportBase
{
    /**
     * Return operation result messages
     *
     * @param ProcessingErrorAggregatorInterface $validationResult
     * @return string[]
     */
    public function getOperationResultMessages(ProcessingErrorAggregatorInterface $validationResult)
    {
        $messages = [];

        if ($this->getProcessedRowsCount()) {
            if (($validationResult->getErrorsCount() &&
                $validationResult->getValidationStrategy() ===
                ProcessingErrorAggregatorInterface::VALIDATION_STRATEGY_STOP_ON_ERROR) ||
                ($validationResult->getErrorsCount() > $validationResult->getAllowedErrorsCount() &&
                $validationResult->getValidationStrategy() ===
                ProcessingErrorAggregatorInterface::VALIDATION_STRATEGY_SKIP_ERRORS)) {
                    $messages[] =
                        __('Data validation failed. Please fix the following errors and upload the file again.');

                    // errors info
                foreach ($validationResult->getRowsGroupedByErrorCode() as $errorMessage => $rows) {
                    $error = $errorMessage . ' ' . __('in row(s)') . ': ' . implode(', ', $rows);
                    $messages[] = $error;
                }
            } else {
                if ($this->isImportAllowed()) {
                    $messages[] = __('The validation is complete.');
                } else {
                    $messages[] = __('The file is valid, but we can\'t import it for some reason.');
                }
            }

            $messages[] = __(
                'Checked rows: %1, checked entities: %2, invalid rows: %3, total errors: %4',
                $this->getProcessedRowsCount(),
                $this->getProcessedEntitiesCount(),
                $validationResult->getInvalidRowsCount(),
                $validationResult->getErrorsCount(
                    [
                        ProcessingError::ERROR_LEVEL_CRITICAL,
                        ProcessingError::ERROR_LEVEL_NOT_CRITICAL
                    ]
                )
            );
        } else {
            $messages[] = __('This file does not contain any data.');
        }
        return $messages;
    }

    public function validateSource(\Magento\ImportExport\Model\Import\AbstractSource $source)
    {
        $this->addLogComment(__('Begin data validation'));

        $errorAggregator = $this->getErrorAggregator();
        $errorAggregator->initValidationStrategy(
            $this->getData(self::FIELD_NAME_VALIDATION_STRATEGY),
            $this->getData(self::FIELD_NAME_ALLOWED_ERROR_COUNT)
        );

        try {
            $adapter = $this->_getEntityAdapter()->setSource($source);
            $adapter->validateData();
        } catch (\Exception $e) {
            $errorAggregator->addError(
                \Magento\ImportExport\Model\Import\Entity\AbstractEntity::ERROR_CODE_SYSTEM_EXCEPTION,
                ProcessingError::ERROR_LEVEL_CRITICAL,
                null,
                null,
                $e->getMessage()
            );
        }

        $messages = $this->getOperationResultMessages($errorAggregator);
        $this->addLogComment($messages);

        $result = ($errorAggregator->getValidationStrategy() ===
            ProcessingErrorAggregatorInterface::VALIDATION_STRATEGY_STOP_ON_ERROR) ?
                    !$errorAggregator->getErrorsCount() :
                    $errorAggregator->getErrorsCount() <= $errorAggregator->getAllowedErrorsCount();

        if ($result) {
             $this->addLogComment(__('Import data validation is complete.'));
        }
        return $result;
    }
}
