<?php

namespace Project\MigrateData\Action;

use Project\MigrateData\Manager\NewsletterCsvManager;
use Symfony\Component\Console\Output\ConsoleOutput as SymfonyOutput;
use Magento\Framework\Filesystem;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\ImportExport\Model\Import as ImportModel;
use Magento\ImportExport\Model\Import\Adapter as ImportAdapter;
use Magento\ImportExport\Model\ImportFactory;
use Magento\Framework\App\State;

/**
 * Class ImportNewsletter
 *
 * @package Project\MigrateData\Action
 * @author Synolia <contact@synolia.com>
 */
class ImportNewsletter implements ActionInterface
{
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $filesystem;
    /**
     * @var \Project\MigrateData\Manager\NewsletterCsvManager
     */
    private $newsletterCsvManager;
    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    private $output;
    /**
     * @var ImportModel
     */
    private $import;


    /**
     * ImportNewsletter constructor.
     *
     * @param Filesystem $filesystem
     * @param NewsletterCsvManager $newsletterCsvManager
     * @param SymfonyOutput $output
     * @param State $state
     * @param ImportFactory $importFactory
     */
    public function __construct(
        Filesystem $filesystem,
        NewsletterCsvManager $newsletterCsvManager,
        SymfonyOutput $output,
        State $state,
        ImportFactory $importFactory
    ) {

        try {
            $state->setAreaCode('adminhtml');
        } catch (\Exception $e) {
            //area code already set
        }

        $this->filesystem             = $filesystem;
        $this->newsletterCsvManager   = $newsletterCsvManager;
        $this->output                 = $output;
        $this->import                 = $importFactory->create();
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|void
     * @throws \Exception
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \TypeError
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        // Export Newsletter
        $startExport = microtime(true);
        $this->newsletterCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Subscriber time : ' . number_format($totalExport, 6));

        // Import Newsletter
        $startImport = microtime(true);
        $this->processCsv(NewsletterCsvManager::CSV_LOCATION);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('Import Subscriber time : ' . number_format($totalImport, 6));
    }

    /**
     * @param $pathFile
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function processCsv($pathFile)
    {
        $this->import->setData($this->prepareData());

        $source = ImportAdapter::findAdapterFor(
            $pathFile,
            $this->filesystem->getDirectoryRead(DirectoryList::ROOT),
            $this->prepareData()[ImportModel::FIELD_FIELD_SEPARATOR]
        );

        $validationResult = $this->import->validateSource($source);

        if (!$validationResult) {
            $errors = $this->import->getFormatedLogTrace();
            throw new \Exception('Error on validate source: '.PHP_EOL.$errors);
        } else {
            $this->import->importSource();
        }
    }

    /**
     * @return array
     */
    protected function prepareData()
    {
        $data = [
            'entity' => 'subscriber_newsletter',
            'behavior' => 'add_update',
            'validation_strategy' => 'validation-skip-errors',
            'allowed_error_count' => '100000',
            '_import_field_separator' => ';',
            '_import_multiple_value_separator' => ',',
            'import_images_file_dir' => "",
        ];

        return $data;
    }
}
