<?php

namespace Project\MigrateData\Action;

use Project\MigrateData\Manager\CustomerCsvManager;
use Magento\Framework\Filesystem;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\ImportExport\Model\Import as ImportModel;
use Magento\ImportExport\Model\Import\Adapter as ImportAdapter;
use Magento\ImportExport\Model\ImportFactory;
use Magento\Framework\App\State;

/**
 * Class ImportCustomer
 *
 * @package Project\MigrateData\Action
 * @author Synolia <contact@synolia.com>
 */
class ImportCustomer implements ActionInterface
{
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $filesystem;
    /**
     * @var \Project\MigrateData\Manager\CustomerCsvManager
     */
    private $customerCsvManager;
    /**
     * @var ImportModel
     */
    private $import;

    private $consoleOutput;

    /**
     * ImportCustomer constructor.
     *
     * @param Filesystem $filesystem
     * @param CustomerCsvManager $customerCsvManager
     * @param State $state
     * @param ImportFactory $importFactory
     */
    public function __construct(
        Filesystem $filesystem,
        CustomerCsvManager $customerCsvManager,
        State $state,
        ImportFactory $importFactory
    ) {

        try {
            $state->setAreaCode('adminhtml');
        } catch (\Exception $e) {
            //area code already set
        }

        $this->filesystem             = $filesystem;
        $this->customerCsvManager     = $customerCsvManager;
        $this->import                 = $importFactory->create();
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|void
     * @throws \Exception
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \TypeError
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $this->consoleOutput=$consoleOutput;
        // Export Customer
        $startExport = microtime(true);
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('start Export Customer time : ' . number_format($totalExport, 6));
        $this->customerCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('end Export Customer time : ' . number_format($totalExport, 6));

        // Import Customer
        $startImport = microtime(true);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('start Import Customer time : ' . number_format($totalImport, 6));
        $this->processCsv(CustomerCsvManager::CSV_LOCATION);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('end Import Customer time : ' . number_format($totalImport, 6));
    }

    /**
     * @param $pathFile
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function processCsv($pathFile)
    {
        $this->import->setData($this->prepareData());

        $consoleOutput=$this->consoleOutput;
        $startImport = microtime(true);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('start findAdapterFor Customer time : ' . number_format($totalImport, 6));
        $source = ImportAdapter::findAdapterFor(
            $pathFile,
            $this->filesystem->getDirectoryRead(DirectoryList::ROOT),
            $this->prepareData()[ImportModel::FIELD_FIELD_SEPARATOR]
        );
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('start validateSource Customer time : ' . number_format($totalImport, 6));
        $validationResult = $this->import->validateSource($source);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('end validateSource Customer time : ' . number_format($totalImport, 6));

        if (!$validationResult) {
            $errors = $this->import->getFormatedLogTrace();
            throw new \Exception('Error on validate source: '.PHP_EOL.$errors);
        } else {
            $totalImport = microtime(true) - $startImport;
            $consoleOutput->writeSuccess('start importSource Customer time : ' . number_format($totalImport, 6));
            $this->import->importSource();
            $totalImport = microtime(true) - $startImport;
            $consoleOutput->writeSuccess('end importSource Customer time : ' . number_format($totalImport, 6));
        }
    }

    /**
     * @return array
     */
    protected function prepareData()
    {
        $data = [
            'entity' => 'customer',
            'behavior' => 'add_update',
            'validation_strategy' => 'validation-skip-errors',
            'allowed_error_count' => '100000',
            '_import_field_separator' => ';',
            '_import_multiple_value_separator' => ',',
            'import_images_file_dir' => "",
        ];

        return $data;
    }
}
