<?php

namespace Project\MigrateData\Action;

use Project\MigrateData\Model\DataCheckManager;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class CheckAddress
 * @package Project\MigrateData\Action
 */
class CheckAddress implements ActionInterface
{
    /**
     * @var DataCheckManager
     */
    protected $dataCheckManager;

    /**
     * CheckAddress constructor.
     * @param DataCheckManager $dataCheckManager
     */
    public function __construct(
        DataCheckManager $dataCheckManager
    ) {
        $this->dataCheckManager = $dataCheckManager;
    }

    /**
     * Main method for sync sequence block
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|void
     */
    public function execute(array $params, array $data, $flowCode, ConsoleOutput $consoleOutput)
    {
        echo "No check address".PHP_EOL;die;
        // check customer addresses
        $startExport = microtime(true);
        $this->dataCheckManager->checkCustomerAddresses();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Check customer addresses time : ' . number_format($totalExport, 6));
    }
}
