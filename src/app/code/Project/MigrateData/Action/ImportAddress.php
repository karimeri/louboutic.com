<?php

namespace Project\MigrateData\Action;

use Project\MigrateData\Manager\AddressCsvManager;
use Magento\Framework\Filesystem;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\ImportExport\Model\Import as ImportModel;
use Magento\ImportExport\Model\Import\Adapter as ImportAdapter;
use Magento\ImportExport\Model\ImportFactory;
use Magento\Framework\App\State;

/**
 * Class ImportAddress
 *
 * @package Project\MigrateData\Action
 * @author Synolia <contact@synolia.com>
 */
class ImportAddress implements ActionInterface
{
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $filesystem;
    /**
     * @var \Project\MigrateData\Manager\AddressCsvManager
     */
    private $addressCsvManager;
    /**
     * @var ImportModel
     */
    private $import;

    private $consoleOutput;

    /**
     * ImportAddress constructor.
     *
     * @param Filesystem $filesystem
     * @param AddressCsvManager $addressCsvManager
     * @param State $state
     * @param ImportFactory $importFactory
     */
    public function __construct(
        Filesystem $filesystem,
        AddressCsvManager $addressCsvManager,
        State $state,
        ImportFactory $importFactory
    ) {

        try {
            $state->setAreaCode('adminhtml');
        } catch (\Exception $e) {
            //area code already set
        }

        $this->filesystem           = $filesystem;
        $this->addressCsvManager    = $addressCsvManager;
        $this->import               = $importFactory->create();
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|void
     * @throws \Exception
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \TypeError
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $this->consoleOutput=$consoleOutput;

        // Export Address
        $startExport = microtime(true);
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Start Export Address time : ' . number_format($totalExport, 6));
        $count = $this->addressCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('End Export Address time : ' . number_format($totalExport, 6));
        $consoleOutput->writeInfo('Nb addresses exported : ' . $count);
        $this->addressCsvManager->addrReport();

        // Import Address
        $startImport = microtime(true);
        $consoleOutput->writeSuccess('Start Import Address time : ' . number_format(microtime(true) - $startImport, 6));
        $this->processCsv(AddressCsvManager::CSV_LOCATION);
        $consoleOutput->writeSuccess('End Import Address time : ' . number_format(microtime(true) - $startImport, 6));
    }

    /**
     * @param $pathFile
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function processCsv($pathFile)
    {
        $this->import->setData($this->prepareData());

        $this->import->setData($this->prepareData());

        $consoleOutput=$this->consoleOutput;
        $source = ImportAdapter::findAdapterFor(
            $pathFile,
            $this->filesystem->getDirectoryRead(DirectoryList::ROOT),
            $this->prepareData()[ImportModel::FIELD_FIELD_SEPARATOR]
        );

        $startImport = microtime(true);
        $consoleOutput->writeSuccess('Start validateSource Address time : ' . $pathFile);
        $validationResult = $this->import->validateSource($source);
        $consoleOutput->writeSuccess('End validateSource Address time : ' . number_format(microtime(true) - $startImport, 6));

        if (!$validationResult) {
            $errors = $this->import->getFormatedLogTrace();
            throw new \Exception('Error on validate source: '.PHP_EOL.$errors);
        } else {
            $startImport = microtime(true);
            $consoleOutput->writeSuccess('Start importSource Address : ' . $pathFile);
            $result = $this->import->importSource();
            $consoleOutput->writeSuccess('End importSource Address time : ' . number_format(microtime(true) - $startImport, 6));
            return $result;
        }
    }

    /**
     * @return array
     */
    protected function prepareData()
    {
        $data = [
            'entity' => 'customer_address',
            'behavior' => 'add_update',
            'validation_strategy' => 'validation-skip-errors',
            'allowed_error_count' => '100000',
            '_import_field_separator' => ';',
            '_import_multiple_value_separator' => ',',
            'import_images_file_dir' => "",
        ];

        return $data;
    }
}
