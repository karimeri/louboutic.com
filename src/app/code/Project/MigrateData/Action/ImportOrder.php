<?php

namespace Project\MigrateData\Action;

use Project\MigrateData\Helper\CsvHelper;
use Project\MigrateData\Manager\OrderCommentCsvManager;
use Project\MigrateData\Manager\OrderCsvManager;
use Project\MigrateData\Manager\OrderLineCsvManager;
use Synolia\OrderArchive\Model\Import\Processor;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class ImportOrder
 *
 * @package Project\MigrateData\Action
 * @author Synolia <contact@synolia.com>
 */
class ImportOrder implements ActionInterface
{
    /**
     * @var \Project\MigrateData\Manager\OrderCsvManager
     */
    private $orderCsvManager;
    /**
     * @var \Project\MigrateData\Manager\OrderLineCsvManager
     */
    private $orderLineCsvManager;
    /**
     * @var \Project\MigrateData\Manager\OrderCommentCsvManager
     */
    private $orderCommentCsvManager;
    /**
     * @var CsvHelper
     */
    private $csvHelper;

    /**
     * ImportOrder constructor.
     *
     * @param OrderCsvManager $orderCsvManager
     * @param OrderLineCsvManager $orderLineCsvManager
     * @param OrderCommentCsvManager $orderCommentCsvManager
     * @param CsvHelper $csvHelper
     */
    public function __construct(
        OrderCsvManager $orderCsvManager,
        OrderLineCsvManager $orderLineCsvManager,
        OrderCommentCsvManager $orderCommentCsvManager,
        CsvHelper $csvHelper
    ) {
        $this->orderCsvManager        = $orderCsvManager;
        $this->orderLineCsvManager    = $orderLineCsvManager;
        $this->orderCommentCsvManager = $orderCommentCsvManager;
        $this->csvHelper              = $csvHelper;
    }

    /**
     * Main method for sync sequence block
     *
     * @param array $params
     * @param array $data
     * @param $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     *
     * @return void
     * @throws \InvalidArgumentException
     * @throws \League\Csv\Exception
     * @throws \LogicException
     * @throws \TypeError
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        // Export primary comments to merge them in order
        $startExport = microtime(true);
        $this->orderCommentCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Comment Order time : ' . number_format($totalExport, 6));

        // Export Orders Headers
        $startExport = microtime(true);
        $this->orderCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Header Order time : ' . number_format($totalExport, 6));

        // Import Orders Headers
        $startImport = microtime(true);
        $this->csvHelper->processCsv(OrderCsvManager::CSV_LOCATION, Processor::TYPE_HEADERS);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('Import Header Order time : ' . number_format($totalImport, 6));

        // Export Orders Items
        $startExport = microtime(true);
        $this->orderLineCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Lines Order time : ' . number_format($totalExport, 6));

        // Import Orders Items
        $startImport = microtime(true);
        $this->csvHelper->processCsv(OrderLineCsvManager::CSV_LOCATION, Processor::TYPE_ITEMS);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('Import Line Order time : ' . number_format($totalImport, 6));
    }
}
