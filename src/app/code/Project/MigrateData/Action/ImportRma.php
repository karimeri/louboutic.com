<?php

namespace Project\MigrateData\Action;

use Project\MigrateData\Manager\RmaCommentCsvManager;
use Project\MigrateData\Manager\RmaCsvManager;
use Project\MigrateData\Manager\RmaLineCsvManager;
use Project\MigrateData\Helper\CsvHelper;
use Synolia\Sync\Model\Action\ActionInterface;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\OrderArchive\Model\Import\Processor;

/**
 * Class ImportRma
 *
 * @package Project\MigrateData\Action
 * @author Synolia <contact@synolia.com>
 */
class ImportRma implements ActionInterface
{
    /**
     * @var \Project\MigrateData\Manager\RmaCommentCsvManager
     */
    private $rmaCommentCsvManager;
    /**
     * @var \Project\MigrateData\Manager\RmaCsvManager
     */
    private $rmaCsvManager;
    /**
     * @var \Project\MigrateData\Manager\RmaLineCsvManager
     */
    private $rmaLineCsvManager;
    /**
     * @var CsvHelper
     */
    private $csvHelper;

    /**
     * ImportRma constructor.
     *
     * @param RmaCommentCsvManager $rmaCommentCsvManager
     * @param RmaCsvManager $rmaCsvManager
     * @param RmaLineCsvManager $rmaLineCsvManager
     * @param CsvHelper $csvHelper
     */
    public function __construct(
        RmaCommentCsvManager $rmaCommentCsvManager,
        RmaCsvManager $rmaCsvManager,
        RmaLineCsvManager $rmaLineCsvManager,
        CsvHelper $csvHelper
    ) {
        $this->rmaCommentCsvManager = $rmaCommentCsvManager;
        $this->rmaCsvManager        = $rmaCsvManager;
        $this->rmaLineCsvManager    = $rmaLineCsvManager;
        $this->csvHelper            = $csvHelper;
    }

    /**
     * Main method for sync sequence block
     *
     * @param array $params
     * @param array $data
     * @param $flowCode
     * @param \Synolia\Sync\Console\ConsoleOutput $consoleOutput
     *
     * @return void
     * @throws \InvalidArgumentException
     * @throws \League\Csv\Exception
     * @throws \LogicException
     * @throws \TypeError
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {

        // Step 1 : get Rma
        $startExport = microtime(true);
        $this->rmaCommentCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Comment RMA time : ' . number_format($totalExport, 6));

        $startExport = microtime(true);
        $this->rmaCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Header RMA time : ' . number_format($totalExport, 6));

        $startImport = microtime(true);
        $this->csvHelper->processCsv(RmaCsvManager::CSV_LOCATION, Processor::TYPE_HEADERS_RMA);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('Import Header RMA time : ' . number_format($totalImport, 6));

        // Step 2 : get Items
        $startExport = microtime(true);
        $this->rmaLineCsvManager->export();
        $totalExport = microtime(true) - $startExport;
        $consoleOutput->writeSuccess('Export Lines RMA time : ' . number_format($totalExport, 6));

        $startImport = microtime(true);
        $this->csvHelper->processCsv(RmaLineCsvManager::CSV_LOCATION, Processor::TYPE_ITEMS_RMA);
        $totalImport = microtime(true) - $startImport;
        $consoleOutput->writeSuccess('Import Line RMA time : ' . number_format($totalImport, 6));
    }
}
