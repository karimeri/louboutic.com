<?php

namespace Project\MigrateData\Helper;

use Symfony\Component\Filesystem\Filesystem;
use Synolia\OrderArchive\Model\Import\Processor;
use Symfony\Component\Console\Output\ConsoleOutput as SymfonyOutput;
use Synolia\OrderArchive\Helper\Data as OrderArchiveHelper;

/**
 * Class CsvHelper
 * @package Project\MigrateData\Helper
 */
class CsvHelper
{
    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $filesystem;
    /**
     * @var \Synolia\OrderArchive\Model\Import\Processor
     */
    private $processor;
    /**
     * @var \Synolia\OrderArchive\Helper\Data
     */
    private $orderArchiveHelper;
    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    private $output;

    /**
     * CsvHelper constructor.
     *
     * @param Filesystem $filesystem
     * @param Processor $processor
     * @param OrderArchiveHelper $orderArchiveHelper
     * @param SymfonyOutput $output
     */
    public function __construct(
        Filesystem $filesystem,
        Processor $processor,
        OrderArchiveHelper $orderArchiveHelper,
        SymfonyOutput $output
    ) {
        $this->filesystem          = $filesystem;
        $this->processor           = $processor;
        $this->orderArchiveHelper  = $orderArchiveHelper;
        $this->output              = $output;
    }

    /**
     * @param string $filename
     * @param $type
     *
     * @return bool
     */
    public function processCsv($filename, $type)
    {
        try {
            if (!$this->filesystem->exists($filename)) {
                throw new \Exception($filename . ' doesn\'t exists');
            }

            if (\is_null($type) || !\array_key_exists($type, $this->orderArchiveHelper->getImportOptions())) {
                throw new \Exception('No correct type for import');
            }

            $result = $this->processor->importProcessForFile($filename, $type);

            if (\is_array($result) && count($result)) {
                $this->output->writeln('<error>');
                $this->output->writeln($result);
                $this->output->writeln('</error>');
            }

            if ($result === true) {
                $this->output->writeln('<info>Import Success for ' . $type . '</info>');

                return true;
            }
        } catch (\Exception $exception) {
            $this->output->writeln([
                '<error>',
                $exception->getMessage(),
                '</error>',
            ]);
        }

        return false;
    }
}
