<?php

namespace Project\MigrateData\Helper;

use Magento\Framework\DataObject;
use Magento\Framework\Webapi\Rest\Request\Deserializer\Json;
use Project\MigrateData\Model\Value;
use Project\Core\Manager\EnvironmentManager;
use Magento\Framework\DataObjectFactory;

/**
 * Class ValueHelper
 * @package Project\MigrateData\Helper
 */
class ValueHelper
{
    const STORE = "store";

    /**
     * Directory of json files for mapping data
     */
    const MAPPING_FILES_DIR = '/mapping/';

    /**
     * @var array
     */
    private $list;

    /**
     * @var array
     */
    private $types;

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * ValueHelper constructor.
     * @param EnvironmentManager $environmentManager
     * @param Json $json
     * @param DataObjectFactory $dataObjectFactory
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function __construct(
        EnvironmentManager $environmentManager,
        Json $json,
        DataObjectFactory $dataObjectFactory
    ) {
        $this->environmentManager = $environmentManager;
        $this->json = $json;
        $this->dataObjectFactory = $dataObjectFactory;

        $this->types = array(
            static::STORE,
        );

        $storeList = $this->getFromJson(static::STORE);

        $this->list = array(
            static::STORE => $storeList,
        );
    }

    /**
     * @param string $type
     * @return bool
     */
    public function checkTypeExist(string $type)
    {
        if (!in_array($type, $this->types)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $type
     * @param string $method
     * @param string $key
     * @return Value
     * @throws \Exception
     */
    private function getValueBy(string $type, string $key)
    {
        $this->checkTypeExist($type);
        $list = $this->list[$type];

        $item = $list->getData($key);

        return $item;
    }

    /**
     * @param $type
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function getValue($type, $data)
    {
        $value = "";
        /** @var Value $value */
        $value = $this->getValueBy($type, $data);

        return $value;
    }

    /**
     * @param $type
     * @return DataObject
     * @throws \Magento\Framework\Webapi\Exception
     */
    private function getFromJson($type)
    {
        $environnement = $this->environmentManager->getEnvironment();

        $data = [];
        // @codingStandardsIgnoreLine
        $file = \file_get_contents(dirname(__DIR__) . self::MAPPING_FILES_DIR . DIRECTORY_SEPARATOR .
            $environnement . DIRECTORY_SEPARATOR . strtolower($type) . '.json');
        if ($file != false) {
            $data = $this->json->deserialize($file);
        }

        $object = $this->dataObjectFactory->create();
        $object->addData($data);

        return $object;
    }
}
