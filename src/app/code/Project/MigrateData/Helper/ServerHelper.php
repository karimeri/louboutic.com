<?php

namespace Project\MigrateData\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class ServerHelper
 *
 * @package Project\MigrateData\Helper
 * @author Synolia <contact@synolia.com>
 */
class ServerHelper extends AbstractHelper
{
    const XPATH_SQL_THROUGH_SSH = 'synchronizations_migratedata/migratedata_general/sql_connection_through_ssh';
    //
    const BASE_PATH = 'synchronizations_migratedata/migratedata_';
    const GROUP_SSH = 'server';
    const GROUP_DB = 'database';
    //
    const SSH_SERVER         = 'ssh_ip';
    const SSH_PORT           = 'ssh_port';
    const SSH_USER           = 'ssh_user';
    const SSH_AUTH_MODE      = 'ssh_auth_mode';
    const SSH_KEY_PATH       = 'ssh_key_path';
    const SSH_KEY_PASSPHRASE = 'ssh_key_passphrase';
    const SSH_PASSWORD       = 'ssh_password';
    const SSH_DIR            = 'ssh_dir';
    //
    const DB_SERVER   = 'db_ip';
    const DB_PORT     = 'db_port';
    const DB_USER     = 'db_user';
    const DB_PASSWORD = 'db_password';
    const DB_NAME     = 'db_name';

    /**
     * Check if we need use SSH to connect to Mysql
     * @return bool
     */
    public function isSshEnabled()
    {
        return (bool) $this->scopeConfig->getValue(self::XPATH_SQL_THROUGH_SSH);
    }

    /**
     * @return array
     */
    public function getSshConfiguration()
    {
        // prevent remote code execution
        $sshServer = null;
        $ip = $this->getValue(static::SSH_SERVER);
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            $sshServer = $ip;
        } else {
            $this->_logger->alert('Invalid IP address');
        }

        return [
            static::SSH_SERVER         => $sshServer,
            static::SSH_PORT           => $this->getValue(static::SSH_PORT),
            static::SSH_USER           => $this->getValue(static::SSH_USER),
            static::SSH_AUTH_MODE      => $this->getValue(static::SSH_AUTH_MODE),
            static::SSH_KEY_PATH       => $this->getValue(static::SSH_KEY_PATH),
            static::SSH_KEY_PASSPHRASE => $this->getValue(static::SSH_KEY_PASSPHRASE),
            static::SSH_PASSWORD       => $this->getValue(static::SSH_PASSWORD),
            static::SSH_DIR            => $this->getValue(static::SSH_DIR),
        ];
    }

    /**
     * @return array
     */
    public function getDatabaseConfiguration()
    {
        // prevent remote code execution
        $dbServer = null;
        $ip = $this->getValue(static::DB_SERVER, self::GROUP_DB);
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            $dbServer = $ip;
        } else {
            $this->_logger->alert('Invalid IP address');
        }

        return [
            static::DB_SERVER   => $dbServer,
            static::DB_PORT     => $this->getValue(static::DB_PORT, self::GROUP_DB),
            static::DB_USER     => $this->getValue(static::DB_USER, self::GROUP_DB),
            static::DB_PASSWORD => $this->getValue(static::DB_PASSWORD, self::GROUP_DB),
            static::DB_NAME     => $this->getValue(static::DB_NAME, self::GROUP_DB),
        ];
    }

    /**
     * @param string $path
     * @param string $type
     *
     * @return mixed
     */
    public function getValue($path, $type = self::GROUP_SSH)
    {
        return $this->scopeConfig->getValue($this->getFullPath($path, $type), ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    /**
     * @param $path
     * @param string $type
     *
     * @return string
     */
    public function getFullPath($path, $type = self::GROUP_SSH)
    {
        return static::BASE_PATH . $type . \DIRECTORY_SEPARATOR . $path;
    }
}
