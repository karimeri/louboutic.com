<?php

namespace Project\MigrateData\Helper;

/**
 * Class RmaCsvHelper
 * @package Project\MigrateData\Helper
 */
class RmaCsvHelper
{
    /**
     * @param $resolution
     * @return string
     */
    public function alterResolution($resolution)
    {
        switch ($resolution) {
            case "REF":
                $resolution = 'Refund';
                break;
            case "SIZ":
                $resolution = 'Size Exchange';
                break;
            case "EXC":
                $resolution = 'Style Exchange';
                break;
            case "MIX":
                $resolution = 'Mixed';
                break;
            case "RES":
                $resolution = 'Reship';
                break;
            default:
                $resolution = '';
                break;
        }

        return $resolution;
    }
}
