<?php

namespace Project\MigrateData\Service;

use Project\MigrateData\Service\Configuration;
use Project\MigrateData\Service\Local;
use Project\MigrateData\Service\PhpSecLib;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Model\Config\Source\AuthMode;

/**
 * Class ServerService
 *
 * @package Project\MigrateData\Service
 * @author Synolia <contact@synolia.com>
 */
class ServerService
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Project\MigrateData\Helper\ServerHelper
     */
    protected $serverHelper;

    /**
     * ServerService constructor.
     *
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param \Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        ServerHelper $serverHelper,
        Filesystem $filesystem
    ) {
        $this->serverHelper = $serverHelper;
        $this->filesystem = $filesystem;
    }

    /**
     * @return \Project\MigrateData\Service\PhpSecLib|\Project\MigrateData\Service\Local
     * @throws \LogicException
     */
    public function getServerInstance()
    {
        $server = $this->getLocalServer();

        if ($this->serverHelper->isSshEnabled()) {
            $server = $this->getSshServer();
        }

        return $server;
    }

    public function runSql($sql)
    {
        $host = $this->getServerInstance();
        $config = $this->serverHelper->getDatabaseConfiguration();

        $password = '';
        if (isset($config[ServerHelper::DB_PASSWORD])) {
            $password = '-p' . $config[ServerHelper::DB_PASSWORD];
        }
        // TODO : assert config is fill before launch sql test
        $command = 'mysql \
            -h ' . $config[ServerHelper::DB_SERVER] . ' \
            -u ' . $config[ServerHelper::DB_USER] . ' \
            ' . $password . ' \
            -P ' . $config[ServerHelper::DB_PORT] . ' \
            ' . $config[ServerHelper::DB_NAME] . '\
            -e "' . \str_replace('`', '\`', $sql) . '"';

        $result = $host->run($command);

        return $result;
    }

    /**
     * @return \Project\MigrateData\Service\PhpSecLib
     * @throws \LogicException
     */
    public function getSshServer()
    {
        $configuration = $this->getConfigurationForServer();
        $host = new PhpSecLib($configuration);

        return $host;
    }

    /**
     * @return \Project\MigrateData\Service\Local
     */
    public function getLocalServer()
    {
        $localhost = new Local();

        return $localhost;
    }

    /**
     * Test an SSH Connection and write rights
     *
     * @param \Project\MigrateData\Service\PhpSecLib $host
     * @param string $directory
     *
     * @return bool True if everything is OK
     * @throws \RuntimeException
     */
    public function testSSHConnection(PhpSecLib $host, $directory)
    {
        $testName = time();
        $commands = array(
            'test -e ' . $directory,
            'mkdir ' . $directory . DIRECTORY_SEPARATOR . $testName,
            'touch ' . $directory . DIRECTORY_SEPARATOR . $testName . '/test.php',
            'rm -rf ' . $directory . DIRECTORY_SEPARATOR . $testName,
        );
        $host->run(implode(' && ', $commands));

        return true;
    }

    /**
     * Test a DB connection
     *
     * @param \Project\MigrateData\Service\PhpSecLib $host
     *
     * @return bool
     */
    public function testDBConnection($host)
    {
        $config = $this->serverHelper->getDatabaseConfiguration();

        $password = '';
        if (isset($config[ServerHelper::DB_PASSWORD])) {
            $password = '-p' . $config[ServerHelper::DB_PASSWORD];
        }
        // TODO : assert config is fill before launch sql test
        $host->run(
            "mysql \
            -h " . $config[ServerHelper::DB_SERVER] . " \
            -u " . $config[ServerHelper::DB_USER] . " \
            " . $password . "\
            -P " . $config[ServerHelper::DB_PORT] . " \
            " . $config[ServerHelper::DB_NAME] . " \
            -e exit"
        );

        return true;
    }

    /**
     * @return \Project\MigrateData\Service\Configuration
     * @throws \LogicException
     */
    protected function getConfigurationForServer()
    {
        $sshConfig = $this->serverHelper->getSshConfiguration();

        $configuration = new Configuration(
            $sshConfig[ServerHelper::SSH_SERVER],
            $sshConfig[ServerHelper::SSH_SERVER],
            $sshConfig[ServerHelper::SSH_PORT]
        );

        $configuration->setUser($sshConfig[ServerHelper::SSH_USER]);

        switch ($sshConfig[ServerHelper::SSH_AUTH_MODE]) {
            case AuthMode::PASSWORD:
                $configuration->setPassword($sshConfig[ServerHelper::SSH_PASSWORD]);
                break;
            case AuthMode::KEY_FILE:
                $reader = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
                $path = $reader->getAbsolutePath($this->serverHelper->getValue(ServerHelper::SSH_KEY_PATH));
                $configuration->setAuthenticationMethod(Configuration::AUTH_BY_IDENTITY_FILE);
                $configuration->setPrivateKey($path);
                $configuration->setPublicKey($path . '.pub');
                $configuration->setPassPhrase($sshConfig[ServerHelper::SSH_KEY_PASSPHRASE]);
                break;
        }

        return $configuration;
    }
}
