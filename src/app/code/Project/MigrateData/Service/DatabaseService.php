<?php

namespace Project\MigrateData\Service;

use Magento\Framework\App\ResourceConnection\ConnectionFactory;
use Project\MigrateData\Helper\ServerHelper;

/**
 * Class DatabaseService
 *
 * @package Project\MigrateData\Service
 * @author Synolia <contact@synolia.com>
 */
class DatabaseService
{
    /**
     * @var \Magento\Framework\App\ResourceConnection\ConnectionFactory
     */
    private $connectionFactory;
    /**
     * @var \Project\MigrateData\Helper\ServerHelper
     */
    private $serverHelper;
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * DatabaseService constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection\ConnectionFactory $connectionFactory
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     */
    public function __construct(
        ConnectionFactory $connectionFactory,
        ServerHelper $serverHelper
    ) {
        $this->connectionFactory = $connectionFactory;
        $this->serverHelper = $serverHelper;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     * @throws \InvalidArgumentException
     */
    public function getInstance()
    {
        if (null === $this->connection) {
            $config = $this->getConfiguration();
            /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connexion */
            $this->connection = $this->connectionFactory->create($config);
        }

        return $this->connection;
    }

    /**
     * @return array
     */
    protected function getConfiguration()
    {
        $storeConfig = $this->serverHelper->getDatabaseConfiguration();
        $config = [
            'host' => $storeConfig[ServerHelper::DB_SERVER] . ':' . $storeConfig[ServerHelper::DB_PORT] ,
            'dbname' => $storeConfig[ServerHelper::DB_NAME],
            'username' => $storeConfig[ServerHelper::DB_USER],
            'password' => $storeConfig[ServerHelper::DB_PASSWORD],
            'model' => 'mysql4',
            'engine' => 'innodb',
            'initStatements' => 'SET NAMES utf8;',
            'active' => '1',
        ];

        return $config;
    }
}
