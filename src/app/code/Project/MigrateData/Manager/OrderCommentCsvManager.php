<?php

namespace Project\MigrateData\Manager;

/**
 * Class OrderCommentCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class OrderCommentCsvManager extends AbstractCsvManager
{
    const FLAT_ORDER_COMMENT_TABLE = 'sales_flat_order_status_history';
    const FLAT_ORDER_TABLE         = 'sales_flat_order';
    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/order_comments.csv';

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['c' => self::FLAT_ORDER_COMMENT_TABLE], $this->getOrderCommentSaleFlatColumns())
            ->joinLeft(
                ['o' => self::FLAT_ORDER_TABLE],
                'o.entity_id = c.parent_id',
                $this->getOrderSaleFlatColumns()
            )->where('c.entity_name = \'order\'', null, 'string');
    }

    /**
     * @return array
     */
    private function getOrderCommentSaleFlatColumns()
    {
        return [
            'date'              => 'created_at',
            'customer_notified' => 'is_customer_notified',
            'visible_on_front'  => 'is_visible_on_front',
            'author'            => 'author',
            'status'            => 'status',
            'comment'           => 'comment',
        ];
    }

    /**
     * @return array
     */
    private function getOrderSaleFlatColumns()
    {
        return [
            'order_number' => 'increment_id',
        ];
    }
}
