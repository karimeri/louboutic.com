<?php

namespace Project\MigrateData\Manager;

/**
 * Class RmaCommentCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class RmaCommentCsvManager extends AbstractCsvManager
{
    const ENTERPRISE_RMA_TABLE      = 'enterprise_rma';
    const ENTERPRISE_RMA_COMMENT_TABLE  = 'enterprise_rma_status_history';

    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/rma_comments.csv';

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['rma_comment' => self::ENTERPRISE_RMA_COMMENT_TABLE], $this->getRmaCommentColumns())
            ->joinLeft(
                ['rma' => self::ENTERPRISE_RMA_TABLE],
                'rma.entity_id = rma_comment.rma_entity_id',
                $this->getRmaColumns()
            );
    }

    /**
     * @return array
     */
    private function getRmaCommentColumns()
    {
        return [
            'date'              => 'created_at',
            'customer_notified' => 'is_customer_notified',
            'visible_on_front'  => 'is_visible_on_front',
            'author'            => 'author',
            'status'            => 'status',
            'comment'           => 'comment',
        ];
    }

    /**
     * @return array
     */
    private function getRmaColumns()
    {
        return [
            'rma_id' => 'entity_id',
        ];
    }
}
