<?php

namespace Project\MigrateData\Manager;

use League\Csv\Reader;
use Magento\Store\Model\StoreManagerInterface;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Helper\ValueHelper;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Service\ServerService;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class OrderCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class OrderCsvManager extends AbstractCsvManager
{
    const FLAT_ORDER_TABLE   = 'sales_flat_order';
    const FLAT_ADDRESS_TABLE = 'sales_flat_order_address';
    const FLAT_INVOICE_TABLE = 'sales_flat_invoice';
    const FLAT_PAYMENT_TABLE = 'sales_flat_order_payment';
    const CORE_STORE         = 'core_store';
    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/order_headers.csv';
    /**
     * @var array
     */
    private $comments = [];

    /**
     * @var \Project\MigrateData\Helper\ValueHelper
     */
    protected $valueHelper;

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * OrderCsvManager constructor.
     * @param ServerService $serverService
     * @param DatabaseService $databaseService
     * @param ServerHelper $serverHelper
     * @param StoreManagerInterface $storeManager
     * @param ValueHelper $valueHelper
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        ValueHelper $valueHelper,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->valueHelper = $valueHelper;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['o' => self::FLAT_ORDER_TABLE], $this->getOrderSaleFlatColumns())
            ->joinLeft(
                ['s' => self::FLAT_ADDRESS_TABLE],
                'o.entity_id = s.parent_id AND o.shipping_address_id = s.entity_id',
                $this->getAdressFlatColumns('shipping')
            )
            ->joinLeft(
                ['b' => self::FLAT_ADDRESS_TABLE],
                'o.entity_id = b.parent_id AND o.billing_address_id = b.entity_id',
                $this->getAdressFlatColumns('billing')
            )->joinLeft(
                ['i' => self::FLAT_INVOICE_TABLE],
                'o.entity_id = i.order_id',
                $this->getInvoiceFlatColumns()
            )->joinLeft(
                ['p' => self::FLAT_PAYMENT_TABLE],
                'o.entity_id = p.parent_id',
                $this->getPaymentFlatColumns()
            )->joinLeft(
                ['core_store' => self::CORE_STORE],
                'o.store_id = core_store.store_id',
                $this->getStoreCode()
            )
            ->group('o.increment_id');
    }

    /**
     * Get all comments in array to set them in csv
     */
    protected function init()
    {
        try {
            $commentCsv = Reader::createFromPath(OrderCommentCsvManager::CSV_LOCATION);
            $commentCsv->setDelimiter(';');
            $commentCsv->setHeaderOffset(0);

            $comments = [];
            foreach ($commentCsv->getRecords() as $commentLine) {
                $orderNumber = $commentLine['order_number'];
                unset($commentLine['order_number']);
                $comments[$orderNumber][] = $commentLine;
            }

            $this->comments = $comments;
        } catch (\Throwable $throwable) {
        }
    }

    protected function alterData($data)
    {
        // If comment csv exist, merge comment in order
        $data['comment'] = null;
        if (count($this->comments) && isset($this->comments[$data['number']])) {
            $data['comment'] = \json_encode($this->comments[$data['number']], \JSON_HEX_QUOT | \JSON_HEX_APOS);
        }

        if (empty($data['store']) || $data['store'] === 'NULL') {
            $data['store']  = $this->alterStore();
        }

        $storeCode = $this->valueHelper->getValue($this->valueHelper::STORE, $data['store']);
        $data['website'] = $this->getWebsiteCodeFromCache($storeCode);

        $data['shipping_fee_excl_tax'] = $data['shipping_fee'] - $data['base_shipping_tax_amount'];
        unset($data['base_shipping_tax_amount']);

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getOrderSaleFlatColumns()
    {
        return [
            'number'                   => 'increment_id',
            'email'                    => 'customer_email',
            'date'                     => 'created_at',
            'shipping_method'          => 'shipping_description',
            'status'                   => 'status',
            'total_amount_incl_tax'    => 'base_grand_total',
            'total_amount_excl_tax'    => 'base_subtotal',
            'discount_amount'          => 'discount_amount',
            'shipping_fee'             => 'base_shipping_amount',
            'shipping_fee_excl_tax'    => '',
            'currency_code'            => 'store_currency_code',
            'base_shipping_tax_amount' => 'base_shipping_tax_amount',
        ];
    }

    /**
     * @param $type
     *
     * @return array
     * @throws \LogicException
     */
    private function getAdressFlatColumns($type)
    {
        $authorizedTypes = ['shipping', 'billing'];

        if (!\in_array($type, $authorizedTypes, true)) {
            throw new \LogicException('Unknow type for address ' . $type);
        }

        return [
            $type . '_firstname'  => 'firstname',
            $type . '_lastname'   => 'lastname',
            $type . '_city'       => 'city',
            $type . '_country_id' => 'country_id',
            $type . '_postcode'   => 'postcode',
            $type . '_telephone'  => 'telephone',
            $type . '_street1'     => 'street',
        ];
    }

    /**
     * @return array
     */
    private function getInvoiceFlatColumns()
    {
        return [
            'invoice_number' => 'increment_id',
            'transaction_number' => 'transaction_id',
        ];
    }

    /**
     * @return array
     */
    private function getPaymentFlatColumns()
    {
        return [
            'payment_method' => 'method'
        ];
    }

    /**
     * @return array
     */
    private function getStoreCode()
    {
        return [
            'store' => 'code',
        ];
    }

    /**
     * @return string
     */
    private function alterStore()
    {
        switch ($this->environmentManager->getEnvironment()) {
            case Environment::EU:
                $store = "fr_fr";
                break;
            case Environment::HK:
                $store = "hk_en";
                break;
            case Environment::JP:
                $store = "jp_ja";
                break;
            case Environment::US:
                $store = "us_en";
                break;
        }

        return $store;
    }
}
