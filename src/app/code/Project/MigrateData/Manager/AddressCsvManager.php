<?php

namespace Project\MigrateData\Manager;

use League\Csv\Writer;
use Magento\Directory\Model\ResourceModel\Region\CollectionFactory as RegionCollection;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Model\Environment;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Helper\ValueHelper;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Service\ServerService;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class AddressCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class AddressCsvManager extends AbstractCsvManager
{
    const ADDRESS_STREET_LIMIT                  = 35;

    const CUSTOMER_ADDRESS_TABLE                = 'customer_address_entity';
    const ATTRIBUTE_TABLE                       = 'eav_attribute';
    const CUSTOMER_ADDRESS_ENTITY_INT_TABLE     = 'customer_address_entity_int';
    const CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE = 'customer_address_entity_varchar';
    const CUSTOMER_ADDRESS_ENTITY_TEXT_TABLE    = 'customer_address_entity_text';
    const ATTRIBUTE_OPTION_VALUE_TABLE          = 'eav_attribute_option_value';
    const CUSTOMER_TABLE                        = 'customer_entity';
    const CORE_STORE                            = 'core_store';

    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/customer_address.csv';

    const ATTRIBUTE_COUNTRY             = 'country_id';
    const ATTRIBUTE_CITY                = 'city';
    const ATTRIBUTE_POSTCODE            = 'postcode';
    const ATTRIBUTE_REGION              = 'region';
    const ATTRIBUTE_STREET              = 'street';
    const ATTRIBUTE_PHONE               = 'telephone';
    const ATTRIBUTE_DEFAULT_BILLING     = 'default_billing';
    const ATTRIBUTE_DEFAULT_SHIPPING    = 'default_shipping';
    const ATTRIBUTE_LASTNAME            = 'lastname';
    const ATTRIBUTE_FIRSTNAME           = 'firstname';

    private $rowsFirstLineUnder35 = [];
    private $rowsFirstLineSup35 = [];
    private $rowSup35Nobreakline = [];
    private $regionCleaned = [];

    /**
     * @var EnvironmentManager
     */
    private $valueHelper;

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Region collection instance
     *
     * @var RegionCollection
     */
    private $regionCollection;
    /**
     * Countries and regions
     *
     * Example array: array(
     *   [country_id_lowercased_1] => array(
     *     [region_code_lowercased_1]         => region_id_1,
     *     [region_default_name_lowercased_1] => region_id_1,
     *     ...,
     *     [region_code_lowercased_n]         => region_id_n,
     *     [region_default_name_lowercased_n] => region_id_n
     *   ),
     *   ...
     * )
     *
     * @var array
     */
    protected $_countryRegions = [];

    /**
     * Region ID to region default name pairs
     *
     * @var array
     */
    protected $_regions = [];

    /**
     * AddressCsvManager constructor.
     * @param ServerService $serverService
     * @param DatabaseService $databaseService
     * @param ServerHelper $serverHelper
     * @param EnvironmentManager $environmentManager
     * @param ValueHelper $valueHelper
     * @param StoreManagerInterface $storeManager
     * @param RegionCollection $regionColFactory
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        ValueHelper $valueHelper,
        EnvironmentManager $environmentManager,
        RegionCollection $regionColFactory
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->valueHelper = $valueHelper;
        $this->environmentManager = $environmentManager;
        $this->regionCollection = $regionColFactory->create();
    }

    public function addrReport()
    {
        $filepath = BP . \DIRECTORY_SEPARATOR . 'var/tmp/';

        echo "Valid transform rows under 35 (" . count($this->rowsFirstLineUnder35) . ") : " . PHP_EOL;
        $csv = Writer::createFromPath($filepath . 'addr-transform.csv', 'w+');
        $csv->setDelimiter(';');
        foreach ($this->rowsFirstLineUnder35 as $addr) {
            $csv->insertOne($addr);
        }

        echo "Error rows upper 35  (" . count($this->rowsFirstLineSup35) . ") : " . PHP_EOL;
        $csv = Writer::createFromPath($filepath . 'addr-error.csv', 'w+');
        $csv->setDelimiter(';');
        foreach ($this->rowsFirstLineSup35 as $addr) {
            $csv->insertOne($addr);
        }

        echo "Error rows without breakline  (" . count($this->rowSup35Nobreakline) . ") : " . PHP_EOL;
        $csv = Writer::createFromPath($filepath . 'addr-no-break.csv', 'w+');
        $csv->setDelimiter(';');
        foreach ($this->rowSup35Nobreakline as $addr) {
            $csv->insertOne($addr);
        }

        echo "Region cleaned  (" . count($this->regionCleaned) . ") : " . PHP_EOL;
        $csv = Writer::createFromPath($filepath . 'addr-no-break.csv', 'w+');
        $csv->setDelimiter(';');
        foreach ($this->regionCleaned as $addr) {
            $csv->insertOne($addr);
        }
    }
    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['ca' => self::CUSTOMER_ADDRESS_TABLE], [])
            ->joinLeft(
                ['country' => self::ATTRIBUTE_TABLE],
                'country.attribute_code = \'' . self::ATTRIBUTE_COUNTRY . '\' AND country.entity_type_id = 2',
                []
            )->joinLeft(
                ['ca_country' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_country.entity_id AND country.attribute_id = ca_country.attribute_id',
                $this->getAttributeCountryColumns()
            )->joinLeft(
                ['city' => self::ATTRIBUTE_TABLE],
                'city.attribute_code = \'' . self::ATTRIBUTE_CITY . '\'',
                []
            )->joinLeft(
                ['ca_city' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_city.entity_id AND city.attribute_id = ca_city.attribute_id',
                $this->getAttributeCityColumns()
            )->joinLeft(
                ['postcode' => self::ATTRIBUTE_TABLE],
                'postcode.attribute_code = \'' . self::ATTRIBUTE_POSTCODE . '\'',
                []
            )->joinLeft(
                ['ca_postcode' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_postcode.entity_id AND postcode.attribute_id = ca_postcode.attribute_id',
                $this->getAttributePostCodeColumns()
            )->joinLeft(
                ['region' => self::ATTRIBUTE_TABLE],
                'region.attribute_code = \'' . self::ATTRIBUTE_REGION . '\'',
                []
            )->joinLeft(
                ['ca_region' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_region.entity_id AND region.attribute_id = ca_region.attribute_id',
                $this->getAttributeRegionColumns()
            )->joinLeft(
                ['street' => self::ATTRIBUTE_TABLE],
                'street.attribute_code = \'' . self::ATTRIBUTE_STREET . '\'',
                []
            )->joinLeft(
                ['ca_street' => self::CUSTOMER_ADDRESS_ENTITY_TEXT_TABLE],
                'ca.entity_id = ca_street.entity_id AND street.attribute_id = ca_street.attribute_id',
                $this->getAttributeStreetColumns()
            )->joinLeft(
                ['telephone' => self::ATTRIBUTE_TABLE],
                'telephone.attribute_code = \'' . self::ATTRIBUTE_PHONE . '\' AND telephone.entity_type_id = 2',
                []
            )->joinLeft(
                ['ca_telephone' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_telephone.entity_id AND telephone.attribute_id = ca_telephone.attribute_id',
                $this->getAttributeTelephoneColumns()
            )->joinLeft(
                ['ce' => self::CUSTOMER_TABLE],
                'ce.entity_id = ca.parent_id',
                $this->getCustomerEmailColumns()
            )->joinLeft(
                ['lastname' => self::ATTRIBUTE_TABLE],
                'lastname.attribute_code = \'' . self::ATTRIBUTE_LASTNAME . '\' AND lastname.entity_type_id = 2',
                []
            )->joinLeft(
                ['ca_lastname' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_lastname.entity_id AND lastname.attribute_id = ca_lastname.attribute_id',
                $this->getAttributeLastnameColumns()
            )->joinLeft(
                ['firstname' => self::ATTRIBUTE_TABLE],
                'firstname.attribute_code = \'' . self::ATTRIBUTE_FIRSTNAME . '\' AND firstname.entity_type_id = 2',
                []
            )->joinLeft(
                ['ca_firstname' => self::CUSTOMER_ADDRESS_ENTITY_VARCHAR_TABLE],
                'ca.entity_id = ca_firstname.entity_id AND firstname.attribute_id = ca_firstname.attribute_id',
                $this->getAttributeFirstnameColumns()
            )->joinLeft(
                ['core_store' => self::CORE_STORE],
                'ce.store_id = core_store.store_id',
                $this->getStoreCode()
            )
            ->where('`ca_country`.`value` IS NOT NULL AND `ca_city`.`value` IS NOT NULL AND `ca_postcode`.`value` IS NOT NULL')
            ->where('`ca_country`.`value` NOT IN (\'US\', \'CA\')')
            //->where('`ca`.`entity_id` > 730')
            //->where('`ca`.`parent_id` NOT IN (10941, 27555, 25218, 17733)')
            //->limit(10000)
            ;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function alterData($data)
    {
        $this->initCountryRegions();
        if (empty($data['store_code']) || $data['store_code'] === 'NULL') {
            switch ((int)$data['_ce_website_id']) {
                case 1  : $data['store_code'] = 'fr_fr'; break;
                case 4  : $data['store_code'] = 'it_en'; break;
                case 5  : $data['store_code'] = 'uk_en'; break;
                case 7  : $data['store_code'] = 'nl_en'; break;
                case 8  : $data['store_code'] = 'de_en'; break;
                case 9  : $data['store_code'] = 'lu_en'; break;
                case 10 : $data['store_code'] = 'be_fr'; break;
                case 11 : $data['store_code'] = 'es_en'; break;
                case 12 : $data['store_code'] = 'ch_en'; break;
                case 13 : $data['store_code'] = 'at_en'; break;
                case 14 : $data['store_code'] = 'ie_en'; break;
                case 15 : $data['store_code'] = 'pt_en'; break;
                case 16 : $data['store_code'] = 'mc_fr'; break;
                case 17 : $data['store_code'] = 'gr_en'; break;
                default :
                    $data['store_code'] = $this->alterStore();
                    echo "Default Store for " . $data['_email'] . ' => ' . $data['store_code'] . PHP_EOL;
                    break;
            }
        }

        $data['_store'] = $this->valueHelper->getValue($this->valueHelper::STORE, $data['store_code']);
        $data['website_id'] = $this->storeManager->getStore($data['_store'])->getWebsiteId();
        $data['_website'] = $this->storeManager->getWebsite($data['website_id'])->getCode();

        $data['_entity_id'] = '';

        if (empty($data['telephone']) || $data['telephone'] === 'NULL') {
            $data['telephone'] = null;
        }

        if (empty($data['firstname']) || $data['firstname'] === 'NULL') {
            $data['firstname'] = null;
        }

        if (empty($data['lastname']) || $data['lastname'] === 'NULL') {
            $data['lastname'] = null;
        }

        if ($data['region'] === 'NULL') {
            $data['region'] = null;
        }
        if (!is_null($data['region'])) {
            if (false === $this->getCountryRegionId($data['country_id'], $data['region'])) {
                $this->regionCleaned[] = $data;
                $data['region'] = null;
            }
        }

        unset($data['store_code']);
        unset($data['_store']);
        unset($data['website_id']);
        unset($data['_ce_entity_id']);
        unset($data['_ce_website_id']);

        if (strlen($data['street']) > self::ADDRESS_STREET_LIMIT) {
            if (strpos($data['street'], "\n") !== false) {
                $street = explode("\n", $data['street']);
                if (strlen($street[0]) > self::ADDRESS_STREET_LIMIT) {
                    $data['street'] = '';
                    $this->rowsFirstLineSup35[] = $data;
                    return null;
                } else {
                    $data['street'] = $street[0];
                    $this->rowsFirstLineUnder35[] = $data;
                }
            } else {
                $this->rowSup35Nobreakline[] = $data;
                return null;
            }
        }

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getAttributeCountryColumns()
    {
        return [
            'country_id'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeCityColumns()
    {
        return [
            'city'  => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributePostCodeColumns()
    {
        return [
            'postcode'  => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeRegionColumns()
    {
        return [
            'region'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeStreetColumns()
    {
        return [
            'street'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeTelephoneColumns()
    {
        return [
            'telephone' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getCustomerEmailColumns()
    {
        return [
            '_email' => 'email',
            '_ce_entity_id' => 'entity_id',
            '_ce_website_id' => 'website_id',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeFirstnameColumns()
    {
        return [
            'firstname' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeLastnameColumns()
    {
        return [
            'lastname' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getStoreCode()
    {
        return [
            'store_code'    => 'code',
        ];
    }


    /**
     * @return int
     */
    private function alterStore()
    {
        switch ($this->environmentManager->getEnvironment()) {
            case Environment::EU:
                $storeCode = "fr_fr";
                break;
            case Environment::HK:
                $storeCode = "hk_en";
                break;
            case Environment::JP:
                $storeCode = "jp_ja";
                break;
            case Environment::US:
                $storeCode = "us_en";
                break;
        }

        return $storeCode;
    }

    /**
     * Initialize country regions hash for clever recognition
     *
     * @return $this
     */
    protected function initCountryRegions()
    {
        if (empty($this->_countryRegions)) {
            $startExport = microtime(true);
            /** @var $region \Magento\Directory\Model\Region */
            foreach ($this->regionCollection as $region) {
                $countryNormalized = strtolower($region->getCountryId());
                $regionCode = strtolower($region->getCode());
                $regionName = strtolower($region->getDefaultName());
                $this->_countryRegions[$countryNormalized][$regionCode] = $region->getId();
                $this->_countryRegions[$countryNormalized][$regionName] = $region->getId();
                $this->_regions[$region->getId()] = $region->getDefaultName();
            }
            echo 'Initializing country regions time : ' . number_format(microtime(true) - $startExport, 6) . PHP_EOL;
        }
        return $this;
    }

    /**
     * Get RegionID from the initialized data
     *
     * @param string $countryId
     * @param string $region
     * @return bool|int
     */
    private function getCountryRegionId(string $countryId, string $region)
    {
        $countryNormalized = strtolower($countryId);
        $regionNormalized = strtolower($region);

        if (isset($this->_countryRegions[$countryNormalized])
            && isset($this->_countryRegions[$countryNormalized][$regionNormalized])) {
            return $this->_countryRegions[$countryNormalized][$regionNormalized];
        }

        return false;
    }
}
