<?php

namespace Project\MigrateData\Manager;

use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Service\ServerService;
use Magento\Store\Model\StoreManagerInterface;
use Project\MigrateData\Helper\ValueHelper;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class OrderLineCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class OrderLineCsvManager extends AbstractCsvManager
{
    const FLAT_ORDER_TABLE                = 'sales_flat_order';
    const FLAT_ORDER_ITEM_TABLE           = 'sales_flat_order_item';
    const ATTRIBUTE_TABLE                 = 'eav_attribute';
    const PRODUCT_ATTRIBUTE_VARCHAR_TABLE = 'catalog_product_entity_varchar';
    const ATTRIBUTE_COLOR                 = 'color_product_page';
    const CORE_STORE                      = 'core_store';
    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/order_items.csv';

    /**
     * @var \Project\MigrateData\Helper\ValueHelper
     */
    protected $valueHelper;

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * OrderLineCsvManager constructor.
     * @param ServerService $serverService
     * @param DatabaseService $databaseService
     * @param ServerHelper $serverHelper
     * @param StoreManagerInterface $storeManager
     * @param ValueHelper $valueHelper
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        ValueHelper $valueHelper,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->valueHelper = $valueHelper;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['i' => self::FLAT_ORDER_ITEM_TABLE], $this->getOrderItemSaleFlatColumns())
            ->joinLeft(
                ['o' => self::FLAT_ORDER_TABLE],
                'o.entity_id = i.order_id',
                $this->getOrderSaleFlatColumns()
            )->joinLeft(
                ['a' => self::ATTRIBUTE_TABLE],
                'a.attribute_code LIKE \'' . self::ATTRIBUTE_COLOR . '\'',
                []
            )->joinLeft(
                ['c' => self::PRODUCT_ATTRIBUTE_VARCHAR_TABLE],
                'c.entity_id = i.product_id AND c.attribute_id = a.attribute_id AND c.store_id = i.store_id',
                ['product_color' => 'value']
            )->joinLeft(
                ['core_store' => self::CORE_STORE],
                'i.store_id = core_store.store_id',
                $this->getStoreCode()
            )
            ->where('i.product_type = \'configurable\'', null, 'string')
            ->orWhere('i.product_type = \'simple\' AND i.parent_item_id is NULL', null, 'string');
    }

    /**
     * {@inheritDoc}
     */
    protected function alterData($data)
    {
        if (isset($data['product_options'])) {
            $productOptions = \str_replace('\"', '"', $data['product_options']);
            $serialized = \unserialize($productOptions, ['allowed_classes' => false]);
            unset($data['product_options']);
            $data['product_size'] = null;
            if (isset($serialized['attributes_info'][0]['value'])) {
                $data['product_size'] = $serialized['attributes_info'][0]['value'];
            }
        }

        if (empty($data['store']) || $data['store'] === 'NULL') {
            $data['store']  = $this->alterStore();
        }

        $storeCode = $this->valueHelper->getValue($this->valueHelper::STORE, $data['store']);
        $data['website'] = $this->getWebsiteCodeFromCache($storeCode);

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getOrderItemSaleFlatColumns()
    {
        return [
            'product_sku'     => 'sku',
            'product_name'    => 'name',
            'product_color'   => '',
            'product_size'    => '',
            'product_price'   => 'price',
            'amount_incl_tax' => 'row_total_incl_tax',
            'amount_excl_tax' => 'row_total',
            'discount_amount' => 'discount_amount',
            'qty'             => 'qty_ordered',
            'qty_returned'    => 'qty_returned',
            'qty_invoiced'    => 'qty_invoiced',
            'qty_shipped'     => 'qty_shipped',
            'qty_refunded'    => 'qty_refunded',
            'product_options' => 'product_options',
        ];
    }

    /**
     * @return array
     */
    private function getOrderSaleFlatColumns()
    {
        return [
            'order_number' => 'increment_id',
        ];
    }

    /**
     * @return array
     */
    private function getStoreCode()
    {
        return [
            'store' => 'code',
        ];
    }

    /**
     * @return string
     */
    private function alterStore()
    {
        switch ($this->environmentManager->getEnvironment()) {
            case Environment::EU:
                $store = "fr_fr";
                break;
            case Environment::HK:
                $store = "hk_en";
                break;
            case Environment::JP:
                $store = "jp_ja";
                break;
            case Environment::US:
                $store = "us_en";
                break;
        }

        return $store;
    }
}
