<?php

namespace Project\MigrateData\Manager;

use League\Csv\Reader;
use League\Csv\Writer;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Service\ServerService;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class AbstractCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
abstract class AbstractCsvManager
{
    /**
     * @var \Project\MigrateData\Service\DatabaseService
     */
    protected $databaseService;

    /**
     * @var \Project\MigrateData\Service\ServerService
     */
    protected $serverService;

    /**
     * @var \Project\MigrateData\Helper\ServerHelper
     */
    protected $serverHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $websiteCodeCache = [];

    /**
     * AbstractCsvManager constructor.
     *
     * @param \Project\MigrateData\Service\ServerService $serverService
     * @param \Project\MigrateData\Service\DatabaseService $databaseService
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager
    ) {
        $this->databaseService = $databaseService;
        $this->serverService   = $serverService;
        $this->serverHelper    = $serverHelper;
        $this->storeManager    = $storeManager;
    }

    /**
     * @return int
     * @throws \InvalidArgumentException
     * @throws \League\Csv\CannotInsertRecord
     * @throws \League\Csv\Exception
     * @throws \TypeError
     */
    public function export()
    {
        $this->init();
        $select = $this->getSelect();

        $csv = Writer::createFromPath(static::CSV_LOCATION, 'w+');
        $csv->setDelimiter(';');

        if ($this->serverHelper->isSshEnabled()) {
            // Need to execute sql through SSH
            $result = $this->serverService->runSql((string) $select);
            $startExport = microtime(true);
            $result = \str_replace('"', '\"', $result);
            echo 'Transforming time : ' . number_format(microtime(true) - $startExport, 6) . PHP_EOL;

            $reader = Reader::createFromString($result);
            $reader->setDelimiter("\t");
            $reader->setHeaderOffset(0);

            $firstRow = $reader->fetchOne();
            $firstRow = $this->alterData($firstRow);
            // Set headers
            $csv->insertOne(\array_keys($firstRow));

            $count = 0;
            foreach ($reader->getRecords() as $data) {
                $data = \str_replace('\n', "\n", $data);
                $data = $this->alterData($data);
                if (!is_null($data)) {
                    $csv->insertOne($data);
                    $count++;
                }
            }
            return $count;
        } else {
            // use connection directly
            $firstRow = $this->databaseService->getInstance()->fetchRow($select);
            $firstRow = $this->alterData($firstRow);

            $csv->insertOne(\array_keys($firstRow));

            //phpcs:ignore Ecg.Performance.FetchAll.Found
            $result = $this->databaseService->getInstance()->fetchAll($select);

            foreach ($result as &$data) {
                $data = $this->alterData($data);
                $data = $this->sanitizeArray($data);
            }

            $csv->insertAll($result);
            return count($result);
        }
    }

    /**
     * @return \Magento\Framework\DB\Select
     */
    abstract protected function getSelect();

    /**
     * Way to alter data for Custom Csv manager
     *
     * @param $data
     *
     * @return mixed
     */
    protected function alterData($data)
    {
        return $data;
    }

    /**
     * Use init to initialize more private var if needed
     */
    protected function init()
    {
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function sanitizeArray($data)
    {
        return \array_map('stripslashes', $data);
    }

    /**
     * @param string $storeCode
     * @return string
     */
    protected function getWebsiteCodeFromCache($storeCode)
    {
        try {
            if (isset($this->websiteCodeCache[$storeCode])) {
                return $this->websiteCodeCache[$storeCode];
            } else {
                $storeId = $this->storeManager->getStore($storeCode)->getId();
                $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
                $websiteCode = $this->storeManager->getWebsite($websiteId)->getCode();
                $this->websiteCodeCache[$storeCode] = $websiteCode;

                return $websiteCode;
            }
        } catch (\Throwable $e) {
            return '';
        }
    }
}
