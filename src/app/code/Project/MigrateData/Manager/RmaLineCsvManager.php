<?php

namespace Project\MigrateData\Manager;

use Magento\Store\Model\StoreManagerInterface;
use Project\MigrateData\Service\ServerService;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Helper\RmaCsvHelper;

/**
 * Class RmaLineCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class RmaLineCsvManager extends AbstractCsvManager
{
    const ENTERPRISE_RMA_ITEM_TABLE     = 'enterprise_rma_item_entity';
    const ATTRIBUTE_TABLE   = 'eav_attribute';
    const ENTERPRISE_RMA_ITEM_INT_TABLE = 'enterprise_rma_item_entity_int';
    const ENTERPRISE_RMA_ITEM_VARCHAR_TABLE = 'enterprise_rma_item_entity_varchar';
    const ATTRIBUTE_OPTION_VALUE_TABLE  = 'eav_attribute_option_value';

    const ATTRIBUTE_CONDITION = 'condition';
    const ATTRIBUTE_REASON = 'reason';
    const ATTRIBUTE_REASON_OTHER = 'reason_other';
    const ATTRIBUTE_RESOLUTION = 'resolution';
    const ATTRIBUTE_ALLOCATION = 'allocation';
    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/rma_items.csv';

    /**
     * @var RmaCsvHelper
     */
    private $rmaCsvHelper;

    /**
     * RmaLineCsvManager constructor.
     * @param \Project\MigrateData\Service\ServerService $serverService
     * @param \Project\MigrateData\Service\DatabaseService $databaseService
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Project\MigrateData\Helper\RmaCsvHelper $rmaCsvHelper
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        RmaCsvHelper $rmaCsvHelper
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->rmaCsvHelper = $rmaCsvHelper;
    }

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['rma_item' => self::ENTERPRISE_RMA_ITEM_TABLE], $this->getEnterpriseRmaItemEntityColumns())
            ->joinLeft(
                ['condition' => self::ATTRIBUTE_TABLE],
                'condition.attribute_code LIKE \'' . self::ATTRIBUTE_CONDITION . '\'',
                []
            )->joinLeft(
                ['rma_condition' => self::ENTERPRISE_RMA_ITEM_INT_TABLE],
                'rma_item.entity_id = rma_condition.entity_id AND condition.attribute_id = rma_condition.attribute_id',
                []
            )->joinLeft(
                ['rma_condition_value' => self::ATTRIBUTE_OPTION_VALUE_TABLE],
                'rma_condition.value = rma_condition_value.option_id AND rma_condition_value.store_id = 0',
                $this->getAttributeConditionColumns()
            )->joinLeft(
                ['reason' => self::ATTRIBUTE_TABLE],
                'reason.attribute_code LIKE \'' . self::ATTRIBUTE_REASON . '\'',
                []
            )->joinLeft(
                ['rma_reason' => self::ENTERPRISE_RMA_ITEM_INT_TABLE],
                'rma_item.entity_id = rma_reason.entity_id AND reason.attribute_id = rma_reason.attribute_id',
                []
            )->joinLeft(
                ['rma_reason_value' => self::ATTRIBUTE_OPTION_VALUE_TABLE],
                'rma_reason.value = rma_reason_value.option_id AND rma_reason_value.store_id = 0',
                $this->getAttributeReasonColumns()
            )->joinLeft(
                ['resolution' => self::ATTRIBUTE_TABLE],
                'resolution.attribute_code LIKE \'' . self::ATTRIBUTE_RESOLUTION . '\'',
                []
            )->joinLeft(
                ['rma_resolution' => self::ENTERPRISE_RMA_ITEM_VARCHAR_TABLE],
                'rma_item.entity_id = rma_resolution.entity_id AND resolution.attribute_id = rma_resolution.attribute_id',
                $this->getAttributeResolutionColumns()
            )->joinLeft(
                ['allocation' => self::ATTRIBUTE_TABLE],
                'allocation.attribute_code LIKE \'' . self::ATTRIBUTE_ALLOCATION . '\'',
                []
            )->joinLeft(
                ['rma_allocation' => self::ENTERPRISE_RMA_ITEM_VARCHAR_TABLE],
                'rma_item.entity_id = rma_allocation.entity_id AND allocation.attribute_id = rma_allocation.attribute_id',
                $this->getAttributeAllocationColumns()
            )->joinLeft(
                ['reason_other' => self::ATTRIBUTE_TABLE],
                'reason_other.attribute_code LIKE \'' . self::ATTRIBUTE_REASON_OTHER . '\'',
                []
            )->joinLeft(
                ['rma_reason_other' => self::ENTERPRISE_RMA_ITEM_VARCHAR_TABLE],
                'rma_item.entity_id = rma_reason_other.entity_id AND reason_other.attribute_id = rma_reason_other.attribute_id',
                $this->getAttributeReasonOtherColumns()
            );
    }

    protected function alterData($data)
    {
        $data['resolution'] = $this->rmaCsvHelper->alterResolution($data['resolution']);

        $data = $this->alterReasonToReturn($data);

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getEnterpriseRmaItemEntityColumns()
    {
        return [
            'rma_id'            => 'rma_entity_id',
            'product_name'      => 'product_name',
            'product_sku'       => 'product_sku',
            'qty_requested'     => 'qty_requested',
            'qty_authorized'    => 'qty_authorized',
            'qty_returned'      => 'qty_returned',
            'qty_approved'      => 'qty_approved',
            'status'            => 'status',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeConditionColumns()
    {
        return [
            'item_condition' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeReasonColumns()
    {
        return [
            'reason_to_return' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeResolutionColumns()
    {
        return [
            'resolution' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeAllocationColumns()
    {
        return [
            'allocation' => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeReasonOtherColumns()
    {
        return [
            'reason_to_other_return' => 'value',
        ];
    }

    /**
     * @param $data
     * @return string
     */
    private function alterReasonToReturn($data)
    {
        if (!isset($data['reason_to_return'])) {
            $data['reason_to_return'] = $data['reason_to_other_return'];
        }

        unset($data['reason_to_other_return']);

        return $data;
    }
}
