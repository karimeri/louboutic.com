<?php

namespace Project\MigrateData\Manager;

use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Model\Environment;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Service\ServerService;
use Project\MigrateData\Helper\ValueHelper;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class CustomerCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class CustomerCsvManager extends AbstractCsvManager
{
    const CUSTOMER_TABLE                 = 'customer_entity';
    const ATTRIBUTE_TABLE                = 'eav_attribute';
    const CUSTOMER_ENTITY_INT_TABLE      = 'customer_entity_int';
    const CUSTOMER_ENTITY_VARCHAR_TABLE  = 'customer_entity_varchar';
    const CUSTOMER_ENTITY_DATETIME_TABLE = 'customer_entity_datetime';
    const ATTRIBUTE_OPTION_VALUE_TABLE   = 'eav_attribute_option_value';
    const CORE_STORE                     = 'core_store';

    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/customer.csv';

    const ATTRIBUTE_LASTNAME            = 'lastname';
    const ATTRIBUTE_FIRSTNAME           = 'firstname';
    const ATTRIBUTE_PREFIX              = 'prefix';
    const ATTRIBUTE_PASSWORD_HASH       = 'password_hash';
    const ATTRIBUTE_RP_TOKEN            = 'rp_token';
    const ATTRIBUTE_CREATED_IN          = 'created_in';
    const ATTRIBUTE_RP_TOKEN_CREATED_AT = 'rp_token_created_at';

    /**
     * @var ValueHelper
     */
    private $valueHelper;

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * CustomerCsvManager constructor.
     * @param ServerService $serverService
     * @param DatabaseService $databaseService
     * @param ServerHelper $serverHelper
     * @param ValueHelper $valueHelper
     * @param EnvironmentManager $environmentManager
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        ValueHelper $valueHelper,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->valueHelper = $valueHelper;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['ce' => self::CUSTOMER_TABLE], $this->getCustomerColumns())
            ->joinLeft(
                ['lastname' => self::ATTRIBUTE_TABLE],
                'lastname.attribute_code LIKE \'' . self::ATTRIBUTE_LASTNAME . '\' AND lastname.entity_type_id = 1',
                []
            )->joinLeft(
                ['ce_lastname' => self::CUSTOMER_ENTITY_VARCHAR_TABLE],
                'ce.entity_id = ce_lastname.entity_id AND lastname.attribute_id = ce_lastname.attribute_id',
                $this->getAttributeLastnameColumns()
            )->joinLeft(
                ['firstname' => self::ATTRIBUTE_TABLE],
                'firstname.attribute_code LIKE \'' . self::ATTRIBUTE_FIRSTNAME . '\' AND firstname.entity_type_id = 1',
                []
            )->joinLeft(
                ['ce_firstname' => self::CUSTOMER_ENTITY_VARCHAR_TABLE],
                'ce.entity_id = ce_firstname.entity_id AND firstname.attribute_id = ce_firstname.attribute_id',
                $this->getAttributeFirstnameColumns()
            )->joinLeft(
                ['prefix' => self::ATTRIBUTE_TABLE],
                'prefix.attribute_code LIKE \'' . self::ATTRIBUTE_PREFIX . '\' AND prefix.entity_type_id = 1',
                []
            )->joinLeft(
                ['ce_prefix' => self::CUSTOMER_ENTITY_VARCHAR_TABLE],
                'ce.entity_id = ce_prefix.entity_id AND prefix.attribute_id = ce_prefix.attribute_id',
                $this->getAttributePrefixColumns()
            )->joinLeft(
                ['passwordhash' => self::ATTRIBUTE_TABLE],
                'passwordhash.attribute_code LIKE \'' . self::ATTRIBUTE_PASSWORD_HASH . '\'',
                []
            )->joinLeft(
                ['ce_passwordhash' => self::CUSTOMER_ENTITY_VARCHAR_TABLE],
                'ce.entity_id = ce_passwordhash.entity_id AND passwordhash.attribute_id = ce_passwordhash.attribute_id',
                $this->getAttributePasswordHashColumns()
            )->joinLeft(
                ['rptoken' => self::ATTRIBUTE_TABLE],
                'rptoken.attribute_code LIKE \'' . self::ATTRIBUTE_RP_TOKEN . '\'',
                []
            )->joinLeft(
                ['ce_rptoken' => self::CUSTOMER_ENTITY_VARCHAR_TABLE],
                'ce.entity_id = ce_rptoken.entity_id AND rptoken.attribute_id = ce_rptoken.attribute_id',
                $this->getAttributeRpTokenColumns()
            )->joinLeft(
                ['createdin' => self::ATTRIBUTE_TABLE],
                'createdin.attribute_code LIKE \'' . self::ATTRIBUTE_CREATED_IN . '\'',
                []
            )->joinLeft(
                ['ce_createdin' => self::CUSTOMER_ENTITY_VARCHAR_TABLE],
                'ce.entity_id = ce_createdin.entity_id AND createdin.attribute_id = ce_createdin.attribute_id',
                $this->getAttributeCreatedInColumns()
            )->joinLeft(
                ['tokencreated' => self::ATTRIBUTE_TABLE],
                'tokencreated.attribute_code LIKE \'' . self::ATTRIBUTE_RP_TOKEN_CREATED_AT . '\'',
                []
            )->joinLeft(
                ['ce_tokencreated' => self::CUSTOMER_ENTITY_DATETIME_TABLE],
                'ce.entity_id = ce_tokencreated.entity_id AND tokencreated.attribute_id = ce_tokencreated.attribute_id',
                $this->getAttributeTokenCreatedColumns()
            )->joinLeft(
                ['core_store' => self::CORE_STORE],
                'ce.store_id = core_store.store_id',
                $this->getStoreCode()
            );
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    protected function alterData($data)
    {
        $data['disable_auto_group_change'] = 0;
        $data['group_id'] = 1;

        if (empty($data['store_code']) || $data['store_code'] === 'NULL') {
            switch ((int)$data['website_id']) {
                case 1  : $data['store_code'] = 'fr_fr'; break;
                case 4  : $data['store_code'] = 'it_en'; break;
                case 5  : $data['store_code'] = 'uk_en'; break;
                case 7  : $data['store_code'] = 'nl_en'; break;
                case 8  : $data['store_code'] = 'de_en'; break;
                case 9  : $data['store_code'] = 'lu_en'; break;
                case 10 : $data['store_code'] = 'be_fr'; break;
                case 11 : $data['store_code'] = 'es_en'; break;
                case 12 : $data['store_code'] = 'ch_en'; break;
                case 13 : $data['store_code'] = 'at_en'; break;
                case 14 : $data['store_code'] = 'ie_en'; break;
                case 15 : $data['store_code'] = 'pt_en'; break;
                case 16 : $data['store_code'] = 'mc_fr'; break;
                case 17 : $data['store_code'] = 'gr_en'; break;
                default :
                    $data['store_code'] = $this->alterStore();
                    echo "Default Store for " . $data['email'] . ' => ' . $data['store_code'] . PHP_EOL;
                    break;
            }
        }

        if (empty($data['firstname']) || $data['firstname'] === 'NULL') {
            $data['firstname'] = 'empty';
        }

        if (empty($data['lastname']) || $data['lastname'] === 'NULL') {
            $data['lastname'] = 'empty';
        }

        if (empty($data['prefix']) || $data['prefix'] === 'NULL') {
            $data['prefix'] = '';
        }

        if (strpos($data['password_hash'], ':') !== false) {
            $data['password_hash'] = $data['password_hash'] . ':1';
        } else {
            $data['password_hash'] = $data['password_hash'] . ':0';
        }

        $data['_store'] = $this->valueHelper->getValue($this->valueHelper::STORE, $data['store_code']);
        $data['store_id'] = $this->storeManager->getStore($data['_store'])->getId();
        $data['created_in'] = $this->storeManager->getStore($data['store_id'])->getName();

        $data['website_id'] = $this->storeManager->getStore($data['store_id'])->getWebsiteId();
        $data['_website'] = $this->storeManager->getWebsite($data['website_id'])->getCode();

        unset($data['store_code']);

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getCustomerColumns()
    {
        return [
            'email'      => 'email',
            'created_at' => 'created_at',
            'website_id' => 'website_id',
            'store_id'   => 'store_id',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeLastnameColumns()
    {
        return [
            'lastname'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeFirstnameColumns()
    {
        return [
            'firstname'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributePrefixColumns()
    {
        return [
            'prefix'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributePasswordHashColumns()
    {
        return [
            'password_hash'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeRpTokenColumns()
    {
        return [
            'rp_token'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeCreatedInColumns()
    {
        return [
            'created_in'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getAttributeTokenCreatedColumns()
    {
        return [
            'rp_token_created_at'    => 'value',
        ];
    }

    /**
     * @return array
     */
    private function getStoreCode()
    {
        return [
            'store_code'    => 'code',
        ];
    }

    /**
     * @return int
     */
    private function alterStore()
    {
        switch ($this->environmentManager->getEnvironment()) {
            case Environment::EU:
                $storeCode = "fr_fr";
                break;
            case Environment::HK:
                $storeCode = "hk_en";
                break;
            case Environment::JP:
                $storeCode = "jp_ja";
                break;
            case Environment::US:
                $storeCode = "us_en";
                break;
        }

        return $storeCode;
    }
}
