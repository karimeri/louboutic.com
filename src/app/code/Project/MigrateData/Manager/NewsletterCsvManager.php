<?php

namespace Project\MigrateData\Manager;

use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Model\Environment;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Service\ServerService;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class NewsletterCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class NewsletterCsvManager extends AbstractCsvManager
{
    const NEWSLETTER_SUBSCRIBER_TABLE   = 'newsletter_subscriber';
    const STORE_TABLE                   = 'core_store';
    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/subscriber_newsletter.csv';

    /**
     * @var EnvironmentManager
     */
    private $environmentManager;

    /**
     * NewsletterCsvManager constructor.
     * @param \Project\MigrateData\Service\ServerService $serverService
     * @param \Project\MigrateData\Service\DatabaseService $databaseService
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->environmentManager = $environmentManager;
    }

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['newsletter' => self::NEWSLETTER_SUBSCRIBER_TABLE], $this->getNewsletterColumns())
            ->joinLeft(
                ['store' => self::STORE_TABLE],
                'newsletter.store_id = store.store_id',
                $this->getStoreColumns()
            );
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function alterData($data)
    {
        $storeForbidden = [
            'admin',
            'ca_en_old',
            'jp_en',
            'ca_fr_old',
            'ot_cns',
            'ot_cnt',
            'us_fr',
            'ot_fr',
        ];

        if (\in_array($data['store'], $storeForbidden) || empty($data['store'])) {
            switch ($this->environmentManager->getEnvironment()) {
                case Environment::EU:
                    $data['store'] = "uk_en";
                    break;
                case Environment::HK:
                    $data['store'] = "hk_en";
                    break;
                case Environment::JP:
                    $data['store'] = "jp_ja";
                    break;
                case Environment::US:
                    $data['store'] = "us_en";
                    break;
            }
        }

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getNewsletterColumns()
    {
        return [
            'email'    => 'subscriber_email',
        ];
    }

    /**
     * @return array
     */
    private function getStoreColumns()
    {
        return [
            'store'    => 'code',
        ];
    }
}
