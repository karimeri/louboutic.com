<?php

namespace Project\MigrateData\Manager;

use Magento\Store\Model\StoreManagerInterface;
use Project\MigrateData\Service\ServerService;
use Project\MigrateData\Service\DatabaseService;
use Project\MigrateData\Helper\ServerHelper;
use Project\MigrateData\Helper\RmaCsvHelper;
use League\Csv\Reader;

/**
 * Class RmaCsvManager
 *
 * @package Project\MigrateData\Manager
 * @author Synolia <contact@synolia.com>
 */
class RmaCsvManager extends AbstractCsvManager
{
    const ENTERPRISE_RMA_TABLE   = 'enterprise_rma';
    const CUSTOMER_ENTITY_TABLE = 'customer_entity';
    const CSV_LOCATION = BP . \DIRECTORY_SEPARATOR . 'var/tmp/rma_headers.csv';

    /**
     * @var RmaCsvHelper
     */
    private $rmaCsvHelper;

    /**
     * @var array
     */
    private $comments = [];

    /**
     * RmaCsvManager constructor.
     * @param \Project\MigrateData\Service\ServerService $serverService
     * @param \Project\MigrateData\Service\DatabaseService $databaseService
     * @param \Project\MigrateData\Helper\ServerHelper $serverHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Project\MigrateData\Helper\RmaCsvHelper $rmaCsvHelper
     */
    public function __construct(
        ServerService $serverService,
        DatabaseService $databaseService,
        ServerHelper $serverHelper,
        StoreManagerInterface $storeManager,
        RmaCsvHelper $rmaCsvHelper
    ) {
        parent::__construct(
            $serverService,
            $databaseService,
            $serverHelper,
            $storeManager
        );

        $this->rmaCsvHelper = $rmaCsvHelper;
    }

    /**
     * Get all comments in array to set them in csv
     */
    protected function init()
    {
        try {
            $commentCsv = Reader::createFromPath(RmaCommentCsvManager::CSV_LOCATION);
            $commentCsv->setDelimiter(';');
            $commentCsv->setHeaderOffset(0);

            $comments = [];
            foreach ($commentCsv->getRecords() as $commentLine) {
                $rmaId = $commentLine['rma_id'];
                unset($commentLine['rma_id']);
                $comments[$rmaId][] = $commentLine;
            }

            $this->comments = $comments;
        } catch (\Throwable $throwable) {
        }
    }

    /**
     * @return \Magento\Framework\DB\Select
     * @throws \InvalidArgumentException
     * @throws \LogicException
     */
    protected function getSelect()
    {
        return $this->databaseService->getInstance()->select()
            ->from(['rma' => self::ENTERPRISE_RMA_TABLE], $this->getEnterpriseRmaColumns())
            ->joinLeft(
                ['c' => self::CUSTOMER_ENTITY_TABLE],
                'rma.customer_id = c.entity_id',
                $this->getCustomerEntityColumns()
            );
    }

    protected function alterData($data)
    {
        $data['comment'] = null;
        if (count($this->comments) && isset($this->comments[$data['rma_id']])) {
            $data['comment'] = \json_encode($this->comments[$data['rma_id']]);
        }

        $data['resolution'] = $this->rmaCsvHelper->alterResolution($data['resolution']);

        return parent::alterData($data);
    }

    /**
     * @return array
     */
    private function getEnterpriseRmaColumns()
    {
        return [
            'rma_id'        => 'entity_id',
            'order_id'      => 'order_increment_id',
            'resolution'    => 'resolution',
            'priority'      => 'priority',
            'category'      => 'category',
            'pickup_date'   => 'date_requested',
            'status'        => 'status',
        ];
    }

    /**
     * @return array
     */
    private function getCustomerEntityColumns()
    {
        return [
            'customer'        => 'email',
        ];
    }
}
