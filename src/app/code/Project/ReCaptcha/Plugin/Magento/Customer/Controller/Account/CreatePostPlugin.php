<?php
namespace Project\ReCaptcha\Plugin\Magento\Customer\Controller\Account;

use Magento\Customer\Controller\Account\CreatePost;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlFactory;
use Synolia\ReCaptcha\Helper\Config;

/**
 * Class CreatePostPlugin
 * @package Project\ReCaptcha\Plugin\Magento\Customer\Controller\Account
 * @author  Synolia <contact@synolia.com>
 */
class CreatePostPlugin
{
    /**
     * @var \Synolia\ReCaptcha\Helper\Config
     */
    protected $recaptchaConfig;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectFactory;
    /**
     * @var \Magento\Framework\UrlFactory
     */
    protected $urlFactory;
    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * Create constructor.
     * @param \Magento\Framework\App\RequestInterface              $request
     * @param \Synolia\ReCaptcha\Helper\Config                     $recaptchaConfig
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Magento\Framework\UrlFactory                        $urlFactory
     * @param \Magento\Framework\Escaper                           $escaper
     * @param \Magento\Framework\Message\ManagerInterface          $messageManager
     * @param \Magento\Framework\App\Response\RedirectInterface    $redirect
     */
    public function __construct(
        RequestInterface $request,
        Config $recaptchaConfig,
        RedirectFactory $redirectFactory,
        UrlFactory $urlFactory,
        Escaper $escaper,
        ManagerInterface $messageManager,
        RedirectInterface $redirect
    ) {
        $this->recaptchaConfig = $recaptchaConfig;
        $this->request = $request;
        $this->redirectFactory = $redirectFactory;
        $this->urlFactory = $urlFactory;
        $this->escaper = $escaper;
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
    }

    /**
     * @param \Magento\Customer\Controller\Account\CreatePost $subject
     * @param \Closure                                        $process
     * @return \Magento\Framework\Controller\Result\Redirect|mixed
     * @throws \Zend_Validate_Exception
     */
    public function aroundExecute(CreatePost $subject, \Closure $process)
    {
        if ($this->recaptchaConfig->isEnabled()) {
            $post = $this->request->getPostValue();

            if (!\Zend_Validate::is(\trim($post['g-recaptcha-response']), 'NotEmpty') ||
                !$this->recaptchaConfig->isValid($post['g-recaptcha-response'])
            ) {
                $this->messageManager->addErrorMessage(__('Invalid or missing CAPTCHA, please try again'));
                $resultRedirect = $this->redirectWithError();
                return $resultRedirect;
            }
        }

        return $process();
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function redirectWithError()
    {
        $urlModel = $this->urlFactory->create();
        $url = $urlModel->getUrl('*/*/create', ['_secure' => true]);

        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setUrl($this->redirect->error($url));

        return $resultRedirect;
    }
}
