<?php

namespace Project\Developer\Model\Magento\TemplateEngine\Decorator;

use Magento\Framework\View\TemplateEngineInterface;

/**
 * Class DebugHints
 * @package Project\Developer\Model\Magento\TemplateEngine\Decorator
 * @author Synolia <contact@synolia.com>
 */
class DebugHints extends \Magento\Developer\Model\TemplateEngine\Decorator\DebugHints
{
    /**
     * @var \Magento\Framework\View\TemplateEngineInterface
     */
    protected $subject;

    /**
     * DebugHints constructor.
     * @param \Magento\Framework\View\TemplateEngineInterface $subject
     * @param bool $showBlockHints
     */
    public function __construct(
        TemplateEngineInterface $subject,
        $showBlockHints
    ) {
        parent::__construct($subject, $showBlockHints);

        $this->subject = $subject;
    }

    /**
     * Insert debugging hints into the rendered block contents
     *
     * {@inheritdoc}
     */
    public function render(\Magento\Framework\View\Element\BlockInterface $block, $templateFile, array $dictionary = [])
    {
        $result = $this->subject->render($block, $templateFile, $dictionary);

        return $result;
    }
}
