<?php

namespace Project\GiftWrapping\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\CartFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Pricing\Helper\Data as PricingData;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\GiftWrapping\Helper\Data;
use Magento\GiftWrapping\Model\ResourceModel\Wrapping\CollectionFactory;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;
use Psr\Log\LoggerInterface;

/**
 * Class ConfigProvider
 * @package Project\GiftWrapping\Model
 */
class ConfigProvider extends \Magento\GiftWrapping\Model\ConfigProvider
{
    /**
     * @var \Project\Core\Helper\AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * ConfigProvider constructor.
     * @param CartFactory $checkoutCartFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Data $giftWrappingData
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $wrappingCollectionFactory
     * @param UrlInterface $urlBuilder
     * @param Repository $assetRepo
     * @param RequestInterface $request
     * @param LoggerInterface $logger
     * @param CheckoutSession $checkoutSession
     * @param PricingData $pricingHelper
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param TaxClassKeyInterfaceFactory $taxClassKeyFactory
     * @param AttributeSetHelper $attributeSetHelper
     */
    public function __construct(
        CartFactory $checkoutCartFactory,
        ProductRepositoryInterface $productRepository,
        Data $giftWrappingData,
        StoreManagerInterface $storeManager,
        CollectionFactory $wrappingCollectionFactory,
        UrlInterface $urlBuilder,
        Repository $assetRepo,
        RequestInterface $request,
        LoggerInterface $logger,
        CheckoutSession $checkoutSession,
        PricingData $pricingHelper,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        TaxClassKeyInterfaceFactory $taxClassKeyFactory,
        AttributeSetHelper $attributeSetHelper
    ) {
        parent::__construct($checkoutCartFactory, $productRepository, $giftWrappingData, $storeManager, $wrappingCollectionFactory, $urlBuilder, $assetRepo, $request, $logger, $checkoutSession, $pricingHelper, $quoteIdMaskFactory, $taxClassKeyFactory);
        $this->attributeSetHelper = $attributeSetHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return [
            'giftWrapping' => [
                'isBeauty' => $this->isBeauty(),
                'canDisplayGiftWrapping' => $this->canDisplayGiftWrapping(),
                'designCollectionCount' => $this->getDesignCollectionCount(),
                'allowForOrder' => $this->getAllowForOrder(),
                'allowForItems' => $this->getAllowForItems(),
                'giftWrappingAvailable' => $this->giftWrappingAvailable,
                'spacerSrc' => $this->getViewFileUrl('images/spacer.gif'),
                'displayWrappingBothPrices' => $this->getDisplayWrappingBothPrices(),
                'displayCardBothPrices' => $this->getDisplayCardBothPrices(),
                'displayGiftWrappingInclTaxPrice' => $this->getDisplayWrappingIncludeTaxPrice(),
                'displayCardInclTaxPrice' => $this->getDisplayCardIncludeTaxPrice(),
                'designsInfo' => $this->getDesignsInfo(),
                'itemsInfo' => $this->getItemsInfo(),
                'isAllowPrintedCard' => $this->getAllowPrintedCard(),
                'isAllowGiftReceipt' => $this->getAllowGiftReceipt(),
                'cardInfo' => $this->getCardInfo(),
                'appliedWrapping' => $this->getAppliedWrapping(),
                'appliedPrintedCard' => $this->getQuote()->getGwAddCard(),
                'appliedGiftReceipt' => $this->getQuote()->getGwAllowGiftReceipt(),
            ],
            'quoteId' => $this->getQuoteMaskId()
        ];
    }

    /**
     * @return bool
     */
    public function isBeauty()
    {
        $isBeauty = false;
        $cartItems = $this->checkoutCartFactory->create()->getItems();
        /** @var  \Magento\Quote\Model\Quote\Item $item */
        foreach ($cartItems as $item) {
            $product = $item->getProduct();
            if (!$this->attributeSetHelper->isBeauty($product)) {
                $isBeauty = false;
                break;
            } else {
                $isBeauty = true;
                continue;
            }
        }
        return $isBeauty;
    }
}
