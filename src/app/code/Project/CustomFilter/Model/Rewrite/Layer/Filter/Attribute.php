<?php

namespace Project\CustomFilter\Model\Rewrite\Layer\Filter;


class Attribute extends \Algolia\AlgoliaSearch\Model\Layer\Filter\Attribute
{


    /** @var \Magento\Framework\Escaper */
    private $escaper;




    /**
     * {@inheritdoc}
     */
    public function apply(\Magento\Framework\App\RequestInterface $request)
    {
        /**####START OVERRIDE => parent::apply hide filter from custom_filter div #### */
       /* $storeId = $this->configHelper->getStoreId();
        if (!$this->configHelper->isBackendRenderingEnabled($storeId)) {
            return parent::apply($request);
        }*/
        /**####END OVERRIDE####*/

        $attribute = $this->getAttributeModel();
        $attributeValue = $request->getParam($this->_requestVar);
        if (!is_null($attributeValue)) {
            $attributeValue = explode('~', $request->getParam($this->_requestVar));
        }

        if (empty($attributeValue)) {
            return $this;
        }

        if (!is_array($attributeValue)) {
            $attributeValue = [$attributeValue];
        }

        $this->currentFilterValue = array_values($attributeValue);

        /** @var \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection $productCollection */
        $productCollection = $this->getLayer()->getProductCollection();
        $productCollection->addFieldToFilter($attribute->getAttributeCode(), ['in' => $this->currentFilterValue]);
        $layerState = $this->getLayer()->getState();

        foreach ($this->currentFilterValue as $currentFilter) {
            $filter = $this->_createItem(
               $this->getOptionText($currentFilter),
                $this->currentFilterValue
            );
            $layerState->addFilter($filter);
        }

        return $this;
    }


}
