<?php

namespace Project\CustomFilter\Model\ResourceModel\Fulltext;

use Magento\Catalog\Model\Product\OptionFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\FilterBuilderFactory;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilderFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection\ProductLimitationFactory;
use Magento\Framework\App\ObjectManager;

/**
 * Class Collection
 * @package Project\CustomFilter\Model\ResourceModel\Fulltext
 * @author  Synolia <contact@synolia.com>
 */
class Collection extends \Synolia\CustomFilter\Model\ResourceModel\Fulltext\Collection
{
    public function reset()
    {
        $this->setFilterBuilder(
            ObjectManager::getInstance()->create(FilterBuilder::class)
        );
        $this->setSearchCriteriaBuilder(
            ObjectManager::getInstance()->create(SearchCriteriaBuilder::class)
        );
    }
}
