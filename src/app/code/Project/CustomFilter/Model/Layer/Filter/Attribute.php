<?php

namespace Project\CustomFilter\Model\Layer\Filter;

use Magento\Catalog\Model\Layer;
use Magento\Framework\Filter\StripTags;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Layer\Filter\ItemFactory;
use Magento\Catalog\Model\Layer\Filter\Item\DataBuilder;

use Synolia\CustomFilter\Helper\Config;
use Synolia\CustomFilter\Model\Layer\Filter\Attribute as CoreAttribute;
use Synolia\CustomFilter\Model\ResourceModel\Fulltext\Collection;

use Project\CustomFilter\Helper\Config as ProjectConfig;

/**
 * Class Attribute
 *
 * @package Project\CustomFilter\Model\Layer\Filter
 * @author  Synolia <contact@synolia.com>
 */
class Attribute extends CoreAttribute
{
    /**
     * @var \Magento\Framework\Filter\StripTags
     */
    private $tagFilter;

    /**
     * @var ProjectConfig
     */
    protected $helperProjectConfig;

    /**
     * @param ItemFactory           $filterItemFactory
     * @param StoreManagerInterface $storeManager
     * @param Layer                 $layer
     * @param DataBuilder           $itemDataBuilder
     * @param StripTags             $tagFilter
     * @param Config                $helperConfig
     * @param ProjectConfig         $helperProjectConfig
     * @param array                 $data
     */
    public function __construct(
        ItemFactory $filterItemFactory,
        StoreManagerInterface $storeManager,
        Layer $layer,
        DataBuilder $itemDataBuilder,
        StripTags $tagFilter,
        Config $helperConfig,
        ProjectConfig $helperProjectConfig,
        array $data = []
    ) {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $tagFilter,
            $helperConfig,
            $data
        );

        $this->tagFilter           = $tagFilter;
        $this->helperProjectConfig = $helperProjectConfig;
    }

    /**
     * Get data array for building attribute filter items
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    // @codingStandardsIgnoreLine
    protected function _getItemsData()
    {
        $mapping = $this->helperProjectConfig->getCategoryAttributesToDisplay(
            $this->getLayer()->getCurrentStore()->getId(),
            $this->getLayer()->getCurrentCategory()
        );

        if ($mapping !== false && $mapping !== null && !\in_array($this->getAttributeModel()->getAttributeCode(), $mapping)) {
            return $this->itemDataBuilder->build();
        }

        if (!$this->helperConfig->keepAllValues() && !$this->helperConfig->allowMultiSelect()) {
            return parent::_getItemsData();
        }

        /** @var Collection $productCollection */
        $productCollection = $this->getLayer()
            ->getProductCollection()
        ;

        /** @var Collection $collection */
        $collection = $this->getLayer()
            ->getCollectionProvider()
            ->getCollection($this->getLayer()->getCurrentCategory())
        ;

        $unfilteredCollection = clone $collection;
        //Rremoved by Magento in 2.3 Magento version
        //$unfilteredCollection->reset();
        $unfilteredCollection->addCategoryFilter($this->getLayer()->getCurrentCategory());

        foreach ($productCollection->getAddedFilters() as $field => $condition) {
            if ($this->getAttributeModel()->getAttributeCode() == $field) {
                continue;
            }
            $collection->addFieldToFilter($field, $condition);
        }

        $collection->addSearchFilter($this->getRequest()->getParam('q'));

        $attribute          = $this->getAttributeModel();
        $optionsFacetedData = $collection->getFacetedData($attribute->getAttributeCode());

        if ($attribute->getFrontendInput() == 'multiselect' && $this->helperConfig->allowMultiSelect()) {
            $originalFacetedData = $productCollection->getFacetedData($attribute->getAttributeCode());
            foreach ($originalFacetedData as $key => $optionData) {
                $optionsFacetedData[$key]['count'] -= $optionData['count'];
                if ($optionsFacetedData[$key]['count'] <= 0) {
                    unset($optionsFacetedData[$key]['count']);
                }
            }
        }

        $unfilteredOptionsFacetedData = $unfilteredCollection->getFacetedData($attribute->getAttributeCode());

        $options     = $attribute->getFrontend()
            ->getSelectOptions()
        ;
        $usedOptions = $this->getValueAsArray();

        foreach ($options as $option) {
            if (empty($option['value'])) {
                continue;
            }

            if (!$this->helperConfig->keepActiveFilter()
                && !$this->helperProjectConfig->isKeepActiveFilter($attribute)
                && array_key_exists($option['value'], array_flip($usedOptions))
            ) {
                continue;
            }

            // Check filter type
            if (empty($unfilteredOptionsFacetedData[$option['value']]['count'])) {
                continue;
            }

            $this->itemDataBuilder->addItemData(
                $this->tagFilter->filter($option['label']),
                $option['value'],
                isset($unfilteredOptionsFacetedData[$option['value']]['count']) ?
                    '+'.
                    $unfilteredOptionsFacetedData[$option['value']]['count'] : 0
            );
        }

        return $this->itemDataBuilder->build();
    }
}
