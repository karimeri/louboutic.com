<?php

namespace Project\CustomFilter\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Synolia\CustomFilter\Model\Layer\Filter\Category;

/**
 * Class Config
 *
 * @package Project\CustomFilter\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_KEEP_ACTIVE_FILTERS = 'synolia_customfilter/attribute_filter/keep_active_filter';
    const XML_PATH_CATEGORY_ATTRIBUTES_TO_DISPLAY = 'synolia_customfilter/category_filter/attributes_to_display';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Config constructor.
     *
     * @param Context             $context
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;

        parent::__construct($context);
    }

    /**
     * @param             $configName
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getKeepActiveFilters($scopeCode = null)
    {
        $config = $this->getConfig(self::XML_PATH_KEEP_ACTIVE_FILTERS, $scopeCode);

        if ($config) {
            return $this->serializer->unserialize($this->getConfig(self::XML_PATH_KEEP_ACTIVE_FILTERS, $scopeCode));
        }

        return $config;
    }

    /**
     * @param AbstractFilter $filter
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function isKeepActiveFilter($filter)
    {
        if ($filter instanceof Category) {
            return false;
        }

        $keepActiveFilters = $this->getKeepActiveFilters();
        $attributeCode     = $filter->getAttributeCode() ?: $filter->getAttributeModel()->getAttributeCode();

        foreach ($keepActiveFilters as $keepActiveFilter) {
            if ($attributeCode === $keepActiveFilter['attribute_code']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string|null $scopeCode
     * @param Category|null $category
     * @return bool|array
     */
    public function getCategoryAttributesToDisplay($scopeCode = null, $category = null)
    {
        $config = $this->getConfig(self::XML_PATH_CATEGORY_ATTRIBUTES_TO_DISPLAY, $scopeCode);

        if ($config) {
            $categoryAttributesToDisplay = $this->serializer->unserialize($this->getConfig(
                self::XML_PATH_CATEGORY_ATTRIBUTES_TO_DISPLAY,
                $scopeCode
            ));

            if ($category) {
                foreach ($categoryAttributesToDisplay as $mapping) {
                    if (\in_array($mapping['category'], $category->getParentIds())) {
                        return isset($mapping['attributes']) ? $mapping['attributes'] : [];
                    }
                }

                return false;
            }

            return $categoryAttributesToDisplay;
        }

        return $config;
    }

    /**
     * @param Category $category
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function isCategoryAttributesToDisplay($category)
    {
        if ($category instanceof Category) {
            return false;
        }

        $categoryAttributesToDisplay = $this->getCategoryAttributesToDisplay();

        foreach ($categoryAttributesToDisplay as $categoryMapping) {
            if ($categoryMapping['category'] === $category->getId()) {
                return true;
            }
        }

        return false;
    }
}
