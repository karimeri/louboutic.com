<?php

namespace Project\CustomFilter\Setup;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class UpgradeData
 *
 * @package Project\CustomFilter\Setup
 * @author  Nadia
 */
class UpgradeData implements UpgradeDataInterface
{
    const DISPLAY_PRODUCT_COUNT_PATH = "catalog/layered_navigation/display_product_count";
    const ATTRIBUTE_CODE_BRA = 'size_bracelets';
    const IS_SEARCHABLE = 'is_searchable';
    const IS_FILTERABLE = 'is_filterable';
    const ATTRIBUTE_CODE_COLOR_FILTER = 'generic-color';
    const ATTRIBUTE_CODE_HEEL_FILTER = 'heel_height_map';
    const POSITION = 'position';

    /**
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     *
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * UpgradeData constructor.
     * @param WriterInterface $configWriter
     * @param EavSetupFactory $eavSetupFactory
     * @param ConfigSetup $configSetup
     * @param EavSetup $eavSetup
     */
    public function __construct(
        WriterInterface $configWriter,
        EavSetupFactory $eavSetupFactory,
        ConfigSetup $configSetup,
        EavSetup $eavSetup
    )
    {
        $this->configWriter = $configWriter;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->configSetup = $configSetup;
        $this->eavSetup = $eavSetup;
    }


    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.1.2') < 0) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $tableName = $resource->getTableName('catalog_category_entity_int');

            //set category anchor
            $sql = "UPDATE " . $tableName . " SET value = 1 where attribute_id = 165;";
            $installer->run($sql);
        }

        if (version_compare($context->getVersion(), '1.1.3') < 0) {
            $this->configWriter->save(self::DISPLAY_PRODUCT_COUNT_PATH, 1);
        }

        if (version_compare($context->getVersion(), '1.1.4') < 0) {
            $this->updateSizeBraceletsAttributeConfig($installer);
            $this->updateCustomFilterConfig();
        }

        if (version_compare($context->getVersion(), '1.1.5') < 0) {
            $this->updateCustomProductAttributes();
        }

        $installer->endSetup();
    }

    /**
     *
     * @param ModuleDataSetupInterface $setup
     */
    protected function updateSizeBraceletsAttributeConfig(ModuleDataSetupInterface $setup)
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY,
            self::ATTRIBUTE_CODE_BRA,
            [
                self::IS_SEARCHABLE => 1,
                self::IS_FILTERABLE => 1

            ]
        );
    }

    protected function updateCustomFilterConfig(){
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('synolia_customfilter/attribute_filter/template/',
            '{"_1521042686105_105":{"attribute_code":"size","template":"layer\/filter\/size.phtml"},
        "_1521043110759_759":{"attribute_code":"generic_color","template":"layer\/filter\/generic-color.phtml"},
        "_1521043227815_815":{"attribute_code":"generic_material","template":"layer\/filter\/generic-material.phtml"},
        "_1521100550068_68":{"attribute_code":"heel_height_map","template":"layer\/filter\/heel-height.phtml"},
        "_1554825288534_534":{"attribute_code":"size_belts","template":"layer\/filter\/size-belts.phtml"},
        "_1597322441781_781":{"attribute_code":"size_bracelets","template":"layer\/filter\/size-bracelets.phtml"}}');
        // @codingStandardsIgnoreEnd
    }

    public function updateCustomProductAttributes()
    {
        $attributes = [
            [
                'type' => \Magento\Catalog\Model\Product::ENTITY,
                'code' => self::ATTRIBUTE_CODE_COLOR_FILTER,
                'data' => [
                    self::POSITION => 2,
                ]
            ],
            [
                'type' => \Magento\Catalog\Model\Product::ENTITY,
                'code' => self::ATTRIBUTE_CODE_HEEL_FILTER,
                'data' => [
                    self::POSITION => 3,
                ]
            ],
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                if ($this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                    $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
                }
            }
        }
    }
}
