<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Project\CustomFilter\Plugin\Magento\Catalog\Model\Layer;
use Project\CustomFilter\Helper\Config as ProjectConfig;

/**
 * Layer navigation filters
 */
class FilterListPlugin
{

    /**
     * @var ProjectConfig
     */
    protected $helperProjectConfig;

    public function __construct(ProjectConfig $helperProjectConfig)
    {
        $this->helperProjectConfig = $helperProjectConfig;
    }
    /**
     * Retrieve list of filters
     *
     * @param \Magento\Catalog\Model\Layer $layer
     * @return array|Filter\AbstractFilter[]
     */
    public function afterGetFilters(\Magento\Catalog\Model\Layer\FilterList $subject, $result,\Magento\Catalog\Model\Layer $layer)
    {
        $mapping = $this->helperProjectConfig->getCategoryAttributesToDisplay(
            $layer->getCurrentStore()->getId(),
            $layer->getCurrentCategory()
        );
        if(\is_array($mapping) && !empty($mapping) && (count($mapping)>0)) {
            foreach ($result as $key => $filter) {
                if ($filter->hasAttributeModel()) {
                    $attribute_code = $filter->getAttributeModel()->getAttributeCode();
                    if (!in_array($attribute_code,$mapping)) {
                      unset($result[$key]);
                    }
                }else {
                       unset($result[$key]);
                }
            }
        }else {
            $result = [];
        }
        return $result;
    }

}
