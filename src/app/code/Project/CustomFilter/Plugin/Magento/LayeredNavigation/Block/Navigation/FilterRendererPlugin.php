<?php

namespace Project\CustomFilter\Plugin\Magento\LayeredNavigation\Block\Navigation;

use Magento\Framework\App\Request\Http;
use Magento\Framework\View\LayoutInterface;
use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\LayeredNavigation\Block\Navigation\FilterRenderer;

use Synolia\CustomFilter\Helper\Config;
use Synolia\CustomFilter\Model\Layer\Filter\Price;
use Synolia\CustomFilter\Model\Layer\Filter\Category;
use Synolia\CustomFilter\Block\Navigation\Filter\PriceRenderer;
use Synolia\CustomFilter\Plugin\Magento\LayeredNavigation\Block\Navigation\FilterRendererPlugin as BasePlugin;

use Project\CustomFilter\Helper\Config as ProjectConfig;


/**
 * Class FilterRendererPlugin
 *
 * @package Project\CustomFilter\Plugin\Magento\LayeredNavigation\Block\Navigation
 * @author  Synolia <contact@synolia.com>
 */
class FilterRendererPlugin extends BasePlugin
{
    /**
     * @var ProjectConfig
     */
    protected $projectConfigHelper;

    /**
     * @param LayoutInterface $layout
     * @param Config          $configHelper
     * @param Http            $request
     * @param ProjectConfig   $projectConfigHelper
     */
    public function __construct(
        LayoutInterface $layout,
        Config $configHelper,
        Http $request,
        ProjectConfig $projectConfigHelper
    ) {
        parent::__construct(
            $layout,
            $configHelper,
            $request
        );

        $this->projectConfigHelper = $projectConfigHelper;
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @param FilterRenderer  $subject
     * @param \Closure        $proceed
     * @param FilterInterface $filter
     *
     * @return mixed
     * @throws \InvalidArgumentException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundRender(
        FilterRenderer $subject,
        \Closure $proceed,
        FilterInterface $filter
    ) {
        if (!$this->configHelper->isModuleEnable()) {
            return $proceed($filter);
        }

        $classDisplay = '';
        $template     = '';
        $block        = '';
        $state        = $filter->getLayer()->getState();

        switch ($filter) {
            case $filter instanceof Price
                && $this->configHelper->getConfig(Config::XML_PATH_PRICE_USE_RANGE_SLIDER)
                && $this->configHelper->canUsePriceRangeSlider():
                $block    = PriceRenderer::class;
                $template = 'layer/filter/price.phtml';
                break;
            case $filter->hasAttributeModel():
                $attributeCode = $filter->getAttributeModel()->getAttributeCode();
                $template      = $this->configHelper->getAttributeFilterTemplate($attributeCode);
                $block         = $this->configHelper->getAttributeFilterBlock($attributeCode);
                break;
            case $filter instanceof Category:
                $template = $this->configHelper->getCategoryFilterTemplate();
                $block    = $this->configHelper->getCategoryFilterBlock();
                break;
        }

        switch ($template) {
            case false:
                $template = Config::TEMPLATE_FILTER_ITEM_DEFAULT;
                break;
            case '':
                $template     = Config::TEMPLATE_FILTER_ITEM_DEFAULT;
                $classDisplay = Config::CLASS_NO_DISPLAY;
                break;
        }

        switch ($block) {
            case false:
                $block = Config::BLOCK_FILTER_ITEM_DEFAULT;
                break;
            case '':
                $block        = Config::BLOCK_FILTER_ITEM_DEFAULT;
                $classDisplay = Config::CLASS_NO_DISPLAY;
                break;
        }

        $block = $this->layout->createBlock($block);

        $this->assignRenderVars(
            $block,
            [
                'filterItems'        => $this->sortItemsData($filter->getItems()),
                'class'              => get_class($filter),
                'filter'             => $filter,
                'classDisplay'       => $classDisplay,
                'state'              => $state,
                'isKeepActiveFilter' => $this->projectConfigHelper->isKeepActiveFilter($filter),
            ]
        );

        return $block
            ->setFilter($filter)
            ->setTemplate(Config::NAME_MODULE.'::'.$template)
            ->toHtml()
            ;
    }

    private function sortItemsData(array $items)
    {
        usort($items, function ($item1, $item2) {
            if (!isset($item1['label']) or !isset($item2['label'])) {
                return 0;
            }

            return $item1['label'] > $item2['label'] ? 1 : -1;
        });

        return $items;
    }
}
