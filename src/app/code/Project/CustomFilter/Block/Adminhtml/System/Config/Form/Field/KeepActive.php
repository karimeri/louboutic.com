<?php

namespace Project\CustomFilter\Block\Adminhtml\System\Config\Form\Field;

use Magento\Framework\DataObject;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Synolia\CustomFilter\Block\Adminhtml\Form\Field\AttributeFilterable;

/**
 * Class KeepActive
 *
 * @package Project\CustomFilter\Block\Adminhtml\System\Config\Form\Field
 * @author  Synolia <contact@synolia.com>
 */
class KeepActive extends AbstractFieldArray
{
    protected $attributeCodeFilterable;

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
    */
    // @codingStandardsIgnoreLine
    protected function _construct()
    {
        parent::_construct();
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Returns renderer for yes or no
     */
    protected function getAttributeFilterableRenderer()
    {
        if (!$this->attributeCodeFilterable) {
            $this->attributeCodeFilterable = $this->_layout->createBlock(
                AttributeFilterable::class,
                '',
                [
                    'data' => [
                        'is_render_to_js_template' => true
                    ]
                ]
            );
        }
        return $this->attributeCodeFilterable;
    }

    /**
     * Prepare to render
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _prepareToRender()
    {
        $this->addColumn(
            'attribute_code',
            [
                'label'    => __('Attribute'),
                'renderer' => $this->getAttributeFilterableRenderer(),
            ]
        );
        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @param DataObject $row
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreLine
    protected function _prepareArrayRow(DataObject $row)
    {
        $attributeCode = $row->getAttributeCode();
        $options       = [];
        if ($attributeCode) {
            $optionKey = 'option_'.$this->getAttributeFilterableRenderer()->calcOptionHash($attributeCode);
            $options[$optionKey] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}
