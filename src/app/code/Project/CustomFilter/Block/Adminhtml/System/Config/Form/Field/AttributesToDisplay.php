<?php

namespace Project\CustomFilter\Block\Adminhtml\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\Factory;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Helper\Eav;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection as AttributeCollection;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as ProductAttributeCollectionFactory;

/**
 * Class AttributesToDisplay
 * @package Project\CustomerFilter\Block\Adminhtml\System\Config
 * @author Synolia <contact@synolia.com>
 */
class AttributesToDisplay extends AbstractFieldArray
{
    /**
     * @var \Magento\Rma\Helper\Eav
     */
    protected $rmaHelper;

    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * phpcs:disable
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Rma\Helper\Eav $rmaHelper
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $productAttributeCollectionFactory
     * @param array $data
     * phpcs:enable
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        Eav $rmaHelper,
        Factory $elementFactory,
        CollectionFactory $categoryCollectionFactory,
        ProductAttributeCollectionFactory $productAttributeCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->rmaHelper = $rmaHelper;
        $this->elementFactory = $elementFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->attributeCollectionFactory = $productAttributeCollectionFactory;
    }

    /**
     * Initialise form fields
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore
    protected function _construct()
    {
        $this->addColumn('category', ['label' => __('Category')]);
        $this->addColumn('attributes', ['label' => __('Attributes')]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * @param string $columnName
     * @return string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName === 'category' && isset($this->_columns[$columnName])) {
            $options = $this->getCategoryOptions();

            $element = $this->elementFactory->create('select');
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                $options
            );

            return str_replace("\n", '', $element->getElementHtml());
        }

        if ($columnName === 'attributes' && isset($this->_columns[$columnName])) {
            $options = $this->getAttributesOptions();

            $element = $this->elementFactory->create('multiselect');
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                $options
            );

            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategoryOptions()
    {
        $options = [];

        /** @var CategoryCollection $collection */
        $collection = $this->categoryCollectionFactory->create();

        $collection
            ->addAttributeToSelect(['id', 'name'])
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('level', 2);

        foreach ($collection as $category) {
            $options[$category->getId()] = $category->getName();
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getAttributesOptions()
    {
        $options = [];

        /** @var AttributeCollection $collection */
        $collection = $this->attributeCollectionFactory->create();
        $collection->addIsFilterableFilter();

        foreach ($collection as $attribute) {
            $options[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getFrontendLabel()
            ];
        }

        return $options;
    }
}
