<?php

namespace Project\WeltPixelGoogleTagManager\Setup;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class UpgradeData
 * @package Project\WeltPixel\GoogleTagManager\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    const GTM_CODE = "weltpixel_googletagmanager/general/gtm_code";

    const GTM_NONJS_CODE = "weltpixel_googletagmanager/general/gtm_nonjs_code";

    const GTM_TRACKING_ID  = "weltpixel_googletagmanager/api/ua_tracking_id";

    const GTM_ENABLE = "weltpixel_googletagmanager/general/enable";

    const GTM_TRACK_REVIEWS_COUNT = "weltpixel_googletagmanager/general/track_reviewscount";

    const GTM_TRACK_REVIEWS_SCORE = "weltpixel_googletagmanager/general/track_reviewsscore";

    const GTM_TRACK_SALE_PRODUCT = "weltpixel_googletagmanager/general/track_saleproduct";

    /** @var WriterInterface */
    protected $configWriter;

    /**
     * UpgradeData constructor.
     * @param WriterInterface $configWriter
     */
    public function __construct(
        WriterInterface $configWriter
    )
    {
        $this->configWriter = $configWriter;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->configWriter->save(self::GTM_CODE, '<!-- Google Tag Manager -->
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":
                new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src=
                "https://www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,"script","dataLayer","GTM-KMK8TN");
                </script>
                <!-- End Google Tag Manager -->');

            $this->configWriter->save(self::GTM_NONJS_CODE, '<!-- Google Tag Manager (noscript) -->
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMK8TN"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <!-- End Google Tag Manager (noscript) -->');

            $this->configWriter->save(self::GTM_TRACKING_ID, 'UA-7246511-10');
            $this->configWriter->save(self::GTM_ENABLE, '1');
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->configWriter->save('weltpixel_googletagmanager/general/enable_variant', '1');
        }

        // Disable dimensions 5, 6, 7
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $this->configWriter->save(self::GTM_TRACK_REVIEWS_COUNT, 0);
            $this->configWriter->save(self::GTM_TRACK_REVIEWS_SCORE, 0);
            $this->configWriter->save(self::GTM_TRACK_SALE_PRODUCT, 0);
        }

        $installer->endSetup();
    }
}