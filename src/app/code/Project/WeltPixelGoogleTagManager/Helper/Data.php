<?php

namespace Project\WeltPixelGoogleTagManager\Helper;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\BlockFactory;
use Magento\Framework\Registry;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Framework\Escaper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Checkout\Model\Session\SuccessValidator;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use WeltPixel\GoogleTagManager\Model\Storage;
use Magento\Cookie\Helper\Cookie;
use Magento\Catalog\Helper\Product\Configuration;
use Magento\Catalog\Api\ProductCustomOptionRepositoryInterface;
use Magento\Framework\View\Page\Config;
use WeltPixel\GoogleTagManager\Model\Dimension;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory as WishlistCollectionFactory;
use \Magento\Framework\App\State;
use \Magento\Framework\View\Element\Template\Context as templateContext;
use \Magento\Tax\Api\TaxCalculationInterface;
use  \Project\Core\Helper\AttributeSet;


class Data extends \WeltPixel\GoogleTagManager\Helper\Data
{
    /**
     * Customer group repository
     *
     * @var GroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * @var WishlistCollectionFactory
     */
    protected $_wishlistCollectionFactory;

    /**
     * Application state
     *
     * @var State
     */
    protected $_appState;

    /**
     * @var templateContext
     */
    protected $templateContext;

    /**
     * @var TaxCalculationInterface
     */
    protected $taxCalculation;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var ObjectManagerInterface
     */
    private $_objectManager;

    public function __construct(
        Context $context,
        BlockFactory $blockFactory,
        Registry $registry,
        CollectionFactory $categoryCollectionFactory,
        Category $resourceCategory,
        Escaper $escaper,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        OrderRepositoryInterface $orderRepository,
        SuccessValidator $checkoutSuccessValidator,
        Storage $storage,
        Cookie $cookieHelper,
        Configuration $configurationHelper,
        ProductCustomOptionRepositoryInterface $productOptionRepository,
        Config $pageConfig,
        Dimension $dimensionModel,
        CacheInterface $cache,
        StateInterface $cacheState,
        GroupRepositoryInterface $groupRepository,
        WishlistCollectionFactory $wishlistCollectionFactory,
        templateContext $templateContext,
        TaxCalculationInterface $taxCalculation,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        AttributeSet $attributeSetHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        EnvironmentManager $environmentManager,
        ObjectManagerInterface $objectManager
    ) {
        $this->taxCalculation = $taxCalculation;
        $this->groupRepository = $groupRepository;
        $this->_wishlistCollectionFactory = $wishlistCollectionFactory;
        $this->_appState = $templateContext->getAppState();
        $this->_categoryFactory = $categoryFactory;
        $this->attributeSetHelper =   $attributeSetHelper;
        $this->_productFactory = $productFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->environmentManager = $environmentManager;
        $this->_objectManager = $objectManager;
        parent::__construct(
            $context,
            $blockFactory,
            $registry,
            $categoryCollectionFactory,
            $resourceCategory,
            $escaper,
            $storeManager,
            $checkoutSession,
            $orderRepository,
            $checkoutSuccessValidator,
            $storage,
            $cookieHelper,
            $configurationHelper,
            $productOptionRepository,
            $pageConfig,
            $dimensionModel,
            $cache,
            $cacheState
        );
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPageData()
    {
        //default values
        $pageData['siteEnvironment'] =  $this->getMagentoMode();
        $env = $this->environmentManager->getEnvironment();
        if ($env === Environment::EU) {
            $env = 'europe';
        }
        $pageData['zone'] = $env;
        $pageData['pageName'] = '';
        $pageData['pageCategory'] = 'other';
        $actionName = $this->_request->getFullActionName();
        if ($actionName === 'cms_index_index') {
            $pageData['pageCategory'] = 'homepage';
        }
        $pageData['pageType'] = 'classic';
        if ($actionName === 'checkout_index_index') {
            $pageData['pageType'] = 'virtual';
        }
        $pageData['pageLanguage'] = $this->storeManager->getWebsite()->getCode();
        $pageData['residencePlace'] = $this->storeManager->getWebsite()->getName();
        $subject = $this->storeManager->getStore()->getCurrentUrl(false);
        $parseUrl = parse_url($subject);
        $pageData['currentPagePath'] =  $parseUrl['path'];

        if ($this->pageConfig) {
            $pageTitle = $this->pageConfig->getTitle()->get();
            $pageData['pageName'] = $pageTitle;
        }

        return ($pageData);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $buyRequest
     * @param \Magento\Wishlist\Model\Item $wishlistItem
     * @return array
     */
    public function addToWishListPushData($product, $buyRequest, $wishlistItem)
    {
        $result = [];

        $result['event'] = 'gtm.ext.event';
        $result['eventLabel'] = html_entity_decode($product->getName());
        $result['eventTmp'] = 'enhancedEcommerce';
        $result['eventCategory'] = 'enhancedEcommerce';
        $result['eventAction'] = 'addToWishlist';

        $result['ecommerce'] = [];
        $result['ecommerce']['currencyCode'] = $this->getCurrencyCode();
        $result['ecommerce']['add'] = [];
        $result['ecommerce']['add']['products'] = [];

        $productData = [];
        $productData['name'] = html_entity_decode($product->getName());
        $productData['id'] = $this->getGtmProductId($product);
        //get prices
        $prices = $this->getPriceInclAndExclTax($product);
        $productData['priceHT'] = number_format($prices['exclTax'], 2, '.', '');
        $productData['price'] = number_format($prices['inclTax'], 2, '.', '');
        $variant = $this->checkVariantForProduct($product, $buyRequest, $wishlistItem);
        if ($variant) {
            $size = explode(':', $variant);
            $productData['dimension1'] =  trim($size[1]);
        }
        $productData['dimension2'] = html_entity_decode($product->getData('color_product_page'));

        $productData['quantity'] = 1;
        if ($this->isBrandEnabled()) {
            $productData['brand'] = $this->getGtmBrand($product);
        }

        $productData['category'] = $this->getGtmCategoryCode($product);

        /**  Set the custom dimensions */
        $customDimensions = $this->dimensionModel->getProductDimensions($product, $this);
        foreach ($customDimensions as $name => $value) :
            $productData[$name] = $value;
        endforeach;

        $result['ecommerce']['add']['products'][] = $productData;

        return $result;
    }


    /**
     * @param $category
     * @return string
     */
    public function getGtmCategoryPath($category)
    {
        return $category->getData('url');
    }

    /**
     * @return string
     */
    public function getMagentoMode()
    {
        $testMode = $this->scopeConfig->getValue('weltpixel_googletagmanager/general/test_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ( ! $testMode) {
            return 'production';
        }
        return 'staging';
    }

    /**
     * @param $product
     * @return float[]|int[]
     */
    public function getPriceInclAndExclTax($product)
    {
        if ($taxAttribute = $product->getCustomAttribute('tax_class_id')) {
            // First get base price (=price excluding tax)
            $productRateId = $taxAttribute->getValue();
            $rate = $this->taxCalculation->getCalculatedRate($productRateId);

            if ((int) $this->scopeConfig->getValue(
                    'tax/calculation/price_includes_tax',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 1
            ) {
                // Product price in catalog is including tax.
                $priceExcludingTax = $product->getPriceInfo()->getPrice('final_price')->getValue() / (1 + ($rate / 100));
            } else {
                // Product price in catalog is excluding tax.
                $priceExcludingTax = $product->getPriceInfo()->getPrice('final_price')->getValue();
            }

            $priceIncludingTax = $priceExcludingTax + ($priceExcludingTax * ($rate / 100));

            return [
                'inclTax' => $priceIncludingTax,
                'exclTax' => $priceExcludingTax
            ];
        }

        throw new LocalizedException(__('Tax Attribute not found'));
    }


    /**
     * Set default gtm options based on configuration
     */
    public function addDefaultInformation()
    {
        $actionName = $this->_request->getFullActionName();
        // UPTILAB COMMON VARIABLES
        $pageData = $this->getPageData();

        if ($this->isCustomDimensionPageNameEnabled()) {
            if ($this->pageConfig) {
                $pageTitle = $this->pageConfig->getTitle()->get();
                $pageData['pageName'] = $pageTitle;
            }
        }
        $show = true;
        if ($this->isCustomDimensionPageTypeEnabled()) {
            //$pageType = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_OTHER;
            switch ($actionName) {
                case 'cms_index_index':
//                    $pageType = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_HOME;
                    $show = false;
                    $pageData['pageCategory'] = 'homepage';
                    break;
                case 'cms_noroute_index':
                    $pageData['pageCategory'] = '404';
                    break;
                case 'catalogsearch_result_index':
                    $pageData['pageCategory'] = 'searchResults';
                    $show = false;
                    break;
                case 'checkout_cart_index':
                    $pageData['pageCategory'] = 'checkout';
                    $pageData['pageType'] = 'classic';
                    break;
                case 'checkout_index_index':
                    $pageData['pageCategory'] = 'checkout';
                    $pageData['pageType'] = 'virtual';
                    $show = false;
                    break;
                case 'firecheckout_index_index':
                    //$pageType = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_CART;
                    break;
            }
        }
        if ($show) {
            // UPTILAB COMMON VARIABLES
            foreach ($pageData as $key => $value) {
                $this->storage->setData($key, $value);
            }
        }
        // END UPTILAB VARIABLES

        if ($this->isAdWordsRemarketingEnabled()) {
            $remarketingData = [];
            switch ($actionName) {
                case 'cms_index_index':
                    $remarketingData['ecomm_pagetype'] = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_HOME;
                    break;
                case 'checkout_index_index':
                    break;
                case 'firecheckout_index_index':
                    $remarketingData['ecomm_pagetype'] = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_CART;
                    break;
                default:
                    $remarketingData['ecomm_pagetype'] = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_OTHER;
                    break;
            }
            $this->storage->setData('google_tag_params', $remarketingData);
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function addProductClick($product, $index = 0, $list = '')
    {
        $productClickBlock = $this->createBlock('Core', 'product_click.phtml');
        $html = '';

        if ($productClickBlock) {
            $productClickBlock->setProduct($product);
            $productClickBlock->setIndex($index);

            /**
             * If a list value is set use that one, if nothing add one
             */
            if (!$list) {
                $currentCategory = $this->getCurrentCategory();
                if (!empty($currentCategory)) {
                    $list = $this->getGtmCategory($currentCategory);
                } else {
                    /* Check if it is from a listing from search or advanced search*/
                    $requestPath = $this->_request->getModuleName() .
                        DIRECTORY_SEPARATOR . $this->_request->getControllerName() .
                        DIRECTORY_SEPARATOR . $this->_request->getActionName();
                    switch ($requestPath) {
                        case 'catalogsearch/advanced/result':
                            $list = __('Advanced Search Result');
                            break;
                        case 'catalogsearch/result/index':
                            $list = __('Search Result');
                            break;
                    }
                }
            }
            $productClickBlock->setList($list);
            $html = trim($productClickBlock->toHtml());
        }

        if (!empty($html)) {
            $html = substr(rtrim($html, ");"), 0, -1);
            $html .= "});";
            $html = 'onclick="' . $html . '"';
        }

        return $html;
    }

    public function getProductAvailableSizes($product)
    {
        if ($product->getTypeId() === 'simple') {
            return '';
        }
        $sizes = array();
        $productInstance = $product->getTypeInstance(true);
        $childs = $productInstance->getUsedProducts($product);
        foreach ( $childs as $child) {
            if ($child->getData('stock_status')  && $child->getData('is_salable')) {
                $attr = 'size';
                if ($this->attributeSetHelper->isBelts($child)) {
                    $attr = 'size_belts';
                };
                if ($this->attributeSetHelper->isBracelets($child)) {
                    $attr = 'size_bracelets';
                };
                $attrObj = $child->getResource()->getAttribute($attr);
                if ($attrObj->usesSource()) {
                    $sizes[] = $attrObj->getSource()->getOptionText($child->getData($attr));
                }
            }
        }
        return $sizes;
    }

    public function addCategoryPageInformation()
    {

        $currentCategory = $this->getCurrentCategory();

        if (!empty($currentCategory)) {
            $categoryBlock = $this->createBlock('Category', 'category.phtml');

            if ($categoryBlock) {
                $categoryBlock->setCurrentCategory($currentCategory);
                $categoryBlock->toHtml();
            }
        }
    }

    /**
     * Set product page detail infromation
     */
    public function addProductPageInformation()
    {
        $currentProduct = $this->getCurrentProduct();

        if (!empty($currentProduct)) {
            $productBlock = $this->createBlock('Product', 'product.phtml');

            if ($productBlock) {
                $productBlock->setCurrentProduct($currentProduct);
                $productBlock->toHtml();
            }
        }
    }

    public function addWishlistPageInformation()
    {
        $requestPath = $this->_request->getModuleName() .
            DIRECTORY_SEPARATOR . $this->_request->getControllerName() .
            DIRECTORY_SEPARATOR . $this->_request->getActionName();

        if ($requestPath == 'wishlist/index/index') {
            $wishlistBlock = $this->blockFactory->createBlock('\Project\WeltPixelGoogleTagManager\Block\Wishlist');
            $wishlistBlock->setTemplate('WeltPixel_GoogleTagManager::wishlist.phtml');
            if ($wishlistBlock) {
                $wishlistBlock->toHtml();
            }
        }
    }

    /**
     * Set search result page product impressions
     */
    public function addSearchResultPageInformation()
    {
        $moduleName = $this->_request->getModuleName();
        $controllerName = $this->_request->getControllerName();
        $listPrefix = '';
        if ($controllerName == 'advanced') {
            $listPrefix = __('Advanced');
        }

        if ($moduleName == 'catalogsearch') {
            if ($this->isCustomDimensionPageTypeEnabled()) {
                $pageType = 'classic';
                $this->storage->setData('pageType', $pageType);
            }

            $searchBlock = $this->createBlock('Search', 'search.phtml');

            if ($searchBlock) {
                $searchBlock->setListPrefix($listPrefix);
                return $searchBlock->toHtml();
            }
        }
    }

    /**
     * Get all categories id, name for the current store view
     */
    private function _populateStoreCategories()
    {
        if (!$this->isEnabled() || !empty($this->storeCategories)) {
            return;
        }

        $rootCategoryId = $this->storeManager->getStore()->getRootCategoryId();
        $storeId = $this->storeManager->getStore()->getStoreId();

        $isWpGtmCacheEnabled = $this->cacheState->isEnabled(\WeltPixel\GoogleTagManager\Model\Cache\Type::TYPE_IDENTIFIER);
        $cacheKey = self::CACHE_ID_CATEGORIES . '-' . $rootCategoryId . '-' . $storeId;
        if ($isWpGtmCacheEnabled) {
            $this->_eventManager->dispatch('weltpixel_googletagmanager_cachekey_after', ['cache_key' => $cacheKey]);

            $cachedCategoriesData = $this->cache->load($cacheKey);
            if ($cachedCategoriesData) {
                $this->storeCategories = json_decode($cachedCategoriesData, true);
                return;
            }
        }

        $categories = $this->categoryCollectionFactory->create()
            ->setStoreId($storeId)
            ->addAttributeToFilter('path', ['like' => "1/{$rootCategoryId}/%"])
            ->addAttributeToSelect('name');

        foreach ($categories as $categ) {
            $this->storeCategories[$categ->getData('entity_id')] = [
                'name' => $categ->getData('name'),
                'path' => $categ->getData('path')
            ];
        }

        if ($isWpGtmCacheEnabled) {
            $cachedCategories = json_encode($this->storeCategories);
            $this->cache->save($cachedCategories, $cacheKey, [\WeltPixel\GoogleTagManager\Model\Cache\Type::CACHE_TAG]);
        }
    }

    /**
     * @param $product
     * @return string
     */
    public function getGtmCategoryCode($product)
    {
        $productType = '';

        if ($this->attributeSetHelper->isBeauty($product)) {
            $productType = 'beauty';
            if ($this->attributeSetHelper->isEyes($product)) $productType .= ' eyes';
            elseif ($this->attributeSetHelper->isLips($product)) $productType .= ' lips';
            elseif ($this->attributeSetHelper->isNails($product)) $productType .= ' nails';
            elseif ($this->attributeSetHelper->isPerfumes($product)) $productType .= ' perfumes';
        } else {
            $gender = ($product->getGender())? $product->getResource()->getAttribute('gender')->getSource()->getOptionText($product->getGender()) . ' ' : '';
            if ($this->attributeSetHelper->isBelts($product)) $productType .= $gender . 'belts';
            elseif ($this->attributeSetHelper->isBracelets($product)) $productType .= $gender . 'bracelets';
            elseif ($this->attributeSetHelper->isShoes($product)) $productType .= $gender . 'shoes';
            elseif ($this->attributeSetHelper->isBag($product)) $productType .= $gender . 'bag';
            elseif ($this->attributeSetHelper->isAccessories($product)) $productType .= $gender . 'accessories';
        }

        return strtolower($productType);
    }

    /**
     * @param int $qty
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return array
     */
    public function removeFromCartPushData($qty, $product, $quoteItem)
    {
        $result = [];
        //get sizes
        $sizes = $this->getProductAvailableSizes($product);
        //get prices
        $prices = $this->getPriceInclAndExclTax($product);

        $result['event'] = 'gtm.ext.event';
        $result['eventTmp'] = 'enhancedEcommerce';
        $result['eventCategory'] = 'enhancedEcommerce';
        $result['eventAction'] = 'removeFromCart';
        $result['eventLabel'] = html_entity_decode($product->getName());

        $result['ecommerce'] = [];
        $result['ecommerce']['currencyCode'] = $this->getCurrencyCode();
        $result['ecommerce']['remove'] = [];
        $result['ecommerce']['remove']['products'] = [];

        $productData = [];
        $productData['id'] = $this->getGtmProductId($product);
        $productData['name'] = html_entity_decode($product->getName());

        $productData['price'] = number_format($prices['inclTax'], 2, '.', '');
        $productData['priceHT'] = number_format($prices['exclTax'], 2, '.', '');

        if ($this->isBrandEnabled()) {
            $productData['brand'] = $this->getGtmBrand($product);
        }

        $productData['category'] = $this->getGtmCategoryCode($product);
        $productData['quantity'] = $qty;

        $productData['dimension1'] = (!empty($sizes)) ? implode(',', $sizes) : '';
        $productData['dimension2'] = html_entity_decode($product->getData('color_product_page'));

        if ($this->isVariantEnabled()) {
            $productFromQuote = $quoteItem->getProduct();
            $variant = $this->checkVariantForProduct($productFromQuote);

            if ($variant) {
                $variants = explode('|', $variant);
                foreach ($variants as $option) {
                    $options = explode(':', $option);
                    $productData['dimension1'] = $options[1];
                }
            }
        }

        $result['ecommerce']['remove']['products'][] = $productData;

        return $result;
    }

    /**
     * @param int $qty
     * @param \Magento\Catalog\Model\Product $product
     * @param array $buyRequest
     * @param boolean $checkForCustomOptions
     * @return array
     */
    public function addToCartPushData($qty, $product, $buyRequest = [], $checkForCustomOptions = false)
    {

        //get prices
        $prices = $this->getPriceInclAndExclTax($product);
        $result = [];
        $result['event'] = 'gtm.ext.event';
        $result['eventTmp'] = 'enhancedEcommerce';
        $result['eventCategory'] = 'enhancedEcommerce';
        $result['eventAction'] = 'addToCart';
        $result['eventLabel'] = html_entity_decode($product->getName());
        $result['ecommerce'] = [];
        $result['ecommerce']['currencyCode'] = $this->getCurrencyCode();
        $result['ecommerce']['add'] = [];
        $result['ecommerce']['add']['products'] = [];
        $productData = [];
        $productData['id'] = $this->getGtmProductId($product);
        $productData['name'] = html_entity_decode($product->getName());
        $productData['price'] = number_format($prices['inclTax'], 2, '.', '');
        $productData['priceHT'] = number_format($prices['exclTax'], 2, '.', '');
        if ($this->isBrandEnabled()) {
            $productData['brand'] = $this->getGtmBrand($product);
        }
        $categoryName = $this->getGtmCategoryCode($product);
        $productData['category'] = $categoryName;
        //$productData['list'] = $categoryName;
        $productData['quantity'] = $qty;
        $productData['dimension1'] = '';
        $productData['dimension2'] = html_entity_decode($product->getData('color_product_page'));
        /**  Set the custom dimensions */
        $customDimensions = $this->dimensionModel->getProductDimensions($product, $this);
        foreach ($customDimensions as $name => $value) :
            $productData[$name] = $value;
        endforeach;

        if ($this->isVariantEnabled()) {
            $variant = $this->checkVariantForProduct($product, $buyRequest, null, $checkForCustomOptions);
            if ($variant) {
                $variants = explode('|', $variant);
                foreach ($variants as $option) {
                    $options = explode(':', $option);
                    $productData['dimension1'] = trim($options[1]);
                }
            }
        }


        $result['ecommerce']['add']['products'][] = $productData;
        return $result;
    }

    public function addCartPageInformation()
    {
        $requestPath = $this->_request->getModuleName() .
            DIRECTORY_SEPARATOR . $this->_request->getControllerName() .
            DIRECTORY_SEPARATOR . $this->_request->getActionName();

        if ($requestPath == 'checkout/cart/index') {
            $cartBlock = $this->createBlock('Cart', 'cart.phtml');

            if ($cartBlock) {
                $quote = $this->checkoutSession->getQuote();
                $cartBlock->setQuote($quote);
                $cartBlock->toHtml();
            }

            if ($this->isCustomDimensionPageTypeEnabled()) {
                $pageType = 'classic';
                $this->storage->setData('pageType', $pageType);
            }
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return string
     */
    public function getQuoteItemOptionValue($item)
    {
        $options = $item->getProduct()->getTypeInstance()->getOrderOptions($item->getProduct());
        if ($options && array_key_exists('attributes_info', $options)) {
            return $options['attributes_info'][0]['value'];
        }
        return '';
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param string $attributeCode
     * @return string
     */
    public function getQuoteItemAttributeValue($item, $attributeCode)
    {
        return html_entity_decode($item->getProduct()->getData($attributeCode));
    }

    public function addCheckoutInformation()
    {
        $requestPath = $this->_request->getModuleName() .
            DIRECTORY_SEPARATOR . $this->_request->getControllerName() .
            DIRECTORY_SEPARATOR . $this->_request->getActionName();

        if ($requestPath == 'checkout/index/index' || $requestPath == 'firecheckout/index/index') {
            $checkoutBlock = $this->createBlock('Checkout', 'checkout.phtml');

            if ($checkoutBlock) {
                $quote = $this->checkoutSession->getQuote();
                $checkoutBlock->setQuote($quote);
                $checkoutBlock->toHtml();
            }

            if ($this->isCustomDimensionPageTypeEnabled()) {
                $pageType = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_CHECKOUT;
                $this->storage->setData('pageType', 'virtual');
                $this->storage->setData('pageCategory', $pageType);

            }
        }
    }

    public function addCheckoutVirtualProductInformation()
    {
        $requestPath = $this->_request->getModuleName() .
            DIRECTORY_SEPARATOR . $this->_request->getControllerName() .
            DIRECTORY_SEPARATOR . $this->_request->getActionName();

        if ($requestPath == 'checkout/index/index' || $requestPath == 'firecheckout/index/index') {
            $checkoutBlock = $this->createBlock('Checkout', 'virtual_page_checkout.phtml');

            if ($checkoutBlock) {
                $quote = $this->checkoutSession->getQuote();
                $checkoutBlock->setQuote($quote);
                $checkoutBlock->toHtml();
            }

            if ($this->isCustomDimensionPageTypeEnabled()) {
                $pageType = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_CHECKOUT;
                $this->storage->setData('pageType', 'virtual');
                $this->storage->setData('pageCategory', $pageType);

            }
        }
    }

    /**
     * Set purchase details
     */
    public function addOrderInformation()
    {
        $lastOrderId = $this->checkoutSession->getLastOrderId();
        $requestPath = $this->_request->getModuleName() .
            DIRECTORY_SEPARATOR . $this->_request->getControllerName() .
            DIRECTORY_SEPARATOR . $this->_request->getActionName();

        $successPagePaths = [
            'checkout/onepage/success'
        ];

        $customSuccessPagePaths = trim($this->getCustomSuccessPagePaths());

        if (strlen($customSuccessPagePaths)) {
            $successPagePaths = array_merge($successPagePaths, explode(",", $customSuccessPagePaths));
        }

        if (!in_array($requestPath, $successPagePaths) || !$lastOrderId) {
            return;
        }

        $orderBlock = $this->createBlock('Order', 'order.phtml');
        if ($orderBlock) {
            $order = $this->orderRepository->get($lastOrderId);
            $orderBlock->setOrder($order);
            $orderBlock->toHtml();
        }

        if ($this->isCustomDimensionPageTypeEnabled()) {
            $pageType = \WeltPixel\GoogleTagManager\Model\Api\Remarketing::ECOMM_PAGETYPE_PURCHASE;
            $this->storage->setData('pageCategory', $pageType);
            $this->storage->setData('pageType', 'virtual');
        }
    }

    /**
     * @param int $step
     * @param array|string $checkoutOption
     * @return array
     */
    public function addCheckoutStepPushData($step, $checkoutOption)
    {
        $checkoutStepResult = [];

        $optionsName = [
            1 => 'shippingOption',
            2 => 'paymentOption'
        ];

        $checkoutOptions = [];
        if (is_array($checkoutOption)) {
            foreach ($checkoutOption as $stepOption => $option) {
                $checkoutOptions[$optionsName[$stepOption]] = $option;
            }
        } else {
            $checkoutOptions[$optionsName[$step]] = $checkoutOption;
        }

        $checkoutStepResult['event'] = 'checkout';
        $checkoutStepResult['eventTmp'] = 'virtualPageView';
        $checkoutStepResult['ecommerce'] = [];
        $checkoutStepResult['ecommerce']['currencyCode'] = $this->getCurrencyCode();
        $optionData = [];
        $optionData['step'] = $step;
        foreach ($checkoutOptions as $checkoutOption => $value) {
            $optionData[$checkoutOption] = $value;
        }
        $checkoutStepResult['ecommerce']['checkout']['actionField'] =  [
            $optionData
        ];

        $products = [];
        $checkoutBlock = $this->createBlock('Checkout', 'checkout.phtml');

        if ($checkoutBlock) {
            $quote = $this->checkoutSession->getQuote();
            $checkoutBlock->setQuote($quote);
            $products = $checkoutBlock->getProducts();
        }

        $checkoutStepResult['ecommerce']['checkout']['products'] = $products;

        $checkoutOptionResult['event'] = 'checkoutOption';
        $checkoutOptionResult['ecommerce'] = [];
        $checkoutOptionResult['ecommerce']['currencyCode'] = $this->getCurrencyCode();
        $checkoutOptionResult['ecommerce']['checkout_option'] = [];
        $optionData = [];
        $optionData['step'] = $step;
        foreach ($checkoutOptions as $checkoutOption => $value) {
            $optionData[$checkoutOption] = $value;
        }
        $checkoutOptionResult['ecommerce']['checkout_option']['actionField'] = $optionData;

        $result = [];
        $result[] = $checkoutStepResult;
//        $result[] = $checkoutOptionResult;

        return $result;
    }

    /**
     * @param string $step
     * @param array|string $checkoutOption
     * @param boolean $success
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    function addCheckoutEventData($step, $checkoutOption, $success = false)
    {
        $checkoutStepResult = [];

        $optionsName = [
            1 => 'shippingOption',
            2 => 'paymentOption'
        ];

        $checkoutOptions = [];
        if (is_array($checkoutOption)) {
            foreach ($checkoutOption as $stepOption => $option) {
                $checkoutOptions[$optionsName[$stepOption]] = $option;
            }
        } else {
            $checkoutOptions[$optionsName[$step]] = $checkoutOption;
        }

        $checkoutStepResult['event'] = 'gtm.ext.event';
        $checkoutStepResult['eventTmp'] = 'virtualPageView';
        $checkoutStepResult['ecommerce'] = [];
        $checkoutStepResult['ecommerce']['currencyCode'] = $this->getCurrencyCode();
        $optionData = [];
        $optionData['step'] = $step;
        foreach ($checkoutOptions as $checkoutOption => $value) {
            $optionData[$checkoutOption] = $value;
        }
        $checkoutStepResult['ecommerce']['checkout']['actionField'] =  [
            $optionData
        ];

        $products = [];
        if (!$success) {
            $checkoutBlock = $this->createBlock('Checkout', '.phtml');
            $quote = $this->checkoutSession->getQuote();
            $checkoutBlock->setQuote($quote);
            $products = $checkoutBlock->getProducts();
        } else {
            $successBlock = $this->blockFactory->createBlock('\Project\WeltPixelGoogleTagManager\Block\Success');
            $products = $successBlock->getProducts();
        }

        $checkoutStepResult['ecommerce']['checkout']['products'] = $products;

        return $checkoutStepResult;
    }

    /**
     * addToCustomerRegisterPushData
     * @return array
     */
    function addToCustomerRegisterPushData()
    {
        $result = [];

        $result['event'] = 'gtm.ext.event';
        $result['eventTmp'] = 'accountCreation';
        $result['eventCategory'] = 'accountCreation';
        $result['eventAction'] = 'yes';
        $result['eventLabel'] = 'signup';

        return $result;
    }

    /**
     * createRmaPushData
     * @return array
     */
    public function createRmaPushData()
    {
        $result = [];

        $result['event'] = 'gtm.ext.event';
        $result['eventTmp'] = 'ReturnCreation';
        $result['eventCategory'] = 'ReturnCreation';
        $result['eventAction'] = 'yes';
        $result['eventLabel'] = 'Return';
        return $result;
    }

    /**
     * addProductSizeGuideClick
     * @param $_product
     * @return string
     */
    public function addProductSizeGuideClick($_product)
    {
        $productClickBlock = $this->createBlock('Core', 'product_size_guide_click.phtml');
        $html = '';

        if ($productClickBlock) {

            $productClickBlock->setProduct($_product);
            $html = trim($productClickBlock->toHtml());
        }

        if (!empty($html)) {
            $html = substr(rtrim($html, ");"), 0, -1);
            $html .= "});";
            $html = 'onclick="' . $html . '"';
        }

        return $html;
    }

    public function addProductShareClick($_product, $socialNetwork)
    {
        $productClickBlock = $this->createBlock('Core', 'product_share_click.phtml');
        $html = '';

        if ($productClickBlock) {

            $productClickBlock->setProduct($_product);
            $productClickBlock->setSocialNetwork($socialNetwork);
            $html = trim($productClickBlock->toHtml());
        }

        if (!empty($html)) {
            $html = substr(rtrim($html, ");"), 0, -1);
            $html .= "});";
            $html = 'onclick="' . $html . '"';
        }

        return $html;
    }

    public function addStoreLocatorCityClick($city, $storeUrl)
    {
        $storeClickBlock = $this->createBlock('Core', 'store_locator_city_click.phtml');
        $html = '';

        if ($storeClickBlock) {
            $storeClickBlock->setCity($city);
            $storeClickBlock->setSoreUrl($storeUrl);
            $html = trim($storeClickBlock->toHtml());
        }

        if (!empty($html)) {
            $html = substr(rtrim($html, ");"), 0, -1);
            $html .= "});";
            $html = 'onclick="' . $html . '"';
        }

        return $html;
    }

    public function addToContactFormPostPushData($optionType)
    {
        $result = [];

        $result['event'] = 'gtm.ext.event';
        $result['eventTmp'] = 'formSubmission';
        $result['eventCategory'] = 'formSubmission';
        $result['eventAction'] = 'contactForm';
        $result['eventLabel'] = $optionType;
        $result['ecommerce'] = [];

        return $result;
    }


    /**
     * @return string
     */
    public function getDataLayerScript()
    {
        $script = '';

        if (!($block = $this->createBlock('Core', 'datalayer.phtml'))) {
            return $script;
        }

        $block->setNameInLayout('wp.gtm.datalayer.scripts');

        $this->addDefaultInformation();
        if (!$this->getCategorySliderPageInformation()) $this->addCategoryPageInformation();
//        $this->addSearchResultPageInformation();
        $this->addProductPageInformation();
        $this->addWishlistPageInformation();
        $this->addCartPageInformation();
//        $this->addCheckoutInformation();
        $this->addOrderInformation();

        $html = $block->toHtml();

        return $html;
    }


    public function addCategorySliderImpressions($productToTrack)
    {
        $productsData = array();
        $data = array();

        foreach ( $productToTrack as $product) {
            $productData = [];
            $productData['name'] = html_entity_decode($product->getName());
            $productData['id'] = $this->getGtmProductId($product);
            //get prices
            $prices = $this->getPriceInclAndExclTax($product);
            $productData['priceHT'] = number_format($prices['exclTax'], 2, '.', '');
            $productData['price'] = number_format($prices['inclTax'], 2, '.', '');
            $productData['position'] = '';
            $productData['dimension2'] = html_entity_decode($product->getData('color_product_page'));

            if ($this->isBrandEnabled()) {
                $productData['brand'] = $this->getGtmBrand($product);
            }

            $productData['category'] = $this->getGtmCategoryCode($product);

            /**  Set the custom dimensions */
            $customDimensions = $this->dimensionModel->getProductDimensions($product, $this);
            foreach ($customDimensions as $name => $value) :
                $productData[$name] = $value;
            endforeach;


            $productsData[] = $productData;
        }

        return($productsData);
    }

    public function addCategorySliderInformation($productToTrack)
    {
        $products = array();
        $allSlidersProducts = array();
        $data = $this->addCategorySliderImpressions($productToTrack);
        $customerSession = $this->getCustomerSession();
        $sessionData = $customerSession->getAddToCategorySliderData();

        if (!($sessionData)) $sessionData = array();

        $products = array_merge($sessionData, $data);

        if ($products) {
            $i = 1;
            foreach ($products as $product) {
                $product['position'] = $i++;
                $allSlidersProducts[] = $product;
            }
        }
        $customerSession->setAddToCategorySliderData($allSlidersProducts);
    }


    /**
     * @return array
     */
    public function getCategorySliderPageInformation()
    {
        $customerSession = $this->getCustomerSession();
        $data = $customerSession->getAddToCategorySliderData();

        $customerSession->setAddToCategorySliderData(null);
        if (!$data) return array();
        return ($data);
    }

    /**
     * @param $label
     * @return $this
     */
    public function unsetDataLayerOption($label) {
        $this->storage->unsetData($label);
        return $this;
    }

    /**
     * @return \Magento\Customer\Model\Session
     */
    private function getCustomerSession()
    {
        return $this->_objectManager->create('Magento\Customer\Model\Session');
    }
}
