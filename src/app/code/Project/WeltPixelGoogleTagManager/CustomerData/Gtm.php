<?php
namespace Project\WeltPixelGoogleTagManager\CustomerData;

/**
 * Gtm section
 */
class Gtm extends \WeltPixel\GoogleTagManager\CustomerData\Gtm
{
    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $data = [];

        /** AddToCart data verifications */
        if ($this->_checkoutSession->getAddToCartData()) {
            $data[] = $this->_checkoutSession->getAddToCartData();
        }

        $this->_checkoutSession->setAddToCartData(null);

        /** RemoveFromCart data verifications */
        if ($this->_checkoutSession->getRemoveFromCartData()) {
            $data[] = $this->_checkoutSession->getRemoveFromCartData();
        }

        $this->_checkoutSession->setRemoveFromCartData(null);

        /** Checkout Steps data verifications */
        if ($this->_checkoutSession->getCheckoutOptionsData()) {
            $checkoutOptions = $this->_checkoutSession->getCheckoutOptionsData();
            foreach ($checkoutOptions as $options) {
                $data[] = $options;
            }
        }
        $this->_checkoutSession->setCheckoutOptionsData(null);

        //checkout event
        if ($this->_checkoutSession->getCheckoutEventData()) {
            $data[] = $this->_checkoutSession->getCheckoutEventData();
        }
        $this->_checkoutSession->setCheckoutEventData(null);

        /** Add To Wishlist Data */
        if ($this->customerSession->getAddToWishListData()) {
            $data[] = $this->customerSession->getAddToWishListData();
        }
        $this->customerSession->setAddToWishListData(null);

        /** Add To Compare Data */
        if ($this->customerSession->getAddToCompareData()) {
            $data[] = $this->customerSession->getAddToCompareData();
        }
        $this->customerSession->setAddToCompareData(null);


        //Newsletter Subscribe
        /* if ($this->customerSession->getAddToNewsletterSubscribeData()) {
             $data[] = $this->customerSession->getAddToNewsletterSubscribeData();
         }
         $this->customerSession->setAddToNewsletterSubscribeData(null);*/

        //acountCreation
        if ($this->customerSession->getAddToCustomerRegisterData()) {
            $data[] = $this->customerSession->getAddToCustomerRegisterData();
        }
        $this->customerSession->setAddToCustomerRegisterData(null);

        //setAddToContactFormPostData
        if ($this->customerSession->getAddToContactFormPostData()) {
            $data[] = $this->customerSession->getAddToContactFormPostData();
        }
        $this->customerSession->setAddToContactFormPostData(null);

        //setAddCreateRma
       if ($this->customerSession->getCreateRmaPushData()) {
            $data[] = $this->customerSession->getCreateRmaPushData();
        }
        $this->customerSession->setCreateRmaPushData(null);

        return [
            'datalayer' => $this->jsonHelper->jsonEncode($data)
        ];
    }

}
