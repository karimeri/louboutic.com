<?php
namespace Project\WeltPixelGoogleTagManager\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerRegisterObserver implements ObserverInterface
{
    /**
     * @var \WeltPixel\GoogleTagManager\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @param \WeltPixel\GoogleTagManager\Helper\Data $helper
     */
    public function __construct(
        \WeltPixel\GoogleTagManager\Helper\Data $helper,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->helper = $helper;
        $this->customerSession = $customerSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->isEnabled()) {
            return $this;
        }

        //$account_controller = $observer->getData('account_controller');
        //$customer = $observer->getData('customer');

        $this->customerSession->setAddToCustomerRegisterData($this->helper->addToCustomerRegisterPushData());
    }

}