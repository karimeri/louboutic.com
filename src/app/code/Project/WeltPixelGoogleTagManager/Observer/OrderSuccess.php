<?php

namespace Project\WeltPixelGoogleTagManager\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

/**
 * Class OrderSuccess
 * @package Project\WeltPixelGoogleTagManager\Observer
 *
 * @deprecated
 */
class OrderSuccess implements ObserverInterface
{
    /**
     * @var \Project\WeltPixelGoogleTagManager\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * OrderSuccess constructor.
     * @param \Project\WeltPixelGoogleTagManager\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Project\WeltPixelGoogleTagManager\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->helper = $helper;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');

        $additionalInformation = $order->getPayment()->getAdditionalInformation();
        $shippingDescription = $order->getShippingDescription();

        if ($shippingDescription && $additionalInformation && isset($additionalInformation['method_title'])) {
            $paymentMethodTitle = $additionalInformation['method_title'];
            $data = [
                1 => $shippingDescription,
                2 => $paymentMethodTitle
            ];
            $this->_checkoutSession->setCheckoutEventData($this->helper->addCheckoutEventData('2', $data, true));
        }
    }
}
