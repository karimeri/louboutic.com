<?php

namespace Project\WeltPixelGoogleTagManager\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CustomerRmaObserver
 * @package Project\WeltPixelGoogleTagManager\Observer
 */
class CustomerRmaObserver implements ObserverInterface
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Project\WeltPixelGoogleTagManager\Helper\Data
     */
    protected $helper;

    /**
     * CustomerRmaObserver constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Project\WeltPixelGoogleTagManager\Helper\Data $helper
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Project\WeltPixelGoogleTagManager\Helper\Data $helper
    ) {
        $this->customerSession = $customerSession;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     */
    public function execute(Observer $observer)
    {
        if (!$this->helper->isEnabled()) {
            return $this;
        }

        $this->customerSession->setCreateRmaPushData($this->helper->createRmaPushData());
    }
}
