define([
    'jquery'
], function($) {
    'use strict';

    return function (target) {
        $.widget('synolia.infiniteScrolling', target, {
            _scroll: function () {
                if (
                    this.$window.scrollTop() + this.$window.height() > this.$mainToolbar.offset().top &&
                    this.nextPageExists === true &&
                    this.ajaxInProgress === false
                ) {
                    this.ajaxInProgress = true;

                    $('body').trigger('processStart');

                    $.get({
                        url: this.$nextPageButton.attr('href'),
                        cache: true
                    }, function (dataHtml) {
                        var $dataHtml      = $(dataHtml),
                            $productsItems = this._getProductItems($dataHtml);

                        //DATALAYER START
                        var xObj = this._getDataLayer(dataHtml);
                        if (xObj != '') {
                            window.dataLayer = window.dataLayer || [];
                            var x = JSON.parse(xObj);
                            if (x) {
                                var data = {
                                    event : 'gtm.ext.event',
                                    eventTmp : 'enhancedEcommerce',
                                    eventCategory: 'enhancedEcommerce',
                                    eventAction: 'listImpression',
                                    eventLabel: document.title,
                                    ecommerce: x[0].ecommerce
                                };
                                window.dataLayer.push(data);
                            }
                        }
                        //DATALAYER END

                        this._setToolbarSelector($dataHtml);

                        this.$productsItems.append($productsItems.find('.item'));

                        $('.products-grid li').each(function () {
                            var $productImg = $(this).find('.product-item-photo').find('img');
                            var $productImgHover = $(this).find('.product-item-photo-hover').find('img');

                            $productImgHover.attr('src', $productImgHover.attr('data-amsrc'));

                            $productImg.attr('src', $productImg.attr('data-amsrc'));
                        });

                        this.ajaxInProgress = false;
                        $('body').trigger('processStop');
                    }.bind(this));
                }
            },

            _getDataLayer: function(myString) {
                let m;
                let strTmp1;

                m = myString.match(/^\s*dlObjects =(.*)/m);

                if (m) {
                    strTmp1 = m[1];
                    return (strTmp1.substring(strTmp1.length -1 , 1));
                }
                return 'null';
            },
        });
    };
});
