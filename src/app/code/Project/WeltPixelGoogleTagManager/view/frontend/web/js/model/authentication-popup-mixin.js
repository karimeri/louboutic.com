define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/utils/wrapper'
], function ($, modal, wrapper) {
    'use strict';

    return function (authenticationPopup) {
        authenticationPopup.showModal = wrapper.wrapSuper(authenticationPopup.showModal, function () {
            $(this.modalWindow).modal('openModal').trigger('contentUpdated');
            // track checkout authentication popup
            window.dataLayer.push({
                'event': 'gtm.ext.event',
                'eventTmp': 'authenticationPopup',
                'eventCategory': 'authenticationPopup',
                'eventAction': 'authenticationPopup',
                'eventLabel': 'yes'
            });
        });

        return authenticationPopup;
    };
});
