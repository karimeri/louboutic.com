define([
    'jquery'
], function($) {
    'use strict';

    return function (target) {
        $.widget('synolia.gmap', target, {
            _initAutcomplete: function() {
                var input = $(this.options.autocompleteInput).get(0),
                    searchBox = new window.google.maps.places.SearchBox(input),
                    map = this.mapInstance;

                /* Bias the SearchBox results towards current map's viewport. */
                map.addListener('bounds_changed', function() {
                    searchBox.setBounds(map.getBounds());
                });

                /* When use click on search result */
                searchBox.addListener('places_changed', function() {
                    var places = searchBox.getPlaces();

                    /* Place not found */
                    if (places.length === 0) {
                        return;
                    }

                    var place = places[0];

                    window.dataLayer.push({
                        'event': 'gtm.ext.event',
                        'eventTmp': 'storeLocatorUse',
                        'eventCategory': 'storeLocatorUse',
                        'eventAction': $('#stockist-search-term').val(),
                        'eventLabel': document.referrer
                    });

                    this.lastSearchedPlace = place;

                    this._centerMapOnPosition(place.geometry.location, 13);

                    if (this._getMarkersDataFromVisibleMarkers().length === 0) {
                        this._prepareNoResultsInfoWindow(places[0]);
                    }
                }.bind(this));

                this.autocompleteInstance = searchBox;

                return this;
            },
        });
    };
});