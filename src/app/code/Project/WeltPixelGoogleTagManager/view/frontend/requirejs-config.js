var config = {

    config: {
        mixins: {
            'Synolia_Retailer/js/gmap': {
                'Project_WeltPixelGoogleTagManager/js/gmap-mixin': true
            },
            'Magento_Catalog/js/synolia/infinite-scrolling': {
                'Project_WeltPixelGoogleTagManager/js/synolia/infinite-scrolling-mixin': true
            },
            'Magento_Customer/js/model/authentication-popup': {
                'Project_WeltPixelGoogleTagManager/js/model/authentication-popup-mixin': true
            }
        }
    }
};