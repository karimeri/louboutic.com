<?php

namespace Project\WeltPixelGoogleTagManager\Plugin;

/**
 * Class ShippingInformation
 * @package Project\WeltPixelGoogleTagManager\Plugin
 *
 * @deprecated
 */
class ShippingInformation
{
    /**
     * @var \Project\WeltPixelGoogleTagManager\Helper\Data
     */
    protected $helper;

    /**
     * Quote repository.
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @param \Project\WeltPixelGoogleTagManager\Helper\Data $helper
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Project\WeltPixelGoogleTagManager\Helper\Data $helper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->helper = $helper;
        $this->quoteRepository = $quoteRepository;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param \Closure $proceed
     * @param int $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return \Magento\Checkout\Api\Data\PaymentDetailsInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $result,
        $cartId,
        $addressInformation
    )
    {
        if (!$this->helper->isEnabled()) {
            return $result;
        }

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        $shippingDescription = $quote->getShippingAddress()->getShippingDescription();
        if ($shippingDescription) {
            $data = [
                1 => $shippingDescription
            ];
            $this->_checkoutSession->setCheckoutOptionsData($this->helper->addCheckoutStepPushData('2', $data));
            $this->_checkoutSession->setCheckoutEventData($this->helper->addCheckoutEventData('1', $data));
        }

        return $result;
    }
}
