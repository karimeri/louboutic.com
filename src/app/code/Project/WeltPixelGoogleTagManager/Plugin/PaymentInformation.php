<?php

namespace Project\WeltPixelGoogleTagManager\Plugin;

/**
 * Class PaymentInformation
 * @package Project\WeltPixelGoogleTagManager\Plugin
 *
 * @deprecated
 */
class PaymentInformation
{
    /**
     * @var \Project\WeltPixelGoogleTagManager\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @param \Project\WeltPixelGoogleTagManager\Helper\Data $helper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepository
     */
    public function __construct(
        \Project\WeltPixelGoogleTagManager\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository
    ) {
        $this->helper = $helper;
        $this->_checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
     * @return int Order ID.
     */
    public function afterSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        $result,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress
    )
    {
        if (!$this->helper->isEnabled()) {
            return $result;
        }

        $orderId = $result;

        $order = $this->orderRepository->get($orderId);
        $additionalInformation = $order->getPayment()->getAdditionalInformation();
        $shippingDescription = $order->getShippingDescription();

        if ($shippingDescription && $additionalInformation && isset($additionalInformation['method_title'])) {
            $paymentMethodTitle = $additionalInformation['method_title'];
            $data = [
                1 => $shippingDescription,
                2 => $paymentMethodTitle
            ];
            $this->_checkoutSession->setCheckoutOptionsData($this->helper->addCheckoutStepPushData('2', $data));
        }

        return $result;
    }

    /**
     * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
     * @param boolean $result
     * @param int $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @return boolean
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSavePaymentInformation(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        $result,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress
    )
    {
        if (!$this->helper->isEnabled()) {
            return $result;
        }

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->getActive($cartId);
        $shippingDescription = $quote->getShippingAddress()->getShippingDescription();
        $paymentMethodTitle = $paymentMethod->getMethod();
        if ($shippingDescription && $paymentMethodTitle) {
            $data = [
                1 => $shippingDescription,
                2 => $paymentMethodTitle
            ];
            $this->_checkoutSession->setCheckoutOptionsData($this->helper->addCheckoutStepPushData('2', $data));
        }

        return $result;
    }
}
