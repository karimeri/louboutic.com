<?php

namespace Project\WeltPixelGoogleTagManager\Controller\Guest;

use Magento\Rma\Controller\Guest\Submit as GuestSubmit;
use Magento\Framework\Event\ManagerInterface;
use Magento\Rma\Model\Rma;
use Magento\Rma\Model\Rma\Status\History;

class Submit
{

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;


    /**
     * Launch constructor.
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(
        ManagerInterface $eventManager
    ) {
        $this->eventManager       = $eventManager;
    }


    public function afterExecute(GuestSubmit $subject, $result)
    {

        $this->eventManager->dispatch('analytics_guest_rma_save_after');

        return $result;

    }
}