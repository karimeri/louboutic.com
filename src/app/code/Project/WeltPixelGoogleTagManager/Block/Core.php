<?php

namespace Project\WeltPixelGoogleTagManager\Block;

/**
 * Class Core
 * @package Project\WeltPixelGoogleTagManager\Block
 */
class Core extends \WeltPixel\GoogleTagManager\Block\Core
{
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * Core constructor.
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \WeltPixel\GoogleTagManager\Helper\Data $helper
     * @param \WeltPixel\GoogleTagManager\Model\Storage $storage
     * @param \WeltPixel\GoogleTagManager\Model\Dimension $dimensionModel
     * @param \WeltPixel\GoogleTagManager\Model\CookieManager $cookieManager
     * @param array $data
     */
    public function __construct(
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\View\Element\Template\Context $context,
        \WeltPixel\GoogleTagManager\Helper\Data $helper,
        \WeltPixel\GoogleTagManager\Model\Storage $storage,
        \WeltPixel\GoogleTagManager\Model\Dimension $dimensionModel,
        \WeltPixel\GoogleTagManager\Model\CookieManager $cookieManager,
        array $data = []
    ) {
        parent::__construct($context, $helper, $storage, $dimensionModel, $cookieManager, $data);
        $this->customerSession = $session;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @return bool|false|string
     */
    public function isCustomerLoggedIn()
    {
        return $this->jsonSerializer->serialize($this->customerSession->isLoggedIn());
    }
}
