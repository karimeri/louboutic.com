<?php
namespace Project\WeltPixelGoogleTagManager\Block;

/**
 * Class \WeltPixel\GoogleTagManager\Block\Category
 */
class Category extends \WeltPixel\GoogleTagManager\Block\Category
{

    /**
     * @return int
     */
    public function getLimit()
    {
        /** @var \Magento\Catalog\Block\Product\ProductList\Toolbar $productListBlockToolbar */
        $productListBlockToolbar = $this->_layout->getBlock('product_list_toolbar');
        if (empty($productListBlockToolbar)) {
            return 9;
        }

        return (int) $productListBlockToolbar->getLimit();
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        $page = (int) $this->_request->getParam('p');
        if (!$page) {
            return 1;
        }

        return $page;
    }

}
