<?php

namespace Project\WeltPixelGoogleTagManager\Block;

/**
 * Class Checkout
 * @package Project\WeltPixelGoogleTagManager\Block
 */
class Checkout extends \WeltPixel\GoogleTagManager\Block\Checkout
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;

    /**
     * Checkout constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \WeltPixel\GoogleTagManager\Helper\Data $helper
     * @param \WeltPixel\GoogleTagManager\Model\Storage $storage
     * @param \WeltPixel\GoogleTagManager\Model\Dimension $dimensionModel
     * @param \WeltPixel\GoogleTagManager\Model\CookieManager $cookieManager
     * @param array $data
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Framework\View\Element\Template\Context $context,
        \WeltPixel\GoogleTagManager\Helper\Data $helper,
        \WeltPixel\GoogleTagManager\Model\Storage $storage,
        \WeltPixel\GoogleTagManager\Model\Dimension $dimensionModel,
        \WeltPixel\GoogleTagManager\Model\CookieManager $cookieManager,
        array $data = []
    ) {
        parent::__construct($context, $helper, $storage, $dimensionModel, $cookieManager, $data);
        $this->checkoutSession = $checkoutSession;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @return array|bool|false|string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\SessionException
     */
    public function getProducts() {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->getCheckoutSession()->getQuote();
        $products = [];
        $displayOption = $this->helper->getParentOrChildIdUsage();

        foreach ($quote->getAllVisibleItems() as $item) {
            $product = $item->getProduct();
            $productIdModel = $product;
            if ($displayOption == \WeltPixel\GoogleTagManager\Model\Config\Source\ParentVsChild::CHILD) {
                if ($item->getProductType() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    $children = $item->getChildren();
                    foreach ($children as $child) {
                        $productIdModel = $child->getProduct();
                    }
                }
            }
            $productDetail = [];

            $productDetail['id'] = $this->helper->getGtmProductId($productIdModel);
            $productDetail['name'] = html_entity_decode($item->getName());
            //get prices
            $prices = $this->helper->getPriceInclAndExclTax($product);
            $productDetail['price'] = number_format($prices['inclTax'], 2, '.', '');
            $productDetail['priceHT'] = number_format($prices['exclTax'], 2, '.', '');
            $productDetail['category'] = $this->helper->getGtmCategoryCode($product);
            $productDetail['quantity'] = $item->getQty();

            $productDetail['dimension1'] = '';
            $variant = $this->helper->checkVariantForProduct($product);
            if ($variant) {
                $size = explode(':', $variant);
                $productDetail['dimension1'] =  trim($size[1]);
            }

            $productDetail['dimension2'] =  $this->helper->getQuoteItemAttributeValue($item, 'color_product_page');

            if ($this->helper->isBrandEnabled()) {
                $productDetail['brand'] = $this->helper->getGtmBrand($product);
            }

            /**  Set the custom dimensions */
            $customDimensions = $this->getProductDimensions($product);
            foreach ($customDimensions as $name => $value) :
                $productDetail[$name] = $value;
            endforeach;
            unset($productDetail['dimension4']);

            $products[] = $productDetail;
        }

        return $this->jsonSerializer->serialize($products);
    }

    /**
     * @return bool|false|string
     */
    public function getPageData()
    {
        $pageData = $this->helper->getPageData();
        $pageData['pageCategory'] = 'checkout';
        return $this->jsonSerializer->serialize($pageData);
    }

    /**
     * @return bool|false|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreCurrencyCode()
    {
        $currencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
        return $this->jsonSerializer->serialize($currencyCode);
    }

    /**
     * Returns checkout session object.
     *
     * @return \Magento\Checkout\Model\Session
     * @throws \Magento\Framework\Exception\SessionException
     */
    private function getCheckoutSession()
    {
        if (!$this->checkoutSession->isSessionExists()) {
            $this->checkoutSession->start();
        }
        return $this->checkoutSession;
    }
}
