<?php

namespace Project\WeltPixelGoogleTagManager\Block;

/**
 * Class Success
 * @package Project\WeltPixelGoogleTagManager\Block
 */
class Success extends \WeltPixel\GoogleTagManager\Block\Core
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * Success constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \WeltPixel\GoogleTagManager\Helper\Data $helper
     * @param \WeltPixel\GoogleTagManager\Model\Storage $storage
     * @param \WeltPixel\GoogleTagManager\Model\Dimension $dimensionModel
     * @param \WeltPixel\GoogleTagManager\Model\CookieManager $cookieManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \WeltPixel\GoogleTagManager\Helper\Data $helper,
        \WeltPixel\GoogleTagManager\Model\Storage $storage,
        \WeltPixel\GoogleTagManager\Model\Dimension $dimensionModel,
        \WeltPixel\GoogleTagManager\Model\CookieManager $cookieManager,
        array $data = []
    ) {
        parent::__construct($context, $helper, $storage, $dimensionModel, $cookieManager, $data);
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @return array
     */
    public function getProducts() {
        $order = $this->_checkoutSession->getLastRealOrder();

        $products = [];
        $displayOption = $this->helper->getParentOrChildIdUsage();

        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            $product = $item->getProduct();
            $productIdModel = $product;
            if ($displayOption == \WeltPixel\GoogleTagManager\Model\Config\Source\ParentVsChild::CHILD) {
                if ($item->getProductType() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    $children = $item->getChildren();
                    foreach ($children as $child) {
                        $productIdModel = $child->getProduct();
                    }
                }
            }
            $productDetail = [];

            $productDetail['id'] = $this->helper->getGtmProductId($productIdModel);
            $productDetail['name'] = html_entity_decode($item->getName());
            //get prices
            $prices = $this->helper->getPriceInclAndExclTax($product);
            $productDetail['price'] = number_format($prices['inclTax'], 2, '.', '');
            $productDetail['priceHT'] = number_format($prices['exclTax'], 2, '.', '');
            $productDetail['category'] = $this->helper->getGtmCategoryCode($product);
            $productDetail['quantity'] = intval($item->getQtyOrdered());

            $productDetail['dimension1'] = '';
            $productOptions = $item->getProductOptions();
            $productType = $item->getProductType();
            $variant = $this->helper->checkVariantForProductOptions($productOptions, $productType);
            if ($variant && is_string($variant)) {
                $size = explode(':', $variant);
                if (is_array($size) && array_key_exists(1, $size)) {
                    $productDetail['dimension1'] =  trim($size[1]);
                }
            }

            $productDetail['dimension2'] = html_entity_decode($product->getData('color_product_page'));

            if ($this->helper->isBrandEnabled()) {
                $productDetail['brand'] = $this->helper->getGtmBrand($product);
            }

            /**  Set the custom dimensions */
            $customDimensions = $this->getProductDimensions($product);
            foreach ($customDimensions as $name => $value) :
                $productDetail[$name] = $value;
            endforeach;
            unset($productDetail['dimension4']);

            $products[] = $productDetail;
        }

        return $products;
    }
}
