<?php
namespace Project\WeltPixelGoogleTagManager\Block;

/**
 * Class \WeltPixel\GoogleTagManager\Block\Order
 */
class Order extends \WeltPixel\GoogleTagManager\Block\Order
{
    /**
     * Returns the product details for the purchase gtm event
     * @return array
     */
    public function getProducts()
    {
        $order = $this->getOrder();
        $products = [];

        $displayOption = $this->helper->getParentOrChildIdUsage();

        foreach ($order->getAllVisibleItems() as $item) {
            $product = $item->getProduct();
            $productIdModel = $product;
            if ($displayOption == \WeltPixel\GoogleTagManager\Model\Config\Source\ParentVsChild::CHILD) {
                if ($item->getProductType() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    $children = $item->getChildrenItems();
                    foreach ($children as $child) {
                        $productIdModel = $child->getProduct();
                    }
                }
            }

            $productDetail = [];
            $productDetail['name'] = html_entity_decode($item->getName());
            $productDetail['id'] = $this->helper->getGtmProductId($productIdModel); //$this->helper->getGtmOrderItemId($item);
            //get prices
            $prices = $this->helper->getPriceInclAndExclTax($product);
            $productDetail['price'] = number_format($prices['inclTax'], 2, '.', '');
            $productDetail['priceHT'] = number_format($prices['exclTax'], 2, '.', '');
            $productDetail['category'] = $this->helper->getGtmCategoryCode($product);
            $productDetail['quantity'] = intval($item->getQtyOrdered());

            if ($this->helper->isBrandEnabled()) {
                $productDetail['brand'] = $this->helper->getGtmBrand($product);
            }
            $productDetail['dimension1'] = '';
            $productDetail['dimension2'] = html_entity_decode($product->getData('color_product_page'));
            if ($this->helper->isVariantEnabled()) {
                $productOptions = $item->getData('product_options');
                $productType = $item->getData('product_type');
                $variant = $this->helper->checkVariantForProductOptions($productOptions, $productType);
                if ($variant) {
                    $size = explode(':', $variant);
                    $productDetail['dimension1'] =  trim($size[1]);
                }
            }

            /**  Set the custom dimensions */
            $customDimensions = $this->getProductDimensions($product);
            foreach ($customDimensions as $name => $value) :
                $productDetail[$name] = $value;
            endforeach;

            $products[] = $productDetail;
        }

        return $products;
    }
}
