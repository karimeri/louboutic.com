<?php

namespace Project\WeltPixelGoogleTagManager\Block;

/**
 * Class Wishlist
 * @package Project\WeltPixelGoogleTagManager\Block
 */
class Wishlist extends \WeltPixel\GoogleTagManager\Block\Core
{
    /**
     * @param $wishlistItem
     * @return array|string
     */
    public function getSize($wishlistItem)
    {
        $size = '';
        $variant = $this->helper->checkVariantForProduct($wishlistItem->getProduct(), $wishlistItem->getBuyRequest(), $wishlistItem);
        if ($variant) {
            $size = explode(':', $variant);
            $size = trim($size[1]);
        }
        return $size;
    }
}
