<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Project\Wishlist\Block\Catalog\Product\View\AddTo;

/**
 * Product view wishlist block
 *
 * @api
 * @since 100.1.1
 */
class Wishlist extends \Magento\Wishlist\Block\Catalog\Product\View\AddTo\Wishlist
{
   

    /**
     * Return wishlist params
     *
     * @return string
     * @since 100.1.1
     */
    public function getWishlistParams()
    {
        $product = $this->getProduct();
        return $this->_wishlistHelper->getAddParams($product);
    }

    
}
