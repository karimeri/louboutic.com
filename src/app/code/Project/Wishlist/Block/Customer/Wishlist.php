<?php

namespace Project\Wishlist\Block\Customer;

/**
 * Class Wishlist
 * @package Project\Wishlist\Block\Customer
 */
class Wishlist extends \Magento\Wishlist\Block\Customer\Wishlist
{
    const MAX_WISHLIST_PRODUCTS = 32;

    /**
     * @inheritdoc
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('My Wish List'));
        $this->getChildBlock('wishlist_item_pager')
            ->setUseContainer(
                true
            )->setShowAmounts(
                true
            )->setFrameLength(
                $this->_scopeConfig->getValue(
                    'design/pagination/pagination_frame',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                )
            )->setJump(
                $this->_scopeConfig->getValue(
                    'design/pagination/pagination_frame_skip',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                )
            )->setLimit(
                self::MAX_WISHLIST_PRODUCTS // louboutin custom limit
            )
            ->setCollection($this->getWishlistItems());
        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection($collection)
    {
        $collection->setInStockFilter(true)->setOrder('added_at', 'DESC');
        return $this;
    }
}
