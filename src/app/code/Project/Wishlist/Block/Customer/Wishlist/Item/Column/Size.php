<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace Project\Wishlist\Block\Customer\Wishlist\Item\Column;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Image\UrlBuilder;
use Magento\Framework\View\ConfigInterface;

/**
 * Wishlist block customer item cart column.
 *
 * @api
 * @deprecated 101.1.2
 * @since 100.0.2
 */
class Size extends \Magento\Wishlist\Block\Customer\Wishlist\Item\Column
{

      /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;


    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     * @param ConfigInterface|null $config
     * @param UrlBuilder|null $urlBuilder
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        array $data = [],
        ConfigInterface $config = null,
        UrlBuilder $urlBuilder = null,
        ProductRepositoryInterface $productRepository
        ) {
            $this->productRepository = $productRepository;
            parent::__construct(
            $context,
            $httpContext,
            $data,
            $config,
            $urlBuilder
        );
    }


    /**
     * get Product Options function
     *
     * @return object
     */
    function getProductOptions()
    {
        $item = $this->getItem();
        $buyRequest = !empty($item->getOptionByCode('info_buyRequest'))
            ? json_decode($item->getOptionByCode('info_buyRequest')->getValue())
            : null;
            
        return $buyRequest;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    function getSimpleProduct()
    {
        $options = $this->getProductOptions();
        $wishlist_selected_configurable_option = !empty($options->wishlist_selected_configurable_option)
        ? $options->wishlist_selected_configurable_option
        :  null;
       
        if (empty($wishlist_selected_configurable_option)) return null;

        $product = $this->productRepository->getById($wishlist_selected_configurable_option);
        
        return $product;
    }
}
