<?php

namespace Project\Wishlist\Block\Header;

use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Wishlist\Block\Link;

/**
 * Class Wishlist
 * @package Project\Wishlist\Block\Header
 */
class Wishlist extends Link
{
    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * Wishlist constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Wishlist\Helper\Data $wishlistHelper
     * @param JsonSerializer $jsonSerializer
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        JsonSerializer $jsonSerializer,
        array $data = []
    ) {
        parent::__construct($context, $wishlistHelper, $data);
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @return bool|false|string
     */
    public function getWishlistUrl() {
        return $this->jsonSerializer->serialize($this->getUrl('wishlist'));
    }

    /**
     * @return string
     */
    public function getRedirectUrl() {
        return $this->getUrl('wishlist/index/index');
    }
}
