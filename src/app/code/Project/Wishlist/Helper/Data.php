<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\Wishlist\Helper;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\ActionInterface;
use Magento\Wishlist\Controller\WishlistProviderInterface;

/**
 * Wishlist Data Helper
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 *
 * @api
 * @since 100.0.2
 */
class Data extends \Magento\MultipleWishlist\Helper\Data
{

    const PRODUCT_SIZE_ATTRIBUTE_ID = 602;

    public function getAddParams($item, array $params = [])
    {
        $productId = null;
        if ($item instanceof \Magento\Catalog\Model\Product) {
            $productId = $item->getEntityId();
        }
        if ($item instanceof \Magento\Wishlist\Model\Item) {
            $productId = $item->getProductId();
        }

        $url = $this->_getUrlStore($item)->getUrl('wishlist/index/addCustom');
        if ($productId) {
            $params['product'] = $productId;
        }

        return $this->_postDataHelper->getPostData($url, $params);
    }

    /**
     * @param array $requestParams
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateRequestParamsOptions(array $requestParams)
    {
        if (!empty($requestParams['wishlist_selected_configurable_option'])) {

            /** @var Product $product */
            $simpleProduct = $this->productRepository->getById($requestParams['wishlist_selected_configurable_option']);

            if ($simpleProduct) {
                $requestParams['super_attribute'] = array($this::PRODUCT_SIZE_ATTRIBUTE_ID => $simpleProduct->getData('size'));
            }
        }
        return $requestParams;
    }
}