<?php
namespace Project\Wishlist\Model;

class Item
{

    function beforeAddToCart(\Magento\Wishlist\Model\Item $subject, \Magento\Checkout\Model\Cart $cart, $delete)
    {
        //keep product in whishlist when adding it to cart
        return [$cart, false];
    }
}