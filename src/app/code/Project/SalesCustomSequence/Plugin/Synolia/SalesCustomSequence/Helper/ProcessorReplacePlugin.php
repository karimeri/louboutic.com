<?php
namespace Project\SalesCustomSequence\Plugin\Synolia\SalesCustomSequence\Helper;

use Synolia\SalesCustomSequence\Helper\ProcessorReplace;
use Project\Wms\Helper\Config;

/**
 * Class ProcessorReplacePlugin
 * @package Project\SalesCustomSequence\Plugin\Synolia\SalesCustomSequence\Helper
 * @author Synolia <contact@synolia.com>
 */
class ProcessorReplacePlugin
{
    /**
     * @var Config
     */
    protected $wmsHelper;

    /**
     * ProcessorReplacePlugin constructor.
     * @param Config $wmsHelper
     */
    public function __construct(Config $wmsHelper)
    {
        $this->wmsHelper = $wmsHelper;
    }

    /**
     * @param ProcessorReplace $subject
     * @param $result
     * @return array
     */
    public function afterGetPatterns(ProcessorReplace $subject, $result)
    {
        return array_merge($result, ['/{STOCKCODE}/']);
    }

    /**
     * @param ProcessorReplace $subject
     * @param $result
     * @return array
     */
    public function afterGetReplacements(ProcessorReplace $subject, $result)
    {
        return array_merge(
            $result,
            [$this->wmsHelper->getStockCode()]
        );
    }

}