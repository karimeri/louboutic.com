<?php

namespace Project\Beauty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Lipstick
 * @package Project\Beauty\Helper
 * @author Synolia <contact@synolia.com>
 */
class Lips extends AbstractHelper
{
    const XML_PATH_BEAUTY_CATEGORY_LIPSTICK_CATEGORY_LIST = 'louboutin_beauty/category/lipstick_category_list';

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $categoryHelper;

    /**
     * @var array
     */
    protected $dataIndexSvgMap = [
        // @codingStandardsIgnoreStart
        [
            'd'     => 'M0,0 C25.5,0 51,1.9 76.3,5.7 L0,512 L0,512 L0,0 L0,0 Z',
            'id'    => 'Fill-0',
            'color' => '#A11400',
            'sku'   => '0810413028818',
        ],
        [
            'd'     => 'M76.3,5.7 C101.6,9.5 126.5,15.2 150.9,22.7 L0,512 L0,512 L76.3,5.7 L76.3,5.7 Z',
            'id'    => 'Fill-1',
            'color' => '#F6264D',
            'sku'   => '0810413028825',
        ],
        [
            'd'     => 'M150.9,22.8 C175.3,30.3 199.1,39.7 222.1,50.8 L0,512 L150.9,22.8 L150.9,22.8 Z',
            'id'    => 'Fill-2',
            'color' => '#FF4973',
            'sku'   => '0810413028832',
        ],
        [
            'd'     => 'M222.1,50.7 C245.1,61.8 267.3,74.6 288.4,89 L0,512 L0,512 L222.1,50.7 L222.1,50.7 Z',
            'id'    => 'Fill-3',
            'color' => '#ED6370',
            'sku'   => '0810413028849',
        ],
        [
            'd'     => 'M288.4,89 C309.5,103.4 329.5,119.3 348.2,136.7 L0,512 L0,512 L288.4,89 L288.4,89 Z',
            'id'    => 'Fill-4',
            'color' => '#F66958',
            'sku'   => '0810413028856',
        ],
        [
            'd'     => 'M348.2,136.7 C366.9,154.1 384.3,172.8 400.2,192.8 L0,512 L0,512 L348.2,136.7 L348.2,136.7 Z',
            'id'    => 'Fill-5',
            'color' => '#D85C64',
            'sku'   => '0810413028863',
        ],
        [
            'd'     => 'M400.3,192.8 C416.2,212.8 430.6,233.9 443.4,256 L0,512 L0,512 L400.3,192.8 L400.3,192.8 Z',
            'id'    => 'Fill-6',
            'color' => '#A12A2A',
            'sku'   => '0810413028870',
        ],
        [
            'd'     => 'M443.4,256 C456.2,278.1 467.3,301.2 476.6,325 L0,512 L0,512 L443.4,256 L443.4,256 Z',
            'id'    => 'Fill-7',
            'color' => '#984448',
            'sku'   => '0810413028887',
        ],
        [
            'd'     => 'M476.6,325 C485.9,348.8 493.5,373.2 499.2,398.1 L0,512 L0,512 L476.6,325 L476.6,325 Z',
            'id'    => 'Fill-8',
            'color' => '#CA6986',
            'sku'   => '0810413028894',
        ],
        [
            'd'     => 'M499.2,398.1 C504.9,423 508.7,448.3 510.6,473.8 L0,512 L0,512 L499.2,398.1 L499.2,398.1 Z',
            'id'    => 'Fill-9',
            'color' => '#76192E',
            'sku'   => '0810413028900',
        ],
        [
            'd'     => 'M510.6,473.7 C512.5,499.2 512.5,524.7 510.6,550.2 L0,512 L0,512 L510.6,473.7 L510.6,473.7 Z',
            'id'    => 'Fill-10',
            'color' => '#450116',
            'sku'   => '0810413028917',
        ],
        [
            'd'     => 'M510.6,550.3 C508.7,575.8 504.9,601.1 499.2,626 L0,512 L0,512 L510.6,550.3 L510.6,550.3 Z',
            'id'    => 'Fill-11',
            'color' => '#66011D',
            'sku'   => '0810413028924',
        ],
        [
            'd'     => 'M499.2,625.9 C493.5,650.8 486,675.2 476.6,699 L0,512 L499.2,625.9 L499.2,625.9 Z',
            'id'    => 'Fill-12',
            'color' => '#CF1D15',
            'sku'   => '0810413028931',
        ],
        [
            'd'     => 'M476.6,699 C467.3,722.8 456.2,745.8 443.4,768 L0,512 L476.6,699 L476.6,699 Z',
            'id'    => 'Fill-13',
            'color' => '#DE1F00',
            'sku'   => '0810413028948',
        ],
        [
            'd'     => 'M443.4,768 C430.6,790.1 416.2,811.3 400.3,831.2 L0,512 L0,512 L443.4,768 L443.4,768 Z',
            'id'    => 'Fill-14',
            'color' => '#FF2D47',
            'sku'   => '0810413028955',
        ],
        [
            'd'     => 'M400.3,831.2 C384.4,851.2 367,869.9 348.3,887.3 L0,512 L0,512 L400.3,831.2 L400.3,831.2 Z',
            'id'    => 'Fill-15',
            'color' => '#E75943',
            'sku'   => '0810413028962',
        ],
        [
            'd'     => 'M348.2,887.3 C329.5,904.7 309.5,920.6 288.4,935 L0,512 L348.2,887.3 L348.2,887.3 Z',
            'id'    => 'Fill-16',
            'color' => '#CF5B47',
            'sku'   => '0810413028979',
        ],
        [
            'd'     => 'M288.4,935 C267.3,949.4 245.1,962.2 222.1,973.3 L0,512 L288.4,935 L288.4,935 Z',
            'id'    => 'Fill-17',
            'color' => '#D9816B',
            'sku'   => '0810413028986',
        ],
        [
            'd'     => 'M222.1,973.3 C199.1,984.4 175.3,993.7 150.9,1001.3 L0,512 L0,512 L222.1,973.3 L222.1,973.3 Z',
            'id'    => 'Fill-18',
            'color' => '#AD6554',
            'sku'   => '0810413028993',
        ],
        [
            'd'     => 'M150.9,1001.2 C126.5,1008.7 101.6,1014.4 76.3,1018.2 L0,512 L150.9,1001.2 L150.9,1001.2 Z',
            'id'    => 'Fill-19',
            'color' => '#82240A',
            'sku'   => '0810413029006',
        ],
        [
            'd'     => 'M469.448909,512.0893 L477,0 L553.3,506.3 C528,510.1 494.948909,512.0893 469.448909,512.0893 Z',
            'id'    => 'Fill-20',
            'color' => '#BF1903',
            'sku'   => '0810413021055',
        ],
        [
            'd'     => 'M394.745957,505.91768 L477,0 L471.601433,512.001842 C446.101433,512.001842 419.898063,510.403589 394.745957,505.91768 Z',
            'id'    => 'Fill-21',
            'color' => '#FE3058',
            'sku'   => '0810413021338',
        ],
        [
            'd'     => 'M320.67703,487.390804 L477,0 L394.770222,505.853461 C369.470222,502.053461 345.07703,494.890804 320.67703,487.390804 Z',
            'id'    => 'Fill-22',
            'color' => '#FF3040',
            'sku'   => '0810413021345',
        ],
        [
            'd'     => 'M321.650724,487.644802 C297.250724,480.144802 272.79465,469.66857 249.79465,458.56857 L477,0 L320.701755,487.326182 L321.650724,487.644802 Z',
            'id'    => 'Fill-23',
            'color' => '#B73A4A',
            'sku'   => '0810413021369',
        ],
        [
            'd'     => 'M186.316746,421.344979 L477,0 L249.82908,458.645555 C226.82908,447.545555 207.416746,435.744979 186.316746,421.344979 Z',
            'id'    => 'Fill-24',
            'color' => '#C86265',
            'sku'   => '0810413021352',
        ],
        [
            'd'     => 'M126.085189,372.71365 L477,0 L186.323792,421.370667 C164.434138,406.455221 144.785189,390.11365 126.085189,372.71365 Z',
            'id'    => 'Fill-25',
            'color' => '#6F0C27',
            'sku'   => '0810413021376',
        ],
        [
            'd'     => 'M74.3932687,316.164926 L477,0 L126.082593,372.710276 C107.282593,355.410276 90.2932687,336.164926 74.3932687,316.164926 Z',
            'id'    => 'Fill-26',
            'color' => '#CD1C02',
            'sku'   => '0810413021383',
        ],
        [
            'd'     => 'M30.680629,250.750048 L477,0 L74.5687816,316.159061 C58.6687816,296.159061 43.480629,272.850048 30.680629,250.750048 Z',
            'id'    => 'Fill-27',
            'color' => '#FD2A18',
            'sku'   => '0810413021390',
        ],
        [
            'd'     => 'M0.4,187.1 L477,0 L30.694921,250.766624 C17.894921,228.666624 9.7,210.8 0.4,187.1 Z',
            'id'    => 'Fill-28',
            'color' => '#843C32',
            'sku'   => '0810413021406',
        ],
        [
            'd'     => 'M12.9247545,621.054397 L513,512 L36.4,699 C27.1,675.2 18.5247545,645.954397 12.9247545,621.054397 Z',
            'id'    => 'Fill-29',
            'color' => '#B11705',
            'sku'   => '0810413021048',
        ],
        [
            'd'     => 'M2.05786266,543.634391 L513,512 L12.8505491,621.382584 C7.15054907,596.482584 3.95786266,569.134391 2.05786266,543.634391 Z',
            'id'    => 'Fill-30',
            'color' => '#EB243F',
            'sku'   => '0810413021253',
        ],
        [
            'd'     => 'M2.64916478,469.901201 L513,512 L2.044015,543.692862 C0.144015,518.192862 0.749164783,495.301201 2.64916478,469.901201 Z',
            'id'    => 'Fill-31',
            'color' => '#B44A47',
            'sku'   => '0810413021260',
        ],
        [
            'd'     => 'M14.9386219,393.329878 L513,512 L2.68446955,469.95978 C4.58446955,444.45978 9.23862191,418.229878 14.9386219,393.329878 Z',
            'id'    => 'Fill-32',
            'color' => '#840028',
            'sku'   => '0810413023578',
        ],
        [
            'd'     => 'M37.4646296,322.023149 L513,512 L14.9098551,393.452529 C20.6098551,368.552529 28.1646296,345.823149 37.4646296,322.023149 Z',
            'id'    => 'Fill-33',
            'color' => '#5D1735',
            'sku'   => '0810413021277',
        ],
        [
            'd'     => 'M69.6,256 L513,512 L37.5294857,322.152861 C46.8294857,298.352861 56.8,278.2 69.6,256 Z',
            'id'    => 'Fill-34',
            'color' => '#71101F',
            'sku'   => '0810413021284',
        ],
        [
            'd'     => 'M69.6,256 C82.4,233.9 96.8,212.7 112.7,192.8 L513,512 L513,512 L69.6,256 L69.6,256 Z',
            'id'    => 'Fill-35',
            'color' => '#64001D',
            'sku'   => '0810413023585'
        ],
        [
            'd'     => 'M112.7,192.8 C128.6,172.8 146,154.1 164.8,136.7 L513,512 L112.7,192.8 L112.7,192.8 Z',
            'id'    => 'Fill-36',
            'color' => '#A01529',
            'sku'   => '0810413021291',
        ],
        [
            'd'     => 'M164.8,136.7 C183.5,119.3 203.5,103.4 224.6,89 L513,512 L513,512 L164.8,136.7 L164.8,136.7 Z',
            'id'    => 'Fill-37',
            'color' => '#af0002',
            'sku'   => '0810413023592',
        ],
        [
            'd'     => 'M224.6,89 C245.7,74.6 267.9,61.8 290.9,50.7 L513,512 L513,512 L224.6,89 L224.6,89 Z',
            'id'    => 'Fill-38',
            'color' => '#D01D02',
            'sku'   => '0810413021307',
        ],
        [
            'd'     => 'M290.9,50.7 C313.9,39.6 337.7,30.3 362.1,22.7 L513,512 L513,512 L290.9,50.7 L290.9,50.7 Z',
            'id'    => 'Fill-39',
            'color' => '#FF3C39',
            'sku'   => '0810413023608',
        ],
        [
            'd'     => 'M362.1,22.8 C386.5,15.3 411.4,9.6 436.7,5.8 L513,512 L513,512 L362.1,22.8 L362.1,22.8 Z',
            'id'    => 'Fill-40',
            'color' => '#B96A59',
            'sku'   => '0810413021314',
        ],
        [
            'd'     => 'M436.7,5.7 C462,1.9 487.5,0 513,0 L513,512 L513,512 L436.7,5.7 L436.7,5.7 Z',
            'id'    => 'Fill-41',
            'color' => '#884431',
            'sku'   => '0810413021321',
        ]
        // @codingStandardsIgnoreEnd
    ];

    /**
     * Lips constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        CategoryRepositoryInterface $categoryRepository,
        CategoryHelper $categoryHelper
    ) {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->categoryHelper = $categoryHelper;

        parent::__construct($context);
    }

    /**
     * Get path of svg
     * @param int $dataIndex
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSvgPath(int $dataIndex)
    {
        $path = '';

        if (!isset($this->dataIndexSvgMap[$dataIndex])) {
            return '';
        }

        $values = $this->dataIndexSvgMap[$dataIndex];
        $sku = $values['sku'];
        $product = $this->getProductBySku($sku);

        if ($product) {
            $category = $this->getCategory($product);

            $path = '<path class="wheel-color"
                       d="' . $values['d'] . '"
                       id="' . $values['id'] . '"
                       stroke="' . $values['color'] . '"
                       fill="' . $values['color'] . '"
                       data-color-name="' . $this->getColorName($product) . '"
                       data-category-name="' . $this->getCategoryName($category) . '"
                       data-product-url="' . $this->getProductUrl($product) . '"
                       data-category-url="' . $this->getCategoryUrl($category) . '"
                       data-index="' . $dataIndex . '"

                 />';
        }


        return $path;
    }

    /**
     * @param string $sku
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProductBySku(string $sku)
    {
        try {
            $product = $this->productRepository->get($sku);
        } catch (\Throwable $e) {
            return false;
        }
        return $product;
    }

    /**
     * Get color name
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return mixed
     */
    public function getColorName(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        return $product->getColorProductPage();
    }

    /**
     * Get product url
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return mixed
     */
    public function getProductUrl(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        return $product->getProductUrl();
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return bool|\Magento\Catalog\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCategory(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        $productCategoryIds = $product->getCategoryIds();
        $lipstickCategoryIds = $this->getLipstickCategoryIds();

        $categoryIds = explode(',', $lipstickCategoryIds);

        $categoryIntersection = array_intersect($productCategoryIds, $categoryIds);
        $categoryId = reset($categoryIntersection);

        if (!$categoryId) {
            return false;
        }

        return $this->getCategoryById($categoryId);
    }

    /**
     * Get category by id
     * @param int $categoryId
     * @return \Magento\Catalog\Api\Data\CategoryInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCategoryById(int $categoryId)
    {
        return $this->categoryRepository->get($categoryId);
    }

    /**
     * @param \Magento\Catalog\Api\Data\CategoryInterface $category
     * @return string
     */
    public function getCategoryName($category)
    {
        if (!$category) {
            return '';
        }

        return $category->getName();
    }

    /***
     * Get category url
     * @param \Magento\Catalog\Api\Data\CategoryInterface $category
     * @return string
     */
    public function getCategoryUrl($category)
    {
        if (!$category) {
            return '';
        }

        return $this->categoryHelper->getCategoryUrl($category);
    }

    /**
     * @param int|null $storeId
     * @return mixed
     */
    public function getLipstickCategoryIds(int $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $this::XML_PATH_BEAUTY_CATEGORY_LIPSTICK_CATEGORY_LIST,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
