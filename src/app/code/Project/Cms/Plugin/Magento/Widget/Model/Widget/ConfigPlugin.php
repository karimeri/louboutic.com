<?php

namespace Project\Cms\Plugin\Magento\Widget\Model\Widget;

/**
 * Class ConfigPlugin
 * @package Project\Cms\Plugin\Magento\Widget\Model\Widget
 */
class ConfigPlugin
{
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepo;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\CollectionFactory
     */
    protected $blockCollectionFactory;

    /**
     * ConfigPlugin constructor.
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $blockCollectionFactory
    ) {
        $this->_assetRepo = $assetRepo;
        $this->blockCollectionFactory = $blockCollectionFactory;
    }

    /**
     * @param \Magento\Widget\Model\Widget\Config $subject
     * @param \Magento\Framework\DataObject $result
     * @param \Magento\Framework\DataObject $config
     * @return \Magento\Framework\DataObject
     */
    public function afterGetConfig(
        \Magento\Widget\Model\Widget\Config $subject,
        \Magento\Framework\DataObject $result,
        \Magento\Framework\DataObject $config
    ) {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $blockCollection */
        $blockCollection = $this->blockCollectionFactory->create();
        $blocks = $blockCollection->getItems();
        $data = $result->convertToArray();
        /** @var \Magento\Cms\Model\Block $block */
        foreach ($blocks as $block) {
            if (isset( $data['plugins'][1])) {
                $data['plugins'][1]['options']['blocks'][$block->getId()] = $block->getTitle();
                $data['plugins'][1]['options']['blocks'][$block->getIdentifier()] = $block->getTitle();
            }
        }
        return $result->addData($data);
    }

    /**
     * Return url to wysiwyg plugin
     *
     * @param \Magento\Widget\Model\Widget\Config $subject
     * @param $result
     * @return string
     */
    public function afterGetWysiwygJsPluginSrc(\Magento\Widget\Model\Widget\Config $subject, $result)
    {
        return $this->_assetRepo->getUrl('Project_Cms::js/editor_plugin.js');
    }
}
