<?php

namespace Project\Cms\Setup;

use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepository;

/**
 * Class UpgradeData
 * @package   Project\Cms\Setup
 * @author    Synolia <contact@synolia.com>
 */
class UpgradeData implements UpgradeDataInterface
{
    const VERSIONS = [
        '1.0.1' => '101',
        '1.0.2' => '102',
        '1.0.3' => '103',
        '1.0.4' => '104'
    ];


    const STORE_CODE_FR_FR = 'fr_fr';
    const STORE_CODE_UK_EN = 'uk_en';
    const STORE_CODE_IT_EN = 'it_en';
    const STORE_CODE_DE_EN = 'de_en';
    const STORE_CODE_ES_EN = 'es_en';
    const STORE_CODE_CH_EN = 'ch_en';
    const STORE_CODE_CH_FR = 'ch_fr';
    const STORE_CODE_NL_EN = 'nl_en';
    const STORE_CODE_LU_FR = 'lu_fr';
    const STORE_CODE_LU_EN = 'lu_en';
    const STORE_CODE_BE_FR = 'be_fr';
    const STORE_CODE_BE_EN = 'be_en';
    const STORE_CODE_AT_EN = 'at_en';
    const STORE_CODE_IE_EN = 'ie_en';
    const STORE_CODE_PT_EN = 'pt_en';
    const STORE_CODE_MC_FR = 'mc_fr';
    const STORE_CODE_MC_EN = 'mc_en';
    const STORE_CODE_GR_EN = 'gr_en';



    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var \Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * UpgradeData constructor.
     * @param ConsoleOutput      $output
     */
    public function __construct(
        ConsoleOutput $output,
        ConfigSetupFactory $configSetupFactory,
        StoreManager $storeManager,
        StoreRepository $storeRepository
    ) {
        $this->output        = $output;
        $this->objectManager = ObjectManager::getInstance();
        $this->configSetupFactory = $configSetupFactory;
        $this->storeManager       = $storeManager;
        $this->storeRepository    = $storeRepository;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $this->output->writeln(""); // new line in console

        foreach (self::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Cms setup : $version");

                $currentSetup = $this->getObjectManager()->create('Project\Cms\Setup\Upgrade\Upgrade'.$fileData);
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ModuleDataSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return ConfigSetup
     */
    public function getConfigSetup()
    {
        return $this->configSetup;
    }

    /**
     * @return StoreManager
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * @param string $storeCode
     * @return int
     */
    public function getStoreId($storeCode)
    {
        return $this->getStoreManager()->getStore($storeCode)->getId();
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocale()
    {
        return [
            'fr_FR' => [
                $this->storeRepository->get(self::STORE_CODE_FR_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CH_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_FR)->getId()
            ],
            'en_GB' => [
                $this->storeRepository->get(self::STORE_CODE_CH_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_UK_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_DE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_ES_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_NL_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_AT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_PT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_GR_EN)->getId()
            ]
        ];
    }
}
