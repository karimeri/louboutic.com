<?php

namespace Project\Cms\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\PageFactory;


/**
 * @package Project\Cms\Setup\Upgrade
 * @author BONI Jean-Michel
 */
class Upgrade103
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $pageFactory;

    /**
     * @var \Eu\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * Upgrade102 constructor.
     * @param CmsSetup $cmsSetup
     * @param PageFactory PageFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        PageFactory $pageFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->pageFactory = $pageFactory;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeObject)
    {
        $this->updateCmsPage($upgradeObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function updateCmsPage($upgradeDataObject)
    {
        $cmsPageIdentifier = 'cookies';
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsPage = [
                'identifier'        => $cmsPageIdentifier,
                'title'             => 'Cookies',
                'page_layout'       => 'nofullscreen',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 0,
                'stores'            => $stores
            ];
            $this->cmsSetup->savePage($cmsPage);
        }
    }

}
