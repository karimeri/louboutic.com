<?php

namespace Project\Cms\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\PageFactory;


/**
 * @package Project\Cmas\Setup\Upgrade
 * @author Karim Esseddouk
 */
class Upgrade101
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $blockFactory;

    /**
     * @var \Eu\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * Upgrade144 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     * @param PageFactory PageFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        PageFactory $blockFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeObject)
    {
        $this->updateCmsPage($upgradeObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function updateCmsPage($upgradeDataObject)
    {
        $cmsPageIdentifier = 'faq';
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsPage = [
                'identifier'           => $cmsPageIdentifier,
                'layout_update_selected' => 'Faq',
                'stores'            => $stores
            ];
            $this->cmsSetup->savePage($cmsPage);
        }
    }

}
