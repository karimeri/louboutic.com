<?php

namespace Project\Cms\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

/**
 * Class Upgrade138
 * @package Project\Cms\Setup\Upgrade
 * @author Rime BENTIKOUK
 */
class Upgrade104
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade138 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeDataObject)
    {
        $this->addCmsBlocks($upgradeDataObject);
        $this->addSliders($upgradeDataObject);
        $this->addCmsPage($upgradeDataObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsBlocks($upgradeDataObject)
    {
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {

            $templatePath = 'misc/cms/blocks/'.$locale;

            $topSection = $this->cmsSetup->getCmsBlockContent(
                'christmas-gift-guide-top',
                'Project_Cms',
                '',
                '',
                $templatePath
            );
            $contentSection = $this->cmsSetup->getCmsBlockContent(
                'christmas-gift-guide-content',
                'Project_Cms',
                '',
                '',
                $templatePath
            );
            $bottomSection = $this->cmsSetup->getCmsBlockContent(
                'christmas-gift-guide-bottom',
                'Project_Cms',
                '',
                '',
                $templatePath
            );

            $cmsBlocks = [
                [
                    'title' => 'Christmas gift guide Top Section',
                    'identifier' => 'christmas-gift-guide-top',
                    'content' => $topSection,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Christmas gift guide content Section',
                    'identifier' => 'christmas-gift-guide-content',
                    'content' => $contentSection,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ],
                [
                    'title' => 'Christmas gift guide bottom Section',
                    'identifier' => 'christmas-gift-guide-bottom',
                    'content' => $bottomSection,
                    'is_active' => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ]
            ];

            $this->cmsSetup->saveMultipleBlocks($cmsBlocks, false);
        }
    }

    /**
     * @param $upgradeDataObject
     */
    public function addSliders($upgradeDataObject)
    {
        $sliders = [
            [
                'identifier'     => 'gift-guid-main-products-slider',
                'title'          => 'GIFT Guid Christmas - main Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-main-products-slider',
                        'title'        => 'GIFT Guid Christmas - main Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3201420J145','3201304M024','3201195R251'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-for-her-products-slider',
                'title'          => 'GIFT Guid Christmas - forHer Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-for-her-products-slider',
                        'title'        => 'GIFT Guid Christmas - forHer Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3201420J145','3201304M024','3201195R251'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-for-him-products-slider',
                'title'          => 'GIFT Guid Christmas - forHim Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-for-him-products-slider',
                        'title'        => 'GIFT Guid Christmas - forHim Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-stocking-fillers-products-slider',
                'title'          => 'GIFT Guid Christmas - stocking fillers Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-stocking-fillers-products-slider',
                        'title'        => 'GIFT Guid Christmas - stocking fillers Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3201420J145','3201304M024','3201195R251'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-leather-goods-products-slider',
                'title'          => 'GIFT Guid Christmas - leather-goods Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-leather-goods-products-slider',
                        'title'        => 'GIFT Guid Christmas - leather-goods Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3201420J145','3201304M024','3201195R251'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-beauty-products-slider',
                'title'          => 'GIFT Guid Christmas - beauty Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-beauty-products-slider',
                        'title'        => 'GIFT Guid Christmas - beauty Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3201420J145','3201304M024','3201195R251'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-evening-her-products-slider',
                'title'          => 'GIFT Guid Christmas - evening-her Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-evening-her-products-slider',
                        'title'        => 'GIFT Guid Christmas - evening-her Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3201420J145','3201304M024','3201195R251'],
                    ],
                ]
            ],
            [
                'identifier'     => 'gift-guid-evening-him-products-slider',
                'title'          => 'GIFT Guid Christmas - evening-him Products Slider',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'gift-guid-evening-him-products-slider',
                        'title'        => 'GIFT Guid Christmas - evening-him Products Slider',
                        'type'         => 0,
                        'link'         => '/',
                        'target'       => '_self',
                        'position'     => 0,
                        'image_small'  => '',
                        'image_medium' => '',
                        'image_big'    => '',
                        'content'      => '',
                        'associated_products' => ['3615482993961','3615482993961','3615482993961'],
                    ],
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @param $upgradeDataObject
     */
    public function addCmsPage($upgradeDataObject)
    {
        $blockIds = [
            "christmas-gift-guide-top",
            "christmas-gift-guide-content",
            "christmas-gift-guide-bottom",
        ];

        $guide = "";
        foreach ($blockIds as $blockId) {
            $guide .= '
                <div>
                    {{widget type="Magento\Cms\Block\Widget\Block"
                             template="widget/static_block/default.phtml"
                             block_id="'. $blockId .'"}}
                </div>';
        }
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            ($locale == 'fr_FR') ? $ln = 'fr' : $ln = 'en';
            $christmasGuidePage = [
                'title' => 'Christmas gift guide '.$ln,
                'page_layout' => 'nofullscreen',
                'identifier' => 'christmas-gift-guide',
                'content_heading' => '',
                'content' => $guide,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->cmsSetup->savePage($christmasGuidePage, false);
        }
    }
}
