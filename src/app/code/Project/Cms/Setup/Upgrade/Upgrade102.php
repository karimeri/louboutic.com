<?php

namespace Project\Cms\Setup\Upgrade;

use Synolia\Standard\Setup\CmsSetup;
use Magento\Cms\Model\PageFactory;


/**
 * @package Project\Cms\Setup\Upgrade
 * @author BONI Jean-Michel
 */
class Upgrade102
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $pageFactory;

    /**
     * @var \Eu\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * Upgrade102 constructor.
     * @param CmsSetup $cmsSetup
     * @param PageFactory PageFactory
     */
    public function __construct(
        CmsSetup $cmsSetup,
        PageFactory $pageFactory
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->pageFactory = $pageFactory;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run($upgradeObject)
    {
        $this->updateCmsPage($upgradeObject);
    }

    /**
     * @param $upgradeDataObject
     */
    public function updateCmsPage($upgradeDataObject)
    {
        $cmsPageIdentifier = 'binding-corporate-rules';
        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsPage = [
                'identifier'        => $cmsPageIdentifier,
                'stores'            => $stores,
                'title'             => 'Binding corporate rules',
                'page_layout'       => 'nofullscreen',
                'content_heading'   => '',
                'content'           => '',
                'is_active'         => 0
            ];
            $this->cmsSetup->savePage($cmsPage);
        }
    }

}
