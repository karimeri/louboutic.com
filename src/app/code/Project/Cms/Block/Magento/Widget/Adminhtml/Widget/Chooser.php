<?php

namespace Project\Cms\Block\Magento\Widget\Adminhtml\Widget;

/**
 * Class Chooser
 * @package Project\Cms\Block\Magento\Widget\Adminhtml\Widget
 */
class Chooser extends \Magento\Widget\Block\Adminhtml\Widget\Chooser
{
    /**
     * Return chooser HTML and init scripts
     *
     * @return string
     */
    protected function _toHtml()
    {
        $element = $this->getElement();
        /* @var $fieldset \Magento\Framework\Data\Form\Element\Fieldset */
        $fieldset = $element->getForm()->getElement($this->getFieldsetId());
        $chooserId = $this->getUniqId();
        $config = $this->getConfig();

        // add chooser element to fieldset
        $chooser = $fieldset->addField(
            'chooser' . $element->getId(),
            'note',
            ['label' => $config->getLabel() ? $config->getLabel() : '', 'value_class' => 'value2']
        );
        $hiddenHtml = '';
        if ($this->getHiddenEnabled()) {
            $hidden = $this->_elementFactory->create('hidden', ['data' => $element->getData()]);
            $hidden->setId("{$chooserId}value")->setForm($element->getForm());
            if ($element->getRequired()) {
                $hidden->addClass('required-entry');
            }
            $hiddenHtml = $hidden->getElementHtml();
            $element->setValue('');
        }

        $buttons = $config->getButtons();
        $chooseButton = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Button::class
        )->setType(
            'button'
        )->setId(
            $chooserId . 'control'
        )->setClass(
            'btn-chooser'
        )->setLabel(
            $buttons['open']
        )->setOnclick(
            $chooserId . '.choose()'
        )->setDisabled(
            $element->getReadonly()
        );
        $chooser->setData('after_element_html', $hiddenHtml . $chooseButton->toHtml());

        // render label and chooser scripts
        $configJson = $this->_jsonEncoder->encode($config->getData());
        // Louboutin custom : add block edit link
        $blockUrl = $this->getBlockUrl() ? ' <a href="' . $this->getBlockUrl() . '" target="_blank">' . __('Open Block') . '</a>' : '';
        return '
            <label class="widget-option-label" id="' .
            $chooserId .
            'label">' .
            ($this->getLabel() ? $this->escapeHtml($this->getLabel()) : __(
                'Not Selected'
            )) .
            '</label>' .
            $blockUrl . '
            <div id="' .
            $chooserId .
            'advice-container" class="hidden"></div>
            <script>
            require(["prototype", "mage/adminhtml/wysiwyg/widget"], function(){
            //<![CDATA[
                (function() {
                    var instantiateChooser = function() {
                        window.' .
            $chooserId .
            ' = new WysiwygWidget.chooser(
                            "' .
            $chooserId .
            '",
                            "' .
            $this->getSourceUrl() .
            '",
                            ' .
            $configJson .
            '
                        );
                        if ($("' .
            $chooserId .
            'value")) {
                            $("' .
            $chooserId .
            'value").advaiceContainer = "' .
            $chooserId .
            'advice-container";
                        }
                    }

                    jQuery(instantiateChooser);
                })();
            //]]>
            });
            </script>
        ';
    }
}
