<?php

namespace Project\Cms\Block\Magento\Cms\Adminhtml\Block\Widget;

use Magento\Cms\Ui\Component\Listing\Column\BlockActions;

/**
 * Class Chooser
 * @package Project\Cms\Block\Magento\Cms\Adminhtml\Block\Widget
 */
class Chooser extends \Magento\Cms\Block\Adminhtml\Block\Widget\Chooser
{
    /**
     * Prepare chooser element HTML
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element Form Element
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     */
    public function prepareElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $uniqId = $this->mathRandom->getUniqueHash($element->getId());
        $sourceUrl = $this->getUrl('cms/block_widget/chooser', ['uniq_id' => $uniqId]);

        $chooser = $this->getLayout()->createBlock(
            \Magento\Widget\Block\Adminhtml\Widget\Chooser::class
        )->setElement(
            $element
        )->setConfig(
            $this->getConfig()
        )->setFieldsetId(
            $this->getFieldsetId()
        )->setSourceUrl(
            $sourceUrl
        )->setUniqId(
            $uniqId
        );

        if ($element->getValue()) {
            $block = $this->_blockFactory->create()->load($element->getValue());
            if ($block->getId()) {
                $chooser->setLabel($this->escapeHtml($block->getTitle()));
                // Louboutin custom : add block edit url
                $chooser->setBlockUrl(
                    $this->_urlBuilder->getUrl(
                        BlockActions::URL_PATH_EDIT,
                        [
                            'block_id' => $block->getId(),
                        ]
                    )
                );
            }
        }

        $element->setData('after_element_html', $chooser->toHtml());
        return $element;
    }
}
