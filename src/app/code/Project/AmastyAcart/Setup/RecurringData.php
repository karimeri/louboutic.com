<?php

namespace Project\AmastyAcart\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class RecurringData
 * @package Project\AmastyAcart\Setup
 */
class RecurringData implements InstallDataInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * UpgradeData constructor.
     * @param ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
            $this->updateAbandonedCartEmailTemplate($setup);
        $setup->endSetup();
    }

    public function updateAbandonedCartEmailTemplate(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->update(
            'email_template',
            ['is_legacy' => 1],
            [
                'orig_template_code = ?' => 'amasty_acart_template'
            ]
        );
    }
}
