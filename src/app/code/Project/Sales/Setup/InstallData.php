<?php

namespace Project\Sales\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Project\Sales\Helper\Config;

/**
 * Class InstallData
 * @package Project\Sales\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    /**
     * array
     */
    protected $fields = [
        Config::FIELD_BASE_AJUSTMENT_SHIPPING_FEES  => 'Base Ajustement Shipping Fees',
        Config::FIELD_AJUSTMENT_SHIPPING_FEES       => 'Ajustement Shipping Fees',
        Config::FIELD_BASE_AJUSTMENT_REPAIRING_FEES => 'Base Ajustement Repairing Fees',
        Config::FIELD_AJUSTMENT_REPAIRING_FEES      => 'Ajustement Repairing Fees',
        Config::FIELD_BASE_AJUSTMENT_CREDIT_NOTE    => 'Base Ajustement Credit Note',
        Config::FIELD_AJUSTMENT_CREDIT_NOTE         => 'Ajustement Credit Note',
        Config::FIELD_BASE_AJUSTMENT_RETURN_CHARGE  => 'Base Ajustement Return Charge',
        Config::FIELD_AJUSTMENT_RETURN_CHARGE       => 'Ajustement Return Charge',
    ];

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $connection = $setup->getConnection();
        if ($connection->isTableExists($setup->getTable('sales_creditmemo'))) {
            foreach ($this->fields as $fieldCode => $comment) {
                $connection->addColumn(
                    $setup->getTable('sales_creditmemo'),
                    $fieldCode,
                    [
                        'type'     => Table::TYPE_DECIMAL,
                        'length'   => '12,4',
                        'nullable' => true,
                        'comment'  => $comment,
                    ]
                );
            }
        }

        $setup->endSetup();
    }
}
