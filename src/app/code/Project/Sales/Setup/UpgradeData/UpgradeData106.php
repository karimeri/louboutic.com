<?php
namespace Project\Sales\Setup\UpgradeData;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Sales\Setup\UpgradeData;

/**
 * Class UpgradeData106
 * Insert New Status
 *
 * @package Project\Sales\Setup\Upgrade
 * @author  Karim Esseddouk
 */
class UpgradeData106
{

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $installer = $upgradeDataObject->getSetup();
        $table     = $installer->getTable('sales_order_status');
        if ($installer->getConnection()->isTableExists($table) == true) {
            $this->initNewStatus($installer);
            $this->initNewStatusState($installer);
        }
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    protected function initNewStatus(ModuleDataSetupInterface $installer)
    {
        $newStatus = [
            [
                'status' => 'packed',
                'label' => 'Packed'
            ]
        ];
        $installer->getConnection()->insertMultiple(
            $installer->getTable('sales_order_status'),
            $newStatus
        );
    }

    /**
     * @param ModuleDataSetupInterface $installer
     */
    private function initNewStatusState(ModuleDataSetupInterface $installer)
    {
        $newStatusState = [
            [
                'status' => 'packed',
                'state' => 'packed',
                'is_default' => 0,
                'visible_on_front' => 1
            ]
        ];
        $installer->getConnection()->insertMultiple(
            $installer->getTable('sales_order_status_state'),
            $newStatusState
        );
    }
}
