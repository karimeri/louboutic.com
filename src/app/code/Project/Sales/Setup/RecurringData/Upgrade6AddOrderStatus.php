<?php

namespace Project\Sales\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Project\Sales\Model\Magento\Sales\Order;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

/**StatusResourceFactory
 * Class Upgrade6AddOrderStatus
 *
 * @package Project\Sales\Setup\RecurringData
 */
class Upgrade6AddOrderStatus implements UpgradeDataSetupInterface
{
    const ORDER_STATUS_CODE = 'waiting_for_customer';
    const ORDER_STATUS_LABEL = 'Waiting for customer';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Status Factory
     *
     * @var StatusFactory
     */
    protected $statusFactory;

    /**
     * Status Resource Factory
     *
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;

    /**
     * Upgrade5ProductQuantity constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource,
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory
    ) {
        $this->connection = $resource->getConnection();
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->addNewOrderStatus();
    }


    /**
     * Create new order status and assign it to the existent state
     *
     * @return void
     *
     * @throws Exception
     */
    protected function addNewOrderStatus()
    {
        /** @var StatusResource $statusResource */
        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => self::ORDER_STATUS_CODE,
            'label' => self::ORDER_STATUS_LABEL,
        ]);

        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {

            return;
        }

        $status->assignState(Order::STATE_PAYMENT_REVIEW, false, true);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add the order status waiting for customer';
    }
}
