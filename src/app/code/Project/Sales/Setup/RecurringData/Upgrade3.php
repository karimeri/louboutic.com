<?php

namespace Project\Sales\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade3
 * @package Project\Sales\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Upgrade3 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        EnvironmentManager $environmentManager
    ) {
        $this->configSetup = $configSetup;
        $this->environmentManager = $environmentManager;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() === Environment::US) {
            $this->configSetup->saveConfig('tax/calculation/cross_border_trade_enabled', 0);
        } else {
            $this->configSetup->saveConfig('tax/calculation/cross_border_trade_enabled', 1);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Change config for cross border trade';
    }
}
