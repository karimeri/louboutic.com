<?php

namespace Project\Sales\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade5ProductQuantity
 *
 * @package Project\Sales\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade5ProductQuantity implements UpgradeDataSetupInterface
{
    const TABLE_ORDER = 'sales_order';
    const TABLE_ORDER_GRID = 'sales_order_grid';
    const ITEM_TOTAL_QTY_ORDERED = 'total_qty_ordered';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade5ProductQuantity constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_ORDER_GRID),
            self::ITEM_TOTAL_QTY_ORDERED,
            [
                'type'     => Table::TYPE_DECIMAL,
                'length'   => '12,4',
                'nullable' => true,
                'comment'  => 'Product quantity',
            ]
        );

        $this->connection->addIndex(
            $this->connection->getTableName(self::TABLE_ORDER_GRID),
            $this->connection->getIndexName(self::TABLE_ORDER_GRID, [self::ITEM_TOTAL_QTY_ORDERED]),
            [self::ITEM_TOTAL_QTY_ORDERED]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add product quantity column to order grid';
    }
}
