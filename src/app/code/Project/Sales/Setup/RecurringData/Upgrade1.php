<?php
namespace Project\Sales\Setup\RecurringData;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Sales\Model\Magento\Sales\Order as ProjectOrder;
use Magento\Sales\Model\Order;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade1
 * @package Project\Sales\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * Upgrade1 constructor.
     * @param ModuleDataSetupInterface $setup
     */
    public function __construct(
        ModuleDataSetupInterface $setup
    ) {
        $this->setup = $setup;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $newStatuses = [
            ProjectOrder::STATE_PROCESSING_RMA => __('Processing RMA'),
            ProjectOrder::STATE_WAITING_CREDITMEMO => __('Waiting for credit memo'),
            ProjectOrder::STATE_RMA_CLOSED => __('RMA closed'),
            ProjectOrder::STATE_CREDITMEMO_REFUNDED => __('Credit memo / Refunded'),
            ProjectOrder::STATE_CREDITMEMO_WAITING_EXCHANGE => __('Credit memo / Waiting for exchange'),
            ProjectOrder::STATE_CREDITMEMO_EXCHANGED => __('Credit memo / Exchanged'),
        ];

        $this
            ->addNewStatuses($newStatuses)
            ->addNewStatusStates($newStatuses);
    }

    /**
     * @param array $newStatuses
     * @return $this
     */
    private function addNewStatuses(array $newStatuses)
    {
        $statuses = [];
        foreach ($newStatuses as $status => $label) {
            $statuses[] = ['status' => $status, 'label' => $label];
        }

        $this->setup
            ->getConnection()
            ->insertMultiple($this->setup->getTable('sales_order_status'), $statuses);

        return $this;
    }

    /**
     * @param array $newStatuses
     * @return $this
     */
    private function addNewStatusStates(array $newStatuses)
    {
        $statusStates = [];
        foreach (\array_keys($newStatuses) as $status) {
            $statusStates[] = [
                'status' => $status,
                'state' => Order::STATE_COMPLETE,
                'is_default' => 0,
                'visible_on_front' => 1,
            ];
        }

        $this->setup
            ->getConnection()
            ->insertMultiple($this->setup->getTable('sales_order_status_state'), $statusStates);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add new order statuses for RMA workflow in DB';
    }
}
