<?php

namespace Project\Sales\Setup\RecurringData;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade4TransactionId
 *
 * @package Project\Sales\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade4TransactionId implements UpgradeDataSetupInterface
{
    const TABLE_ORDER = 'sales_order';
    const TABLE_ORDER_GRID = 'sales_order_grid';
    const ITEM_TRANSACTION_ID = 'transaction_id';

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * Upgrade4TransactionId constructor.
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->connection = $resource->getConnection();
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_ORDER),
            self::ITEM_TRANSACTION_ID,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Transaction Id',
            ]
        );

        $this->connection->addColumn(
            $this->connection->getTableName(self::TABLE_ORDER_GRID),
            self::ITEM_TRANSACTION_ID,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Transaction Id',
            ]
        );

        $this->connection->addIndex(
            $this->connection->getTableName(self::TABLE_ORDER_GRID),
            $this->connection->getIndexName(self::TABLE_ORDER_GRID, [self::ITEM_TRANSACTION_ID]),
            [self::ITEM_TRANSACTION_ID]
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add transaction id column to order grid';
    }
}
