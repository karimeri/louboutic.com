<?php
namespace Project\Sales\Setup\RecurringData;

use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade7
 * @package Project\Sales\Setup\RecurringData
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade7 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade7 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('payment/adyen_hpp/title', 'Paypal');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Change config title paypal';
    }
}
