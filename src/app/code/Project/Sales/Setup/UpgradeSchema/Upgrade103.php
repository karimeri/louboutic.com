<?php

namespace Project\Sales\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Upgrade103
 * @package Project\Sales\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class Upgrade103
{
    const TABLE_ORDER = 'sales_order';
    const TABLE_ORDER_GRID = 'sales_order_grid';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tables = [
            self::TABLE_ORDER,
            self::TABLE_ORDER_GRID
        ];

        foreach ($tables as $table) {
            $columnExists = $setup->getConnection()->tableColumnExists(
                $setup->getTable($table),
                'is_fraud'
            );

            if (!$columnExists) {
                $setup->getConnection()->addColumn(
                    $setup->getTable($table),
                    'is_fraud',
                    [
                        'type' => Table::TYPE_BOOLEAN,
                        'nullable' => true,
                        'comment' => 'Order is fraud',
                        'default' => 0
                    ]
                );
            }
        }
    }
}
