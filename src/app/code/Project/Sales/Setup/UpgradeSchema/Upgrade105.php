<?php

namespace Project\Sales\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Upgrade105
 * @package Project\Sales\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade105
{
    const TABLE_ORDER_GRID = 'sales_order_grid';
    const BILLING_PHONE = 'billing_phone';
    const SHIPPING_PHONE = 'shipping_phone';
    const SOURCE_CODE = 'source_code';
    const REMOTE_IP = 'remote_ip';
    const ORDER_STORE_ID = 'order_store_id';


    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->addColumn(
            $setup->getConnection()->getTableName(self::TABLE_ORDER_GRID),
            self::BILLING_PHONE,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'comment'  => 'Billing Phone',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getConnection()->getTableName(self::TABLE_ORDER_GRID),
            self::SHIPPING_PHONE,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'comment'  => 'Shipping Phone',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getConnection()->getTableName(self::TABLE_ORDER_GRID),
            self::REMOTE_IP,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 45,
                'comment'  => 'Remote IP',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getConnection()->getTableName(self::TABLE_ORDER_GRID),
            self::SOURCE_CODE,
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 6,
                'comment'  => 'Source Code',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getConnection()->getTableName(self::TABLE_ORDER_GRID),
            self::ORDER_STORE_ID,
            [
                'type'     => Table::TYPE_SMALLINT,
                'length'   => 6,
                'comment'  => 'Order Store Id',
            ]
        );

        $setup->getConnection()->addIndex(
            $setup->getConnection()->getTableName(self::TABLE_ORDER_GRID),
            $setup->getConnection()->getIndexName(self::TABLE_ORDER_GRID, [self::ORDER_STORE_ID]),
            [self::ORDER_STORE_ID]
        );
    }

}
