<?php
namespace Project\Sales\Setup\UpgradeSchema;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema101
 * @package Project\Sales\Setup\UpgradeSchema
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema101
{
    const TABLE_ORDER = 'sales_order';
    const XML_PATH_SOURCES = 'synchronizations/customer/general/sources';

    /**
     * @var ScopeConfigInterface
     */
    protected $config;

    /**
     * UpgradeSchema101 constructor.
     * @param ScopeConfigInterface $config
     */
    public function __construct(
        ScopeConfigInterface $config
    ) {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function run(UpgradeSchemaInterface $upgradeSchema)
    {
        $installer = $upgradeSchema->getSetup();

        $sources = \json_decode($this->config->getValue(self::XML_PATH_SOURCES), true);

        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_ORDER),
            'source_code',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 6,
                'nullable' => false,
                'comment'  => 'Source customer code',
                'default'  => $sources[1]['code'],
            ]
        );
    }
}
