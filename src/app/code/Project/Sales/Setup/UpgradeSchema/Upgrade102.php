<?php

namespace Project\Sales\Setup\UpgradeSchema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Upgrade102
 * @package Project\Sales\Setup\UpgradeSchema
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade102
{
    const TABLE_ORDER = 'sales_order';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $columnExists = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_ORDER),
            'items_to_invoice'
        );

        if (!$columnExists) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_ORDER),
                'items_to_invoice',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Items to invoiced if error in the first invoice',
                ]
            );
        }
    }
}
