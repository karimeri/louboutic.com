<?php

namespace Project\Sales\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetupFactory;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class UpgradeData
 * @package Project\Rma\Setup
 * @author  Synolia <contact@synolia.com>
 */
class UpgradeData implements UpgradeDataInterface
{
    const VERSIONS = [
        '1.0.6' => '106'
    ];

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * UpgradeData constructor.
     * @param ConsoleOutput          $output
     * @param ObjectManagerInterface $objectManager
     * @param ConfigSetupFactory     $configSetupFactory
     */
    public function __construct(
        ConsoleOutput $output,
        ObjectManagerInterface $objectManager,
        ConfigSetupFactory $configSetupFactory
    ) {
        $this->output             = $output;
        $this->objectManager      = $objectManager;
        $this->configSetupFactory = $configSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;
        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $this->output->writeln(""); // new line in console

        foreach ($this::VERSIONS as $version => $fileData) {
            if (\version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Sales setup : $version");

                $currentSetup = $this->getObjectManager()->create(
                    'Project\Sales\Setup\UpgradeData\UpgradeData' . $fileData
                );
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ModuleDataSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return ConfigSetup
     */
    public function getConfigSetup()
    {
        return $this->configSetup;
    }
}
