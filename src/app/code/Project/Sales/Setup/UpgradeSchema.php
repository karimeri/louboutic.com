<?php

namespace Project\Sales\Setup;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class UpgradeSchema
 * @package Project\Sales\Setup
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const VERSIONS = [
        '1.0.2' => '102',
        '1.0.3' => '103',
        '1.0.5' => '105',
    ];

    /**
     * ConsoleOutput
     */
    protected $output;

    /**
     * @var SchemaSetupInterface
     */
    protected $setup;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * UpgradeSchema constructor.
     * @param ConsoleOutput          $output
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ConsoleOutput $output,
        ObjectManagerInterface $objectManager
    ) {
        $this->output        = $output;
        $this->objectManager = $objectManager;
    }

    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $setup->startSetup();

        $this->output->writeln(''); // new line in console

        foreach (self::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing Project Sales setup : $version");

                $currentSetup = $this->getObjectManager()
                    ->create('Project\Sales\Setup\UpgradeSchema\Upgrade'.$fileData);
                $currentSetup->upgrade($setup, $context);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return SchemaSetupInterface
     */
    public function getSetup()
    {
        return $this->setup;
    }

    /**
     * @return ObjectManagerInterface
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
