<?php

namespace Project\Sales\Ui\Component\Listing\Column;

use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Project\Wms\Helper\Config as WmsHelper;

/**
 * Class ErpId
 *
 * @package Project\Sales\Ui\Component\Listing\Column
 * @author Synolia <contact@synolia.com>
 */
class ErpId extends Column
{
    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    protected $customerRepository;

    /**
     * @var \Project\Erp\Helper\ErpCustomerId
     */
    protected $helperErp;

    /**
     * @var \Project\Wms\Helper\Config
     */
    protected $wmsHelper;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Status constructor.
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository
     * @param \Project\Erp\Helper\ErpCustomerId $helperErp
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CustomerRepository $customerRepository,
        \Project\Erp\Helper\ErpCustomerId $helperErp,
        WmsHelper $wmsHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->customerRepository = $customerRepository;
        $this->helperErp = $helperErp;
        $this->wmsHelper = $wmsHelper;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param array $dataSource
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $customer = $this->customerRepository->getById($item['entity_id']);
                $orderCollection = $this->collectionFactory->create($customer->getId());
                $lastOrder = $orderCollection->getLastItem();

                if ($lastOrder) {
                    $erpId = $this->helperErp->getCustomerIdentifierCode(
                        $customer->getEmail(),
                        $this->wmsHelper->getEstablishmentCode($lastOrder->getStore()->getWebsiteId())
                    );

                    $item[$this->getData('name')] = $erpId;
                }
            }
        }

        return $dataSource;
    }
}
