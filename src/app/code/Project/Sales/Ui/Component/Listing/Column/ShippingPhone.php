<?php

namespace Project\Sales\Ui\Component\Listing\Column;

use \Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class ShippingPhone
 * @package Project\Sales\Ui\Component\Listing\Column
 * @author Synolia <contact@synolia.com>
 */
class ShippingPhone extends Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['shipping_phone']) && $item['shipping_phone']) {
                    $item[$this->getData('name')] = $item['shipping_phone'];
                }
            }
        }

        return $dataSource;
    }
}
