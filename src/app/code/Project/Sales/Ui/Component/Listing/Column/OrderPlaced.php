<?php

namespace Project\Sales\Ui\Component\Listing\Column;

use Magento\Framework\App\ResourceConnection;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class OrderPlaced
 *
 * @package Project\Sales\Ui\Component\Listing\Column
 * @author Synolia <contact@synolia.com>
 */
class OrderPlaced extends Column
{
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * OrderOrigin constructor.
     *
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ResourceConnection $resource,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->connection = $resource->getConnection();
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                try {
                    $select = $this->connection->select()->from(
                        $this->connection->getTableName('sales_order'),
                        'COUNT(`entity_id`)'
                    )->where('customer_email = ?', $item['customer_email']);
                    $total = $this->connection->fetchOne($select);
                } catch (\Exception $e) {
                    $total = 0;
                }

                $item[$this->getData('name')] = $total;
            }
        }

        return $dataSource;
    }
}
