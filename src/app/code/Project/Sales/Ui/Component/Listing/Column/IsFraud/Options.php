<?php

namespace Project\Sales\Ui\Component\Listing\Column\IsFraud;

use Magento\Framework\Escaper;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 * @package Project\Sales\Ui\Component\Listing\Column\IsFraud
 * @author Synolia <contact@synolia.com>
 */
class Options implements OptionSourceInterface
{
    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * Constructor
     *
     * @param Escaper $escaper
     */
    public function __construct(Escaper $escaper)
    {
        $this->escaper = $escaper;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 0,
                'label' => $this->escaper->escapeHtml(__('Not'))
            ],
            [
                'value' => 1,
                'label' => $this->escaper->escapeHtml(__('Yes'))
            ]
        ];
    }
}
