<?php

namespace Project\Sales\Ui\Component\Listing\Column;

use \Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class TotalQtyOrdered
 *
 * @package Project\Sales\Ui\Component\Listing\Column
 * @author Synolia <contact@synolia.com>
 */
class TotalQtyOrdered extends Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $qty = $item['total_qty_ordered'];
                $item[$this->getData('name')] = (int)$qty;
            }
        }

        return $dataSource;
    }
}
