<?php

namespace Project\Sales\Ui\Component\Listing\Column;

use \Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class BillingPhone
 * @package Project\Sales\Ui\Component\Listing\Column
 * @author Synolia <contact@synolia.com>
 */
class BillingPhone extends Column
{
    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                /** @var \Magento\Sales\Model\Order $order */
                if (isset($item['billing_phone']) && $item['billing_phone']) {
                    $item[$this->getData('name')] = $item['billing_phone'];
                }
            }
        }

        return $dataSource;
    }
}
