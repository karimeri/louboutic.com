<?php

namespace Project\Sales\Ui\Component\Listing\Column\Order;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Project\Erp\Helper\ErpCustomerId;
use Project\Wms\Helper\Config as WmsHelper;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class ErpId
 * @package Project\Sales\Ui\Component\Listing\Column\Order
 * @author Synolia <contact@synolia.com>
 */
class ErpId extends Column
{
    /**
     * @var ErpCustomerId
     */
    protected $helperErp;

    /**
     * @var Config
     */
    protected $wmsHelper;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ErpId constructor.
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Project\Erp\Helper\ErpCustomerId $helperErp
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ErpCustomerId $helperErp,
        WmsHelper $wmsHelper,
        StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );

        $this->helperErp = $helperErp;
        $this->wmsHelper = $wmsHelper;
        $this->storeManager = $storeManager;
    }

    /**
     * @param array $dataSource
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                /** @var \Magento\Sales\Model\Order $order */
                if(isset($item['order_store_id']) && $item['order_store_id'] && $item['customer_email']){
                    $store = $this->storeManager->getStore($item['order_store_id']);
                    if($store->getId()){
                        $erpId = $this->helperErp->getCustomerIdentifierCode(
                            $item['customer_email'],
                            $this->wmsHelper->getEstablishmentCode($store->getWebsiteId())
                        );
                        $item[$this->getData('name')] = $erpId;
                    }

                }

            }
        }

        return $dataSource;
    }
}
