<?php

namespace Project\Sales\Ui\Component\Listing\Column;

use \Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class OrderOrigin
 *
 * @package Project\Sales\Ui\Component\Listing\Column
 * @author Synolia <contact@synolia.com>
 */
class OrderOrigin extends Column
{

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = (isset($item['remote_ip']) && $item['remote_ip']) ? 'FO' : 'BO';
            }
        }

        return $dataSource;
    }
}
