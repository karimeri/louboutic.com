<?php

namespace Project\Sales\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package Project\Sales\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_DISPLAY_SHIPPING_FEES_IN_ADMIN  = 'louboutin_sales/creditmemo/display_shipping_fees_in_admin';
    const XML_PATH_DISPLAY_REPAIRING_FEES_IN_ADMIN = 'louboutin_sales/creditmemo/display_repairing_fees_in_admin';
    const XML_PATH_DISPLAY_CREDIT_NOTE_IN_ADMIN    = 'louboutin_sales/creditmemo/display_credit_note_in_admin';
    const XML_PATH_DISPLAY_RETURN_CHARGE_IN_ADMIN  = 'louboutin_sales/creditmemo/display_return_charge_in_admin';
    const XML_PATH_RETURN_CHARGE                   = 'louboutin_sales/creditmemo/return_charge';
    const XML_PATH_SOURCE_CODE                     = 'synchronizations_customer/general/sources';

    const FIELD_BASE_AJUSTMENT_SHIPPING_FEES  = 'base_adjustment_shipping_fees';
    const FIELD_BASE_AJUSTMENT_REPAIRING_FEES = 'base_adjustment_repairing_fees';
    const FIELD_BASE_AJUSTMENT_CREDIT_NOTE    = 'base_adjustment_credit_note';
    const FIELD_BASE_AJUSTMENT_RETURN_CHARGE  = 'base_adjustment_return_charge';
    const FIELD_AJUSTMENT_SHIPPING_FEES       = 'adjustment_shipping_fees';
    const FIELD_AJUSTMENT_REPAIRING_FEES      = 'adjustment_repairing_fees';
    const FIELD_AJUSTMENT_CREDIT_NOTE         = 'adjustment_credit_note';
    const FIELD_AJUSTMENT_RETURN_CHARGE       = 'adjustment_return_charge';
    const FIELD_AJUSTMENT_TAX_RATE            = 'adjustment_tax_rate';

    /**
     * @param int|null $scopeCode
     * @return bool
     */
    public function isDisplayShippingFeesInAdmin($scopeCode = null)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_DISPLAY_SHIPPING_FEES_IN_ADMIN,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return bool
     */
    public function isDisplayRepairingFeesInAdmin($scopeCode = null)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_DISPLAY_REPAIRING_FEES_IN_ADMIN,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return bool
     */
    public function isDisplayCreditNoteInAdmin($scopeCode = null)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_DISPLAY_CREDIT_NOTE_IN_ADMIN,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return bool
     */
    public function isDisplayReturnChargeInAdmin($scopeCode = null)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_DISPLAY_RETURN_CHARGE_IN_ADMIN,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param int|null $scopeCode
     * @return int|mixed
     */
    public function getReturnCharge($scopeCode = null)
    {
        $returnCharge = $this->scopeConfig->getValue(
            self::XML_PATH_RETURN_CHARGE,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
        return $returnCharge ? $returnCharge : 0;
    }

    /**
     * @param int|null $scopeCode
     * @return array
     */
    public function getAdjustments($scopeCode = null)
    {
        return [
            self::FIELD_AJUSTMENT_SHIPPING_FEES  => [
                'active' => $this->isDisplayShippingFeesInAdmin($scopeCode),
                'label'  => sprintf("%s (+)", __('Shipping Fees'))
            ],
            self::FIELD_AJUSTMENT_REPAIRING_FEES => [
                'active' => $this->isDisplayRepairingFeesInAdmin($scopeCode),
                'label'  => sprintf("%s (-)", __('Repairing Fees'))
            ],
            self::FIELD_AJUSTMENT_CREDIT_NOTE    => [
                'active' => $this->isDisplayCreditNoteInAdmin($scopeCode),
                'label'  => sprintf("%s (+)", __('Credit Note'))
            ],
            self::FIELD_AJUSTMENT_RETURN_CHARGE  => [
                'active' => $this->isDisplayReturnChargeInAdmin($scopeCode),
                'label'  => __('Charge Return To Customer')
            ]
        ];
    }

    /**
     * @param int|null $scopeCode
     * @return array
     */
    public function getSourcesCodes($scopeCode = null)
    {
        return \json_decode($this->scopeConfig->getValue(
            self::XML_PATH_SOURCE_CODE,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        ), true);
    }

    /**
     * @param $code
     * @param null $scopeCode
     * @return string|null
     */
    public function getSourcesCodeLabel($code, $scopeCode = null)
    {
        $sourceCodes = $this->getSourcesCodes($scopeCode);

        foreach ($sourceCodes as $sourceCode) {
            if ($sourceCode['code'] === $code) {
                return $sourceCode['label'];
            }
        }

        return null;
    }
}
