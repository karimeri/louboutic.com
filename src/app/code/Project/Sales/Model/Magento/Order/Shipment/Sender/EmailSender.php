<?php

namespace Project\Sales\Model\Magento\Order\Shipment\Sender;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Payment\Helper\Data;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Sales\Model\Order\Email\Container\ShipmentIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\SenderBuilderFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment;
use Project\Core\Model\Environment;
use Magento\Sales\Model\Order\Pdf\InvoiceFactory as InvoicePdfFactory;
use Magento\Sales\Model\Order\InvoiceRepository;
use Project\Core\Manager\EnvironmentManager;
use Magento\Sales\Model\Order\Shipment\Sender\EmailSender as BaseEmailSender;
use Psr\Log\LoggerInterface;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoiceFactory;

/**
 * Class EmailSender
 * @package Project\Sales\Model\Magento\Order\Shipment\Sender
 * @author Synolia <contact@synolia.com>
 */
class EmailSender extends BaseEmailSender
{
    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * @var InvoicePdfFactory
     */
    protected $invoicePdfFactory;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var Data
     */
    protected $paymentHelper;

    /**
     * @var Shipment
     */
    protected $shipmentResource;

    /**
     * @var ScopeConfigInterface
     */
    protected $globalConfig;

    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoiceFactory
     */
    protected $giftInvoiceFactory;

    /**
     * EmailSender constructor.
     * @param \Magento\Sales\Model\Order\Email\Container\Template $templateContainer
     * @param \Magento\Sales\Model\Order\Email\Container\ShipmentIdentity $identityContainer
     * @param \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param Data $paymentHelper
     * @param Shipment $shipmentResource
     * @param ScopeConfigInterface $globalConfig
     * @param ManagerInterface $eventManager
     * @param \Magento\Sales\Model\Order\InvoiceRepository $invoiceRepository
     * @param \Magento\Sales\Model\Order\Pdf\InvoiceFactory $invoicePdfFactory
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoiceFactory $giftInvoiceFactory
     */
    public function __construct(
        Template $templateContainer,
        ShipmentIdentity $identityContainer,
        SenderBuilderFactory $senderBuilderFactory,
        LoggerInterface $logger,
        Renderer $addressRenderer,
        Data $paymentHelper,
        Shipment $shipmentResource,
        ScopeConfigInterface $globalConfig,
        ManagerInterface $eventManager,
        InvoiceRepository $invoiceRepository,
        InvoicePdfFactory $invoicePdfFactory,
        EnvironmentManager $environmentManager,
        GiftInvoiceFactory $giftInvoiceFactory
    ) {
        parent::__construct(
            $templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $shipmentResource,
            $globalConfig,
            $eventManager
        );
        $this->paymentHelper = $paymentHelper;
        $this->shipmentResource = $shipmentResource;
        $this->globalConfig = $globalConfig;
        $this->eventManager = $eventManager;
        $this->invoiceRepository = $invoiceRepository;
        $this->invoicePdfFactory = $invoicePdfFactory;
        $this->environmentManager = $environmentManager;
        $this->giftInvoiceFactory = $giftInvoiceFactory;
    }

    /**
     * Sends order shipment email to the customer.
     *
     * Email will be sent immediately in two cases:
     *
     * - if asynchronous email sending is disabled in global settings
     * - if $forceSyncMode parameter is set to TRUE
     *
     * Otherwise, email will be sent later during running of
     * corresponding cron job.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \Magento\Sales\Api\Data\ShipmentInterface $shipment
     * @param \Magento\Sales\Api\Data\ShipmentCommentCreationInterface|null $comment
     * @param bool $forceSyncMode
     *
     * @return bool
     * @throws \Exception
     */
    public function send(
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Sales\Api\Data\ShipmentInterface $shipment,
        \Magento\Sales\Api\Data\ShipmentCommentCreationInterface $comment = null,
        $forceSyncMode = false
    ) {
        $shipment->setSendEmail(true);

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            $order = $shipment->getOrder();

            $attachment = null;

            if ($this->environmentManager->getEnvironment() != Environment::JP) {
                try {
                    foreach ($order->getInvoiceCollection() as $lazyInvoice) {
                        $invoice = $this->invoiceRepository->get($lazyInvoice->getId());
                        #Send different pdf template for gift order
                        if ($this->isGiftOrder($order)) {
                            $pdf = $this->giftInvoiceFactory->create()->getPdf([$invoice]);
                        } else {
                            $pdf = $this->invoicePdfFactory->create()->getPdf([$invoice]);
                        }

                        $attachment = $pdf->render();
                    }
                } catch (\Throwable $e) {
                    $attachment = null;
                }
            }

            $transport = [
                'order' => $order,
                'shipment' => $shipment,
                'comment' => $comment ? $comment->getComment() : '',
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                'attachment' => $attachment
            ];

            $this->eventManager->dispatch(
                'email_shipment_set_template_vars_before',
                ['sender' => $this, 'transport' => $transport]
            );

            $this->templateContainer->setTemplateVars($transport);

            if ($this->checkAndSend($order)) {
                $shipment->setEmailSent(true);

                $this->shipmentResource->saveAttribute($shipment, ['send_email', 'email_sent']);

                return true;
            }
        } else {
            $shipment->setEmailSent(null);

            $this->shipmentResource->saveAttribute($shipment, 'email_sent');
        }

        $this->shipmentResource->saveAttribute($shipment, 'send_email');

        return false;
    }

    /**
     * Returns payment info block as HTML.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return string
     */
    private function getPaymentHtml(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()->getStoreId()
        );
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return bool
     */
    protected function isGiftOrder($order)
    {
        return ($order->getGiftMessageId() || $order->getGwId() || $order->getGwItemsBasePriceInclTax());
    }
}
