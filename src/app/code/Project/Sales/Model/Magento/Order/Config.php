<?php

namespace Project\Sales\Model\Magento\Order;

use Magento\Sales\Model\Order\Config as BaseConfig;

/**
 * Class Config
 * @package Project\Sales\Model\Magento\Order
 * @author Synolia <contact@synolia.com>
 */
class Config extends BaseConfig
{
    /**
     * Mask status for order for specified area
     *
     * @param string $area
     * @param string $code
     * @return string
     */
    protected function maskStatusForArea($area, $code)
    {
        return $code;
    }
}
