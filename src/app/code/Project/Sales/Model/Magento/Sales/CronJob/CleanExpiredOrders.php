<?php

namespace Project\Sales\Model\Magento\Sales\CronJob;

use Magento\Sales\Model\CronJob\CleanExpiredOrders as BaseCleanExpiredOrders;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class CleanExpiredOrders
 * @package Project\Sales\Model\Magento\Sales\CronJob
 * @author  Synolia <contact@synolia.com>
 */
class CleanExpiredOrders extends BaseCleanExpiredOrders
{
    /**
     * @inheritdoc
     */
    public function execute()
    {
        $lifetimes = $this->storesConfig->getStoresConfigByPath('sales/orders/delete_pending_after');
        foreach ($lifetimes as $storeId => $lifetime) {
            /** @var $orders \Magento\Sales\Model\ResourceModel\Order\Collection */
            $orders = $this->orderCollectionFactory->create();
            $orders->addFieldToFilter('store_id', $storeId);
            $orders->addFieldToFilter('status', Order::STATE_PENDING_PAYMENT);
            $orders->getSelect()->where(
                new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `updated_at`)) >= '.$lifetime * 60)
            );
            $argsCancelFunction = [
                'state' => Order::STATE_AUTO_CANCEL
            ];
            $orders->walk('cancel', $argsCancelFunction);
            $argsAddStatusFunction = [
                'status' => Order::STATE_AUTO_CANCEL,
                'comment' => 'No payment info received after 1 hour'
            ];
            $orders->walk('addStatusToHistory', $argsAddStatusFunction);
            $orders->walk('save');
        }
    }
}
