<?php

namespace Project\Sales\Model\Magento\Sales;

use Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use Magento\Sales\Model\Order as BaseOrder;
use Magento\Sales\Model\Order\Shipment;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Order
 * @package Project\Sales\Model\Magento\Sales
 * @author  Synolia <contact@synolia.com>
 */
class Order extends BaseOrder
{
    const STATE_AUTO_CANCEL = 'auto_cancel';
    const STATE_SENT_TO_LOGISTIC = 'sent_to_logistic';
    const STATE_PICKING = 'picking';
    const STATE_PACKING = 'packing';
    const STATE_SHIPPING = 'shipping';
    const STATE_CAPTURE_REJECTED = 'capture_rejected';
    const STATE_INVOICED = 'invoiced';
    const STATE_AUTORIZATION_ERROR = 'authorization_error';
    const STATE_LABEL_GENERATION_ERROR = 'label_generation_error';
    const STATE_PAYMENT_AUTHORIZED_PENDING = 'pending_payment';
    const STATE_PAYMENT_AUTHORIZED_PROCESSING = 'processing';
    const STATE_PROCESSING_RMA = 'processing_rma';
    const STATE_WAITING_CREDITMEMO = 'waiting_creditmemo';
    const STATE_RMA_CLOSED = 'rma_closed';
    const STATE_CREDITMEMO_REFUNDED = 'creditmemo_refunded';
    const STATE_CREDITMEMO_WAITING_EXCHANGE = 'creditmemo_waiting_exchange';
    const STATE_CREDITMEMO_EXCHANGED = 'creditmemo_exchanged';
    const STATE_NEED_TO_RESHIP = 'need_to_reship';
    const STATE_READY_TO_RESHIP = 'ready_to_reship';
    const STATE_PREORDER_ENTERING_STOCK = 'preorder_entering_stock';
    const STATE_WAITING_FOR_CUSTOMER = 'waiting_for_customer';
    const STATE_DOCUMENTS_READY_TO_BE_PRINTED = 'documents_ready_to_be_printed';
    const STATE_REFUND_ERROR = 'refund_error';
    const STATE_WAITING_ADYEN = 'waiting_adyen';
    const STATE_SUSPECTED_FRAUD = 'fraud';
    const STATE_PENDING = 'pending';

    const ORDER_NOT_CANCELABLE_STATUS = [
        self::STATE_SUSPECTED_FRAUD,
        self::STATE_WAITING_FOR_CUSTOMER,
        self::STATE_PAYMENT_REVIEW,
        self::STATE_PAYMENT_AUTHORIZED_PENDING,
        self::STATE_PAYMENT_AUTHORIZED_PROCESSING
    ];


    /**
     * @return string|null
     */
    public function getAirwayBillNumber()
    {
        if (!$this->getData('airway_bill_number')) {
            $shipments = $this->getShipmentsCollection();

            /** @var Shipment $shipment */
            foreach ($shipments as $shipment) {
                $tracks = $shipment->getTracksCollection();

                if ($tracks->getLastItem()->getNumber()) {
                    $this->setAirwayBillNumber($tracks->getLastItem()->getNumber());
                }
            }
        }

        return $this->getData('airway_bill_number');
    }

    /**
     * Cancel order
     *
     * @return $this
     */
    public function cancelOffline()
    {
        $this->registerCancellation();

        $this->_eventManager->dispatch('order_cancel_after', ['order' => $this]);

        return $this;
    }

    /**
     * Cancel order
     *
     * @return $this
     */
    public function cancelOrder()
    {
        $this->getPayment()->cancel();
        $this->registerCancellation();
        //whene order is not yet in wms
        if((!in_array($this->getStatus(), self::ORDER_NOT_CANCELABLE_STATUS)) || $this->getState() == self::STATE_WAITING_ADYEN) {
            $this->_eventManager->dispatch('order_cancel_after', ['order' => $this]);
        }
        return $this;
    }

    /**
     * @param string $state
     * @return $this
     * @throws LocalizedException
     */
    public function cancel($state = self::STATE_CANCELED)
    {
        if ($this->canCancel()) {
            $this->getPayment()->cancel();
            $this->registerCancellation('', true, $state);
            $this->_eventManager->dispatch('order_cancel_after', ['order' => $this]);
        }

        return $this;
    }

    /**
     * Retrieve order cancel availability
     *
     * @return bool
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function canCancel()
    {
        $status = $this->getStatus();
        $state = $this->getState();

        if ($status === self::STATE_PAYMENT_AUTHORIZED_PENDING || $status === self::STATE_PAYMENT_AUTHORIZED_PROCESSING || $status == self::STATE_WAITING_ADYEN) {
            return true;
        }

        if (!$this->_canVoidOrder()) {
            return false;
        }

        if ($this->canUnhold()) {
            return false;
        }

        if (!$this->canReviewPayment() && $this->canFetchPaymentReviewUpdate()) {
            return false;
        }

        $allInvoiced = true;
        foreach ($this->getAllItems() as $item) {
            if ($item->getQtyToInvoice()) {
                $allInvoiced = false;
                break;
            }
        }
        if ($allInvoiced) {
            return false;
        }

        ///CL Workflow
        //@TODO Add Status to cancel

        if ($this->isCanceled() || $state === self::STATE_COMPLETE || $state === self::STATE_CLOSED) {
            return false;
        }

        if ($this->getActionFlag(self::ACTION_FLAG_CANCEL) === false) {
            return false;
        }

        return true;
    }

    /**
     * Prepare order totals to cancellation
     *
     * @param string $comment
     * @param bool   $graceful
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function registerCancellation($comment = '', $graceful = true, $cancelState = self::STATE_CANCELED)
    {
        if ($this->canCancel() || $this->isPaymentReview() || $this->isFraudDetected()) {
            $state = $cancelState;
            foreach ($this->getAllItems() as $item) {
                if ($state != self::STATE_PROCESSING && $item->getQtyToRefund()) {
                    if ($item->isProcessingAvailable()) {
                        $state = self::STATE_PROCESSING;
                    } else {
                        $state = self::STATE_COMPLETE;
                    }
                }
                $item->cancel();
            }

            $this->setSubtotalCanceled($this->getSubtotal() - $this->getSubtotalInvoiced());
            $this->setBaseSubtotalCanceled($this->getBaseSubtotal() - $this->getBaseSubtotalInvoiced());

            $this->setTaxCanceled($this->getTaxAmount() - $this->getTaxInvoiced());
            $this->setBaseTaxCanceled($this->getBaseTaxAmount() - $this->getBaseTaxInvoiced());

            $this->setShippingCanceled($this->getShippingAmount() - $this->getShippingInvoiced());
            $this->setBaseShippingCanceled($this->getBaseShippingAmount() - $this->getBaseShippingInvoiced());

            $this->setDiscountCanceled(abs($this->getDiscountAmount()) - $this->getDiscountInvoiced());
            $this->setBaseDiscountCanceled(abs($this->getBaseDiscountAmount()) - $this->getBaseDiscountInvoiced());

            $this->setTotalCanceled($this->getGrandTotal() - $this->getTotalPaid());
            $this->setBaseTotalCanceled($this->getBaseGrandTotal() - $this->getBaseTotalPaid());

            $this->setState($state)
                ->setStatus($this->getConfig()->getStateDefaultStatus($state));
            if (!empty($comment)) {
                $this->addStatusHistoryComment($comment, false);
            }
        } elseif (!$graceful) {
            throw new LocalizedException(__('We cannot cancel this order.'));
        }
        return $this;
    }


    /**
     * Add a comment to order status history.
     *
     * Different or default status may be specified.
     *
     * @param string $comment
     * @param bool|string $status
     * @param bool $isVisibleOnFront
     * @return OrderStatusHistoryInterface
     * @since 101.0.5
     */
    public function addCommentToStatusHistory($comment, $status = false, $isVisibleOnFront = false)
    {
        if (false === $status || null === $status) {
            $status = $this->getStatus();
        } elseif (true === $status) {
            $status = $this->getConfig()->getStateDefaultStatus($this->getState());
        } else {
            $this->setStatus($status);
        }
        $history = $this->_orderHistoryFactory->create()->setStatus(
            $status
        )->setComment(
            $comment
        )->setEntityName(
            $this->entityType
        )->setIsVisibleOnFront(
            $isVisibleOnFront
        );
        $this->addStatusHistory($history);
        return $history;
    }
}
