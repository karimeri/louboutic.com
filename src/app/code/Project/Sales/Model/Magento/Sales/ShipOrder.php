<?php

namespace Project\Sales\Model\Magento\Sales;

use Magento\Sales\Api\Data\ShipmentCommentCreationInterface;
use Magento\Sales\Api\Data\ShipmentCreationArgumentsInterface;
use Magento\Sales\Exception\CouldNotShipException;
use Magento\Sales\Exception\DocumentValidationException;
use Magento\Sales\Model\ShipOrder as BaseShipOrder;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Model\Order\Config as OrderConfig;
use Magento\Sales\Model\Order\OrderStateResolverInterface;
use Magento\Sales\Model\Order\ShipmentDocumentFactory;
use Magento\Sales\Model\Order\Shipment\NotifierInterface;
use Magento\Sales\Model\Order\Shipment\OrderRegistrarInterface;
use Magento\Sales\Model\Order\Validation\ShipOrderInterface as ShipOrderValidator;
use Project\Wms\Model\Service\Mail;
use Psr\Log\LoggerInterface;
use Project\Wms\Model\Service\Mail as MailService;

/**
 * Class ShipOrder
 * @package Project\Sales\Model\Magento\Sales
 * @author  Synolia <contact@synolia.com>
 */
class ShipOrder extends BaseShipOrder
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var ShipmentDocumentFactory
     */
    protected $shipmentDocumentFactory;

    /**
     * @var OrderConfig
     */
    protected $config;

    /**
     * @var ShipmentRepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var ShipOrderValidator
     */
    protected $shipOrderValidator;

    /**
     * @var NotifierInterface
     */
    protected $notifierInterface;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var OrderRegistrarInterface
     */
    protected $orderRegistrar;

    /**
     * @var MailService
     */
    protected $mailService;

    /**
     * @param ResourceConnection          $resourceConnection
     * @param OrderRepositoryInterface    $orderRepository
     * @param ShipmentDocumentFactory     $shipmentDocumentFactory
     * @param OrderStateResolverInterface $orderStateResolver
     * @param OrderConfig                 $config
     * @param ShipmentRepositoryInterface $shipmentRepository
     * @param ShipOrderValidator          $shipOrderValidator
     * @param NotifierInterface           $notifierInterface
     * @param OrderRegistrarInterface     $orderRegistrar
     * @param LoggerInterface             $logger
     * @param MailService                 $mailService
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        OrderRepositoryInterface $orderRepository,
        ShipmentDocumentFactory $shipmentDocumentFactory,
        OrderStateResolverInterface $orderStateResolver,
        OrderConfig $config,
        ShipmentRepositoryInterface $shipmentRepository,
        ShipOrderValidator $shipOrderValidator,
        NotifierInterface $notifierInterface,
        OrderRegistrarInterface $orderRegistrar,
        LoggerInterface $logger,
        MailService $mailService
    ) {
        parent::__construct(
            $resourceConnection,
            $orderRepository,
            $shipmentDocumentFactory,
            $orderStateResolver,
            $config,
            $shipmentRepository,
            $shipOrderValidator,
            $notifierInterface,
            $orderRegistrar,
            $logger
        );

        $this->resourceConnection      = $resourceConnection;
        $this->orderRepository         = $orderRepository;
        $this->shipmentDocumentFactory = $shipmentDocumentFactory;
        $this->config                  = $config;
        $this->shipmentRepository      = $shipmentRepository;
        $this->shipOrderValidator      = $shipOrderValidator;
        $this->notifierInterface       = $notifierInterface;
        $this->logger                  = $logger;
        $this->orderRegistrar          = $orderRegistrar;
        $this->mailService             = $mailService;
    }

    /**
     * @param int                                                             $orderId
     * @param \Magento\Sales\Api\Data\ShipmentItemCreationInterface[]         $items
     * @param bool                                                            $notify
     * @param bool                                                            $appendComment
     * @param \Magento\Sales\Api\Data\ShipmentCommentCreationInterface|null   $comment
     * @param \Magento\Sales\Api\Data\ShipmentTrackCreationInterface[]        $tracks
     * @param \Magento\Sales\Api\Data\ShipmentPackageCreationInterface[]      $packages
     * @param \Magento\Sales\Api\Data\ShipmentCreationArgumentsInterface|null $arguments
     * @return int
     * @throws \Magento\Sales\Api\Exception\DocumentValidationExceptionInterface
     * @throws \Magento\Sales\Api\Exception\CouldNotShipExceptionInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \DomainException
     */
    public function execute(
        $orderId,
        array $items = [],
        $notify = false,
        $appendComment = false,
        ShipmentCommentCreationInterface $comment = null,
        array $tracks = [],
        array $packages = [],
        ShipmentCreationArgumentsInterface $arguments = null
    ) {
        $connection         = $this->resourceConnection->getConnection('sales');
        $order              = $this->orderRepository->get($orderId);
        $shipment           = $this->shipmentDocumentFactory->create(
            $order,
            $items,
            $tracks,
            $comment,
            ($appendComment && $notify),
            $packages,
            $arguments
        );
        $validationMessages = $this->shipOrderValidator->validate(
            $order,
            $shipment,
            $items,
            $notify,
            $appendComment,
            $comment,
            $tracks,
            $packages
        );
        if ($validationMessages->hasMessages()) {
            throw new DocumentValidationException(
                __("Shipment Document Validation Error(s):\n".implode("\n", $validationMessages->getMessages()))
            );
        }
        $connection->beginTransaction();
        try {
            $this->orderRegistrar->register($order, $shipment);
            $order->setState(Order::STATE_COMPLETE);
            $order->setStatus(Order::STATE_COMPLETE);
            $this->shipmentRepository->save($shipment);
            $this->logger->info('Order N '.$order->getIncrementId().' shipped');
            $order->addStatusHistoryComment(__('Order shipped'))
                ->setIsCustomerNotified(false);
            $this->orderRepository->save($order);

            $connection->commit();
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $connection->rollBack();
            $entityInError = [
                'ID : '.$order->getIncrementId().' => '.$e->getMessage()
            ];
            $this->mailService->sendEmail($entityInError, 'order', 'ship');
            $order->addStatusHistoryComment(
                __('An error occurred when the WMS send ship information : %1', $e->getMessage())
            )
                ->setIsCustomerNotified(false);
            $this->orderRepository->save($order);
            throw new CouldNotShipException(
                __('Could not save a shipment, see error log for details')
            );
        }
        if ($notify) {
            $comment = $appendComment ? $comment : null;
            $this->notifierInterface->notify($order, $shipment, $comment);
        }
        return $shipment->getEntityId();
    }
}
