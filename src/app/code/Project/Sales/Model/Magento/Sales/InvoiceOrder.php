<?php

namespace Project\Sales\Model\Magento\Sales;

use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Api\Data\InvoiceCommentCreationInterface;
use Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Exception\CouldNotInvoiceException;
use Magento\Sales\Model\InvoiceOrder as BaseInvoiceOrder;
use Magento\Sales\Model\Order\Config as OrderConfig;
use Magento\Sales\Model\Order\Invoice\NotifierInterface;
use Magento\Sales\Model\Order\InvoiceDocumentFactory;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Sales\Model\Order\OrderStateResolverInterface;
use Magento\Sales\Model\Order\PaymentAdapterInterface;
use Magento\Sales\Model\Order\Validation\InvoiceOrderInterface as InvoiceOrderValidator;
use Project\Sales\Model\Invoice\Service;
use Psr\Log\LoggerInterface;

/**
 * Class InvoiceOrder
 * @package Project\Sales\Model\Magento\Sales
 * @author  Synolia <contact@synolia.com>
 */
class InvoiceOrder extends BaseInvoiceOrder
{
    /**
     * @var Service
     */
    protected $invoiceService;

    /**
     * InvoiceOrder constructor.
     * @param ResourceConnection          $resourceConnection
     * @param OrderRepositoryInterface    $orderRepository
     * @param InvoiceDocumentFactory      $invoiceDocumentFactory
     * @param PaymentAdapterInterface     $paymentAdapter
     * @param OrderStateResolverInterface $orderStateResolver
     * @param OrderConfig                 $config
     * @param InvoiceRepository           $invoiceRepository
     * @param InvoiceOrderValidator       $invoiceOrderValidator
     * @param NotifierInterface           $notifierInterface
     * @param LoggerInterface             $logger
     * @param Service                     $invoiceService
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        OrderRepositoryInterface $orderRepository,
        InvoiceDocumentFactory $invoiceDocumentFactory,
        PaymentAdapterInterface $paymentAdapter,
        OrderStateResolverInterface $orderStateResolver,
        OrderConfig $config,
        InvoiceRepository $invoiceRepository,
        InvoiceOrderValidator $invoiceOrderValidator,
        NotifierInterface $notifierInterface,
        LoggerInterface $logger,
        Service $invoiceService
    ) {
        parent::__construct(
            $resourceConnection,
            $orderRepository,
            $invoiceDocumentFactory,
            $paymentAdapter,
            $orderStateResolver,
            $config,
            $invoiceRepository,
            $invoiceOrderValidator,
            $notifierInterface,
            $logger
        );

        $this->invoiceService = $invoiceService;
    }

    /**
     * @param int                                    $orderId
     * @param bool                                   $capture
     * @param array                                  $items
     * @param bool                                   $notify
     * @param bool                                   $appendComment
     * @param InvoiceCommentCreationInterface|null   $comment
     * @param InvoiceCreationArgumentsInterface|null $arguments
     * @return int|null
     * @throws \Exception
     */
    public function execute(
        $orderId,
        $capture = false,
        array $items = [],
        $notify = false,
        $appendComment = false,
        InvoiceCommentCreationInterface $comment = null,
        InvoiceCreationArgumentsInterface $arguments = null
    ) {
        try {
            $invoiceEntityId = $this->invoiceService->invoice(
                $orderId,
                $capture,
                $items,
                $notify,
                $appendComment,
                $comment,
                $arguments
            );
        } catch (\Throwable $e) {
            throw new CouldNotInvoiceException(__($e->getMessage()));
        }

        return $invoiceEntityId;
    }
}
