<?php

namespace Project\Sales\Model\Magento\Sales;

use Magento\Framework\App\ResourceConnection;

/**
 * Class OrderRepository
 * @package Project\Sales\Model\Magento\Sales
 * @author  Synolia <contact@synolia.com>
 */
class OrderRepository
{
    const TABLE_NAME_STATUS_HISTORY = 'sales_order_status_history';

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * OrderRepository constructor.
     * @param ResourceConnection $connection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Give the state of the order before cancellation
     *
     * @param int $orderId
     */
    public function getLastStateBeforeCancel($orderId)
    {
        $bind = [
            'parent_id' => $orderId,
            'status' => Order::STATE_CANCELED,
            'entity_name' => 'order'
        ];

        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()->from($connection->getTableName(self::TABLE_NAME_STATUS_HISTORY))
            ->where('parent_id = :parent_id')
            ->where('status <> :status')
            ->where('entity_name = :entity_name')
            ->order('created_at DESC')
            ->limit(1);

        return $connection->fetchRow($select, $bind)['status'];
    }

    /**
     * @param array $comments
     */
    public function addCommentOnMultipleOrder(array $comments)
    {
        $connection = $this->resourceConnection->getConnection();
        $connection->insertMultiple($connection->getTableName(self::TABLE_NAME_STATUS_HISTORY), $comments);
    }
}
