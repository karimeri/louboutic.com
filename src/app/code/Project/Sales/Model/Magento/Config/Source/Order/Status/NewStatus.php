<?php

namespace Project\Sales\Model\Magento\Config\Source\Order\Status;

/**
 * Class NewStatus
 * @package Project\Sales\Model\Magento\Config\Source\Order\Status
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.CamelCasePropertyName)
 */
class NewStatus extends \Magento\Sales\Model\Config\Source\Order\Status\NewStatus
{
    /**
     * @var string
     */
    protected $_stateStatuses = [
        \Magento\Sales\Model\Order::STATE_NEW,
        \Magento\Sales\Model\Order::STATE_PROCESSING,
    ];
}
