<?php

namespace Project\Sales\Model\Invoice;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Api\Data\InvoiceCommentCreationInterface;
use Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Exception\CouldNotInvoiceException;
use Magento\Sales\Exception\DocumentValidationException;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\NotifierInterface;
use Magento\Sales\Model\Order\InvoiceDocumentFactory;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Sales\Model\Order\PaymentAdapterInterface;
use Magento\Sales\Model\Order\Validation\InvoiceOrderInterface as InvoiceOrderValidator;
use Project\Sales\Model\Magento\Sales\Order;
use Synolia\Logger\Model\LoggerFactory;
use Synolia\Logger\Model\Logger;
use Magento\Sales\Model\Order\Invoice\ItemCreationFactory;
use Project\Wms\Model\Service\Mail as MailService;

/**
 * Class Service
 * @package Project\Sales\Model\Invoice
 * @author  Synolia <contact@synolia.com>
 */
class Service
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var InvoiceDocumentFactory
     */
    protected $invoiceDocumentFactory;

    /**
     * @var PaymentAdapterInterface
     */
    protected $paymentAdapter;

    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * @var InvoiceOrderValidator
     */
    protected $invoiceOrderValidator;

    /**
     * @var NotifierInterface
     */
    protected $notifierInterface;

    /**
     * @var Logger
     */
    protected $synoliaLogger;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var ItemCreationFactory
     */
    protected $itemCreationFactory;

    /**
     * @var MailService
     */
    protected $mailService;

    /**
     * Service constructor.
     * @param ResourceConnection       $resourceConnection
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceDocumentFactory   $invoiceDocumentFactory
     * @param PaymentAdapterInterface  $paymentAdapter
     * @param InvoiceRepository        $invoiceRepository
     * @param InvoiceOrderValidator    $invoiceOrderValidator
     * @param NotifierInterface        $notifierInterface
     * @param LoggerFactory            $loggerFactory
     * @param DirectoryList            $directoryList
     * @param SerializerInterface      $serializer
     * @param ItemCreationFactory      $itemCreationFactory
     * @param MailService              $mailService
     * @throws FileSystemException
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        OrderRepositoryInterface $orderRepository,
        InvoiceDocumentFactory $invoiceDocumentFactory,
        PaymentAdapterInterface $paymentAdapter,
        InvoiceRepository $invoiceRepository,
        InvoiceOrderValidator $invoiceOrderValidator,
        NotifierInterface $notifierInterface,
        LoggerFactory $loggerFactory,
        DirectoryList $directoryList,
        SerializerInterface $serializer,
        ItemCreationFactory $itemCreationFactory,
        MailService $mailService
    ) {
        $this->resourceConnection     = $resourceConnection;
        $this->orderRepository        = $orderRepository;
        $this->invoiceDocumentFactory = $invoiceDocumentFactory;
        $this->paymentAdapter         = $paymentAdapter;
        $this->invoiceRepository      = $invoiceRepository;
        $this->invoiceOrderValidator  = $invoiceOrderValidator;
        $this->notifierInterface      = $notifierInterface;
        $this->serializer             = $serializer;
        $this->itemCreationFactory    = $itemCreationFactory;
        $this->mailService            = $mailService;

        $this->synoliaLogger = $loggerFactory->create(
            LoggerFactory::FILE_HANDLER,
            ['filePath' => $directoryList->getPath('log').'/invoice_error.log']
        );
    }

    /**
     * @param int                                    $orderId
     * @param bool                                   $capture
     * @param array                                  $items
     * @param bool                                   $notify
     * @param bool                                   $appendComment
     * @param InvoiceCommentCreationInterface|null   $comment
     * @param InvoiceCreationArgumentsInterface|null $arguments
     * @return int|null
     * @throws CouldNotInvoiceException
     * @throws DocumentValidationException
     */
    public function invoice(
        $orderId,
        $capture = false,
        array $items = [],
        $notify = false,
        $appendComment = false,
        InvoiceCommentCreationInterface $comment = null,
        InvoiceCreationArgumentsInterface $arguments = null
    ) {
        $connection    = $this->resourceConnection->getConnection('sales');
        $order         = $this->orderRepository->get($orderId);
        $invoice       = $this->invoiceDocumentFactory->create(
            $order,
            $items,
            $comment,
            ($appendComment && $notify),
            $arguments
        );
        $errorMessages = $this->invoiceOrderValidator->validate(
            $order,
            $invoice,
            $capture,
            $items,
            $notify,
            $appendComment,
            $comment,
            $arguments
        );

        if ($errorMessages->hasMessages()) {
            throw new DocumentValidationException(
                __("Invoice Document Validation Error(s):\n".implode("\n", $errorMessages->getMessages()))
            );
        }
        $connection->beginTransaction();

        try {
            $order = $this->paymentAdapter->pay($order, $invoice, $capture);
            $invoice->setState(Invoice::STATE_PAID);
            $this->invoiceRepository->save($invoice);
            $order->setStatus(Order::STATE_INVOICED);
            $order->setState(Order::STATE_INVOICED);
            $order->setItemsToInvoice(null);
            $this->orderRepository->save($order);
            $connection->commit();
        } catch (\Exception $e) {
            $this->synoliaLogger->addError($e->getMessage());
            $connection->rollBack();
            $order->setStatus(Order::STATE_CAPTURE_REJECTED);
            $order->setState(Order::STATE_CAPTURE_REJECTED);
            $itemsToInvoice = $this->serializer->serialize($this->itemsObjectToArray($items));
            $order->setItemsToInvoice($itemsToInvoice);
            $entityInError = [
                'ID : '.$order->getIncrementId().' => '.$e->getMessage()
            ];
            $this->mailService->sendEmail($entityInError, 'order', 'invoice');
            $order->addStatusHistoryComment(
                __('An error occurred when the WMS send invoice information : %1', $e->getMessage())
            )
                ->setIsCustomerNotified(false);
            $this->orderRepository->save($order);
            throw new CouldNotInvoiceException(
                __('Could not save an invoice, see error log for details')
            );
        }
        if ($notify) {
            $comment = $appendComment ? $comment : null;
            $this->notifierInterface->notify($order, $invoice, $comment);
        }
        return $invoice->getEntityId();
    }

    /**
     * @param array $items
     * @return array
     */
    private function itemsObjectToArray(array $items)
    {
        $itemsArray = [];
        foreach ($items as $item) {
            $itemsArray[] = [
                'order_item_id' => $item->getOrderItemId(),
                'qty' => $item->getQty()
            ];
        }

        return $itemsArray;
    }

    /**
     * @param array $items
     * @return array
     */
    public function arrayToItemsObject(array $items)
    {
        $itemsObject = [];

        foreach ($items as $item) {
            $itemCreation = $this->itemCreationFactory->create();
            $itemCreation->setOrderItemId($item['order_item_id']);
            $itemCreation->setQty($item['qty']);
            $itemsObject[] = $itemCreation;
        }

        return $itemsObject;
    }
}
