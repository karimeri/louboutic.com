<?php

namespace Project\Sales\Model\Order\Creditmemo\Total;

use Magento\Sales\Model\Order\Total\AbstractTotal;
use Magento\Sales\Model\Order\Creditmemo;

/**
 * Class Adjustment
 * @package Project\Sales\Model\Order\Creditmemo\Total
 * @author Synolia <contact@synolia.com>
 */
class Adjustment extends AbstractTotal
{
    /**
     * @param Creditmemo $creditmemo
     * @return $this
     */
    public function collect(Creditmemo $creditmemo)
    {
        $grandTotal     = $creditmemo->getGrandTotal();
        $baseGrandTotal = $creditmemo->getBaseGrandTotal();
        $baseTaxAmount = $creditmemo->getBaseTaxAmount();
        $taxAmount = $creditmemo->getTaxAmount();

        $baseAjustments = $creditmemo->getBaseAdjustmentShippingFees()
            - $creditmemo->getBaseAdjustmentRepairingFees()
            + $creditmemo->getBaseAdjustmentCreditNote()
            - $creditmemo->getBaseAdjustmentReturnCharge();

        $ajustments = $creditmemo->getAdjustmentShippingFees()
            - $creditmemo->getAdjustmentRepairingFees()
            + $creditmemo->getAdjustmentCreditNote()
            - $creditmemo->getAdjustmentReturnCharge();

        $taxRate = $creditmemo->getAdjustmentTaxRate();
        $ttc = $creditmemo->getTtc();
        if ($ttc) {
            $baseAjustmentsTaxAmount = round(($baseAjustments / (100 + $taxRate)) * ($taxRate), 2);
            $ajustmentsTaxAmount = round(($ajustments / (100 + $taxRate)) * ($taxRate), 2);

        } else {
            $baseAjustmentsTaxAmount = 0;
            $ajustmentsTaxAmount = 0;
        }


        $baseGrandTotal += $baseAjustments;
        $baseTaxAmount += $baseAjustmentsTaxAmount;
        $grandTotal += $ajustments;
        $taxAmount += $ajustmentsTaxAmount;

        $creditmemo->setBaseGrandTotal($baseGrandTotal);
        $creditmemo->setGrandTotal($grandTotal);
        $creditmemo->setBaseTaxAmount($baseTaxAmount);
        $creditmemo->setTaxAmount($taxAmount);

        $creditmemo->setBaseAdjustment($creditmemo->getBaseAdjustment() + $baseAjustments);
        $creditmemo->setAdjustment($creditmemo->getAdjustment() + $ajustments);


        return $this;
    }
}
