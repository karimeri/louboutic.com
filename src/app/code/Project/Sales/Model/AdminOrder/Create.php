<?php

namespace Project\Sales\Model\AdminOrder;

/**
 * Class Create
 *
 * @package Project\Sales\Model\AdminOrder
 * @author Synolia <contact@synolia.com>
 */
class Create extends \Magento\Sales\Model\AdminOrder\Create
{
    /**
     * override of parent, added [$this->getSession()->getStore()->getId()] to getForCustomer
     * @return \Magento\Quote\Model\Quote
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCustomerCart()
    {
        if (!is_null($this->_cart)) {
            return $this->_cart;
        }

        $this->_cart = $this->quoteFactory->create();

        $customerId = (int)$this->getSession()->getCustomerId();
        if ($customerId) {
            try {
                $this->_cart = $this->quoteRepository->getForCustomer($customerId, [$this->getSession()->getStore()->getId()]);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->_cart->setStore($this->getSession()->getStore());
                $customerData = $this->customerRepository->getById($customerId);
                $this->_cart->assignCustomer($customerData);
                $this->quoteRepository->save($this->_cart);
            }
        }

        return $this->_cart;
    }
}
