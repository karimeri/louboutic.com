<?php
namespace Project\Sales\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class DemoMode
 * @package Project\Sales\Model\Config\Source
 * @author  Synolia <contact@synolia.com>
 */
class DemoMode implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $recurringTypes = [
            '1' => 'Test Mode',
            '0' => 'Production Mode'
        ];

        $options = [];
        foreach ($recurringTypes as $code => $label) {
            $options[] = ['value' => $code, 'label' => $label];
        }

        return $options;
    }
}
