<?php
namespace Project\Sales\Model\Plugin\Quote;

use Magento\Sales\Model\AdminOrder\Create;

/**
 * Class QuoteToOrder
 * @package Project\Sales\Model\Plugin\Quote
 */
class QuoteToOrder
{
    /**
     * @param Create $subject
     * @param $result
     * @return mixed
     */
    public function afterCreateOrder(Create $subject, $result)
    {
        $sourceCode = $subject->getData('source_code');
        $result->setSourceCode($sourceCode);
        $result->save();
        return $result;
    }
}
