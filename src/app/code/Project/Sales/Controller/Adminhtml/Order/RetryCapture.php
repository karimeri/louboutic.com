<?php

namespace Project\Sales\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Translate\InlineInterface;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Project\Sales\Model\Invoice\Service;
use Project\Sales\Model\Magento\Sales\Order;
use Psr\Log\LoggerInterface;

/**
 * Class RetryCapture
 * @package Project\Sales\Controller\Adminhtml\Order
 * @author  Synolia <contact@synolia.com>
 */
class RetryCapture extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @var Service
     */
    protected $invoiceService;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * RetryCapture constructor.
     * @param Context                  $context
     * @param Registry                 $coreRegistry
     * @param FileFactory              $fileFactory
     * @param InlineInterface          $translateInline
     * @param PageFactory              $resultPageFactory
     * @param JsonFactory              $resultJsonFactory
     * @param LayoutFactory            $resultLayoutFactory
     * @param RawFactory               $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface          $logger
     * @param Service                  $invoiceService
     * @param SerializerInterface      $serializer
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        InlineInterface $translateInline,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        LayoutFactory $resultLayoutFactory,
        RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        Service $invoiceService,
        SerializerInterface $serializer
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );

        $this->invoiceService = $invoiceService;
        $this->serializer     = $serializer;
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var Order $order */
        $order = $this->_initOrder();
        if ($order) {
            try {
                $itemsToInvoice = $this->serializer->unserialize($order->getItemsToInvoice());
                $this->invoiceService->invoice(
                    $order->getId(),
                    true,
                    $this->invoiceService->arrayToItemsObject($itemsToInvoice)
                );
                $this->messageManager->addSuccessMessage(__('Order capture successful'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Error during retry capture. See log for more informations'));
                $this->logger->critical($e);
            }
            return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
        }
        return $resultRedirect->setPath('sales/*/');
    }
}
