<?php

namespace Project\Sales\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Registry;
use Magento\Framework\Translate\InlineInterface;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Project\Adyen\Helper\Service\AuthorizeService;
use Project\Wms\Model\Api\Connector;
use Psr\Log\LoggerInterface;

/**
 * Class RetryFullAuthorization
 * @package Project\Sales\Controller\Adminhtml\Order
 * @author  Synolia <contact@synolia.com>
 */
class RetryFullAuthorization extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @var AuthorizeService
     */
    protected $authorizeService;

    /**
     * @var Connector
     */
    protected $wmsConnector;

    /**
     * RetryFullAuthorization constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Sales\Api\OrderManagementInterface $orderManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Project\Adyen\Helper\Service\AuthorizeService $authorizeService
     * @param \Project\Wms\Model\Api\Connector $wmsConnector
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        InlineInterface $translateInline,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        LayoutFactory $resultLayoutFactory,
        RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        AuthorizeService $authorizeService,
        Connector $wmsConnector
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );

        $this->authorizeService = $authorizeService;
        $this->wmsConnector = $wmsConnector;
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->_initOrder();
        if ($order) {
            try {
                $this->authorizeService->fullAuthorize($order, true);

                $isPreorder = false;
                foreach ($order->getAllVisibleItems() as $item) {
                    if ($item->getIsPreorder()) {
                        $isPreorder = true;
                        break;
                    }
                }

                if (!$isPreorder) {
                    $websiteId = $order->getStore()->getWebsite()->getWebsiteId();
                    $wmsId = $order->getWmsId();
                    $request = [
                        'state' => 'SENTTOLOGISTIC'
                    ];

                    $wmsResponse = $this->wmsConnector->request(
                        '/api/web_orders/'.$wmsId.'/state',
                        $websiteId,
                        $request,
                        'PUT'
                    );

                    if (!$wmsResponse || isset($wmsResponse->error)) {
                        $this->messageManager->addErrorMessage(
                            __('Error during send informations to wms. See log for more informations')
                        );
                        $this->logger->critical($wmsResponse->error);
                    } else {
                        $this->messageManager->addSuccessMessage(__('Order full authorization successful'));
                    }
                }
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(
                    __('Error during retry full authorization. See log for more informations')
                );
                $this->logger->critical($e);
            }
            return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
        }
        return $resultRedirect->setPath('sales/*/');
    }
}
