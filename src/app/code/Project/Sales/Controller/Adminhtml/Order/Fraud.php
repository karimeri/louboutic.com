<?php

namespace Project\Sales\Controller\Adminhtml\Order;

/**
 * Class Fraud
 * @package Project\Sales\Controller\Adminhtml\Order
 * @author Synolia <contact@synolia.com>
 */
class Fraud extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $order = $this->_initOrder();
        if (!$order) {
            return $resultRedirect->setPath('sales/*/');
        }
        try {
            if ($order->getIsFraud()) {
                $order->setIsFraud(false);
                $this->messageManager->addSuccessMessage(__('The order is unmarked as fraud'));
            } else {
                $order->setIsFraud(true);
                $this->messageManager->addSuccessMessage(__('The order is marked as fraud'));
            }
            $this->orderRepository->save($order);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error during operation. See log for more informations'));
            $this->logger->critical($e);
        }
        return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
    }
}
