<?php

namespace Project\Sales\Controller\Adminhtml\Order;

use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class WaitingForCustomer
 * @package Project\Sales\Controller\Adminhtml\Order
 */
class WaitingForCustomer extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $order = $this->_initOrder();
        if (!$order) {
            return $resultRedirect->setPath('sales/*/');
        }
        try {
            $order->setStatus(Order::STATE_WAITING_FOR_CUSTOMER);
            $this->orderRepository->save($order);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error during operation. See log for more informations'));
            $this->logger->critical($e);
        }
        return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
    }
}
