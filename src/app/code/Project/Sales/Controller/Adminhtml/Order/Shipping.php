<?php

namespace Project\Sales\Controller\Adminhtml\Order;

use Magento\Sales\Model\Order;
use Project\Wms\Model\Api\Connector;

/**
 * Class Shipping
 * @package Project\Sales\Controller\Adminhtml\Order
 * @author Synolia <contact@synolia.com>
 */
class Shipping extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @var \Project\Wms\Model\Api\Connector
     */
    protected $wmsConnector;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Sales\Api\OrderManagementInterface $orderManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Psr\Log\LoggerInterface $logger,
        Connector $connector
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );

        $this->wmsConnector = $connector;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $order = $this->_initOrder();

        if (!$order) {
            return $resultRedirect->setPath('sales/*/');
        }

        try {
            $shippingValue = $this->getRequest()->getParam('shipping_method');
            $shippingMethod = substr(
                $shippingValue,
                strpos($shippingValue, '|') + 1
            );
            $shippingDescription = strtok($shippingValue, '|');

            $shippingAssignments = $order->getExtensionAttributes()->getShippingAssignments();
            if (!empty($shippingAssignments)) {
                $shippingAssignment = array_shift($shippingAssignments);
                $shippingAssignment->getShipping()->setMethod($shippingMethod);

                $order->getExtensionAttributes()->setShippingAssignments([$shippingAssignment]);
            }

            $order->setShippingDescription($shippingDescription);
            $this->orderRepository->save($order);

            $this->sendToWms($order, $shippingMethod);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error during operation. See log for more informations'));
            $this->logger->critical($e);
        }

        return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
    }

    /**
     * @param Order $order
     * @param string $shippingMethod
     *
     * @throws \Exception
     */
    protected function sendToWms($order, $shippingMethod)
    {
        $websiteId = $order->getStore()->getWebsite()->getWebsiteId();
        $wmsId = $order->getWmsId();
        $request = ['shippingMethod' => $shippingMethod];

        $wmsResponse = $this->wmsConnector->request(
            sprintf('/api/web_orders/%s/shipping', $wmsId),
            $websiteId,
            $request,
            'PUT'
        );

        if (!$wmsResponse || isset($wmsResponse->error)) {
            $this->messageManager->addErrorMessage(
                __('Error during send informations to wms. See log for more informations')
            );
            $this->logger->critical($wmsResponse->error);
        } else {
            $this->messageManager->addSuccessMessage(__('Shipping method updated'));
        }
    }
}
