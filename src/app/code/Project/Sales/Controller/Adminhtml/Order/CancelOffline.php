<?php

namespace Project\Sales\Controller\Adminhtml\Order;

/**
 * Class CancelOffline
 * @package Project\Sales\Controller\Adminhtml\Order
 * @author  Synolia <contact@synolia.com>
 */
class CancelOffline extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::cancel';

    /**
     * Cancel order offline. Without cancel in cybersource
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $order = $this->_initOrder();
        if (!$order) {
            return $resultRedirect->setPath('sales/*/');
        }
        try {
            $order->cancelOffline();
            $this->orderRepository->save($order);
            $this->messageManager->addSuccessMessage(__('You canceled the order offline.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error during cancellation. See log for more informations'));
            $this->logger->critical($e);
        }
        return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
    }
}
