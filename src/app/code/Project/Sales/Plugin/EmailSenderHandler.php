<?php

namespace Project\Sales\Plugin;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\AreaList;
use \Magento\Framework\App\State;
use \Magento\Framework\App\Area;

/**
 * Class EmailSenderHandler
 * @package Project\Sales\Plugin
 * @author Marwen JELLOUL
 */
class EmailSenderHandler
{
    /**
     * @var ScopeConfigInterface
     */
    private $globalConfig;

    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @var State
     */
    private $appState;

    /**
     * EmailSenderHandler constructor.
     * @param ScopeConfigInterface $globalConfig
     * @param AreaList $areaList
     * @param State $appState
     */
    public function __construct(
        ScopeConfigInterface $globalConfig,
        AreaList $areaList,
        State $appState
    ){
        $this->globalConfig = $globalConfig;
        $this->areaList = $areaList;
        $this->appState = $appState;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeSendEmails()
    {
        //Initialisation la traduction dans le cas ou on active l'envoi des emails async par cron
        if ($this->globalConfig->getValue('sales_email/general/async_sending')) {
            $area = $this->areaList->getArea($this->appState->getAreaCode());
            $area->load(Area::PART_TRANSLATE);
        }
    }
}
