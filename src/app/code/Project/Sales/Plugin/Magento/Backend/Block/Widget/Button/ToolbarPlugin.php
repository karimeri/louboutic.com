<?php

namespace Project\Sales\Plugin\Magento\Backend\Block\Widget\Button;

use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Backend\Block\Widget\Button\ButtonList;
use Magento\Sales\Block\Adminhtml\Order\View;
use Magento\Framework\UrlInterface;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class ToolbarPlugin
 * @package Project\Sales\Plugin\Magento\Backend\Block\Widget\Button
 * @author  Synolia <contact@synolia.com>
 */
class ToolbarPlugin
{
    const ORDER_CANCELABLE_STATES = [
        Order::STATE_WAITING_ADYEN,
        Order::STATE_PAYMENT_REVIEW,
        Order::STATE_SENT_TO_LOGISTIC,
        Order::STATE_WAITING_FOR_CUSTOMER,
        Order::STATE_SUSPECTED_FRAUD,
        Order::STATE_PAYMENT_AUTHORIZED_PENDING,
        Order::STATE_PAYMENT_AUTHORIZED_PROCESSING,
        Order::STATE_PICKING,
        Order::STATE_PACKING,
        Order::STATE_LABEL_GENERATION_ERROR,
        Order::STATE_CREDITMEMO_REFUNDED,
    ];


    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * ToolbarPlugin constructor.
     * @param UrlInterface $urlBuilder
     */
    public function __construct(UrlInterface $urlBuilder)
    {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @param ToolbarContext $toolbar
     * @param AbstractBlock  $context
     * @param ButtonList     $buttonList
     * @return array
     */
    public function beforePushButtons(
        ToolbarContext $toolbar,
        AbstractBlock $context,
        ButtonList $buttonList
    ) {
        if (!$context instanceof View) {
            return [$context, $buttonList];
        }

        $order = $context->getOrder();

        if ($order->getState() == Order::STATE_AUTORIZATION_ERROR) {
            $buttonList->add(
                'retry_full_authorization',
                [
                    'label' => __('Retry Full Authorization'),
                    'onclick' => 'setLocation(\''.$context->getUrl('project_sales/*/retryFullAuthorization').'\')'
                ]
            );
        }

        if (in_array($order->getStatus(), self::ORDER_CANCELABLE_STATES)) {
            $buttonList->remove('order_cancel');
            $buttonList->remove('void_payment');
            $buttonList->add(
                'order_cancel',
                [
                    'label' => __('Cancel'),
                    'class' => 'cancel',
                    'id' => 'order-view-cancel-button',
                    'data_attribute' => [
                        'url' => $context->getUrl('sales/*/cancel')
                    ]
                ]
            );
        }else {
            $buttonList->remove('order_cancel');
            $buttonList->remove('void_payment');
        }

        if ($order->getIsFraud()) {
            $labelFraudButton = __('Unmark as fraud');
        } else {
            $labelFraudButton = __('Mark as fraud');
        }

        $buttonList->add(
            'order_mark_as_fraud',
            [
                'label' => $labelFraudButton,
                'onclick' => 'setLocation(\'' . $context->getUrl('project_sales/*/fraud') . '\')',
            ]
        );

        if ($order->getStatus() == Order::STATE_PAYMENT_REVIEW) {
            $buttonList->add(
                'order_waiting_for_customer',
                [
                    'label' => __('Waiting for customer'),
                    'onclick' => 'setLocation(\'' . $context->getUrl('project_sales/*/waitingForCustomer') . '\')',
                ]
            );
        }
    }
}
