<?php
namespace Project\Sales\Plugin\Magento\Sales\Block\Adminhtml\Items;

use Magento\Framework\DataObject;
use Magento\Sales\Block\Adminhtml\Items\AbstractItems;
use Magento\Sales\Model\ResourceModel\Order\Tax\Item as TaxItem;

/**
 * Class AbstractItemsPlugin
 *
 * @package Project\Sales\Plugin\Magento\Sales\Block\Adminhtml\Items
 * @author Synolia <contact@synolia.com>
 */
class AbstractItemsPlugin
{
    /**
     * @var TaxItem
     */
    protected $taxItem;

    public function __construct(
        TaxItem $taxItem
    ) {
        $this->taxItem = $taxItem;
    }

    /**
     * @param \Magento\Sales\Block\Adminhtml\Items\AbstractItems $subject
     * @param \Magento\Framework\DataObject $item
     *
     * @return array
     */
    public function beforeDisplayTaxPercent(AbstractItems $subject, DataObject $item)
    {
        $item->setTaxPercent($this->getItemTaxRate($item));

        return [$item];
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     *
     * @return int
     */
    protected function getItemTaxRate($item)
    {
        $taxRate = 0;

        foreach ($this->taxItem->getTaxItemsByOrderId($item->getOrderId()) as $tax) {
            if ($tax['item_id'] === (string)$item->getOrderItemId() || $tax['item_id'] === (string)$item->getItemId()) {
                $taxRate += $tax['tax_percent'];
            }
        }

        return $taxRate;
    }
}
