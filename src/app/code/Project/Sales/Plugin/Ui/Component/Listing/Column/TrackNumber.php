<?php

namespace Project\Sales\Plugin\Ui\Component\Listing\Column;

use Magento\GiftRegistry\Model\ResourceModel\Helper as ResourceHelper;

/**
 * Class TrackNumber
 * @package Project\Sales\Plugin\Ui\Component\Listing\Column
 */
class TrackNumber
{
    const TABLE_SALES_SHIPMENT_GRID = 'sales_shipment_grid';
    const TABLE_SALES_SHIPMENT_TRACK = 'sales_shipment_track';

    /**
     * @var ResourceHelper
     */
    protected $resourceHelper;

    public function __construct(
        ResourceHelper $resourceHelper
    ) {
        $this->resourceHelper = $resourceHelper;
    }

    public function afterGetReport($subject, $collection, $requestName)
    {
        if ($requestName == 'sales_order_shipment_grid_data_source' || $requestName == 'sales_order_view_shipment_grid_data_source') {
            $collection->addFilterToMap('created_at', 'main_table.created_at');
            $collection->addFilterToMap('order_id', 'main_table.order_id');

            if ($collection->getMainTable() === $collection->getConnection()->getTableName(self::TABLE_SALES_SHIPMENT_GRID)) {
                $salesShipmentTrackTable = $collection->getConnection()->getTableName(self::TABLE_SALES_SHIPMENT_TRACK);
                $collection
                    ->getSelect()
                    ->joinLeft(
                        ['sst' => $salesShipmentTrackTable],
                        "sst.parent_id = main_table.entity_id",
                        [
                        ]
                    )
                    ->group('main_table.entity_id');

                $this->resourceHelper->addGroupConcatColumn(
                    $collection->getSelect(),
                    'track_number',
                    'sst.track_number',
                    ', ',
                    ' '
                );

                return $collection;
            }
        }

        return $collection;
    }
}