<?php

namespace Project\Sales\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\OrderRepository;

/**
 * Class SaveCustomerSource
 *
 * @package Project\Sales\Observer
 * @author Synolia <contact@synolia.com>
 */
class SaveCustomerSource implements ObserverInterface
{
    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * SaveCustomerSource constructor.
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     */
    public function __construct(
        OrderRepository $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $event = $observer->getEvent();
        $orderId = $event->getOrderIds()[0];
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderRepository->get($orderId);
        $order->setSourceCode('ORI003');
        $this->orderRepository->save($order);

        return $this;
    }
}
