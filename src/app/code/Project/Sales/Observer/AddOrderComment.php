<?php

namespace Project\Sales\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderRepository;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class AddOrderComment
 * @package Project\Sales\Observer
 * @author Synolia <contact@synolia.com>
 */
class AddOrderComment implements ObserverInterface
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * AddOrderComment constructor.
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     */
    public function __construct(
        OrderRepository $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(Observer $observer)
    {
        $address = $observer->getAddress();
        $oldValues  = $address->getOrigData();

        if (is_array($oldValues)) {
            $newValues  = $address->getData();
            $valuesDiff = array_diff($oldValues, $newValues);
            foreach ($valuesDiff as $field => $value) {
                $comment[] = $field.' value was before '.$value.' change in '.$newValues[$field];
            }

            if (isset($comment) && is_array($comment)) {
                $order = $address->getOrder();
                $status = ($order->getStatus() == Order::STATE_NEED_TO_RESHIP)?Order::STATE_READY_TO_RESHIP:null;
                $order->addStatusHistoryComment(
                    implode(', ', $comment),
                    $status
                )
                    ->setIsCustomerNotified(false);
                $this->orderRepository->save($order);
            }
        }
    }
}
