<?php

namespace Project\Sales\Observer;

use Magento\CustomerCustomAttributes\Model\Sales\OrderFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Model\OrderRepository;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class SalesOrderAfterSave
 * @package Project\Sales\Observer
 * @author Synolia <contact@synolia.com>
 */
class SalesOrderAfterSave implements ObserverInterface
{
    /**
     * @var \Magento\CustomerCustomAttributes\Model\Sales\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @param \Magento\CustomerCustomAttributes\Model\Sales\OrderFactory $orderFactory
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(
        OrderFactory $orderFactory,
        OrderRepository $orderRepository,
        State $state,
        QuoteRepository $quoteRepository,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->orderFactory = $orderFactory;
        $this->orderRepository = $orderRepository;
        $this->state = $state;
        $this->quoteRepository = $quoteRepository;
        $this->request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $save = false;

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        if (!$order->getTransactionId() && $order->getPayment()->getCcTransId()) {
            $order->setTransactionId($order->getPayment()->getCcTransId());

            $save = true;
        }

        if ($this->state->getAreaCode() === Area::AREA_ADMINHTML) {
            try {
                $quote = $this->quoteRepository->get($order->getQuoteId());
            } catch (\Exception $e) {
                $quote = null;
            }
            if (!is_null($quote) && !empty($quote->getOrderId())) {
                /** @var \Project\Sales\Model\Magento\Sales\Order $order */
                $originalOrder = $this->orderRepository->get($quote->getOrderId());
                $originalOrder->setStatus(Order::STATE_CREDITMEMO_EXCHANGED);

                $this->orderRepository->save($originalOrder);

                if (!$order->getRmaId() && !empty($quote->getRmaId())) {
                    $order->setRmaId($quote->getRmaId());
                    $save = true;
                }
            }
        }

        if (!$order->getSourceCode() && isset($this->request->getParam('order')['source_code'])) {
            $order->setSourceCode($this->request->getParam('order')['source_code']);
            $save = true;
        }

        //Adyen Payment  WorkFlow for review
        if ($order->getStatus() == Order::STATE_PAYMENT_REVIEW && $order->getState() == Order::STATE_NEW) {
            $order->setState(Order::STATE_PAYMENT_REVIEW);
            $save = true;
        }

        if ($save) {
            $order->save();
        }
        return $this;
    }
}
