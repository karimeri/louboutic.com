<?php

namespace Project\Sales\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderRepository;
use Project\Sales\Model\Magento\Sales\Order;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Project\Sales\Model\Magento\Sales\OrderRepository as ProjectOrderRepository;

/**
 * Class GenerateCreditMemo
 * @package Project\Sales\Observer
 * @author  Synolia <contact@synolia.com>
 */
class GenerateCreditMemo implements ObserverInterface
{
    /**
     * @var CreditmemoFactory
     */
    protected $creditmemoFactory;

    /**
     * @var CreditmemoManagementInterface
     */
    protected $creditmemoManagement;

    /**
     * @var ProjectOrderRepository
     */
    protected $projectOrderRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * GenerateCreditMemo constructor.
     * @param CreditmemoFactory             $creditmemoFactory
     * @param CreditmemoManagementInterface $creditmemoManagement
     * @param ProjectOrderRepository        $projectOrderRepository
     * @param OrderRepository               $orderRepository
     */
    public function __construct(
        CreditmemoFactory $creditmemoFactory,
        CreditmemoManagementInterface $creditmemoManagement,
        ProjectOrderRepository $projectOrderRepository,
        OrderRepository $orderRepository
    ) {
        $this->creditmemoFactory      = $creditmemoFactory;
        $this->creditmemoManagement   = $creditmemoManagement;
        $this->projectOrderRepository = $projectOrderRepository;
        $this->orderRepository        = $orderRepository;
    }

    /**
     * @param Observer $observer
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order * */
        $order = $observer->getOrder();

        $lastStateBeforeCancel = $this->projectOrderRepository->getLastStateBeforeCancel($order->getId());

        if ($lastStateBeforeCancel == Order::STATE_LABEL_GENERATION_ERROR) {
            $this->createCreditMemo($order);
            $order->setState(Order::STATE_CANCELED);
            $order->setStatus(Order::STATE_CANCELED);
            $this->orderRepository->save($order);
        }
    }

    /**
     * @param Order $order
     * @throws LocalizedException
     */
    protected function createCreditMemo(Order $order)
    {
        $itemsToCancel = [];
        foreach ($order->getAllItems() as $item) {
            if ($item->getQtyCanceled() > 0) {
                $itemsToCancel[$item->getId()] = $item->getQtyCanceled();
            }
        }

        $arrayCreditMemo = [
            'qtys' => $itemsToCancel,
            'shipping_amount' => 0
        ];

        $creditmemo = $this->creditmemoFactory->createByOrder($order, $arrayCreditMemo);

        $this->creditmemoManagement->refund($creditmemo, true);
    }
}
