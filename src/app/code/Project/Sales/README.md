
**`Champs BO :`**

- **Shipping fees** (+) : Champ texte représentant les frais remboursés au client (Ex : le colis est arrivé après le délai de livraison et demande un geste commercial).

- **Repairing fees** (-) : Champ texte représentant un montant de réparation facturé au client (Ex : Le client retourne un produit dont la semelle est endommagée. Les frais de réparation seront facturés au client).

- **Credit note** (+) : Champ texte représentant un geste commercial

- Charge return to customer : Champ « Oui/Non ». Un montant est associé à cette option. Si l'option est à « Oui », ce montant est facturé.

L'affichage de ces champs en Bo est configurable dans la section Christian Louboutin => Sales.
Ces champs s'affichent sur le formulaire de création d'un credit memo accessible depuis une commande ou d'une facture.

**Attention !!** Ces champs ne sont pas affichés sur le récapitulatif du credit memo ni sur le PDF