<?php

namespace Project\Sales\Block\Adminhtml\Items\Column;

use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;

/**
 * Class Name
 * @package Project\Sales\Block\Adminhtml\Items\Column
 */
class Name extends \Magento\Sales\Block\Adminhtml\Items\Column\Name
{
    /** @var \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory */
    private $attributeFactory;

    /**
     * Name constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\Product\OptionFactory $optionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory,
        array $data = []
    ) {
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $optionFactory, $data);
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * @param int $optionId
     * @return string
     */
    public function getOptionLabel($optionId)
    {
        $attributeModel = $this->attributeFactory->create()->load($optionId);
        return $attributeModel->getFrontendLabel();
    }
}
