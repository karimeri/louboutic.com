<?php

namespace Project\Sales\Block\Adminhtml\Items\Renderer;

use Magento\Sales\Model\Order\Item;

/**
 * Class DefaultRenderer
 *
 * @package Project\Sales\Block\Adminhtml\Items\Renderer
 * @author Synolia <contact@synolia.com>
 */
class DefaultRenderer extends \Magento\Sales\Block\Adminhtml\Items\Renderer\DefaultRenderer
{
    /**
     * @param \Magento\Sales\Model\Order\Creditmemo\Item $item
     *
     * @return int
     */
    public function getQty($item)
    {
        $request = $this->getRequest();
        $qtyUpdate = $request->getParam('creditmemo') && isset(
            $request->getParam('creditmemo')['items'][$item->getOrderItemId()]['qty']
        );

        if ($qtyUpdate || $request->getParam('rma_id')) {
            return $item->getQty();
        }

        return 0;
    }
}
