<?php

namespace Project\Sales\Block\Adminhtml\Order\Create\Search;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Backend\Model\Session\Quote;
use Magento\Catalog\Model\Config;
use Magento\Catalog\Model\ProductFactory;
use Magento\Sales\Model\Config as SalesConfig;
use Project\PreOrder\Model\StockRegistry as PreOrderStockRegistry;
use Magento\CatalogInventory\Model\StockRegistry;

/**
 * Class Grid
 * @package Project\Sales\Block\Adminhtml\Order\Create\Search
 * @author Synolia <contact@synolia.com>
 */
class Grid extends \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid
{
    /**
     * @var \Project\PreOrder\Model\StockRegistry
     */
    protected $preOrderStockRegistry;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistry
     */
    protected $stockRegistry;

    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Sales\Model\Config $salesConfig
     * @param \Project\Sales\Block\Adminhtml\Order\Create\Search\PreOrderStockRegistry $preOrderStockRegistry
     * @param \Magento\CatalogInventory\Model\StockRegistry $stockRegistry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        ProductFactory $productFactory,
        Config $catalogConfig,
        Quote $sessionQuote,
        SalesConfig $salesConfig,
        PreOrderStockRegistry $preOrderStockRegistry,
        StockRegistry $stockRegistry,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $backendHelper,
            $productFactory,
            $catalogConfig,
            $sessionQuote,
            $salesConfig,
            $data
        );

        $this->preOrderStockRegistry = $preOrderStockRegistry;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     * @return $this|\Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid
     */
    protected function _prepareCollection()
    {
        $attributes = $this->_catalogConfig->getProductAttributes();
        /* @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productFactory->create()->getCollection();
        $collection->setStore(
            $this->getStore()
        )->addAttributeToSelect(
            $attributes
        )->addAttributeToSelect(
            'sku'
        )->addStoreFilter()->addAttributeToFilter(
            'type_id',
            $this->_salesConfig->getAvailableProductTypes()
        )->addAttributeToSelect(
            'gift_message_available'
        );

        $websiteId = $this->_sessionQuote->getStore()->getWebsiteId();
        $preOrderStock = $this->preOrderStockRegistry->getStock($websiteId);
        $stock = $this->stockRegistry->getStock($websiteId);

        //Synolia Rewrite begin
        $collection->getSelect()->join(
            ['stock_preorder' => 'cataloginventory_stock_item'],
            'entity_id = stock_preorder.product_id AND stock_preorder.stock_id = '.$preOrderStock->getStockId(),
            ['stock_preorder_qty' => 'qty']
        );

        $collection->getSelect()->join(
            ['stock' => 'cataloginventory_stock_item'],
            'entity_id = stock.product_id AND stock.stock_id = '.$stock->getStockId(),
            ['stock_qty' => 'qty']
        );

        $collection->getSelect()->join(
            ['r' => 'catalog_product_relation'],
            'r.child_id = entity_id',
            ['parent_id']
        );
        $collection->getSelect()->join(
            ['configurable' => 'catalog_product_entity'],
            'r.parent_id = configurable.entity_id',
            ['parent_sku' => 'configurable.sku']
        );

        $collection->addAttributeToSelect('color_product_page')
            ->addAttributeToSelect('size');
        //Synolia Rewrite end

        $this->setCollection($collection);

        return Extended::_prepareCollection();
    }

    /**
     * added columns (color, parent_sku, size, stock, preorder_stock) and reordered them
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
     * @return \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'index' => 'entity_id'
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Product Name'),
                'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Product::class,
                'index' => 'name'
            ]
        );

        $this->addColumn(
            'color',
            [
                'header' => __('Color'),
                'index' => 'color_product_page',
                'filter' => false,
                'sortable' => false,
            ]
        );

        $this->addColumn(
            'parent_sku',
            [
                'header' => __('Reference'),
                'index' => 'parent_sku',
                'filter' => false,
                'sortable' => false,
            ]
        );

        $this->addColumn('sku', ['header' => __('SKU'), 'index' => 'sku']);

        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'column_css_class' => 'price',
                'type' => 'currency',
                'currency_code' => $this->getStore()->getCurrentCurrencyCode(),
                'rate' => $this->getStore()->getBaseCurrency()->getRate($this->getStore()->getCurrentCurrencyCode()),
                'index' => 'price',
                'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Price::class
            ]
        );

        $this->addColumn(
            'size',
            [
                'header' => __('Size'),
                'index' => 'size',
                'filter' => false,
                'sortable' => false,
                'renderer' => \Project\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Size::class,
            ]
        );

        $this->addColumn(
            'qty',
            [
                'filter' => false,
                'sortable' => false,
                'header' => __('Quantity'),
                'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Qty::class,
                'name' => 'qty',
                'inline_css' => 'qty',
                'type' => 'input',
                'validate_class' => 'validate-number',
                'index' => 'qty'
            ]
        );

        $this->addColumn(
            'stock',
            [
                'header' => __('Stock'),
                'index' => 'stock_qty',
                'filter' => false,
                'sortable' => false
            ]
        );

        $this->addColumn(
            'preorder_stock',
            [
                'header' => __('Preorder stock'),
                'index' => 'stock_preorder_qty',
                'filter' => false,
                'sortable' => false
            ]
        );

        $this->addColumn(
            'in_products',
            [
                'header' => __('Select'),
                'type' => 'checkbox',
                'name' => 'in_products',
                'values' => $this->_getSelectedProducts(),
                'index' => 'entity_id',
                'sortable' => false,
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        return Extended::_prepareColumns();
    }
}
