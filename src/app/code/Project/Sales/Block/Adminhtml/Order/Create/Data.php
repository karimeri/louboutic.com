<?php

namespace Project\Sales\Block\Adminhtml\Order\Create;

/**
 * Class Data
 *
 * @package Project\Sales\Block\Adminhtml\Order\Create
 * @author Synolia <contact@synolia.com>
 */
class Data extends \Magento\Sales\Block\Adminhtml\Order\Create\Data
{
    /**
     * override of parent, replaced $this->_storeManager->getStore() by $this->getStore()
     * @return string[]
     */
    public function getAvailableCurrencies()
    {
        $dirtyCodes = $this->getStore()->getAvailableCurrencyCodes();
        $codes = [];
        if (is_array($dirtyCodes) && count($dirtyCodes)) {
            $rates = $this->_currencyFactory->create()->getCurrencyRates(
                $this->getStore()->getBaseCurrency(),
                $dirtyCodes
            );
            foreach ($dirtyCodes as $code) {
                if (isset($rates[$code]) || $code == $this->getStore()->getBaseCurrencyCode()) {
                    $codes[] = $code;
                }
            }
        }
        return $codes;
    }
}
