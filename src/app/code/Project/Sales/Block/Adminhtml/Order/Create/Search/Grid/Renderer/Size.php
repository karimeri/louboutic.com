<?php

namespace Project\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer;

use Magento\Catalog\Model\ProductFactory;

/**
 * Class Size
 * @package Project\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer
 * @author Synolia <contact@synolia.com>
 */
class Size extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * Qty constructor.
     *
     * @param \Magento\Backend\Block\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        ProductFactory $productFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productFactory = $productFactory;
    }

    /**
     * Render product qty field
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $optionText = '';

        if ($row->getSize()) {
            $product = $this->productFactory->create();
            $exists = $product->getResource()->getAttribute('size');


            if ($exists && $exists->usesSource()) {
                $optionText = $exists->getSource()->getOptionText($row->getSize());
            }
        }

        return $optionText;
    }
}
