<?php

namespace Project\Sales\Block\Adminhtml\Order\Creditmemo\Create;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\DataObject;
use Magento\Framework\Registry;
use Project\Sales\Helper\Config;

/**
 * Class Totals
 * @package Project\Sales\Block\Adminhtml\Sales\Order\Creditmemo
 * @author Synolia <contact@synolia.com>
 */
class Totals extends Template
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Creditmemo
     * @var Creditmemo|null
     */
    protected $creditmemo;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * Totals constructor.
     * @param Context $context
     * @param Config $config
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config   = $config;
        $this->registry = $registry;
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $total = new DataObject(
            [
                'code'       => 'ajustments',
                'block_name' => $this->getNameInLayout()
            ]
        );
        $this->getParentBlock()->addTotalBefore($total, 'tax');
        return $this;
    }

    /**
     * Retrieve creditmemo model instance
     *
     * @return Creditmemo
     */
    public function getCreditmemo()
    {
        if ($this->creditmemo === null) {
            if ($this->registry->registry('current_creditmemo')) {
                $this->creditmemo = $this->registry->registry('current_creditmemo');
            } elseif ($this->getParentBlock() && $this->getParentBlock()->getCreditmemo()) {
                $this->creditmemo = $this->getParentBlock()->getCreditmemo();
            }
        }
        return $this->creditmemo;
    }

    /**
     * @return array
     */
    public function getAjustments()
    {
        return $this->config->getAdjustments();
    }

    /**
     * @param $code
     * @return string
     */
    public function isInputSelect($code)
    {
        return $code == Config::FIELD_AJUSTMENT_RETURN_CHARGE;
    }

    /**
     * @return int|mixed
     */
    public function getReturnCharge()
    {
        return $this->config->getReturnCharge();
    }

    /**
     * Get update url
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->getUrl(
            'sales/*/updateQty',
            [
                'order_id' => $this->getCreditmemo()->getOrderId(),
                'invoice_id' => $this->getRequest()->getParam('invoice_id', null)
            ]
        );
    }
}

