<?php

namespace Project\Sales\Block\Adminhtml\Order;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class ChangeShippingOrder
 * @package Project\Sales\Block\Adminhtml\Order
 * @author Synolia <contact@synolia.com>
 */
class ChangeShippingOrder extends AbstractOrder
{
    /**
     * @var \Magento\Shipping\Model\Config
     */
    protected $shippingConfig;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var @var \Magento\Quote\Model\Quote
     */
    protected $quote = false;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Magento\Shipping\Model\Config $shippingConfig
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Magento\Shipping\Model\Config $shippingConfig,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        EnvironmentManager $environmentManager,
        QuoteRepository $quoteRepository,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);

        $this->shippingConfig = $shippingConfig;
        $this->scopeConfig = $scopeConfig;
        $this->environmentManager = $environmentManager;
        $this->quoteRepository = $quoteRepository;
    }

    public function canChangeShipping()
    {
        return $this->getQuote() != false;
    }

    public function getQuote()
    {
        if (!$this->quote) {
            try {
                $quoteId = $this->getOrder()->getQuoteId();
                $this->quote = $this->quoteRepository->get($quoteId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }

        return $this->quote;
    }

    /**
     * @return array
     */
    public function getShippingMethods()
    {
        $activeCarriers = $this->getQuote()->getShippingAddress()->getGroupedAllShippingRates();
        $methods = [];

        foreach ($activeCarriers as $carrierModel) {
            foreach ($carrierModel as $method) {
                $label = trim(sprintf('%s - %s', $method->getCarrierTitle(), $method->getMethodTitle()));
                $methods[] = [
                    'value' => sprintf('%s|%s', $label, $method->getCode()),
                    'label' => $label
                ];
            }
        }

        return $methods;
    }
}
