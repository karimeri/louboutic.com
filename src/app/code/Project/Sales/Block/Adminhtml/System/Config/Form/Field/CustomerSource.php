<?php
namespace Project\Sales\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class CustomerSource
 * @package Project\Sales\Block\Adminhtml\System\Config\Form\Field
 * @author  Synolia <contact@synolia.com>
 */
class CustomerSource extends AbstractFieldArray
{
    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * // phpcs:disable
     */
    protected function _construct()
    {
        // phpcs:enable
        $this->addColumn(
            'code',
            [
                'label' => __('Code'),
            ]
        );
        $this->addColumn(
            'label',
            [
                'label' => __('Label')
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');

        parent::_construct();
    }
}
