<?php

namespace Project\Sales\Block\Email;

use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order\Shipment\Track;
use Magento\Shipping\Helper\Data as ShippingHelper;
use Magento\Shipping\Model\CarrierFactory;

/**
 * Class Trackings
 * @package Project\Sales\Block\Email
 * @author Synolia <contact@synolia.com>
 */
class Trackings extends Template
{
    /**
     * @var \Magento\Shipping\Helper\Data
     */
    private $shippingHelper;

    /**
     * Shipping carrier factory
     *
     * @var \Magento\Shipping\Model\CarrierFactory
     */
    protected $carrierFactory;

    /**
     * Trackings constructor.
     * @param Template\Context $context
     * @param \Magento\Shipping\Helper\Data $shippingHelper
     * @param \Magento\Shipping\Model\CarrierFactory $carrierFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ShippingHelper $shippingHelper,
        CarrierFactory $carrierFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->shippingHelper = $shippingHelper;
        $this->carrierFactory = $carrierFactory;
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment\Track $track
     * @return string
     */
    public function getUrlByTrack(Track $track)
    {
        if ($track->getEntityId()) {
            return $this->shippingHelper->getTrackingPopupUrlBySalesModel($track);
        }

        return "#";
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment\Track $track
     * @return string
     */
    public function getTrackingUrl(Track $track)
    {
        $url = '#';
        if ($track->getEntityId())
        {
            if ($track->getCarrierCode() == 'ups') {
                $url = "http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&error_carried=true" .
                    "&tracknums_displayed=5&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1={$track->getTrackNumber()}" .
                    "&AgreeToTermsAndConditions=yes";
            } else {
                /** @var \Owebia\AdvancedShipping\Model\Carrier $carrierInstance */
                $carrierInstance = $this->carrierFactory->create($track->getCarrierCode());
                /** @var \Magento\Shipping\Model\Tracking\Result\Status $trackingInfo */
                if($carrierInstance){
                    $trackingInfo = $carrierInstance->getTrackingInfo($track->getTrackNumber());
                    $url =  $trackingInfo->getData('url');
                }
            }
        }
        return $url;
    }
}
