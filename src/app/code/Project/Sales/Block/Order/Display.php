<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\Sales\Block\Order;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 *
 * @api
 * @since 100.0.2
 */
class Display extends \Magento\Framework\View\Element\Template
{

    const XML_PATH_TO_PHONE = 'general/store_information/phone';
    const XML_PATH_TO_EMAIL_CUSTOMER_SERVICE = 'general/store_information/customer_service_email';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;



    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    public function getMessage() {
        $current_url = $this->storeManager->getStore()->getCurrentUrl();
        if($current_url) {
            $spiles = $this->_getExplode($current_url);
            if (\is_array($spiles) && (\in_array('history', $spiles) || \in_array('orderArchive', $spiles))) {

                return __('For all orders placed prior to the 13th of October 2020, we invite you to contact the Customer Service team to organize the return of your order at %1 or %2 and apologise for any inconvenience this may cause.',$this->_getPhone(),$this->_getMailTo());
            }
        }
        return false;
    }
   protected function  _getExplode($string){
          return explode('/',$string);
   }

   protected function _getPhone($scopeCode = null){
       return $this->_scopeConfig->getValue(self::XML_PATH_TO_PHONE, ScopeInterface::SCOPE_STORE, $scopeCode);
   }

    protected function _getMailTo($scopeCode = null){
        $email = $this->_scopeConfig->getValue(self::XML_PATH_TO_EMAIL_CUSTOMER_SERVICE, ScopeInterface::SCOPE_STORE, $scopeCode);
        return "<a href='mailto: $email'> $email</a>";
    }
}
