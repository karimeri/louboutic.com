<?php

namespace Project\Sales\Block\Order\Creditmemo;

class Totals extends \Magento\Sales\Block\Order\Creditmemo\Totals
{
    /**
     * Verify if is a free shipping
     * @return bool
     */
    public function isFreeShipping($code){
        if ($code == "shipping" && $this->_totals['shipping']['value'] == 0){
            return true;
        } else {
            return false;
        }
    }
}