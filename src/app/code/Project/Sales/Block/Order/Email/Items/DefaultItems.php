<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\Sales\Block\Order\Email\Items;

/**
 * Sales Order Email items default renderer
 *
 * @api
 * @author     Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class DefaultItems extends \Magento\Sales\Block\Order\Email\Items\DefaultItems
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    /**
     * @var \Project\Core\Helper\Product
     */
    protected $_productHelper;

    /**
     * DefaultItems constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Project\Core\Helper\Product $productHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Project\Core\Helper\Product $productHelper,
        array $data = []
    )
    {
        $this->_productRepository = $productRepository;
        $this->_productHelper = $productHelper;
        parent::__construct($context, $data);
    }

    /**
     * @param $id
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id)
    {
        try {
            $product = $this->_productRepository->getById($id);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noEntityException) {
            $product = null;
        }
        return $product;
    }

    /**
     * Retrieve current size of item
     * @return string
     */
    public function getSize($product)
    {
        return $this->_productHelper->getProductSize($product, $this->getItemOptions());
    }

    /**
     * Retrieve current order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->getItem()->getOrder();
    }
}
