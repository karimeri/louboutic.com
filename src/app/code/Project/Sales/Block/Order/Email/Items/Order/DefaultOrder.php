<?php

namespace Project\Sales\Block\Order\Email\Items\Order;

use Magento\Backend\Block\Template\Context;
use Project\Core\Helper\Product;

class DefaultOrder extends \Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder
{
    /**
     * @var Product
     */
    protected $_productHelper;

    /**
     * DefaultOrder constructor.
     * @param Context $context
     * @param Product $productHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Product $productHelper,
        array $data = []
    )
    {
        $this->_productHelper = $productHelper;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve current size of item
     * @return string
     */
    public function getSize($product)
    {
        return $this->_productHelper->getProductSize($product, $this->getItemOptions());
    }
}
