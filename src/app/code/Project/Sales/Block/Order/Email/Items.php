<?php

namespace Project\Sales\Block\Order\Email;

use Magento\Framework\View\Element\Template\Context;
use Project\Core\Helper\AttributeSet;
use Project\Core\Helper\Product;

class Items extends \Magento\Sales\Block\Order\Email\Items
{
    /**
     * @var Product
     */
    protected $_productHelper;
    /**
     * @var AttributeSet
     */
    protected $_attributeSetHelper;

    /**
     * Items constructor.
     * @param Context $context
     * @param Product $productHelper
     * @param AttributeSet $attributeSetHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Product $productHelper,
        AttributeSet $attributeSetHelper,
        array $data = []
    ) {
        $this->_productHelper = $productHelper;
        $this->_attributeSetHelper = $attributeSetHelper;
        parent::__construct($context, $data);
    }

    /**
     * @param $items
     * @return \Magento\Framework\Phrase
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSizeLabel($items){
        $orderCount = 0;
        $isBeautyCount = 0;
        $isShoesCount = 0;
        $sizeLabel =__('Size');
        foreach ($items as $item) {
            if ($item->getParentItem()) continue;
            $orderCount++;
            $product = $this->_productHelper->getProductById($item->getData('product_id'));
            if($product) {
                if ($this->_attributeSetHelper->isShoes($product))
                    $isShoesCount++;
                elseif ($this->_attributeSetHelper->isBeauty($product))
                    $isBeautyCount++;
            }
        }
        if ($orderCount == $isShoesCount)
            $sizeLabel =__('size');
        elseif ($orderCount == $isBeautyCount)
            $sizeLabel =__('Volume');

        return $sizeLabel;
    }
}