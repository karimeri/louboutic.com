<?php

namespace Project\Sales\Block\Magento\Sales\Adminhtml\Order\Creditmemo;

use Magento\Sales\Model\Order\Creditmemo;

/**
 * Class Totals
 * removed the function adding adjustments
 *
 * @package Project\Sales\Magento\Sales\Adminhtml\Order\Creditmemo
 * @author Synolia <contact@synolia.com>
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Totals
{
    /**
     * Creditmemo
     *
     * @var Creditmemo|null
     */
    protected $_creditmemo;

    /**
     * Retrieve creditmemo model instance
     *
     * @return Creditmemo
     */
    public function getCreditmemo()
    {
        if ($this->_creditmemo === null) {
            if ($this->hasData('creditmemo')) {
                $this->_creditmemo = $this->_getData('creditmemo');
            } elseif ($this->_coreRegistry->registry('current_creditmemo')) {
                $this->_creditmemo = $this->_coreRegistry->registry('current_creditmemo');
            } elseif ($this->getParentBlock() && $this->getParentBlock()->getCreditmemo()) {
                $this->_creditmemo = $this->getParentBlock()->getCreditmemo();
            }
        }
        return $this->_creditmemo;
    }

    /**
     * Get source
     *
     * @return Creditmemo|null
     */
    public function getSource()
    {
        return $this->getCreditmemo();
    }

    /**
     * @return $this
     */
    protected function _initTotals()
    {
        parent::_initTotals();

        if (isset($this->_totals['shipping'])) {
            unset($this->_totals['shipping']);
        }

        return $this;
    }
}
