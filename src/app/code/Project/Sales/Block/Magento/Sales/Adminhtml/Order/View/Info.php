<?php

namespace Project\Sales\Block\Magento\Sales\Adminhtml\Order\View;

use Magento\Framework\Exception\LocalizedException;
use Project\Sales\Model\Magento\Sales\Order;

/**
 * Class Info
 * @package Project\Sales\Block\Magento\Sales\Adminhtml\Order\View
 * @author  Synolia <contact@synolia.com>
 */
class Info extends \Magento\Sales\Block\Adminhtml\Order\View\Info
{
    const ORDER_EDITABLE_STATES = [
        Order::STATE_PENDING_PAYMENT,
        Order::STATUS_FRAUD,
        Order::STATE_HOLDED,
        Order::STATE_PROCESSING,
        Order::STATE_PAYMENT_REVIEW,
        Order::STATE_SENT_TO_LOGISTIC,
        Order::STATE_PICKING,
        Order::STATE_PACKING,
        Order::STATE_SHIPPING,
        Order::STATE_LABEL_GENERATION_ERROR,
        Order::STATE_NEED_TO_RESHIP,
        Order::STATE_READY_TO_RESHIP,
        Order::STATE_DOCUMENTS_READY_TO_BE_PRINTED,
        Order::STATE_WAITING_FOR_CUSTOMER
    ];

    /**
     * Use in app\design\adminhtml\Synolia\backend\Magento_Sales\templates\order\view\info.phtml
     * In order to display or not the edit button of the billing or shipping address
     * @return bool
     */
    public function isOrderEditable()
    {
        try {
            $order = $this->getOrder();

            return in_array($order->getStatus(), self::ORDER_EDITABLE_STATES);
        } catch (LocalizedException $e) {
            return false;
        }
    }
}
