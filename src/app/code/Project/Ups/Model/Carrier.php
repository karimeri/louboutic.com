<?php

namespace Project\Ups\Model;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Directory\Helper\Data as DirectoryData;
use Magento\Directory\Model\CountryFactory;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\HTTP\AsyncClientInterface;
use Magento\Framework\Locale\FormatInterface;
use Magento\Shipping\Model\Rate\Result\ProxyDeferredFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Ups\Helper\Config;
use Magento\Ups\Model\Carrier as CarrierBase;
use Magento\Framework\Xml\Security;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory as RateErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory as RateMethodFactory;
use Magento\Sales\Model\Order\Shipment;
use Magento\Shipping\Model\Rate\ResultFactory as RateResultFactory;
use Magento\Shipping\Model\Simplexml\ElementFactory as XmlElementFactory;
use Magento\Shipping\Model\Tracking\Result\ErrorFactory as TrackingErrorFactory;
use Magento\Shipping\Model\Tracking\Result\StatusFactory as TrackingStatusFactory;
use Magento\Shipping\Model\Tracking\ResultFactory as TrackingResultFactory;
use Project\Rma\Helper\Config as RmaConfigHelper;
use Project\Rma\Model\ReturnAddressRepository;
use Project\Wms\Helper\Config as WmsConfig;
use Psr\Log\LoggerInterface;
use Magento\Framework\HTTP\ClientFactory;

/**
 * Class Carrier
 * @package Project\Ups\Model
 */
class Carrier extends CarrierBase
{
    /**
     * @var ReturnAddressRepository
     */
    protected $returnAddressRepository;

    /**
     * @var WmsConfig
     */
    protected $wmsConfig;

    /**
     * @var RmaConfigHelper
     */
    protected $rmaConfigHelper;

    /**
     * Carrier constructor.
     * @param ScopeConfigInterface    $scopeConfig
     * @param RateErrorFactory        $rateErrorFactory
     * @param LoggerInterface         $logger
     * @param Security                $xmlSecurity
     * @param XmlElementFactory       $xmlElFactory
     * @param RateResultFactory       $rateFactory
     * @param RateMethodFactory       $rateMethodFactory
     * @param TrackingResultFactory   $trackFactory
     * @param TrackingErrorFactory    $trackErrorFactory
     * @param TrackingStatusFactory   $trackStatusFactory
     * @param RegionFactory           $regionFactory
     * @param CountryFactory          $countryFactory
     * @param CurrencyFactory         $currencyFactory
     * @param DirectoryData           $directoryData
     * @param StockRegistryInterface  $stockRegistry
     * @param FormatInterface         $localeFormat
     * @param Config                  $configHelper
     * @param ReturnAddressRepository $returnAddressRepository
     * @param WmsConfig               $wmsConfig
     * @param RmaConfigHelper         $rmaConfigHelper
     * @param ClientFactory           $clientFactory
     * @param array                   $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        RateErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        Security $xmlSecurity,
        XmlElementFactory $xmlElFactory,
        RateResultFactory $rateFactory,
        RateMethodFactory $rateMethodFactory,
        TrackingResultFactory $trackFactory,
        TrackingErrorFactory $trackErrorFactory,
        TrackingStatusFactory $trackStatusFactory,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        CurrencyFactory $currencyFactory,
        DirectoryData $directoryData,
        StockRegistryInterface $stockRegistry,
        FormatInterface $localeFormat,
        Config $configHelper,
        ReturnAddressRepository $returnAddressRepository,
        WmsConfig $wmsConfig,
        RmaConfigHelper $rmaConfigHelper,
        ClientFactory $clientFactory,
        array $data = [],
        ?AsyncClientInterface $asyncHttpClient = null,
        ?ProxyDeferredFactory $proxyDeferredFactory
    ) {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $localeFormat,
            $configHelper,
            $clientFactory,
            $data,
            $asyncHttpClient,
            $proxyDeferredFactory
        );
        $this->returnAddressRepository = $returnAddressRepository;
        $this->wmsConfig = $wmsConfig;
        $this->rmaConfigHelper = $rmaConfigHelper;
    }

    /**
     * @override
     * @param RateRequest $request
     * @return $this|CarrierBase
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * phpcs:disable Generic.Metrics.CyclomaticComplexity
     */
    public function setRequest(RateRequest $request)
    {
        $this->setStore($request->getStoreId());

        $this->_request = $request;

        $rowRequest = new DataObject();

        if ($request->getLimitMethod()) {
            $rowRequest->setAction($this->configHelper->getCode('action', 'single'));
            $rowRequest->setProduct($request->getLimitMethod());
        } else {
            $rowRequest->setAction($this->configHelper->getCode('action', 'all'));
            $rowRequest->setProduct('GND' . $this->getConfigData('dest_type'));
        }

        if ($request->getUpsPickup()) {
            $pickup = $request->getUpsPickup();
        } else {
            $pickup = $this->getConfigData('pickup');
        }
        $rowRequest->setPickup($this->configHelper->getCode('pickup', $pickup));

        if ($request->getUpsContainer()) {
            $container = $request->getUpsContainer();
        } else {
            $container = $this->getConfigData('container');
        }
        $rowRequest->setContainer($this->configHelper->getCode('container', $container));

        if ($request->getUpsDestType()) {
            $destType = $request->getUpsDestType();
        } else {
            $destType = $this->getConfigData('dest_type');
        }
        $rowRequest->setDestType($this->configHelper->getCode('dest_type', $destType));


        try {
            /** @var \Project\Rma\Model\ReturnAddress $returnAddress */
            $returnAddress = $this->returnAddressRepository->getByEstablishmentCode(
                $this->wmsConfig->getEstablishmentCode($request->getWebsiteId())
            );

            $origCountryId = $this->getDefaultAddressValue(
                $request->getOrigCountryId(),
                $returnAddress->getCountryId()
            );
            $origState = $this->getDefaultAddressValue($request->getOrigState(), $returnAddress->getRegion());
            $origCity = $this->getDefaultAddressValue($request->getOrigCity(), $returnAddress->getCity());
            $origPostcode = $this->getDefaultAddressValue(
                $request->getOrigPostcode(),
                $returnAddress->getPostcode()
            );
        } catch (\Exception $exception) {
            $origCountryId = $this->getDefaultValue(
                $request->getOrigCountry(),
                Shipment::XML_PATH_STORE_COUNTRY_ID
            );
            $origState = $this->getDefaultValue($request->getOrigRegionCode(), Shipment::XML_PATH_STORE_REGION_ID);
            $origCity = $this->getDefaultValue($request->getOrigCity(), Shipment::XML_PATH_STORE_CITY);
            $origPostcode = $this->getDefaultValue($request->getOrigPostcode(), Shipment::XML_PATH_STORE_ZIP);
        }

        if (\is_numeric($origState)) {
            $origState = $this->_regionFactory->create()->load($origState)->getCode();
        }

        $rowRequest->setOrigRegionCode($origState);
        $rowRequest->setOrigCountry($this->_countryFactory->create()->load($origCountryId)->getData('iso2_code'));
        $rowRequest->setOrigPostal($origPostcode);
        $rowRequest->setOrigCity($origCity);

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }

        //for UPS, puero rico state for US will assume as puerto rico country
        if ($destCountry == self::USA_COUNTRY_ID && ($request->getDestPostcode() == '00912' ||
                $request->getDestRegionCode() == self::PUERTORICO_COUNTRY_ID)
        ) {
            $destCountry = self::PUERTORICO_COUNTRY_ID;
        }

        // For UPS, Guam state of the USA will be represented by Guam country
        if ($destCountry == self::USA_COUNTRY_ID && $request->getDestRegionCode() == self::GUAM_REGION_CODE) {
            $destCountry = self::GUAM_COUNTRY_ID;
        }

        $country = $this->_countryFactory->create()->load($destCountry);
        $rowRequest->setDestCountry($country->getData('iso2_code') ?: $destCountry);

        $rowRequest->setDestRegionCode($request->getDestRegionCode());

        if ($request->getDestPostcode()) {
            $rowRequest->setDestPostal($request->getDestPostcode());
        }

        $weight = $this->getTotalNumOfBoxes($request->getPackageWeight());
        $weight = $this->_getCorrectWeight($weight);

        $rowRequest->setWeight($weight);
        if ($request->getFreeMethodWeight() != $request->getPackageWeight()) {
            $rowRequest->setFreeMethodWeight($request->getFreeMethodWeight());
        }

        $rowRequest->setValue($request->getPackageValue());
        $rowRequest->setValueWithDiscount($request->getPackageValueWithDiscount());

        if ($request->getUpsUnitMeasure()) {
            $unit = $request->getUpsUnitMeasure();
        } else {
            $unit = $this->getConfigData('unit_of_measure');
        }
        $rowRequest->setUnitMeasure($unit);
        $rowRequest->setIsReturn($request->getIsReturn());
        $rowRequest->setBaseSubtotalInclTax($request->getBaseSubtotalInclTax());

        $this->_rawRequest = $rowRequest;

        return $this;
    }

    /**
     * Returns value of given variable
     *
     * @param string|int $origValue
     * @param string $pathToValue
     * @return string|int|null
     */
    protected function getDefaultValue($origValue, $pathToValue)
    {
        if (!$origValue) {
            $origValue = $this->_scopeConfig->getValue(
                $pathToValue,
                ScopeInterface::SCOPE_STORE,
                $this->getStore()
            );
        }

        return $origValue;
    }

    /**
     * @param $origValue
     * @param $returnAddressValue
     * @return mixed
     */
    protected function getDefaultAddressValue($origValue, $returnAddressValue)
    {
        if (!$origValue) {
            return $returnAddressValue;
        }

        return $origValue;
    }

    /**
     * Form XML for shipment request
     *
     * @param \Magento\Framework\DataObject $request
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _formShipmentRequest(\Magento\Framework\DataObject $request)
    {
        $packageParams = $request->getPackageParams();
        $height = $packageParams->getHeight();
        $width = $packageParams->getWidth();
        $length = $packageParams->getLength();
        $weightUnits = $packageParams->getWeightUnits() == \Zend_Measure_Weight::POUND ? 'LBS' : 'KGS';
        $dimensionsUnits = $packageParams->getDimensionUnits() == \Zend_Measure_Length::INCH ? 'IN' : 'CM';

        $itemsDesc = [];
        $itemsShipment = $request->getPackageItems();
        foreach ($itemsShipment as $itemShipment) {
            $item = new \Magento\Framework\DataObject();
            $item->setData($itemShipment);
            $itemsDesc[] = $item->getName();
        }

        $xmlRequest = $this->_xmlElFactory->create(
            ['data' => '<?xml version = "1.0" ?><ShipmentConfirmRequest xml:lang="en-US"/>']
        );
        $requestPart = $xmlRequest->addChild('Request');
        $requestPart->addChild('RequestAction', 'ShipConfirm');
        $requestPart->addChild('RequestOption', 'nonvalidate');

        $shipmentPart = $xmlRequest->addChild('Shipment');
        if ($request->getIsReturn()) {
            $returnPart = $shipmentPart->addChild('ReturnService');
            // UPS Print Return Label
            $returnPart->addChild('Code', '9');
        }
        $shipmentPart->addChild('Description', substr(implode(' ', $itemsDesc), 0, 35));
        //empirical

        $shipperPart = $shipmentPart->addChild('Shipper');
        if ($request->getIsReturn()) {
            $shipperPart->addChild('Name', $this->rmaConfigHelper->getRmaUpsShipperCompany());
            $shipperPart->addChild('AttentionName', $this->rmaConfigHelper->getRmaUpsShipperContactName());
            $shipperPart->addChild('ShipperNumber', $this->getConfigData('shipper_number'));
            $shipperPart->addChild('PhoneNumber', $this->rmaConfigHelper->getRmaUpsShipperContactPhone());

            $addressPart = $shipperPart->addChild('Address');
            $addressPart->addChild('AddressLine1', $this->rmaConfigHelper->getRmaUpsShipperStreet1());
            $addressPart->addChild('AddressLine2', $this->rmaConfigHelper->getRmaUpsShipperStreet2());
            $addressPart->addChild('City', $this->rmaConfigHelper->getRmaUpsShipperCity());
            $addressPart->addChild('CountryCode', $this->rmaConfigHelper->getRmaUpsShipperCountry());
            $addressPart->addChild('PostalCode', $this->rmaConfigHelper->getRmaUpsShipperPostcode());
            if ($this->rmaConfigHelper->getRmaUpsShipperRegion()) {
                $addressPart->addChild('StateProvinceCode', $this->rmaConfigHelper->getRmaUpsShipperRegion());
            }
        } else {
            $shipperPart->addChild('Name', $request->getShipperContactCompanyName());
            $shipperPart->addChild('AttentionName', $request->getShipperContactPersonName());
            $shipperPart->addChild('ShipperNumber', $this->getConfigData('shipper_number'));
            $shipperPart->addChild('PhoneNumber', $request->getShipperContactPhoneNumber());

            $addressPart = $shipperPart->addChild('Address');
            $addressPart->addChild('AddressLine1', $request->getShipperAddressStreet());
            $addressPart->addChild('AddressLine2', $request->getShipperAddressStreet2());
            $addressPart->addChild('City', $request->getShipperAddressCity());
            $addressPart->addChild('CountryCode', $request->getShipperAddressCountryCode());
            $addressPart->addChild('PostalCode', $request->getShipperAddressPostalCode());
            if ($request->getShipperAddressStateOrProvinceCode()) {
                $addressPart->addChild('StateProvinceCode', $request->getShipperAddressStateOrProvinceCode());
            }
        }

        $shipToPart = $shipmentPart->addChild('ShipTo');
        $shipToPart->addChild('AttentionName', $request->getRecipientContactPersonName());
        $shipToPart->addChild(
            'CompanyName',
            $request->getRecipientContactCompanyName() ? $request->getRecipientContactCompanyName() : 'N/A'
        );
        $shipToPart->addChild('PhoneNumber', $request->getRecipientContactPhoneNumber());

        $addressPart = $shipToPart->addChild('Address');
        $addressPart->addChild('AddressLine1', $request->getRecipientAddressStreet1());
        $addressPart->addChild('AddressLine2', $request->getRecipientAddressStreet2());
        $addressPart->addChild('City', $request->getRecipientAddressCity());
        $addressPart->addChild('CountryCode', $request->getRecipientAddressCountryCode());
        $addressPart->addChild('PostalCode', $request->getRecipientAddressPostalCode());
        if ($request->getRecipientAddressStateOrProvinceCode()) {
            $addressPart->addChild('StateProvinceCode', $request->getRecipientAddressRegionCode());
        }
        if ($this->getConfigData('dest_type') == 'RES') {
            $addressPart->addChild('ResidentialAddress');
        }

        if ($request->getIsReturn()) {
            $shipFromPart = $shipmentPart->addChild('ShipFrom');
            $shipFromPart->addChild('AttentionName', $request->getShipperContactPersonName());
            $shipFromPart->addChild(
                'CompanyName',
                $request->getShipperContactCompanyName() ? $request
                    ->getShipperContactCompanyName() : $request
                    ->getShipperContactPersonName()
            );
            $shipFromAddress = $shipFromPart->addChild('Address');
            $shipFromAddress->addChild('AddressLine1', $request->getShipperAddressStreet1());
            $shipFromAddress->addChild('AddressLine2', $request->getShipperAddressStreet2());
            $shipFromAddress->addChild('City', $request->getShipperAddressCity());
            $shipFromAddress->addChild('CountryCode', $request->getShipperAddressCountryCode());
            $shipFromAddress->addChild('PostalCode', $request->getShipperAddressPostalCode());
            if ($request->getShipperAddressStateOrProvinceCode()) {
                $shipFromAddress->addChild('StateProvinceCode', $request->getShipperAddressStateOrProvinceCode());
            }

            $addressPart = $shipToPart->addChild('Address');
            $addressPart->addChild('AddressLine1', $request->getShipperAddressStreet1());
            $addressPart->addChild('AddressLine2', $request->getShipperAddressStreet2());
            $addressPart->addChild('City', $request->getShipperAddressCity());
            $addressPart->addChild('CountryCode', $request->getShipperAddressCountryCode());
            $addressPart->addChild('PostalCode', $request->getShipperAddressPostalCode());
            if ($request->getShipperAddressStateOrProvinceCode()) {
                $addressPart->addChild('StateProvinceCode', $request->getShipperAddressStateOrProvinceCode());
            }
            if ($this->getConfigData('dest_type') == 'RES') {
                $addressPart->addChild('ResidentialAddress');
            }
        }

        $servicePart = $shipmentPart->addChild('Service');
        $servicePart->addChild('Code', $request->getShippingMethod());
        $packagePart = $shipmentPart->addChild('Package');
        if ($request->getHasDangerousGood()) {
            $packagePart->addChild('Description', 'DANGEROUS GOODS IN EQ');
        } else {
            $packagePart->addChild('Description', substr(implode(' ', $itemsDesc), 0, 35));
        }
        //empirical
        $packagePart->addChild('PackagingType')->addChild('Code', $request->getPackagingType());
        $packageWeight = $packagePart->addChild('PackageWeight');
        $packageWeight->addChild('Weight', $request->getPackageWeight());
        $packageWeight->addChild('UnitOfMeasurement')->addChild('Code', $weightUnits);

        // set dimensions
        if ($length || $width || $height) {
            $packageDimensions = $packagePart->addChild('Dimensions');
            $packageDimensions->addChild('UnitOfMeasurement')->addChild('Code', $dimensionsUnits);
            $packageDimensions->addChild('Length', $length);
            $packageDimensions->addChild('Width', $width);
            $packageDimensions->addChild('Height', $height);
        }

        // ups support reference number only for domestic service
        if ($this->_isUSCountry(
                $request->getRecipientAddressCountryCode()
            ) && $this->_isUSCountry(
                $request->getShipperAddressCountryCode()
            )
        ) {
            if ($request->getReferenceData()) {
                $referenceData = $request->getReferenceData() . $request->getPackageId();
            } else {
                $referenceData = 'Order #' .
                    $request->getOrderShipment()->getOrder()->getIncrementId() .
                    ' P' .
                    $request->getPackageId();
            }
            $referencePart = $packagePart->addChild('ReferenceNumber');
            $referencePart->addChild('Code', '02');
            $referencePart->addChild('Value', $referenceData);
        }

        $deliveryConfirmation = $packageParams->getDeliveryConfirmation();
        if ($deliveryConfirmation) {
            /** @var $serviceOptionsNode Element */
            $serviceOptionsNode = null;
            switch ($this->_getDeliveryConfirmationLevel($request->getRecipientAddressCountryCode())) {
                case self::DELIVERY_CONFIRMATION_PACKAGE:
                    $serviceOptionsNode = $packagePart->addChild('PackageServiceOptions');
                    break;
                case self::DELIVERY_CONFIRMATION_SHIPMENT:
                    $serviceOptionsNode = $shipmentPart->addChild('ShipmentServiceOptions');
                    break;
                default:
                    break;
            }
            if (!is_null($serviceOptionsNode)) {
                $serviceOptionsNode->addChild(
                    'DeliveryConfirmation'
                )->addChild(
                    'DCISType',
                    $packageParams->getDeliveryConfirmation()
                );
            }
        }

        if ($request->getIsReturn()) {
            $itemizedPaymentInformationPart = $shipmentPart->addChild('ItemizedPaymentInformation');
            $shipmentCharge01 = $itemizedPaymentInformationPart->addChild('ShipmentCharge');
            $shipmentCharge01->addChild('Type', '01');
            $shipmentCharge01->addChild(
                'BillShipper'
            )->addChild(
                'AccountNumber',
                $this->getConfigData('shipper_number')
            );
            $shipmentCharge02 = $itemizedPaymentInformationPart->addChild('ShipmentCharge');
            $shipmentCharge02->addChild('Type', '02');
            $shipmentCharge02->addChild(
                'BillShipper'
            )->addChild(
                'AccountNumber',
                $this->getConfigData('shipper_number')
            );
        } else {
            $shipmentPart->addChild(
                'PaymentInformation'
            )->addChild(
                'Prepaid'
            )->addChild(
                'BillShipper'
            )->addChild(
                'AccountNumber',
                $this->getConfigData('shipper_number')
            );
        }

        if ($request->getPackagingType() != $this->configHelper->getCode(
                'container',
                'ULE'
            ) &&
            $request->getShipperAddressCountryCode() == self::USA_COUNTRY_ID &&
            ($request->getRecipientAddressCountryCode() == 'CA' ||
                $request->getRecipientAddressCountryCode() == 'PR')
        ) {
            $invoiceLineTotalPart = $shipmentPart->addChild('InvoiceLineTotal');
            $invoiceLineTotalPart->addChild('CurrencyCode', $request->getBaseCurrencyCode());
            $invoiceLineTotalPart->addChild('MonetaryValue', ceil($packageParams->getCustomsValue()));
        }

        // RMA ID
        if ($request->getIsReturn()) {
            $referenceNumberPart = $shipmentPart->addChild('ReferenceNumber');
            $referenceNumberPart->addChild('Code', 'RZ');
            $referenceNumberPart->addChild('Value', $request->getOrderShipment()->getRma()->getIncrementId());
        }

        $labelPart = $xmlRequest->addChild('LabelSpecification');
        $labelPart->addChild('LabelPrintMethod')->addChild('Code', 'GIF');
        $labelPart->addChild('LabelImageFormat')->addChild('Code', 'GIF');

        $this->setXMLAccessRequest();
        $xmlRequest = $this->_xmlAccessRequest . $xmlRequest->asXml();

        return $xmlRequest;
    }

    /**
     * Parse xml tracking response
     *
     * @param string $trackingValue
     * @param string $xmlResponse
     * @return null
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _parseXmlTrackingResponse($trackingValue, $xmlResponse)
    {
        $errorTitle = 'For some reason we can\'t retrieve tracking info right now.';
        $resultArr = [];
        $packageProgress = [];

        if ($xmlResponse) {
            $xml = new \Magento\Framework\Simplexml\Config();
            $xml->loadString($xmlResponse);
            $arr = $xml->getXpath("//TrackResponse/Response/ResponseStatusCode/text()");
            $success = (int)$arr[0][0];

            if ($success === 1) {
                $arr = $xml->getXpath("//TrackResponse/Shipment/Service/Description/text()");
                $resultArr['service'] = (string)$arr[0];

                $arr = $xml->getXpath("//TrackResponse/Shipment/PickupDate/text()");
                $resultArr['shippeddate'] = (string)$arr[0];

                $arr = $xml->getXpath("//TrackResponse/Shipment/Package/PackageWeight/Weight/text()");
                $weight = (string)$arr[0];

                $arr = $xml->getXpath("//TrackResponse/Shipment/Package/PackageWeight/UnitOfMeasurement/Code/text()");
                $unit = (string)$arr[0];

                $resultArr['weight'] = "{$weight} {$unit}";

                $activityTags = $xml->getXpath("//TrackResponse/Shipment/Package/Activity");
                if ($activityTags) {
                    $index = 1;
                    foreach ($activityTags as $activityTag) {
                        $addArr = [];
                        if (isset($activityTag->ActivityLocation->Address->City)) {
                            $addArr[] = (string)$activityTag->ActivityLocation->Address->City;
                        }
                        if (isset($activityTag->ActivityLocation->Address->StateProvinceCode)) {
                            $addArr[] = (string)$activityTag->ActivityLocation->Address->StateProvinceCode;
                        }
                        if (isset($activityTag->ActivityLocation->Address->CountryCode)) {
                            $addArr[] = (string)$activityTag->ActivityLocation->Address->CountryCode;
                        }
                        $dateArr = [];
                        $date = (string)$activityTag->Date;
                        //YYYYMMDD
                        $dateArr[] = substr($date, 0, 4);
                        $dateArr[] = substr($date, 4, 2);
                        $dateArr[] = substr($date, -2, 2);

                        $timeArr = [];
                        $time = (string)$activityTag->Time;
                        //HHMMSS
                        $timeArr[] = substr($time, 0, 2);
                        $timeArr[] = substr($time, 2, 2);
                        $timeArr[] = substr($time, -2, 2);

                        if ($index === 1) {
                            $resultArr['status'] = (string)$activityTag->Status->StatusType->Description;
                            $resultArr['deliverydate'] = implode('-', $dateArr);
                            //YYYY-MM-DD
                            $resultArr['deliverytime'] = implode(':', $timeArr);
                            //HH:MM:SS
                            $resultArr['deliverylocation'] = (string)$activityTag->ActivityLocation->Description;
                            $resultArr['signedby'] = (string)$activityTag->ActivityLocation->SignedForByName;
                            if ($addArr) {
                                $resultArr['deliveryto'] = implode(', ', $addArr);
                            }

                            // Louboutin custom dev
                            $tempArr = [];
                            $tempArr['activity'] = (string)$activityTag->Status->StatusType->Description;
                            $tempArr['deliverydate'] = implode('-', $dateArr);
                            //YYYY-MM-DD
                            $tempArr['deliverytime'] = implode(':', $timeArr);
                            //HH:MM:SS
                            if ($addArr) {
                                $tempArr['deliverylocation'] = implode(', ', $addArr);
                            }
                            $packageProgress[] = $tempArr;
                        } else {
                            $tempArr = [];
                            $tempArr['activity'] = (string)$activityTag->Status->StatusType->Description;
                            $tempArr['deliverydate'] = implode('-', $dateArr);
                            //YYYY-MM-DD
                            $tempArr['deliverytime'] = implode(':', $timeArr);
                            //HH:MM:SS
                            if ($addArr) {
                                $tempArr['deliverylocation'] = implode(', ', $addArr);
                            }
                            $packageProgress[] = $tempArr;
                        }
                        $index++;
                    }
                    $resultArr['progressdetail'] = $packageProgress;
                }
            } else {
                $arr = $xml->getXpath("//TrackResponse/Response/Error/ErrorDescription/text()");
                $errorTitle = (string)$arr[0][0];
            }
        }

        if (!$this->_result) {
            $this->_result = $this->_trackFactory->create();
        }

        if ($resultArr) {
            $tracking = $this->_trackStatusFactory->create();
            $tracking->setCarrier('ups');
            $tracking->setCarrierTitle($this->getConfigData('title'));
            $tracking->setTracking($trackingValue);
            $tracking->addData($resultArr);
            $this->_result->append($tracking);
        } else {
            $error = $this->_trackErrorFactory->create();
            $error->setCarrier('ups');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setTracking($trackingValue);
            $error->setErrorMessage($errorTitle);
            $this->_result->append($error);
        }

        return $this->_result;
    }
}
