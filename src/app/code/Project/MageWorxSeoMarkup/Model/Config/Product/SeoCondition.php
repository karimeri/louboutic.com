<?php

namespace Project\MageWorxSeoMarkup\Model\Config\Product;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class SeoCondition
 * @package Project\MageWorxSeoMarkup\Model\Config\Product
 */
class SeoCondition extends AbstractSource
{
    /**
     * @return array|null
     */
    public function getAllOptions()
    {
        $this->_options = [];
        $this->_options[] = ['label' => 'New', 'value' => 'NewCondition'];
        return $this->_options;
    }
}