<?php

namespace Project\MageWorxSeoMarkup\Helper;

use Magento\Store\Model\ScopeInterface;

/**
 * Class Store
 * @package Project\MageWorxSeoMarkup\Helper
 */
class Store extends \MageWorx\SeoMarkup\Helper\Data
{
    const XML_PATH_STORE_ENABLED = 'mageworx_seo/markup/store/rs_enabled';

    const XML_PATH_ADD_OPENING_HOURS = 'mageworx_seo/markup/store/add_opening_hours';

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isRsEnabled($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_STORE_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function addOpeningHours($storeId = null)
    {
        return (bool)$this->scopeConfig->getValue(
            self::XML_PATH_ADD_OPENING_HOURS,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
