<?php

namespace Project\MageWorxSeoMarkup\Helper\Json;

/**
 * Class Category
 * @package Project\MageWorxSeoMarkup\Helper\Json
 */
class Category extends \MageWorx\SeoMarkup\Helper\Json\Category
{
    const NEW_CONDITION = 'http://schema.org/NewCondition';

    const SELLER_BRAND = 'Christian Louboutin';

    /**
     * @param $category
     * @return array|bool
     */
    protected function getJsonCategoryData($category)
    {
        $productCollection = $this->getProductCollection();

        $data = [];

        if ($productCollection) {
            $data['@context']           = 'http://schema.org';
            $data['@type']              = 'ItemList';
            $data['itemListElement']    = [];

            if ($this->helperCategory->isUseOfferForCategoryProducts()) {
                $items = $productCollection->getItems();
                $items = array_values($items);
                foreach ($items as $index => $product) {
                    $data['itemListElement'][$index]['@type']       = 'ListItem';
                    $data['itemListElement'][$index]['position']    = $index + 1;
                    $data['itemListElement'][$index]['item']        = $this->getProductData($product);
                }
            }
        }

        return $data;
    }

    /**
     * @param \Magento\Framework\DataObject|\Magento\Catalog\Model\Product $product
     * @return array
     */
    protected function getProductData($product)
    {
        $this->_product = $product;
        $this->helperProductDataProvider->reset();

        $data                = [];
        $data['@type']       = 'Product';
        $data['name']        = $this->_product->getName();
        $data['description'] = $this->helperProductDataProvider->getDescriptionValue($this->_product);
        $data['image']       = $this->helperProductDataProvider->getProductImage($this->_product)->getImageUrl();
        $data['url']         = $this->urlBuilder->getCurrentUrl() . '#' . $this->_product->getUrlKey();

        $offers = $this->getOfferData();
        if (!empty($offers['price']) || !empty($offers[0]['price'])) {
            $data['offers'] = $offers;
        }

        $aggregateRatingData = $this->helperProductDataProvider->getAggregateRatingData($this->_product, false);

        if (!empty($aggregateRatingData)) {
            $aggregateRatingData['@type'] = 'AggregateRating';
            $data['aggregateRating']      = $aggregateRatingData;
        }

        /**
         * Google console error: "Either 'offers', 'review' or 'aggregateRating' should be specified"
         */
        if ($this->helperProduct->isRsEnabledForSpecificProduct() === false
            && empty($data['aggregateRating'])
            && empty($data['offers'])
        ) {
            return [];
        }

        $productIdValue = $this->helperProductDataProvider->getProductIdValue($this->_product);

        if ($productIdValue) {
            $data['productID'] = $productIdValue;
        }

        $color = $this->helperProductDataProvider->getColorValue($this->_product);
        if ($color) {
            $data['color'] = $color;
        }

        $data['brand'] = [
            '@type' => 'Brand',
            'name'  => self::SELLER_BRAND
        ];

        $manufacturer = $this->helperProductDataProvider->getManufacturerValue($this->_product);
        if ($manufacturer) {
            $data['manufacturer'] = $manufacturer;
        }

        $model = $this->helperProductDataProvider->getModelValue($this->_product);
        if ($model) {
            $data['model'] = $model;
        }

        $gtin = $this->helperProductDataProvider->getGtinData($this->_product);
        if (!empty($gtin['gtinType']) && !empty($gtin['gtinValue'])) {
            $data[$gtin['gtinType']] = $gtin['gtinValue'];
        }

        $skuValue = $this->helperProductDataProvider->getSkuValue($this->_product);
        if ($skuValue) {
            $data['sku'] = $skuValue;
        }

        $weightValue = $this->helperProductDataProvider->getWeightValue($this->_product);
        if ($weightValue) {
            $data['weight'] = $weightValue;
        }

        $categoryName = $this->helperProductDataProvider->getCategoryValue($this->_product);
        if ($categoryName) {
            $data['category'] = $categoryName;
        }

        $customProperties = $this->helperProduct->getCustomProperties();

        if ($customProperties) {
            foreach ($customProperties as $propertyName => $propertyValue) {
                if (!$propertyName || !$propertyValue) {
                    continue;
                }
                $value = $this->helperProductDataProvider->getCustomPropertyValue($product, $propertyValue);
                if ($value) {
                    $data[$propertyName] = $value;
                }
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function getOfferData()
    {
        $data = [];

        $data['@type'] = \MageWorx\SeoMarkup\Block\Head\Json\Product::OFFER;
        $data['price'] = number_format($this->getPrice(), 2, '.', '');

        $data['url']           = $this->_product->getProductUrl();
        $data['priceCurrency'] = $this->helperProductDataProvider->getCurrentCurrencyCode();

        if ($this->helperProductDataProvider->getAvailability($this->_product)
            && $this->_product->getData(\Project\MageWorxSeoMarkup\Block\Head\Json\Product::PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE)) {
            $data['availability'] = \Project\MageWorxSeoMarkup\Block\Head\Json\Product::ONLINE_ONLY;
        } elseif (!$this->helperProductDataProvider->getAvailability($this->_product)) {
            $data['availability'] = \MageWorx\SeoMarkup\Block\Head\Json\Product::OUT_OF_STOCK;
        } else {
            $data['availability'] = \MageWorx\SeoMarkup\Block\Head\Json\Product::IN_STOCK;
        }

        $priceValidUntil = $this->helperProductDataProvider->getPriceValidUntilValue($this->_product);

        if ($priceValidUntil) {
            $data['priceValidUntil'] = $priceValidUntil;
        }

        $data['itemCondition'] = self::NEW_CONDITION;

        $data['seller'] = [
            '@type' => 'Organization',
            'name'  => self::SELLER_BRAND
        ];

        return $data;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|null
     */
    protected function getProductCollection()
    {
        $collection = parent::getProductCollection();
        // load all category products because of infinite scroll
        $size = $collection->getSize();
        $collection->setPageSize($size);
        if ($collection->isLoaded()) {
            $collection->clear()
                ->load();
        }
        return $collection;
    }
}
