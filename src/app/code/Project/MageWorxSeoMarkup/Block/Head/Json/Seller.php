<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Project\MageWorxSeoMarkup\Block\Head\Json;

class Seller extends \MageWorx\SeoMarkup\Block\Head\Json\Seller
{
    /**
     *
     * @var \MageWorx\SeoMarkup\Helper\Seller
     */
    protected $helperSeller;
    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $localeResolver;

    /**
     * Seller constructor.
     * @param \MageWorx\SeoMarkup\Helper\Seller $helperSeller
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Locale\Resolver $localeResolver
     * @param array $data
     */
    public function __construct(
        \MageWorx\SeoMarkup\Helper\Seller $helperSeller,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Locale\Resolver $localeResolver,
        array $data = []
    ) {
        $this->helperSeller = $helperSeller;
        $this->localeResolver = $localeResolver;
        parent::__construct($helperSeller, $context, $data);
    }

    /**
     *
     * {@inheritDoc}
     */
    protected function getMarkupHtml()
    {
        $html = '';

        if (!$this->helperSeller->isRsEnabled()) {
            return $html;
        }

        if ($this->helperSeller->isShowForAllPages()
            || ($this->helperSeller->isShowOnlyForHomePage() && $this->isHomePage())
        ) {
            $sellerJsonData = $this->getJsonOrganizationData();
            $sellerJson     = $sellerJsonData  ? json_encode($sellerJsonData, JSON_UNESCAPED_SLASHES) : '';

            if ($sellerJsonData) {
                $html .= '<script type="application/ld+json">' . $sellerJson . '</script>';
            }
        }

        return $html;
    }

    /**
     *
     * @return array|boolean
     */
    protected function getJsonOrganizationData()
    {
        $name = $this->helperSeller->getName();
        $image = $this->getImageUrl();

        if (!$name || ! $image) { // Name and Image are required fields
            return false;
        }
        $data = [];
        $data['@context']    = 'http://schema.org';
        $data['@type']       = $this->helperSeller->getType();

        $name = $this->helperSeller->getName();
        if ($name) {
            $data['name'] = $name;
        }

        $storeCode = $this->_storeManager->getStore()->getCode();
        $data['url'] = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB) . $storeCode . '/' ;

        $socialLinks = $this->helperSeller->getSameAsLinks();
        if (is_array($socialLinks) && !empty($socialLinks)) {
            foreach ($socialLinks as $key => $socialLink){
                if(strpos($socialLink, 'wikipedia') !== false) {
                    $locale = $this->localeResolver->getLocale();
                    $lang = strstr($locale, '_', true);
                    if(!strcmp($lang, 'en')){
                        $socialLinks[$key] = str_replace('fr', 'en', $socialLink);
                    }
                }
            }
            $data['sameAs'] = [];
            $data['sameAs'] = $socialLinks;
        }

       $potentialActionData = $this->getPotentialActionData();
        if ($potentialActionData) {
            $data['potentialAction'] = $potentialActionData;
        }

        return $data;
    }

    protected function getPotentialActionData()
    {
        $storeBaseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $storeCode = $this->_storeManager->getStore()->getCode();
        $locale = $this->localeResolver->getLocale();

        $data = array();
        $data['@type']       = 'SearchAction';
        $data['target']      = $storeBaseUrl . $storeCode . '/catalogsearch/result/?q={search_term}&lang='. $locale . '#bl=searchbox-sitelinks';
        $data['query-input'] = 'required name=search_term';

        return $data;
    }
}