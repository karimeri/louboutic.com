<?php
/**
 * Copyright © 2019 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Project\MageWorxSeoMarkup\Block\Head\Json;

class Product extends \MageWorx\SeoMarkup\Block\Head\Json\Product
{
    const ONLINE_ONLY  = 'http://schema.org/OnlineOnly';
    const SELLER_BRAND_NAME = 'Christian Louboutin';
    const PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE = 'online_exclusive';

    /**
     *
     * {@inheritDoc}
     */
    protected function getMarkupHtml()
    {
        $html            = '';
        $productJsonData = '';

        if ($this->helperProduct->isRsEnabled() && $this->helperProduct->isGaEnabled()) {
            $productJsonData = [];
            $productJsonData[] = $this->getJsonProductData();
            $productJsonData[] = $this->getGoogleAssistantJsonData();

        }elseif ($this->helperProduct->isRsEnabled()) {
            $productJsonData = $this->getJsonProductData();
        }elseif ($this->helperProduct->isGaEnabled()){
            $productJsonData = $this->getGoogleAssistantJsonData();
        }

        $productJson = !empty($productJsonData) ? json_encode($productJsonData, JSON_UNESCAPED_SLASHES) : '';

        if ($productJson) {
            $html .= '<script type="application/ld+json">' . $productJson . '</script>';
        }

        return $html;
    }

    /**
     *
     * @return array
     */
    protected function getJsonProductData()
    {
        $product = $this->registry->registry('current_product');

        if (!$product) {
            return [];
        }

        $this->_product = $product;

        $data                = [];
        $data['@context']    = 'http://schema.org';
        $data['@type']       = 'Product';
        $data['name']        = $this->_product->getName();

        $images = $this->_product->getMediaGalleryImages();
        if ($images) {
            foreach ($images as $image) {
                $url = $image->getUrl();
                $data['image'][] = $url;
            }
        }

        $data['description'] = $this->helperDataProvider->getDescriptionValue($this->_product);$skuValue = $this->helperDataProvider->getSkuValue($this->_product);
        if ($skuValue) {
            $data['sku'] = $skuValue;
        }

        $data['brand'] = [
            '@type' => 'Brand',
            'name'  => self::SELLER_BRAND_NAME
        ];

        $skuValue = $this->helperDataProvider->getSkuValue($this->_product);
        if ($skuValue) {
            $data['sku'] = $skuValue;
        }

        $offers = $this->getOfferData();
        if (!empty($offers['price']) || !empty($offers[0]['price'])) {
            $data['offers'] = $offers;
        }

        $aggregateRatingData = $this->helperDataProvider->getAggregateRatingData($this->_product, false);

        if (!empty($aggregateRatingData)) {
            $aggregateRatingData['@type'] = 'AggregateRating';
            $data['aggregateRating']      = $aggregateRatingData;
        }

        /**
         * Google console error: "Either 'offers', 'review' or 'aggregateRating' should be specified"
         */
        if ($this->helperProduct->isRsEnabledForSpecificProduct() === false
            && empty($data['aggregateRating'])
            && empty($data['offers'])
        ) {
            return [];
        }

        if (!empty($data['aggregateRating']) && $this->helperProduct->isReviewsEnabled()) {
            $data['review'] = $this->helperDataProvider->getReviewData($this->_product, false);
        }

        $productIdValue = $this->helperDataProvider->getProductIdValue($this->_product);

        if ($productIdValue) {
            $data['productID'] = $productIdValue;
        }

        $color = $this->helperDataProvider->getColorValue($this->_product);
        if ($color) {
            $data['color'] = $color;
        }

        $brand = $this->helperDataProvider->getBrandValue($this->_product);
        if ($brand) {
            $data['brand'] = $brand;
        }

        $manufacturer = $this->helperDataProvider->getManufacturerValue($this->_product);
        if ($manufacturer) {
            $data['manufacturer'] = $manufacturer;
        }

        $model = $this->helperDataProvider->getModelValue($this->_product);
        if ($model) {
            $data['model'] = $model;
        }

        $gtin = $this->helperDataProvider->getGtinData($this->_product);
        if (!empty($gtin['gtinType']) && !empty($gtin['gtinValue'])) {
            $data[$gtin['gtinType']] = $gtin['gtinValue'];
        }

        $weightValue = $this->helperDataProvider->getWeightValue($this->_product);
        if ($weightValue) {
            $data['weight'] = $weightValue;
        }

        $categoryName = $this->helperDataProvider->getCategoryValue($this->_product);
        if ($categoryName) {
            $data['category'] = $categoryName;
        }

        $customProperties = $this->helperProduct->getCustomProperties();

        if ($customProperties) {
            foreach ($customProperties as $propertyName => $propertyValue) {
                if (!$propertyName || !$propertyValue) {
                    continue;
                }
                $value = $this->helperDataProvider->getCustomPropertyValue($product, $propertyValue);
                if ($value) {
                    $data[$propertyName] = $value;
                }
            }
        }

        return $data;
    }

    /**
     *
     * @return array
     */
    protected function getOfferData()
    {
        $data = [];

        if ($this->helperProduct->useMultipleOffer()
            && $this->_product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE
        ) {
            /** @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable $productType */
            $productType = $this->_product->getTypeInstance();

            $children = $productType->getUsedProducts($this->_product);

            /** @var \Magento\Catalog\Model\Product $child */
            foreach ($children as $child) {
                $data[] = $this->getChildProductOfferData($child, $this->_product);
            }

        } else {
            $data['@type'] = self::OFFER;
            $data['url']           = $this->_product->getProductUrl();
            $data['priceCurrency'] = $this->helperDataProvider->getCurrentCurrencyCode();
            $data['price'] = number_format($this->getPrice(), 2, '.', '');

            $condition = $this->helperDataProvider->getConditionValue($this->_product);
            if ($condition) {
                $data['itemCondition'] = 'http://schema.org/' . $condition;
            }

            if ($this->helperDataProvider->getAvailability($this->_product) && $this->_product->getData(self::PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE)) {
                $data['availability'] = self::ONLINE_ONLY;
            }elseif (!$this->helperDataProvider->getAvailability($this->_product)){
                $data['availability'] = self::OUT_OF_STOCK;
            }else {
                $data['availability'] = self::IN_STOCK;
            }

            $data['seller'] = [
                '@type' => 'Organization',
                'name'  => self::SELLER_BRAND_NAME
            ];

            $priceValidUntil = $this->helperDataProvider->getPriceValidUntilValue($this->_product);

            if ($priceValidUntil) {
                $data['priceValidUntil'] = $priceValidUntil;
            }
        }

        return $data;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Catalog\Model\Product $parentProduct
     * @return array
     */
    protected function getChildProductOfferData($product, $parentProduct)
    {
        $data                  = [];
        $data['@type']         = self::OFFER;
        $data['url']           = $parentProduct->getProductUrl();
        $data['priceCurrency'] = $this->helperDataProvider->getCurrentCurrencyCode();
        $data['price']         = number_format($this->getPrice($product), 2, '.', '');

        $condition = $this->helperDataProvider->getConditionValue($this->_product);
        if ($condition) {
            $data['itemCondition'] = 'http://schema.org/' . $condition;
        }

        if ($this->helperDataProvider->getAvailability($product) && $product->getData(self::PRODUCT_ATTRIBUTE_ONELINEEXCLUSIVE)) {
            $data['availability'] = self::ONLINE_ONLY;
        }elseif (!$this->helperDataProvider->getAvailability($product)){
            $data['availability'] = self::OUT_OF_STOCK;
        }else {
            $data['availability'] = self::IN_STOCK;
        }

        $data['seller'] = [
            '@type' => 'Organization',
            'name'  => self::SELLER_BRAND_NAME
        ];

        $priceValidUntil = $this->helperDataProvider->getPriceValidUntilValue($product);

        if ($priceValidUntil) {
            $data['priceValidUntil'] = $priceValidUntil;
        }

        return $data;
    }
}