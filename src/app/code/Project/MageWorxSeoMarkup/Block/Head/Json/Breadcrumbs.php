<?php
/**
 * Copyright © MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Project\MageWorxSeoMarkup\Block\Head\Json;

use Magento\Framework\UrlInterface;

abstract class Breadcrumbs extends \MageWorx\SeoMarkup\Block\Head\Json\Breadcrumbs
{
    /**
     *
     * {@inheritDoc}
     */
    protected function getMarkupHtml()
    {
        $html = '';

        if (!$this->helperBreadcrumbs->isRsEnabled() || $this->_request->getFullActionName() === 'cms_index_index') {
            return $html;
        }

        $breadcrumbsJsonData = $this->getJsonBreadcrumbsData();
        $breadcrumbsJson     = $breadcrumbsJsonData ? json_encode($breadcrumbsJsonData, JSON_UNESCAPED_SLASHES) : '';

        if ($breadcrumbsJsonData) {
            $html .= '<script type="application/ld+json">' . $breadcrumbsJson . '</script>';
        }

        return $html;
    }

    /**
     *
     * @return array
     */
    protected function getJsonBreadcrumbsData()
    {
        $breadcrumbsBlock = $this->getBreadcrumbsBlock();
        if (!$breadcrumbsBlock) {
            return [];
        }

        $crumbsArray = $this->getBreadcrumbs();

        if (empty($crumbsArray)) {
            return [];
        }

        $crumbs    = array_values($crumbsArray);
        $listitems = [];

        $data             = [];
        $data['@context'] = 'http://schema.org';
        $data['@type']    = 'BreadcrumbList';

        for ($i = 0; $i < count($crumbs); $i++) {
            $listItem          = [];
            $listItem['@type'] = 'ListItem';
            $position = $i+1;
            $listItem['position'] = $position;
            $listItem['name'] = $crumbs[$i]['label'];

            if (!empty($crumbs[$i]['link'])) {
                $listItem['item'] = $crumbs[$i]['link'];
            } else {
                $currentUrl              = $this->_storeManager->getStore()->getCurrentUrl();
                $listItem['item'] = explode('?', $currentUrl)[0];
            }

            $listitems[] = $listItem;
        }

        $data['itemListElement'] = $listitems;

        return !empty($data) ? $data : [];
    }
}
