<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Project\MageWorxSeoMarkup\Block\Head\Json\Breadcrumbs;

class Page extends \Project\MageWorxSeoMarkup\Block\Head\Json\Breadcrumbs
{
    protected $cmsPageBlockName = 'cms_page';

    /**
     *
     * {@inheritDoc}
     */
    protected function getBreadcrumbs()
    {
        $crumbs = $this->getHomeBreadcrumbs();
        $pageModel = $this->getPage();
        if (is_object($pageModel) && $pageModel->getTitle()) {
            $crumbs = $this->addCrumb(
                'page',
                [
                    'label' => $pageModel->getTitle(),
                    'title' => $pageModel->getTitle()
                ],
                $crumbs
            );
        }
        return $crumbs;
    }
}
