<?php

namespace Project\MageWorxSeoMarkup\Block\Head\Json;

/**
 * Class Store
 * @package Project\MageWorxSeoMarkup\Block\Head\Json
 */
class Store extends \MageWorx\SeoMarkup\Block\Head\Json
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Project\MageWorxSeoMarkup\Helper\Store
     */
    protected $storeHelper;

    /**
     * @var \Synolia\Retailer\Helper\FormatData
     */
    protected $formatData;

    /**
     * Store constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param  \Project\MageWorxSeoMarkup\Helper\Store $storeHelper
     * @param \Synolia\Retailer\Helper\FormatData $formatData
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\View\Element\Template\Context $context,
        \Project\MageWorxSeoMarkup\Helper\Store $storeHelper,
        \Synolia\Retailer\Helper\FormatData $formatData,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->urlInterface = $urlInterface;
        $this->storeHelper = $storeHelper;
        $this->formatData = $formatData;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Date_Exception
     * @throws \Zend_Json_Exception
     */
    protected function getMarkupHtml()
    {
        $html = '';

        if (!$this->storeHelper->isRsEnabled()) {
            return $html;
        }

        $storeJsonData = $this->getJsonStoreData();
        $storeJson     = $storeJsonData ? json_encode($storeJsonData) : '';

        if ($storeJsonData) {
            $html .= '<script type="application/ld+json">' . $storeJson . '</script>';
        }

        return $html;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Date_Exception
     * @throws \Zend_Json_Exception
     */
    protected function getJsonStoreData()
    {
        $retailer = $this->getRetailer();

        $data = [];
        $data['@context']   = 'https://schema.org';
        $data['@type']      = 'Store';
        $data['image']      = [$retailer->getData('image')];
        $data['@id']        = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $data['name']       = $retailer->getData('name');
        $data['address']    = $this->getAddressData($retailer);
        $data['geo']        = $this->getGeoData($retailer);
        $data['url']        = $this->urlInterface->getCurrentUrl();
        $data['telephone']  = $retailer->getData('phone_number');
        if ($this->storeHelper->addOpeningHours()) {
            $data['openingHoursSpecification'] = $this->getOpeningHours($retailer);
        }

        return $data;
    }

    /**
     * @return \Synolia\Retailer\Model\Retailer
     */
    private function getRetailer()
    {
        return $this->registry->registry('individual_retailer');
    }

    /**
     * @param \Synolia\Retailer\Model\Retailer $retailer
     * @return array
     */
    private function getAddressData($retailer)
    {
        return [
            '@type'             => 'PostalAddress',
            'streetAddress'     => $retailer->getData('street') . ' ' . $retailer->getData('street_bis'),
            'addressLocality'   => $retailer->getData('city'),
            'postalCode'        => $retailer->getData('zip_code'),
            'addressCountry'    => $retailer->getData('country')
        ];
    }

    /**
     * @param \Synolia\Retailer\Model\Retailer $retailer
     * @return array
     */
    private function getGeoData($retailer)
    {
        return [
            '@type'         => 'GeoCoordinates',
            'latitude'      => $retailer->getData('latitude'),
            'longitude'     => $retailer->getData('longitude')
        ];
    }

    /**
     * @param $retailer
     * @return array
     * @throws \Zend_Date_Exception
     * @throws \Zend_Json_Exception
     */
    private function getOpeningHours($retailer)
    {
        $schedules = [];
        foreach ($retailer->getSchedules() as $item) {
            $day = new \Zend_Date($item->getDay(), \Zend_Date::WEEKDAY_DIGIT, 'en');
            $day = ucfirst($day->get(\Zend_Date::WEEKDAY));
            $schedule['day'] = $day;
            $schedule['start_time'] = $item->getStartTime();
            $schedule['end_time'] = $item->getEndTime();
            $schedules[($item->getDay() == 0) ? 7 : $item->getDay()][] = $schedule;
        }
        $schedules = $this->formatData->formatSchedules($schedules);
        $data = [];
        foreach ($schedules as $day => $schedule) {
            $data[$day]['@type']        = 'OpeningHoursSpecification';
            $data[$day]['dayOfWeek']   = $schedule[0]['day'];
            $data[$day]['opens']        = $schedule[0]['start_time'];
            $endTime = isset($schedule[1]) ?
                date("H:i", strtotime($schedule[1]['end_time'] . " PM")) : $schedule[0]['end_time'];
            $data[$day]['closes']       = $endTime;
        }
        return $data;
    }
}
