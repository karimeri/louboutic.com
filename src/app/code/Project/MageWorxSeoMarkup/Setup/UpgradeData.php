<?php

namespace Project\MageWorxSeoMarkup\Setup;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Project\MageWorxSeoMarkup\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    private $eavSetup;
    private $logger;

    /**
     * @var WriterInterface $configWriter
     */
    protected $configWriter;


    public function __construct(
        \Magento\Eav\Setup\EavSetup $eavSetup,
        \Psr\Log\LoggerInterface $logger,
        WriterInterface $configWriter
    )
    {
        $this->eavSetup = $eavSetup;
        $this->logger   = $logger;
        $this->configWriter = $configWriter;

    }


    /**
     * @return WriterInterface
     */
    public function getConfigWriter()
    {
        return $this->configWriter;
    }

    /**
     * @param $configPath
     * @param string $scope
     * @param int $scopeId
     */
    public function deleteConfig($configPath, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeId = 0)
    {
        $this->getConfigWriter()->delete($configPath, $scope, $scopeId);

    }


    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->addSeoConditionAttributes();
        }

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->deleteRobotConf();
        }

        $installer->endSetup();

    }

    private function addSeoConditionAttributes()
    {
        try {
            $eavSetup = $this->eavSetup;
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'product_seo_condition',
                [
                    'type'  => 'varchar',
                    'group' => 'Marketing',
                    'label' => 'SEO Condition',
                    'input' => 'select',
                    'source' => 'Project\MageWorxSeoMarkup\Model\Config\Product\SeoCondition',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'visible' => true,
                    'required' => false,
                    'default' => 'NewCondition',
                    'sort_order' => 30,
                    'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_STORE,
                    'used_in_product_listing' => true,
                    'visible_on_front' => false,
                    'note' => 'This setting was added by MageWorx SEO Markup module'
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    private function deleteRobotConf()
    {
        $this->deleteConfig('design/search_engine_robots/default_robots', 'stores', 6);
    }
}
