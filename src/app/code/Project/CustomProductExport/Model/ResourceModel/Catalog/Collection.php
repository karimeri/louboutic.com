<?php

namespace Project\CustomProductExport\Model\ResourceModel\Catalog;

/**
 * Class Collection
 * @package Project\CustomProductExport\Model\ResourceModel\Attribute\Configurations
 */
class Collection extends \Magento\Framework\Data\Collection
{
    /**
     * @var \Magento\Eav\Model\AttributeFactory
     */
    protected $attributeFactory;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactory $entityFactory
     * @param \Magento\Eav\Model\AttributeFactory $attributeFactory
     * @throws \Exception
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Magento\Eav\Model\AttributeFactory $attributeFactory
    ) {
        $this->attributeFactory = $attributeFactory;

        // Add others rows
        $attributes = [
            'sku' => __('Sku (barcode)'),
            'searchable_sku' => __('Searchable sku'),
            'Product_name' => __('Product name'),
            'Product_collection' => __('Product collection'),
            'Family' => __('Family'),
            'Product_color' => __('Product color'),
            'back_in_stock' => __('Back in stock'),
            'In_stock_info' => __('In stock info'),
            'allow_preorder' => __('Allow preorder'),
            'stock_available' => __('Stock available'),
            'stock_vendable' => __('Stock vendable'),
            'price' => __('Price'),
            'status' => __('Status')
        ];

        foreach ($attributes as $attributeCode => $attributeLabel) {

            $attributeData = [
                'attribute_code' => $attributeCode,
                'frontend_label' => $attributeLabel,
                'backend_type' => 'varchar'
            ];

            $this->addItem(
                $this->attributeFactory->createAttribute('Magento\Eav\Model\Entity\Attribute', $attributeData)
            );
        }

        return parent::__construct($entityFactory);
    }
}
