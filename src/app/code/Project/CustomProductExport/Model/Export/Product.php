<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\CustomProductExport\Model\Export;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\ImportExport\Model\Export;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Catalog\Model\Product\Visibility;


/**
 * Class Product
 * @package Project\ImportExport\Model\Export
 */
class Product extends \Magento\ImportExport\Model\Export\AbstractEntity
{
    /**
     * Export entity type code
     */
    const ENTITY_TYPE_CODE = 'qlik';

    /**#@+
     * Product Status values
     */
    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 2;

    /**
     * Attribute collection name
     */
    const ATTRIBUTE_COLLECTION_NAME = 'Project\CustomProductExport\Model\ResourceModel\Catalog\Collection';

    /**
     * @var \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $_stockItemRepository;

    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory
     */
    protected $_itemFactory;
    /**
     * @var \Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory
     */
    protected $_itemOrderFactory;

    /**
     * DB connection.
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;

    /**
     * DB connection.
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_itemConfigurableFactory;

    protected $_request;

    /**
     * Configurations constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\ImportExport\Model\Export\Factory $collectionFactory
     * @param \Magento\ImportExport\Model\ResourceModel\CollectionByPagesIteratorFactory $resourceColFactory
     * @param \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory $configsFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\ImportExport\Model\Export\Factory $collectionFactory,
        \Magento\ImportExport\Model\ResourceModel\CollectionByPagesIteratorFactory $resourceColFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection,
        \Magento\CatalogInventory\Model\ResourceModel\Stock\ItemFactory $itemFactory,
        \Magento\Sales\Model\ResourceModel\Order\ItemFactory $itemOrderFactory,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\ConfigurableFactory $configurableItem,
        ResourceConnection $resource,
        array $data = []
    ) {
        parent::__construct(
            $scopeConfig,
            $storeManager,
            $collectionFactory,
            $resourceColFactory,
            $data
        );
        $this->_request = $context->getRequest();
        $this->collectionFactory = $collection;
        $this->_itemFactory = $itemFactory;
        $this->_itemOrderFactory = $itemOrderFactory;
        $this->_itemConfigurableFactory = $configurableItem;
        $this->_connection = $resource->getConnection();

    }



    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function export()
    {
        //Execution time may be long
        set_time_limit(0);

        $this->_initStores();
        $this->_initWebsites();
        $writer     = $this->getWriter();
        $exportData = $this->getExportData();
        $writer->setHeaderCols($this->_getHeaderColumns());

        foreach ($exportData as $item) {
            $this->exportItem($item);
        }

        return $writer->getContents();
    }

    //Get Stock Item
    protected function _getStockItem($productId,$type_id)
    {
        $ids = [];
        $total_qty = 0;
        if($type_id == 'configurable') {
            $childrens = $this->_getChildrens($productId);
            if (\is_array($childrens) && !empty($childrens)){
                $result = [];
                foreach ($childrens as $child){
                    $ids[] = $child['product_id'];
                }
                $select = $this->_connection->select();
                $select->from(
                    $this->_itemFactory->create()->getMainTable()
                    ,['qty' => new \Zend_Db_Expr('SUM(qty)')]
                )
                    ->where(
                        'product_id in (?) and stock_id = 1',
                        $ids
                    );
                $stock_parent = $this->_connection->query($select)->fetchAll();
                if(\is_array($stock_parent) && !empty($stock_parent) && isset($stock_parent[0]['qty'])) {
                    $total_qty = $stock_parent[0]['qty'];
                }
                $select_configurable = $this->_connection->select();
                $select_configurable->from(
                    $this->_itemFactory->create()->getMainTable()
                )->where(
                    'product_id in (?) and stock_id = 1',
                    $productId
                );
                $result = $this->_connection->query($select_configurable)->fetchAll();
                if($total_qty > 0 && isset($result[0]['qty'])) {
                    $result[0]['qty'] = $total_qty;
                }
                return $result;
            }
        }else if($type_id == 'simple') {
            $select = $this->_connection->select();
            $select->from(
                $this->_itemFactory->create()->getMainTable()
            );
            $select->where(
                'product_id = ? and stock_id = 1',
                $productId
            );
            return $this->_connection->query($select)->fetchAll();
        }
        return false;
    }

    //Get Children Item
    protected function _getChildrens($productId)
    {
        $select = $this->_connection->select()->from(
            $this->_itemConfigurableFactory->create()->getMainTable()
        )->where(
            'parent_id=?',
            $productId
        );
        $stmt = $this->_connection->query($select);
        return  $stmt->fetchAll();
    }

    //Get ordered Stock
    protected function _getOrderedQty($productId)
    {
        $select = $this->_connection->select()->from(
            $this->_itemOrderFactory->create()->getMainTable(),
            ['product_id'=> 'product_id','ordered_qty' => new \Zend_Db_Expr('SUM(qty_ordered)')]
        )->where(
            'product_id = ?',
            $productId
        );
        return  $this->_connection->query($select)->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE|\PDO::FETCH_ASSOC);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getExportData()
    {
        $exportData = [];
        $collection = $this->_getEntityCollection();
        $array_attribute_select = ['louboutin_collection','louboutin_family','color'];
        $array_attribute_bolean = ['back_in_stock','allow_preorder'];
        foreach ($collection as &$item) {
            $stock = $this->_getStockItem($item->getId(), $item->getTypeId());
            $item['status'] = $this->_getOptionText($item->getStatus());
            $ordered = 0;
            if($stock) {
                $item['In_stock_info'] = (int)$stock['0']['is_in_stock'] ? __('Out of Stock'): __('In Stock');
                $item['stock_available'] = (int)$stock['0']['qty'];
            }
            $stock_ordered = $this->_getOrderedQty($item->getId());
            if(\is_array($stock_ordered) && !empty($stock_ordered) && isset($stock_ordered[$item->getId()])) {
                $ordered = $stock_ordered[$item->getId()]['ordered_qty'];
            }
            $item['stock_vendable'] = (int)$ordered;
            $item['Product_name'] = $item->getName();
            foreach ($array_attribute_select as $attribute) {
                $item = $this->_getTextOptionAttributeSelect($item, $attribute);
            }
            foreach ($array_attribute_bolean as $attribute) {
                $item = $this->_getBoleanAttribute($item, $attribute);
            }
            $item['Product_color'] = $item->getColor();
            $item['Family'] = $item->getLouboutinFamily();
            $item['Product_collection'] = $item->getLouboutinCollection();
            $exportData[] = $item;
        }
        return $exportData;

    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public static function getOptionArray()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    protected function _getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getTextOptionAttributeSelect($item,$attribute)
    {
        $attr = $item->getResource()->getAttribute($attribute);
        if ($attr->usesSource()) {
            $item[$attribute] = $attr->getSource()->getOptionText($item->getData($attribute));
        }
        return $item;
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getBoleanAttribute($item,$attribute)
    {
        $attr =(int) $item->getData($attribute);
        $item[$attribute] = $attr ? __('Yes'):__('No') ;
        return $item;
    }


    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getEntityCollection()
    {
        $collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('visibility', ['eq' => Visibility::VISIBILITY_BOTH]);
        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    /**
     * {@inheritdoc}
     */
    protected function _getHeaderColumns()
    {
        return $this->_getExportAttributeCodes();
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $item
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function exportItem($item)
    {
        $this->getWriter()->writeRow($item->getData());
    }

    /**
     * @return string
     */
    public function getEntityTypeCode()
    {
        return self::ENTITY_TYPE_CODE;
    }

}
