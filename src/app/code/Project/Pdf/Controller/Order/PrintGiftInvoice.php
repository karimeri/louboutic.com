<?php

namespace Project\Pdf\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Controller\AbstractController\OrderViewAuthorizationInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class PrintGiftInvoice
 *
 * @package Project\Pdf\Controller\Order
 * @author Synolia <contact@synolia.com>
 */
class PrintGiftInvoice extends \Magento\Sales\Controller\Order\PrintInvoice
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;

    /**
     * @param Context $context
     * @param OrderViewAuthorizationInterface $orderAuthorization
     * @param \Magento\Framework\Registry $registry
     * @param PageFactory $resultPageFactory
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */
    public function __construct(
        Context $context,
        OrderViewAuthorizationInterface $orderAuthorization,
        \Magento\Framework\Registry $registry,
        PageFactory $resultPageFactory,
        FileFactory $fileFactory
    ) {
        parent::__construct($context, $orderAuthorization, $registry, $resultPageFactory);
        $this->fileFactory = $fileFactory;
    }

    /**
     * Print Invoice Action
     * rewrite of parent. Just changing the pdf generation class
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     * @throws \Exception
     */
    public function execute()
    {
        $invoiceId = (int) $this->getRequest()->getParam('invoice_id');

        if ($invoiceId) {
            $invoice = $this->_objectManager->create(
                \Magento\Sales\Api\InvoiceRepositoryInterface::class
            )->get($invoiceId);
            $order = $invoice->getOrder();
            $invoices = [$invoice];
        } else {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = $this->_objectManager->create(\Magento\Sales\Model\Order::class)->load($orderId);
            $invoices = $order->getInvoiceCollection();
        }

        if ($this->orderAuthorization->canView($order)) {
            $this->_coreRegistry->register('current_order', $order);
            if (isset($invoice)) {
                $this->_coreRegistry->register('current_invoice', $invoice);
            }

            if (\count($invoices)) {
                $pdf = $this->_objectManager
                    ->create(\Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoice::class)
                    ->getPdf($invoices);

                $date = $this->_objectManager->get(
                    \Magento\Framework\Stdlib\DateTime\DateTime::class
                )->date('Y-m-d_H-i-s');

                return $this->fileFactory->create(
                    'gift_invoice' . $date . '.pdf',
                    $pdf->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }

            /** @var \Magento\Framework\View\Result\Page $resultPage */
            $resultPage = $this->resultPageFactory->create();
            $resultPage->addHandle('print');

            return $resultPage;
        } else {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            if ($this->_objectManager->get(\Magento\Customer\Model\Session::class)->isLoggedIn()) {
                $resultRedirect->setPath('*/*/history');
            } else {
                $resultRedirect->setPath('sales/guest/form');
            }

            return $resultRedirect;
        }
    }
}
