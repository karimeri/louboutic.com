<?php

namespace Project\Pdf\Controller\Adminhtml\Rma;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class PrintAction
 *
 * @package Project\Pdf\Controller\Adminhtml\Rma
 * @author Synolia <contact@synolia.com>
 */
class PrintAction extends \Magento\Rma\Controller\Adminhtml\Rma\PrintAction
{
    /**
     * rewrite of parent. Just changing the pdf generation class
     * @return \Magento\Framework\App\ResponseInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Pdf_Exception
     * @throws \Exception
     */
    public function execute()
    {
        $rmaId = (int)$this->getRequest()->getParam('rma_id');
        if ($rmaId) {
            /** @var $rmaModel \Magento\Rma\Model\Rma */
            $rmaModel = $this->_objectManager->create(\Magento\Rma\Model\Rma::class)->load($rmaId);
            if ($rmaModel) {
                /** @var $dateModel \Magento\Framework\Stdlib\DateTime\DateTime */
                $dateModel = $this->_objectManager->get(\Magento\Framework\Stdlib\DateTime\DateTime::class);
                /** @var $pdfModel \Magento\Rma\Model\Pdf\Rma */
                $pdfModel = $this->_objectManager->create(\Project\Pdf\Model\Magento\Sales\Rma\Pdf\Rma::class);
                $pdf = $pdfModel->getPdf([$rmaModel]);

                return $this->_fileFactory->create(
                    'rma' . $dateModel->date('Y-m-d_H-i-s') . '.pdf',
                    $pdf->render(),
                    DirectoryList::MEDIA,
                    'application/pdf'
                );
            }
        } else {
            $this->_forward('noroute');
        }
    }
}
