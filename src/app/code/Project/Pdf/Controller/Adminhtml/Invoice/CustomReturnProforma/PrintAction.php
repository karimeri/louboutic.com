<?php

namespace Project\Pdf\Controller\Adminhtml\Invoice\CustomReturnProforma;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class PrintAction
 *
 * @package Project\Pdf\Controller\Adminhtml\Invoice\CustomReturnProforma
 * @author Synolia <contact@synolia.com>
 */
class PrintAction extends \Magento\Sales\Controller\Adminhtml\Invoice\AbstractInvoice\PrintAction
{
    /**
     * rewrite of parent. Just changing the pdf generation class
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Exception
     */
    public function execute()
    {
        $invoiceId = $this->getRequest()->getParam('invoice_id');

        if ($invoiceId) {
            $invoice = $this->_objectManager->create(
                \Magento\Sales\Api\InvoiceRepositoryInterface::class
            )->get($invoiceId);

            if ($invoice) {
                $pdf = $this->_objectManager
                    ->create(\Project\Pdf\Model\Magento\Sales\Order\Pdf\CustomReturnProformaInvoice::class)
                    ->getPdf([$invoice]);

                $date = $this->_objectManager->get(
                    \Magento\Framework\Stdlib\DateTime\DateTime::class
                )->date('Y-m-d_H-i-s');

                return $this->_fileFactory->create(
                    'custom_return_proforma_invoice' . $date . '.pdf',
                    $pdf->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
