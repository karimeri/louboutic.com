<?php

namespace Project\Pdf\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\ScopeInterface;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class Config
 *
 * @package Project\Pdf\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    // phpcs:disable
    const XML_PATH_SALES_IDENTITY_ADDRESS = 'sales/identity/address';
    const XML_PATH_SALES_IDENTITY_LOGO = 'sales/identity/logo';
    const XML_PATH_SALES_PDF_INVOICE_SUPPLIER = 'sales_pdf/invoice/supplier_information';
    const XML_PATH_SALES_PDF_CREDITMEMO_SUPPLIER = 'sales_pdf/creditmemo/supplier_information';
    const XML_PATH_SALES_PDF_INVOICE_FOOTER_FIRSTLINE = 'sales_pdf/invoice/footer_first_line';
    const XML_PATH_SALES_PDF_INVOICE_FOOTER_CORE = 'sales_pdf/invoice/footer_core';
    const XML_PATH_SALES_PDF_INVOICE_FOOTER_ENDLINE = 'sales_pdf/invoice/footer_end_line';
    const XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_FIRSTLINE = 'sales_pdf/creditmemo/footer_first_line';
    const XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_CORE = 'sales_pdf/creditmemo/footer_core';
    const XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_ENDLINE = 'sales_pdf/creditmemo/footer_end_line';
    const XML_PATH_SALES_PDF_INVOICE_SIGNAUTRE_TEXT = 'sales_pdf/invoice/signature_text';
    const XML_PATH_SALES_PDF_PROFORMA_INVOICE_FOOTER_FIRSTLINE = 'sales_pdf/proforma_invoice/footer_first_line';
    const XML_PATH_SALES_PDF_PROFORMA_INVOICE_FOOTER_CORE = 'sales_pdf/proforma_invoice/footer_core';
    const XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_FOOTER_FIRSTLINE = 'sales_pdf/custom_return_proforma_invoice/footer_first_line';
    const XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_FOOTER_CORE = 'sales_pdf/custom_return_proforma_invoice/footer_core';
    const XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_FOOTER_ENDLINE = 'sales_pdf/custom_return_proforma_invoice/footer_end_line';
    const XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_REASON_FOR_EXPORT = 'sales_pdf/custom_return_proforma_invoice/reason_for_export';
    const XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_TEXT_BEFORE_PRODUCTS = 'sales_pdf/custom_return_proforma_invoice/text_before_products';
    const XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_RETURN_TO = 'sales_pdf/custom_return_proforma_invoice/return_to';
    const XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_TEXT = 'sales_pdf/creditmemo/footer_text';
    const XML_PATH_SALES_PDF_CREDITMEMO_AFTER_PRODUCT_GRID_TEXT = 'after_product_grid_text';
    const WEBSITE_CA = 'ca';
    const WEBSITE_US = 'us';
    const XML_PATH_CONTACT_MAIL = 'trans_email/ident_support/email';
    const XML_PATH_STORE_PHONE2 = 'general/store_information/phone2';
    // phpcs:enable

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Config constructor.
     *
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param EnvironmentManager $environmentManager
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        EnvironmentManager $environmentManager
    ) {
        $this->serializer = $serializer;
        $this->environmentManager = $environmentManager;

        parent::__construct($context);
    }

    /**
     * @param             $configName
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesIdentityAddress($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_IDENTITY_ADDRESS, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesIdentityLogo($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_IDENTITY_LOGO, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfInvoiceSupplier($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_INVOICE_SUPPLIER, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCreditmemoSupplier($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_SUPPLIER, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfInvoiceFooterFirstline($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_INVOICE_FOOTER_FIRSTLINE, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCreditMemoFooterFirstline($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_FIRSTLINE, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfInvoiceFooterCore($scope = null)
    {
        return $this->parserContact(
            $this->getConfig(self::XML_PATH_SALES_PDF_INVOICE_FOOTER_CORE, $scope),
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCreditMemoFooterCore($scope = null)
    {
        return $this->parserContact(
            $this->getConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_CORE, $scope),
            $scope
        );
    }


    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfInvoiceFooterEndline($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_INVOICE_FOOTER_ENDLINE, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCreditMemoFooterEndline($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_ENDLINE, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfInvoiceSignagureText($scope = null)
    {
        return $this->getConfig(self::XML_PATH_SALES_PDF_INVOICE_SIGNAUTRE_TEXT, $scope);
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfProformaInvoiceFooterFirstline($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_PROFORMA_INVOICE_FOOTER_FIRSTLINE,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfProformaInvoiceFooterCore($scope = null)
    {
        return $this->parserContact(
            $this->getConfig(self::XML_PATH_SALES_PDF_PROFORMA_INVOICE_FOOTER_CORE, $scope),
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCustomReturnProformaInvoiceFooterFirstline($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_FOOTER_FIRSTLINE,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCustomReturnProformaInvoiceFooterCore($scope = null)
    {
        return $this->parserContact(
            $this->getConfig(self::XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_FOOTER_CORE, $scope),
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCustomReturnProformaInvoiceFooterEndline($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_FOOTER_ENDLINE,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCustomReturnProformaInvoiceReasonForExport($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_REASON_FOR_EXPORT,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCustomReturnProformaInvoiceTextBeforeProducts($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_TEXT_BEFORE_PRODUCTS,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCustomReturnProformaInvoiceReturnTo($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CUSTOM_RETURN_PROFORMA_INVOICE_RETURN_TO,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCreditmemoFooterText($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CREDITMEMO_FOOTER_TEXT,
            $scope
        );
    }

    /**
     * @param null $scope
     *
     * @return bool|string
     */
    public function getSalesPdfCreditmemoAfterProductGridText($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CREDITMEMO_AFTER_PRODUCT_GRID_TEXT,
            $scope
        );
    }

    /**
     * @param int|null $scope
     *
     * @return bool|string
     */
    public function getStorePhone2($scope = null)
    {
        return $this->getConfig(
            self::XML_PATH_SALES_PDF_CREDITMEMO_AFTER_PRODUCT_GRID_TEXT,
            $scope
        );
    }

    /**
     * @param string|null $string
     * @param int|null $store
     *
     * @return mixed
     */
    protected function parserContact($string, $scope = null)
    {
        if ($string) {
            $string = str_replace(
                ['{{store contact email adress}}', '{{store contact phone number}}'],
                [
                    $this->getConfig(self::XML_PATH_CONTACT_MAIL, $scope),
                    $this->getConfig(self::XML_PATH_STORE_PHONE2, $scope),
                ],
                $string
            );
        }

        return $string;
    }

    /**
     * @param string $code
     *
     * @return bool
     */
    public function callFunctionByCode($code, $store)
    {
        if (in_array($code, ['subtotal', 'soustotal'])) {
            return $this->hasSubtotal();
        } elseif (in_array($code, ['tax', 'taxe', 'tax_amount'])) {
            return $this->hasTax($store);
        } elseif (in_array($code, ['shipping_handling', 'frais_de_port', 'shipping_amount'])) {
            return $this->hasShippingFees();
        } elseif (in_array($code, ['grand_total', 'montant_global'])) {
            return $this->hasGrandTotal();
        } elseif (in_array($code, [
            'declared_value',
            'total_wo_tax',
            'total_ht',
            'total_without_tax',
            'base_subtotal',
        ])) {
            return $this->hasTotalWithoutTax();
        }

        return false;
    }
}
