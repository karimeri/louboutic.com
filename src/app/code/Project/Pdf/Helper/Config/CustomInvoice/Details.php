<?php

namespace Project\Pdf\Helper\Config\CustomInvoice;

use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class Details
 *
 * @package Project\Pdf\Helper\Config\CustomInvoice
 * @author Synolia <contact@synolia.com>
 */
class Details extends Config
{
    /**
     * @return bool
     */
    public function hasHtsCode()
    {
        return \in_array($this->environmentManager->getEnvironment(), [Environment::US, Environment::HK]);
    }

    /**
     * @return bool
     */
    public function hasCustomCode()
    {
        return $this->environmentManager->getEnvironment() === Environment::EU;
    }

    /**
     * @return bool
     */
    public function hasSku()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasConfigurableSku()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasUnitPrice()
    {
        return \in_array($this->environmentManager->getEnvironment(), [Environment::EU, Environment::HK]);
    }

    /**
     * @return bool
     */
    public function hasComposition()
    {
        return $this->environmentManager->getEnvironment() === Environment::HK;
    }
}
