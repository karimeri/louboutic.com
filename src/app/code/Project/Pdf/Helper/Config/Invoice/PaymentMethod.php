<?php

namespace Project\Pdf\Helper\Config\Invoice;

use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class PaymentMethod
 *
 * @package Project\Pdf\Helper\Config\Invoice
 * @author Synolia <contact@synolia.com>
 */
class PaymentMethod extends Config
{
    const PAYMENT_INFO_MAPPING = [
        'card_type' => 'hasCardType',
        'cardType' => 'hasCardType',
        'card_number' => 'hasCardNumber',
        'last4' => 'hasCardNumber',
        'card_expiry_date' => 'hasExpiryDate',
        'card_expire' => 'hasExpiryDate',
        'payment_type' => 'hasTransactionType',
        'cc_exp' => 'hasExpiryDate',
    ];

    const PAYMENT_INFO_TRANSLATION = [
        'card_type' => 'Card Type',
        'cardType' => 'Card Type',
        'card_number' => 'Card Number',
        'last4' => 'Card Number',
        'card_expiry_date' => 'Expiry Date',
        'card_expire' => 'Expiry Date',
        'payment_type' => 'Transaction Type',
        'cc_exp' => 'Expiry Date',
    ];

    /**
     * @return bool
     */
    public function hasCardHolder()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasCardType()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasCardNumber()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasExpiryDate()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasTransactionType()
    {
        return false;
    }

    /**
     * @param $label
     *
     * @return bool|mixed|null|string
     */
    public function getPaymentConfig($label)
    {
        if (isset(self::PAYMENT_INFO_MAPPING[$label])) {
            return $this->{self::PAYMENT_INFO_MAPPING[$label]}();
        }

        return null;
    }

    /**
     * @param string $code
     *
     * @return mixed
     */
    public function translateCode($code)
    {
        return self::PAYMENT_INFO_TRANSLATION[$code];
    }
}
