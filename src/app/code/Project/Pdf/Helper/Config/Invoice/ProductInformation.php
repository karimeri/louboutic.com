<?php

namespace Project\Pdf\Helper\Config\Invoice;

use Magento\Catalog\Model\Product;
use Project\Core\Helper\AttributeSet;
use Project\Core\Manager\EnvironmentManager;
use Project\Pdf\Helper\Config;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class ProductInformation
 * @package Project\Pdf\Helper\Config
 * @author Synolia <contact@synolia.com>
 */
class ProductInformation extends Config
{
    /**
     * @var AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * Config constructor.
     *
     * @param Context $context
     * @param SerializerInterface $serializer
     * @param EnvironmentManager $environmentManager
     * @param AttributeSet $attributeSetHelper
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        EnvironmentManager $environmentManager,
        AttributeSet $attributeSetHelper
    ) {
        parent::__construct($context, $serializer, $environmentManager);

        $this->attributeSetHelper = $attributeSetHelper;
    }

    /**
     * @return bool
     */
    public function hasName()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasColor()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasSize()
    {
        return true;
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    public function hasSizeLabel($product)
    {
        if ($this->attributeSetHelper->isBeauty($product)) {
            return 'Volume';
        } elseif ($this->attributeSetHelper->isShoes($product)) {
            if ($product->getResource()->getAttribute('gender')->getFrontend()->getValue($product) === 'MEN') {
                return "Men's Size";
            }

            return "Women's Size";
        }

        return 'Size';
    }

    /**
     * @return bool
     */
    public function hasOrigin()
    {
        return $this->environmentManager->getEnvironment() === \Project\Core\Model\Environment::HK;
    }

    /**
     * @param Product $product
     *
     * @return mixed
     */
    public function hasGender($product)
    {
        return $product->getResource()->getAttribute('gender')->getFrontend()->getValue($product);
    }
}
