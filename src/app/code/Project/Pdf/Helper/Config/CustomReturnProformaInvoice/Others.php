<?php

namespace Project\Pdf\Helper\Config\CustomReturnProformaInvoice;

use Magento\Store\Model\Store;
use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class Others
 *
 * @package Project\Pdf\Helper\Config\CustomReturnProformaInvoice
 * @author Synolia <contact@synolia.com>
 */
class Others extends Config
{
    /**
     * @return bool
     */
    public function hasQuantity()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasUnitPrice()
    {
        return true;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTaxRate($store)
    {
        return false;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasPriceWithVat($store)
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasTotalWithoutTax()
    {
        return false;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTotalTax($store)
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasDuty()
    {
        return false;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTotal($store)
    {
        return false;
    }
}
