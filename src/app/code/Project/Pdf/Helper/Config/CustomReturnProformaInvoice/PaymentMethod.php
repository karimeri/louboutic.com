<?php

namespace Project\Pdf\Helper\Config\CustomReturnProformaInvoice;

use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class PaymentMethod
 *
 * @package Project\Pdf\Helper\Config\CustomReturnProformaInvoice
 * @author Synolia <contact@synolia.com>
 */
class PaymentMethod extends Config
{
    const PAYMENT_INFO_MAPPING = [
        'card_type' => 'hasCardType',
        'cardType' => 'hasCardType',
        'card_number' => 'hasCardNumber',
        'last4' => 'hasCardNumber',
        'card_expiry_date' => 'hasExpiryDate',
        'card_expire' => 'hasExpiryDate',
        'payment_type' => 'hasTransactionType',
    ];

    /**
     * @return bool
     */
    public function hasCardHolder()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasCardType()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasCardNumber()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasExpiryDate()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasTransactionType()
    {
        return false;
    }

    /**
     * @param $label
     *
     * @return bool|mixed|null|string
     */
    public function getPaymentConfig($label)
    {
        if (isset(self::PAYMENT_INFO_MAPPING[$label])) {
            return $this->{self::PAYMENT_INFO_MAPPING[$label]}();
        }

        return null;
    }
}
