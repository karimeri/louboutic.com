<?php

namespace Project\Pdf\Helper\Config\ProformaInvoice;

use Magento\Store\Model\Store;
use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class Totals
 *
 * @package Project\Pdf\Helper\Config\ProformaInvoice
 * @author Synolia <contact@synolia.com>
 */
class Totals extends Config
{
    const FUNCTION_MAPPING = [
        'subtotal' => 'hasSubtotal',
        'tax' => 'hasTax',
    ];

    /**
     * @return bool
     */
    public function hasSubtotal()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasShippingFees()
    {
        return \in_array($this->environmentManager->getEnvironment(), [Environment::EU, Environment::US]);
    }

    /**
     * @return bool
     */
    public function hasTotalWithoutTax()
    {
        return true;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTax($store)
    {
        return (
            $this->environmentManager->getEnvironment() === Environment::EU
            || $store->getWebsite()->getCode() === Environment::US
        );
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTaxRegister($store)
    {
        return $store->getWebsite()->getCode() === self::WEBSITE_CA;
    }

    /**
     * @param string $region
     *
     * @return array
     */
    public function getTaxRegisterLabel($region)
    {
        if (\in_array($region, ['AB', 'ON', 'NB', 'NS', 'NL'])) {
            return ['GST/HST Registration #: 842858573RT001'];
        } elseif ($region === 'QC') {
            return ['GST Registration #: 842858573RT001','QST Registration #: 1220724537 TQ0001'];
        } elseif (\in_array($region, ['BC', 'SK', 'MB'])) {
            return ['GST Registration #: 842858573RT001', 'PST Registration #: 84285 8573 MT0001'];
        }

        return [];
    }

    /**
     * @return bool
     */
    public function hasGrandTotal()
    {
        return true;
    }
}
