<?php

namespace Project\Pdf\Helper\Config\ProformaInvoice;

use Magento\Store\Model\Store;
use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class Others
 *
 * @package Project\Pdf\Helper\Config\ProformaInvoice
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class Others extends Config
{
    /**
     * @return bool
     */
    public function hasQuantity()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasUnitPrice()
    {
        return true;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTaxRate($store)
    {
        return $store->getWebsite()->getCode() !== self::WEBSITE_CA;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasPriceWithVat($store)
    {
        return (
            $this->environmentManager->getEnvironment() === Environment::EU
            || $store->getWebsite()->getCode() === Environment::US
        );
    }

    /**
     * @return bool
     */
    public function hasTotalWithoutTax()
    {
        return \in_array($this->environmentManager->getEnvironment(), [Environment::EU, Environment::US]);
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTotalTax($store)
    {
        return (
            \in_array($this->environmentManager->getEnvironment(), [Environment::EU, Environment::HK, Environment::JP])
            || $store->getWebsite()->getCode() === self::WEBSITE_US
        );
    }

    /**
     * @return bool
     */
    public function hasDuty()
    {
        return $this->environmentManager->getEnvironment() === Environment::HK;
    }

    /**
     * @param Store $store
     *
     * @return bool
     */
    public function hasTotal($store)
    {
        return $store->getWebsite()->getCode() !== self::WEBSITE_CA;
    }

    /**
     * @return array
     */
    public function getImporterLines()
    {
        return [
            'CHRISTIAN LOUBOUTIN Suisse',
            'Fiscal Representative Baseroma SRL',
            'P.zza Lorenzo in Lucinda 22',
            '00186 Roma',
            'ITALY',
            'VAT 11789141006',
        ];
    }

    /**
     * @return bool
     */
    public function hasDate()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasReference()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasWaybillNumber()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasTotalDeclaredValue()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasTotalPieces()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasTypeOfExport()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasReasonOfExport()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasTermsOfTrade()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasDHLAccount()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasReturnExWaybill()
    {
        return true;
    }
}
