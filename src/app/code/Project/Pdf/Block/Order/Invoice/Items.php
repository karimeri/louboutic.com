<?php

namespace Project\Pdf\Block\Order\Invoice;

use Magento\Sales\Model\Order;

/**
 * Class Items
 *
 * @package Project\Pdf\Block\Order\Invoice
 * @author Synolia <contact@synolia.com>
 */
class Items extends \Magento\Sales\Block\Order\Invoice\Items
{
    /**
     * @param object $invoice
     * @return string
     */
    public function getPrintGiftInvoiceUrl($invoice)
    {
        return $this->getUrl('sales/order/printGiftInvoice', ['invoice_id' => $invoice->getId()]);
    }

    /**
     * @param Order $order
     * @return string
     */
    public function hasGift($order)
    {
        return ($order->getGiftMessageId() || $order->getGwId());
    }
}
