<?php

namespace Project\Pdf\Plugin;

use Magento\Framework\UrlInterface;
use Magento\Rma\Block\Adminhtml\Rma\Edit;
use Magento\Store\Model\StoreManagerInterface;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class PluginBeforeRmaView
 *
 * @package Project\ExchangeOffline\Plugin
 * @author Synolia <contact@synolia.com>
 */
class PluginBeforeRmaView
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * PluginBeforeRmaView constructor.
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        UrlInterface $urlBuilder,
        EnvironmentManager $environmentManager,
        StoreManagerInterface $storeManager
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->environmentManager = $environmentManager;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Edit $subject
     * @param string $result
     *
     * @return null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetBackUrl(Edit $subject, $result)
    {
        if ($this->canDisplayProformaInvoiceButton($subject)) {
            $url = $this->urlBuilder->getUrl(
                'sales_custom/rma/printproforma',
                ['rma_id' => $subject->getRma()->getId()]
            );
            $onClick = "setLocation('{$url}')";

            $subject->addButton(
                'print_proforma',
                [
                    'label' => __('Print Proforma Invoice'),
                    'onclick' => $onClick,
                    'class' => 'reset',
                ],
                -1
            );
        }

        return $subject->getUrl('*/*/');
    }

    /**
     * @param Edit $subject
     * @return bool
     */
    protected function canDisplayProformaInvoiceButton($subject)
    {
        $store = $this->storeManager->getStore($subject->getRma()->getStoreId());
        return strpos($store->getCode(), 'ch') !== false;
    }
}
