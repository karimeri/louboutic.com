<?php

namespace Project\Pdf\Plugin;

use Magento\Framework\UrlInterface;
use Magento\Sales\Block\Adminhtml\Order\Invoice\View;
use Magento\Sales\Model\Order;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class PluginBeforeInvoiceView
 *
 * @package Project\ExchangeOffline\Plugin
 * @author Synolia <contact@synolia.com>
 */
class PluginBeforeInvoiceView
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * PluginBeforeInvoiceView constructor.
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        UrlInterface $urlBuilder,
        EnvironmentManager $environmentManager
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @param View $subject
     * @param string $result
     *
     * @return null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetBackUrl(View $subject, $result)
    {
        if ($this->canDisplayGiftInvoiceButton($subject->getInvoice()->getOrder())) {
            $url = $this->urlBuilder->getUrl(
                'sales_custom/invoice_gift/print',
                ['invoice_id' => $subject->getInvoice()->getId()]
            );
            $onClick = "setLocation('{$url}')";

            $subject->addButton(
                'print_gift',
                [
                    'label' => __('Print Gift'),
                    'onclick' => $onClick,
                    'class' => 'reset',
                ],
                -1
            );
        }

        if ($this->canDisplayCustomInvoiceButton()) {
            $url = $this->urlBuilder->getUrl(
                'sales_custom/invoice_custom/print',
                ['invoice_id' => $subject->getInvoice()->getId()]
            );
            $onClick = "setLocation('{$url}')";

            $subject->addButton(
                'print_custom',
                [
                    'label' => __('Print Custom'),
                    'onclick' => $onClick,
                    'class' => 'reset',
                ],
                -1
            );
        }

        if ($this->canDisplayCustomInvoiceButton()) {
            $url = $this->urlBuilder->getUrl(
                'sales_custom/invoice_customreturnproforma/print',
                ['invoice_id' => $subject->getInvoice()->getId()]
            );
            $onClick = "setLocation('{$url}')";

            $subject->addButton(
                'print_customreturnproforma',
                [
                    'label' => __('Print Custom Return Proforma'),
                    'onclick' => $onClick,
                    'class' => 'reset',
                ],
                -1
            );
        }

        return $result;
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    protected function canDisplayGiftInvoiceButton($order)
    {
        return ($order->getGiftMessageId() || $order->getGwId() || $order->getGwItemsBasePriceInclTax());
    }

    /**
     * @return bool
     */
    protected function canDisplayCustomInvoiceButton()
    {
        return $this->environmentManager->getEnvironment() === Environment::HK;
    }
}
