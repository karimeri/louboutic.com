<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf;

use Magento\Sales\Model\Order;

/**
 * Class Invoice
 *
 * @package Project\Pdf\Model\Order\Pdf
 * @author Synolia <contact@synolia.com>
 */
class GiftInvoice extends \Project\Pdf\Model\Magento\Sales\Order\Pdf\Invoice
{
    protected $invoiceType = 'gift_invoice';
    protected $documentName = 'Gift Invoice';

    /**
     * @param Order $order
     *
     * @return array
     */
    /*protected function preparePaymentBlockLines($order)
    {
        return [\mb_strtoupper(__('Gift'))];
    }*/

    /**
     * @param Invoice $invoice
     * @param $startPosition
     * @param $tableWidth
     * @param array $itemsHeader
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getItemsHeaderConfiguration($invoice, $startPosition, $tableWidth, $itemsHeader = [])
    {
        $itemsHeader = \array_merge(
            $itemsHeader,
            [
                'informations' => [
                    'label' => \mb_strtoupper(__('Product Information')),
                    'length' => 100,
                ],
                'details' => [
                    'label' => \mb_strtoupper(__('Details')),
                    'length' => 80,
                ],
            ]
        );

        if ($this->othersHelper->hasQuantity()) {
            $itemsHeader['qty'] = [
                'label' => \mb_strtoupper(__('Qty')),
                'length' => 20,
            ];
        }

        $columnsLength = 0;
        foreach ($itemsHeader as $columnCode => $columnHeader) {
            $columnsLength += $columnHeader['length'];
        }

        $margin = ($tableWidth - $startPosition - $columnsLength) / (count($itemsHeader) - 1);

        $position = $startPosition;
        foreach ($itemsHeader as $columnCode => $columnHeader) {
            $itemsHeader[$columnCode]['position'] = (int) $position;
            $position += $margin + $itemsHeader[$columnCode]['length'];
        }

        return $itemsHeader;
    }

    /**
     * @return bool
     */
    public function displayPrices()
    {
        return false;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\AbstractModel $invoice
     *
     * @return \Zend_Pdf_Page
     */
    public function insertTotals($page, $invoice)
    {
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 17;
        return $page;
    }

    /**
     * Calculate Totals bloc height
     * @param $invoice
     * @return int
     */
    protected function calcTotalsHeight($invoice)
    {
        $height = 17;

        return $height;
    }
}
