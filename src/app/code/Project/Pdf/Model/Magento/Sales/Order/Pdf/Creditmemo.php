<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\System\Store;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Pdf\Helper\Config;

/**
 * Class Creditmemo
 * @package Project\Pdf\Model\Magento\Sales\Order\Pdf
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.CamelCaseMethodName)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class Creditmemo extends \Magento\Sales\Model\Order\Pdf\Creditmemo
{
    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Project\Pdf\Helper\Config
     */
    protected $helperConfig;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $appEmulation;

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\Order\Pdf\Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation,
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Pdf\Helper\Config $helperConfig
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        EnvironmentManager $environmentManager,
        Config $helperConfig,
        array $data = []
    ) {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $data
        );

        $this->environmentManager = $environmentManager;
        $this->helperConfig = $helperConfig;
    }

    /**
     * @param array $creditmemos
     * @return \Zend_Pdf
     * @throws \Zend_Pdf_Exception
     */
    public function getPdf($creditmemos = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('creditmemo');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($creditmemos as $creditmemo) {
            if ($creditmemo->getStoreId()) {
                $this->appEmulation->startEnvironmentEmulation(
                    $creditmemo->getStoreId(),
                    \Magento\Framework\App\Area::AREA_FRONTEND,
                    true
                );
                $this->_storeManager->setCurrentStore($creditmemo->getStoreId());
            }

            $page = $this->newPage();
            $order = $creditmemo->getOrder();

            /* Add image */
            $logoWidth = 135;
            $logoHeight = 101;
            $this->insertCustomLogo($page, $logoWidth, $logoHeight, $creditmemo->getStore());

            /* Add address */
            $this->insertAddress($page, $creditmemo->getStore());

            $this->y = min($this->y, $this->getY() - $logoHeight - 5);

            /* Add order */
            $this->insertOrder(
                $page,
                $creditmemo,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_CREDITMEMO_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );

            /* Add items */
            $this->insertItems($page, $creditmemo);

            /* line */
            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
            $page->drawLine(25, $this->y, 570, $this->y);
            $this->y -= 17;

            /* Add totals */
            $page = $this->insertTotals($page, $creditmemo);

            /* line */
            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
            $page->drawLine(25, $this->y, 570, $this->y);

            /* Add footer text */
            $textAfterGridLines = explode("\n", $this->helperConfig->getSalesPdfCreditmemoSupplier($order->getStoreId()));
            $textAfterGridLinesHeight = $this->calcTextAfterGridHeight($textAfterGridLines);
            $footerTextHeight = $this->calcFooterTextHeight($order);
            // phpcs:ignore
            if (count($textAfterGridLines) && ($this->y - $textAfterGridLinesHeight) < $footerTextHeight) {
                $this->insertFooterText($page, $order);
                $page = $this->newPage();
                $this->_setFontRegular($page);
                $this->y = 810;
            }

            if (!empty($textAfterGridLines)) {
                $this->_setFontRegular($page, 7);

                foreach ($textAfterGridLines as $line) {
                    $this->y -= 10;
                    $page->drawText(trim(strip_tags($line)), 26, $this->y, 'UTF-8');
                }
            }
            $this->insertFooterText($page, $order);
        }

        $this->_afterGetPdf();

        if ($creditmemo->getStoreId()) {
            $this->appEmulation->stopEnvironmentEmulation();
        }

        return $pdf;
    }

    /**
     * Insert logo to pdf page
     * @param \Zend_Pdf_Page &$page
     * @param int $width
     * @param int $height
     * @param null $store
     * @return void
     * @throws \Zend_Pdf_Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function insertCustomLogo(&$page, $width, $height, $store = null)
    {
        $this->y = 815;
        $image = $this->_scopeConfig->getValue(
            'sales/identity/logo',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );

        if ($image) {
            $imagePath = '/sales/store/logo/' . $image;
            if ($this->_mediaDirectory->isFile($imagePath)) {
                $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));

                $page->drawImage($image, 25, $this->y - $height, 25 + $width, $this->y);

                $this->y -= $height;
            }
        }
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param Store|null $store
     * @throws \Zend_Pdf_Exception
     */
    protected function insertAddress(&$page, $store = null)
    {
        $this->y = $this->getY();
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 7);
        $this->_setFontRegular($page, 8);

        $address = $this->_scopeConfig->getValue(
            'sales/identity/address',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );

        foreach (explode("\n", $address) as $value) {
            if ($value !== '') {
                $page->drawText(
                    trim(strip_tags($value)),
                    $this->getAlignRight(
                        $value,
                        -25,
                        $page->getWidth(),
                        $page->getFont(),
                        $page->getFontSize()
                    ),
                    $this->y,
                    'UTF-8'
                );

                $this->y -= 9;
            }
        }
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->environmentManager->getEnvironment() === Environment::US ? 745 : 795;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\Order $obj
     * @param bool $putOrderId
     * @throws \Zend_Pdf_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    // phpcs:disable
    protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        $shipment = null;
        $creditmemo = null;

        if ($obj instanceof Order) {
            $order = $obj;
        } elseif ($obj instanceof Order\Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        } elseif ($obj instanceof Order\Creditmemo) {
            $creditmemo = $obj;
            $order = $creditmemo->getOrder();
        }

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.3));
        $this->_setFontRegular($page, 9);

        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 15;

        if (null !== $creditmemo) {
            $page->drawText(
                sprintf(
                    "%s%s/%s%s",
                    mb_strtoupper(__('Creditmemo # ')),
                    $creditmemo->getIncrementId(),
                    mb_strtoupper(__('Creditmemo Date: ')),
                    $this->_localeDate->formatDate(
                        $this->_localeDate->scopeDate(
                            $creditmemo->getStore(),
                            \DateTime::createFromFormat('Y-m-d H:i:s', $creditmemo->getCreatedAt()),
                            true
                        ),
                        \IntlDateFormatter::MEDIUM,
                        false
                    )
                ),
                25,
                $this->y,
                'UTF-8'
            );

            $this->y -= 12;
        }

        if ($putOrderId) {
            $page->drawText(
                sprintf("%s%s", mb_strtoupper(__('Order # ')), $order->getIncrementId()),
                25,
                $this->y,
                'UTF-8'
            );

            $this->y -= 12;
        }

        $page->drawText(
            sprintf(
                "%s%s",
                mb_strtoupper(__('Order Date: ')),
                $this->_localeDate->formatDate(
                    $this->_localeDate->scopeDate(
                        $order->getStore(),
                        \DateTime::createFromFormat('Y-m-d H:i:s', $order->getCreatedAt()),
                        true
                    ),
                    \IntlDateFormatter::MEDIUM,
                    false
                )
            ),
            25,
            $this->y,
            'UTF-8'
        );

        $this->y -= 10;

        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 10;

        /* Billing Address */
        $billingAddress = $this->_formatAddress(
            $this->addressRenderer->format(
                $order->getBillingAddress(),
                'pdf'
            )
        );

        /* Shipping Address and Method */
        $shippingAddress = $this->_formatAddress(
            $this->addressRenderer->format(
                $order->getShippingAddress(),
                'pdf'
            )
        );
        $shippingMethod = $order->getShippingDescription();

        /* Payment */
        $paymentInfo = $this->_paymentData->getInfoBlock($order->getPayment())->setIsSecureMode(true)->toPdf();
        $payment = explode('{{pdf_row_separator}}', $paymentInfo);

        foreach ($payment as $key => $value) {
            if (strip_tags(trim($value)) == '') {
                unset($payment[$key]);
            }
        }
        reset($payment);

        $y = $this->y - (max(count($billingAddress), count($shippingAddress)) * 10 + 35);

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->drawRectangle(25, $this->y, 280, $y);
        $page->drawRectangle(290, $this->y, 570, $y);

        $this->y -= 15;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 8);

        $page->drawText(mb_strtoupper(__('Sold To:')), 35, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Ship To:')), 300, $this->y, 'UTF-8');

        $this->y -= 5;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawLine(35, $this->y, 270, $this->y);
        $page->drawLine(300, $this->y, 560, $this->y);

        $this->_setFontRegular($page);

        $lineCounter = 1;
        foreach ($billingAddress as $value) {
            if ($value !== '') {
                $page->drawText(
                    strip_tags(ltrim($value)),
                    35,
                    $this->y - 10 * $lineCounter,
                    'UTF-8'
                );
                $lineCounter++;
            }
        }

        $lineCounter = 1;
        foreach ($shippingAddress as $value) {
            if ($value !== '') {
                $page->drawText(
                    strip_tags(ltrim($value)),
                    300,
                    $this->y - 10 * $lineCounter,
                    'UTF-8'
                );
                $lineCounter++;
            }
        }

        $this->y = $y - 10;

        $y = $this->y - (max(count($payment), 2) * 10 + 35);

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->drawRectangle(25, $this->y, 280, $y);
        $page->drawRectangle(290, $this->y, 570, $y);

        $this->y -= 15;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 8);

        $page->drawText(mb_strtoupper(__('Payment Method:')), 35, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Shipment Method:')), 300, $this->y, 'UTF-8');

        $this->y -= 5;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawLine(35, $this->y, 270, $this->y);
        $page->drawLine(300, $this->y, 560, $this->y);

        $this->_setFontRegular($page);

        $lineCounter = 1;
        foreach ($payment as $value) {
            if (trim($value) !== '') {
                $page->drawText(strip_tags(trim($value)), 35, $this->y - 10 * $lineCounter, 'UTF-8');
                $lineCounter++;
            }
        }

        $shippingMethodArr = explode(',', $shippingMethod);
        $lineCounter = 1;
        foreach ($shippingMethodArr as $sm){
            $page->drawText($sm, 300, $this->y - 10 * $lineCounter, 'UTF-8');
            $lineCounter++;
        }

        $totalShippingChargesText = sprintf(
            "(%s %s)",
            __('Total Shipping Charges'),
            $order->formatPriceTxt($order->getShippingAmount())
        );

        $page->drawText($totalShippingChargesText, 300, $this->y - 10 * $lineCounter, 'UTF-8');

        $this->y = $y - 10;
        // phpcs:enable
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @throws \Zend_Pdf_Exception
     */
    protected function insertItems(&$page, $creditmemo)
    {
        $order = $creditmemo->getOrder();

        $this->y -= 15;

        /* Add table head */
        $this->drawItemsHeader($page);

        $this->y -= 15;

        foreach ($creditmemo->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            if ($this->y < 15) {
                $page = $this->newPage();
                $this->y = 800;
                $this->drawItemsHeader($page);
            }
            /* Draw item */
            $this->_drawItem($item, $page, $order);
        }
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @throws \Zend_Pdf_Exception
     */
    protected function drawItemsHeader(&$page)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.4));
        $page->drawText(mb_strtoupper(__('Product')), 24, $this->y, 'UTF-8');
        $page->drawText(__('HTS'), 105, $this->y, 'UTF-8');
        $page->drawText('SKU', 165, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Unit Price')), 205, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Tax Rate')), 260, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Price With Vat')), 315, $this->y, 'UTF-8');
        $page->drawText(__('QTY'), 370, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Total Without Tax')), 405, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Total Tax')), 486, $this->y, 'UTF-8');
        $page->drawText(mb_strtoupper(__('Total')), 540, $this->y, 'UTF-8');
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @throws \Zend_Pdf_Exception
     */
    protected function insertFooterText(&$page, $order)
    {
        $this->_setFontBold($page, 10);

        $firstLine = $this->helperConfig->getSalesPdfCreditMemoFooterFirstline($order->getStoreId());
        $this->y = max(80, $this->calcFooterTextHeight($order));

        $page->drawText(
            trim(strip_tags($firstLine)),
            $this->getAlignCenter($firstLine, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
            $this->y,
            'UTF-8'
        );

        $this->y -= 15;
        $this->_setFontBold($page, 7);

        foreach (explode("\n", $this->helperConfig->getSalesPdfCreditMemoFooterCore($order->getStoreId())) as $value) {
            if ($value !== '') {
                $page->drawText(
                    trim(strip_tags($value)),
                    $this->getAlignCenter($value, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
                    $this->y,
                    'UTF-8'
                );

                $this->y -= 10;
            }
        }

        $this->_setFontBold($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(1, 0, 0));
        $endLine = $this->helperConfig->getSalesPdfCreditMemoFooterEndline($order->getStoreId());
        $this->y -= 5;
        $page->drawText(
            trim(strip_tags($endLine)),
            $this->getAlignCenter($endLine, -18, $page->getWidth(), $page->getFont(), $page->getFontSize()),
            $this->y,
            'UTF-8'
        );
    }

    /**
     * Calculate extAfterGrid Height while ignoring empty line in the end of the paragraph
     * @param $textAfterGridLines
     * @return int
     */
    protected function calcTextAfterGridHeight($textAfterGridLines){
        $height = 0;
        $cleanArray = array_reverse($textAfterGridLines);
        foreach ($cleanArray as $line){
            if(!trim($line) && $height == 0){
                continue;
            }
            $height += 10;
        }
        return $height;
    }

    /**
     * Calculate bLoc Footer Height
     * @param $order
     * @return float|int
     */
    protected function calcFooterTextHeight($order)
    {
        $height = 25;
        $height += count(explode("\n", $this->helperConfig->getSalesPdfCreditMemoFooterCore($order->getStoreId()))) * 10;
        $height += 15;

        return $height;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param Store|null $store
     * @throws \Zend_Pdf_Exception
     */
    protected function insertAfterProductGridText(&$page, $store = null)
    {
        $textAfterProductGrid = $this->helperConfig
            ->getSalesPdfCreditmemoAfterProductGridText($store);

        if ($textAfterProductGrid) {
            $this->_setFontRegular($page, 7);
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

            foreach (explode("\n", $textAfterProductGrid) as $line) {
                $this->y -= 10;
                $page->drawText($line, 25, $this->y, 'UTF-8');
            }
        }
    }

    /**
     * Set font as regular
     *
     * @param  \Zend_Pdf_Page $object
     * @param  int $size
     *
     * @return \Zend_Pdf_Resource_Font
     * @throws \Zend_Pdf_Exception
     */
    // phpcs:ignore
    protected function _setFontRegular($object, $size = 7)
    {
        $font = \Zend_Pdf_Font::fontWithPath(
            $this->_rootDirectory->getAbsolutePath('lib/internal/LinLibertineFont/LinLibertineC_Re-2.8.0.ttf')
        );
        $object->setFont($font, $size);

        return $font;
    }
}
