<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf\Items\Creditmemo;

use Magento\Catalog\Model\ProductRepository;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;

/**
 * Class DefaultCreditmemo
 * @package Project\Pdf\Model\Magento\Sales\Order\Pdf\Items\Creditmemo
 * @author Synolia <contact@synolia.com>
 */
class DefaultCreditmemo extends \Magento\Sales\Model\Order\Pdf\Items\Creditmemo\DefaultCreditmemo
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Project\Core\Helper\AttributeSet
     */
    protected $attributeSetHelper;

    /**
     * protected $attributeSetHelper;
     * /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Project\Core\Helper\AttributeSet $attributeSetHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProductRepository $productRepository,
        AttributeSetHelper $attributeSetHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $taxData,
            $filesystem,
            $filterManager,
            $string,
            $resource,
            $resourceCollection,
            $data
        );

        $this->productRepository = $productRepository;
        $this->attributeSetHelper = $attributeSetHelper;
    }

    /**
     * Draw process
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function draw()
    {
        $order = $this->getOrder();
        $item = $this->getItem();
        $product = $this->productRepository->getById($item->getProductId());
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $lines = [];

        // draw Product name
        $lines[0] = [
            [
                'text' => $this->string->split($item->getName(), 20, true, true),
                'feed' => 25,
                'font_size' => 7,
            ],
        ];

        // draw HTS
        $originalHtsArray = explode(' ', $product->getCustomCode());
        $htsCode = $originalHtsArray[0];
        $lines[0][] = [
            'text' => $this->string->split($htsCode, 25),
            'feed' => 100,
            'font_size' => 7,
        ];

        // draw SKU
        $lines[0][] = [
            'text' => $this->string->split($this->getSku($item), 25),
            'feed' => 150,
            'font_size' => 7,
        ];

        // draw Price
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getPrice()),
            'feed' => 245,
            'font_size' => 7,
            'font' => 'bold',
            'align' => 'right',
        ];

        // draw Tax rate
        if ($item->getOrderItem()->getTaxPercent()) {
            $taxRateText =  sprintf('%s%%', $item->getOrderItem()->getTaxPercent() + 0);
        } else {
            $taxRateText =  '0%';
        }
        $lines[0][] = [
            'text' => $taxRateText,
            'feed' => 285,
            'font_size' => 7,
            'font' => 'bold',
            'align' => 'right',
        ];

        // draw Price with tax
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getPriceInclTax()),
            'feed' => 340,
            'font_size' => 7,
            'font' => 'bold',
            'align' => 'right',
        ];

        // draw QTY
        $lines[0][] = [
            'text' => $item->getQty() * 1,
            'feed' => 370,
            'font_size' => 7,
        ];

        // draw subtotal without tax
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getBasePrice() * $item->getQty()),
            'feed' => 410,
            'font_size' => 7,
            'font' => 'bold',
        ];

        // draw Tax
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getTaxAmount()),
            'feed' => 515,
            'font_size' => 7,
            'font' => 'bold',
            'align' => 'right',
        ];

        // draw Subtotal
        $lines[0][] = [
            'text' => $order->formatPriceTxt($item->getRowTotal() + $item->getTaxAmount() + $item->getDuty()),
            'feed' => 565,
            'font_size' => 7,
            'font' => 'bold',
            'align' => 'right',
        ];

        if ($this->attributeSetHelper->isBeauty($product)) {
            $lines[][] = [
                'text' => mb_strtoupper(__('Volume')),
                'feed' => 35,
                'font_size' => 7,
            ];

            $lines[][] = [
                'text' => sprintf(
                    "%s",
                    $product->getVolume()
                ),
                'feed' => 40,
                'font_size' => 7,
            ];
        } else {
            // custom options
            $options = $this->getItemOptions();

            if ($options) {
                foreach ($options as $option) {
                    // draw options label
                    $lines[][] = [
                        'text' => $this->string->split(
                            $this->filterManager->stripTags($option['label']),
                            70,
                            true,
                            true
                        ),
                        'font' => 'italic',
                        'feed' => 35,
                        'font_size' => 7,
                    ];

                    if ($option['value']) {
                        $printValue = isset(
                            $option['print_value']
                        ) ? $option['print_value'] : $this->filterManager->stripTags(
                            $option['value']
                        );

                        $values = explode(', ', $printValue);
                        foreach ($values as $value) {
                            $lines[][] = [
                                'text' => $this->string->split($value, 50, true, true),
                                'feed' => 40,
                                'font_size' => 7,
                            ];
                        }
                    }
                }
            }
        }

        $lineBlock = [
            'lines' => $lines,
            'height' => 10,
        ];

        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }
}
