<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf\Items\Invoice;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Directory\Model\Currency;
use Magento\Eav\Model\AttributeRepository;
use Magento\Eav\Model\Config;
use Magento\Sales\Model\Order;
use Project\Pdf\Helper\Config\Invoice\Details;
use Project\Pdf\Helper\Config\Invoice\ProductInformation;
use Project\Core\Helper\AttributeSet as AttributeSetHelper;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Magento\Sales\Model\Order\Creditmemo\Item as CreditmemoItem;

/**
 * Class DefaultInvoice
 *
 * @package Project\Pdf\Model\Magento\Sales\Order\Pdf\Items\Invoice
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class DefaultInvoice extends \Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var ProductInformation
     */
    protected $productInfoHelper;

    /**
     * @var Details
     */
    protected $detailsHelper;

    /**
     * @var AttributeSetHelper
     */
    protected $attributeSetHelper;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var Config
     */
    protected $eavConfig;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $configurableResourceModel;

    /**
     * @var \Synolia\DutyCalculator\Helper\TaxData
     */
    protected $taxDataHelper;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param ProductRepository $productRepository
     * @param ProductInformation $productInfoHelper
     * @param Details $detailsHelper
     * @param AttributeSetHelper $attributeSetHelper
     * @param Currency $currency
     * @param Config $eavConfig
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableResourceModel
     * @param \Synolia\DutyCalculator\Helper\TaxData $taxDataHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProductRepository $productRepository,
        ProductInformation $productInfoHelper,
        Details $detailsHelper,
        AttributeSetHelper $attributeSetHelper,
        Currency $currency,
        Config $eavConfig,
        AttributeRepository $attributeRepository,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableResourceModel,
        \Synolia\DutyCalculator\Helper\TaxData $taxDataHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $taxData,
            $filesystem,
            $filterManager,
            $string,
            $resource,
            $resourceCollection,
            $data
        );

        $this->productRepository = $productRepository;
        $this->productInfoHelper = $productInfoHelper;
        $this->detailsHelper = $detailsHelper;
        $this->attributeSetHelper = $attributeSetHelper;
        $this->currency = $currency;
        $this->eavConfig = $eavConfig;
        $this->attributeRepository = $attributeRepository;
        $this->configurableResourceModel = $configurableResourceModel;
        $this->taxDataHelper = $taxDataHelper;
    }

    /**
     * Draw item line
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function draw()
    {
        $order = $this->getOrder();
        $item = $this->getItem();
        $product = $this->productRepository->getById($item->getProductId());

        if ($product->getTypeId() === "simple") {
            if ($product->getParentProduct()) {
                $product = $product->getParentProduct();
            }
        }

        $page = $this->getPage();
        $lines = [];

        $headerConfig = $this->getPdf()->getItemsHeaderConfiguration($item->getInvoice(), 25, 570);

        if (isset($headerConfig['return_reason'])) {
            $attribute = $this->attributeRepository->get('rma_item', 'resolution');
            $reasons = $attribute->getOptions();
            $position = $headerConfig['return_reason']['position'];
            $counter = 0;

            foreach ($reasons as $reason) {
                if (!$reason->getLabel()) {
                    continue;
                }

                $this->getPdf()->insertCheckbox($page, $position, $this->getPdf()->y - 2 - (10 * $counter));

                $lines[$counter++][] = array(
                    'text' => $reason->getLabel(),
                    'feed' => $position + 10
                );
            }

            if (isset($headerConfig['line_checkbox'])) {
                $position = $headerConfig['line_checkbox']['position'];
                $this->getPdf()->insertCheckbox($page, $position, $this->getPdf()->y + 3 - (10 * ($counter / 2)), 11);
            }
        }

        $this->drawInformations($headerConfig, $product, $lines);
        $this->drawDetails($headerConfig, $product, $item, $lines, $order);
        $this->drawColumns($headerConfig, $item, $lines, $order);

        foreach ($lines as &$line) {
            foreach ($line as &$entry) {
                $entry['font_size'] = 7;
            }
        }

        $lineBlock = [
            'lines' => $lines,
            'height' => 10,
        ];

        $page = $this->getPdf()->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }

    /**
     * @param array $headerConfig
     * @param Product $product
     * @param array $lines
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function drawInformations($headerConfig, $product, &$lines)
    {
        if (isset($headerConfig['informations'])) {
            $position = $headerConfig['informations']['position'];
            $counter = 0;

            if ($this->productInfoHelper->hasName()) {
                $nameLines = $this->strSplit($product->getName(), 20);

                foreach ($nameLines as $nameLine) {
                    $lines[$counter++][] = [
                        'text' => $nameLine,
                        'feed' => $position,
                    ];
                }
            }

            if ($this->productInfoHelper->hasColor()) {
                $lines[$counter++][] = [
                    'text' => \mb_strtoupper(__('Color: ')),
                    'feed' => $position,
                ];
                $attribute = $this->eavConfig->getAttribute('catalog_product', 'color');
                $lines[$counter++][] = [
                    'text' => '  '.$attribute->getSource()->getOptionText($product->getColor()),
                    'feed' => $position,
                ];
            }

            if ($this->productInfoHelper->hasOrigin()) {
                if ($product->getProductCountryOrigin()) {
                    $lines[$counter++][] = [
                        'text' => \mb_strtoupper(__('Origin: ')) . $product->getProductCountryOrigin(),
                        'feed' => $position,
                    ];
                }
            }

            if ($this->productInfoHelper->hasSize()) {
                $options = $this->getItemOptions();

                $lines[$counter++][] = [
                    'text' => \mb_strtoupper(__($this->productInfoHelper->hasSizeLabel($product))).':',
                    'feed' => $position,
                ];

                if ($this->attributeSetHelper->isBeauty($product)) {
                    $lines[$counter++][] = [
                        'text' => $product->getBeautyVolume() ,
                        'feed' => $position + 5,
                    ];
                } else {
                    if ($options) {
                        $this->getOptionValues($lines, $counter, $options, $position);
                    }
                }
            }
        }
    }

    /**
     * @param array $lines
     * @param int $counter
     * @param array $options
     * @param int $position
     */
    protected function getOptionValues(&$lines, &$counter, $options, $position)
    {
        foreach ($options as $option) {
            if ($option['value']) {
                $printValue = isset($option['print_value']) ? $option['print_value'] : strip_tags($option['value']);
                $values = explode(', ', $printValue);
                foreach ($values as $value) {
                    $lines[$counter++][] = [
                        'text' => $value,
                        'feed' => $position + 5,
                    ];
                }
            }
        }
    }

    /**
     * @param string $string
     * @param int $length
     *
     * @return array
     */
    protected function strSplit($string, $length)
    {
        $arrayOutput = [];
        $arrayWords = explode(' ', $string);
        $currentLength = 0;
        $index = 0;

        foreach ($arrayWords as $word) {
            $wordLength = strlen($word) + 1;

            if (($currentLength + $wordLength) <= $length) {
                if (isset($arrayOutput[$index])) {
                    $arrayOutput[$index] .= ' ' . $word;
                } else {
                    $arrayOutput[$index] = $word;
                }

                $currentLength += $wordLength;
            } else {
                $index++;
                $currentLength = $wordLength;
                $arrayOutput[$index] = $word;
            }
        }

        return $arrayOutput;
    }

    /**
     * @param array $headerConfig
     * @param Product $product
     * @param $item
     * @param array $lines
     * @param Order $order
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function drawDetails($headerConfig, $product, $item, &$lines, $order)
    {
        if (isset($headerConfig['details'])) {
            $counter = 0;
            $position = $headerConfig['details']['position'];

            if ($this->detailsHelper->hasHtsCode()) {
                $lines[$counter++][] = [
                    'text' => __('HTS Code: ') . $product->getCustomCode(),
                    'feed' => $position,
                ];
            }

            if ($this->detailsHelper->hasCustomCode()) {
                $lines[$counter++][] = [
                    'text' => __('Custom Code: ') . $product->getCustomCode(),
                    'feed' => $position,
                ];
            }

            if ($this->detailsHelper->hasSku()) {
                $lines[$counter++][] = [
                    'text' => __('SKU: ') . $item->getSku(),
                    'feed' => $position,
                ];
            }

            if ($this->detailsHelper->hasConfigurableSku()) {
                $lines[$counter++][] = [
                    'text' => \mb_strtoupper(__('CONFIGURABLE SKU:')),
                    'feed' => $position,
                ];

                $lines[$counter++][] = [
                    'text' => $product->getSku(),
                    'feed' => $position,
                ];
            }

            if ($this->detailsHelper->hasUnitPrice() && $this->getPdf()->displayPrices()) {
                $lines[$counter++][] = [
                    'text' => __('Unit price: ') . $order->formatPriceTxt($item->getPrice()),
                    'feed' => $position,
                ];
            }

            if ($product->getData('material_product_page')) {
                $lines[$counter++][] = [
                    'text' => __('Composition: '),
                    'feed' => $position,
                ];

                $compositionLines = explode('/', $product->getData('material_product_page'));
                foreach ($compositionLines as $compositionLine) {
                    $lines[$counter++][] = [
                        'text' => trim($compositionLine),
                        'feed' => $position,
                    ];
                }
            }
        }
    }

    /**
     * @param array $headerConfig
     * @param CreditmemoItem|InvoiceItem $item
     * @param array $lines
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function drawColumns($headerConfig, $item, &$lines, $order)
    {
        if (isset($headerConfig['qty'])) {
            $lines[0][] = [
                'text' => $item->getQty() * 1,
                'feed' => $headerConfig['qty']['position'],
            ];
        }

        if (isset($headerConfig['unit_price'])) {
            if ($this->getPdf()->getInvoiceType() === 'custom_return_proforma_invoice') {
                $price = $item->getBaseRowTotal();
            } else {
                $price = $item->getPrice();
            }

            $lines[0][] = [
                'text' => $order->formatPriceTxt($price),
                'feed' => $headerConfig['unit_price']['position'] + $headerConfig['unit_price']['length'] - 4,
                'font' => 'bold',
                'align' => 'right',
            ];
        }

        if (isset($headerConfig['tax_rate'])) {
            if ($item->getOrderItem()->getTaxPercent()) {
                $taxRateText =  sprintf('%s%%', $item->getOrderItem()->getTaxPercent() + 0);
            } else {
                $taxRateText =  '0%';
            }
            $lines[0][] = [
                'text' => $taxRateText,
                'feed' => $headerConfig['tax_rate']['position'] + 10,
                'font' => 'bold',
                'align' => 'left',
            ];
        }

        if (isset($headerConfig['price_with_vat'])) {
            $lines[0][] = [
                'text' => $order->formatPriceTxt($item->getPriceInclTax()),
                'feed' => $headerConfig['price_with_vat']['position'],
                'font' => 'bold',
                'align' => 'left',
            ];
        }

        if (isset($headerConfig['total_without_tax'])) {
            $lines[0][] = [
                'text' => $order->formatPriceTxt($item->getBasePrice() * $item->getQty()),
                'feed' => $headerConfig['total_without_tax']['position'],
                'font' => 'bold',
            ];
        }

        if (isset($headerConfig['total_tax'])) {
            $lines[0][] = [
                'text' => $order->formatPriceTxt($item->getTaxAmount()),
                'feed' => $headerConfig['total_tax']['position'],
                'font' => 'bold',
                'align' => 'left',
            ];
        }

        if (isset($headerConfig['duty'])) {
            $lines[0][] = [
                'text' => $order->formatPriceTxt($this->taxDataHelper->getDutyByOrderItemId($item->getOrderItemId())),
                'feed' => $headerConfig['duty']['position'],
                'font' => 'bold',
            ];
        }

        if (isset($headerConfig['shipping_fee'])) {
            $lines[0][] = [
                'text' => $order->formatPriceTxt($item->getShippingFee()),
                'feed' => $headerConfig['shipping_fee']['position'],
                'font' => 'bold',
            ];
        }

        if (isset($headerConfig['total'])) {
            $lines[0][] = [
                'text' => $order->formatPriceTxt(
                    $item->getRowTotal() + $item->getTaxAmount() + $item->getDuty() + $item->getShippingFee()
                ),
                'feed' => $headerConfig['total']['position'] + $headerConfig['total']['length'],
                'font' => 'bold',
                'align' => 'right',
            ];
        }
    }
}
