<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Email\Sender;

use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Email\Container\ShipmentIdentity;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Pdf\InvoiceFactory as InvoicePdfFactory;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\ResourceModel\Order\Shipment as ShipmentResource;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\Event\ManagerInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoiceFactory;

/**
 * Class ShipmentSender
 *
 * @package Project\Pdf\Model\Order\Email\Sender
 * @author Synolia <contact@synolia.com>
 */
class ShipmentSender extends \Magento\Sales\Model\Order\Email\Sender\ShipmentSender
{
    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * @var InvoicePdfFactory
     */
    protected $invoicePdfFactory;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoiceFactory
     */
    protected $giftInvoiceFactory;

    /**
     * ShipmentSender constructor.
     * @param \Magento\Sales\Model\Order\Email\Container\Template $templateContainer
     * @param \Magento\Sales\Model\Order\Email\Container\ShipmentIdentity $identityContainer
     * @param \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment $shipmentResource
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Sales\Model\Order\InvoiceRepository $invoiceRepository
     * @param \Magento\Sales\Model\Order\Pdf\InvoiceFactory $invoicePdfFactory
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Pdf\Model\Magento\Sales\Order\Pdf\GiftInvoiceFactory $giftInvoiceFactory
     */
    public function __construct(
        Template $templateContainer,
        ShipmentIdentity $identityContainer,
        \Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        Renderer $addressRenderer,
        PaymentHelper $paymentHelper,
        ShipmentResource $shipmentResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        ManagerInterface $eventManager,
        InvoiceRepository $invoiceRepository,
        InvoicePdfFactory $invoicePdfFactory,
        EnvironmentManager $environmentManager,
        GiftInvoiceFactory $giftInvoiceFactory
    ) {
        parent::__construct(
            $templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer,
            $paymentHelper,
            $shipmentResource,
            $globalConfig,
            $eventManager
        );

        $this->invoiceRepository = $invoiceRepository;
        $this->invoicePdfFactory = $invoicePdfFactory;
        $this->environmentManager = $environmentManager;
        $this->giftInvoiceFactory = $giftInvoiceFactory;
    }

    /**
     * @param Shipment $shipment
     * @param bool $forceSyncMode
     *
     * @return bool
     * @throws \Exception
     */
    public function send(Shipment $shipment, $forceSyncMode = false)
    {
        $shipment->setSendEmail(true);

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            $order = $shipment->getOrder();

            $attachment = null;

            if ($this->environmentManager->getEnvironment() != Environment::JP) {
                foreach ($order->getInvoiceCollection() as $lazyInvoice) {
                    $invoice = $this->invoiceRepository->get($lazyInvoice->getId());
                    if ($this->isGiftOrder($order)) {
                        $pdf = $this->giftInvoiceFactory->create()->getPdf([$invoice]);
                    } else {
                        $pdf = $this->invoicePdfFactory->create()->getPdf([$invoice]);
                    }

                    $attachment = $pdf->render();
                }
            }

            $transport = [
                'order' => $order,
                'shipment' => $shipment,
                'comment' => $shipment->getCustomerNoteNotify() ? $shipment->getCustomerNote() : '',
                'billing' => $order->getBillingAddress(),
                'payment_html' => $this->getPaymentHtml($order),
                'store' => $order->getStore(),
                'formattedShippingAddress' => $this->getFormattedShippingAddress($order),
                'formattedBillingAddress' => $this->getFormattedBillingAddress($order),
                'attachment' => $attachment
            ];

            $this->eventManager->dispatch(
                'email_shipment_set_template_vars_before',
                ['sender' => $this, 'transport' => $transport]
            );

            $this->templateContainer->setTemplateVars($transport);

            if ($this->checkAndSend($order)) {
                $shipment->setEmailSent(true);
                $this->shipmentResource->saveAttribute($shipment, ['send_email', 'email_sent']);

                return true;
            }
        } else {
            $shipment->setEmailSent(null);
            $this->shipmentResource->saveAttribute($shipment, 'email_sent');
        }

        $this->shipmentResource->saveAttribute($shipment, 'send_email');

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return bool
     */
    protected function isGiftOrder($order)
    {
        return ($order->getGiftMessageId() || $order->getGwId() || $order->getGwItemsBasePriceInclTax());
    }
}
