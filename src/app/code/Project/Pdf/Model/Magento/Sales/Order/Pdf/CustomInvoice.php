<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf;

use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\Region;
use Magento\Sales\Model\Order;
use Project\Pdf\Helper\Config;
use Project\Pdf\Helper\Config\Invoice\Others;
use Project\Pdf\Helper\Config\Invoice\PaymentMethod;
use Project\Pdf\Helper\Config\Invoice\Totals;
use Project\Pdf\Helper\Config\CustomInvoice\Others as CustomOtherHelper;
use Project\Pdf\Helper\Config\CustomInvoice\PaymentMethod as CustomPaymentHelper;
use Project\Pdf\Helper\Config\CustomInvoice\Totals as CustomTotalsHelper;

/**
 * Class CustomInvoice
 *
 * @package Project\Pdf\Model\Magento\Sales\Order\Pdf
 * @author Synolia <contact@synolia.com>
 */
class CustomInvoice extends \Project\Pdf\Model\Magento\Sales\Order\Pdf\Invoice
{
    protected $invoiceType = 'custom_invoice';
    protected $documentName = 'Custom Invoice';

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\Order\Pdf\Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation,
     * @param \Project\Pdf\Helper\Config\Invoice\PaymentMethod $paymentMethodHelper
     * @param \Magento\Directory\Model\Region $region
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Project\Pdf\Helper\Config\Invoice\Totals $totalsHelper
     * @param \Project\Pdf\Helper\Config\Invoice\Others $othersHelper
     * @param \Project\Pdf\Helper\Config $configHelper
     * @param \Project\Pdf\Helper\Config\CustomInvoice\Totals $customTotalsHelper
     * @param \Project\Pdf\Helper\Config\CustomInvoice\Others $customOthersHelper
     * @param \Project\Pdf\Helper\Config\CustomInvoice\PaymentMethod $customPaymentMethodHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        PaymentMethod $paymentMethodHelper,
        Region $region,
        RegionFactory $regionFactory,
        Totals $totalsHelper,
        Others $othersHelper,
        Config $configHelper,
        CustomTotalsHelper $customTotalsHelper,
        CustomOtherHelper $customOthersHelper,
        CustomPaymentHelper $customPaymentMethodHelper,
        array $data = []
    ) {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $paymentMethodHelper,
            $region,
            $regionFactory,
            $totalsHelper,
            $othersHelper,
            $configHelper,
            $data
        );

        $this->paymentMethodHelper = $customPaymentMethodHelper;
        $this->othersHelper = $customOthersHelper;
        $this->totalsHelper = $customTotalsHelper;
    }

    /**
     * @return \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal[]
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    //phpcs:ignore
    protected function _getTotalsList()
    {
        $totals = parent::_getTotalsList();

        foreach ($totals as $total) {
            if ($total->getTitle() === 'Subtotal') {
                $total->setTitle(__('Declared Value'));
            }
        }

        return $totals;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function preparePaymentBlockLines($order)
    {
        return [];
    }
}
