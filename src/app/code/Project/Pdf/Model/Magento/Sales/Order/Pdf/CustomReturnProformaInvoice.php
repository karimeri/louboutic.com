<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf;

use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\Region;
use Magento\Sales\Model\Order;
use Project\Pdf\Helper\Config;
use Project\Pdf\Helper\Config\Invoice\Others;
use Project\Pdf\Helper\Config\Invoice\PaymentMethod;
use Project\Pdf\Helper\Config\Invoice\Totals;
use Project\Pdf\Helper\Config\CustomReturnProformaInvoice\Others as CustomOtherHelper;
use Project\Pdf\Helper\Config\CustomReturnProformaInvoice\PaymentMethod as CustomPaymentHelper;
use Project\Pdf\Helper\Config\CustomReturnProformaInvoice\Totals as CustomTotalsHelper;

/**
 * Class CustomReturnProformaInvoice
 * @package Project\Pdf\Model\Magento\Sales\Order\Pdf
 * @author Synolia <contact@synolia.com>
 */
class CustomReturnProformaInvoice extends \Project\Pdf\Model\Magento\Sales\Order\Pdf\Invoice
{
    protected $invoiceType = 'custom_return_proforma_invoice';
    protected $documentName = 'Custom Return Proforma Invoice';

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\Order\Pdf\Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation,
     * @param \Project\Pdf\Helper\Config\Invoice\PaymentMethod $paymentMethodHelper
     * @param \Magento\Directory\Model\Region $region
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Project\Pdf\Helper\Config\Invoice\Totals $totalsHelper
     * @param \Project\Pdf\Helper\Config\Invoice\Others $othersHelper
     * @param \Project\Pdf\Helper\Config $configHelper
     * @param \Project\Pdf\Helper\Config\CustomReturnProformaInvoice\Totals $customTotalsHelper
     * @param \Project\Pdf\Helper\Config\CustomReturnProformaInvoice\Others $customOthersHelper
     * @param \Project\Pdf\Helper\Config\CustomReturnProformaInvoice\PaymentMethod $customPaymentMethodHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        PaymentMethod $paymentMethodHelper,
        Region $region,
        RegionFactory $regionFactory,
        Totals $totalsHelper,
        Others $othersHelper,
        Config $configHelper,
        CustomTotalsHelper $customTotalsHelper,
        CustomOtherHelper $customOthersHelper,
        CustomPaymentHelper $customPaymentMethodHelper,
        array $data = []
    ) {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $paymentMethodHelper,
            $region,
            $regionFactory,
            $totalsHelper,
            $othersHelper,
            $configHelper,
            $data
        );

        $this->paymentMethodHelper = $customPaymentMethodHelper;
        $this->othersHelper = $customOthersHelper;
        $this->totalsHelper = $customTotalsHelper;
    }

    /**
     * @param Order $order
     * @return array
     */
    protected function prepareShippingAddressBlock($order)
    {
        $shippingAddress = [];
        $returnTo = $this->configHelper->getSalesPdfCustomReturnProformaInvoiceReturnTo($order->getStoreId());

        foreach (explode("\n", $returnTo) as $value) {
            if ($value !== '') {
                $shippingAddress[] = $value;
            }
        }

        return $shippingAddress;
    }

    /**
     * @param Order $order
     * @return array
     */
    protected function preparePaymentBlockLines($order)
    {
        $lines = [];

        if ($airwayBillNUmber = $order->getAirwayBillNumber()) {
            $lines[] = __('Airway Bill Number') . ": " . $airwayBillNUmber;
        } else {
            $lines[] = '';
        }

        return $lines;
    }

    /**
     * @param Order $order
     * @param bool $withTotalCharges
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function prepareShipmentBlockLines($order, $withTotalCharges = true)
    {
        $lines = [];

        $reasonForExport = $this->configHelper
            ->getSalesPdfCustomReturnProformaInvoiceReasonForExport($order->getStoreId());

        if ($reasonForExport) {
            $lines[] = $reasonForExport;
        }

        return $lines;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param Order $order
     * @return int
     * @throws \Zend_Pdf_Exception
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    protected function insertMessage(&$page, $order)
    {
        $message = $this->configHelper->getSalesPdfCustomReturnProformaInvoiceTextBeforeProducts($order->getStoreId());

        if ($message) {
            $y = $this->y - 10;

            $this->_setFontRegular($page);

            $page->drawText(
                $message,
                $this->getAlignCenter(
                    $message,
                    20,
                    $page->getWidth() - 40,
                    $page->getFont(),
                    $page->getFontSize()
                ),
                $y
            );

            return 15;
        }

        return 0;
    }

    /**
     * @param $page
     * @param $invoice
     */
    protected function insertProducts(&$page, $invoice)
    {
        // do nothing in this invoice
    }

    /**
     * @param Invoice $invoice
     * @param $startPosition
     * @param $tableWidth
     * @param array $itemsHeader
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * phpcs:disable
     */
    public function getItemsHeaderConfiguration($invoice, $startPosition, $tableWidth, $itemsHeader = [])
    {
        if ($this->hasMoreThanOneItem($invoice)) {
            $itemsHeader['line_checkbox'] = [
                'label' => '',
                'length' => 20,
            ];
        }
        $itemsHeader['return_reason'] = [
            'label' => mb_strtoupper(__('Return Reason')),
            'length' => 60,
        ];

        return parent::getItemsHeaderConfiguration($invoice, $startPosition, $tableWidth, $itemsHeader);
    }

    /**
     * @param Invoice $invoice
     * @return bool
     */
    protected function hasMoreThanOneItem($invoice)
    {
        $counter = 0;
        foreach ($invoice->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            $counter++;
        }

        return $counter > 1;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param Invoice $invoice
     * @return \Zend_Pdf_Page
     * @throws \Zend_Pdf_Exception
     */
    public function insertTotals($page, $invoice)
    {
        $counter = 0;
        foreach ($invoice->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            $counter += $item->getQty();
        }

        if ($counter > 1) {
            $this->y -= 20;
            $blockLength = 180;

            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
            $page->drawRectangle(570 - $blockLength, $this->y, 570, $this->y - 50);

            $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
            $this->_setFontRegular($page, 8);

            $this->y -= 15;
            $text = __('To be filled by Customer Service:');
            $page->drawText(
                $text,
                $this->getAlignCenter(
                    $text,
                    580 - $blockLength,
                    $blockLength - 20,
                    $page->getFont(),
                    $page->getFontSize()
                ),
                $this->y,
                'UTF-8'
            );

            $this->y -= 5;
            $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
            $page->setLineWidth(0.5);
            $page->drawLine(580 - $blockLength, $this->y, 560, $this->y);

            $this->y -= 10;
            $text = __('Total Qty') . ' : _______';
            $page->drawText(
                $text,
                $this->getAlignRight(
                    $text,
                    580 - $blockLength,
                    $blockLength - 20,
                    $page->getFont(),
                    $page->getFontSize()
                ),
                $this->y,
                'UTF-8'
            );

            $this->y -= 10;
            $text = __('Total value') . ' <' . $invoice->getOrder()->getOrderCurrencyCode() . '> : _______';
            $page->drawText(
                $text,
                $this->getAlignRight(
                    $text,
                    580 - $blockLength,
                    $blockLength - 20,
                    $page->getFont(),
                    $page->getFontSize()
                ),
                $this->y,
                'UTF-8'
            );
        }

        return $page;
    }

    /**
     * @return string
     */
    protected function getBillingAddressBlockTitle()
    {
        return mb_strtoupper(__('Sent From / Original Recipient:'));
    }

    /**
     * @return string
     */
    protected function getShippingAddressBlockTitle()
    {
        return mb_strtoupper(__('Return To:'));
    }

    /**
     * @return string
     */
    protected function getPaymentBlockTitle()
    {
        return mb_strtoupper(__('Original Outbound Airway Bill:'));
    }

    /**
     * @return string
     */
    protected function getShipmentBlockTitle()
    {
        return mb_strtoupper(__('Reason For Export'));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @throws \Zend_Pdf_Exception
     */
    protected function insertFooterText(&$page, $order)
    {
        $this->y = 110;
        $this->_setFontBold($page, 10);

        $firstLine = $this->configHelper->getSalesPdfCustomReturnProformaInvoiceFooterFirstline($order->getStoreId());
        $page->drawText(
            trim(strip_tags($firstLine)),
            $this->getAlignCenter($firstLine, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
            $this->y,
            'UTF-8'
        );

        $this->y -= 15;
        $this->_setFontBold($page, 7);

        foreach (explode("\n", $this->configHelper->getSalesPdfCustomReturnProformaInvoiceFooterCore($order->getStoreId())) as $value) {
            if ($value !== '') {
                $page->drawText(
                    trim(strip_tags($value)),
                    $this->getAlignCenter($value, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
                    $this->y,
                    'UTF-8'
                );

                $this->y -= 10;
            }
        }

        $this->_setFontBold($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(1, 0, 0));
        $endLine = $this->configHelper->getSalesPdfCustomReturnProformaInvoiceFooterEndline($order->getStoreId());
        $page->drawText(
            trim(strip_tags($endLine)),
            $this->getAlignCenter($endLine, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
            $this->y,
            'UTF-8'
        );
    }
}
