<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Email;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Mail\Template\TransportBuilderByStore;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\SenderBuilder as BaseSenderBuilder;
use Project\Pdf\Magento\Mail\Template\TransportBuilder as CustomTransportBuilder;
use Zend_Mime;

/**
 * Class SenderBuilder
 *
 * @package Project\Pdf\Model\Order\Email
 * @author Synolia <contact@synolia.com>
 */
class SenderBuilder extends BaseSenderBuilder
{
    /**
     * Prepare and send email message
     *
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    public function send()
    {
        $this->configureEmailTemplate();

        $this->transportBuilder->addTo(
            $this->identityContainer->getCustomerEmail(),
            $this->identityContainer->getCustomerName()
        );

        if (isset($this->templateContainer->getTemplateVars()['attachment'])) {
            /** @var Order $order */
            $order = $this->templateContainer->getTemplateVars()['order'];
            $this->transportBuilder->addAttachment(
                $this->templateContainer->getTemplateVars()['attachment'],
                sprintf('%s-%s.pdf', __('invoice'), $order->getIncrementId()),
                'application/pdf'
            );
        }

        $copyTo = $this->identityContainer->getEmailCopyTo();

        if (!empty($copyTo) && $this->identityContainer->getCopyMethod() == 'bcc') {
            foreach ($copyTo as $email) {
                $this->transportBuilder->addBcc($email);
            }
        }

        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }
}
