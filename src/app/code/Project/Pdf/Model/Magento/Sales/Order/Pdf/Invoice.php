<?php

namespace Project\Pdf\Model\Magento\Sales\Order\Pdf;

use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\Region;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection;
use Project\Pdf\Helper\Config;
use Project\Pdf\Helper\Config\Invoice\Others;
use Project\Pdf\Helper\Config\Invoice\PaymentMethod;
use Project\Pdf\Helper\Config\Invoice\Totals;

/**
 * Class Invoice
 *
 * @package Project\Pdf\Model\Magento\Sales\Order\Pdf
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.CamelCaseMethodName)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class Invoice extends \Magento\Sales\Model\Order\Pdf\Invoice
{
    protected $invoiceType = 'commercial_invoice';
    protected $documentName = 'Commercial Invoice';

    /**
     * @var PaymentMethod
     */
    protected $paymentMethodHelper;

    /**
     * @var Others
     */
    protected $othersHelper;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var Totals
     */
    protected $totalsHelper;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Region
     */
    protected $region;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $regionFactory;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $appEmulation;

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\Order\Pdf\Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation,
     * @param \Project\Pdf\Helper\Config\Invoice\PaymentMethod $paymentMethodHelper
     * @param \Magento\Directory\Model\Region $region
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Project\Pdf\Helper\Config\Invoice\Totals $totalsHelper
     * @param \Project\Pdf\Helper\Config\Invoice\Others $othersHelper
     * @param \Project\Pdf\Helper\Config $configHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        PaymentMethod $paymentMethodHelper,
        Region $region,
        RegionFactory $regionFactory,
        Totals $totalsHelper,
        Others $othersHelper,
        Config $configHelper,
        array $data = []
    ) {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $data
        );

        $this->paymentMethodHelper = $paymentMethodHelper;
        $this->othersHelper = $othersHelper;
        $this->configHelper = $configHelper;
        $this->totalsHelper = $totalsHelper;
        $this->region = $region;
        $this->regionFactory = $regionFactory;
    }

    /**
     * @return string
     */
    protected function getBillingAddressBlockTitle()
    {
        return mb_strtoupper(__('Sold To:'));
    }

    /**
     * @return string
     */
    protected function getShippingAddressBlockTitle()
    {
        return mb_strtoupper(__('Ship To: '));
    }

    /**
     * @return string
     */
    protected function getPaymentBlockTitle()
    {
        return mb_strtoupper(__('Payment Method:'));
    }

    /**
     * @return string
     */
    protected function getShipmentBlockTitle()
    {
        return mb_strtoupper(__('Shipment Method:'));
    }

    /**
     * Set font as regular
     *
     * @param  \Zend_Pdf_Page $object
     * @param  int $size
     *
     * @return \Zend_Pdf_Resource_Font
     * @throws \Zend_Pdf_Exception
     */
    // phpcs:ignore
    protected function _setFontRegular($object, $size = 7)
    {
        $font = \Zend_Pdf_Font::fontWithPath(
            $this->_rootDirectory->getAbsolutePath('lib/internal/LinLibertineFont/LinLibertineC_Re-2.8.0.ttf')
        );
        $object->setFont($font, $size);

        return $font;
    }

    /**
     * Return PDF document
     *
     * @param array|Collection $invoices
     *
     * @return \Zend_Pdf
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPdf($invoices = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                $this->appEmulation->startEnvironmentEmulation(
                    $invoice->getStoreId(),
                    \Magento\Framework\App\Area::AREA_FRONTEND,
                    true
                );
                $this->_storeManager->setCurrentStore($invoice->getStoreId());
            }

            $page = $this->newPage();
            $order = $invoice->getOrder();

            $this->insertCustomLogo($page, 135, 101, $invoice->getStore());
            $this->insertAddress($page, $invoice->getStore());
            $this->insertOrder(
                $page,
                $invoice,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );

            /* Add message */
            $this->y -= $this->insertMessage($page, $order);

            $this->insertItems($page, $invoice);
            $insertTotals = true;
            if ($this->invoiceType === 'commercial_invoice' || $this->invoiceType === 'gift_invoice') {
                $textAfterGridLines = explode("\n", $this->configHelper->getSalesPdfInvoiceSupplier($order->getStoreId()));
                $textAfterGridLinesHeight = $this->calcTextAfterGridHeight($textAfterGridLines);
                $footerTextHeight = $this->calcFooterTextHeight($order);
                $calcTotalsHeight = $this->calcTotalsHeight($invoice);
                // phpcs:ignore
                if (count($textAfterGridLines) && ($this->y - $textAfterGridLinesHeight) < ($calcTotalsHeight + $footerTextHeight)) {
                    if (($this->y  - $calcTotalsHeight) < $footerTextHeight) {
                        $this->insertFooterText($page, $order);
                        $page = $this->newPage();
                        $this->_setFontRegular($page);
                        $this->y = 810;
                    } else {
                        $page = $this->insertTotals($page, $invoice);
                        $insertTotals = false;
                        $this->insertFooterText($page, $order);
                        $page = $this->newPage();
                        $this->_setFontRegular($page);
                        $this->y = 810;
                    }
                }
            }

            if($insertTotals){
                $page = $this->insertTotals($page, $invoice);
            }

            if (!empty($textAfterGridLines)) {
                $this->_setFontRegular($page, 7);

                foreach ($textAfterGridLines as $line) {
                    $page->drawText(trim(strip_tags($line)), 30, $this->y, 'UTF-8');
                    $this->y -= 10;
                }
            }
        }

        if(isset($order)){
          $this->insertFooterText($page, $order);
        }

        $this->_afterGetPdf();

        return $pdf;
    }

    /**
     * Calculate extAfterGrid Height while ignoring empty line in the end of the paragraph
     * @param $textAfterGridLines
     * @return int
     */
    protected function calcTextAfterGridHeight($textAfterGridLines){
        $height = 0;
        $cleanArray = array_reverse($textAfterGridLines);
        foreach ($cleanArray as $line){
            if(!trim($line) && $height == 0){
                continue;
            }
            $height += 10;
        }
        return $height;
    }


    /**
     * Calculate BLoc Footer Height
     * @param $order
     * @return float|int
     */
    protected function calcFooterTextHeight($order)
    {
        $height = 0;

        $image = $this->_scopeConfig->getValue(
            'sales_pdf/invoice/signature',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $order->getStore()
        );

        $maxHeight = ($image) ? 130 : 110;
        $height += count(explode("\n", $this->configHelper->getSalesPdfInvoiceFooterCore($order->getStoreId()))) * 10;

        $signatureText = $this->configHelper->getSalesPdfInvoiceSignagureText($order->getStoreId());
        if ($signatureText != null) {
            $height += ($image) ? 40 : 30;
            $height += 10;
        }
        return max($height, $maxHeight);
    }

    /**
     * Calculate Totals bloc height
     * @param $invoice
     * @return int
     */
    protected function calcTotalsHeight($invoice){
        $height = 17;

        $totals = $this->_getTotalsList();
        $order = $invoice->getOrder();

        foreach ($totals as $total) {
            $total->setOrder($order)->setSource($invoice);

            if ($total->canDisplay()) {
                foreach ($total->getTotalsForDisplay() as $totalData) {
                    if (!$this->canDisplayTotal($total->getSourceField(), $order->getStore()) && !isset($totalData['tax'])) {
                        continue;
                    }

                    $height += 15;
                }
            }
        }
        $height += 10;

        return $height;
    }

    /**
     * Insert logo to pdf page
     *
     * @param \Zend_Pdf_Page &$page
     * @param int $width
     * @param int $height
     * @param null $store
     *
     * @return void
     * @throws \Zend_Pdf_Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    protected function insertCustomLogo(&$page, $width, $height, $store = null)
    {
        $this->y = 815;
        $image = $this->_scopeConfig->getValue(
            'sales/identity/logo',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );

        if ($image) {
            $imagePath = '/sales/store/logo/' . $image;
            if ($this->_mediaDirectory->isFile($imagePath)) {
                $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));

                $page->drawImage($image, 25, $this->y - $height, 25 + $width, $this->y);

                $this->y -= $height;
            }
        }
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param null $store
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertAddress(&$page, $store = null)
    {
        $y = 815;
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 7);
        $this->_setFontRegular($page, 9);

        $address = $this->_scopeConfig->getValue(
            'sales/identity/address',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );

        foreach (explode("\n", $address) as $value) {
            if ($value !== '') {
                $page->drawText(
                    trim(strip_tags($value)),
                    $this->getAlignRight($value, -25, $page->getWidth(), $page->getFont(), $page->getFontSize()),
                    $y,
                    'UTF-8'
                );

                $y -= 9;
            }
        }
    }

    /**
     * Insert order to pdf page
     *
     * @param \Zend_Pdf_Page &$page
     * @param Order $obj
     * @param bool $putOrderId
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Zend_Pdf_Exception
     */
    protected function insertOrder(&$page, $obj, $putOrderId = true)
    {
        if ($obj instanceof Order) {
            $shipment = null;
            $order = $obj;
        } elseif ($obj instanceof Order\Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
        } elseif ($obj instanceof Order\Invoice) {
            $shipment = null;
            $invoice = $obj;
            $order = $invoice->getOrder();
        }

        $this->y = $this->y ? $this->y : 815;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.3));
        $this->_setFontRegular($page, 9);

        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 15;

        if ($invoice !== null && ($this->invoiceType === 'commercial_invoice' || $this->invoiceType === 'gift_invoice')) {
            $date = ' / ' . mb_strtoupper(__('Invoice Date: ')) . $this->_localeDate->formatDate(
                $this->_localeDate->scopeDate(
                    $invoice->getStore(),
                    \DateTime::createFromFormat('Y-m-d H:i:s', $invoice->getCreatedAt()),
                    true
                ),
                \IntlDateFormatter::MEDIUM,
                false
            );

            $page->drawText(
                mb_strtoupper(__($this->documentName)) . ' # ' . $invoice->getIncrementId() . $date,
                25,
                $this->y,
                'UTF-8'
            );
            $this->y -= 12;
        }

        if ($putOrderId) {
            $page->drawText(mb_strtoupper(__('Order # ')) . $order->getRealOrderId(), 25, $this->y, 'UTF-8');
            $this->y -= 12;
        }

        $page->drawText(
            mb_strtoupper(
                __('Order Date: ') .
                $this->_localeDate->formatDate(
                    $this->_localeDate->scopeDate(
                        $order->getStore(),
                        \DateTime::createFromFormat('Y-m-d H:i:s', $order->getCreatedAt()),
                        true
                    ),
                    \IntlDateFormatter::MEDIUM,
                    false
                )
            ),
            25,
            $this->y,
            'UTF-8'
        );
        $this->y -= 10;

        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 10;

        $billingAddress = $this->_formatAddress($this->addressRenderer->format($order->getBillingAddress(), 'pdf'));

        $shippingAddress = $this->prepareShippingAddressBlock($order);

        $blockHeight = max(count($billingAddress) + 2, count($shippingAddress) + 2) * 10 + 15;

        $this->insertBillingAddressBlock($page, $order, $blockHeight, $billingAddress);
        $this->insertShippingAddressBlock($page, $order, $blockHeight, $shippingAddress);

        $this->y -= $blockHeight;
        $this->y -= 10;

        $paymentLines = $this->preparePaymentBlockLines($order);
        $shipmentLines = $this->prepareShipmentBlockLines($order);

        $blockHeight = max(count($paymentLines) + 2, count($shipmentLines) + 2, 4) * 10 + 15;

        $this->insertPaymentBlock($page, $order, $blockHeight, $paymentLines);
        if (count($paymentLines)) {
            $this->insertShipmentBlock($page, $order, $blockHeight, $shipmentLines);
        } else {
            $this->insertShipmentBlock($page, $order, $blockHeight, $shipmentLines, 25, 545);
        }

        $this->y -= $blockHeight;
        $this->y -= 10;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @param int $blockHeight
     * @param array $billingLines
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertBillingAddressBlock(&$page, $order, $blockHeight, $billingLines)
    {
        $y = $this->y;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->drawRectangle(25, $y, 280, $y - $blockHeight);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 8);

        $y -= 15;
        $page->drawText($this->getBillingAddressBlockTitle(), 35, $y, 'UTF-8');

        $y -= 5;
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawLine(35, $y, 270, $y);

        $this->_setFontRegular($page);

        $lineCounter = 1;
        foreach ($billingLines as $value) {
            if ($value !== '') {
                $page->drawText(strip_tags(ltrim($value)), 35, $y - 10 * $lineCounter, 'UTF-8');
                $lineCounter++;
            }
        }
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @param int $blockHeight
     * @param array $shippingLines
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertShippingAddressBlock(&$page, $order, $blockHeight, $shippingLines)
    {
        $y = $this->y;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->drawRectangle(290, $y, 570, $y - $blockHeight);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 8);

        $y -= 15;
        $page->drawText($this->getShippingAddressBlockTitle(), 300, $y, 'UTF-8');

        $y -= 5;
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawLine(300, $y, 560, $y);

        $this->_setFontRegular($page);

        $lineCounter = 1;
        foreach ($shippingLines as $value) {
            if ($value !== '') {
                $page->drawText(strip_tags(ltrim($value)), 300, $y - 10 * $lineCounter, 'UTF-8');
                $lineCounter++;
            }
        }
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function preparePaymentBlockLines($order)
    {
        $payment = $order->getPayment();
        $paymentInfo = $this->_paymentData->getInfoBlock($payment);
        $paymentInfoArray = $order->getPayment()->getAdditionalInformation();

        if (!is_array($paymentInfoArray)) {
            $paymentInfoArray = [];
            $pdfPaymentInfo = $paymentInfo->setIsSecureMode(true)->toPdf();
            $pdfPaymentLines = explode('{{pdf_row_separator}}', $pdfPaymentInfo);
            foreach ($pdfPaymentLines as $key => $value) {
                if (($lineValue = strip_tags(trim($value))) != '') {
                    $paymentInfoArray[] = $lineValue;
                }
            }
        }

        if ($payment->getCcExpMonth() && $payment->getCcExpYear()) {
            $paymentInfoArray['cc_exp'] = sprintf(
                '%s/%s',
                $payment->getCcExpMonth(),
                $payment->getCcExpYear()
            );
        }

        $paymentLines = [];
        foreach ($paymentInfoArray as $code => $data) {
            if (is_array($data)) {
                if (isset($data['value'], $data['label']) && trim($data['value']) != '') {
                    if ($this->paymentMethodHelper->getPaymentConfig($code)) {
                        $paymentLines[] = __($data['label']) . ': ' . strip_tags(trim($data['value']));
                    }
                }
            } elseif (is_string($data)) {
                if ($this->paymentMethodHelper->getPaymentConfig($code)) {
                    $paymentLines[] = __($this->paymentMethodHelper->translateCode($code)) . ': ' . strip_tags(trim($data));
                }
            }
        }

        return $paymentLines;
    }

    /**
     * @param \Project\Sales\Model\Magento\Sales\Order $order
     * @param bool $withTotalCharges
     *
     * @return array
     */
    protected function prepareShipmentBlockLines($order, $withTotalCharges = true)
    {
        if ($order->getShippingAddress()) {
            $shippingMethod = $order->getShippingDescription();
        } else {
            $shippingMethod = __('None');
        }

        $shippingLines = [];

        if ($shippingMethod) {
            $shippingLines[] = __('Delivery Method') . ": " . $shippingMethod;
        }

        if ($withTotalCharges) {
            $shippingLines[] = '(' . __('Total Shipping Charges') . ' ' . $order->formatPriceTxt(
                $order->getShippingAmount()
            ) . ')';
        }


        if ($airwayBillNumber = $order->getAirwayBillNumber()) {
            $shippingLines[] = __('Airway Bill Number') . ": " . $airwayBillNumber;
        }

        return $shippingLines;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @param int $blockHeight
     * @param array $paymentLines
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertPaymentBlock(&$page, $order, $blockHeight, $paymentLines)
    {
        $y = $this->y;
        $x = 25;
        $width = 255;
        $padding = 10;
        $rowHeight = 10;
        $paddingTop = 15;
        $titleFontSize = 8;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $this->_setFontRegular($page, $titleFontSize);

        $page->drawRectangle($x, $y, $x + $width, $y - $blockHeight);

        $y -= $paddingTop;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->drawText($this->getPaymentBlockTitle(), $x + $padding, $y, 'UTF-8');

        $y -= $rowHeight / 2;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawLine($x + $padding, $y, $x + $width - $padding, $y);

        $y -= $rowHeight;

        foreach ($paymentLines as $lineNumber => $lineValue) {
            $page->drawText($lineValue, $x + $padding, $y - $rowHeight * $lineNumber, 'UTF-8');
        }
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @param int $blockHeight
     * @param array $shippingLines
     * @param int $x
     * @param int $width
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertShipmentBlock(&$page, $order, $blockHeight, $shippingLines, $x = 290, $width = 280)
    {
        $y = $this->y;
        $padding = 10;
        $rowHeight = 10;
        $paddingTop = 15;
        $titleFontSize = 8;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->drawRectangle($x, $y, $x + $width, $y - $blockHeight);

        $y -= $paddingTop;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, $titleFontSize);

        $page->drawText($this->getShipmentBlockTitle(), $x + $padding, $y, 'UTF-8');

        $y -= $rowHeight / 2;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawLine($x + $padding, $y, $x + $width - $padding, $y);

        $this->_setFontRegular($page);

        $y -= $rowHeight;

        foreach ($shippingLines as $lineNumber => $lineValue) {
            $page->drawText($lineValue, $x + $padding, $y - $rowHeight * $lineNumber, 'UTF-8');
        }
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Invoice $invoice
     *
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function insertItems(&$page, $invoice)
    {
        /** @var Order $order */
        $order = $invoice->getOrder();

        $this->y -= 15;

        $this->drawItemsHeader($page, $invoice);

        $this->y -= 15;
        $isFirstProduct = true;

        $counter = 0;
        foreach ($invoice->getAllItems() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                continue;
            }

            if ($this->y < $this->calcFooterTextHeight($order) || $counter === 2) {
                $this->insertFooterText($page, $order);
                $page = $this->newPage();
                $this->y = 810;
                $this->_setFontRegular($page);
                $this->insertCustomLogo($page, 135, 101, $invoice->getStore());
                $this->insertAddress($page, $invoice->getStore());
                $this->insertOrder(
                    $page,
                    $invoice,
                    $this->_scopeConfig->isSetFlag(
                        self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $order->getStoreId()
                    )
                );
                $this->y -= $this->insertMessage($page, $order);
                $this->y -= 15;
                $this->drawItemsHeader($page, $invoice);
                $this->y -= 15;
                $isFirstProduct = true;
                $counter = 0;
            }

            if (!$isFirstProduct) {
                $this->drawItemSeparator($page);
            }

            $this->_drawItem($item, $page, $order);
            $isFirstProduct = false;
            $counter++;
        }
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Invoice $invoice
     *
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function drawItemsHeader(&$page, $invoice)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.4));

        $headerConfig = $this->getItemsHeaderConfiguration($invoice, 25, 570);

        foreach ($headerConfig as $columnConfig) {
            $page->drawText($columnConfig['label'], $columnConfig['position'], $this->y, 'UTF-8');
        }

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Order $order
     * @throws \Zend_Pdf_Exception
     */
    protected function insertFooterText(&$page, $order)
    {

        $image = $this->_scopeConfig->getValue(
            'sales_pdf/invoice/signature',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $order->getStore()
        );

        if ($image) {
            $imagePath = '/sales/store/signature/' . $image;
            if ($this->_mediaDirectory->isFile($imagePath)) {
                $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));
                $width = $image->getPixelWidth() * 72 / 300;
                $height = $image->getPixelHeight() * 72 / 300;

                //coordinates after transformation are rounded by Zend
                $page->drawImage($image, 400, 72-$height, 400+$width, 72);
            }
        }

        $this->y = ($image) ? 130 : 110;
        $this->_setFontBold($page, 10);

        $firstLine = $this->configHelper->getSalesPdfInvoiceFooterFirstline($order->getStoreId());
        $page->drawText(
            trim(strip_tags($firstLine)),
            $this->getAlignCenter($firstLine, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
            $this->y,
            'UTF-8'
        );

        $this->y -= 15;
        $this->_setFontBold($page, 7);

        foreach (explode("\n", $this->configHelper->getSalesPdfInvoiceFooterCore($order->getStoreId())) as $value) {
            if ($value !== '') {
                $page->drawText(
                    trim(strip_tags($value)),
                    $this->getAlignCenter($value, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
                    $this->y,
                    'UTF-8'
                );

                $this->y -= 10;
            }
        }

        $this->_setFontBold($page, 10);
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(1, 0, 0));
        $endLine = $this->configHelper->getSalesPdfInvoiceFooterEndline($order->getStoreId());
        $page->drawText(
            trim(strip_tags($endLine)),
            $this->getAlignCenter($endLine, 0, $page->getWidth(), $page->getFont(), $page->getFontSize()),
            $this->y,
            'UTF-8'
        );
        $signatureText = $this->configHelper->getSalesPdfInvoiceSignagureText($order->getStoreId());
        if($signatureText != null) {
            $this->_setFontBold($page, 9);
            $page->setFillColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));
            $this->y -= ($image) ? 40 : 30;
            $page->drawText(
                trim(strip_tags($signatureText)),
                $this->getAlignCenter($signatureText, 450, 10, $page->getFont(), $page->getFontSize()),
                $this->y,
                'UTF-8'
            );
        }
    }

    /**
     * @param \Zend_Pdf_Page &$page
     */
    protected function drawItemSeparator($page)
    {
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.7));
        $page->setLineWidth(0.3);
        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 12;
    }

    /**
     * @param Invoice $invoice
     * @param $startPosition
     * @param $tableWidth
     * @param array $itemsHeader
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * phpcs:disable
     */
    public function getItemsHeaderConfiguration($invoice, $startPosition, $tableWidth, $itemsHeader = [])
    {
        $store = $invoice->getOrder()->getStore();

        $itemsHeader = \array_merge(
            $itemsHeader,
            [
                'informations' => [
                    'label' => \mb_strtoupper(__('Product Information')),
                    'length' => 100,
                ],
                'details' => [
                    'label' => \mb_strtoupper(__('Details ')),
                    'length' => 80,
                ],
            ]
        );

        if ($this->othersHelper->hasQuantity()) {
            $itemsHeader['qty'] = [
                'label' => \mb_strtoupper(__('Qty ')),
                'length' => 20,
            ];
        }

        if ($this->othersHelper->hasUnitPrice()) {
            $itemsHeader['unit_price'] = [
                'label' => \mb_strtoupper(__('Unit Price ')),
                'length' => 40,
            ];
        }

        if ($this->othersHelper->hasTaxRate($store)) {
            $itemsHeader['tax_rate'] = [
                'label' => \mb_strtoupper(__('Tax Rate ')),
                'length' => 35,
            ];
        }

        if ($this->othersHelper->hasPriceWithVat($store)) {
            $itemsHeader['price_with_vat'] = [
                'label' => \mb_strtoupper(__('Price w/ Vat')),
                'length' => 50,
            ];
        }

        if ($this->othersHelper->hasTotalWithoutTax()) {
            $itemsHeader['total_without_tax'] = [
                'label' => \mb_strtoupper(__('Total w/o Tax')),
                'length' => 55,
            ];
        }

        if ($this->othersHelper->hasTotalTax($store)) {
            $itemsHeader['total_tax'] = [
                'label' => \mb_strtoupper(__('Total Tax ')),
                'length' => 60,
            ];
        }

        if ($this->othersHelper->hasDuty()) {
            $itemsHeader['duty'] = [
                'label' => \mb_strtoupper(__('Duty')),
                'length' => 60,
            ];
        }

        if ($this->othersHelper->hasTotal($store)) {
            $itemsHeader['total'] = [
                'label' => \mb_strtoupper(__('Total ')),
                'length' => 23,
            ];
        }

        $columnsLength = 0;
        foreach ($itemsHeader as $columnCode => $columnHeader) {
            $columnsLength += $columnHeader['length'];
        }

        $margin = ($tableWidth - $startPosition - $columnsLength) / (count($itemsHeader) - 1);

        $position = $startPosition;
        foreach ($itemsHeader as $columnCode => $columnHeader) {
            $itemsHeader[$columnCode]['position'] = (int) $position;
            $position += $margin + $itemsHeader[$columnCode]['length'];
        }

        return $itemsHeader;
        // phpcs:enable
    }

    /**
     * @return bool
     */
    public function displayPrices()
    {
        return true;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\AbstractModel $invoice
     *
     * @return \Zend_Pdf_Page
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function insertTotals($page, $invoice)
    {
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 17;

        $this->insertRegistrationNumber($page, $invoice);

        $order = $invoice->getOrder();
        $totals = $this->_getTotalsList();
        $lineBlock = [
            'lines' => [],
            'height' => 15,
        ];

        foreach ($totals as $total) {
            $total->setOrder($order)->setSource($invoice);

            if ($total->canDisplay()) {
                foreach ($total->getTotalsForDisplay() as $totalData) {
                    if (!$this->canDisplayTotal($total->getSourceField(), $order->getStore()) && !isset($totalData['tax'])) {
                        continue;
                    }

                    $lineBlock['lines'][] = [
                        [
                            'text' => __($total->getTitle().': '),
                            'feed' => 475,
                            'align' => 'right',
                            'font_size' => $totalData['font_size'],
                            'font' => 'bold',
                        ],
                        [
                            'text' => $totalData['amount'],
                            'feed' => 565,
                            'align' => 'right',
                            'font_size' => $totalData['font_size'],
                            'font' => 'bold',
                        ],
                    ];
                }
            }
        }

        $page = $this->drawLineBlocks($page, [$lineBlock]);

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 10;

        return $page;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\AbstractModel $invoice
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertRegistrationNumber(&$page, $invoice)
    {
        $order = $invoice->getOrder();

        if (!$this->totalsHelper->hasTaxRegister($order->getStore())) {
            return;
        }

        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress->getCountryId() === 'CA') {
            $this->_setFontRegular($page);
            $regionId = $shippingAddress->getRegionId();

            if ($regionId) {
                $region = $this->regionFactory->create()->loadByName(
                    $shippingAddress->getRegion(),
                    $shippingAddress->getCountryId()
                );
                $registrationNumber = $this->totalsHelper->getTaxRegisterLabel($region->getCode());
                $offsetY = 0;

                foreach ($registrationNumber as $line) {
                    $page->drawText($line, 25, $this->y - $offsetY, 'UTF-8');
                    $offsetY += 10;
                }
            }
        }
    }

    /**
     * @param string $sourceField
     *
     * @return bool
     */
    protected function canDisplayTotal($sourceField, $store)
    {
        return $this->totalsHelper->callFunctionByCode($sourceField, $store);
    }

    /**
     * @param Order $order
     * @return array
     */
    protected function prepareShippingAddressBlock($order)
    {
        $shippingAddress = [];
        if (!$order->getIsVirtual()) {
            $shippingAddress = $this->_formatAddress(
                $this->addressRenderer->format(
                    $order->getShippingAddress(),
                    'pdf'
                )
            );
        }

        return $shippingAddress;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param Order $order
     * @return int
     */
    protected function insertMessage(&$page, $order)
    {
        return 0;
    }

    /**
     * @param \Zend_Pdf_Page $page
     * @param int $x
     * @param int $y
     * @param int $size
     */
    public function insertCheckbox(&$page, $x, $y, $size = 7)
    {
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->drawLine($x, $y, $x, $y + $size);
        $page->drawLine($x, $y, $x + $size, $y);
        $page->drawLine($x + $size, $y + $size, $x + $size, $y);
        $page->drawLine($x + $size, $y + $size, $x, $y + $size);
    }

    /**
     * @return string
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }
}
