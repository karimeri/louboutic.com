<?php

namespace Project\Pdf\Model\Magento\Sales\Rma\Pdf;

use Magento\Catalog\Model\ProductRepository;
use Magento\Directory\Model\RegionFactory;
use Magento\Directory\Model\Region;
use Project\Pdf\Helper\Config;
use Project\Pdf\Helper\Config\Invoice\Others;
use Project\Pdf\Helper\Config\Invoice\PaymentMethod;
use Project\Pdf\Helper\Config\Invoice\Totals;
use Project\Pdf\Helper\Config\ProformaInvoice\Others as CustomOthers;
use Project\Pdf\Helper\Config\ProformaInvoice\ProductInformation as CustomProductInformation;
use Project\Pdf\Helper\Config\ProformaInvoice\Totals as CustomTotals;
use Project\Rma\Model\ResourceModel\ImporterAddress\Collection;
use Project\Rma\Model\Rma;

/**
 * Class ProformaInvoice
 *
 * @package Project\Pdf\Model\Magento\Sales\Rma\Pdf
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.CamelCaseMethodName)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ProformaInvoice extends \Project\Pdf\Model\Magento\Sales\Order\Pdf\Invoice
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Sales\Model\Order\ItemRepository
     */
    protected $itemRepository;

    /**
     * @var \Project\Rma\Model\ReturnAddress\Renderer
     */
    protected $returnAddressRenderer;

    /**
     * @var \Project\Rma\Model\RmaAddress\Renderer
     */
    protected $rmaAddressRenderer;

    /**
     * @var Collection
     */
    protected $addressCollection;

    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $appEmulation;

    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Sales\Model\Order\Pdf\Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation,
     * @param \Project\Pdf\Helper\Config\Invoice\PaymentMethod $paymentMethodHelper
     * @param \Magento\Directory\Model\Region $region
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Project\Pdf\Helper\Config\Invoice\Totals $totalsHelper
     * @param \Project\Pdf\Helper\Config\Invoice\Others $othersHelper
     * @param \Project\Pdf\Helper\Config $configHelper
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Sales\Model\Order\ItemRepository $itemRepository
     * @param \Project\Rma\Model\ReturnAddress\Renderer $returnAddressRenderer
     * @param \Project\Rma\Model\RmaAddress\Renderer $rmaAddressRenderer
     * @param \Project\Pdf\Helper\Config\ProformaInvoice\Totals $customTotalsHelper
     * @param \Project\Pdf\Helper\Config\ProformaInvoice\ProductInformation $customProductInformationHelper
     * @param \Project\Pdf\Helper\Config\ProformaInvoice\Others $customOthersHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        PaymentMethod $paymentMethodHelper,
        Region $region,
        RegionFactory $regionFactory,
        Totals $totalsHelper,
        Others $othersHelper,
        Config $configHelper,
        ProductRepository $productRepository,
        \Magento\Sales\Model\Order\ItemRepository $itemRepository,
        \Project\Rma\Model\ReturnAddress\Renderer $returnAddressRenderer,
        \Project\Rma\Model\RmaAddress\Renderer $rmaAddressRenderer,
        CustomTotals $customTotalsHelper,
        CustomProductInformation $customProductInformationHelper,
        CustomOthers $customOthersHelper,
        Collection $addressCollection,
        array $data = []
    ) {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $paymentMethodHelper,
            $region,
            $regionFactory,
            $totalsHelper,
            $othersHelper,
            $configHelper,
            $data
        );

        $this->productRepository = $productRepository;
        $this->itemRepository = $itemRepository;
        $this->returnAddressRenderer = $returnAddressRenderer;
        $this->rmaAddressRenderer = $rmaAddressRenderer;
        $this->totalsHelper = $customTotalsHelper;
        $this->paymentMethodHelper = $customProductInformationHelper;
        $this->othersHelper = $customOthersHelper;
        $this->rmaAddressRenderer = $rmaAddressRenderer;
        $this->addressCollection = $addressCollection;
    }

    /**
     * @param array $rmas
     * @return \Zend_Pdf
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPdf($rmas = [])
    {
        $this->_beforeGetPdf();

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        $startY = 795;
        $this->y = $startY;

        foreach ($rmas as $rma) {
            if ($rma->getStoreId()) {
                $this->appEmulation->startEnvironmentEmulation(
                    $rma->getStoreId(),
                    \Magento\Framework\App\Area::AREA_FRONTEND,
                    true
                );
                $this->_storeManager->setCurrentStore($rma->getStoreId());
            }

            $page = $this->newPage();

            /* Add image */
            $logoHeight = 101;
            $this->insertCustomLogo($page, 135, $logoHeight, $rma->getStore());

            $this->y = min($this->y, $startY - $logoHeight - 5);

            /* Add order */
            $this->insertRma($page, $rma);

            $this->insertFooterText($page, $rma);

            $this->insertSignature($page, $rma->getStore());
        }

        $this->_afterGetPdf();

        return $pdf;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Rma $rma
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    // phpcs:ignore
    protected function insertRma(&$page, $rma)
    {
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.3));
        $this->_setFontRegular($page, 9);

        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 15;

        $page->drawText(mb_strtoupper(__('Commercial Invoice')), 25, $this->y, 'UTF-8');
        $this->y -= 12;

        $page->drawLine(25, $this->y, 570, $this->y);
        $this->y -= 10;

        $senderLines = $this->prepareSenderBlock($rma);
        $blockHeight = max(count($senderLines) + 2, 4) * 10 + 15;
        $this->insertSenderImporterReceiverBlock(
            $page,
            $blockHeight,
            $senderLines,
            545,
            $this->getSenderBlockTitle()
        );

        $this->y -= $blockHeight;
        $this->y -= 10;

        $importerLines = $this->prepareImporterBlock($rma);
        $receiverLines = $this->prepareReceiverBlock($rma);
        $blockHeight = max(count($senderLines) + 2, count($receiverLines) + 2, 4) * 10 + 15;

        $this->insertSenderImporterReceiverBlock(
            $page,
            $blockHeight,
            $importerLines,
            255,
            $this->getImporterBlockTitle()
        );
        $this->insertSenderImporterReceiverBlock(
            $page,
            $blockHeight,
            $receiverLines,
            280,
            $this->getReceiverBlockTitle(),
            290
        );

        $this->y -= $blockHeight;
        $this->y -= 20;
        $this->_setFontRegular($page, 7);

        $dataLines = [];
        if ($this->othersHelper->hasDate()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Date')), 'value' => $rma->getPickupDate()];
        }
        if ($this->othersHelper->hasReference()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Reference')), 'value' => $rma->getIncrementId()];
        }
        $dataLines[] = ['label' => '', 'value' => ''];

        if ($this->othersHelper->hasWaybillNumber()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Waybill Number')), 'value' => $rma->getAirwayBillNumber()];
        }

        $this->insertDataValues($page, $dataLines, 35, 150);
        $this->y -= count($dataLines) * 10;
        $this->y -= 10;

        $this->_setFontRegular($page, 6);
        $itemsTotals = $this->insertItems($page, $rma);
        $this->_setFontRegular($page, 7);

        $this->y -= 20;

        $dataLines = [];

        if ($this->othersHelper->hasTotalDeclaredValue()) {
            $dataLines[] = [
                'label' => mb_strtoupper(__('Total Declared Value')),
                'value' => $rma->getOrder()->formatPriceTxt($itemsTotals['declared_value'])
            ];
        }

        if ($this->othersHelper->hasTotalPieces()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Total Pieces')), 'value' => $itemsTotals['pieces']];
        }
        if ($this->othersHelper->hasTypeOfExport()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Type Of Export')), 'value' => 'COM'];
        }
        if ($this->othersHelper->hasReasonOfExport()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Reason Of Export')), 'value' => 'PERMANENT'];
        }
        if ($this->othersHelper->hasTermsOfTrade()) {
            $dataLines[] = ['label' => mb_strtoupper(__('Terms Of Trade')), 'value' => 'DDU'];
        }

        if ($this->othersHelper->hasDHLAccount()) {
            $dataLines[] = [
                'label' => mb_strtoupper(__('DHL Account')),
                'value' => $this->_scopeConfig->getValue(
                    'carriers/dhl/account',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $rma->getStore()
                ),
            ];
        }

        $dataLines[] = ['label' => '', 'value' => ''];

        if ($this->othersHelper->hasReturnExWaybill()) {
            $dataLines[] = [
                'label' => mb_strtoupper(__('Return Ex Waybill')),
                'value' => $rma->getOrder()->getAirwayBillNumber()
            ];
        }

        $this->insertDataValues($page, $dataLines, 35, 150);

        $dataLines2 = [
            ['label' => mb_strtoupper(__('Total Net Weight')), 'value' => $itemsTotals['pieces']],
            ['label' => mb_strtoupper(__('Currency')), 'value' => $rma->getOrder()->getOrderCurrencyCode()],
        ];
        $this->insertDataValues($page, $dataLines2, 265, 380);

        $this->y -= max(count($dataLines), count($dataLines2)) * 10;
        $this->y -= 10;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param array $dataLines
     * @param int $xColumn1
     * @param int $xColumn2
     * @throws \Zend_Pdf_Exception
     */
    public function insertDataValues(&$page, $dataLines, $xColumn1, $xColumn2)
    {
        $y = $this->y;
        $rowHeight = 10;

        foreach ($dataLines as $line) {
            $page->drawText($line['label'], $xColumn1, $y, 'UTF-8');
            $page->drawText($line['value'], $xColumn2, $y, 'UTF-8');
            $y -= $rowHeight;
        }
    }

    /**
     * @param Rma $rma
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function prepareSenderBlock($rma)
    {
        $senderLines = $this->_formatAddress(
            $this->rmaAddressRenderer->format($rma->getRmaAddress(), 'pdf')
        );

        return str_replace('T:', 'Mobile :', $senderLines);
    }

    /**
     * @param Rma $rma
     * @return array
     */
    protected function prepareImporterBlock($rma)
    {
        $data =  $this->addressCollection->getData();
        $data = $data[0];
        return [
            $data['address_name'],
            $data['address_company'],
            $data['address_street'],
            $data['address_code_postal'].' '.$data['address_city'],
            $data['address_country'],
            'VAT '.$data['address_vat'],
        ];
    }

    /**
     * @param Rma $rma
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function prepareReceiverBlock($rma)
    {
        return $this->_formatAddress(
            $this->returnAddressRenderer->format($rma->getReturnAddress(), 'pdf')
        );
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param $blockHeight
     * @param $senderLines
     * @param int $width
     * @param string $title
     * @param int $x
     *
     * @throws \Zend_Pdf_Exception
     */
    protected function insertSenderImporterReceiverBlock(&$page, $blockHeight, $senderLines, $width, $title, $x = 25)
    {
        $y = $this->y;
        $padding = 10;
        $rowHeight = 10;
        $paddingTop = 15;
        $titleFontSize = 8;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.93));
        $this->_setFontRegular($page, $titleFontSize);

        $page->drawRectangle($x, $y, $x + $width, $y - $blockHeight);

        $y -= $paddingTop;

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $page->drawText($title, $x + $padding, $y, 'UTF-8');

        $y -= $rowHeight / 2;

        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawLine($x + $padding, $y, $x + $width - $padding, $y);

        $y -= $rowHeight;

        foreach ($senderLines as $lineNumber => $lineValue) {
            $page->drawText($lineValue, $x + $padding, $y - $rowHeight * $lineNumber, 'UTF-8');
        }
    }

    /**
     * @return string
     */
    protected function getSenderBlockTitle()
    {
        return mb_strtoupper(__('Sender:'));
    }

    /**
     * @return string
     */
    protected function getImporterBlockTitle()
    {
        return mb_strtoupper(__('Importer:'));
    }

    /**
     * @return string
     */
    protected function getReceiverBlockTitle()
    {
        return mb_strtoupper(__('Receiver:'));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param int $startPosition
     * @param int $tableWidth
     * @param array $itemsHeader
     * @return array
     */
    // phpcs:ignore
    public function getItemsHeaderConfiguration($page, $startPosition, $tableWidth, $itemsHeader = [])
    {
        $itemsHeader = [];

        if ($this->paymentMethodHelper->hasFullDescriptionOfTheGoods()) {
            $itemsHeader['name'] = [
                'label' => mb_strtoupper(__('Full Description Of The Goods')),
            ];
        }

        if ($this->paymentMethodHelper->hasQuantity()) {
            $itemsHeader['qty'] = [
                'label' => mb_strtoupper(__('Quantity')),
            ];
        }

        if ($this->paymentMethodHelper->hasCommodityCode()) {
            $itemsHeader['code'] = [
                'label' => mb_strtoupper(__('Commodity Code')),
            ];
        }

        if ($this->paymentMethodHelper->hasUnitValue()) {
            $itemsHeader['unit_value'] = [
                'label' => mb_strtoupper(__('Unit Value')),
            ];
        }

        if ($this->paymentMethodHelper->hasSubtotalValue()) {
            $itemsHeader['subtotal_value'] = [
                'label' => mb_strtoupper(__('Subtotal Value')),
            ];
        }

        if ($this->paymentMethodHelper->hasUnitNetWeight()) {
            $itemsHeader['unit_net_weight'] = [
                'label' => mb_strtoupper(__('Unit Net Weight')),
            ];
        }

        if ($this->paymentMethodHelper->hasSubtotalNetWeight()) {
            $itemsHeader['subtotal_net_weight'] = [
                'label' => mb_strtoupper(__('Subtotal Net Weight')),
            ];
        }

        if ($this->paymentMethodHelper->hasCountryOfManufacture()) {
            $itemsHeader['country'] = [
                'label' => mb_strtoupper(__('Country Of Manufacture')),
            ];
        }

        $columnsLength = 0;
        foreach ($itemsHeader as $columnCode => $columnHeader) {
            if (!isset($columnHeader['length'])) {
                $length = $this->widthForStringUsingFontSize(
                    $columnHeader['label'],
                    $page->getFont(),
                    $page->getFontSize()
                );
                $itemsHeader[$columnCode]['length'] = $length;
            } else {
                $length = $columnHeader['length'];
            }
            $columnsLength += $length;
        }

        $margin = ($tableWidth - $startPosition - $columnsLength) / (count($itemsHeader) - 1);

        $position = $startPosition;
        foreach ($itemsHeader as $columnCode => $columnHeader) {
            $itemsHeader[$columnCode]['position'] = $position;
            $position += $margin + $itemsHeader[$columnCode]['length'];
        }

        return $itemsHeader;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param array $headerConfig
     * @throws \Zend_Pdf_Exception
     */
    protected function drawItemsHeader(&$page, $headerConfig)
    {
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.4));

        foreach ($headerConfig as $columnConfig) {
            $page->drawText($columnConfig['label'], $columnConfig['position'], $this->y, 'UTF-8');
        }

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Rma $rma
     * @return array
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function insertItems(&$page, $rma)
    {
        $this->y -= 15;
        $headerConfig = $this->getItemsHeaderConfiguration($page, 25, 570);

        /* Add table head */
        $this->drawItemsHeader($page, $headerConfig);

        $this->y -= 15;
        $isFirstProduct = true;

        $pieces = 0;
        $declaredValue = 0;

        foreach ($rma->getItemsForDisplay() as $item) {
            $orderItem = $this->itemRepository->get($item->getOrderItemId());

            if ($orderItem->getParentItem()) {
                continue;
            }

            if ($this->y < 250) {
                $this->insertFooterText($page, $rma->getStore());
                $page = $this->newPage();
                $this->_setFontRegular($page);
                $this->y = 790;
                $this->drawItemsHeader($page, $headerConfig);
                $this->y -= 20;
                $isFirstProduct = true;
            }

            /* Draw item */
            if (!$isFirstProduct) {
                $this->drawItemSeparator($page);
            }

            $this->drawRmaItem($item, $page, $headerConfig, $declaredValue);
            $isFirstProduct = false;

            $pieces += $item->getQtyAuthorized();
        }

        return [
            'declared_value' => $declaredValue,
            'pieces' => $pieces,
        ];
    }

    /**
     * @param \Magento\Rma\Model\Item $item
     * @param \Zend_Pdf_Page &$page
     * @param array $headerConfig
     * @param int $totalDeclaredValue
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Pdf_Exception
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function drawRmaItem($item, $page, $headerConfig, &$totalDeclaredValue)
    {
        $orderItem = $this->itemRepository->get($item->getOrderItemId());
        $order = $orderItem->getOrder();
        $product = $this->productRepository->getById($orderItem->getProductId());

        $productName = $this->string->split($item->getProductName(), 60, true, true);
        $productName = isset($productName[0]) ? $productName[0] : '';
        $page->drawText($productName, $headerConfig['name']['position'], $this->y, 'UTF-8');
        $qty = round($item->getQtyAuthorized());
        $page->drawText($qty, $headerConfig['qty']['position'], $this->y, 'UTF-8');

        $htsCode = $product->getCustomCode();
        $page->drawText($htsCode, $headerConfig['code']['position'], $this->y, 'UTF-8');

        $unitValue = (
            $orderItem->getRowTotal()
            + $orderItem->getTaxAmount()
            + $orderItem->getDuty()
            + $orderItem->getShippingFee()
        ) / $orderItem->getQtyOrdered();
        $subtotalValue = $unitValue * $qty;
        $totalDeclaredValue += $subtotalValue;

        $unitValue = $order->formatPriceTxt($unitValue);
        $page->drawText($unitValue, $headerConfig['unit_value']['position'], $this->y, 'UTF-8');

        $subtotalValue = $order->formatPriceTxt($subtotalValue);
        $page->drawText($subtotalValue, $headerConfig['subtotal_value']['position'], $this->y, 'UTF-8');

        $unitWeight = 1;
        $page->drawText($unitWeight, $headerConfig['unit_net_weight']['position'], $this->y, 'UTF-8');

        $subtotalWeight = $unitWeight * $qty;
        $page->drawText($subtotalWeight, $headerConfig['subtotal_net_weight']['position'], $this->y, 'UTF-8');

        $country = $product->getProductCountryOrigin();
        $page->drawText($country, $headerConfig['country']['position'], $this->y, 'UTF-8');

        $this->y -= 10;

        $page->drawText(
            mb_strtoupper(__('Composition: ')) . $product->getData('material_product_page'),
            $headerConfig['name']['position'],
            $this->y,
            'UTF-8'
        );

        $this->y -= 10;
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param null $store
     * @throws \Zend_Pdf_Exception
     */
    protected function insertSignature(&$page, $store = null)
    {
        $image = $this->_scopeConfig->getValue(
            'sales_pdf/proforma_invoice/signature',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );

        if ($image) {
            $imagePath = '/sales/store/signature/' . $image;
            if ($this->_mediaDirectory->isFile($imagePath)) {
                $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));
                $width = $image->getPixelWidth() * 72 / 300;
                $height = $image->getPixelHeight() * 72 / 300;

                //coordinates after transformation are rounded by Zend
                $page->drawImage($image, 400, 120-$height, 400+$width, 120);
            }
        }
    }

    /**
     * @param \Zend_Pdf_Page &$page
     * @param Rma $rma
     * @throws \Zend_Pdf_Exception
     */
    protected function insertFooterText(&$page, $rma)
    {
        $this->_setFontBold($page, 7);
        $this->y -= 30;
        $page->drawText(
            $this->configHelper->getSalesPdfProformaInvoiceFooterFirstline($rma->getStoreId()),
            25,
            $this->y,
            'UTF-8'
        );

        $this->_setFontRegular($page, 7);
        $this->y -= 30;

        $core = $this->configHelper->getSalesPdfProformaInvoiceFooterCore($rma->getStoreId());
        foreach (explode("\n", $core) as $value) {
            if ($value !== '') {
                $page->drawText(
                    trim(strip_tags($value)),
                    25,
                    $this->y,
                    'UTF-8'
                );
            }

            $this->y -= 10;
        }
    }
}
