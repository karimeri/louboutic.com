<?php

namespace Project\Pdf\Model\Magento\Sales\Rma\Pdf;

/**
 * Class Rma
 *
 * @package Project\Pdf\Model\Magento\Sales\Rma\Pdf
 * @author Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD.ShortVariable)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Rma extends \Magento\Rma\Model\Pdf\Rma
{
    /**
     * @param array $rmaArray
     * @return \Zend_Pdf
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Pdf_Exception
     */
    public function getPdf($rmaArray = array())
    {
        $this->_beforeGetPdf();

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new \Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        if (!(is_array($rmaArray) && (count($rmaArray) === 1))) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Only one RMA is available for printing'));
        }
        $rma = $rmaArray[0];

        $storeId = $rma->getOrder()->getStore()->getId();
        if ($storeId) {
            $this->_localeResolver->emulate($storeId);
            $this->_storeManager->setCurrentStore($storeId);
        }

        $page = $this->newPage();

        /* Add image */
        $this->insertLogo($page, $storeId);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page, 5);

        $page->setLineWidth(0.5);
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->drawLine(125, 825, 125, 790);

        $page->setLineWidth(0);
        /* start y-position for next block */
        $this->y = 820;

        /* Add head */
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->drawRectangle(25, $this->y - 30, 570, $this->y - 75);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $this->_setFontRegular($page);

        $page->drawText(
            __('Return # ') . $rma->getIncrementId() . ' - ' . $rma->getStatusLabel(),
            35,
            $this->y - 40,
            'UTF-8'
        );

        $page->drawText(
            __('Return Date: ') . $this->_localeDate->formatDate(
                $rma->getDateRequested(),
                \IntlDateFormatter::MEDIUM,
                false
            ),
            35,
            $this->y - 50,
            'UTF-8'
        );

        $page->drawText(
            __('Order # ') . $rma->getOrder()->getIncrementId(),
            35,
            $this->y - 60,
            'UTF-8'
        );

        $page->drawText(
            __('Ordered Date: ') .
            $this->_localeDate->formatDate(
                $rma->getOrder()->getCreatedAt(),
                \IntlDateFormatter::MEDIUM,
                false
            ),
            35,
            $this->y - 70,
            'UTF-8'
        );

        /* start y-position for next block */
        $this->y = $this->y - 80;

        /* add address blocks */
        $shippingAddress = $this->_formatAddress(
            $this->addressRenderer->format(
                $rma->getOrder()->getShippingAddress(),
                'pdf'
            )
        );
        $returnAddress = $this->_formatAddress($this->_rmaData->getReturnAddress(
            'pdf',
            [],
            $this->getStoreId()
        ));

        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.93, 0.92, 0.92));
        $page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 290, $this->y - 15);
        $page->drawRectangle(305, $this->y, 570, $this->y - 15);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);
        $page->drawText(__('Shipping Address:'), 35, $this->y - 10, 'UTF-8');
        $page->drawText(__('Return Address:'), 315, $this->y - 10, 'UTF-8');

        $y = $this->y - 15 - (max(count($shippingAddress), count($returnAddress)) * 10 + 5);

        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->drawRectangle(25, $this->y - 15, 290, $y);
        $page->drawRectangle(305, $this->y - 15, 570, $y);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        $this->_setFontRegular($page);

        $yStartAddresses = $this->y - 25;

        foreach ($shippingAddress as $value) {
            if ($value!=='') {
                $page->drawText(strip_tags(ltrim($value)), 35, $yStartAddresses, 'UTF-8');
                $yStartAddresses -=10;
            }
        }

        $yStartAddresses = $this->y - 25;

        foreach ($returnAddress as $value) {
            if ($value!=='') {
                $page->drawText(strip_tags(ltrim($value)), 315, $yStartAddresses, 'UTF-8');
                $yStartAddresses -=10;
            }
        }

        /* start y-position for next block */
        $this->y = $this->y - 20 - (max(count($shippingAddress), count($returnAddress)) * 10 + 5);

        /* Add table */
        $this->_setColumnXs();
        $this->_addItemTableHead($page);

        /* Add body */
        /** @var $collection \Magento\Rma\Model\ResourceModel\Item\Collection */
        $collection = $rma->getItemsForDisplay();

        foreach ($collection as $item) {
            if ($this->y < 15) {
                $page = $this->_addNewPage();
            }

            /* Draw item */
            $this->_drawRmaItem($item, $page);
        }

        if ($storeId) {
            $this->_localeResolver->revert();
        }

        $this->_afterGetPdf();
        return $pdf;
    }
}
