<?php

namespace Project\Pdf\Model\Magento\Tax\Pdf;

/**
 * Class Shipping
 *
 * @package Project\Pdf\Model\Magento\Tax\Pdf
 * @author Synolia <contact@synolia.com>
 */
class Shipping extends \Magento\Tax\Model\Sales\Pdf\Shipping
{
    /**
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $amount = $this->getOrder()->formatPriceTxt($this->getAmount());
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;

        return [
            [
                'amount' => $this->getAmountPrefix() . $amount,
                'label' => __($this->getTitle()) . ':',
                'font_size' => $fontSize,
            ],
        ];
    }
}
