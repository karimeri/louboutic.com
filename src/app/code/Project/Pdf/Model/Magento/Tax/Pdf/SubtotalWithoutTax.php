<?php

namespace Project\Pdf\Model\Magento\Tax\Pdf;

/**
 * Class SubtotalWithoutTax
 *
 * @package Project\Pdf\Model\Magento\Tax\Pdf
 * @author Synolia <contact@synolia.com>
 */
class SubtotalWithoutTax extends \Magento\Tax\Model\Sales\Pdf\Subtotal
{
    /**
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $subtotalWithoutTax = $this->getOrder()->getBaseSubtotal() + $this->getOrder()->getBaseShippingAmount();
        $amount = $this->getOrder()->formatPriceTxt($subtotalWithoutTax);

        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $totals = [
            [
                'amount' => $this->getAmountPrefix() . $amount,
                'label' => __($this->getTitle()) . ':',
                'font_size' => $fontSize,
            ],
        ];

        return $totals;
    }
}
