<?php

namespace Project\Pdf\Model\Magento\Tax\Pdf;

class Tax extends \Magento\Tax\Model\Sales\Pdf\Tax
{
    /**
     * @return array
     */
    public function getTotalsForDisplay()
    {
        if ($this->getOrder()->getShippingAddress()->getCountryId() !== 'CA') {
            return parent::getTotalsForDisplay();
        }

        $totals = [];

        foreach ($this->getFullTaxInfo() as $tax) {
            $totals[] = [
                'amount' => $tax['amount'],
                'label' => $tax['label'],
                'font_size' => $tax['font_size'],
                'tax' => true,
            ];
        }


        return $totals;
    }
}
