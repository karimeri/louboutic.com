<?php

namespace Project\Pdf\Model\Magento\Tax\Pdf;

/**
 * Class Subtotal
 *
 * @package Project\Pdf\Model\Magento\Tax\Pdf
 * @author Synolia <contact@synolia.com>
 */
class Subtotal extends \Magento\Tax\Model\Sales\Pdf\Subtotal
{
    /**
     * @return array
     */
    public function getTotalsForDisplay()
    {
        return [
            [
                'amount' => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($this->getAmount()),
                'label' => __($this->getTitle()) . ':',
                'font_size' => $this->getFontSize() ? $this->getFontSize() : 7,
            ],
        ];
    }
}
