<?php

namespace Project\NewsletterPopup\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const COOKIE_NAME = 'cl_newsletter';

    /**
     * @var array
     */
    protected $newsletterOptions;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
        $this->newsletterOptions = $this->scopeConfig->getValue('louboutin_newsletterpopup',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return integer
     */
    public function getNbVisitedPages() {
        return $this->newsletterOptions['general']['display_after_pages'];
    }

    /**
     * @return string
     */
    public function getCookieName() {
        return self::COOKIE_NAME;
    }
    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->_getUrl('newsletterpopup/subscriber/new', ['_secure' => true]);
    }
}
