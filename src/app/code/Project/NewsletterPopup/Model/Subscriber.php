<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\NewsletterPopup\Model;

/**
 * Subscriber model
 *
 * @method int getStoreId()
 * @method $this setStoreId(int $value)
 * @method string getChangeStatusAt()
 * @method $this setChangeStatusAt(string $value)
 * @method int getCustomerId()
 * @method $this setCustomerId(int $value)
 * @method string getSubscriberEmail()
 * @method $this setSubscriberEmail(string $value)
 * @method int getSubscriberCollectionGender()
 * @method $this setSubscriberCollectionGender(int $value)
 * @method int getSubscriberStatus()
 * @method $this setSubscriberStatus(int $value)
 * @method string getSubscriberConfirmCode()
 * @method $this setSubscriberConfirmCode(string $value)
 * @method int getSubscriberId()
 * @method Subscriber setSubscriberId(int $value)
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 *
 * @api
 * @since 100.0.2
 */
class Subscriber extends \Magento\Newsletter\Model\Subscriber
{
    /**
     * Alias for getSubscriberCollectionGender()
     *
     * @return int
     */
    public function getCollectionGender()
    {
        return $this->getSubscriberCollectionGender();
    }

    /**
     * Alias for setSubscriberCollectionGender()
     *
     * @param int $value
     * @return $this
     */
    public function setCollectionGender($value)
    {
        return $this->setSubscriberCollectionGender($value);
    }
}
