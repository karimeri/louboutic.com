define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'Magento_Customer/js/customer-data',
    'mage/mage',
    'mage/cookies',
    'jquery/ui'
], function ($, modal, customerData) {
    'use strict';

    $.widget('project.processPopupNewsletter', {
        options: {
            'modalForm': '#popup-newsletter',
            'modalButton': '#subscribe-popup',
            'cookieName': 'cl_newsletter',
            'nbVisitedPages': 3,
            'pageCounter': 'cl_visitedpagescounter'
        },
        customer: customerData.get('customer'),

        _create: function() {
            this.options.modalOption = this._getModalOptions();
            this._bind();
        },
        _getModalOptions: function() {
            var popup_newsletter_options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                clickableOverlay: false,
                buttons: true,
                modalClass : 'popup-newsletter'
            };

            return popup_newsletter_options;
        },

        _bind: function() {
            var modalOption = this.options.modalOption;
            var modalForm = this.options.modalForm;
            var self = this;

            if ( !this.getNewsletterCookie() && (this.getVisitedPageCount() >= this.options.nbVisitedPages -1)) {
                if(self._getEmail() != 'undefined'){
                    $(modalForm).find('#newsletter-email').val(self._getEmail());
                }
                // Create modal
                $(modalForm).modal(modalOption);
                // Open modal
                $(modalForm).trigger('openModal');

                $(document).on('click', 'input[type="checkbox"]', function() {
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });

                $(modalForm).modal('openModal').on('modalclosed', function() {
                    $.cookie('cl_newsletter', 'true', { path: '/'});
                });

                $(modalForm).find('form.subscribe-newsletter').submit(function() {
                    self._submitHandler($(this), modalForm);
                    return false;
                });
            }else {
                if ( !this.getNewsletterCookie() && (this.getVisitedPageCount() < this.options.nbVisitedPages -1 )) {
                    this.countVisitedPages();
                }
            }

        },

        _submitHandler: function($form, modalForm) {
            if ($form.validation('isValid')) {
                $.ajax({
                    url: $form.attr('action'),
                    cache: true,
                    data: $form.serialize(),
                    dataType: 'json',
                    type: 'POST',
                    showLoader: true

                }).done(function (data) {
                    $(modalForm).find('.responses .response div').html(data.message);
                    if (data.error) {
                        $(modalForm).find('.responses .response').addClass('error ');
                    } else {
                        $(modalForm).find('.responses .response').addClass('success');
                        $.cookie('cl_newsletter', 'true', { path: '/'});
                        $(modalForm).find('#newsletter-email').val("");
                        //track newsletter POPUP
                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                            "event":"gtm.ext.event",
                            "eventTmp":"newsletterSubscription",
                            "eventCategory":"newsletterSubscription",
                            "eventAction":"popUp",
                            "eventLabel": data.collectionGender
                        });
                        setTimeout(function() {
                            $(modalForm).modal('closeModal');
                        }, 3000);
                    }
                    $(modalForm).find('.messages').show();
                    setTimeout(function() {
                        $(modalForm).find('.messages').hide();
                    }, 5000);
                });
            }
        },

        _getEmail: function () {
            return this.customer().email;
        },

        countVisitedPages: function() {
            $.cookie(this.options.pageCounter, this.getVisitedPageCount() +1, { path: '/'});
        },

        getVisitedPageCount: function () {
            return $.cookie(this.options.pageCounter) - 0;
        },

        getNewsletterCookie: function () {
            return $.cookie(this.options.cookieName);
        },

    });

    return $.project.processPopupNewsletter;
});
