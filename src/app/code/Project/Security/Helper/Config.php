<?php
namespace Project\Security\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 * @package Project\Security\Helper
 * @author Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_DURATION    = 'louboutin_security/blacklist_ip/duration';
    const XML_PATH_ATTEMPTS    = 'louboutin_security/blacklist_ip/attempts';
    const XML_PATH_ALERT_EMAIL = 'louboutin_security/blacklist_ip/alert_email';

    /**
     * @param $configName
     * @param null $scopeCode
     * @return mixed
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     * @return int
     */
    public function getDuration($scopeCode = null)
    {
        return (int) $this->getConfig($this::XML_PATH_DURATION, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     * @return int
     */
    public function getAttempts($scopeCode = null)
    {
        return (int) $this->getConfig($this::XML_PATH_ATTEMPTS, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     * @return string
     */
    public function getAlertEmail($scopeCode = null)
    {
        return $this->getConfig($this::XML_PATH_ALERT_EMAIL, $scopeCode);
    }
}
