<?php
namespace Project\Security\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Project\Security\Model\LoginFailure;
use Project\Security\Model\ResourceModel\LoginFailure as LoginFailureResource;

/**
 * Class InstallSchema
 * @package Project\Security\Setup
 * @author Synolia <contact@synolia.com>
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (!$setup->getConnection()->isTableExists($setup->getTable(LoginFailureResource::TABLE_NAME))) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable(LoginFailureResource::TABLE_NAME)
            )->addColumn(
                LoginFailure::IP,
                Table::TYPE_TEXT,
                45,
                ['nullable' => false, 'primary' => true],
                'IP field ; can contain IPv4 and IPv6'
            )->addColumn(
                LoginFailure::FAILURE_COUNT,
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'unsigned' => true, 'default' => 0],
                'Failure count'
            )->addColumn(
                LoginFailure::FAILURE_DATE,
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Failure first date'
            )->addColumn(
                LoginFailure::FLAG,
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'unsigned' => true, 'default' => 0],
                'Flag ; can be 0 or 1 with 1 = blacklisted'
            )->addIndex(
                'failure_date_count',
                [LoginFailure::FAILURE_DATE, LoginFailure::FAILURE_COUNT]
            );

            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }
}
