<?php
namespace Project\Security\Observer;

use Magento\Contact\Model\Config as ConfigContact;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\Store;
use Project\Security\Helper\Config;
use Project\Security\Model\LoginFailure as LoginFailureModel;
use Project\Security\Model\LoginFailureRepository;
use Project\Security\Model\ResourceModel\LoginFailure as LoginFailureResource;

/**
 * Class LoginFailure
 * @package Project\Security\Observer
 * @author Synolia <contact@synolia.com>
 */
class LoginFailure implements ObserverInterface
{
    /**
     * @var LoginFailureResource
     */
    protected $loginFailure;

    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var LoginFailureRepository
     */
    protected $loginFailureRepository;

    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var ConfigInterface
     */
    protected $configContact;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * LoginFailure constructor.
     * @param LoginFailureResource                               $loginFailure
     * @param LoginFailureRepository                             $loginFailureRepository
     * @param RemoteAddress                                      $remoteAddress
     * @param Config                                             $configHelper
     * @param TransportBuilder                                   $transportBuilder
     * @param ConfigContact                                      $configContact
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        LoginFailureResource $loginFailure,
        LoginFailureRepository $loginFailureRepository,
        RemoteAddress $remoteAddress,
        Config $configHelper,
        TransportBuilder $transportBuilder,
        ConfigContact $configContact,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->loginFailure = $loginFailure;
        $this->remoteAddress = $remoteAddress;
        $this->loginFailureRepository = $loginFailureRepository;
        $this->configHelper = $configHelper;
        $this->transportBuilder = $transportBuilder;
        $this->configContact = $configContact;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $ip = $this->remoteAddress->getRemoteAddress();

        try {
            $failure = $this->loginFailureRepository->getByIp($ip);

            if ($failure->getFlag() === LoginFailureRepository::BLACKLISTED) {
                return;
            }
        } catch (\Exception $exception) {
        }

        $data = [
            LoginFailureModel::IP => $ip,
            LoginFailureModel::FAILURE_COUNT => 1,
        ];

        $fields = [
            LoginFailureModel::FAILURE_COUNT => new \Zend_Db_Expr(LoginFailureModel::FAILURE_COUNT . ' + 1'),
        ];

        $this->loginFailure->insertOnDuplicate($data, $fields);

        try {
            $failure = $this->loginFailureRepository->getByIp($ip);

            $compareDate = new \DateTime(\sprintf('-%d hour', $this->configHelper->getDuration()));
            $failureDate = new \DateTime($failure->getFailureDate());

            if ($failure->getFailureCount() === $this->configHelper->getAttempts() &&
                $failureDate > $compareDate
            ) {
                $failure->setFlag(LoginFailureRepository::BLACKLISTED);
                $this->loginFailureRepository->save($failure);

                $this->sendAlertEmail($ip);
            } elseif ($failureDate < $compareDate) {
                $currentDate = new \DateTime();
                $failure
                    ->setFailureCount(1)
                    ->setFailureDate($currentDate->format("Y-m-d H:i:s"));
                $this->loginFailureRepository->save($failure);
            }
        } catch (\Exception $exception) {
        }
    }

    /**
     * Sends email
     * @param string $ip
     */
    public function sendAlertEmail($ip)
    {
        $this->transportBuilder
            ->setTemplateIdentifier('security_alert_email_template')
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(
                [
                    'ip' => $ip,
                    'env' => $this->getBaseUrl(),
                ]
            )
            ->setFrom(
                ['name' => $this->configContact->emailSender(), 'email' => $this->configContact->emailRecipient()]
            )
            ->addTo($this->configHelper->getAlertEmail());

        $this->transportBuilder->getTransport()->sendMessage();
    }

    /**
     * @return string
     */
    protected function getBaseUrl()
    {
        return $this->scopeConfig->getValue(\Magento\Store\Model\Store::XML_PATH_SECURE_BASE_URL);
    }
}
