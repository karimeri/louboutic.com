<?php
namespace Project\Security\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Project\Security\Model\LoginFailureFactory;
use Project\Security\Model\ResourceModel\LoginFailure as LoginFailureResource;
use Project\Security\Model\ResourceModel\LoginFailure\CollectionFactory as LoginFailureCollectionFactory;

/**
 * Class LoginFailureRepository
 * @package Project\Security\Model
 * @author Synolia <contact@synolia.com>
 */
class LoginFailureRepository
{


    const BLACKLISTED = 1;

    /**
     * @var LoginFailureCollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Project\Security\Model\LoginFailureFactory
     */
    protected $loginFailureFactory;

    /**
     * @var LoginFailureResource
     */
    protected $loginFailureResource;

    /**
     * LoginFailureRepository constructor.
     * @param LoginFailureCollectionFactory $collectionFactory
     * @param \Project\Security\Model\LoginFailureFactory $loginFailureFactory
     * @param LoginFailureResource $loginFailureResource
     */
    public function __construct(
        LoginFailureCollectionFactory $collectionFactory,
        LoginFailureFactory $loginFailureFactory,
        LoginFailureResource $loginFailureResource
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->loginFailureFactory = $loginFailureFactory;
        $this->loginFailureResource = $loginFailureResource;
    }

    /**
     * @param $ip
     * @return LoginFailure
     * @throws NoSuchEntityException
     */
    public function getByIp($ip)
    {
        $failure = $this->loginFailureFactory->create();
        $this->loginFailureResource->load($failure, $ip);

        if (!$failure->getIp()) {
            throw new NoSuchEntityException(__('Requested failure doesn\'t exist'));
        }

        return $failure;
    }

    /**
     * @param $ip
     * @param $flag
     * @return LoginFailure
     * @throws NoSuchEntityException
     */
    public function getByIpAndFlag($ip, $flag)
    {
        $ip = $this->loginFailureResource->getByIpAndFlag($ip, $flag);

        if (!$ip) {
            throw new NoSuchEntityException(__('Requested failure doesn\'t exist'));
        }

        return $this->getByIp($ip);
    }

    /**
     * @param LoginFailure $failure
     */
    public function save(LoginFailure $failure)
    {
        $this->loginFailureResource->save($failure);
    }
}
