<?php
namespace Project\Security\Model;

use Magento\Framework\Model\AbstractModel;
use Project\Security\Model\ResourceModel\LoginFailure as LoginFailureResource;

/**
 * Class LoginFailure
 * @package Project\Security\Model
 * @author Synolia <contact@synolia.com>
 */
class LoginFailure extends AbstractModel
{
    /**
     * Ip field name
     */
    const IP = 'ip';

    /**
     * Flag field name
     */
    const FLAG = 'flag';

    /**
     * Failure count field name
     */
    const FAILURE_COUNT = 'failure_count';

    /**
     * Failure date field name
     */
    const FAILURE_DATE = 'failure_date';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    public function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(LoginFailureResource::class);
    }

    /**
     * @return int
     */
    public function getFlag()
    {
        return (int) $this->getData(self::FLAG);
    }

    /**
     * @param int $flag
     * @return LoginFailure
     */
    public function setFlag($flag)
    {
        $this->setData(self::FLAG, $flag);
        return $this;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->getData(self::IP);
    }

    /**
     * @param string $ip
     * @return LoginFailure
     */
    public function setIp($ip)
    {
        $this->setData(self::IP, $ip);
        return $this;
    }

    /**
     * @return int
     */
    public function getFailureCount()
    {
        return (int) $this->getData(self::FAILURE_COUNT);
    }

    /**
     * @param int $count
     * @return LoginFailure
     */
    public function setFailureCount($count)
    {
        $this->setData(self::FAILURE_COUNT, $count);
        return $this;
    }

    /**
     * @return string
     */
    public function getFailureDate()
    {
        return $this->getData(self::FAILURE_DATE);
    }

    /**
     * @param string $date
     * @return LoginFailure
     */
    public function setFailureDate($date)
    {
        $this->setData(self::FAILURE_DATE, $date);
        return $this;
    }
}
