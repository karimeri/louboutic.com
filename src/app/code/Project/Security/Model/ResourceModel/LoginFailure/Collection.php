<?php
namespace Project\Security\Model\ResourceModel\LoginFailure;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Project\Security\Model\LoginFailure;
use Project\Security\Model\ResourceModel\LoginFailure as LoginFailureResource;

/**
 * Class Collection
 * @package Project\Security\Model\ResourceModel\LoginFailure
 * @author Synolia <contact@synolia.com>
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(LoginFailure::class, LoginFailureResource::class);
    }
}
