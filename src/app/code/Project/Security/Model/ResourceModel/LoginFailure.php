<?php
namespace Project\Security\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Project\Security\Model\LoginFailure as LoginFailureModel;

/**
 * Class LoginFailure
 * @package Project\Security\Model\ResourceModel
 * @author Synolia <contact@synolia.com>
 */
class LoginFailure extends AbstractDb
{
    const TABLE_NAME = 'project_login_failure_admin';

    /**
     * Constructor
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @codingStandardsIgnoreStart
     */
    protected  function _construct()
    {
        $this->_init($this::TABLE_NAME, 'ip');
    }

    /**
     * @param array $data
     * @param array $fields
     */
    public function insertOnDuplicate(array $data, array $fields = []) {
        $this->getConnection()->insertOnDuplicate($this->getMainTable(), $data, $fields);
    }

    /**
     * @param $ip
     * @param $flag
     * @return string
     */
    public function getByIpAndFlag($ip, $flag)
    {
        $select = $this->getConnection()
            ->select()
            ->from($this->getTable($this->getMainTable()))
            ->where(LoginFailureModel::FLAG . ' = ?', $flag)
            ->where(LoginFailureModel::IP . ' = ?', $ip)
            ->limit(1);

        return $this->getConnection()->fetchOne($select);
    }
}
