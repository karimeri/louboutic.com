<?php
namespace Project\Security\Plugin\Magento\Backend\Model;

use Magento\Backend\Model\Auth;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Project\Security\Model\LoginFailureRepository;

/**
 * Class AuthPlugin
 * @package Project\Security\Plugin\Magento\Backend\Model
 * @author Synolia <contact@synolia.com>
 */
class AuthPlugin
{
    /**
     * @var LoginFailureRepository
     */
    protected $loginFailureRepository;

    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * AuthPlugin constructor.
     * @param LoginFailureRepository $loginFailureRepository
     * @param RemoteAddress $remoteAddress
     */
    public function __construct(
        LoginFailureRepository $loginFailureRepository,
        RemoteAddress $remoteAddress
    ) {
        $this->loginFailureRepository = $loginFailureRepository;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * @param Auth $subject
     * @param \Closure $proceed
     * @param $username
     * @param $password
     * @return mixed
     * @throws AuthenticationException
     */
    public function aroundLogin(Auth $subject, \Closure $proceed, $username, $password)
    {
        try {
            $failure = $this->loginFailureRepository->getByIpAndFlag(
                $this->remoteAddress->getRemoteAddress(),
                LoginFailureRepository::BLACKLISTED
            );

            if ($failure->getId()) {
                throw new AuthenticationException(__('Your IP is blacklisted'));
            }
        } catch (AuthenticationException $authenticationException) {
            throw new AuthenticationException(__($authenticationException->getMessage()));
        } catch (\Exception $exception) {
        }

        return $proceed($username, $password);
    }
}
