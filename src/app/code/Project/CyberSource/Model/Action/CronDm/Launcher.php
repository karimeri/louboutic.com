<?php

namespace Project\CyberSource\Model\Action\CronDm;

use Project\CyberSource\Core\Cron\Dm;
use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

/**
 * Class Launcher
 * @package Project\CyberSource\Model\Action\CronDm
 * @author Synolia <contact@synolia.com>
 */
class Launcher implements ActionInterface
{
    /**
     * @var \Project\CyberSource\Core\Cron\Dm
     */
    protected $cronDm;

    /**
     * CallApi constructor.
     * @param \Project\CyberSource\Core\Cron\Dm $cronDm
     */
    public function __construct(
        Dm $cronDm
    ) {
        $this->cronDm = $cronDm;
    }

    /**
     * @param array $params
     * @param array $data
     * @param string $flowCode
     * @param ConsoleOutput $consoleOutput
     * @return array|bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $consoleOutput->writeInfo('Launching the dm cron');
        try {
            $this->cronDm->execute();
            $consoleOutput->writeSuccess('The dm cron has been correctly executed');
        } catch (\Throwable $e) {
            $consoleOutput->writeError('Error in the dm cron : '.$e->getMessage());
        }
    }
}
