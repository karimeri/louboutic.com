<?php

namespace Project\CyberSource\PayPal\Service;

use CyberSource\PayPal\Helper\RequestDataBuilder;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject\Mapper;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use CyberSource\Core\Model\LoggerInterface;

/**
 * Class CyberSourcePayPalSoapAPI
 * @package Project\CyberSource\PayPal\Service
 * @author Synolia <contact@synolia.com>
 */
class CyberSourcePayPalSoapAPI extends \CyberSource\PayPal\Service\CyberSourcePayPalSoapAPI
{
    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    private $countryFactory;
    /**
     * @var array
     */
    protected $billingAddressMap = [
        'payer' => 'email',
        'payerFirstname' => 'firstname',
        'payerLastname' => 'lastname',
        'countryCode' => 'country_id', // iso-3166 two-character code
        'state' => 'region',
        'city' => 'city',
        'street1' => 'street',
        'postalCode' => 'postcode'
    ];

    /**
     * CyberSourcePayPalSoapAPI constructor.
     * Assign private properties and call parent constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \CyberSource\Core\Service\AbstractConnection $logger
     * @param \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder
     * @param \CyberSource\PayPal\Helper\RequestDataBuilder $requestDataHelper
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger,
        BuilderInterface $transactionBuilder,
        RequestDataBuilder $requestDataHelper,
        DataObjectFactory $dataObjectFactory,
        CountryFactory $countryFactory
    ) {
        parent::__construct(
            $scopeConfig,
            $logger,
            $transactionBuilder,
            $requestDataHelper,
            $dataObjectFactory,
            $countryFactory
        );
        $this->dataObjectFactory = $dataObjectFactory;
        $this->countryFactory = $countryFactory;
    }

    /**
     * Get PayPal order details
     * Rewrite by Synolia for getting payer and address status
     *
     * @param \stdClass $request
     *
     * @return array
     * @throws \Exception
     */
    public function getDetailsService($request)
    {
        $this->logger->info('SOAP ' . __METHOD__);
        $response = null;
        try {
            $result = $this->client->runTransaction($request);

            if (null !== $result && 100 == $result->reasonCode) {
                $shippingAddress = $this->convertPayPalAddressToAddress($result->payPalEcGetDetailsReply);

                $response = [
                    'paypalToken' => $result->payPalEcGetDetailsReply->paypalToken,
                    'paypalPayerId' => $result->payPalEcGetDetailsReply->payerId,
                    'paypalEcSetRequestID' => $result->requestID,
                    'paypalEcSetRequestToken' => $result->requestToken,
                    'paypalCustomerEmail' => $result->payPalEcGetDetailsReply->payer,
                    'shippingAddress' => $shippingAddress,
                    'merchantReferenceCode' => $request->merchantReferenceCode,
                    // Start Synolia rewrite
                    'paypalPayerStatus' => $result->payPalEcGetDetailsReply->payerStatus,
                    'shippingAddressStatus' => $result->payPalEcGetDetailsReply->addressStatus,
                    // End Synolia rewrite
                ];
            } else {
                throw new LocalizedException(__("Unable to retrieve details from PayPal"));
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $response;
    }

    /**
     * Copy/Pasted function from parent because it is private
     *
     * @param array|\stdClass $data
     *
     * @return \Magento\Framework\DataObject
     */
    private function convertPayPalAddressToAddress($data)
    {
        $address = $this->dataObjectFactory->create();
        Mapper::accumulateByMap((array) $data, $address, $this->billingAddressMap);
        $address->setExportedKeys(array_values($this->billingAddressMap));

        // attempt to fetch region_id from directory
        if ($address->getCountryId() && $address->getRegion()) {
            $regions = $this->countryFactory->create()->loadByCode(
                $address->getCountryId()
            )->getRegionCollection()->addRegionCodeOrNameFilter(
                $address->getRegion()
            )->setPageSize(
                1
            );
            foreach ($regions as $region) {
                $address->setRegionId($region->getId());
                $address->setExportedKeys(array_merge($address->getExportedKeys(), ['region_id']));
                break;
            }
        }

        return $address;
    }
}
