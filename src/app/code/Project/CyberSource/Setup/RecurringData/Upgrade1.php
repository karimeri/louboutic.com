<?php

namespace Project\CyberSource\Setup\RecurringData;

use CyberSource\SecureAcceptance\Model\Payment;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManager;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\CyberSource\Plugin\SecureAcceptance\Model\Source\PaymentActionPlugin;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade1
 * @package Project\CyberSource\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade1 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        EnvironmentManager $environmentManager,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->environmentManager = $environmentManager;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->setDownCybersource();

        if ($this->environmentManager->getEnvironment() == Environment::EU) {
            $this->setupCybersource(Payment::ACTION_AUTHORIZE);
            $this->setupCybersourcePaypal();

            // Translate Credit Card for France website
            $frWebsite = $this->storeManager->getWebsite('fr');
            $this->configSetup->saveConfig(
                'payment/chcybersource/title',
                'Carte Bancaire',
                ScopeInterface::SCOPE_WEBSITES,
                $frWebsite->getWebsiteId()
            );

            // Disable Cybersource on Switzerland Website
            $chWebsite = $this->storeManager->getWebsite('ch');
            $this->setDownCybersource(ScopeInterface::SCOPE_WEBSITES, $chWebsite->getWebsiteId());
        }

        if ($this->environmentManager->getEnvironment() == Environment::US) {
            $this->setupCybersource(PaymentActionPlugin::ACTION_CREATE_TOKEN);
            $this->setupCybersourcePaypal();
        }

        if ($this->environmentManager->getEnvironment() == Environment::HK) {
            $this->setupCybersource(Payment::ACTION_AUTHORIZE);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Init Cybersource Payment Config';
    }

    /**
     * Active Cybersource on default scope
     *
     * @param string $paymentAction
     */
    protected function setupCybersource($paymentAction)
    {
        $this->configSetup->saveConfig('payment/chcybersource/active', 1);
        $this->configSetup->saveConfig('payment/chcybersource/title', 'Credit Card');
        $this->configSetup->saveConfig('payment/chcybersource/payment_action', $paymentAction);
        $this->configSetup->saveConfig('payment/chcybersource/test_mode', 1);
        $this->configSetup->saveConfig('payment/chcybersource/use_test_wsdl', 1);
        $this->configSetup->saveConfig('payment/chcybersource/auth_indicator', 0);
        $this->configSetup->saveConfig('payment/chcybersource/use_iframe', 0);
        $this->configSetup->saveConfig('payment/chcybersource/rewrite_dm', 1);
    }

    /**
     * Active Cybersource Paypal on default scope
     */
    protected function setupCybersourcePaypal()
    {
        $this->configSetup->saveConfig('payment/cybersourcepaypal/active', 1);
        $this->configSetup->saveConfig('payment/cybersourcepaypal/title', 'Paypal');
        $this->configSetup->saveConfig('payment/cybersourcepaypal/paypal_test_mode', 1);
        $this->configSetup->saveConfig('payment/cybersourcepaypal/order_status', 'processing');
        $this->configSetup->saveConfig('payment/cybersourcepaypal/paypal_redirection_type', 'traditional');
        $this->configSetup->saveConfig('payment/cybersourcepaypal/paypal_payment_action', 'authorize');
        $this->configSetup->saveConfig('payment/cybersourcepaypal/rewrite_dm', 1);
    }

    /**
     * Disable Cybersource
     *
     * @param string $scope
     * @param int $scopeId
     */
    protected function setDownCybersource($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeId = 0)
    {
        $this->configSetup->saveConfig('payment/chcybersource/active', 0, $scope, $scopeId);
        $this->configSetup->saveConfig('payment/cybersourceecheck/active', 0, $scope, $scopeId);
        $this->configSetup->saveConfig('payment/chcybersource/fingerprint_enabled', 0, $scope, $scopeId);
        $this->configSetup->saveConfig('payment/chcybersource/address_check_enabled', 0, $scope, $scopeId);
        $this->configSetup->saveConfig('payment/chcybersource/address_force_normal', 0, $scope, $scopeId);
        $this->configSetup->saveConfig('tax/cybersourcetax/tax_enabled', 0, $scope, $scopeId);
    }
}
