<?php

namespace Project\CyberSource\Setup\RecurringData;

use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade2
 * @package Project\CyberSource\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade2 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade21 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        EnvironmentManager $environmentManager,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->environmentManager = $environmentManager;
        $this->taskRepository = $taskRepository;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() != Environment::JP) {
            $task = [
                'name' => 'cron_dm_launcher',
                'active' => 1,
                'frequency' => '0 */2 * * *',
                'command' => 'synolia:sync:launch cron_dm_launcher',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ];

            $taskModel = $this->task->setData($task);
            $this->taskRepository->save($taskModel);
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Init platform cron';
    }
}
