<?php

namespace Project\CyberSource\Api\Service\Workflow;

/**
 * Interface PaymentInterface
 * @package Project\CyberSource\Api\Service
 */
interface PaymentInterface
{
    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return \stdClass|null
     */
    public function finalAuthorize($order);
}
