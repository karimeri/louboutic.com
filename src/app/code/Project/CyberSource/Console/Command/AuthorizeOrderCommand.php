<?php

namespace Project\CyberSource\Console\Command;

use CyberSource\Core\Helper\ReasonCodeHandler;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Project\CyberSource\Api\Service\Workflow\PaymentInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SwitchEnvironmentCommand
 *
 * @package   Project\Core\Console\Command
 * @author    Synolia <contact@synolia.com>
 */
class AuthorizeOrderCommand extends Command
{
    const INPUT_ORDER_ID = 'order_id';

    /**
     * @var \Project\CyberSource\Service\Workflow\Payment
     */
    protected $paymentWorkflow;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * AuthorizeOrder constructor.
     *
     * @param \Project\CyberSource\Api\Service\Workflow\PaymentInterface $paymentWorkflow
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        PaymentInterface $paymentWorkflow,
        OrderRepository $orderRepository,
        State $state
    ) {
        try {
            $state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Throwable $throwable) {
            // Nothing
        }

        parent::__construct();

        $this->paymentWorkflow = $paymentWorkflow;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('project:cybersource:authorize')
            ->setDescription('Launch a full auth request for an order')
            ->addArgument(self::INPUT_ORDER_ID, InputArgument::REQUIRED, 'Which order?');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $orderId = $input->getArgument(self::INPUT_ORDER_ID);

            $output->writeln(sprintf("Searching for order #%s ...\n", $orderId));
            $order = $this->orderRepository->get($orderId);

            $output->writeln("<info>Order found. Send auth request...</info>");
            $result = $this->paymentWorkflow->finalAuthorize($order);

            if (empty($result)) {
                $output->writeln("<error>Authorize Request Failed. See logs for error details</error>");

                return \Magento\Framework\Console\Cli::RETURN_FAILURE;
            }

            $reasonCode = $result->reasonCode ?? null;

            switch ($reasonCode) {
                case 100:
                    $this->processAccept($order, $result);
                    $output->writeln("<info>Final authorization accepted</info>");
                    break;

                case 480:
                    $this->processReview($order, $result);
                    $output->writeln("<warning>Authorization Review requested. Fraud suspected</warning>");
                    break;

                case ReasonCodeHandler::isDeclined($reasonCode):
                    $this->processDecline($order, $result);
                    $output->writeln("<error>Authorization declined. Order has been canceled</error>");
                    $output->writeln(ReasonCodeHandler::getMessageForCode($reasonCode));
                    break;
                case ReasonCodeHandler::isError($reasonCode):
                    $output->writeln("<error>Authorize Request Rejected.</error>");
                    $output->writeln(ReasonCodeHandler::getMessageForCode($reasonCode));
                    break;

                default:
                    break;
            }
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');

            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param \stdClass $result
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function processAccept(\Magento\Sales\Api\Data\OrderInterface $order, $result)
    {
        $this->updatePaymentInfo($order, $result);
        $order->setStatus('sent_to_logistic');

        return $this->orderRepository->save($order);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param $result
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function processReview(\Magento\Sales\Api\Data\OrderInterface $order, $result)
    {
        $this->updatePaymentInfo($order, $result);
        $order->setStatus(Order::STATE_PAYMENT_REVIEW);

        return $this->orderRepository->save($order);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param $result
     *
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function processDecline(\Magento\Sales\Api\Data\OrderInterface $order, $result)
    {
        $this->updatePaymentInfo($order, $result);
        $order->setState(Order::STATE_CANCELED);
        $order->setStatus(Order::STATE_CANCELED);


        return $this->orderRepository->save($order);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param $result
     */
    protected function updatePaymentInfo($order, $result)
    {
        $payment = $order->getPayment();
        $payment->setCcTransId($result->requestID);
        $payment->setLastTransId($result->requestID);
        $payment->setAdditionalInformation(
            array_merge(
                $payment->getAdditionalInformation(),
                [
                    'last_trans_id' => $result->requestID,
                    'cc_trans_id' => $result->requestID,
                    'authorized_datetime' => $result->ccAuthReply->authorizedDateTime,
                ]
            )
        );
    }
}
