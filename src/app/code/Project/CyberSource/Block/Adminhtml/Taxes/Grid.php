<?php

namespace Project\CyberSource\Block\Adminhtml\Taxes;

/**
 * Class Grid
 * @package Project\CyberSource\Block\Adminhtml\Taxes
 * @author Synolia <contact@synolia.com>
 */
class Grid extends \CyberSource\Tax\Block\Adminhtml\Taxes\Grid
{
    /**
     * @param \Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'cybersourcetax/*/edit',
            ['class_id' => $row->getId()]
        );
    }
}
