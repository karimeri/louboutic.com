<?php

namespace Project\CyberSource\Service\Workflow;

use Project\Sales\Model\Magento\Sales\Order;
use Magento\Sales\Model\OrderRepository;
use Project\CyberSource\Api\Service\Workflow\PaymentInterface;
use Magento\Sales\Model\OrderFactory;

/**
 * Class AuthorizeOrder
 * @package Project\CyberSource\Service\Workflow
 * @author  Synolia <contact@synolia.com>
 */
class AuthorizeOrder
{
    /**
     * @var Payment
     */
    protected $paymentWorkflow;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * AuthorizeOrder constructor.
     * @param \Project\CyberSource\Api\Service\Workflow\PaymentInterface $paymentWorkflow
     * @param \Magento\Sales\Model\OrderRepository                       $orderRepository
     * @param \Magento\Sales\Model\OrderFactory                          $orderFactory
     */
    public function __construct(
        PaymentInterface $paymentWorkflow,
        OrderRepository $orderRepository,
        OrderFactory $orderFactory
    ) {
        $this->paymentWorkflow = $paymentWorkflow;
        $this->orderRepository = $orderRepository;
        $this->orderFactory    = $orderFactory;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    public function fullAuthorize($order)
    {
        try {
            $result = $this->paymentWorkflow->finalAuthorize($order);

            if (empty($result)) {
                throw new \Exception('No result for full athorization. See log for more information');
            }

            $reasonCode = $result->reasonCode ?? null;

            switch ($reasonCode) {
                case 100:
                    $message = __(
                        'Amount %1 has been authorized. Transaction ID = %2',
                        round($order->getGrandTotal(), 2),
                        $result->requestID
                    );
                    $this->processResponse($order, $result, $message, Order::STATE_SENT_TO_LOGISTIC);
                    break;
                default:
                    $message = __(
                        'There is an error in processing the payment. 
                            Please try again or contact us. Transaction ID = %1, Reason code = %2',
                        $result->requestID,
                        $reasonCode
                    );
                    $this->processResponse($order, $result, $message, Order::STATE_AUTORIZATION_ERROR);
                    break;
            }
        } catch (\Exception $exception) {
            throw new \Exception('Error during full authorization : '.$exception->getMessage());
        }

        return true;
    }

    /**
     * @param \Project\Sales\Model\Magento\Sales\Order $order
     * @param                                          $result
     * @param                                          $message
     * @param                                          $state
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    protected function processResponse(Order $order, $result, $message, $state)
    {
        if (isset($result->ccAuthReply->authorizedDateTime)) {
            $this->updatePaymentInfo($order, $result);
        }

        $order->setState($state);
        $order->setStatus($state);
        $order->addStatusHistoryComment($message)
            ->setIsCustomerNotified(false);

        return $this->orderRepository->save($order);
    }

    /**
     * @param \Project\Sales\Model\Magento\Sales\Order $order
     * @param                                          $result
     */
    protected function updatePaymentInfo(Order $order, $result)
    {
        $payment = $order->getPayment();
        $payment->setCcTransId($result->requestID);
        $payment->setLastTransId($result->requestID);
        $payment->setAdditionalInformation(
            array_merge(
                $payment->getAdditionalInformation(),
                [
                    'last_trans_id' => $result->requestID,
                    'cc_trans_id' => $result->requestID,
                    'authorized_datetime' => $result->ccAuthReply->authorizedDateTime,
                ]
            )
        );
    }
}
