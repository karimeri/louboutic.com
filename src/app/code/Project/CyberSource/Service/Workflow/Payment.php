<?php

namespace Project\CyberSource\Service\Workflow;

use CyberSource\Core\Service\AbstractConnection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Project\CyberSource\Api\Service\Workflow\PaymentInterface;
use Project\CyberSource\Helper\RequestDataBuilder;
use CyberSource\Core\Model\LoggerInterface;

/**
 * Class Payment
 * @package Project\CyberSource\Service\Workflow
 * @author Synolia <contact@synolia.com>
 */
class Payment extends AbstractConnection implements PaymentInterface
{
    /**
     * @var \Project\CyberSource\Helper\RequestDataBuilder
     */
    protected $requestDataBuilder;

    /**
     * Payment constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \CyberSource\Core\Service\AbstractConnection $logger
     * @param \Project\CyberSource\Helper\RequestDataBuilder $requestDataBuilder
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger,
        RequestDataBuilder $requestDataBuilder
    ) {
        parent::__construct($scopeConfig, $logger);
        $this->requestDataBuilder = $requestDataBuilder;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return \stdClass|null
     */
    public function finalAuthorize($order)
    {
        $request = $this->requestDataBuilder->buildFinalAuthorizeRequest($order);

        $result = null;
        try {
            $this->logger->info('Send final authorize request for order ' . $order->getIncrementId());
            $this->setCredentialsByStore($order->getStoreId());
            $this->initSoapClient();
            $request->merchantID = $this->merchantId;
            $result = $this->client->runTransaction($request);
            $this->logger->info('Final authorize request for order ' . $order->getIncrementId() . 'got response');
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $result;
    }
}
