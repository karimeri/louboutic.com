<?php
/**
 * Copyright © 2017 CyberSource. All rights reserved.
 * See accompanying License.txt for applicable terms of use and license.
 */

namespace Project\CyberSource\Service;

use CyberSource\Core\Helper\RequestDataBuilder;
use CyberSource\Core\Model\Config;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\GiftMessage\Model\Message as GiftMessage;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;

class CyberSourceSoapAPI extends \CyberSource\Core\Service\CyberSourceSoapAPI
{

    /**
     * Create profile from transaction
     *
     * @param array $data
     * @param int $storeId
     * @return \stdClass
     */
    public function convertToProfile($data, $storeId = null)
    {
        $request = $this->requestDataHelper->buildTokenByTransaction($data);
        $result = null;
        try {
            if (!empty($storeId)) {
                $this->setCredentialsByStore($storeId);
                $this->initSoapClient();
            }
            $result = $this->client->runTransaction($request);
        } catch (\Exception $e) {
            $this->logger->error("convert error: " . $e->getMessage());
            $this->logger->error("dump request: " . print_r(var_dump($request), true));
        }
        return $result;
    }

}
