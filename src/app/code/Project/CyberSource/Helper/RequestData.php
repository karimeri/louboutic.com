<?php

namespace Project\CyberSource\Helper;

use Magento\Backend\Model\Auth;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * Class RequestData
 * @package Project\CyberSource\Helper
 * @author Synolia <contact@synolia.com>
 */
class RequestData
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Backend\Model\Auth
     */
    protected $authBackend;

    /**
     * RequestData constructor.
     *
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Backend\Model\Auth $authBackend
     */
    public function __construct(
        OrderCollectionFactory $orderCollectionFactory,
        Auth $authBackend
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->authBackend = $authBackend;
    }

    /**
     * @return string
     */
    public function getOrderOrigin()
    {
        return $this->authBackend->isLoggedIn() ? 'BO' : 'FO';
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return float
     */
    public function getOrderAmount($quote)
    {
        return $quote->getGrandTotal();
    }

    /**
     * @param \CyberSource\Core\Helper\AbstractDataBuilder $subject
     *
     * @return string
     */
    public function getIsRegisteredUser($subject)
    {
        return $subject->customerSession->isLoggedIn() ? 'Registered' : 'Guest';
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return string
     */
    public function getShippingMethod($quote)
    {
        return $quote->getShippingAddress()->getShippingMethod();
    }

    /**
     * @param \CyberSource\Core\Helper\AbstractDataBuilder $subject
     *
     * @return int
     */
    public function getDaysSinceAccountCreation($subject)
    {
        $customer = $subject->customerSession->getCustomer();
        $customerCreatedDate = $customer->getData('created_at');
        $customerCreatedDate = preg_replace('#[0-9]{2}:[0-9]{2}:[0-9]{2}$#', '', $customerCreatedDate);
        $creationDate = new \DateTime($customerCreatedDate);
        $accountCreationSince = $creationDate->diff(new \DateTime());

        return $accountCreationSince->days;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return string
     */
    public function getCustomerPreviouslyRefounded($quote)
    {
        $orders = $this->orderCollectionFactory->create()
            ->addFieldToFilter('customer_id', $quote->getCustomerId())
            ->addFieldToFilter('total_refunded', ['gt' => 0]);

        if ($orders->getSize() > 0) {
            return 'Yes';
        }

        return 'No';
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return int
     */
    public function getDaysSinceLastOrder($quote)
    {
        $result = 0;

        $orders = $this->orderCollectionFactory->create()
            ->addFieldToFilter('customer_id', $quote->getCustomerId())
            ->setOrder('created_at', 'desc')
            ->setPageSize(1);

        if ($orders->getSize() > 0) {
            // phpcs:ignore Ecg.Performance.GetFirstItem.Found
            $lastOrder = $orders->getFirstItem();
            $lastOrderDateCreatedAt = $lastOrder->getData('created_at');
            $lastOrderDateCreatedAt = preg_replace('#[0-9]{2}:[0-9]{2}:[0-9]{2}$#', '', $lastOrderDateCreatedAt);
            $lastOrderDate = new \DateTime($lastOrderDateCreatedAt);
            $lastOrderSince = $lastOrderDate->diff(new \DateTime());

            $result = $lastOrderSince->days;
        }

        return $result;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return string
     */
    public function getBillingName($quote)
    {
        $billingAddress = $quote->getBillingAddress();

        return $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
    }
}
