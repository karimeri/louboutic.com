<?php

namespace Project\CyberSource\Helper;

use CyberSource\Core\Model\ResourceModel\Token\Collection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;

/**
 * Class RequestDataBuilder
 * @package Project\CyberSource\Helper
 * @author Synolia <contact@synolia.com>
 */
class RequestDataBuilder
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \CyberSource\Core\Model\ResourceModel\Token\Collection
     */
    protected $tokenCollection;

    /**
     * RequestDataBuilder constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Psr\Log\LoggerInterface $logger
     * @param \CyberSource\Core\Model\ResourceModel\Token\Collection $tokenCollection
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger,
        Collection $tokenCollection
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->tokenCollection = $tokenCollection;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return \stdClass
     */
    public function buildFinalAuthorizeRequest($order)
    {
        $request = new \stdClass();
        $request->partnerSolutionID = Config::PARTNER_SOLUTION_ID;
        $developerId = $this->scopeConfig->getValue(
            Config::XML_PATH_CYBERSOURCE_DEVELOPER_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (!empty($developerId) || $developerId !== null) {
            $request->developerId = $developerId;
        }
        $request->merchantReferenceCode = $order->getIncrementId();
        $request->authIndicator = 1;

        $ccAuthService = new \stdClass();
        $ccAuthService->run = "true";
        $request->ccAuthService = $ccAuthService;

        $subscriptionInfo = new \stdClass();
        $subscriptionInfo->subscriptionID = $this->getTokenByOrder($order)->getData('payment_token');
        $request->recurringSubscriptionInfo = $subscriptionInfo;

        $request->billTo = $this->buildAddress($order->getBillingAddress(), $order->getCustomerEmail());

        $orderItems = [];
        foreach ($order->getItems() as $orderItem) {
            if ($orderItem->getQtyOrdered() >= 1) {
                $orderItems[] = $orderItem;
            }
        }
        $request = $this->buildOrderItems($orderItems, $request);


        $purchaseTotals = new \stdClass();
        $purchaseTotals->currency = $order->getOrderCurrencyCode();
        $amount = $this->formatAmount($order->getTotalDue());
        $purchaseTotals->grandTotalAmount = $amount;
        $request->purchaseTotals = $purchaseTotals;

        return $request;
    }


    /**
     * @param $items
     * @param \stdClass $request
     *
     * @return \stdClass
     */
    protected function buildOrderItems($items, \stdClass $request)
    {
        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        foreach ($items as $i => $item) {
            $requestItem = new \stdClass();
            $requestItem->id = $i;
            $requestItem->productName = $item->getName();
            $requestItem->productSKU = $item->getSku();
            $requestItem->quantity = (int) $item->getQtyOrdered();
            $requestItem->productCode = 'default';
            $requestItem->unitPrice = $this->formatAmount($item->getPrice());
            $requestItem->taxAmount = $this->formatAmount($item->getTaxAmount());
            $request->item[] = $requestItem;
        }

        foreach ($request->item as $key => $item) {
            if ($item->unitPrice == 0) {
                unset($request->item[$key]);
            }
        }

        $request->item = array_values($request->item);

        return $request;
    }

    /**
     * @param \Magento\Sales\Model\Order\Address $orderAddress
     * @param string $customerEmail
     *
     * @return \stdClass
     */
    protected function buildAddress($orderAddress, $customerEmail)
    {
        $address = new \stdClass();
        $address->city = $orderAddress->getCity();
        $address->country = $orderAddress->getCountryId();
        $address->postalCode = $orderAddress->getPostcode();
        $address->state = $orderAddress->getRegionCode();
        $address->street1 = $orderAddress->getStreetLine(1);
        $address->email = $customerEmail;
        $address->firstName = $orderAddress->getFirstname();
        $address->lastName = $orderAddress->getLastname();

        return $address;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return \CyberSource\SecureAcceptance\Model\Token|null
     */
    protected function getTokenByOrder($order)
    {
        $this->tokenCollection->addFieldToFilter('order_id', $order->getEntityId());
        $this->tokenCollection->load();
        if ($this->tokenCollection->getSize()) {
            /** @var \CyberSource\SecureAcceptance\Model\Token $token */
            //phpcs:ignore Ecg.Performance.GetFirstItem.Found
            $token = $this->tokenCollection->getFirstItem();

            return $token;
        }

        return null;
    }

    /**
     * @param float $amount
     *
     * @return string
     */
    protected function formatAmount($amount)
    {
        if (!is_float($amount)) {
            $amount = (float) $amount;
        }

        return number_format($amount, 2, '.', '');
    }
}
