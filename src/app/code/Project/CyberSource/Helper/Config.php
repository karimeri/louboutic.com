<?php

namespace Project\CyberSource\Helper;

use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package    Project\Cybersource\Helper
 * @author    Synolia <contact@synolia.com>
 */
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_CYBERSOURCE_DM_REWRITE = 'payment/chcybersource/rewrite_dm';
    const XML_PATH_CYBERSOURCE_PAYPAL_DM_REWRITE = 'payment/cybersourcepaypal/rewrite_dm';
    const XML_PATH_CYBERSOURCE_DEVELOPER_ID = 'payment/chcybersource/developer_id';

    const PARTNER_SOLUTION_ID = 'T54H9OLO';

    /**
     * @param null|string $store
     *
     * @return mixed
     */
    public function isDMRewriteEnabled($store = null)
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_CYBERSOURCE_DM_REWRITE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null|string $store
     *
     * @return mixed
     */
    public function isPaypalDMRewriteEnabled($store = null)
    {
        return $this->scopeConfig->getValue(
            static::XML_PATH_CYBERSOURCE_PAYPAL_DM_REWRITE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }
}
