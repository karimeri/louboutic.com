<?php

namespace Project\CyberSource\Plugin\SecureAcceptance\Model\Source;

use CyberSource\SecureAcceptance\Model\Source\PaymentAction;

/**
 * Class PaymentActionPlugin
 * @package Project\CyberSource\Plugin\SecureAcceptance\Model\Source
 * @author Synolia <contact@synolia.com>
 */
class PaymentActionPlugin
{

    const ACTION_CREATE_TOKEN = 'create_token';

    /**
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterToOptionArray(PaymentAction $subject, $options)
    {
        $options[] = [
            'value' => self::ACTION_CREATE_TOKEN,
            'label' => __('Create Token Only'),
        ];

        return $options;
    }
}
