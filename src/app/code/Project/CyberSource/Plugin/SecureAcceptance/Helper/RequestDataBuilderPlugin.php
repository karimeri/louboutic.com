<?php

namespace Project\CyberSource\Plugin\SecureAcceptance\Helper;

use CyberSource\SecureAcceptance\Helper\RequestDataBuilder as PluggedClass;
use CyberSource\SecureAcceptance\Gateway\Config\Config as GatewayConfig;
use Magento\Quote\Model\Quote;
use Project\CyberSource\Helper\Config;
use Project\CyberSource\Helper\RequestData;
use Project\CyberSource\Plugin\SecureAcceptance\Model\Source\PaymentActionPlugin;

/**
 * Class RequestDataBuilderPlugin
 * @package Project\CyberSource\Plugin\SecureAcceptance\Helper
 * @author Synolia <contact@synolia.com>
 */
class RequestDataBuilderPlugin
{
    const CREATE_TOKEN_URL = 'create_token_url';
    const CREATE_TOKEN_TEST_URL = 'create_token_test_url';

    /**
     * @var array
     */
    protected $requestUrls = [
        self::CREATE_TOKEN_URL => 'https://secureacceptance.cybersource.com/embedded/pay',
        self::CREATE_TOKEN_TEST_URL => 'https://testsecureacceptance.cybersource.com/embedded/pay',
    ];

    /**
     * @var \Project\CyberSource\Helper\RequestData
     */
    protected $requestDataHelper;

    /**
     * @var \Project\Cybersource\Helper\Config
     */
    protected $configHelper;

    /**
     * @var \CyberSource\SecureAcceptance\Model\Config
     */
    protected $gatewayConfig;

    /**
     * RequestDataBuilderPlugin constructor.
     *
     * @param \Project\CyberSource\Helper\RequestData $requestDataHelper
     * @param \Project\Cybersource\Helper\Config $configHelper
     * @param \CyberSource\SecureAcceptance\Model\Config $gatewayConfig
     */
    public function __construct(
        RequestData $requestDataHelper,
        Config $configHelper,
        GatewayConfig $gatewayConfig
    ) {
        $this->requestDataHelper = $requestDataHelper;
        $this->configHelper = $configHelper;
        $this->gatewayConfig = $gatewayConfig;
    }

    /**
     * Use Custom Decision Manager Fields
     *
     * @param \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject
     * @param callable $proceed
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address $billingAddress
     * @param array $params
     *
     * @return array
     */
    public function aroundBuildDecisionManagerFields(
        PluggedClass $subject,
        callable $proceed,
        Quote $quote,
        Quote\Address $billingAddress,
        $params
    ) {
        $result = $proceed($quote, $billingAddress, $params);

        if ($this->configHelper->isDMRewriteEnabled()) {
            //Order Origin (eg FO/BO)
            $result['merchant_defined_data1'] = $this->requestDataHelper->getOrderOrigin();
            //Total Order Amount
            $result['merchant_defined_data2'] = $this->requestDataHelper->getOrderAmount($quote);
            //Guest or Registered
            $result['merchant_defined_data3'] = $this->requestDataHelper->getIsRegisteredUser($subject);
            //Shipping method
            $result['merchant_defined_data4'] = $this->requestDataHelper->getShippingMethod($quote);
            //Account creation days number
            $result['merchant_defined_data5'] = $this->requestDataHelper->getDaysSinceAccountCreation($subject);
            //Customer previously refunded
            $result['merchant_defined_data6'] = $this->requestDataHelper->getCustomerPreviouslyRefounded($quote);
            //Number of days since last purchase
            $result['merchant_defined_data7'] = $this->requestDataHelper->getDaysSinceLastOrder($quote);
            //Billing Name
            $result['merchant_defined_data8'] = $this->requestDataHelper->getBillingName($quote);

            $merchantDefinedDataFields = [
                'merchant_defined_data1',
                'merchant_defined_data2',
                'merchant_defined_data3',
                'merchant_defined_data4',
                'merchant_defined_data5',
                'merchant_defined_data6',
                'merchant_defined_data7',
                'merchant_defined_data8',
            ];
            $signedFieldNames = explode(',', $result['signed_field_names']);
            foreach ($merchantDefinedDataFields as $mddField) {
                if (!in_array($mddField, $signedFieldNames)) {
                    $signedFieldNames[] = $mddField;
                }
            }

            $result['signed_field_names'] = implode(',', $signedFieldNames);
        }

        $result['merchant_defined_data22'] = substr($result['merchant_defined_data22'], 0, 100);

        return $result;
    }

    /**
     * Fix null params values signed as empty strings instead of 'null'
     *
     * @param \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject
     * @param array $params
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeBuildDataToSign(PluggedClass $subject, $params)
    {
        foreach ($params as &$param) {
            if ($param === null) {
                $param = 'null';
            }
        }

        return [$params];
    }

    /**
     * Handle "Create Token" payment action
     *
     * @param \CyberSource\SecureAcceptance\Helper\RequestDataBuilder $subject
     * @param array $params
     *
     * @return array
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterBuildRequestData(PluggedClass $subject, $params)
    {
        $paymentAction = $this->gatewayConfig->getPaymentAction();

        if (PaymentActionPlugin::ACTION_CREATE_TOKEN === $paymentAction) {
            $params['request_url'] = $this->requestUrls[self::CREATE_TOKEN_URL];

            if ($this->gatewayConfig->isTestMode()) {
                $params['request_url'] = $this->requestUrls[self::CREATE_TOKEN_TEST_URL];
            }

            $params['transaction_type'] = 'authorization,create_payment_token';
            $params['amount'] = '1.00';

            // Recalculate signature since signed fields has been changed
            $params['signature'] = $subject->sign($params, $this->gatewayConfig->getSecretKey());
        }

        return $params;
    }
}
