<?php

namespace Project\CyberSource\Plugin\SecureAcceptance\Controller\Index;

use CyberSource\SecureAcceptance\Controller\Index\Receipt as PluggedClass;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

/**
 * Class Receipt
 * @package Project\CyberSource\Plugin\SecureAcceptance\Controller\Index
 * @author Synolia <contact@synolia.com>
 */
class ReceiptPlugin
{
    const MAPPING_FIELDS = [
        'bill_to_phone' => 'Phone number (Billing)',
        'ship_to_phone' => 'Phone number (Shipping)',
        'bill_to_surname' => 'Surname (Billing)',
        'bill_to_forename' => 'Forename (Billing)',
        'ship_to_forename' => 'Forename (Shipping)',
        'ship_to_surname' => 'Surname (Shipping)',
        'ship_to_address_postal_code' => 'Postal code (Shipping)',
        'bill_to_address_postal_code' => 'Postal code (Billing)',
        'bill_to_address_country' => 'Country (Billing)',
        'ship_to_address_country' => 'Country (Shipping)',
        'ship_to_address_city' => 'City (Shipping)',
        'bill_to_address_city' => 'City (Billing)',
        'bill_to_address_line1' => 'Address (Billing)',
        'ship_to_address_line1' => 'Address (Shipping)',
        'bill_to_email' => 'Email (Billing)',
        'ship_to_email' => 'Email (Shipping)'
    ];
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * Receipt constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     */
    public function __construct(
        ManagerInterface $messageManager,
        UrlInterface $url,
        ResultFactory $resultFactory
    ) {
        $this->messageManager = $messageManager;
        $this->url = $url;
        $this->resultFactory = $resultFactory;
    }

    /**
     * @param \CyberSource\SecureAcceptance\Controller\Index\Receipt $subject
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function beforeExecute(PluggedClass $subject)
    {
        $responses = $subject->getRequest()->getParams();
        $url = $this->url->getUrl('checkout/cart');

        if (!array_key_exists('transaction_id', $responses) && !empty($responses['invalid_fields'])) {
            $invalidFields = explode(',', $responses['invalid_fields']);

            foreach ($invalidFields as $invalidField) {
                $invalidFieldsTrad[] = __(self::MAPPING_FIELDS[$invalidField]) ?? '';
            }

            $errorMessage = __('Please check your personal information : ');
            $errorMessage .= implode(',', $invalidFieldsTrad);
            $this->messageManager->addErrorMessage($errorMessage);
            return $this->processResponse($url);
        }
    }

    /**
     * @param $url
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processResponse($url)
    {
        $html = '<html>
                    <body>
                        <script type="text/javascript">
                            window.onload = function() {
                                window.top.location.href = "'.$url.'";
                            };
                        </script>
                    </body>
                </html>';

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultRedirect->setContents($html);
        return $resultRedirect;
    }
}
