<?php

namespace Project\CyberSource\Plugin\PayPal\Helper;

use CyberSource\Paypal\Helper\RequestDataBuilder as PluggedClass;
use Magento\Checkout\Model\Session as CheckoutSession;
use Project\CyberSource\Helper\Config;
use Project\CyberSource\Helper\RequestData;

/**
 * Class RequestDataBuilderPlugin
 * @package Project\CyberSource\Plugin\Paypal\Helper
 * @author    Synolia <contact@synolia.com>
 */
class RequestDataBuilderPlugin
{
    /**
     * @var \Project\CyberSource\Helper\RequestData
     */
    protected $requestDataHelper;

    /**
     * @var \Project\Cybersource\Helper\Config
     */
    protected $configHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * RequestDataBuilderPlugin constructor.
     *
     * @param \Project\CyberSource\Helper\RequestData $requestDataHelper
     * @param \Project\Cybersource\Helper\Config $configHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        RequestData $requestDataHelper,
        Config $configHelper,
        CheckoutSession $checkoutSession
    ) {
        $this->requestDataHelper = $requestDataHelper;
        $this->configHelper = $configHelper;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \CyberSource\PayPal\Helper\RequestDataBuilder $subject
     * @param callable $proceed
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return \stdClass
     */
    public function aroundBuildDecisionManagerFields(
        PluggedClass $subject,
        callable $proceed,
        $quote
    ) {
        $merchantDefinedData = $proceed($quote);

        if ($this->configHelper->isPaypalDMRewriteEnabled()) {
            //Order Origin (eg FO/BO)
            $merchantDefinedData->field1 = $this->requestDataHelper->getOrderOrigin();
            //Total Order Amount
            $merchantDefinedData->field2 = $this->requestDataHelper->getOrderAmount($quote);
            //Guest or Registered
            $merchantDefinedData->field3 = $this->requestDataHelper->getIsRegisteredUser($subject);
            //Shipping method
            $merchantDefinedData->field4 = $this->requestDataHelper->getShippingMethod($quote);
            //Account creation days number
            $merchantDefinedData->field5 = $this->requestDataHelper->getDaysSinceAccountCreation($subject);
            //Customer previously refunded
            $merchantDefinedData->field6 = $this->requestDataHelper->getCustomerPreviouslyRefounded($quote);
            //Number of days since last purchase
            $merchantDefinedData->field7 = $this->requestDataHelper->getDaysSinceLastOrder($quote);
            //Billing Name
            $merchantDefinedData->field8 = $this->requestDataHelper->getBillingName($quote);

            $detailsResponse = $this->checkoutSession->getGetDetailsResponse();

            //Paypal user email
            $merchantDefinedData->field9 = $detailsResponse['paypalCustomerEmail'] ?? null;
            //Paypal user ID
            $merchantDefinedData->field10 = $detailsResponse['paypalPayerId'] ?? null;
            //Paypal user status
            $merchantDefinedData->field11 = $detailsResponse['paypalPayerStatus'] ?? null;
            //Paypal address Status
            $merchantDefinedData->field12 = $detailsResponse['shippingAddressStatus'] ?? null;

            /** @var \Magento\Framework\DataObject $shippingAddress */
            $shippingAddress = $detailsResponse['shippingAddress'] ?? null;

            if ($shippingAddress) {
                //Paypal user first name
                $merchantDefinedData->field13 = $shippingAddress->getData('firstname');
                //Paypal user last name
                $merchantDefinedData->field14 = $shippingAddress->getData('lastname');
                //Paypal address street
                $merchantDefinedData->field15 = $shippingAddress->getData('street');
                //Paypal address city
                $merchantDefinedData->field16 = $shippingAddress->getData('city');
                //Paypal address state
                $merchantDefinedData->field17 = $shippingAddress->getData('region');
                //Paypal address postal code
                $merchantDefinedData->field18 = $shippingAddress->getData('postcode');
                //Paypal Merchant Protection
                $merchantDefinedData->field19 = 'Elibility';
            }
        }

        return $merchantDefinedData;
    }
}
