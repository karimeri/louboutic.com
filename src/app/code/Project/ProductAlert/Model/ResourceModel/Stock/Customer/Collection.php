<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Project\ProductAlert\Model\ResourceModel\Stock\Customer;

/**
 * ProductAlert Stock Customer and Guest collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Collection extends \Magento\Customer\Model\ResourceModel\Customer\Collection
{
    /**
     * joinRight productalert stock data to customer and Guest collection
     *
     * @param int $productId
     * @param int $websiteId
     * @return $this
     */
    public function join($productId, $websiteId)
    {
        $this->getSelect()->reset()->from(
            ['ce' => $this->getTable('customer_entity')],
            ['entity_id', 'website_id', 'firstname', 'lastname']
        );

        $this->getSelect()->joinRight(
            ['alert' => $this->getTable('product_alert_stock')],
            'alert.customer_id=ce.entity_id',
            ['email', 'alert_stock_id', 'add_date', 'send_date', 'send_count', 'status']
        );

        $this->getSelect()->where('alert.product_id=?', $productId);
        if ($websiteId) {
            $this->getSelect()->where('alert.website_id=?', $websiteId);
        }
        $this->_setIdFieldName('alert_stock_id');
        $this->addAttributeToSelect('*');

        return $this;
    }
}
