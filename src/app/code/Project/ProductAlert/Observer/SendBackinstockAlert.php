<?php

namespace Project\ProductAlert\Observer;

use Magento\Framework\Event\Observer;

class SendBackinstockAlert implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Project\BackInStock\Model\Observer
     */
    protected $backInStockObserver;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistryStorage $stockRegistryStorage
     */
    protected $stockRegistryStorage;

    /**
     * @var \Magento\CatalogInventory\Api\StockConfigurationInterface
     */
    protected $stockConfiguration;

    /**
     * SendBackinstockAlert constructor.
     * @param \Project\BackInStock\Model\Observer $backInStockObserver
     * @param \Magento\CatalogInventory\Model\StockRegistryStorage $stockRegistryStorage
     * @param \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
     */
    public function __construct(
        \Project\BackInStock\Model\Observer $backInStockObserver,
        \Magento\CatalogInventory\Model\StockRegistryStorage $stockRegistryStorage,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration
    )
    {
        $this->backInStockObserver = $backInStockObserver;
        $this->stockRegistryStorage = $stockRegistryStorage;
        $this->stockConfiguration = $stockConfiguration;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $stockItem = $observer->getEvent()->getData('item');

        if (!$stockItem) {
            return;
        }

        $scopeId = $this->stockConfiguration->getDefaultScopeId();

        $newStockStatus = $stockItem->getIsInStock();
        $oldStockStatus = $this->stockRegistryStorage
            ->getStockStatus($stockItem->getProductId(), $scopeId);
        $oldStockStatus = $oldStockStatus ? $oldStockStatus->getStockStatus() : 0;

        if(!$oldStockStatus && $newStockStatus){
            $this->backInStockObserver->process($stockItem->getProductId());
        }
    }
}
