<?php

namespace Project\Cookie\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;

/**
 * Class InstallData
 * @package Project\Cookie\Setup
 * @author Marwen JELLOUL
 */
class InstallData implements InstallDataInterface
{
    const XML_PATH_COOKIE_RESTRICTION = 'web/cookie/cookie_restriction';

    /**
     * @var ResourceConfig
     */
    private $resourceConfig;

    /**
     * InstallData constructor.
     * @param ResourceConfig $resourceConfig
     */
    public function __construct(
        ResourceConfig $resourceConfig
    ) {
        $this->resourceConfig = $resourceConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $this->resourceConfig->saveConfig(
            self::XML_PATH_COOKIE_RESTRICTION,
            '0',
            'default',
            '0'
        );
    }
}
