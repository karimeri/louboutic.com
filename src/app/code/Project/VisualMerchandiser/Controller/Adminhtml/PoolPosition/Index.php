<?php

namespace Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition;

use Magento\Framework\Exception\NotFoundException;
use Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition;

/**
 * Class Index
 * @package Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition
 */
class Index extends PoolPosition
{

    /**
     * Index action
     *
     * @return void
     * @throws NotFoundException
     */
    public function execute()
    {
        throw new NotFoundException(__('Page not found.'));
    }
}
