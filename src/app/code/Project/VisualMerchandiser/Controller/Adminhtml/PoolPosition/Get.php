<?php

namespace Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition;

/**
 * Class Get
 * @package Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition
 */
class Get extends PoolPosition implements HttpPostActionInterface
{

    /**
     * Get pool products positions from cache
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $cacheKey = $this->getRequest()->getParam(
            \Project\VisualMerchandiser\Model\Position\PoolCache::POSITION_POOL_CACHE_KEY
        );

        $positions = \Zend_Json::encode($this->cache->getPositions($cacheKey));

        $resultJson->setData($positions);

        return $resultJson;
    }
}
