<?php

namespace Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition;

/**
 * Class Save
 * @package Project\VisualMerchandiser\Controller\Adminhtml\PoolPosition
 */
class Save extends PoolPosition implements HttpPostActionInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param SerializerInterface|null $serializer
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        SerializerInterface $serializer = null
    ) {
        parent::__construct($context, $poolCache, $resultJsonFactory);
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(SerializerInterface::class);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $cacheKey = $this->getRequest()->getParam(
            \Project\VisualMerchandiser\Model\Position\PoolCache::POSITION_POOL_CACHE_KEY
        );

        $positions = $this->getRequest()->getParam('positions', false) ?
            $this->getRequest()->getParam('positions', false) : [];
        $decodedPositions = $this->serializer->unserialize($positions);

        $this->cache->saveData(
            $cacheKey,
            $decodedPositions,
            $this->getRequest()->getParam('sort_order', null)
        );

        $resultJson->setData([]);
        return $resultJson;
    }
}
