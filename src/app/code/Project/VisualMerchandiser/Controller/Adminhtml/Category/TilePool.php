<?php

namespace Project\VisualMerchandiser\Controller\Adminhtml\Category;

use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Class TilePool
 * @package Project\VisualMerchandiser\Controller\Adminhtml\Category
 */
class TilePool extends AbstractGrid implements HttpPostActionInterface
{
    /**
     * @var string
     */
    protected $blockClass = \Project\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\TilePool::class;

    /**
     * @var string
     */
    protected $blockName = 'tile_pool';
}
