<?php

namespace Project\VisualMerchandiser\Controller\Adminhtml\Category;

use Magento\Framework\App\Action\HttpPostActionInterface;

/**
 * Class GridPool
 * @package Project\VisualMerchandiser\Controller\Adminhtml\Category
 */
class GridPool extends AbstractGrid implements HttpPostActionInterface
{
    /**
     * @var string
     */
    protected $blockClass = \Project\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\GridPool::class;

    /**
     * @var string
     */
    protected $blockName = 'grid_pool';
}
