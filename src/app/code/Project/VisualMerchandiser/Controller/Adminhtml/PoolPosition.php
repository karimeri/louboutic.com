<?php

namespace Project\VisualMerchandiser\Controller\Adminhtml;

/**
 * Class PoolPosition
 * @package Project\VisualMerchandiser\Controller\Adminhtml
 */
abstract class PoolPosition extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Catalog::categories';

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Project\VisualMerchandiser\Model\Position\PoolCache
     */
    protected $cache;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->cache = $poolCache;
    }
}
