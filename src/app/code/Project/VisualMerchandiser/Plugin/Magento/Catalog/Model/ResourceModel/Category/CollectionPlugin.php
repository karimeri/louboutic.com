<?php

namespace Project\VisualMerchandiser\Plugin\Magento\Catalog\Model\ResourceModel\Category;

/**
 * Class CollectionPlugin
 * @package Project\VisualMerchandiser\Plugin\Magento\Catalog\Model\ResourceModel\Category
 */
class CollectionPlugin
{
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Category\Collection $subject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeLoad(\Magento\Catalog\Model\ResourceModel\Category\Collection $subject)
    {
        $subject->addAttributeToSelect('to_merch');
    }
}
