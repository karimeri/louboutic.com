<?php

namespace Project\VisualMerchandiser\Plugin\Magento\Catalog\Controller\Adminhtml\Category;

use \Magento\Catalog\Controller\Adminhtml\Category\Edit as EditController;

/**
 * Class EditPlugin
 * @package Project\VisualMerchandiser\Plugin\Magento\Catalog\Controller\Adminhtml\Category
 */
class EditPlugin
{
    /**
     * @var \Project\VisualMerchandiser\Model\Position\PoolCache
     */
    protected $cache;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache,
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
        $this->cache = $poolCache;
    }

    /**
     * Register the cache key before controller is executed
     *
     * @param $subject EditController
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeExecute(EditController $subject)
    {
        $this->registry->register(
            \Project\VisualMerchandiser\Model\Position\PoolCache::POSITION_POOL_CACHE_KEY,
            uniqid()
        );
    }
}
