<?php

namespace Project\VisualMerchandiser\Plugin\Magento\VisualMerchandiser\Model\Category;

use Magento\VisualMerchandiser\Model\Category\Products;

/**
 * Class ProductsPlugin
 * @package Project\VisualMerchandiser\Plugin\Magento\VisualMerchandiser\Model\Category
 * @author Synolia <contact@synolia.com>
 */
class ProductsPlugin
{
    /**
     * @param \Magento\VisualMerchandiser\Model\Category\Products $subject
     * @param $collection
     * @return mixed
     */
    public function afterGetCollectionForGrid(
        Products $subject,
        $collection
    ) {
        $collection->getSelect()->joinLeft(
            ['super_link' => 'catalog_product_super_link'],
            'super_link.product_id = e.entity_id',
            []
        )->where('super_link.product_id IS NULL ');

        return $collection;
    }
}
