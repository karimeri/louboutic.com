<?php

namespace Project\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status as StatusSource;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Tile
 * @package Project\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser
 */
class Tile extends \Magento\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\Tile
{
    /**
     * @var string
     * @since 100.1.0
     */
    protected $_template = 'category/merchandiser/tile.phtml';

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var Product\Visibility
     */
    protected $productVisibility;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * @var int
     */
    protected $_defaultLimit = 500;

    /**
     * Tile constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Catalog\Helper\Image $catalogImage
     * @param \Magento\VisualMerchandiser\Model\Category\Products $products
     * @param \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Factory $attributeFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param StockRegistryInterface $stockRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Helper\Image $catalogImage,
        \Magento\VisualMerchandiser\Model\Category\Products $products,
        \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Factory $attributeFactory,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        StockRegistryInterface $stockRegistry,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $coreRegistry, $catalogImage, $products, $attributeFactory, $data);
        $this->priceCurrency = $priceCurrency;
        $this->productVisibility = $productVisibility;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @return string
     */
    protected function _getPositionCacheKey()
    {
        return $this->getPositionCacheKey() ?? $this->getParentBlock()->getPositionCacheKey();
    }

    /**
     * @return \Magento\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\Tile
     * @since 100.1.0
     */
    protected function _prepareCollection()
    {
        $this->_products->setCacheKey($this->_getPositionCacheKey());

        $collection = $this->_products->getCollectionForGrid(
            (int)$this->getRequest()->getParam('id', 0),
            $this->getRequest()->getParam('store')
        );

        $collection->addAttributeToSelect('visibility');
        $collection->setVisibility($this->productVisibility->getVisibleInSiteIds());

        $collection->clear();
        $this->setCollection($collection);

        $this->_preparePage();

        $idx = ($collection->getCurPage() * $collection->getPageSize()) - $collection->getPageSize();

        foreach ($collection as $item) {
            $item->setPosition($idx);
            $idx++;
        }
        $this->_products->savePositions($collection);

        return $this;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function isProductDisabled(Product $product)
    {
        return $product->getStatus() != StatusSource::STATUS_ENABLED;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     * @since 100.1.0
     */
    public function getAttributesToDisplay($product)
    {
        $attributeCodes = $this->getUsableAttributes();
        $availableAttributes = $product->getTypeInstance()->getSetAttributes($product);
        $availableFields = array_keys($product->getData());
        $filteredAttributes = [];

        foreach ($attributeCodes as $code) {
            $renderer = $this->attributeFactory->create($code);

            if ($code == 'price') {
                $price = $this->priceCurrency->format($product->getPrice());
                if ($product->getTypeId() == 'configurable') {
                    $children = $product->getTypeInstance()->getUsedProducts($product);
                    if(!empty($children))
                        $price = $this->priceCurrency->format($children[0]->getData('price'));
                }
                $attributeObject = $availableAttributes[$code];
                $filteredAttributes[] = $renderer->addData([
                    'label' => $attributeObject->getFrontend()->getLabel(),
                    'value' => $price
                ]);
            } elseif ($code == 'stock') {
                $filteredAttributes[] = $renderer->addData([
                    'label' => ucwords($code),
                    'value' => $this->getStock($product)
                ]);
            } elseif (isset($availableAttributes[$code])) {
                $attributeObject = $availableAttributes[$code];
                $filteredAttributes[] = $renderer->addData([
                    'label' => $attributeObject->getFrontend()->getLabel(),
                    'value' => $product->getData($code)
                ]);
            } elseif (in_array($code, $availableFields)) {
                $filteredAttributes[] = $renderer->addData([
                    'label' => ucwords($code),
                    'value' => $product->getData($code)
                ]);
            }
        }

        return $filteredAttributes;
    }

    /**
     * @param Product $product
     * @return int|mixed
     */
    protected function getStock(Product $product)
    {
        $stock = 0;
        if ($product->getTypeId() === Configurable::TYPE_CODE) {
            /** @var Configurable $productType */
            $productType = $product->getTypeInstance();
            $children = $productType->getUsedProducts($product);
            foreach ($children as $child) {
                $stockItem = $this->stockRegistry->getStockItem($child->getId());
                $stock += $stockItem->getQty();
            }
        } else {
            $stock = $product->getData('stock');
        }

        return $stock;
    }
}
