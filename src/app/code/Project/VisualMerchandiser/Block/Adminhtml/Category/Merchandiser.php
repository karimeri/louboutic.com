<?php

namespace Project\VisualMerchandiser\Block\Adminhtml\Category;

/**
 * Class Merchandiser
 * @package Project\VisualMerchandiser\Block\Adminhtml\Category
 */
class Merchandiser extends \Magento\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser
{
    /**
     * @var \Project\VisualMerchandiser\Model\Position\PoolCache
     */
    protected $poolCache;

    /**
     * @var \Project\VisualMerchandiser\Helper\Config
     */
    protected $configHelper;

    /**
     * Merchandiser constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\VisualMerchandiser\Model\Position\Cache $cache
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     * @param \Project\VisualMerchandiser\Helper\Config $configHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\VisualMerchandiser\Model\Position\Cache $cache,
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache,
        \Project\VisualMerchandiser\Helper\Config $configHelper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $cache, $data);
        $this->poolCache = $poolCache;
        $this->configHelper = $configHelper;
    }

    /**
     * @return bool
     */
    public function isAllowed()
    {
        return $this->_authorization->isAllowed('Project_VisualMerchandiser::sort_order_toolbar');
    }

    /**
     * @return string
     * @since 100.1.0
     */
    public function getDialogUrl()
    {
        return $this->getUrl(
            'merchandiser/*/addproduct',
            [
                'cache_key' => $this->getPositionCacheKey() . ':' . $this->getPoolPositionCacheKey(),
                'componentJson' => true
            ]
        );
    }

    /**
     * @return string
     * @since 100.1.0
     */
    public function getSavePoolPositionsUrl()
    {
        return $this->getUrl('merchandiser/poolposition/save');
    }

    /**
     * Get products positions url
     *
     * @return string
     * @since 100.1.0
     */
    public function getProductsPoolPositionsUrl()
    {
        return $this->getUrl('merchandiser/poolposition/get');
    }

    /**
     * @return string
     * @since 100.1.0
     */
    public function getPoolPositionCacheKey()
    {
        return $this->_coreRegistry->registry($this->getPoolPositionCacheKeyName());
    }

    /**
     * @return string
     * @since 100.1.0
     */
    public function getPoolPositionCacheKeyName()
    {
        return \Project\VisualMerchandiser\Model\Position\PoolCache::POSITION_POOL_CACHE_KEY;
    }

    /**
     * @return string
     */
    public function getPoolPositionDataJson()
    {
        return \Zend_Json::encode($this->poolCache->getPositions($this->getPoolPositionCacheKey()));
    }

    /**
     * @return bool
     */
    public function hideDisabledProducts()
    {
        return $this->configHelper->hideDisabledProducts();
    }
}
