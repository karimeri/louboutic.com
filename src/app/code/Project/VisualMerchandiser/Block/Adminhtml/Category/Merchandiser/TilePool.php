<?php

namespace Project\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser;

use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

/**
 * Class TilePool
 * @package Project\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser
 */
class TilePool extends \Magento\Backend\Block\Widget\Grid
{
    const XML_PATH_ADDITIONAL_ATTRIBUTES = 'visualmerchandiser/options/product_attributes';
    const IMAGE_WIDTH = 130;
    const IMAGE_HEIGHT = 130;

    /**
     * @var string
     * @since 100.1.0
     */
    protected $_template = 'Project_VisualMerchandiser::category/merchandiser/tile_pool.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     * @since 100.1.0
     */
    protected $_coreRegistry = null;

    /**
     * Collection object
     *
     * @var \Magento\Framework\Data\Collection
     * @since 100.1.0
     */
    protected $_collection;

    /**
     * Catalog image
     *
     * @var \Magento\Catalog\Helper\Image
     * @since 100.1.0
     */
    protected $_catalogImage = null;

    /**
     * @var \Project\VisualMerchandiser\Model\Category\Products
     */
    protected $_products;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     * @since 100.1.0
     */
    protected $scopeConfig;

    /**
     * @var array
     * @since 100.1.0
     */
    protected $usableAttributes = null;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     * @since 100.1.0
     */
    protected $attributeFactory;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * TilePool constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Catalog\Helper\Image $catalogImage
     * @param \Project\VisualMerchandiser\Model\Category\Products $products
     * @param \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Factory $attributeFactory
     * @param StockRegistryInterface $stockRegistry
     * @param array $data
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface|null $priceCurrency
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Helper\Image $catalogImage,
        \Project\VisualMerchandiser\Model\Category\Products $products,
        \Magento\VisualMerchandiser\Block\Adminhtml\Widget\Tile\Attribute\Factory $attributeFactory,
        StockRegistryInterface $stockRegistry,
        array $data = [],
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency = null
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_catalogImage = $catalogImage;
        $this->_products = $products;
        $this->scopeConfig = $context->getScopeConfig();
        $this->attributeFactory = $attributeFactory;
        $this->priceCurrency = $priceCurrency ?: \Magento\Framework\App\ObjectManager::getInstance()->get(
            \Magento\Framework\Pricing\PriceCurrencyInterface::class
        );
        parent::__construct($context, $backendHelper, $data);
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @inheritdoc
     *
     * @since 100.1.0
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDefaultSort('position');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
    }

    /**
     * Get image helper.
     *
     * @return \Magento\Catalog\Helper\Image
     * @since 100.1.0
     */
    public function getImageHelper()
    {
        return $this->_catalogImage;
    }

    /**
     * Retrieve product image url.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     * @since 100.1.0
     */
    public function getImageUrl($product)
    {
        $image = $this->getImageHelper()
            ->init($product, 'small_image', ['type' => 'small_image'])
            ->resize(self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        return $image->getUrl();
    }

    /**
     * Initialize grid
     *
     * @return void
     * @since 100.1.0
     */
    protected function _prepareGrid()
    {
        $this->_prepareCollection();
    }

    /**
     * Retrieve position cache key.
     *
     * @return string
     * @since 100.1.0
     */
    protected function _getPoolPositionCacheKey()
    {
        return $this->getPoolPositionCacheKey() ?
            $this->getPoolPositionCacheKey() :
            $this->getParentBlock()->getPoolPositionCacheKey();
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection()
    {
        $this->_products->setPoolCacheKey($this->getPoolPositionCacheKey());

        $collection = $this->_products->getCollectionForGridPool(
            (int) $this->getRequest()->getParam('id', 0),
            $this->getRequest()->getParam('store')
        );

        $collection->clear();
        $this->setCollection($collection);

        $this->_preparePage();

        $idx = ($collection->getCurPage() * $collection->getPageSize()) - $collection->getPageSize();

        foreach ($collection as $item) {
            $item->setPosition($idx);
            $idx++;
        }
        $this->_products->savePoolPositions($collection);

        return $this;
    }

    /**
     * Set collection object
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @return void
     * @since 100.1.0
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;
    }

    /**
     * Get collection object
     *
     * @return \Magento\Framework\Data\Collection
     * @since 100.1.0
     */
    public function getCollection()
    {
        return $this->_collection;
    }

    /**
     * Retrieve column by id
     *
     * @param string $columnId
     * @return \Magento\Framework\View\Element\AbstractBlock|bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @since 100.1.0
     */
    public function getColumn($columnId)
    {
        return false;
    }

    /**
     * Retrieve list of grid columns
     *
     * @return array
     * @since 100.1.0
     */
    public function getColumns()
    {
        return [];
    }

    /**
     * Process column filtration values
     *
     * @param mixed $data
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @since 100.1.0
     */
    protected function _setFilterValues($data)
    {
        return $this;
    }

    /**
     * Get category.
     *
     * @return array|null
     * @since 100.1.0
     */
    public function getCategory()
    {
        return $this->_coreRegistry->registry('category');
    }

    /**
     * @inheritdoc
     */
    public function getGridPoolUrl()
    {
        return $this->getUrl('merchandiser/*/tilepool', ['_current' => true]);
    }

    /**
     * Retrieve additional product attributes.
     *
     * @return array
     * @since 100.1.0
     */
    protected function getUsableAttributes()
    {
        if ($this->usableAttributes == null) {
            $attributeCodes = (string) $this->scopeConfig->getValue(self::XML_PATH_ADDITIONAL_ATTRIBUTES);
            $attributeCodes = explode(',', $attributeCodes);
            $this->usableAttributes = array_map('trim', $attributeCodes);
        }
        return $this->usableAttributes;
    }

    /**
     * Retrieve product attributes to be displayed in tile view.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     * @since 100.1.0
     */
    public function getAttributesToDisplay($product)
    {
        $attributeCodes = $this->getUsableAttributes();
        $availableAttributes = $product->getTypeInstance()->getSetAttributes($product);
        $availableFields = array_keys($product->getData());
        $filteredAttributes = [];

        foreach ($attributeCodes as $code) {
            $renderer = $this->attributeFactory->create($code);

            if ($code == 'price') {
                $price = $this->priceCurrency->format($product->getPrice());
                if ($product->getTypeId() == 'configurable') {
                    $children = $product->getTypeInstance()->getUsedProducts($product);
                        if(!empty($children))
                            $price = $this->priceCurrency->format($children[0]->getData('price'));
                }
                $attributeObject = $availableAttributes[$code];
                $filteredAttributes[] = $renderer->addData([
                    'label' => $attributeObject->getFrontend()->getLabel(),
                    'value' => $price
                ]);
            } elseif ($code == 'stock') {
                $filteredAttributes[] = $renderer->addData([
                    'label' => ucwords($code),
                    'value' => $this->getStock($product)
                ]);
            } elseif (isset($availableAttributes[$code])) {
                $attributeObject = $availableAttributes[$code];
                $filteredAttributes[] = $renderer->addData([
                    'label' => $attributeObject->getFrontend()->getLabel(),
                    'value' => $product->getData($code)
                ]);
            } elseif (in_array($code, $availableFields)) {
                $filteredAttributes[] = $renderer->addData([
                    'label' => ucwords($code),
                    'value' => $product->getData($code)
                ]);
            }
        }

        return $filteredAttributes;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function isProductDisabled(Product $product)
    {
        return $product->getStatus() != Product\Attribute\Source\Status::STATUS_ENABLED;
    }

    /**
     * @param Product $product
     * @return int|mixed
     */
    protected function getStock(Product $product)
    {
        $stock = 0;
        if ($product->getTypeId() === Configurable::TYPE_CODE) {
            /** @var Configurable $productType */
            $productType = $product->getTypeInstance();
            $children = $productType->getUsedProducts($product);
            foreach ($children as $child) {
                $stockItem = $this->stockRegistry->getStockItem($child->getId());
                $stock += $stockItem->getQty();
            }
        } else {
            $stock = $product->getData('stock');
        }

        return $stock;
    }
}
