<?php

namespace Project\VisualMerchandiser\Observer;

use Magento\Catalog\Model\Category;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CategorySaveMerchandiserData
 * @package Project\VisualMerchandiser\Observer
 */
class CategorySaveMerchandiserData implements ObserverInterface
{
    /**
     * @var \Magento\VisualMerchandiser\Model\Position\Cache
     */
    protected $_cache;

    /**
     * @var \Magento\VisualMerchandiser\Model\Rules
     */
    protected $_rules;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var \Project\VisualMerchandiser\Model\Position\PoolCache
     */
    protected $_poolCache;

    /**
     * @param \Magento\VisualMerchandiser\Model\Position\Cache $cache
     * @param \Magento\VisualMerchandiser\Model\Rules $rules
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     */
    public function __construct(
        \Magento\VisualMerchandiser\Model\Position\Cache $cache,
        \Magento\VisualMerchandiser\Model\Rules $rules,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
    ) {
        $this->_cache = $cache;
        $this->_rules = $rules;
        $this->categoryRepository = $categoryRepository;
        $this->_poolCache = $poolCache;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Category $category */
        $category = $observer->getEvent()->getCategory();
        // Assign cached positions
        $cacheKey = $observer->getEvent()->getRequest()->getPostValue(
            \Magento\VisualMerchandiser\Model\Position\Cache::POSITION_CACHE_KEY
        );
        $positions = $this->_cache->getPositions($cacheKey);
        if (is_array($positions)) {
            $category->setPostedProducts(
                $positions
            );
        }

        // Assign cached pool products
        $poolCacheKey = $observer->getEvent()->getRequest()->getPostValue(
            \Project\VisualMerchandiser\Model\Position\PoolCache::POSITION_POOL_CACHE_KEY
        );
        $poolPositions = $this->_poolCache->getPositions($poolCacheKey);
        if (is_array($poolPositions)) {
            $category->setPostedProductsPool(
                $poolPositions
            );
        }

        // Can't save smart rules if it's a category without an ID
        if ($category->isObjectNew() || !$category->getId() || empty($category->getOrigData())) {
            return;
        }

        $postData = $observer->getEvent()->getRequest()->getPostValue();

        // Save 'is smart category' state (or clear it)
        if (!isset($postData['is_smart_category'])) {
            return;
        }

        // Save smart category rules (or clear it)
        $rule = $this->_rules->loadByCategory($category);
        $rule->setData([
            'rule_id' => $rule->getId(),
            'category_id' => $category->getId(),
            'is_active' => $postData['is_smart_category'] == 1 ? '1' : '0',
            'conditions_serialized' => isset($postData['smart_category_rules']) ? $postData['smart_category_rules'] : ''
        ]);
        $rule->save();
    }
}
