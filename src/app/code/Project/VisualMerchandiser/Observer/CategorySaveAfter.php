<?php

namespace Project\VisualMerchandiser\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CategorySaveAfter
 * @package Project\VisualMerchandiser\Observer
 */
class CategorySaveAfter implements ObserverInterface
{
    /**
     * @var \Project\VisualMerchandiser\Model\MerchManager
     */
    protected $merchManager;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * CategorySaveAfter constructor.
     * @param \Project\VisualMerchandiser\Model\MerchManager $merchManager
     * @param \Magento\Framework\Registry $registry
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Project\VisualMerchandiser\Model\MerchManager $merchManager,
        \Magento\Framework\Registry $registry,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->merchManager = $merchManager;
        $this->registry = $registry;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        try {
            $category = $observer->getEvent()->getData('category');
           // $this->merchManager->unflagCategoryTree($category);
        } catch (\Exception $e) {
            $this->logger->error('Error - categorySaveAfter - merchandiser : '.$e->getMessage());
        }
    }
}
