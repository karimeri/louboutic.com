<?php

namespace Project\VisualMerchandiser\Observer;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AddToMerchFilterToProductCollection
 * @package Project\VisualMerchandiser\Observer
 */
class AddToMerchFilterToProductCollection implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var ProductCollection $productCollection */
        $productCollection = $observer->getEvent()->getCollection();

        $productCollection->getSelect()
            ->joinLeft(
                ['ccp' => 'catalog_category_product'],
                'ccp.product_id = cat_index.product_id AND ccp.category_id = cat_index.category_id',
                'to_merch'
            )->where(
                'ccp.to_merch = ?',
                0
            );
        return $this;
    }
}
