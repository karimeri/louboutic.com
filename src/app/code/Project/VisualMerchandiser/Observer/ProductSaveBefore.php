<?php

namespace Project\VisualMerchandiser\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ProductSaveBefore
 * @package Project\VisualMerchandiser\Observer
 */
class ProductSaveBefore implements ObserverInterface
{
    /**
     * @var \Project\VisualMerchandiser\Model\MerchManager
     */
    protected $merchManager;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * ProductSaveBefore constructor.
     * @param \Project\VisualMerchandiser\Model\MerchManager $merchManager
     * @param \Magento\Framework\Registry $registry
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Project\VisualMerchandiser\Model\MerchManager $merchManager,
        \Magento\Framework\Registry $registry,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->merchManager = $merchManager;
        $this->registry = $registry;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var Product $product */
            $product = $observer->getEvent()->getProduct();

            $categoryIds = $product->getOrigData('category_ids');
            $categoryIdsBeforeSave = $product->getCategoryIds();

            if ($product->isObjectNew()) {
                $addedCategoryIds = $categoryIdsBeforeSave;
                $deletedCategoryIds = [];
            } else {
                $addedCategoryIds = array_diff($categoryIdsBeforeSave, $categoryIds);
                $deletedCategoryIds = array_diff($categoryIds, $categoryIdsBeforeSave);
            }

            if ($deletedCategoryIds) {
                $this->registry->register('deleted_cat_ids', $deletedCategoryIds);
            }
            if ($addedCategoryIds) {
                $this->registry->register('added_cat_ids', $addedCategoryIds);
            }
        } catch (\Exception $e) {
            $this->logger->error('Error - productSaveBefore - merchandiser : '.$e->getMessage());
        }
    }
}
