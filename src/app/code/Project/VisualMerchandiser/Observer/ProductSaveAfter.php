<?php

namespace Project\VisualMerchandiser\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ProductSaveAfter
 * @package Project\VisualMerchandiser\Observer
 */
class ProductSaveAfter implements ObserverInterface
{
    /**
     * @var \Project\VisualMerchandiser\Model\MerchManager
     */
    protected $merchManager;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * ProductSaveAfter constructor.
     * @param \Project\VisualMerchandiser\Model\MerchManager $merchManager
     * @param \Magento\Framework\Registry $registry
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Project\VisualMerchandiser\Model\MerchManager $merchManager,
        \Magento\Framework\Registry $registry,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->merchManager = $merchManager;
        $this->registry = $registry;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var Product $product */
            $product = $observer->getEvent()->getProduct();
            $addedCategoryIds = $this->registry->registry('added_cat_ids');
            if ($addedCategoryIds) {
                foreach ($addedCategoryIds as $addedCategoryId) {
                    $this->merchManager->flagCategoryProduct($addedCategoryId, $product->getId());
                }
                $this->merchManager->flagCategoriesTrees($addedCategoryIds);
            }
            $deletedCategoryIds = $this->registry->registry('deleted_cat_ids');
            if ($deletedCategoryIds) {
                foreach ($deletedCategoryIds as $deletedCategoryId) {
                    $this->merchManager->unflagCategoryProduct($deletedCategoryId, $product->getId());
                }
                $this->merchManager->unflagCategoriesTrees($deletedCategoryIds);
            }
        } catch (\Exception $e) {
            $this->logger->info('Error - productSaveAfter - merchandiser : '.$e->getMessage());
        }
    }
}
