<?php

namespace Project\VisualMerchandiser\Observer;

use Magento\Catalog\Model\Category;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class AssignChildrenProducts
 * @package Project\VisualMerchandiser\Observer
 */
class AssignChildrenProducts implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\ConfigurableProduct\Api\LinkManagementInterface
     */
    protected $linkManagement;

    /**
     * AssignChildrenProducts constructor.
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\ConfigurableProduct\Api\LinkManagementInterface $linkManagement
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\ConfigurableProduct\Api\LinkManagementInterface $linkManagement
    ) {
        $this->productRepository    = $productRepository;
        $this->linkManagement       = $linkManagement;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        return true;
        /** @var Category $category */
        $category = $observer->getEvent()->getCategory();

        $products = $category->getPostedProducts();
        if ($products) {
            $productIds = array_keys($products);
            foreach ($productIds as $productId) {
                $product = $this->productRepository->getById($productId);
                if ($product->getTypeId() === Configurable::TYPE_CODE) {
                    $children = $this->linkManagement->getChildren($product->getSku());
                    foreach ($children as $child) {
                        // Assign configurable product children to category with default position
                        $products[$child->getId()] = 0;
                    }
                }
            }
            $category->setPostedProducts($products);
        }

        $productsPool = $category->getPostedProductsPool();
        if ($productsPool) {
            $productIds = array_keys($productsPool);
            foreach ($productIds as $productId) {
                $product = $this->productRepository->getById($productId);
                if ($product->getTypeId() === Configurable::TYPE_CODE) {
                    $children = $this->linkManagement->getChildren($product->getSku());
                    foreach ($children as $child) {
                        // Assign configurable product children to category with default position
                        $productsPool[$child->getId()] = 0;
                    }
                }
            }
            $category->setPostedProductsPool($productsPool);
        }
    }
}
