<?php

namespace Project\VisualMerchandiser\Model;

use Magento\Store\Model\Store;
use Project\Catalog\Model\CategoryProduct;
use Project\Catalog\Model\Magento\Catalog\Category;

/**
 * Class MerchManager
 * @package Project\VisualMerchandiser\Model
 */
class MerchManager
{
    // category attribute
    CONST ATTRIBUTE_TO_MERCH = 'to_merch';

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Project\Catalog\Model\CategoryProductFactory
     */
    protected $categoryProductFactory;

    /**
     * @var \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory
     */
    protected $categoryProductCollectionFactory;

    /**
     * MerchManager constructor.
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Project\Catalog\Model\CategoryProductFactory $categoryProductFactory
     * @param \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory
     */
    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Project\Catalog\Model\CategoryProductFactory $categoryProductFactory,
        \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory
    ) {
        $this->categoryFactory                  = $categoryFactory;
        $this->categoryCollectionFactory        = $categoryCollectionFactory;
        $this->categoryProductFactory           = $categoryProductFactory;
        $this->categoryProductCollectionFactory = $categoryProductCollectionFactory;
    }

    /**
     * @param int $categoryId
     * @param int $productId
     * @throws \Exception
     */
    public function flagCategoryProduct($categoryId, $productId)
    {
        /** @var CategoryProduct $categoryProduct */
        $categoryProduct = $this->categoryProductFactory->create();
        $categoryProduct = $categoryProduct->loadByCategoryIdAndProductId($categoryId, $productId);
        $categoryProduct->setData(self::ATTRIBUTE_TO_MERCH, 1);
        $categoryProduct->save();
    }

    /**
     * @param int $categoryId
     * @param int $productId
     * @throws \Exception
     */
    public function unflagCategoryProduct($categoryId, $productId)
    {
        /** @var CategoryProduct $categoryProduct */
        $categoryProduct = $this->categoryProductFactory->create();
        $categoryProduct = $categoryProduct->loadByCategoryIdAndProductId($categoryId, $productId);
        $categoryProduct->setData(self::ATTRIBUTE_TO_MERCH, 0);
        $categoryProduct->save();
    }

    /**
     * @param Category|int $category
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function flagCategoryTree($category)
    {
        if (is_numeric($category)) {
            $category = $this->categoryFactory->create()->load($category);
        }
        $categories = $this->getCategoriesToFlag($category);
        foreach ($categories as $category) {
            /** @var Category $category */
            $category->addAttributeUpdate(self::ATTRIBUTE_TO_MERCH, 1, Store::DEFAULT_STORE_ID);
        }
    }

    /**
     * @param Category|int $category
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function unflagCategoryTree($category)
    {
        if (is_numeric($category)) {
            $category = $this->categoryFactory->create()->load($category);
        }
        $categories = $this->getCategoriesToFlag($category);
        /** @var Category $category */
        foreach ($categories as $category) {
            /** @var \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection $categoryProductCollection */
            $categoryProductCollection = $this->categoryProductCollectionFactory->create();
            $count = $categoryProductCollection
                ->addFieldToFilter('category_id', $category->getId())
                ->addFieldToFilter('to_merch', 1)
                ->count();
            if ($count === 0 && !$category->hasChildren()) {
                $category->addAttributeUpdate(self::ATTRIBUTE_TO_MERCH, 0, Store::DEFAULT_STORE_ID);
            }elseif ($count === 0 && $category->hasChildren()) {
                /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categories */
                $categories = $this->categoryCollectionFactory->create();
                $categoryChildren = $category->getChildren();
                $categories->setStore(
                    Store::DEFAULT_STORE_ID
                )->addAttributeToFilter('entity_id',
                    ['in' => explode(',', $categoryChildren)]
                )->addAttributeToSelect(
                    ['name', 'to_merch']
                )->addFieldToFilter('to_merch',
                    1
                );

                if ($categories->getSize() === 0 ) {
                    $category->addAttributeUpdate(self::ATTRIBUTE_TO_MERCH, 0, Store::DEFAULT_STORE_ID);
                }
            }
        }
    }

    /**
     * @param array $categories
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function flagCategoriesTrees(array $categories)
    {
        foreach ($categories as $category) {
            $this->flagCategoryTree($category);
        }
    }

    /**
     * @param array $categories
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function unflagCategoriesTrees(array $categories)
    {
        foreach ($categories as $category) {
            $this->unflagCategoryTree($category);
        }
    }

    /**
     * @param Category $category
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getCategoriesToFlag($category)
    {
        $pathIds = $category->getPathIds();
        /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categories */
        $categories = $this->categoryCollectionFactory->create();
        return $categories->setStore(
            Store::DEFAULT_STORE_ID
        )->addAttributeToSelect(
            'name'
        )->addFieldToFilter(
            'entity_id',
            ['in' => $pathIds]
        )->addFieldToFilter(
            'path',
            ['neq' => '1']
        )->load()->getItems();
    }
}
