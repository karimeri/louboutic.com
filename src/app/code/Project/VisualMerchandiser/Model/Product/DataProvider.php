<?php

namespace Project\VisualMerchandiser\Model\Product;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class DataProvider
 * @package Project\VisualMerchandiser\Model\Product
 */
class DataProvider extends \Magento\VisualMerchandiser\Model\Product\DataProvider
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Project\VisualMerchandiser\Model\Position\PoolCache
     */
    protected $poolCache;

    /**
     * @var string
     */
    protected $poolPositionCacheKey;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\VisualMerchandiser\Model\Position\Cache $cache
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\VisualMerchandiser\Model\Position\Cache $cache,
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $collectionFactory, $request, $cache, $meta, $data);
        $this->request = $request;
        $this->poolCache = $poolCache;

        $this->collection->getSelect()->joinLeft(
            ['super_link' => 'catalog_product_super_link'],
            'super_link.product_id = e.entity_id',
            []
        )->where('super_link.product_id IS NULL ');
    }

    /**
     * Prepares update url
     *
     * @return void
     */
    protected function prepareUpdateUrl()
    {
        if (!isset($this->data['config']['filter_url_params'])) {
            return;
        }
        foreach ($this->data['config']['filter_url_params'] as $paramName => $paramValue) {
            if ('*' == $paramValue) {
                $paramValue = $this->request->getParam($paramName);
                $params = explode(':', $paramValue);
                $this->positionCacheKey = $params[0];
                $this->poolPositionCacheKey = $params[1];
            }

            if ($paramValue) {
                $this->data['config']['update_url'] = sprintf(
                    '%s%s/%s',
                    $this->data['config']['update_url'],
                    $paramName,
                    $paramValue
                );
            }
        }
    }

    /**
     * Sets the position values
     *
     * @return void
     */
    public function addPositionData()
    {
        $positions = $this->poolCache->getPositions($this->poolPositionCacheKey);

        if ($positions === false) {
            return;
        }

        foreach ($this->collection as $item) {
            if (array_key_exists($item->getEntityId(), $positions)) {
                $item->setPosition(
                    $positions[$item->getEntityId()]
                );
                $item->setIds(
                    $item->getEntityId()
                );
            } else {
                $item->setIds(null);
                $item->setPosition(null);
            }
        }
    }

    /**
     * Get data
     *
     * @return array
     * @throws \Zend_Json_Exception
     */
    public function getData()
    {
        $this->addPositionData();
        $arrItems = [];
        $arrItems['totalRecords'] = $this->collection->getSize();
        $arrItems['items'] = [];
        $positions = $this->cache->getPositions($this->positionCacheKey);
        $poolPositions = $this->poolCache->getPositions($this->poolPositionCacheKey);
        if (is_array($positions) && is_array($poolPositions)) {
            $positions = $positions + $poolPositions;
        }
        $arrItems['selectedData'] = $positions;
        $arrItems['allIds'] = $this->collection->getAllIds();

        foreach ($this->collection->getItems() as $item) {
            $arrItems['items'][] =  $item->toArray();
        }

        return $arrItems;
    }
}
