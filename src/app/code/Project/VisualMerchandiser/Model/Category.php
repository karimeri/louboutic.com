<?php

namespace Project\VisualMerchandiser\Model;

/**
 * Class Category
 * @package Project\VisualMerchandiser\Model
 */
class Category extends \Project\Catalog\Model\Magento\Catalog\Category
{
    /**
     * Retrieve array of product id's for category
     *
     * The array returned has the following format:
     * array($productId => $position)
     *
     * @return array
     */
    public function getProductsPoolPosition()
    {
        if (!$this->getId()) {
            return [];
        }

        $array = $this->getData('products_pool_position');
        if ($array === null) {
            $array = $this->getResource()->getProductsPoolPosition($this);
            $this->setData('products_pool_position', $array);
        }
        return $array;
    }
}
