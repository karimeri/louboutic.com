<?php

namespace Project\VisualMerchandiser\Model\ResourceModel;

/**
 * Class Category
 * @package Project\VisualMerchandiser\Model\ResourceModel
 */
class Category extends \Magento\Catalog\Model\ResourceModel\Category
{
    /**
     * @var \Project\VisualMerchandiser\Model\MerchManager
     */
    protected $merchManager;

    /**
     * Category constructor.
     * @param \Magento\Eav\Model\Entity\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Factory $modelFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Catalog\Model\ResourceModel\Category\TreeFactory $categoryTreeFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Magento\Catalog\Model\Indexer\Category\Product\Processor $indexerProcessor
     * @param \Project\VisualMerchandiser\Model\MerchManager $merchManager
     * @param array $data
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     */
    public function __construct(
        \Magento\Eav\Model\Entity\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Factory $modelFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Catalog\Model\ResourceModel\Category\TreeFactory $categoryTreeFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\Indexer\Category\Product\Processor $indexerProcessor,
        \Project\VisualMerchandiser\Model\MerchManager $merchManager,
        array $data = [],
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    ) {
        parent::__construct(
            $context,
            $storeManager,
            $modelFactory,
            $eventManager,
            $categoryTreeFactory,
            $categoryCollectionFactory,
            $indexerProcessor,
            $data,
            $serializer
        );
        $this->merchManager = $merchManager;
    }

    /**
     * @param \Project\VisualMerchandiser\Model\Category $category
     * @return $this|\Magento\Catalog\Model\ResourceModel\Category
     */
    protected function _saveCategoryProducts($category)
    {
        $category->setIsChangedProductList(false);
        $id = $category->getId();
        /**
         * new category-product relationships
         */
        $products = $category->getPostedProducts();

        /**
         * new category-products pool relationships
         */
        $productsPool = $category->getPostedProductsPool();

        /**
         * Example re-save category
         */
        if ($products === null) {
            return $this;
        }

        /**
         * old category-product relationships
         */
        $oldProducts = $category->getProductsPosition();

        /**
         * old category-product pool relationships
         */
        $oldProductsPool = $category->getProductsPoolPosition();

        $insert = array_diff_key($products, $oldProducts);
        $delete = array_diff_key($oldProducts, $products);

        if (is_array($productsPool) && is_array($oldProductsPool)) {
            $insertPool = array_diff_key($productsPool, $oldProductsPool);
            $deletePool = array_diff_key($oldProductsPool, $productsPool);
        }

        /**
         * Find product ids which are presented in both arrays
         * and saved before (check $oldProducts array)
         */
        $update = array_intersect_key($products, $oldProducts);
        $update = array_diff_assoc($update, $oldProducts);

        if (is_array($productsPool) && is_array($oldProductsPool)) {
            $updatePool = array_intersect_key($productsPool, $oldProductsPool);
            $updatePool = array_diff_assoc($updatePool, $oldProductsPool);
        }

        $connection = $this->getConnection();

        /**
         * Delete products from category
         */
        if (!empty($delete)) {
            $cond = ['product_id IN(?)' => array_keys($delete), 'category_id=?' => $id];
            $connection->delete($this->getCategoryProductTable(), $cond);
        }

        /**
         * Delete products from category pool
         */
        if (!empty($deletePool)) {
            $cond = ['product_id IN(?)' => array_keys($deletePool), 'category_id=?' => $id];
            $connection->delete($this->getCategoryProductTable(), $cond);
            // Unflag category tree
            $this->merchManager->unflagCategoryTree($id);
        }

        /**
         * Add products to category
         */
        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $productId => $position) {
                $data[] = [
                    'category_id' => (int)$id,
                    'product_id' => (int)$productId,
                    'position' => (int)$position,
                    'to_merch' => 0
                ];
            }
            $connection->insertMultiple($this->getCategoryProductTable(), $data);
        }

        /**
         * Add products to category pool
         */
        if (!empty($insertPool)) {
            $data = [];
            foreach ($insertPool as $productId => $position) {
                $data[] = [
                    'category_id' => (int)$id,
                    'product_id' => (int)$productId,
                    'position' => (int)$position,
                    'to_merch' => 1
                ];
            }
            $connection->insertMultiple($this->getCategoryProductTable(), $data);
            // Flag category tree
            $this->merchManager->flagCategoryTree($id);
        }

        /**
         * Update product positions in category
         */
        if (!empty($update)) {
            $newPositions = [];
            foreach ($update as $productId => $position) {
                $delta = $position - $oldProducts[$productId];
                if (!isset($newPositions[$delta])) {
                    $newPositions[$delta] = [];
                }
                $newPositions[$delta][] = $productId;
            }

            foreach ($newPositions as $delta => $productIds) {
                $bind = [
                    'position' => new \Zend_Db_Expr("position + ({$delta})"),
                    'to_merch' => new \Zend_Db_Expr('0')
                ];
                $where = ['category_id = ?' => (int)$id, 'product_id IN (?)' => $productIds];
                $connection->update($this->getCategoryProductTable(), $bind, $where);
            }
        }

        /**
         * Update product positions in category pool
         */
        if (!empty($updatePool)) {
            $newPositions = [];
            foreach ($updatePool as $productId => $position) {
                $delta = $position - $oldProductsPool[$productId];
                if (!isset($newPositions[$delta])) {
                    $newPositions[$delta] = [];
                }
                $newPositions[$delta][] = $productId;
            }

            foreach ($newPositions as $delta => $productIds) {
                $bind = [
                    'position' => new \Zend_Db_Expr("position + ({$delta})"),
                    'to_merch' => new \Zend_Db_Expr('1')
                ];
                $where = ['category_id = ?' => (int)$id, 'product_id IN (?)' => $productIds];
                $connection->update($this->getCategoryProductTable(), $bind, $where);
            }
        }

        $productIds = [];
        if (!empty($insert) || !empty($delete)) {
            $productIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));
           /* $this->_eventManager->dispatch(
                'catalog_category_change_products',
                ['category' => $category, 'product_ids' => $productIds]
            );*/
        }

        if (isset($insertPool) || isset($deletePool)) {
            $poolProductIds = array_unique(array_merge(array_keys($insertPool), array_keys($deletePool)));
            $productIds = $productIds + $poolProductIds;
           /* $this->_eventManager->dispatch(
                'catalog_category_change_products_pool',
                ['category' => $category, 'product_ids' => $poolProductIds]
            );*/
        }
        if ($productIds && is_array($productIds)) {
            $category->setChangedProductIds($productIds);
        }

        if (!empty($insert) || !empty($update) || !empty($delete) || !empty($insertPool) || !empty($updatePool) || !empty($deletePool)) {
            $category->setIsChangedProductList(true);

            /**
             * Setting affected products to category for third party engine index refresh
             */
            $productIds = array_keys($insert + $delete + $update);
            if (isset($insertPool) || isset($deletePool) || isset($updatePool)) {
                $poolProductIds = array_keys($insertPool + $deletePool + $updatePool);
                $productIds = $productIds + $poolProductIds;
            }
            $category->setAffectedProductIds($productIds);
        }
        return $this;
    }

    /**
     * Get positions of associated to category products
     *
     * @param \Magento\Catalog\Model\Category $category
     * @return array
     */
    public function getProductsPosition($category)
    {
        $select = $this->getConnection()->select()->from(
            $this->getCategoryProductTable(),
            ['product_id', 'position']
        )->where(
            "{$this->getTable('catalog_category_product')}.category_id = ?",
            $category->getId()
        )->where(
            "{$this->getTable('catalog_category_product')}.to_merch = ?",
            0
        );
        $websiteId = $category->getStore()->getWebsiteId();
        if ($websiteId) {
            $select->join(
                ['product_website' => $this->getTable('catalog_product_website')],
                "product_website.product_id = {$this->getTable('catalog_category_product')}.product_id",
                []
            )->where(
                'product_website.website_id = ?',
                $websiteId
            );
        }
        $bind = ['category_id' => (int)$category->getId()];

        return $this->getConnection()->fetchPairs($select, $bind);
    }

    /**
     * Get positions of associated to category products pool
     *
     * @param \Project\VisualMerchandiser\Model\Category $category
     * @return array
     */
    public function getProductsPoolPosition($category)
    {
        $select = $this->getConnection()->select()->from(
            $this->getCategoryProductTable(),
            ['product_id', 'position']
        )->where(
            "{$this->getTable('catalog_category_product')}.category_id = ?",
            $category->getId()
        )->where(
            "{$this->getTable('catalog_category_product')}.to_merch = ?",
            1
        );
        $websiteId = $category->getStore()->getWebsiteId();
        if ($websiteId) {
            $select->join(
                ['product_website' => $this->getTable('catalog_product_website')],
                "product_website.product_id = {$this->getTable('catalog_category_product')}.product_id",
                []
            )->where(
                'product_website.website_id = ?',
                $websiteId
            );
        }
        $bind = ['category_id' => (int)$category->getId()];

        return $this->getConnection()->fetchPairs($select, $bind);
    }
}
