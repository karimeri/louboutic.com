<?php

namespace Project\VisualMerchandiser\Model\Pimgento\Product\Factory;

use Louboutin\Varnish\Model\PurgeCache;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface as scopeConfig;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Module\Manager as moduleManager;
use Magento\Framework\Notification\NotifierInterface;
use Magento\Rma\Helper\Data as RmaHelper;
use Magento\Store\Model\ScopeInterface;
use Pimgento\Entities\Model\Entities;
use Pimgento\Import\Helper\Config as helperConfig;
use Pimgento\Import\Helper\Serializer as Json;
use Pimgento\Import\Helper\UrlRewrite as urlRewriteHelper;
use Pimgento\Log\Model\Log as LogModel;
use Pimgento\Product\Helper\Config as productHelper;
use Pimgento\Product\Helper\Media as mediaHelper;
use Pimgento\Product\Model\Factory\Import\Media;
use Pimgento\Product\Model\Factory\Import\Related;
use Project\Catalog\Model\CategoryProduct;
use Project\Catalog\Model\CategoryProductFactory;
use Project\Core\Manager\EnvironmentManager;
use Project\Pimgento\Helper\Config as PimgentoHelper;
use Project\Pimgento\Helper\Reporting as ReportingHelper;
use Project\Pimgento\Model\PimgentoRepository;
use Project\Pimgento\Model\Product\Factory\Import\Media as PimgentoMedia;
use Project\Pimgento\Model\QueryRegistry;
use Project\Pimgento\Model\Store\WebsiteRepository;
use Project\VisualMerchandiser\Model\MerchManager;
use \Zend_Db_Expr as Expr;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductFactory;
use MageWorx\SeoXTemplates\Model\Template\ProductFactory as TemplateProductFactory;
use MageWorx\SeoXTemplates\Model\DbWriterProductFactory;

/**
 * Class Import
 * @package Project\VisualMerchandiser\Model\Pimgento\Product\Factory
 */
class Import extends \Project\Pimgento\Model\Product\Factory\Import
{

    const XML_PATH_MAGEWORX_TEMPLATE_ID = 'pimgento/product/product_url_template';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;


    /**
     * @var CategoryProductFactory
     */
    protected $categoryProductFactory;

    /**
     * @var MerchManager
     */
    protected $merchManager;

    /**
     * @var PurgeCache
     */
    private $purgeCache;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * Template product factory
     *
     * @var templateProductFactory
     */
    protected $templateProductFactory;

    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;


    /**
     * Import constructor.
     * @param helperConfig $helperConfig
     * @param ManagerInterface $eventManager
     * @param moduleManager $moduleManager
     * @param scopeConfig $scopeConfig
     * @param Entities $entities
     * @param TypeListInterface $cacheTypeList
     * @param SetFactory $attributeSetFactory
     * @param productHelper $productHelper
     * @param mediaHelper $mediaHelper
     * @param urlRewriteHelper $urlRewriteHelper
     * @param Related $related
     * @param Media $media
     * @param Product $product
     * @param Json $serializer
     * @param PimgentoRepository $pimgentoRepository
     * @param PimgentoHelper $pimgentoHelper
     * @param EnvironmentManager $environmentManager
     * @param WebsiteRepository $websiteRepository
     * @param PimgentoMedia $pimgentoMedia
     * @param RmaHelper $rmaHelper
     * @param QueryRegistry $queryRegistry
     * @param CategoryFactory $categoryFactory
     * @param CategoryProductFactory $categoryProductFactory
     * @param ReportingHelper $reportingHelper
     * @param NotifierInterface $notifier
     * @param LogModel $log
     * @param MerchManager $merchManager
     * @param PurgeCache $purgeCache
     * @param CollectionFactory $categoryCollectionFactory
     * @param DbWriterProductFactory $dbWriterProductFactory
     * @param ProductFactory $productCollectionFactory
     * @param TemplateProductFactory $templateProductFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param array $data
     */
    public function __construct(
        helperConfig $helperConfig,
        ManagerInterface $eventManager,
        moduleManager $moduleManager,
        scopeConfig $scopeConfig,
        Entities $entities,
        TypeListInterface $cacheTypeList,
        SetFactory $attributeSetFactory,
        productHelper $productHelper,
        mediaHelper $mediaHelper,
        urlRewriteHelper $urlRewriteHelper,
        Related $related,
        Media $media,
        Product $product,
        Json $serializer,
        PimgentoRepository $pimgentoRepository,
        PimgentoHelper $pimgentoHelper,
        EnvironmentManager $environmentManager,
        WebsiteRepository $websiteRepository,
        PimgentoMedia $pimgentoMedia,
        RmaHelper $rmaHelper,
        QueryRegistry $queryRegistry,
        CategoryFactory $categoryFactory,
        CategoryProductFactory $categoryProductFactory,
        ReportingHelper $reportingHelper,
        NotifierInterface $notifier,
        LogModel $log,
        MerchManager $merchManager,
        PurgeCache $purgeCache,
        CollectionFactory $categoryCollectionFactory,
        DbWriterProductFactory $dbWriterProductFactory,
        ProductFactory $productCollectionFactory,
        templateProductFactory $templateProductFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        array $data = []
    ) {
        parent::__construct(
            $helperConfig,
            $eventManager,
            $moduleManager,
            $scopeConfig,
            $entities,
            $cacheTypeList,
            $attributeSetFactory,
            $productHelper,
            $mediaHelper,
            $urlRewriteHelper,
            $related,
            $media,
            $product,
            $serializer,
            $pimgentoRepository,
            $pimgentoHelper,
            $environmentManager,
            $websiteRepository,
            $pimgentoMedia,
            $rmaHelper,
            $queryRegistry,
            $categoryFactory,
            $reportingHelper,
            $notifier,
            $log,
            $data
        );
        $this->categoryProductFactory = $categoryProductFactory;
        $this->merchManager = $merchManager;
        $this->purgeCache = $purgeCache;
        $this->categoryCollectionFactory = $categoryCollectionFactory;

        $this->dbWriterProductFactory = $dbWriterProductFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->templateProductFactory = $templateProductFactory;
        $this->date = $date;
        $this->_scopeConfig = $scopeConfig;

    }

    /**
     * Set categories
     */
    public function setCategories()
    {
        $resource = $this->_entities->getResource();
        $connection = $resource->getConnection();
        $tmpTable = $this->_entities->getTableName($this->getCode());

        if (!$connection->tableColumnExists($tmpTable, 'categories')) {
            $this->setStatus(false);
            $this->setMessage(
                __('Column categories not found')
            );
        } else {
            if ($connection->tableColumnExists($tmpTable, 'parent')) {
                $array = [
                    'category_id' => 'c.entity_id',
                    'product_id' => 'p._entity_id',
                    'reactivated' => 'p.reactivated',
                    'parent' => 'p.parent',
                    'sku' => 'p.sku'
                ];
            } else {
                $array = [
                    'category_id' => 'c.entity_id',
                    'product_id' => 'p._entity_id',
                    'reactivated' => 'p.reactivated'
                ];
            }

            $select = $connection->select()
                ->from(
                    array(
                        'c' => $resource->getTable('pimgento_entities')
                    ),
                    array(
                        'position' => new Expr('184'),
                    )
                )
                ->joinInner(
                    array('p' => $tmpTable),
                    'FIND_IN_SET(`c`.`code`, `p`.`categories`) AND `c`.`import` = "category"',
                    $array
                )
                ->joinInner(
                    array('e' => $resource->getTable('catalog_category_entity')),
                    'c.entity_id = e.entity_id',
                    array()
                );

            //get all import products with assigned categories
            $allProducts = $connection->fetchAll($select);

            //configurable products type
            $configurableProducts = [];
            if ($connection->tableColumnExists($tmpTable, 'parent')) {
                $select->where('p.parent is NULL');
                $configurableProducts = $connection->fetchAll($select);
            }

            // simple products type
            if ($connection->tableColumnExists($tmpTable, 'parent')) {
                $select->reset(\Magento\Framework\DB\Select::WHERE)
                    ->where('p.reactivated IS NOT NULL')
                    ->where('p.parent is NOT NULL')
                    ->where('p.parent = ""');
            } else {
                $select->reset(\Magento\Framework\DB\Select::WHERE)
                    ->where('p.reactivated IS NOT NULL');
            }

            $simpleProducts = $connection->fetchAll($select);

            $configurableProductsUpdated = [];

            if (!empty($configurableProducts)) {
                foreach ($configurableProducts as $configurableProduct) {
                    foreach ($allProducts as $product) {
                        if (array_key_exists('sku', $configurableProduct)) {
                            if ( $configurableProduct['sku'] == $product['parent'] &&
                                ($product['reactivated'] == '1' )) {
                                $configurableProduct['reactivated'] = $product['reactivated'];
                                break;
                            }
                        }
                    }
                    $configurableProductsUpdated [] = $configurableProduct;
                }
            }

            $productsToInsert = array_merge($simpleProducts, $configurableProductsUpdated);

            $categoryIds = array_unique(array_column($allProducts, 'category_id'));
            $rootCategoryId = $categoryIds[0];
            $productsToInsertWithPositions = [];

            $productsToInsertTmp = $productsToInsert;
            foreach ($productsToInsert as $product) {
                $assignedProductCategories = $this->getProductAssignedCategories($product['product_id']);
                if ($assignedProductCategories) {
                    unset($assignedProductCategories[array_search($rootCategoryId, $assignedProductCategories)]);
                    foreach ($assignedProductCategories as $assignedProductCategory) {
                        $product['category_id'] = $assignedProductCategory;
                        $productsToInsertTmp [] = $product;
                        $categoryIds [] = $assignedProductCategory;
                    }
                }
            }
            $productsToInsert = $productsToInsertTmp;

            foreach ($categoryIds as $categoryId) {
                $categoryProductCount = $this->categoryFactory->create()->load($categoryId)->getProductCount();

                $productsToInsertByCategory = array_filter($productsToInsert,
                    function ($v) use ($categoryId) {
                        return $v['category_id'] == $categoryId;
                    });
                foreach ($productsToInsertByCategory as $key => $product) {
                    $productsToInsertByCategory[$key]['position'] = $categoryProductCount;
                    $assignedCategories = $this->getProductAssignedCategories($product['product_id']);
                    if (!in_array($categoryId, $assignedCategories)) {
                        // flag to_merch and set position to 0 for new category <> product association
                        $productsToInsertByCategory[$key]['to_merch'] = 1;
                        $productsToInsertByCategory[$key]['position'] = 0;
                        // flag category tree OBSOLETE PIMGENTO NOT SEND CATEGORIES AFECTATIONS
                        // $this->merchManager->flagCategoryTree($categoryId);
                    }  /* elseif ($productsToInsertByCategory[$key]['reactivated'] == 1) {
                        // flag to_merch and set position to 0 for reactivated product
                        $productsToInsertByCategory[$key]['to_merch'] = 1;
                        $productsToInsertByCategory[$key]['position'] = 0;
                        // flag category tree OBSOLETE PIMGENTO NONT SEND CATEGORIES AFFECTATIONS
                        //$this->merchManager->flagCategoryTree($categoryId);
                    } elseif ($productsToInsertByCategory[$key]['reactivated'] == 2) {
                        // Set position to 9999 for deactivated product
                        $categoryProduct = $this->loadByCategoryIdAndProductId($categoryId, $product['product_id']);
                        $productsToInsertByCategory[$key]['to_merch'] = $categoryProduct->getData('to_merch');
                        $productsToInsertByCategory[$key]['position'] = 9999;
                    } */ else {
                        // retrieve existing to_merch value for already existing category <> product association
                        $categoryProduct = $this->loadByCategoryIdAndProductId($categoryId, $product['product_id']);
                        $productsToInsertByCategory[$key]['to_merch'] = $categoryProduct->getData('to_merch');
                        $productsToInsertByCategory[$key]['position'] = $categoryProduct->getData('position');
                    }
                    $categoryProductCount++;
                    // Remove temporary column
                    unset($productsToInsertByCategory[$key]['reactivated']);
                    unset($productsToInsertByCategory[$key]['searchable_sku']);
                    unset($productsToInsertByCategory[$key]['sku']);
                    unset($productsToInsertByCategory[$key]['parent']);
                }

                $productsToInsertWithPositions = array_merge($productsToInsertWithPositions,
                    $productsToInsertByCategory);
            }
            $connection->insertOnDuplicate(
                $resource->getTable('catalog_category_product'),
                $productsToInsertWithPositions
            );

            // Louboutin custom : does not remove existing product <> category assignation
        }
    }

    /**
     * @param int $productId
     * @return array
     */
    private function getProductAssignedCategories($productId)
    {
        /** @var CategoryProduct $categoryProduct */
        $categoryProduct = $this->categoryProductFactory->create();
        return $categoryProduct->getCategoriesByProductId($productId);
    }

    /**
     * @param int $categoryId
     * @param int $productId
     * @return \Magento\Framework\DataObject
     */
    private function loadByCategoryIdAndProductId($categoryId, $productId)
    {
        /** @var CategoryProduct $categoryProduct */
        $categoryProduct = $this->categoryProductFactory->create();
        return $categoryProduct->loadByCategoryIdAndProductId($categoryId, $productId);
    }

    /**
     * Clean cache
     */
    public function cleanCache()
    {
        // if  not exist long_product_name-fr_FR-ecommerce => quick
        $resource = $this->_entities->getResource();
        $connection = $resource->getConnection();
        $tmpTable = $this->_entities->getTableName($this->getCode());
        $msgFlushProducts = '';

        if (!$connection->tableColumnExists($tmpTable, 'long_product_name-fr_FR-ecommerce')) {
            //quick export
            $types = array(
                \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER
            );


            $select = $connection->select()
                ->from(
                    ['c' => $resource->getTable($tmpTable)],
                    'sku'
                )
                ->joinInner(
                    array('e' => $resource->getTable('catalog_product_entity')),
                    'c.sku = e.sku',
                    'entity_id'
                );

            $allProducts = $connection->fetchAll($select);
            $bareTags = array();
            $categsId = array();
            foreach ($allProducts as $product) {
                $bareTags[] = 'CAT_P_' . $product['entity_id'];
                $assignedProductCategories = $this->getProductAssignedCategories($product['entity_id']);
                if ($assignedProductCategories) {
                    foreach ($assignedProductCategories as $assignedProductCategory) {
                        $categsId[$assignedProductCategory] = 1;
                    }
                }
            }

            if (!empty($categsId)) {
                /** @var CategoryCollection $collection */
                $collection = $this->categoryCollectionFactory->create();
                $options = array();
                $collection
                    ->addAttributeToSelect(['entity_id', 'name', 'level'])
                    ->addAttributeToFilter('is_active', 1)
                    // ->addAttributeToFilter('level', 1)
                    ->addAttributeToFilter('entity_id', ['in' => array_keys($categsId)]);

                foreach ($collection as $category) {
                    if (!in_array($category->getLevel(), array(1, 2))) {
                        $bareTags[] = 'CAT_C_' . $category->getId();
                    }
                }
            }


            $tags = [];
            $pattern = "((^|,)%s(,|$))";
            foreach ($bareTags as $tag) {
                $tags[] = sprintf($pattern, $tag);
            }
            if (!empty($tags)) {
                if ($this->purgeCache->sendPurgeRequest(implode('|', array_unique($tags)))) {
                    $msgFlushProducts = __(' , Products Cache cleaned for: %1', join(', ', $bareTags));
                }
            }

        } else {
            //full export
            $types = array(
                \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
                \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER
            );
        }

        //flush caches
        foreach ($types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }

        $this->setMessage(
            __('Cache cleaned for: %1', join(', ', $types)) . $msgFlushProducts
        );
    }

    /**
     * @param int|null $scopeCode
     * @return int|mixed
     */
    public function getProductTemplateId($scopeCode = null)
    {
        $templateId = $this->_scopeConfig->getValue(
            self::XML_PATH_MAGEWORX_TEMPLATE_ID,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
        return $templateId ? $templateId : 0;
    }


    /**
     * Set Url Rewrite
     */
    public function setUrlRewrite()
    {

        echo 'set url natif magento';
        $resource = $this->_entities->getResource();
        $connection = $resource->getConnection();
        $tmpTable = $this->_entities->getTableName($this->getCode());

        $stores = array_merge(
            $this->_helperConfig->getStores(['lang']), // en_US
            $this->_helperConfig->getStores(['lang', 'channel_code']) // en_US-channel
        );

        $this->_urlRewriteHelper->createUrlTmpTable();

        $columns = [];

        foreach ($stores as $local => $affected) {
            if ($connection->tableColumnExists($tmpTable, 'url_key-' . $local)) {
                foreach ($affected as $store) {
                    $columns[$store['store_id']] = 'url_key-' . $local;
                }
            }
        }

        if (!count($columns)) {
            foreach ($stores as $local => $affected) {
                foreach ($affected as $store) {
                    $columns[$store['store_id']] = 'url_key';
                }
            }
        }

        foreach ($columns as $store => $column) {
            if ($store == 0) {
                continue;
            }

            $duplicates = $connection->fetchCol(
                $connection->select()
                    ->from($tmpTable, [$column])
                    ->group($column)
                    ->having('COUNT(*) > 1')
            );

            foreach ($duplicates as $urlKey) {
                if ($urlKey) {
                    $connection->update(
                        $tmpTable,
                        [$column => new Expr('CONCAT(`' . $column . '`, "-", `sku`)')],
                        ['`' . $column . '` = ?' => $urlKey]
                    );
                }
            }

            $this->_entities->setValues(
                $this->getCode(),
                $resource->getTable('catalog_product_entity'),
                ['url_key' => $column],
                4,
                $store,
                AdapterInterface::INSERT_ON_DUPLICATE
            );

            $this->_urlRewriteHelper->rewriteUrls(
                $this->getCode(),
                $store,
                $column,
                $this->_scopeConfig->getValue(productHelper::CONFIG_CATALOG_SEO_PRODUCT_URL_SUFFIX)
            );

        }

        $this->_urlRewriteHelper->dropUrlRewriteTmpTable();

        if ( $this->getProductTemplateId() > 0 ) {
            echo 'SET CUSTOM MAGEWORX URLS';
            $resource = $this->_entities->getResource();
            $connection = $resource->getConnection();
            $tmpTable = $this->_entities->getTableName($this->getCode());

            //select import products conf && simple
            $skus = $connection->fetchCol(
                $connection->select()
                    ->from($tmpTable)
                    ->where('parent is NULL')
                    ->group('sku')
            );

            // get product collection
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('name')
                ->addFieldToSelect('visibility')
                ->addAttributeToSelect('color')
                ->addAttributeToFilter('sku', ['in' => $skus]);

            //get Mageworxs custom product template
            /** @var \MageWorx\SeoXTemplates\Model\Template\Product $template */
            $template = $this->templateProductFactory->create();
            $template->load($this->getProductTemplateId());

            $template->setDateApplyStart($this->date->gmtDate());
            $dbWriter  = $this->dbWriterProductFactory->create($template->getTypeId());

            $stores = array_merge(
                $this->_helperConfig->getStores(['lang']), // en_US
                $this->_helperConfig->getStores(['lang', 'channel_code']) // en_US-channel
            );

            foreach ($stores as $local => $affected) {
                foreach ($affected as $store) {
                    $dbWriter->write($collection, $template, $store['store_id']);
                }
            }

            $template->setDateApplyFinish($this->date->gmtDate());
            $template->save();


        }

    }
}
