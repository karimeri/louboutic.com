<?php

namespace Project\VisualMerchandiser\Model\Import;

use Magento\Catalog\Model\Category;
use Magento\Store\Model\Store;
use Project\Catalog\Model\CategoryProduct;

/**
 * Class Merchandising
 * @package Project\VisualMerchandiser\Model\Import
 */
class Merchandising extends \Synolia\ImportExport\Model\Import\Merchandising\Merchandising
{
    // override column name for understanding issue
    const COLUMN_ADMIN_PATH = 'category_id';

    /**
     * @var string
     */
    protected $masterAttributeCode = 'category_id';

    /**
     * @var array
     */
    protected $_permanentAttributes = [
        self::COLUMN_ADMIN_PATH,
        self::COLUMN_SKU
    ];

    /**
     * @var array
     */
    protected $mappingFields = [
        self::COLUMN_ADMIN_PATH => self::COLUMN_PATH
    ];

    /**
     * @var \Project\Catalog\Model\CategoryProductFactory
     */
    protected $categoryProductFactory;

    /**
     * @var \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory
     */
    protected $categoryProductCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * Merchandising constructor.
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\ImportExport\Model\ImportFactory $importFactory
     * @param \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface $errorAggregator
     * @param \Synolia\ImportExport\Model\Catalog\CategoryRepository $categoryRepository
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Catalog\Model\CategoryLinkManagement $categoryLinkManagement
     * @param \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository
     * @param \Magento\Catalog\Model\CategoryProductLinkFactory $categoryProductLinkFactory
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Synolia\ImportExport\Model\Import\Catalog\CategoryPathManagementFactory $categoryPathManagementFactory
     * @param \Magento\Store\Model\App\Emulation $emulation
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Project\Catalog\Model\CategoryProductFactory $categoryProductFactory
     * @param \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\ImportExport\Model\ImportFactory $importFactory,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface $errorAggregator,
        \Synolia\ImportExport\Model\Catalog\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Catalog\Model\CategoryLinkManagement $categoryLinkManagement,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository,
        \Magento\Catalog\Model\CategoryProductLinkFactory $categoryProductLinkFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Synolia\ImportExport\Model\Import\Catalog\CategoryPathManagementFactory $categoryPathManagementFactory,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Project\Catalog\Model\CategoryProductFactory $categoryProductFactory,
        \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory,
        array $data = []
    ) {
        parent::__construct(
            $string,
            $scopeConfig,
            $importFactory,
            $resourceHelper,
            $resource,
            $errorAggregator,
            $categoryRepository,
            $categoryFactory,
            $filterManager,
            $categoryLinkManagement,
            $categoryLinkRepository,
            $categoryProductLinkFactory,
            $productRepository,
            $categoryPathManagementFactory,
            $emulation,
            $objectManager,
            $data
        );
        $this->categoryProductFactory = $categoryProductFactory;
        $this->categoryProductCollectionFactory = $categoryProductCollectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @param $rowData
     * @return array
     */
    public function _prepareData($rowData)
    {
        $finalArray = [];
        foreach ($rowData as $key => $value) {
            if (array_key_exists($key, $this->mappingFields)) {
                $finalArray[$this->mappingFields[$key]] = $value;
                continue;
            }
            $finalArray[$key] = $value;
        }
        $finalArray[self::COLUMN_CATEGORY_ID] = $rowData[self::COLUMN_ADMIN_PATH];
        return $finalArray;
    }

    /**
     * @return bool
     */
    protected function _importData()
    {
        $rowNum = 0;

        $entitiesToCreate = [];
        $entitiesToUpdate = [];
        $nbToCreate = [];
        $nbToUpdate = [];

        while ($bunch = $this->_dataSourceModel->getNextBunch()) {

            foreach ($bunch as $rowNumber => $rowData) {

                if (!$this->validateRow($rowData, $rowNumber)) {
                    continue;
                }

                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNumber);
                    continue;
                }

                $processedData                       = $this->_prepareData($rowData);
                $processedData[self::COLUMN_ROW_NUM] = $rowNum;

                $categoryLinkProducts = $this->getCategoryLinkProducts(
                    $processedData[self::COLUMN_CATEGORY_ID]
                );

                if (!array_key_exists($processedData[self::COLUMN_SKU], $categoryLinkProducts) ||
                    $this->getBehavior() == \Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE
                ) {
                    $entitiesToCreate[$processedData[self::COLUMN_CATEGORY_ID]][$processedData[self::COLUMN_SKU]] = $processedData;
                    $nbToCreate[] = $processedData;
                }
                // Remove entities update - useless as position column not used anymore

                $rowNum++;
            }

            $this->updateItemsCounterStats($nbToCreate, $nbToUpdate);

        }

        if (!empty($entitiesToCreate)) {
            if ($this->getBehavior() == \Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE) {
                $this->deleteMerchandisingEntities($entitiesToCreate);
            }
            $this->addMerchandisingEntities($entitiesToCreate);
        }

        return true;
    }

    /**
     * @param array $rowData
     * @param $rowNumber
     */
    protected function _validateRowForUpdate(array $rowData, $rowNumber)
    {
        $adminPath = trim($rowData[self::COLUMN_ADMIN_PATH]);
        $sku       = trim($rowData[self::COLUMN_SKU]);

        if (empty($adminPath)) {
            $this->addRowError(self::ERROR_ADMIN_PATH_IS_EMPTY, $rowNumber);
        }

        /** if (empty($this->categoryPathManagement->getCategoryId($adminPath))) {
        $this->addRowError(self::ERROR_ADMIN_PATH_NOT_FOUND, $rowNumber);
        }**/

        if (empty($sku)) {
            $this->addRowError(self::ERROR_CODE_COLUMN_NAME_INVALID, $rowNumber);
        }
    }

    /**
     * @param $category
     * @param $productLinks
     */
    public function addCategoryProductLink($category, $productLinks)
    {
        foreach ($productLinks as $productLink) {
            try {
                $product = $this->productRepository->get($productLink[self::COLUMN_SKU]);
                /** @var CategoryProduct $categoryProduct */
                $categoryProduct = $this->categoryProductFactory->create();
                $categoryProduct->setData('category_id', $category);
                $categoryProduct->setData('product_id', $product->getId());
                $categoryProduct->setData('position', $productLink[self::COLUMN_POSITION]);
                $categoryProduct->setData('to_merch', 0);
                $categoryProduct->save();
            } catch (\Exception $exception) {
                $this->addRowError(
                    $exception->getCode(),
                    $productLink[self::COLUMN_ROW_NUM],
                    null,
                    $exception->getMessage()
                );
            }
        }
    }

    /**
     * @param $entitiesToDelete
     */
    public function deleteMerchandisingEntities($entitiesToDelete)
    {
        foreach ($entitiesToDelete as $category => $productLink) {
            try {
                $this->deleteMerchedProducts($category);
                $categoryProductLinks = $this->getPoolProducts($category);
                $this->emulation->startEnvironmentEmulation(Store::DEFAULT_STORE_ID,
                    \Magento\Framework\App\Area::AREA_ADMINHTML);

                if (!empty($categoryProductLinks)) {
                    foreach ($categoryProductLinks as $categoryProductLink) {
                        if (array_key_exists($categoryProductLink->getSku(), $productLink)) {
                            $this->categoryLinkRepository->delete($categoryProductLink);
                        }
                    }
                }

                $this->emulation->stopEnvironmentEmulation();
            } catch (\Exception $exception) {
                $this->addRowError(
                    $exception->getCode(),
                    $productLink[self::COLUMN_ROW_NUM],
                    null,
                    $exception->getMessage()
                );
            }
        }
    }

    /**
     * @param int $categoryId
     * @return array|\Magento\Catalog\Api\Data\CategoryProductLinkInterface[]
     */
    public function getPoolProducts(int $categoryId)
    {
        /** @var Category $category */
        $category = $this->categoryFactory->create()->load($categoryId);

        $this->emulation->startEnvironmentEmulation(Store::DEFAULT_STORE_ID,
            \Magento\Framework\App\Area::AREA_ADMINHTML);

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
        $productCollection = $this->productCollectionFactory->create()->addCategoryFilter($category);

        /** @var \Magento\Catalog\Api\Data\CategoryProductLinkInterface[] $links */
        $links = [];

        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($productCollection->getItems() as $product) {
            /** @var \Magento\Catalog\Api\Data\CategoryProductLinkInterface $link */
            $link = $this->categoryProductLinkFactory->create();
            $link->setSku($product->getSku())
                ->setPosition($product->getData('cat_index_position'))
                ->setCategoryId($category->getId());
            $links[] = $link;
        }

        $this->emulation->stopEnvironmentEmulation();

        return $links;
    }

    /**
     * @param int $categoryId
     * @return bool
     * @throws \Exception
     */
    public function deleteMerchedProducts(int $categoryId)
    {
        $categoryProductCollection = $this->getCategoryProductCollection(
            [
                'category_id' => $categoryId,
                'to_merch'    => 0
            ]
        );

        foreach ($categoryProductCollection->getItems() as $item) {
            /** @var CategoryProduct $categoryProduct */
            $categoryProduct = $this->categoryProductFactory->create();
            $categoryProduct = $categoryProduct->loadByCategoryIdAndProductId($categoryId, $item->getData('product_id'));
            $categoryProduct->delete();
        }

        return true;
    }

    /**
     * @param array $filters
     * @return \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection
     */
    protected function getCategoryProductCollection(array $filters = [])
    {
        /** @var \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection $categoryProductCollection */
        $categoryProductCollection = $this->categoryProductCollectionFactory->create();
        foreach ($filters as $field => $condition) {
            $categoryProductCollection->addFieldToFilter($field, $condition);
        }
        return $categoryProductCollection->load();
    }
}
