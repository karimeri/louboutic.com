<?php

namespace Project\VisualMerchandiser\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;

/**
 * Class CategoryLinkRepository
 * @package Project\VisualMerchandiser\Model
 */
class CategoryLinkRepository extends \Magento\Catalog\Model\CategoryLinkRepository
{
    /**
     * {@inheritdoc}
     */
    public function deleteByIds($categoryId, $sku)
    {
        $category = $this->categoryRepository->get($categoryId);
        $product = $this->productRepository->get($sku);
        $productPositions = $category->getProductsPosition();
        $productPoolPositions = $category->getProductsPoolPosition();

        $productID = $product->getId();
        if (!isset($productPositions[$productID]) && !isset($productPoolPositions[$productID])) {
            throw new InputException(__("The category doesn't contain the specified product."));
        }
        if (isset($productPositions[$productID])) {
            $backupPosition = $productPositions[$productID];
            unset($productPositions[$productID]);
        }
        if (isset($productPoolPositions[$productID])) {
            $backupPoolPosition = $productPoolPositions[$productID];
            unset($productPoolPositions[$productID]);
        }

        $category->setPostedProducts($productPositions);
        $category->setPostedProductsPool($productPoolPositions);
        try {
            $category->save();
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __(
                    'Could not save product "%product" with position %position to category %category',
                    [
                        "product" => $product->getId(),
                        "position" => $backupPosition ? $backupPosition : $backupPoolPosition,
                        "category" => $category->getId()
                    ]
                ),
                $e
            );
        }
        return true;
    }
}
