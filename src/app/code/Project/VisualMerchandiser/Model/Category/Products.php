<?php

namespace Project\VisualMerchandiser\Model\Category;

use \Magento\Catalog\Model\Category\Product\PositionResolver;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DB\Select;
use Project\VisualMerchandiser\Model\Category\Product\PositionResolver as PositionResolverCustom;

/**
 * Class Products
 * @package Project\VisualMerchandiser\Model\Category
 */
class Products extends \Magento\VisualMerchandiser\Model\Category\Products
{
    /**
     * @var \Project\VisualMerchandiser\Model\Position\PoolCache
     */
    protected $poolCache;

    /**
     * @var string
     */
    protected $_poolCacheKey;

    /**
     * @var \Project\VisualMerchandiser\Model\Category\Product\PositionResolver
     */
    private $positionResolverCustom;

    /**
     * @var PositionResolver
     */
    private $positionResolver;

    /**
     * Products constructor.
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\VisualMerchandiser\Model\Position\Cache $cache
     * @param \Magento\VisualMerchandiser\Model\Sorting $sorting
     * @param \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache
     * @param PositionResolver|null $positionResolver
     * @param PositionResolverCustom|null $positionResolverCustom
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\VisualMerchandiser\Model\Position\Cache $cache,
        \Magento\VisualMerchandiser\Model\Sorting $sorting,
        \Project\VisualMerchandiser\Model\Position\PoolCache $poolCache,
        PositionResolver $positionResolver = null,
        PositionResolverCustom $positionResolverCustom = null
    ) {
        parent::__construct(
            $productFactory,
            $moduleManager,
            $cache,
            $sorting,
            $positionResolver
        );

        $this->positionResolver = $positionResolver ?: ObjectManager::getInstance()->get(PositionResolver::class);
        $this->positionResolverCustom = $positionResolverCustom ?: ObjectManager::getInstance()->get(PositionResolverCustom::class);
        $this->poolCache = $poolCache;
    }

    /**
     * @param string $key
     * @return void
     */
    public function setPoolCacheKey($key)
    {
        $this->_poolCacheKey = $key;
    }

    /**
     * @param int $categoryId
     * @param int $store
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getCollectionForGrid($categoryId, $store = null, $productPositions = NULL)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->getFactory()->create()
            ->getCollection()
            ->addAttributeToSelect([
                'sku',
                'name',
                'price',
                'small_image',
                'status'
            ]);

        $collection->getSelect()
            ->where('at_position.category_id = ?', $categoryId);

        if ($this->_moduleManager->isEnabled('Magento_CatalogInventory')) {
            $collection->joinField(
                'stock',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                ['stock_id' => $this->getStockId()],
                'left'
            );
        }

        $collection->getSelect()
            ->joinLeft(
                ['super_link' => 'catalog_product_super_link'],
                'super_link.product_id = e.entity_id',
                []
            )->where('super_link.product_id IS NULL ');

        $cache = $this->_cache->getPositions($this->_cacheKey);

        if ($cache === false) {
            $collection->joinField(
                'position',
                'catalog_category_product',
                'position',
                'product_id=entity_id',
                ['to_merch' => 0],
                'left'
            );
            $collection->setOrder('position', $collection::SORT_ORDER_ASC);

            $positions = $this->positionResolverCustom->getPositions($categoryId);

            $this->_cache->saveData($this->_cacheKey, $positions);
        } else {
            $collection->getSelect()
                ->reset(Select::WHERE)
                ->reset(Select::HAVING);

            $collection->addAttributeToFilter('entity_id', ['in' => array_keys($cache)]);
        }

        $collection = $this->applyCachedChanges($collection);

        if ($store !== null) {
            $collection->addStoreFilter($store);
        }

        return $collection;
    }

    /**
     * @param $categoryId
     * @param null $store
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCollectionForGridPool($categoryId, $store = null)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->getFactory()->create()
            ->getCollection()
            ->addAttributeToSelect([
                'sku',
                'name',
                'price',
                'small_image',
                'status'
            ]);

        $collection->getSelect()
            ->where('at_position.category_id = ?', $categoryId);

        if ($this->_moduleManager->isEnabled('Magento_CatalogInventory')) {
            $collection->joinField(
                'stock',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                ['stock_id' => $this->getStockId()],
                'left'
            );
        }

        $collection->getSelect()
            ->joinLeft(
                ['super_link' => 'catalog_product_super_link'],
                'super_link.product_id = e.entity_id',
                []
            )->where('super_link.product_id IS NULL ');

        $cache = $this->poolCache->getPositions($this->_poolCacheKey);

        if ($cache === false) {
            $collection->joinField(
                'position',
                'catalog_category_product',
                'position',
                'product_id=entity_id',
                ['to_merch' => 1],
                'left'
            );
            $collection->setOrder('position', $collection::SORT_ORDER_ASC);

            $positions = $this->positionResolverCustom->getPoolPositions($categoryId);

            $this->poolCache->saveData($this->_poolCacheKey, $positions);
        } else {
            $collection->getSelect()
                ->reset(Select::WHERE)
                ->reset(Select::HAVING);

            $collection->addAttributeToFilter('entity_id', ['in' => array_keys($cache)]);
        }

        $collection = $this->applyPoolCachedChanges($collection);

        if ($store !== null) {
            $collection->addStoreFilter($store);
        }

        return $collection;
    }

    /**
     * Save positions from collection to pool cache
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return void
     */
    public function savePoolPositions(\Magento\Catalog\Model\ResourceModel\Product\Collection $collection)
    {
        if (!$collection->isLoaded()) {
            $collection->load();
        }
        $select = clone $collection->getSelect();

        $select->reset(\Magento\Framework\DB\Select::LIMIT_COUNT);
        $select->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET);
        $this->prependColumn($select, $collection->getEntity()->getIdFieldName());

        $positions = array_flip($collection->getConnection()->fetchCol($select));

        $this->savePositionsToPoolCache($positions);
    }

    /**
     * Add needed column to the Select on the first position
     *
     * There are no problems for MySQL with several same columns in the result set
     *
     * @param \Magento\Framework\DB\Select $select
     * @param string $columnName
     * @return void
     */
    private function prependColumn(\Magento\Framework\DB\Select $select, string $columnName)
    {
        $columns = $select->getPart(\Magento\Framework\DB\Select::COLUMNS);
        array_unshift($columns, ['e', $columnName, null]);
        $select->setPart(\Magento\Framework\DB\Select::COLUMNS, $columns);
    }

    /**
     * Apply cached positions, sort order products
     * returns a base collection with WHERE IN filter applied
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function applyPoolCachedChanges(\Magento\Catalog\Model\ResourceModel\Product\Collection $collection)
    {
        $positions = $this->poolCache->getPositions($this->_poolCacheKey);

        if (empty($positions)) {
            return $collection;
        }

        $collection->getSelect()->reset(Select::ORDER);
        asort($positions, SORT_NUMERIC);

        $ids = implode(',', array_keys($positions));
        $select = $collection->getSelect();
        $field = $select->getAdapter()->quoteIdentifier('e.entity_id');
        $orderExpression = new \Zend_Db_Expr("FIELD({$field}, {$ids})");
        $select->order($orderExpression);

        $sortOrder = $this->poolCache->getSortOrder($this->_poolCacheKey);
        $sortBuilder = $this->sorting->getSortingInstance($sortOrder);

        $sortedCollection = $sortBuilder->sort($collection);

        return $sortedCollection;
    }

    /**
     * Save products positions to pool cache
     *
     * @param array $positions
     * @return void
     */
    protected function savePositionsToPoolCache($positions)
    {
        $this->poolCache->saveData(
            $this->_poolCacheKey,
            $positions
        );
    }
}
