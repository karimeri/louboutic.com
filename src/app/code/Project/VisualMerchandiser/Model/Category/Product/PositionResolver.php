<?php

namespace Project\VisualMerchandiser\Model\Category\Product;

/**
 * Class PositionResolver
 * @package Project\VisualMerchandiser\Model\Category\Product
 */
class PositionResolver extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('catalog_product_entity', 'entity_id');
    }

    /**
     * Get category product positions
     *
     * @param int $categoryId
     * @return array
     */
    public function getPositions(int $categoryId): array
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from(
            ['cpe' => $this->getTable('catalog_product_entity')],
            'entity_id'
        )->joinLeft(
            ['ccp' => $this->getTable('catalog_category_product')],
            'ccp.product_id=cpe.entity_id'
        )->where(
            'ccp.category_id = ?',
            $categoryId
        )->where(
            'ccp.to_merch = ?',
            0
        )->order(
            'ccp.position ' . \Magento\Framework\DB\Select::SQL_ASC
        )->order(
            'ccp.product_id ' . \Magento\Framework\DB\Select::SQL_DESC
        );

        return array_flip($connection->fetchCol($select));
    }

    /**
     * Get category product pool positions
     *
     * @param int $categoryId
     * @return array
     */
    public function getPoolPositions(int $categoryId): array
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from(
            ['cpe' => $this->getTable('catalog_product_entity')],
            'entity_id'
        )->joinLeft(
            ['ccp' => $this->getTable('catalog_category_product')],
            'ccp.product_id=cpe.entity_id'
        )->where(
            'ccp.category_id = ?',
            $categoryId
        )->where(
            'ccp.to_merch = ?',
            1
        )->order(
            'ccp.position ' . \Magento\Framework\DB\Select::SQL_ASC
        )->order(
            'ccp.product_id ' . \Magento\Framework\DB\Select::SQL_DESC
        );

        return array_flip($connection->fetchCol($select));
    }
}
