<?php

namespace Project\VisualMerchandiser\Model\Source\Import\Behavior;

use Magento\ImportExport\Model\Import;
use Magento\ImportExport\Model\Source\Import\AbstractBehavior;

/**
 * Class Replace
 * @package Project\VisualMerchandiser\Model\Source\Import\Behavior
 */
class Replace extends AbstractBehavior
{

    /**
     * Get array of possible values
     *
     * @return array
     */
    public function toArray()
    {
        return [
            Import::BEHAVIOR_REPLACE => __('Replace')
        ];
    }

    /**
     * Get current behaviour group code
     *
     * @return string
     */
    public function getCode()
    {
        return 'replace';
    }
}
