<?php

namespace Project\VisualMerchandiser\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CheckCategoryProducts
 * @package Project\VisualMerchandiser\Console\Command
 */
class CheckCategoryProducts extends Command
{
    const INPUT_CATEGORY_ID = 'category_id';

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory
     */
    protected $categoryProductCollectionFactory;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $configurableModel;

    /**
     * CheckCategoryProducts constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableModel
     * @param null $name
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableModel,
        $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->categoryProductCollectionFactory = $categoryProductCollectionFactory;
        $this->configurableModel = $configurableModel;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('project:visual-merchandiser:check-category-products')
            ->setDescription('Check category products');

        $this->addArgument(self::INPUT_CATEGORY_ID, InputArgument::REQUIRED, __('Category ID'));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $io = new SymfonyStyle($input, $output);

            $categoryId = $input->getArgument(self::INPUT_CATEGORY_ID);
            $this->checkCategoryProducts($categoryId, $io);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }

    private function checkCategoryProducts(int $categoryId, SymfonyStyle $io)
    {
        /** @var \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection $categoryProductCollection */
        $categoryProductCollection = $this->categoryProductCollectionFactory->create();
        $categoryProductCollection->addFieldToFilter('category_id', $categoryId);
        $categoryProductCollection->join(
            ['p' => $categoryProductCollection->getTable('catalog_product_entity')],
            'main_table.product_id=p.entity_id',
            ['type_id']
        );
        $categoryProductCollection->addOrder('position', $categoryProductCollection::SORT_ORDER_ASC);
        $items = $categoryProductCollection->getItems();

        $data = [];
        $nbConfigurable = 0;
        $nbSimple = 0;
        $nbChildren = 0;
        foreach ($items as $key => $item) {
            $parentIds = $this->configurableModel->getParentIdsByChild($item->getData('product_id'));
            if ($item->getData('type_id') === 'configurable') {
                $nbConfigurable++;
            } elseif ($item->getData('type_id') === 'simple' && !$parentIds) {
                $nbSimple++;
            } elseif ($item->getData('type_id') === 'simple' && $parentIds) {
                $nbChildren++;
            }
            $data[$key] = [
                $item->getData('product_id'),
                $parentIds ? 'simple (children)' : $item->getData('type_id'),
                $item->getData('position'),
                $item->getData('to_merch')
            ];
        }
        $io->table(
            ['Product ID', 'Type', 'Position', 'To Merch'],
            $data
        );
        $io->title(sprintf('%s product(s) in category %d', $categoryProductCollection->count(), $categoryId));
        $nbConfigurable > 0 ?
            $io->note(sprintf('%s configurable product(s) in category %d', $nbConfigurable, $categoryId)) : null;
        $nbSimple > 0 ?
            $io->note(sprintf('%s simple product(s) in category %d', $nbSimple, $categoryId)) : null;
        $nbChildren > 0 ?
            $io->warning(sprintf('%s children product(s) in category %d', $nbChildren, $categoryId)) : null;
    }
}
