<?php

namespace Project\VisualMerchandiser\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CleanCategoryProducts
 * @package Project\VisualMerchandiser\Console\Command
 */
class CleanCategoryProducts extends Command
{
    const INPUT_CATEGORY_ID = 'category_id';

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory
     */
    protected $categoryProductCollectionFactory;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $configurableModel;

    /**
     * @var \Magento\Catalog\Api\CategoryLinkRepositoryInterface
     */
    protected $categoryLinkRepository;

    /**
     * CheckCategoryProducts constructor.
     * @param \Magento\Framework\App\State $state
     * @param \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableModel
     * @param \Magento\Catalog\Api\CategoryLinkRepositoryInterface $categoryLinkRepository
     * @param null $name
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \Project\Catalog\Model\ResourceModel\CategoryProduct\CollectionFactory $categoryProductCollectionFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableModel,
        \Magento\Catalog\Api\CategoryLinkRepositoryInterface $categoryLinkRepository,
        $name = null
    ) {
        parent::__construct($name);
        $this->state = $state;
        $this->categoryProductCollectionFactory = $categoryProductCollectionFactory;
        $this->configurableModel = $configurableModel;
        $this->categoryLinkRepository = $categoryLinkRepository;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('project:visual-merchandiser:clean-category-products')
            ->setDescription('Clean category products');

        $this->addArgument(self::INPUT_CATEGORY_ID, InputArgument::REQUIRED, __('Category ID'));
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);

            $io = new SymfonyStyle($input, $output);

            $categoryId = $input->getArgument(self::INPUT_CATEGORY_ID);
            $this->cleanCategoryProducts($categoryId, $io);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $output->writeln("<error>$message</error>");
        }
    }

    private function cleanCategoryProducts(int $categoryId, SymfonyStyle $io)
    {
        /** @var \Project\Catalog\Model\ResourceModel\CategoryProduct\Collection $categoryProductCollection */
        $categoryProductCollection = $this->categoryProductCollectionFactory->create();
        $categoryProductCollection->addFieldToFilter('category_id', $categoryId);
        $categoryProductCollection->join(
            ['p' => $categoryProductCollection->getTable('catalog_product_entity')],
            'main_table.product_id=p.entity_id',
            ['sku']
        );
        $categoryProductCollection->join(
            ['s' => $categoryProductCollection->getTable('catalog_product_super_link')],
            'main_table.product_id=s.product_id',
            ['parent_id']
        );
        $categoryProductCollection->addOrder('position', $categoryProductCollection::SORT_ORDER_ASC);
        $items = $categoryProductCollection->getItems();

        $data = [];
        foreach ($items as $key => $item) {
            $productId = $item->getData('product_id');
            $sku = $item->getData('sku');
            $this->categoryLinkRepository->deleteByIds($categoryId, $sku);
            $data[$key] = [
                $productId,
                $sku
            ];
        }
        $io->title(sprintf('%s product(s) have been removed from category %d', $categoryProductCollection->count(), $categoryId));
        $io->table(
            ['Product ID', 'Sku'],
            $data
        );
    }
}
