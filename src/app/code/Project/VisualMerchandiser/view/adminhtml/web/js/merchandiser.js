/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global $H */
/*global $$ */
/*global jQuery */
/*@api*/
define([
    'jquery',
    'mage/template',
    'jquery/ui',
    'jquery/ui-multi-sortable',
    'prototype',
    'Magento_VisualMerchandiser/js/tabs',
    'Magento_VisualMerchandiser/js/add_products'
], function ($, mageTemplate) {
    'use strict';

    $.widget('mage.visualMerchandiser', {
        options: {
            'gridSelector': '[data-grid-id=catalog_category_products]',
            'gridPoolSelector': '[data-grid-id=catalog_category_products_pool]',
            'tileSelector': '[data-role=catalog_category_merchandiser]',
            'tilePoolSelector': '[data-role=catalog_category_merchandiser_pool]',
            'tabsSelector': '[data-role=merchandiser-tabs]',
            'tabsPoolSelector': '[data-role=merchandiser-tabs-pool]',
            'addProductsContainer': '[data-role=catalog_category_add_product_content]',
            'switchMode' : '[data-role="mode-switcher"]',
            'switchModePool' : '[data-role="mode-switcher-pool"]',
            'addProductsUrl': null,
            'savePositionsUrl': null,
            'getPositionsUrl': null,
            'currentCategoryId': null,
            'positionCacheKeyName': null,
            'positionCacheKey': null,
            'savePoolPositionsUrl': null,
            'getPoolPositionsUrl': null,
            'poolPositionCacheKeyName': null,
            'poolPositionCacheKey': null,
            'hideDisabledProducts': null
        },
        sourcePosition: null,
        sourceIndex: null,
        sourcePoolPosition: null,
        sourcePoolIndex: null,
        currentView: null,
        reloadAllViews: false,

        /**
         * @private
         */
        _create: function () {
            this.gridView = $(this.options.gridSelector);
            this.gridPoolView = $(this.options.gridPoolSelector);
            this.tileView = $(this.options.tileSelector);
            this.tilePoolView = $(this.options.tilePoolSelector);
            this.switchMode = $(this.options.switchMode);
            this.switchModePool = $(this.options.switchModePool);

            this.element.prepend(
                $('<input type="hidden" />')
                    .attr('id', this.options.positionCacheKeyName)
                    .attr('name', this.options.positionCacheKeyName)
                    .attr('data-form-part', this.options.formName)
                    .val(this.options.positionCacheKey)
            );

            this.element.prepend(
                $('<input type="hidden" />')
                    .attr('id', this.options.poolPositionCacheKeyName)
                    .attr('name', this.options.poolPositionCacheKeyName)
                    .attr('data-form-part', this.options.formName)
                    .val(this.options.poolPositionCacheKey)
            );

            this.setupGridView();
            this.initGridEventHandlers();
            this.setupGridPoolView();
            this.initGridPoolEventHandlers();

            this.setupTileView();
            this.setupTilePoolView();
            this.initTileEventHandlers();
            this.initTilePoolEventHandlers();

            $(this.options.addProductsContainer).visualMerchandiserAddProducts({
                dialogUrl: this.options.addProductsUrl,
                dialogButton: '[data-ui-id=category-merchandiser-add-product-button]',
                dialogSave: $.proxy(this._dialogSave, this)
            });

            this.setupSmartCategory();

            // Only updates when Ajax loads another page
            this.gridView.on(
                'contentUpdated', function () {
                    this.setupGridView();
                }.bind(this)
            );
            this.gridPoolView.on(
                'poolContentUpdated', function () {
                    this.setupGridPoolView();
                }.bind(this)
            );
            this.tileView.on(
                'contentUpdated', function () {
                    this.setupTileView();
                    this.reloadPoolViews();
                }.bind(this)
            );
            this.tilePoolView.on(
                'poolContentUpdated', function () {
                    this.setupTilePoolView();
                }.bind(this)
            );

            this.switchMode.on(
                'click', function () {
                    this.reloadViews();
                }.bind(this)
            );
            this.switchModePool.on(
                'click', function () {
                    this.reloadPoolViews();
                }.bind(this)
            );


            // Tabs setup
            $(this.options.tabsSelector).visualMerchandiserTabs({
                viewDidChange: $.proxy(this._setView, this)
            });

            // Tabs pool setup
            $(this.options.tabsPoolSelector).visualMerchandiserTabs({
                viewDidChange: $.proxy(this._setView, this)
            });
        },

        /**
         * @returns void
         */
        initGridEventHandlers: function () {
            this.gridView.on(
                'focus',
                'input[name=position]',
                this.positionFieldFocused.bindAsEventListener(this)
            ).on(
                'change',
                'input[name=position]',
                this.positionFieldChanged.bindAsEventListener(this)
            ).keypress(function (event) {
                if (event.which === Event.KEY_RETURN) {
                    $(event.target).trigger('blur');
                }
            });

            this.gridView.on('click', 'a[name=unassign]', function (e) {
                this.removeProduct(e);
            }.bind(this));
            this.gridView.on('click', '.move-top', this.moveToTop.bindAsEventListener(this));
            this.gridView.on('click', '.move-bottom', this.moveToBottom.bindAsEventListener(this));

            // Talks to paging/ajax component directly, converts events into calls
            this.gridView.on('click', '.action-next:not(.disabled)', function (e) {
                $(e.target).attr('data-value', this.getPage(this.gridView) + 1);
                $(this.options.tileSelector).visualMerchandiserTilePager('setPage', e);
            }.bind(this));
            this.gridView.on('click', '.action-previous:not(.disabled)', function (e) {
                $(e.target).attr('data-value', this.getPage(this.gridView) - 1);
                $(this.options.tileSelector).visualMerchandiserTilePager('setPage', e);
            }.bind(this));
            this.gridView.on('keypress', 'input[name=page]:not(.disabled)', function (e) {
                $(this.options.tileSelector).visualMerchandiserTilePager('inputPage', e);
            }.bind(this));
            this.gridView.on('change', 'select[name=limit]:not(.disabled)', function (e) {
                $(this.options.tileSelector).visualMerchandiserTilePager('loadByElement', e);
            }.bind(this));

            this.gridView.on('gridajaxsettings', function (event, settings) {
                settings.data[this.options.positionCacheKeyName] = this.getCacheKey();
            }.bind(this));
        },

        /**
         * @returns void
         */
        initGridPoolEventHandlers: function () {
            this.gridPoolView.on(
                'focus',
                'input[name=position]',
                this.positionFieldFocusedPool.bindAsEventListener(this)
            ).on(
                'change',
                'input[name=position]',
                this.positionFieldChangedPool.bindAsEventListener(this)
            ).keypress(function (event) {
                if (event.which === Event.KEY_RETURN) {
                    $(event.target).trigger('blur');
                }
            });

            this.gridPoolView.on('click', 'a[name=unassign]', function (e) {
                this.removePoolProduct(e);
            }.bind(this));
            this.gridPoolView.on('click', '.move-top', this.moveToTopPool.bindAsEventListener(this));
            this.gridPoolView.on('click', '.move-bottom', this.moveToBottomPool.bindAsEventListener(this));

            // Talks to paging/ajax component directly, converts events into calls
            this.gridPoolView.on('click', '.action-next:not(.disabled)', function (e) {
                $(e.target).attr('data-value', this.getPoolPage(this.gridPoolView) + 1);
                $(this.options.tilePoolSelector).visualMerchandiserTilePagerPool('setPage', e);
            }.bind(this));
            this.gridPoolView.on('click', '.action-previous:not(.disabled)', function (e) {
                $(e.target).attr('data-value', this.getPoolPage(this.gridPoolView) - 1);
                $(this.options.tilePoolSelector).visualMerchandiserTilePagerPool('setPage', e);
            }.bind(this));
            this.gridPoolView.on('keypress', 'input[name=page]:not(.disabled)', function (e) {
                $(this.options.tilePoolSelector).visualMerchandiserTilePagerPool('inputPage', e);
            }.bind(this));
            this.gridPoolView.on('change', 'select[name=limit]:not(.disabled)', function (e) {
                $(this.options.tilePoolSelector).visualMerchandiserTilePagerPool('loadByElement', e);
            }.bind(this));

            this.gridPoolView.on('gridajaxsettings', function (event, settings) {
                settings.data[this.options.poolPositionCacheKeyName] = this.getPoolCacheKey();
            }.bind(this));
        },

        /**
         * @returns void
         */
        initTileEventHandlers: function () {
            var self = this;

            this.tileView.on(
                'focus',
                'input[name=position]',
                this.positionFieldFocused.bindAsEventListener(this)
            ).on(
                'change',
                'input[name=position]',
                this.positionFieldChanged.bindAsEventListener(this)
            ).keypress(function (event) {
                if (event.which === Event.KEY_RETURN) {
                    $(event.target).trigger('blur');
                }
            });

            this.tileView.on('click', '#check-all-products', function (e) {
                e.stopPropagation();
                $('#catalog_category_merchandiser_list').find('.product_tile').toggleClass('selected', this.checked);
            });
            this.tileView.on('click', '#deleteProducts', this.removeSelectedProducts.bindAsEventListener(this));
            // click outside merch area
            $(document).on('click', function (e) {
                if ($(e.target).closest(self.options.tileSelector).length === 0) {
                    $('#catalog_category_merchandiser_list').find('.product_tile').toggleClass('selected', false);
                    self.tileView.find('#check-all-products').prop('checked', false);
                }
            });

            this.tileView.on('click', '.remove-product', this.removeProduct.bindAsEventListener(this));
            this.tileView.on('click', '.icon-gripper', function () {
                return false;
            });
            this.tileView.on('click', '.move-top', this.moveToTop.bindAsEventListener(this));
            this.tileView.on('click', '.move-bottom', this.moveToBottom.bindAsEventListener(this));

            // Talks to paging/ajax component directly, converts events into calls
            this.tileView.on('click', '.action-next:not(.disabled)', function () {
                this._getGridViewComponent().setPage(this.getPage(this.tileView) + 1);
            }.bind(this));
            this.tileView.on('click', '.action-previous:not(.disabled)', function () {
                this._getGridViewComponent().setPage(this.getPage(this.tileView) - 1);
            }.bind(this));
            this.tileView.on('keypress', 'input[name=page]:not(.disabled)', function (e) {
                this._getGridViewComponent().inputPage(e);
            }.bind(this));
            this.tileView.on('change', 'select[name=limit]:not(.disabled)', function (e) {
                this._getGridViewComponent().loadByElement(e.target);
            }.bind(this));

            this.tileView.on('gridajaxsettings', function (event, settings) {
                settings.data[this.options.positionCacheKeyName] = this.getCacheKey();
            }.bind(this));
        },

        /**
         * @returns void
         */
        initTilePoolEventHandlers: function () {
            this.tilePoolView.on(
                'focus',
                'input[name=position]',
                this.positionFieldFocusedPool.bindAsEventListener(this)
            ).on(
                'change',
                'input[name=position]',
                this.positionFieldChangedPool.bindAsEventListener(this)
            ).keypress(function (event) {
                if (event.which === Event.KEY_RETURN) {
                    $(event.target).trigger('blur');
                }
            });

            this.tilePoolView.on('click', '#check-all-products-pool', function (e) {
                e.stopPropagation();
                var merchPool = $('#catalog_category_merchandiser_pool_list');
                merchPool.find('.product_tile').toggleClass('selected', this.checked);
            });
            this.tilePoolView.on('click', '#deletePoolProducts', this.removeSelectedPoolProducts.bindAsEventListener(this));

            this.tilePoolView.on('click', '.remove-product', this.removePoolProduct.bindAsEventListener(this));
            this.tilePoolView.on('click', '.icon-gripper', function () {
                return false;
            });
            this.tilePoolView.on('click', '.move-top', this.moveToTopPool.bindAsEventListener(this));
            this.tilePoolView.on('click', '.move-bottom', this.moveToBottomPool.bindAsEventListener(this));

            // Talks to paging/ajax component directly, converts events into calls
            this.tilePoolView.on('click', '.action-next:not(.disabled)', function () {
                this._getGridPoolViewComponent().setPage(this.getPoolPage(this.tilePoolView) + 1);
            }.bind(this));
            this.tilePoolView.on('click', '.action-previous:not(.disabled)', function () {
                this._getGridPoolViewComponent().setPage(this.getPoolPage(this.tilePoolView) - 1);
            }.bind(this));
            this.tilePoolView.on('keypress', 'input[name=page]:not(.disabled)', function (e) {
                this._getGridPoolViewComponent().inputPage(e);
            }.bind(this));
            this.tilePoolView.on('change', 'select[name=limit]:not(.disabled)', function (e) {
                this._getGridPoolViewComponent().loadByElement(e.target);
            }.bind(this));

            this.tilePoolView.on('gridajaxsettings', function (event, settings) {
                settings.data[this.options.poolPositionCacheKeyName] = this.getPoolCacheKey();
            }.bind(this));
        },

        /**
         * @param {Event} event
         * @param {Array} selected
         * @param {mage.visualMerchandiserAddProducts} modal
         * @private
         */
        _dialogSave: function (event, selected, modal) {
            var selectedSortData,
                selectedSortPoolData,
                sorted,
                sortedPool,
                finalData,
                finalPoolData;

            // Pool area
            selectedSortPoolData = this.sortSelectedProductsPool(selected);
            sortedPool = this.getSortedPositionsFromData(selectedSortPoolData);

            finalPoolData = $H();
            sortedPool.each(function (item, idx) {
                finalPoolData.set(item.key, String(idx));
            });

            $('#vm_category_products_pool').val(Object.toJSON(finalPoolData));
            this.savePoolPositionCache($.proxy(this._modalDidSave, this, modal));

            // Merch area
            selectedSortData = this.sortSelectedProducts(selected);
            sorted = this.getSortedPositionsFromData(selectedSortData);

            finalData = $H();
            sorted.each(function (item, idx) {
                finalData.set(item.key, String(idx));
            });

            $('#vm_category_products').val(Object.toJSON(finalData));
            this.savePositionCache(function () {
                this.reloadViews();
            }.bind(this));
        },

        /**
         * @param {mage.visualMerchandiserAddProducts} modal
         * @private
         */
        _modalDidSave: function (modal) {
            this.reloadPoolViews();
            modal.closeDialog();
        },

        /**
         * @returns void
         */
        reloadViews: function () {
            $.when(this._getGridViewComponent().reload()).then(function () {
                this.getNewPositions();
            }.bind(this));
            $(this.options.tileSelector).visualMerchandiserTilePager('reload');
        },

        /**
         * @returns void
         */
        reloadPoolViews: function () {
            $.when(this._getGridPoolViewComponent().reload()).then(function () {
                this.getPoolNewPositions();
            }.bind(this));
            $(this.options.tilePoolSelector).visualMerchandiserTilePagerPool('reload');
        },

        /**
         * @returns {*}
         * @private
         */
        _getGridViewComponent: function () {
            // XXX: refactor
            var k = $(this.options.gridSelector).attr('id') + 'JsObject';

            return window[k];
        },

        /**
         * @returns {*}
         * @private
         */
        _getGridPoolViewComponent: function () {
            // XXX: refactor
            var k = $(this.options.gridPoolSelector).attr('id') + 'JsObject';

            return window[k];
        },

        /**
         * @param {Event} event
         * @param {View} view
         * @private
         */
        _setView: function (event, view) {
            this.currentView = view;
        },

        /**
         * @returns void
         */
        setupSmartCategory: function () {
            var attributeRules,
                switchSmartCategory,
                divSmartCategory,
                rulesSmartCategory,
                divRegularCategory;

            $('#catalog_category_sort_products_tabs').on('click', function () {
                this.savePositionCache(function () {
                    this.reloadViews();
                }.bind(this));
            }.bind(this));

            // Init
            if (this.options.hideDisabledProducts) {
                this.tileView.find('.disabled-product').toggle();
                this.gridView.find('tr:contains("Disabled")').toggle();
            }

            $('#catalog_category_hide_disabled_products_tabs').on('click', function () {
                this.tileView.find('.disabled-product').toggle();
                this.gridView.find('tr:contains("Disabled")').toggle();
            }.bind(this));

            attributeRules = {
                table: $('#attribute-rules-table'),
                itemCount: 0,

                /**
                 * @param {Object} data
                 */
                add: function (data) {
                    this.template = mageTemplate('#row-template');

                    if (typeof data.id == 'undefined') {
                        data = {
                            'id': 'rule_' + this.itemCount
                        };
                    }

                    Element.insert($$('[data-role=rules-container]')[0], this.template({
                        data: data
                    }));
                    this.disableLastLogicSelect();
                    this.showHideRulesTable();
                    $('#smart_category_table tbody tr:last').find('.smart_category_rule').each(function () {
                        $(this).bind('change', function () {
                            window.attributeRules.getRules();
                        });
                    });

                    this.itemCount++;
                },

                /**
                 * @param {Event} event
                 */
                remove: function (event) {
                    var element = $(Event.findElement(event, 'tr'));

                    element.remove();
                    this.disableLastLogicSelect();
                    this.getRules();
                    this.showHideRulesTable();
                },

                /**
                 * Hide irrelevant operators for selected attribute
                 *
                 * @param {Event} event
                 * @param {Object} data
                 */
                hideIrrelevantOperators: function (event, data) {
                    var element;

                    if (event === null && data === null) {
                        return;
                    }

                    if (event !== null) {
                        element = $(Event.findElement(event, 'tr'));
                    }

                    if (data !== null) {
                        element = data;
                    }

                    element.find('select[name=operator_select] option').show();

                    if (element.find('select[name=attribute_select] option:selected').val() === 'category_id') {
                        element.find('select[name=operator_select] option').hide();
                        element.find('select[name=operator_select] option[value=eq]').show();
                    }

                    if (
                        element.find('select[name=attribute_select] option:selected').val() === 'created_at' ||
                        element.find('select[name=attribute_select] option:selected').val() === 'updated_at'
                    ) {
                        element.find('select[name=operator_select] option[value=neq]').hide();
                        element.find('select[name=operator_select] option[value=like]').hide();
                    }
                },

                disableLastLogicSelect: function () {
                    if (this.element.find('#smart_category_table > tbody > tr').length === 1) {
                        this.element
                            .find('#smart_category_table tbody tr:first')
                            .find('.smart_category_logic_select')
                            .hide();
                    } else {
                        this.element
                            .find('#smart_category_table tbody tr:first')
                            .find('.smart_category_logic_select')
                            .show();
                    }
                    this.element.find('#smart_category_table tbody > tr').each(function (index, element) {
                        $(element).find('[name=\'logic_select\']').show();
                    });
                    this.element.find('#smart_category_table tbody tr:last').find('[name=\'logic_select\']').hide();

                    if (this.element.find('#smart_category_table > tbody > tr').length === 1) {
                        this.element.find('.logic_header').addClass('hidden');
                    } else {
                        this.element.find('.logic_header').removeClass('hidden');
                    }

                }.bind(this),

                showHideRulesTable: function () {
                    if (this.element.find('#smart_category_table > tbody > tr').length === 0) {
                        this.element.find('#smart_category_table_wrapper').addClass('hidden');
                        this.element.find('#mode_select').addClass('hidden');
                    } else {
                        this.element.find('#smart_category_table_wrapper').removeClass('hidden');
                        this.element.find('#mode_select').removeClass('hidden');
                    }
                }.bind(this),

                /**
                 * @returns void
                 */
                getRules: function () {
                    var rows = [],
                        row;

                    $('#smart_category_table tbody > tr').each(function (index, element) {
                        if ($(element).find('[name=\'attribute_select\']').val() !== '') {
                            row = {
                                'attribute': $(element).find('[name=\'attribute_select\']').val(),
                                'operator': $(element).find('[name=\'operator_select\']').val(),
                                'value': $(element).find('[name=\'rule_value\']').val(),
                                'logic': $(element).find('[name=\'logic_select\']').val()
                            };
                            rows.push(row);
                        }
                    });
                    $('#smart_category_rules').val(Object.toJSON(rows));
                },

                /**
                 * @returns void
                 */
                setRules: function () {
                    var rulesArray,
                        i;

                    if ($('#smart_category_rules').val() !== '') {
                        rulesArray = JSON.parse($('#smart_category_rules').val());

                        for (i = 0; i < rulesArray.length; i++) {
                            this.add(rulesArray[i]);
                            $('#smart_category_table tbody tr:last')
                                .find('[name=\'attribute_select\']')
                                .val(rulesArray[i].attribute);
                            $('#smart_category_table tbody tr:last')
                                .find('[name=\'operator_select\']')
                                .val(rulesArray[i].operator);
                            $('#smart_category_table tbody tr:last')
                                .find('[name=\'rule_value\']')
                                .val(rulesArray[i].value);
                            $('#smart_category_table tbody tr:last')
                                .find('[name=\'logic_select\']')
                                .val(rulesArray[i].logic);
                            attributeRules.hideIrrelevantOperators(null, $('#smart_category_table tbody tr:last'));
                        }
                    }
                }
            };

            attributeRules.setRules();

            switchSmartCategory = this.element.find('#catalog_category_smart_category_onoff');
            divSmartCategory = this.element.find('#manage-rules-panel');
            rulesSmartCategory = this.element.find('#smart_category_rules');
            divRegularCategory = this.element.find('#regular-category-settings');

            if (switchSmartCategory.is(':checked')) {
                divSmartCategory.removeClass('hidden');
                divRegularCategory.addClass('hidden');
                attributeRules.showHideRulesTable();
            }
            switchSmartCategory.change(function () {
                if (switchSmartCategory.is(':checked')) {
                    rulesSmartCategory.removeAttr('disabled');
                    divSmartCategory.removeClass('hidden');
                    divRegularCategory.addClass('hidden');
                } else {
                    divSmartCategory.addClass('hidden');
                    divRegularCategory.removeClass('hidden');
                }
                attributeRules.showHideRulesTable();
            });

            $('#smart_category_table tbody').find('.smart_category_rule').each(function () {
                jQuery(this).bind('change', function () {
                    window.attributeRules.getRules();
                });
            });

            if ($('#add_new_rule_button')) {
                Event.observe('add_new_rule_button', 'click', attributeRules.add.bind(attributeRules));
            }

            $('#manage-rules-panel').on('click', '.delete-rule', function (event) {
                attributeRules.remove(event);
            });

            $('#manage-rules-panel').on('change', 'select[name=attribute_select]', function (event) {
                attributeRules.hideIrrelevantOperators(event, null);
            });

            window.attributeRules = attributeRules;
        },

        /**
         * @returns void
         */
        setupTileView: function () {
            var sortableParent = this.tileView.find('#catalog_category_merchandiser_list'),
                data;

            sortableParent.sortable({
                scroll: true,
                scrollSensitivity: 30,
                scrollSpeed: 40,
                cancel: 'input, button',
                forcePlaceholderSize: true,
                receive: this.removeProductFromPool.bind(this),
                update: this.sortableDidUpdate.bind(this),
                start: this.sortableStartUpdate.bind(this),
                stop: $.proxy(function () {
                    if (this.isAutosortEnabled()) {
                        sortableParent.sortable('cancel');
                    }
                }, this)
            });

            data = {};
            sortableParent.data('sortable').items.each(function (instance) {
                var key = $(instance.item).find('input[name=entity_id]').val();

                data[key] = $(instance.item);
            });

            sortableParent.data('item_id_mapper', data);

            sortableParent.multisortable({
                receive: this.removeMultiProductsFromPool.bind(this),
                update: this.sortableDidUpdate.bind(this),
                start: this.sortableStartUpdate.bind(this),
                stop: $.proxy(function () {
                    if (this.isAutosortEnabled()) {
                        sortableParent.sortable('cancel');
                    }
                }, this)
            });
        },

        /**
         * @returns void
         */
        setupTilePoolView: function () {
            var sortableParent = this.tilePoolView.find('#catalog_category_merchandiser_pool_list'),
                data;

            sortableParent.sortable({
                scroll: true,
                scrollSensitivity: 30,
                scrollSpeed: 40,
                cancel: 'input, button',
                forcePlaceholderSize: true,
                connectWith: this.tileView.find('#catalog_category_merchandiser_list'),
                update: this.sortableDidPoolUpdate.bind(this),
                start: this.sortableStartPoolUpdate.bind(this),
                stop: $.proxy(function () {
                    if (this.isAutosortEnabled()) {
                        sortableParent.sortable('cancel');
                    }
                }, this)
            });
            data = {};
            sortableParent.data('sortable').items.each(function (instance) {
                var key = $(instance.item).find('input[name=entity_id]').val();

                data[key] = $(instance.item);
            });

            sortableParent.data('item_id_mapper', data);

            sortableParent.multisortable({
                update: this.sortableDidPoolUpdate.bind(this),
                start: this.sortableStartPoolUpdate.bind(this),
                stop: $.proxy(function () {
                    if (this.isAutosortEnabled()) {
                        sortableParent.sortable('cancel');
                    }
                }, this)
            });

            var poolHeight = sortableParent.height();
            this.tileView.find('#catalog_category_merchandiser_list').css('min-height', poolHeight);
        },

        /**
         * @returns void
         */
        setupGridView: function () {
            var sortableParent = this.gridView.find('table > tbody'),
                data;

            sortableParent.sortable({
                distance: 1,
                tolerance: 'pointer',
                cancel: 'input, button',
                forcePlaceholderSize: true,
                axis: 'y',
                update: this.sortableDidUpdate.bind(this),
                start: this.sortableStartUpdate.bind(this),
                stop: $.proxy(function () {
                    if (this.isAutosortEnabled()) {
                        sortableParent.sortable('cancel');
                    }
                }, this),
                grid: [ 20, 10 ]
            });

            data = {};
            sortableParent.data('sortable').items.each(function (instance) {
                var key = $(instance.item).find('input[name=entity_id]').val();

                data[key] = $(instance.item);
            });
            sortableParent.data('item_id_mapper', data);
        },

        /**
         * @returns void
         */
        setupGridPoolView: function () {
            var sortableParent = this.gridPoolView.find('table > tbody'),
                data;

            sortableParent.sortable({
                distance: 1,
                tolerance: 'pointer',
                cancel: 'input, button',
                forcePlaceholderSize: true,
                axis: 'y',
                update: this.sortableDidPoolUpdate.bind(this),
                start: this.sortableStartPoolUpdate.bind(this),
                stop: $.proxy(function () {
                    if (this.isAutosortEnabled()) {
                        sortableParent.sortable('cancel');
                    }
                }, this),
                grid: [ 20, 10 ]
            });

            data = {};
            sortableParent.data('sortable').items.each(function (instance) {
                var key = $(instance.item).find('input[name=entity_id]').val();

                data[key] = $(instance.item);
            });
            sortableParent.data('item_id_mapper', data);
        },

        sortSelectedProducts: function (selectedIds) {
            var sortData = this.getSortData(),
                sortPoolData = this.getSortPoolData(),
                selectedSortData = $H(),
                newSelectedIds,
                offset;

            selectedIds = selectedIds.filter(function (entityId) {
                return sortPoolData.get(entityId) === undefined;
            });

            newSelectedIds = selectedIds.filter(function (entityId) {
                return sortData.get(entityId) === undefined;
            });

            offset = newSelectedIds.length;
            $(selectedIds).each(function (idx, entityId) {
                var pos = 0;

                if (sortData.get(entityId) === undefined) {
                    pos = newSelectedIds.indexOf(entityId);
                } else {
                    pos = sortData.get(entityId) + offset;
                }
                selectedSortData.set(entityId, pos);
            });

            return selectedSortData;
        },

        /**
         * @param {Array} selectedIds
         * @returns {Array}
         */
        sortSelectedProductsPool: function (selectedIds) {
            var sortData = this.getSortData(),
                sortPoolData = this.getSortPoolData(),
                selectedSortData = $H(),
                newSelectedIds,
                offset;

            selectedIds = selectedIds.filter(function (entityId) {
                return sortData.get(entityId) === undefined;
            });

            newSelectedIds = selectedIds.filter(function (entityId) {
                return sortPoolData.get(entityId) === undefined;
            });

            offset = newSelectedIds.length;
            $(selectedIds).each(function (idx, entityId) {
                var pos = 0;

                if (sortPoolData.get(entityId) === undefined) {
                    pos = newSelectedIds.indexOf(entityId);
                } else {
                    pos = sortPoolData.get(entityId) + offset;
                }
                selectedSortData.set(entityId, pos);
            });

            return selectedSortData;
        },

        /**
         * @param {Array} sortData
         * @returns {Array}
         */
        getSortedPositionsFromData: function (sortData) {
            // entity_id => pos
            var sortedArr = [];

            sortData.each(Array.prototype.push.bindAsEventListener(sortedArr));
            sortedArr.sort(this.sortArrayAsc.bind(this, sortData));

            return sortedArr;
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getPage: function (view) {
            var parentView = $(view).parents('.merchandiser-tab');

            return parseInt(parentView.find('input[name=page]').val(), 10);
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getPoolPage: function (view) {
            var parentView = $(view).parents('.merchandiser-tab-pool');

            return parseInt(parentView.find('input[name=page]').val(), 10);
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getPageSize: function (view) {
            var parentView = $(view).parents('.merchandiser-tab');

            return parseInt(parentView.find('select[name=limit]').val(), 10);
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getPoolPageSize: function (view) {
            var parentView = $(view).parents('.merchandiser-tab-pool');

            return parseInt(parentView.find('select[name=limit]').val(), 10);
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getStartIdx: function (view) {
            var perPage = this.getPageSize(view);

            return this.getPage(view) * perPage - perPage;
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getPoolStartIdx: function (view) {
            var perPage = this.getPoolPageSize(view);

            return this.getPoolPage(view) * perPage - perPage;
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getFinalIndex: function (view) {
            return this.getPage(view) * this.getPageSize(view);
        },

        /**
         * @param {String} view
         * @returns {Number}
         */
        getPoolFinalIndex: function (view) {
            return this.getPoolPage(view) * this.getPoolPageSize(view);
        },

        /**
         * @returns {Number}
         */
        getTotalIndex: function () {
            return parseInt($('#catalog_category_products-total-count').text(), 10) - 1;
        },

        /**
         * @returns {Number}
         */
        getPoolTotalIndex: function () {
            return parseInt($('#catalog_category_products_pool-total-count').text(), 10) - 1;
        },

        /**
         * @returns {Boolean}
         */
        isAutosortEnabled: function () {
            return $('#catalog_category_smart_category_onoff').prop('checked');
        },

        /**
         * Used to re-populate text inputs and move items in non-active view
         * triggered by {sortable}
         *
         * @param {Event} event
         * @param {Object} ui
         */
        sortableDidUpdate: function (event, ui) {
            var to,
                from,
                otherViews,
                reload;

            if (this.isAutosortEnabled()) {
                return;
            }

            to = ui.item.index();
            from = ui.item.data('originIndex');

            this.populateFromIdx(ui.item.parents('.ui-sortable').find('> *'));

            otherViews = $(this.options.tabsSelector).find('.ui-sortable').not(ui.item.parent());
            otherViews.each(function (idx, view) {
                this.moveItemInView($(view), from, to);
            }.bind(this));

            // check if we have to reload all views after sorting data, used when moving product from pool to merch area
            reload = this.reloadAllViews;
            this.sortDataObject({
                target: ui.item.parents('.ui-sortable')
            }, reload);
            // reset reload trigger value
            this.reloadAllViews = false;
        },

        /**
         * Used to re-populate text inputs and move items in non-active view
         * triggered by {sortable}
         *
         * @param {Event} event
         * @param {Object} ui
         */
        sortableDidPoolUpdate: function (event, ui) {
            var to,
                from,
                otherViews;

            if (this.isAutosortEnabled()) {
                return;
            }

            to = ui.item.index();
            from = ui.item.data('originIndex');

            this.populateFromPoolIdx(ui.item.parents('.ui-sortable').find('> *'));

            otherViews = $(this.options.tabsPoolSelector).find('.ui-sortable').not(ui.item.parent());
            otherViews.each(function (idx, view) {
                this.moveItemInPoolView($(view), from, to);
            }.bind(this));

            this.sortPoolDataObject({
                target: ui.item.parents('.ui-sortable')
            });
        },

        /**
         * Remove product from pool
         * @param {Event} event
         * @param {Object} ui
         */
        removeProductFromPool: function (event, ui) {
            var data;
            data = this.getSortPoolData();
            data.unset(ui.item.find('[name=entity_id]').val());
            $('#vm_category_products_pool').val(Object.toJSON(data));
            // set reload trigger value
            this.reloadAllViews = true;
        },

        /**
         * Remove multiple products from pool
         * @param {Event} event
         * @param {Object} ui
         */
        removeMultiProductsFromPool: function (event, ui) {
            var list = this.tileView,
                sortPoolData = this.getSortPoolData(),
                items = list.find('.selected').filter(function(x) {
                    return $(this).data('i');
                });
            items.each(function (idx, item) {
                sortPoolData.unset($(item).find('[name=entity_id]').val());
            });
            $('#vm_category_products_pool').val(Object.toJSON(sortPoolData));
            // set reload trigger value
            this.reloadAllViews = true;
        },

        /**
         * Generic helper to move items in DOM and repopulate position indexes
         *
         * @param {Object} view
         * @param {Integer} from
         * @param {Integer} to
         */
        moveItemInView: function (view, from, to) {
            var items = view.find('>*');

            if (to > from) {
                $(items.get(from)).insertAfter($(items.get(to)));
            } else {
                $(items.get(from)).insertBefore($(items.get(to)));
            }
            this.populateFromIdx(items);
        },

        /**
         * Generic helper to move items in DOM and repopulate position indexes
         *
         * @param {Object} view
         * @param {Integer} from
         * @param {Integer} to
         */
        moveItemInPoolView: function (view, from, to) {
            var items = view.find('>*');

            if (to > from) {
                $(items.get(from)).insertAfter($(items.get(to)));
            } else {
                $(items.get(from)).insertBefore($(items.get(to)));
            }
            this.populateFromPoolIdx(items);
        },

        /**
         * Store the original index
         *
         * @param {Event} event
         * @param {Object} ui
         */
        sortableStartUpdate: function (event, ui) {
            ui.item.data('originIndex', ui.item.index());
        },

        /**
         * Store the original index
         *
         * @param {Event} event
         * @param {Object} ui
         */
        sortableStartPoolUpdate: function (event, ui) {
            ui.item.data('originIndex', ui.item.index());
        },

        /**
         * UI trigger for product remove
         *
         * @param {Event} event
         */
        removeProduct: function (event) {
            var row;

            event.preventDefault();
            row = $(event.currentTarget).parents('li,tr');

            this.removeRow(row);
        },

        /**
         * UI trigger for products remove
         *
         * @param {Event} event
         */
        removeSelectedProducts: function (event) {
            var rows,
                data = this.getSortData();

            event.preventDefault();

            rows = $('#catalog_category_merchandiser_list').children('li.selected');
            rows.each(function (idx, row) {
                data.unset($(row).find('[name=entity_id]').val());
            });

            $('#vm_category_products').val(Object.toJSON(data));
            this.savePositionCache(function () {
                this.reloadViews();
            }.bind(this));
        },

        /**
         * UI trigger for product remove
         *
         * @param {Event} event
         */
        removePoolProduct: function (event) {
            var row;

            event.preventDefault();
            row = $(event.currentTarget).parents('li,tr');

            this.removePoolRow(row);
        },

        /**
         * UI trigger for products remove
         *
         * @param {Event} event
         */
        removeSelectedPoolProducts: function (event) {
            var rows,
                data = this.getSortPoolData();

            event.preventDefault();

            rows = $('#catalog_category_merchandiser_pool_list').children('li.selected');
            rows.each(function (idx, row) {
                data.unset($(row).find('[name=entity_id]').val());
            });

            $('#vm_category_products_pool').val(Object.toJSON(data));
            this.savePoolPositionCache(function () {
                this.reloadPoolViews();
            }.bind(this));
        },

        /**
         * Remove product by refreshing the grids
         *
         * @param {Object} row
         */
        removeRow: function (row) {
            var data = this.getSortData();

            data.unset(row.find('[name=entity_id]').val());
            $('#vm_category_products').val(Object.toJSON(data));
            this.savePositionCache(function () {
                this.reloadViews();
            }.bind(this));
        },

        /**
         * Remove product by refreshing the grids
         *
         * @param {Object} row
         */
        removePoolRow: function (row) {
            var data = this.getSortPoolData();

            data.unset(row.find('[name=entity_id]').val());
            $('#vm_category_products_pool').val(Object.toJSON(data));
            this.savePoolPositionCache(function () {
                this.reloadPoolViews();
            }.bind(this));
        },

        /**
         * Triggered by clicking on move to top button
         *
         * @param {Event} event
         */
        moveToTop: function (event) {
            var input;

            event.preventDefault();

            if (this.isAutosortEnabled()) {
                return;
            }

            input = $(event.currentTarget).next('input[name=position]');
            this.moveToPosition(input, 0);
        },

        /**
         * Triggered by clicking on move to top button
         *
         * @param {Event} event
         */
        moveToTopPool: function (event) {
            var input;

            event.preventDefault();

            if (this.isAutosortEnabled()) {
                return;
            }

            input = $(event.currentTarget).next('input[name=position]');
            this.moveToPositionPool(input, 0);
        },

        /**
         * Triggered by clicking on move to bottom button
         *
         * @param {Event} event
         */
        moveToBottom: function (event) {
            var input;

            event.preventDefault();

            if (this.isAutosortEnabled()) {
                return;
            }

            input = $(event.currentTarget).prev('input[name=position]');
            this.moveToPosition(input, this.getTotalIndex());
        },

        /**
         * Triggered by clicking on move to bottom button
         *
         * @param {Event} event
         */
        moveToBottomPool: function (event) {
            var input;

            event.preventDefault();

            if (this.isAutosortEnabled()) {
                return;
            }

            input = $(event.currentTarget).prev('input[name=position]');
            this.moveToPositionPool(input, this.getTotalIndex());
        },

        /**
         * @param {Object} input
         * @param {Integer} targetPosition
         */
        moveToPosition: function (input, targetPosition) {
            this.positionFieldFocused({
                'currentTarget': input
            });

            input.val(targetPosition);
            this.changePosition(input);
        },

        /**
         * @param {Object} input
         * @param {Integer} targetPosition
         */
        moveToPositionPool: function (input, targetPosition) {
            this.positionFieldFocusedPool({
                'currentTarget': input
            });

            input.val(targetPosition);
            this.changePoolPosition(input);
        },

        /**
         * Triggered by 'onchange' and keypress
         *
         * @param {Event} event
         */
        positionFieldChanged: function (event) {
            var input = $(event.currentTarget);

            if (input.val() !== parseInt(input.val(), 10).toString()) {
                input.val(this.sourcePosition);

                return;
            }
            this.changePosition(input);
        },

        /**
         * Triggered by 'onchange' and keypress
         *
         * @param {Event} event
         */
        positionFieldChangedPool: function (event) {
            var input = $(event.currentTarget);

            if (input.val() !== parseInt(input.val(), 10).toString()) {
                input.val(this.sourcePoolPosition);

                return;
            }
            this.changePoolPosition(input);
        },

        /**
         * Do all the necessary calls to re-position an element
         *
         * @param {Object} input
         */
        changePosition: function (input) {
            var destinationPosition = parseInt(input.val(), 10),
                destinationIndex = destinationPosition - this.getStartIdx(input),
                data,
                sorted,
                result,
                movedItem;

            if (destinationPosition > this.getTotalIndex()) {
                input.val(this.getTotalIndex());
                this.changePosition(input);

                return;
            }

            // Moving within current page
            if (this.isValidMove(this.sourcePosition, destinationPosition)) {
                // Move on all views
                this.element.find('.ui-sortable').each(function (idx, item) {
                    this.moveItemInView($(item), this.sourceIndex, destinationIndex);
                }.bind(this));

                this.sortDataObject({
                    target: input.parents('.ui-sortable')
                });

                return;
            }

            // Moving off the current page
            if (
                this.isValidPosition(this.sourcePosition) &&
                destinationPosition >= 0 &&
                this.sourcePosition !== destinationPosition
            ) {
                data = this.getSortData();
                sorted = this.getSortedPositionsFromData(data);
                result = [];

                movedItem = sorted[this.sourcePosition];
                movedItem.value = String(destinationPosition);

                sorted.each(function (item, idx) {
                    if (idx !== this.sourcePosition && idx !== destinationPosition) {
                        result.push(item);
                    }

                    if (idx === destinationPosition) {
                        if (destinationPosition > this.sourcePosition) {
                            result.push(item);
                            result.push(movedItem);
                        } else {
                            result.push(movedItem);
                            result.push(item);
                        }
                    }
                }.bind(this));

                result.each(function (item, idx) {
                    data.set(item.key, String(idx));
                });

                $('#vm_category_products').val(Object.toJSON(data));

                this.savePositionCache(function () {
                    this.reloadViews();
                }.bind(this));

                return;
            }

        },

        /**
         * Do all the necessary calls to re-position an element
         *
         * @param {Object} input
         */
        changePoolPosition: function (input) {
            var destinationPosition = parseInt(input.val(), 10),
                destinationIndex = destinationPosition - this.getPoolStartIdx(input),
                data,
                sorted,
                result,
                movedItem;

            if (destinationPosition > this.getPoolTotalIndex()) {
                input.val(this.getPoolTotalIndex());
                this.changePoolPosition(input);

                return;
            }

            // Moving within current page
            if (this.isValidMovePool(this.sourcePoolPosition, destinationPosition)) {
                // Move on all views
                this.element.find('.ui-sortable').each(function (idx, item) {
                    this.moveItemInPoolView($(item), this.sourcePoolIndex, destinationIndex);
                }.bind(this));

                this.sortPoolDataObject({
                    target: input.parents('.ui-sortable')
                });

                return;
            }

            // Moving off the current page
            if (
                this.isValidPositionPool(this.sourcePoolPosition) &&
                destinationPosition >= 0 &&
                this.sourcePoolPosition !== destinationPosition
            ) {
                data = this.getSortPoolData();
                sorted = this.getSortedPositionsFromData(data);
                result = [];

                movedItem = sorted[this.sourcePoolPosition];
                movedItem.value = String(destinationPosition);

                sorted.each(function (item, idx) {
                    if (idx !== this.sourcePoolPosition && idx !== destinationPosition) {
                        result.push(item);
                    }

                    if (idx === destinationPosition) {
                        if (destinationPosition > this.sourcePoolPosition) {
                            result.push(item);
                            result.push(movedItem);
                        } else {
                            result.push(movedItem);
                            result.push(item);
                        }
                    }
                }.bind(this));

                result.each(function (item, idx) {
                    data.set(item.key, String(idx));
                });

                $('#vm_category_products_pool').val(Object.toJSON(data));

                return;
            }

        },

        /**
         * @param {Array} items
         */
        populateFromIdx: function (items) {
            var startIdx = this.getStartIdx(items);

            items.find('input[name=position][type=text]').each(function (idx, item) {
                $(item).val(startIdx + idx);
            });
        },

        populateFromPoolIdx: function (items) {
            var startIdx = this.getPoolStartIdx(items);

            items.find('input[name=position][type=text]').each(function (idx, item) {
                $(item).val(startIdx + idx);
            });
        },

        /**
         * @param {Integer} src
         * @param {Integer} dst
         * @returns {Boolean}
         */
        isValidMove: function (src, dst) {
            return this.isValidPosition(src) && this.isValidPosition(dst) && src !== dst;
        },

        /**
         * @param {Integer} src
         * @param {Integer} dst
         * @returns {Boolean}
         */
        isValidMovePool: function (src, dst) {
            return this.isValidPositionPool(src) && this.isValidPositionPool(dst) && src !== dst;
        },

        /**
         * @param {Integer} pos
         * @returns {Boolean}
         */
        isValidPosition: function (pos) {
            var view = this.currentView.find('>*:eq(0)'),
                maxPos = this.getFinalIndex(view),
                minPos = this.getStartIdx(view);

            return pos !== null && pos >= minPos && pos < maxPos;
        },

        /**
         * @param {Integer} pos
         * @returns {Boolean}
         */
        isValidPositionPool: function (pos) {
            var view = this.currentView.find('>*:eq(0)'),
                maxPos = this.getPoolFinalIndex(view),
                minPos = this.getPoolStartIdx(view);

            return pos !== null && pos >= minPos && pos < maxPos;
        },

        /**
         * Stores position for later use by this.positionFieldChanged
         *
         * @param {Event} event
         */
        positionFieldFocused: function (event) {
            var idx = parseInt($(event.currentTarget).parents('tr,li').index(), 10),
                pos = idx + this.getStartIdx($(event.currentTarget));

            if (!this.isValidPosition(pos)) {
                this.sourcePosition = null;
                this.sourceIndex = null;
            } else {
                this.sourcePosition = pos;
                this.sourceIndex = idx;
            }
        },

        /**
         * Stores position for later use by this.positionFieldChanged
         *
         * @param {Event} event
         */
        positionFieldFocusedPool: function (event) {
            var idx = parseInt($(event.currentTarget).parents('tr,li').index(), 10),
                pos = idx + this.getPoolStartIdx($(event.currentTarget));

            if (!this.isValidPositionPool(pos)) {
                this.sourcePoolPosition = null;
                this.sourcePoolIndex = null;
            } else {
                this.sourcePoolPosition = pos;
                this.sourcePoolIndex = idx;
            }
        },

        /**
         * @param {Object} sortData
         * @param {Object} a
         * @param {Object} b
         * @returns {Number}
         */
        sortArrayAsc: function (sortData, a, b) {
            var keyA = sortData.get(a.key),
                keyB = sortData.get(b.key),
                diff = parseFloat(a.value) - parseFloat(b.value);

            if (diff !== 0) {
                return diff;
            }

            if (keyA === undefined && keyB !== undefined) {
                return -1;
            }

            if (keyA !== undefined && keyB === undefined) {
                return 1;
            }

            return 0;
        },

        /**
         * @returns {Hash}
         */
        getSortData: function () {
            return $H(JSON.parse($('#vm_category_products').val()));
        },

        /**
         * @returns {Hash}
         */
        getSortPoolData: function () {
            return $H(JSON.parse($('#vm_category_products_pool').val()));
        },

        /**
         * Re-sort the actual positions array which will be sent to the server
         *
         * @param {Event} event
         * @param {boolean} reload
         */
        sortDataObject: function (event, reload = false) {
            // Data format: {entity_id => sort index, ... }
            var data = this.getSortData(),
                startIdx = this.getStartIdx($(event.target));

            // Overwrite positions with items from UI
            $(event.target).find('> *').find('[name=entity_id]').each(function (idx, item) {
                data.set($(item).val(), startIdx);
                startIdx++;
            });

            $('#vm_category_products').val(Object.toJSON(data));

            // Reload all views when moving product from pool to merch area
            if (reload) {
                this.savePositionCache(function () {
                    //Optim Perf - Disable relaod views
                    //this.reloadViews();
                    this.savePoolPositionCache(function () {
                        //Optim Perf - Disable relaod views
                        //this.reloadPoolViews();
                    }.bind(this));
                }.bind(this));
            } else {
                this.savePositionCache();
            }

            //Optim Perf
            // Refresh Count for merch and pool views
            var countMerchUpdated = this.getCountElementMerch();
            var countPoolUpdated = this.getCountElementPool();
            $('#catalog_category_products-total-count').html(countMerchUpdated);
            $('[data-ui-id="category-merchandiser-tile-total-count"]').html(countMerchUpdated);
            $('[data-ui-id="tile-total-count"]').html(countMerchUpdated);
            $('#catalog_category_products_pool-total-count').html(countPoolUpdated);
            $('[data-ui-id="category-merchandiser-tile-pool-total-count"]').html(countPoolUpdated);
            $('[data-ui-id="tile-pool-total-count"]').html(countPoolUpdated);
        },


        /**
         * Get Count of Element View
         * @returns {String}
         */
        getCountElementPool: function () {
            var jsonElementPool = JSON.parse($('#vm_category_products_pool').val());

            $.each(jsonElementPool, function(i, item) {
                if(jsonElementPool[i] ===null){
                    delete jsonElementPool[i];
                }
            });
            var countElPool = Object.keys(jsonElementPool).length;
            return countElPool.toString();
        },

        /**
         * Get Count of Element View
         * @returns {String}
         */
        getCountElementMerch: function () {
            var jsonElementMerch = JSON.parse($('#vm_category_products').val());
            var countElMerch = Object.keys(jsonElementMerch).length;
            return countElMerch.toString();
        },


        /**
         * Re-sort the actual positions array which will be sent to the server
         *
         * @param {Event} event
         */
        sortPoolDataObject: function (event) {
            // Data format: {entity_id => sort index, ... }
            var data = this.getSortPoolData(),
                startIdx = this.getPoolStartIdx($(event.target));

            // Overwrite positions with items from UI
            $(event.target).find('> *').find('[name=entity_id]').each(function (idx, item) {
                data.set($(item).val(), startIdx);
                startIdx++;
            });

            $('#vm_category_products_pool').val(Object.toJSON(data));

            this.savePoolPositionCache();
        },

        /**
         * @returns {jQuery}
         */
        getCacheKey: function () {
            return $('#' + this.options.positionCacheKeyName).val();
        },

        /**
         * @returns {jQuery}
         */
        getPoolCacheKey: function () {
            return $('#' + this.options.poolPositionCacheKeyName).val();
        },

        /**
         * @param {Function} callback
         */
        savePositionCache: function (callback) {
            var data = {
                    'category_id': this.options.currentCategoryId,
                    'positions': $('#vm_category_products').val(),
                    'sort_order': $('select.sort_order').val()
                },
                loader = typeof callback !== 'undefined';

            data[this.options.positionCacheKeyName] = this.getCacheKey();

            $.ajax({
                type: 'POST',
                url: this.options.savePositionsUrl,
                data: data,
                context: $('body')
            }).success(function () {
                if (callback) {
                    callback();
                }
            });
        },

        /**
         * @param {Function} callback
         */
        savePoolPositionCache: function (callback) {
            var data = {
                    'category_id': this.options.currentCategoryId,
                    'positions': $('#vm_category_products_pool').val(),
                    'sort_order': $('select.sort_order').val()
                },
                loader = typeof callback !== 'undefined';

            data[this.options.poolPositionCacheKeyName] = this.getPoolCacheKey();

            $.ajax({
                type: 'POST',
                url: this.options.savePoolPositionsUrl,
                data: data,
                context: $('body')
            }).success(function () {
                if (callback) {
                    callback();
                }
            });
        },

        /**
         * @param {Function} callback
         */
        getNewPositions: function (callback) {
            var data = {
                    'category_id': this.options.currentCategoryId
                },
                loader = typeof callback !== 'undefined';

            data[this.options.positionCacheKeyName] = this.getCacheKey();

            if ($('[data-role=catalog_category_merchandiser]').find('.disabled-product').first().css("display") == 'list-item') {
                $('[data-role=catalog_category_merchandiser]').find('.disabled-product').show();
            }
            if ($('[data-grid-id=catalog_category_products]').find('tr:contains("Disabled")').first().css("display") == 'list-item') {
                $('[data-grid-id=catalog_category_products]').find('tr:contains("Disabled")').show();
            }

            $.ajax({
                type: 'POST',
                url: this.options.getPositionsUrl,
                data: data,
                context: $('body'),
                showLoader: loader
            }).success(function (result) {
                $('#vm_category_products').val(result);
            });
        },

        /**
         * @param {Function} callback
         */
        getPoolNewPositions: function (callback) {
            var data = {
                    'category_id': this.options.currentCategoryId
                },
                loader = typeof callback !== 'undefined';

            if ($('[data-role=catalog_category_merchandiser]').find('.disabled-product').first().css("display") == 'list-item') {
                $('[data-role=catalog_category_merchandiser]').find('.disabled-product').show();
            }
            if ($('[data-grid-id=catalog_category_products]').find('tr:contains("Disabled")').first().css("display") == 'list-item') {
                $('[data-grid-id=catalog_category_products]').find('tr:contains("Disabled")').show();
            }

            data[this.options.poolPositionCacheKeyName] = this.getPoolCacheKey();

            $.ajax({
                type: 'POST',
                url: this.options.getPoolPositionsUrl,
                data: data,
                context: $('body'),
                showLoader: loader
            }).success(function (result) {
                $('#vm_category_products_pool').val(result);
            });
        }
    });

    return $.mage.visualMerchandiser;
});
