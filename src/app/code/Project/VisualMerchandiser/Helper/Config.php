<?php

namespace Project\VisualMerchandiser\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 * @package Project\VisualMerchandiser\Helper
 */
class Config extends AbstractHelper
{
    const XML_PATH_TO_MERCH_FLAG_ENABLED = 'louboutin_merchandising/to_merch_flag/enabled';

    const XML_PATH_HIDE_DISABLED_PRODUCTS = 'louboutin_merchandising/visual_merchandiser/hide_disabled_products';

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_TO_MERCH_FLAG_ENABLED
        );
    }

    /**
     * @return bool
     */
    public function hideDisabledProducts()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_HIDE_DISABLED_PRODUCTS
        );
    }
}
