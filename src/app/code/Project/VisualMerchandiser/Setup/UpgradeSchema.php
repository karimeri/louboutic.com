<?php

namespace Project\VisualMerchandiser\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Project\VisualMerchandiser\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE_CATALOG_CATEGORY_PRODUCT = 'catalog_category_product';

    const COLUMN_TO_MERCH = 'to_merch';

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->addToMerchColumn($installer);
        }

        $installer->endSetup();
    }

    /**
     * @param SchemaSetupInterface $installer
     */
    private function addToMerchColumn(SchemaSetupInterface $installer)
    {
        $installer->getConnection()->addColumn(
            $installer->getTable(self::TABLE_CATALOG_CATEGORY_PRODUCT),
            self::COLUMN_TO_MERCH,
            [
                'type'     => Table::TYPE_BOOLEAN,
                'unsigned' => false,
                'nullable' => false,
                'comment'  => 'To Merch',
                'default'  => 0
            ]
        );
    }
}
