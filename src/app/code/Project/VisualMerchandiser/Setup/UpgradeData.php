<?php

namespace Project\VisualMerchandiser\Setup;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\VisualMerchandiser\Block\Adminhtml\Category\Merchandiser\Tile;
use Psr\Log\LoggerInterface;
use Synolia\Standard\Setup\Eav\CategorySetup;
use Synolia\Standard\Setup\Eav\ProductSetup;

/**
 * Class UpgradeData
 * @package Project\VisualMerchandiser\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CategorySetup
     */
    protected $categorySetup;

    /**
     * @var ProductSetup
     */
    protected $productSetup;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var ResourceConfig
     */
    private $resourceConfig;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * UpgradeData constructor.
     * @param CategorySetup $categorySetup
     * @param ProductSetup $productSetup
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ProductCollectionFactory $productCollectionFactory
     * @param Action $action
     * @param ResourceConfig $resourceConfig
     * @param LoggerInterface $logger
     */
    public function __construct(
        CategorySetup $categorySetup,
        ProductSetup $productSetup,
        CategoryCollectionFactory $categoryCollectionFactory,
        CategoryRepositoryInterface $categoryRepository,
        ProductCollectionFactory $productCollectionFactory,
        Action $action,
        ResourceConfig $resourceConfig,
        LoggerInterface $logger
    ) {
        $this->categorySetup                = $categorySetup;
        $this->productSetup                 = $productSetup;
        $this->categoryCollectionFactory    = $categoryCollectionFactory;
        $this->categoryRepository           = $categoryRepository;
        $this->productCollectionFactory     = $productCollectionFactory;
        $this->productAction                = $action;
        $this->resourceConfig               = $resourceConfig;
        $this->logger                       = $logger;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->initToMerchAttributes();
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $this->deleteToMerchProductAttribute();
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $this->updateAttributesToDisplay();
        }
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $this->displayStockAttribute();
        }

        $installer->endSetup();
    }

    private function initToMerchAttributes()
    {
        try {
            // Create "to_merch" category attribute
            $this->categorySetup->saveCategoryAttribute(
                'to_merch',
                [
                    'type'                      => 'int',
                    'label'                     => 'To Merch',
                    'input'                     => 'boolean',
                    'source'                    => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'required'                  => false,
                    'default'                   => false,
                    'sort_order'                => 200,
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'used_in_product_listing'   => false,
                    'group'                     => 'General Information',
                    'visible'                   => true,
                    'user_defined'              => true
                ]
            );

            // Create "to_merch" product attribute
            $this->productSetup->saveProductAttribute(
                'to_merch',
                [
                    'type'                      => 'int',
                    'label'                     => 'To Merch',
                    'input'                     => 'boolean',
                    'source'                    => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                    'required'                  => false,
                    'default'                   => false,
                    'sort_order'                => 200,
                    'global'                    => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'used_in_product_listing'   => false,
                    'group'                     => 'General',
                    'visible'                   => true,
                    'user_defined'              => true
                ]
            );

            // Init existing categories attribute value
            /** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categoryCollection */
            $categoryCollection = $this->categoryCollectionFactory->create();
            $categoryCollection->addFieldToFilter('path', ['neq' => '1']);
            /** @var Category $category */
            foreach ($categoryCollection->getItems() as $category) {
                $category->setCustomAttribute('to_merch', 0);
                $this->categoryRepository->save($category);
            }

            // Init existing products attribute value
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
            $productCollection = $this->productCollectionFactory->create();
            $this->productAction->updateAttributes($productCollection->getAllIds(), ['to_merch' => 0], 0);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    private function deleteToMerchProductAttribute()
    {
        $this->categorySetup->removeAttribute(Product::ENTITY, 'to_merch');
    }

    private function updateAttributesToDisplay()
    {
        $this->resourceConfig->saveConfig(
            Tile::XML_PATH_ADDITIONAL_ATTRIBUTES,
            'name,sku,price,status',
            'default',
            '0'
        );
    }

    private function displayStockAttribute()
    {
        $this->resourceConfig->saveConfig(
            Tile::XML_PATH_ADDITIONAL_ATTRIBUTES,
            'name,sku,price,stock,status',
            'default',
            '0'
        );
    }
}
