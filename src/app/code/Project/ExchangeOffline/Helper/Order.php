<?php

namespace Project\ExchangeOffline\Helper;

use Magento\Sales\Model\Order as ModelOrder;
use Magento\Rma\Model\RmaRepository;
use Magento\Framework\DB\Transaction;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\CustomerFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;

/**
 * Class Order
 *
 * @package Project\ExchangeOffline\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Order extends AbstractHelper
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepositoryInterface;

    /**
     * @var CartManagementInterface
     */
    protected $cartManagementInterface;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * @var Transaction
     */
    protected $transaction;

    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;

    /**
     * @var ItemRepository
     */
    protected $itemRepository;

    /**
     * Config constructor.
     *
     * @param Context                     $context
     * @param OrderRepository             $orderRepository
     * @param RmaRepository               $rmaRepository
     * @param StoreManagerInterface       $storeManager
     * @param ProductRepository           $productRepository
     * @param CartRepositoryInterface     $cartRepositoryInterface
     * @param CartManagementInterface     $cartManagementInterface
     * @param CustomerFactory             $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param InvoiceService              $invoiceService
     * @param Transaction                 $transaction
     * @param InvoiceRepository           $invoiceRepository
     * @param InvoiceSender               $invoiceSender
     */
    public function __construct(
        Context $context,
        OrderRepository $orderRepository,
        RmaRepository $rmaRepository,
        StoreManagerInterface $storeManager,
        ProductRepository $productRepository,
        CartRepositoryInterface $cartRepositoryInterface,
        CartManagementInterface $cartManagementInterface,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        InvoiceService $invoiceService,
        Transaction $transaction,
        InvoiceRepository $invoiceRepository,
        InvoiceSender $invoiceSender,
        ItemRepository $itemRepository
    ) {
        $this->orderRepository         = $orderRepository;
        $this->rmaRepository           = $rmaRepository;
        $this->storeManager            = $storeManager;
        $this->productRepository       = $productRepository;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory         = $customerFactory;
        $this->customerRepository      = $customerRepository;
        $this->invoiceService          = $invoiceService;
        $this->transaction             = $transaction;
        $this->invoiceRepository       = $invoiceRepository;
        $this->invoiceSender           = $invoiceSender;
        $this->itemRepository          = $itemRepository;

        parent::__construct($context);
    }

    /**
     * @param Creditmemo $creditMemo
     *
     * @return \Magento\Sales\Api\Data\OrderInterface|ModelOrder
     * @throws \Exception
     */
    public function createOrderAndInvoiceFromCreditMemo($creditMemo)
    {
        $rma   = $this->rmaRepository->get($creditMemo->getRmaId());
        $order = $creditMemo->getOrder();

        $store = $this->storeManager->getStore();

        $cartId = $this->cartManagementInterface->createEmptyCart();
        $quote  = $this->cartRepositoryInterface->get($cartId);
        $quote->setStore($store);

        $quote->setCurrency();
        $customer = $this->customerRepository->get($order->getCustomerEmail());
        $quote->assignCustomer($customer);

        // add items to quote
        foreach ($rma->getItems(true) as $item) {
            $orderItem = $this->itemRepository->get($item->getOrderItemId());
            $product   = $this->productRepository->getById($orderItem->getProductId());
            $product->setPrice($orderItem->getPrice());
            $quote->addProduct($product, (int)$item->getQty());
        }

        // set address to quote
        $quote->getBillingAddress()->addData($order->getBillingAddress()->getData());
        $quote->getShippingAddress()->addData($order->getShippingAddress()->getData());

        // Collect Rates and Set Shipping & Payment Method
        $quote->getShippingAddress()->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod($order->getShippingMethod())
        ;

        $quote->setPaymentMethod('exchange_offline');

        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => 'exchange_offline']);
        $quote->save();

        // Collect Totals
        $quote->collectTotals();

        // Create Order From Quote
        $quote   = $this->cartRepositoryInterface->get($quote->getId());
        $orderId = $this->cartManagementInterface->placeOrder($quote->getId());
        $order   = $this->orderRepository->get($orderId);

        $order->setRmaId($rma->getId());
        $order->setEmailSent(0);
        $order->setState(OrderModel::STATE_PROCESSING);

        if ($order->getEntityId()) {
            $this->createInvoiceFromOrder($order);
            $rma->setStatus('closed');
            $this->rmaRepository->save($rma);

            return $order;
        } else {
            throw new \Exception(sprintf('There has been an error creating an order from the rma : %s', $rma->getId()));
        }
    }

    /**
     * @param ModelOrder $order
     */
    public function createInvoiceFromOrder($order)
    {
        if ($order->canInvoice()) {
            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->setRmaId($order->getRmaId());
            $invoice->register();

            $this->invoiceRepository->save($invoice);

            $transactionSave = $this->transaction->addObject($invoice)
                ->addObject($invoice->getOrder())
            ;
            $transactionSave->save();

            $this->invoiceSender->send($invoice);

            $order->addStatusHistoryComment(__('Invoice', $invoice->getId()))
                ->setIsCustomerNotified(false)
                ->save()
            ;
        }
    }
}
