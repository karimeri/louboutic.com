<?php

namespace Project\ExchangeOffline\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Config
 *
 * @package Project\ExchangeOffline\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_EXCHANGE_OFFLINE_ACTIVE = 'payment/exchange_offline/active';

    /**
     * @param             $configName
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     */
    public function isActive($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_EXCHANGE_OFFLINE_ACTIVE, $scopeCode);
    }
}
