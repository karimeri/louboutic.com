<?php

namespace Project\ExchangeOffline\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade1
 * @package Project\ExchangeOffline\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    const TABLE_ORDER = 'sales_order';
    const TABLE_QUOTE = 'quote';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade1 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnToAdd = [
            [
                'name' => 'creditmemo_id',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'Origin creditmemo'
                ]
            ]
        ];

        $this->abstractSetup->addColumn(
            self::TABLE_ORDER,
            $columnToAdd[0]['name'],
            $columnToAdd[0]['definition']
        );

        $this->abstractSetup->addColumn(
            self::TABLE_QUOTE,
            $columnToAdd[0]['name'],
            $columnToAdd[0]['definition']
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add column creditmemo_id to order table';
    }
}
