<?php

namespace Project\ExchangeOffline\Setup;

use Magento\Framework\App\Config;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Magento\Store\Model\StoreManager;
use Synolia\Standard\Setup\ConfigSetupFactory;

use Project\Core\Setup\CoreInstallData;

/**
 * Class InstallData
 * @package   Project\ExchangeOffline\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData extends CoreInstallData implements InstallDataInterface
{
    const TABLE_ORDER       = 'sales_order';
    const TABLE_INVOICE     = 'sales_invoice';
    const TABLE_CREDIT_MEMO = 'sales_creditmemo';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * InstallData constructor.
     * @param DirectoryList      $directoryList
     * @param Csv                $csvProcessor
     * @param Reader             $moduleReader
     * @param StoreManager       $storeManager
     * @param WriterInterface    $configWriter
     * @param Config             $config
     * @param ConfigSetupFactory $configSetupFactory
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $configWriter,
        Config $config,
        ConfigSetupFactory $configSetupFactory
    ) {
        parent::__construct(
            $directoryList,
            $csvProcessor,
            $moduleReader,
            $storeManager,
            $configWriter
        );

        $this->config             = $config;
        $this->configSetupFactory = $configSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $this->setupOrder($setup);
        $this->setupInvoice($setup);
        $this->setupCreditMemo($setup);
        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function setupOrder(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::TABLE_ORDER),
            'rma_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'length'   => 10,
                'nullable' => true,
                'comment'  => 'RMA reference',
            ]
        )
        ;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function setupInvoice(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::TABLE_INVOICE),
            'rma_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'length'   => 10,
                'nullable' => true,
                'comment'  => 'RMA reference',
            ]
        )
        ;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function setupCreditMemo(ModuleDataSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::TABLE_CREDIT_MEMO),
            'type',
            [
                'type'     => Table::TYPE_TEXT,
                'length'   => 255,
                'nullable' => true,
                'comment'  => 'Type of Credit Memo',
            ]
        )
        ;

        $setup->getConnection()->addColumn(
            $setup->getTable(self::TABLE_CREDIT_MEMO),
            'rma_id',
            [
                'type'     => Table::TYPE_INTEGER,
                'length'   => 10,
                'nullable' => true,
                'comment'  => 'RMA reference',
            ]
        )
        ;
    }
}
