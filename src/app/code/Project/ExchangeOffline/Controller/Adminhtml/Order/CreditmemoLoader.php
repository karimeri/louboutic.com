<?php

namespace Project\ExchangeOffline\Controller\AdminHtml\Order;

use Magento\Framework\Registry;
use Magento\Backend\Model\Session;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader as BaseCreditmemoLoader;

use Project\ExchangeOffline\Model\Order\CreditmemoFactory;

/**
 * Class CreditmemoLoader
 *
 * @package Project\ExchangeOffline\Controller\AdminHtml\Order\Creditmemo
 * @author  Synolia <contact@synolia.com>
 */
class CreditmemoLoader extends BaseCreditmemoLoader
{
    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param CreditmemoFactory             $creditmemoFactory
     * @param OrderFactory                  $orderFactory
     * @param InvoiceRepositoryInterface    $invoiceRepository
     * @param ManagerInterface              $eventManager
     * @param Session                       $backendSession
     * @param MessageManagerInterface       $messageManager
     * @param Registry                      $registry
     * @param StockConfigurationInterface   $stockConfiguration
     * @param array                         $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoFactory $creditmemoFactory,
        OrderFactory $orderFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        ManagerInterface $eventManager,
        Session $backendSession,
        MessageManagerInterface $messageManager,
        Registry $registry,
        StockConfigurationInterface $stockConfiguration,
        RmaRepository $rmaRepository,
        array $data = []
    ) {
        $this->rmaRepository = $rmaRepository;

        parent::__construct(
            $creditmemoRepository,
            $creditmemoFactory,
            $orderFactory,
            $invoiceRepository,
            $eventManager,
            $backendSession,
            $messageManager,
            $registry,
            $stockConfiguration,
            $data
        );
    }

    /**
     * Initialize creditmemo model instance
     *
     * @return Creditmemo|false
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * phpcs:disable
     */
    // @codingStandardsIgnoreLine
    public function load()
    {
        // phpcs:enable
        $creditmemo   = false;
        $creditmemoId = $this->getCreditmemoId();
        $orderId      = $this->getOrderId();
        $rmaId        = $this->getRmaId();

        if ($creditmemoId) {
            $creditmemo = $this->creditmemoRepository->get($creditmemoId);
        } elseif ($rmaId) {
            $data  = $this->getCreditmemo();
            $rma   = $this->rmaRepository->get($rmaId);
            $order = $rma->getOrder();
            $invoice = $this->_initInvoice($order);

            if (!$this->_canCreditmemo($order)) {
                return false;
            }

            $savedData = $this->_getItemData();

            $qtys        = [];
            $backToStock = [];
            foreach ($savedData as $orderItemId => $itemData) {
                if (isset($itemData['qty'])) {
                    $qtys[$orderItemId] = $itemData['qty'];
                }
                if (isset($itemData['back_to_stock'])) {
                    $backToStock[$orderItemId] = true;
                }
            }
            $data['qtys'] = $qtys;

            $creditmemo = $this->creditmemoFactory->createByRma($rma, $data, $invoice);

            /**
             * Process back to stock flags
             */
            foreach ($creditmemo->getAllItems() as $creditmemoItem) {
                $creditmemoItem->setBackToStock(true);
            }
        } elseif ($orderId) {
            $data    = $this->getCreditmemo();
            $order   = $this->orderFactory->create()->load($orderId);
            $invoice = $this->_initInvoice($order);

            if (!$this->_canCreditmemo($order)) {
                return false;
            }

            $savedData = $this->_getItemData();

            $qtys        = [];
            $backToStock = [];
            foreach ($savedData as $orderItemId => $itemData) {
                if (isset($itemData['qty'])) {
                    $qtys[$orderItemId] = $itemData['qty'];
                }
                if (isset($itemData['back_to_stock'])) {
                    $backToStock[$orderItemId] = true;
                }
            }
            $data['qtys'] = $qtys;

            if ($invoice) {
                $creditmemo = $this->creditmemoFactory->createByInvoice($invoice, $data);
            } else {
                $creditmemo = $this->creditmemoFactory->createByOrder($order, $data);
            }

            /**
             * Process back to stock flags
             */
            foreach ($creditmemo->getAllItems() as $creditmemoItem) {
                $orderItem = $creditmemoItem->getOrderItem();
                $parentId = $orderItem->getParentItemId();
                if ($parentId && isset($backToStock[$parentId]) && $backToStock[$parentId]) {
                    $creditmemoItem->setBackToStock(true);
                } elseif (isset($backToStock[$orderItem->getId()])) {
                    $creditmemoItem->setBackToStock(true);
                } elseif (empty($savedData)) {
                    $creditmemoItem->setBackToStock(
                        $this->stockConfiguration->isAutoReturnEnabled()
                    );
                } else {
                    $creditmemoItem->setBackToStock(false);
                }
            }
        }

        $this->eventManager->dispatch(
            'adminhtml_sales_order_creditmemo_register_before',
            ['creditmemo' => $creditmemo, 'input' => $this->getCreditmemo()]
        );

        $this->registry->register('current_creditmemo', $creditmemo);

        return $creditmemo;
    }
}
