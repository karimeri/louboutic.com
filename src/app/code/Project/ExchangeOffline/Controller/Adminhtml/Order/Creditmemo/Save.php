<?php

namespace Project\ExchangeOffline\Controller\AdminHtml\Order\Creditmemo;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Rma\Model\RmaRepository;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Magento\Sales\Controller\Adminhtml\Order\Creditmemo\Save as BaseSave;
use Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader;

use Project\ExchangeOffline\Helper\Order;

/**
 * Class Save
 *
 * @package Project\ExchangeOffline\Controller\AdminHtml\Order\Creditmemo
 * @author  Synolia <contact@synolia.com>
 */
class Save extends BaseSave
{
    /**
     * @var Order
     */
    protected $orderHelper;

    /**
     * @var CreditmemoManagementInterface
     */
    protected $creditmemoManagement;

    /**
     * @var \Magento\Rma\Model\RmaRepository
     */
    protected $rmaRepository;

    /**
     * @param Context $context
     * @param CreditmemoLoader $creditmemoLoader
     * @param CreditmemoSender $creditmemoSender
     * @param ForwardFactory $resultForwardFactory
     * @param Order $orderHelper
     * @param CreditmemoManagementInterface $creditmemoManagement
     * @param \Magento\Rma\Model\RmaRepository $rmaRepository
     */
    public function __construct(
        Context $context,
        CreditmemoLoader $creditmemoLoader,
        CreditmemoSender $creditmemoSender,
        ForwardFactory $resultForwardFactory,
        Order $orderHelper,
        CreditmemoManagementInterface $creditmemoManagement,
        RmaRepository $rmaRepository
    ) {
        $this->orderHelper          = $orderHelper;
        $this->creditmemoManagement = $creditmemoManagement;

        parent::__construct(
            $context,
            $creditmemoLoader,
            $creditmemoSender,
            $resultForwardFactory
        );

        $this->rmaRepository = $rmaRepository;
    }

    /**
     * Save creditmemo
     * We can save only new creditmemo. Existing creditmemos are not editable
     *
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Backend\Model\View\Result\Forward
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * phpcs:disable Generic.Metrics.CyclomaticComplexity
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data           = $this->getRequest()->getPost('creditmemo');
        if (!empty($data['comment_text'])) {
            $this->_getSession()->setCommentText($data['comment_text']);
        }
        try {
            $this->creditmemoLoader->setOrderId($this->getRequest()->getParam('order_id'));
            $this->creditmemoLoader->setCreditmemoId($this->getRequest()->getParam('creditmemo_id'));
            $this->creditmemoLoader->setCreditmemo($this->getRequest()->getParam('creditmemo'));
            $this->creditmemoLoader->setInvoiceId($this->getRequest()->getParam('invoice_id'));
            // start synolia
            $this->creditmemoLoader->setRmaId($this->getRequest()->getParam('rma_id'));
            // end synolia

            $creditmemo = $this->creditmemoLoader->load();

            if ($creditmemo) {
                if (!$creditmemo->isValidGrandTotal()) {
                    throw new LocalizedException(
                        __('The credit memo\'s total must be positive.')
                    );
                }

                if (!empty($data['comment_text'])) {
                    $creditmemo->addComment(
                        $data['comment_text'],
                        isset($data['comment_customer_notify']),
                        isset($data['is_visible_on_front'])
                    );

                    $creditmemo->setCustomerNote($data['comment_text']);
                    $creditmemo->setCustomerNoteNotify(isset($data['comment_customer_notify']));
                }

                if (isset($data['do_offline'])) {
                    //do not allow online refund for Refund to Store Credit
                    if (!$data['do_offline'] && !empty($data['refund_customerbalance_return_enable'])) {
                        throw new LocalizedException(
                            __('Cannot create online refund for Refund to Store Credit.')
                        );
                    }
                }

                $creditmemo->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
                $this->creditmemoManagement->refund($creditmemo, (bool)$data['do_offline']);

                if (!empty($data['send_email'])) {
                    $this->creditmemoSender->send($creditmemo);
                }

                $this->messageManager->addSuccess(__('You created the credit memo.'));
                $this->_getSession()->getCommentText(true);

                // start synolia
                $resultRedirect->setPath('sales/order/view', ['order_id' => $creditmemo->getOrderId()]);

                if ($this->getRequest()->getParam('rma_id')) {

                    $backToStock = false;
                    foreach ($creditmemo->getAllItems() as $creditmemoItem) {
                        $backToStock = (bool) $creditmemoItem->getBackToStock();
                        break;
                    }

                    $rma = $this->rmaRepository->get($this->getRequest()->getParam('rma_id'));
                    $rma->setBackToStock($backToStock);
                    $this->rmaRepository->save($rma);

                    $order = $creditmemo->getOrder();

                    if (mb_strripos($rma->getResolution(), 'change') !== false) {
                        $resultRedirect->setPath(
                            'sales/order_create/index/',
                            [
                                'customer_id' => $order->getCustomerId(),
                                'store_id' => $order->getStoreId(),
                                'order_id' => $order->getId(),
                                'creditmemo_id' => $creditmemo->getId(),
                                'rma_id' => $rma->getId(),
                            ]
                        );
                    }
                }
                // end synolia

                return $resultRedirect;
            } else {
                $resultForward = $this->resultForwardFactory->create();
                $resultForward->forward('noroute');
                return $resultForward;
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
            $this->_getSession()->setFormData($data);
        } catch (\Exception $e) {
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            $this->messageManager->addError(__('We can\'t save the credit memo right now.'));
        }
        $resultRedirect->setPath('sales/*/new', ['_current' => true]);

        return $resultRedirect;
    }
}
