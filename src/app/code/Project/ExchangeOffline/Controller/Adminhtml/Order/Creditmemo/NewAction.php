<?php

namespace Project\ExchangeOffline\Controller\Adminhtml\Order\Creditmemo;

use Magento\Sales\Controller\Adminhtml\Order\Creditmemo\NewAction as BaseNewAction;

/**
 * Class NewAction
 *
 * @package Project\ExchangeOffline\Controller\AdminHtml\Order\Creditmemo
 * @author  Synolia <contact@synolia.com>
 */
class NewAction extends BaseNewAction
{
    /**
     * Creditmemo create page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        $this->creditmemoLoader->setRmaId($this->getRequest()->getParam('rma_id'));

        return parent::execute();
    }
}
