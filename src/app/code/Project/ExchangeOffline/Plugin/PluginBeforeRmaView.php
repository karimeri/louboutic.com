<?php

namespace Project\ExchangeOffline\Plugin;

use Magento\Framework\UrlInterface;
use Magento\Rma\Model\RmaRepository;
use Magento\Rma\Block\Adminhtml\Rma\Edit;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Sales\Model\Order\CreditmemoRepository;

use Project\Rma\Model\Rma;
use Project\Rma\Manager\EavValueManager;
use Project\ExchangeOffline\Helper\Order;
use Project\ExchangeOffline\Helper\Config;

/**
 * Class PluginBeforeRmaView
 *
 * @package Project\ExchangeOffline\Plugin
 * @author  Synolia <contact@synolia.com>
 */
class PluginBeforeRmaView
{
    const EXCHANGE_VALUE = 'Exchange';

    /**
     * @var RmaRepository
     */
    protected $rmaRepository;

    /**
     * @var CreditmemoRepository
     */
    protected $creditmemoRepository;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var AdapterInterface
     */
    protected $connection;

    /**
     * @var Order
     */
    protected $helper;

    /**
     * @var Config
     */
    protected $helperConfig;

    /**
     * PluginBeforeRmaView constructor.
     *
     * @param RmaRepository        $rmaRepository
     * @param UrlInterface         $urlBuilder
     * @param CreditmemoRepository $creditmemoRepository
     * @param Order                $helper
     * @param Config               $helperConfig
     * @param EavValueManager      $eavValueManager
     */
    public function __construct(
        RmaRepository $rmaRepository,
        UrlInterface $urlBuilder,
        CreditmemoRepository $creditmemoRepository,
        Order $helper,
        Config $helperConfig,
        EavValueManager $eavValueManager
    ) {
        $this->urlBuilder           = $urlBuilder;
        $this->rmaRepository        = $rmaRepository;
        $this->creditmemoRepository = $creditmemoRepository;
        $this->helper               = $helper;
        $this->helperConfig         = $helperConfig;
        $this->eavValueManager      = $eavValueManager;
    }

    /**
     * @param Edit $subject
     * @param String $result
     *
     * @return null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetBackUrl(Edit $subject, string $result)
    {
        if ($this->canDisplayButton($subject->getRma())) {
            $url     = $this->urlBuilder->getUrl(
                'sales/order_creditmemo/new',
                ['rma_id' => $subject->getRma()->getId()]
            );
            $onClick = "setLocation('{$url}')";

            $subject->addButton(
                'exchange_offline',
                [
                    'label'   => __('Exchange Offline'),
                    'onclick' => $onClick,
                    'class'   => 'reset',
                ],
                -1
            );

            $url     = $this->urlBuilder->getUrl(
                'sales/order_creditmemo/new',
                [
                    'rma_id' => $subject->getRma()->getId(),
                    'refund' => 1,
                ]
            );
            $onClick = "setLocation('{$url}')";

            $subject->addButton(
                'refund_exchange',
                [
                    'label'   => __('Refund / Exchange'),
                    'onclick' => $onClick,
                    'class'   => 'reset',
                ],
                -1
            );
        }

        return $subject->getUrl('*/*/');
    }

    /**
     * @param Rma $rma
     *
     * @return bool
     */
    protected function canDisplayButton($rma)
    {
        if (!$this->helperConfig->isActive()) {
            return false;
        }

        if ($rma->getOrder()->getTotalRefunded()) {
            return false;
        }

        foreach ($rma->getItems(true) as $item) {
            if ($this->eavValueManager->getValueByAttributeAndItem('resolution', $item) !== self::EXCHANGE_VALUE) {
                return false;
            }
        }

        return true;
    }
}
