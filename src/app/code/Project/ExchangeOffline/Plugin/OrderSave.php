<?php
namespace Project\ExchangeOffline\Plugin;

use Magento\Framework\App\Action\Context;
use Magento\Sales\Controller\Adminhtml\Order\Create\Save;
use Magento\Sales\Model\OrderRepository;

/**
 * Class OrderSave
 *
 * @package Project\ExchangeOffline\Plugin
 * @author Synolia <contact@synolia.com>
 */
class OrderSave
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Project\Sales\Model\Magento\Sales\OrderRepository
     */
    protected $orderRepository;

    /**
     * OrderSave constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     */
    public function __construct(
        Context $context,
        OrderRepository $orderRepository
    ) {
        $this->objectManager = $context->getObjectManager();
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param \Magento\Sales\Controller\Adminhtml\Order\Create\Save $subject
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeExecute(Save $subject)
    {
        if ($subject->getRequest()->getParam('rma_id') !== null) {
            $this->objectManager->get(\Magento\Sales\Model\AdminOrder\Create::class)->setRmaId(
                $subject->getRequest()->getParam('rma_id')
            );
        }

        if ($subject->getRequest()->getParam('creditmemo_id') !== null) {
            $this->objectManager->get(\Magento\Sales\Model\AdminOrder\Create::class)->setCreditmemoId(
                $subject->getRequest()->getParam('creditmemo_id')
            );
        }
    }
}
