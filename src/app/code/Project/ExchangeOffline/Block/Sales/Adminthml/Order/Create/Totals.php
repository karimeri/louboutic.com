<?php

namespace Project\ExchangeOffline\Block\Sales\Adminthml\Order\Create;

use Jp\Address\Helper\Data as JPData;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Helper\Data;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Sales\Model\Config;
use Magento\Sales\Model\Order\CreditmemoRepository;

/**
 * Class Totals
 *
 * @package Project\ExchangeOffline\Block\Sales\Adminthml\Order\Create
 * @author Synolia <contact@synolia.com>
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Order\Create\Totals
{
    /**
     * @var \Magento\Sales\Model\Order\CreditmemoRepository
     */
    protected $creditmemoRepository;

    /**
     * @var \Jp\Address\Helper\Data
     */
    protected $jpData;

    /**
     * Totals constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Sales\Model\AdminOrder\Create $orderCreate
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Sales\Helper\Data $salesData
     * @param \Magento\Sales\Model\Config $salesConfig
     * @param \Magento\Sales\Model\Order\CreditmemoRepository $creditmemoRepository
     * @param \Jp\Address\Helper\Data $jpData
     * @param array $data
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        Data $salesData,
        Config $salesConfig,
        CreditmemoRepository $creditmemoRepository,
        JPData $jpData,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $sessionQuote,
            $orderCreate,
            $priceCurrency,
            $salesData,
            $salesConfig,
            $data
        );

        $this->creditmemoRepository = $creditmemoRepository;
        $this->jpData = $jpData;
    }

    /**
     * @return float|null
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOriginalOrderTotal()
    {
        $quote = $this->getQuote();

        if ($quote->getCreditmemoId()) {
            return $this->creditmemoRepository->get($quote->getCreditmemoId())->getGrandTotal();
        }

        return 0.00;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOnClick()
    {
        if ($this->jpData->isJapan()) {
            $onclick = 'order.checkAddressBeforeSubmit()';
        } else {
            $onclick = 'order.submit()';
        }

        if ($this->getOriginalOrderTotal()) {
            $onclick = sprintf(
                'if (order.checkTotal(%s)) {%s}',
                $this->getOriginalOrderTotal(),
                $onclick
            );
        }

        return $onclick;
    }
}
