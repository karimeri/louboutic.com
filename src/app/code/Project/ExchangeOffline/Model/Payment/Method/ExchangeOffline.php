<?php

namespace Project\ExchangeOffline\Model\Payment\Method;

use Magento\Payment\Model\Method\Free;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Payment
 *
 * @package Project\ExchangeOffline\Model\Payment\Method
 * @author  Synolia <contact@synolia.com>
 */
class ExchangeOffline extends Free
{
    /**
     * @var string
     */
    protected $_code = 'exchange_offline';

    /**
     * @var bool
     */
    protected $_canUseCheckout = false;

    /**
     * Check whether payment method can be used
     *
     * @param CartInterface|null $quote
     * @return bool
     *
     */
    public function isAvailable(CartInterface $quote = null)
    {
        try {
            if ($quote->getCreditmemoId()) {
                return true;
            }
        } catch (LocalizedException $e) {
            // do nothing
        }

        return false;
    }
}
