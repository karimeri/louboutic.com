<?php

namespace Project\ExchangeOffline\Model\Order;

use Magento\Framework\App\RequestInterface;
use Magento\Rma\Model\Rma;
use Magento\Rma\Model\Rma\Source\Status;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\OrderFactory as OrderModelFactory;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\Tax\Model\Config;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Convert\OrderFactory;
use Magento\Sales\Model\Order\ItemRepository;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\CreditmemoFactory as BaseCreditmemoFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Project\Core\Model\Environment;
use Project\Sales\Helper\Config as ConfigHelper;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Erp\Helper\Config\Y2\Sales;
use Project\Erp\Helper\ExportSalesTickets;
use Magento\Sales\Model\ResourceModel\Order\Tax\Item as TaxItem;

/**
 * Class CreditmemoFactory
 *
 * @package Project\ExchangeOffline\Model\Order
 * @author  Synolia <contact@synolia.com>
 */
class CreditmemoFactory extends BaseCreditmemoFactory
{
    const EXCHANGE_OFFLINE = 'exchange_offline';

    /**
     * @var ItemRepository
     */
    protected $itemRepository;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Project\Erp\Helper\Config\Y2\Sales
     */
    protected $helperY2Sales;

    /**
     * @var \Project\Erp\Helper\ExportSalesTickets
     */
    protected $helperSalesTickets;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order
     */
    protected $orderResource;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Tax\Item
     */
    protected $taxItem;

    /**
     * Factory constructor
     *
     * @param OrderFactory $convertOrderFactory
     * @param Config $taxConfig
     * @param ItemRepository $itemRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Project\Erp\Helper\Config\Y2\Sales $helperY2Sales
     * @param \Project\Erp\Helper\ExportSalesTickets $helperSalesTickets
     * @param \Magento\Sales\Model\ResourceModel\Order $orderResource
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Sales\Model\ResourceModel\Order\Tax\Item $taxItem
     * @param Json $serializer
     */
    public function __construct(
        OrderFactory $convertOrderFactory,
        Config $taxConfig,
        ItemRepository $itemRepository,
        PriceCurrencyInterface $priceCurrency,
        EnvironmentManager $environmentManager,
        Sales $helperY2Sales,
        ExportSalesTickets $helperSalesTickets,
        Order $orderResource,
        OrderModelFactory $orderFactory,
        RequestInterface $request,
        TaxItem $taxItem,
        Json $serializer = null
    ) {
        $this->itemRepository = $itemRepository;
        $this->serializer     = $serializer;
        $this->priceCurrency  = $priceCurrency;
        $this->environmentManager  = $environmentManager;
        $this->helperY2Sales  = $helperY2Sales;
        $this->helperSalesTickets  = $helperSalesTickets;
        $this->orderResource = $orderResource;
        $this->orderFactory = $orderFactory;
        $this->request = $request;
        $this->taxItem = $taxItem;

        parent::__construct(
            $convertOrderFactory,
            $taxConfig,
            $serializer
        );
    }

    /**
     * Prepare order creditmemo based on rma and requested params
     *
     * @param Rma   $rma
     * @param array $data
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     *
     * @return Creditmemo
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createByRma(Rma $rma, $data, Invoice $invoice)
    {
        if (empty($data['qtys'])) {
            $creditmemo = $this->generateByRma($rma);
        } else {
            $creditmemo = $this->createByOrder($rma->getOrder(), $data);
        }

        $creditmemo->setRmaId($rma->getId());
        $creditmemo->setType(self::EXCHANGE_OFFLINE);
        $creditmemo->setInvoice($invoice);

        return $creditmemo;
    }

    /**
     * synolia : set default quantity to 0
     *
     * @param \Magento\Sales\Model\Order $order
     * @param array $data
     * @return Creditmemo
     */
    public function createByOrder(\Magento\Sales\Model\Order $order, array $data = [])
    {
        $totalQty = 0;
        $creditmemo = $this->convertor->toCreditmemo($order);
        $qtys = isset($data['qtys']) ? $data['qtys'] : [];

        foreach ($order->getAllItems() as $orderItem) {
            if (!$this->canRefundItem($orderItem, $qtys)) {
                continue;
            }

            $item = $this->convertor->itemToCreditmemoItem($orderItem);
            if ($orderItem->isDummy()) {
                if (isset($data['qtys'][$orderItem->getParentItemId()])) {
                    $parentQty = $data['qtys'][$orderItem->getParentItemId()];
                } else {
                    $parentQty = 0;
                }
                $qty = $this->calculateProductOptions($orderItem, $parentQty);
                $orderItem->setLockedDoShip(true);
            } else {
                if (isset($qtys[$orderItem->getId()])) {
                    $qty = (double)$qtys[$orderItem->getId()];
                } elseif (!count($qtys)) {
                    $qty = 0;
                } else {
                    continue;
                }
            }
            $totalQty += $qty;
            $item->setQty($qty);
            $creditmemo->addItem($item);
        }
        $creditmemo->setTotalQty($totalQty);

        $this->initData($creditmemo, $data);

        $creditmemo->collectTotals();
        return $creditmemo;
    }

    /**
     * synolia : set default quantity to 0
     *
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @param array $data
     * @return Creditmemo
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createByInvoice(\Magento\Sales\Model\Order\Invoice $invoice, array $data = [])
    {
        $order = $invoice->getOrder();
        $totalQty = 0;
        $qtys = isset($data['qtys']) ? $data['qtys'] : [];
        $creditmemo = $this->convertor->toCreditmemo($order);
        $creditmemo->setInvoice($invoice);

        $invoiceQtysRefunded = [];
        foreach ($invoice->getOrder()->getCreditmemosCollection() as $createdCreditmemo) {
            if ($createdCreditmemo->getState() != Creditmemo::STATE_CANCELED &&
                $createdCreditmemo->getInvoiceId() == $invoice->getId()
            ) {
                foreach ($createdCreditmemo->getAllItems() as $createdCreditmemoItem) {
                    $orderItemId = $createdCreditmemoItem->getOrderItem()->getId();
                    if (isset($invoiceQtysRefunded[$orderItemId])) {
                        $invoiceQtysRefunded[$orderItemId] += $createdCreditmemoItem->getQty();
                    } else {
                        $invoiceQtysRefunded[$orderItemId] = $createdCreditmemoItem->getQty();
                    }
                }
            }
        }

        $invoiceQtysRefundLimits = [];
        foreach ($invoice->getAllItems() as $invoiceItem) {
            $invoiceQtyCanBeRefunded = $invoiceItem->getQty();
            $orderItemId = $invoiceItem->getOrderItem()->getId();
            if (isset($invoiceQtysRefunded[$orderItemId])) {
                $invoiceQtyCanBeRefunded = $invoiceQtyCanBeRefunded - $invoiceQtysRefunded[$orderItemId];
            }
            $invoiceQtysRefundLimits[$orderItemId] = $invoiceQtyCanBeRefunded;
        }

        foreach ($invoice->getAllItems() as $invoiceItem) {
            $orderItem = $invoiceItem->getOrderItem();

            if (!$this->canRefundItem($orderItem, $qtys, $invoiceQtysRefundLimits)) {
                continue;
            }

            $item = $this->convertor->itemToCreditmemoItem($orderItem);
            if ($orderItem->isDummy()) {
                if (isset($data['qtys'][$orderItem->getParentItemId()])) {
                    $parentQty = $data['qtys'][$orderItem->getParentItemId()];
                } else {
                    $parentQty = 0;
                }
                $qty = $this->calculateProductOptions($orderItem, $parentQty);
            } else {
                if (isset($qtys[$orderItem->getId()])) {
                    $qty = (double)$qtys[$orderItem->getId()];
                } elseif (!count($qtys)) {
                    $qty = 0;
                } else {
                    continue;
                }
                if (isset($invoiceQtysRefundLimits[$orderItem->getId()])) {
                    $qty = min($qty, $invoiceQtysRefundLimits[$orderItem->getId()]);
                }
            }
            $qty = min($qty, $invoiceItem->getQty());
            $totalQty += $qty;
            $item->setQty($qty);
            $creditmemo->addItem($item);
        }
        $creditmemo->setTotalQty($totalQty);

        $this->initData($creditmemo, $data);

        $creditmemo->collectTotals();
        return $creditmemo;
    }

    /**
     * Generate order creditmemo based on rma
     *
     * @param Rma $rma
     *
     * @return Creditmemo
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function generateByRma(Rma $rma)
    {
        $order      = $rma->getOrder();
        $creditmemo = $this->convertor->toCreditmemo($order);

        $rmaQtysRefundLimits = [0 => 0];
        foreach ($rma->getItems(true) as $rmaItem) {
            if (!in_array($rmaItem->getStatus(), [Status::STATE_DENIED, Status::STATE_REJECTED])) {
                $rmaQtysRefundLimits[0] += $rmaItem->getQty();
            }
        }

        // add items to creditmemo
        $totalQty = $this->addItemsToCreditmemo($rma, $creditmemo, $rmaQtysRefundLimits);

        $creditmemo->setTotalQty($totalQty);
        $this->initData($creditmemo, []);
        $creditmemo->collectTotals();

        return $creditmemo;
    }

    /**
     * identical to parent
     *
     * @param OrderItemInterface $orderItem
     * @param int                $parentQty
     *
     * @return int
     */
    private function calculateProductOptions(OrderItemInterface $orderItem, $parentQty)
    {
        $qty            = $parentQty;
        $productOptions = $orderItem->getProductOptions();

        if (isset($productOptions['bundle_selection_attributes'])) {
            $bundleSelectionAttributes = $this->serializer->unserialize(
                $productOptions['bundle_selection_attributes']
            );

            if ($bundleSelectionAttributes) {
                $qty = $bundleSelectionAttributes['qty'] * $parentQty;
            }
        }

        return $qty;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function getRefundedItemQuantity($order)
    {
        $rmaQtysRefunded = [];

        foreach ($order->getCreditmemosCollection() as $createdCreditmemo) {
            if ($createdCreditmemo->getState() !== Creditmemo::STATE_CANCELED) {
                foreach ($createdCreditmemo->getAllItems() as $createdCreditmemoItem) {
                    $orderItemId = $createdCreditmemoItem->getOrderItem()->getId();

                    if (isset($rmaQtysRefunded[$orderItemId])) {
                        $rmaQtysRefunded[$orderItemId] += $createdCreditmemoItem->getQty();
                    } else {
                        $rmaQtysRefunded[$orderItemId] = $createdCreditmemoItem->getQty();
                    }
                }
            }
        }

        return $rmaQtysRefunded;
    }

    /**
     * @param Rma   $rma
     * @param array $rmaQtysRefunded
     *
     * @return array
     */
    protected function getRefundedItemQuantityLimits($rma, $rmaQtysRefunded)
    {
        $rmaQtysRefundLimits = [];

        foreach ($rma->getItems(true) as $rmaItem) {
            $rmaQtyCanBeRefunded = $rmaItem->getQty();
            $orderItemId         = $rmaItem->getOrderItemId();

            if (isset($rmaQtysRefunded[$orderItemId])) {
                $rmaQtyCanBeRefunded -= $rmaQtysRefunded[$orderItemId];
            }

            $rmaQtysRefundLimits[$orderItemId] = $rmaQtyCanBeRefunded;
        }

        return $rmaQtysRefundLimits;
    }

    /**
     * @param Rma        $rma
     * @param Creditmemo $creditmemo
     * @param array      $rmaQtysRefundLimits
     *
     * @return int
     */
    protected function addItemsToCreditmemo($rma, $creditmemo, $rmaQtysRefundLimits)
    {
        $qtys     = [];
        $totalQty = 0;

        foreach ($rma->getItems(true) as $rmaItem) {
            if (in_array($rmaItem->getStatus(), [Status::STATE_DENIED, Status::STATE_REJECTED])) {
                continue;
            }

            $orderItem = $this->itemRepository->get($rmaItem->getOrderItemId());

            if (!$this->canRefundItem($orderItem, $qtys, $rmaQtysRefundLimits)) {
                continue;
            }

            $item = $this->convertor->itemToCreditmemoItem($orderItem);
            if ($orderItem->isDummy()) {
                $parentQty = $orderItem->getParentItem() ? $orderItem->getParentItem()->getQtyToRefund() : 1;

                $qty = $this->calculateProductOptions($orderItem, $parentQty);
            } else {
                if (isset($qtys[$orderItem->getId()])) {
                    $qty = (double)$qtys[$orderItem->getId()];
                } elseif (!count($qtys)) { // phpcs:ignore
                    $qty = $orderItem->getQtyToRefund();
                } else {
                    continue;
                }

                if (isset($rmaQtysRefundLimits[$orderItem->getId()])) {
                    $qty = min($qty, $rmaQtysRefundLimits[$orderItem->getId()]);
                }
            }

            $qty      = min($qty, $rmaItem->getQty());
            $totalQty += $qty;

            $item->setQty($qty);
            $creditmemo->addItem($item);
        }

        return $totalQty;
    }

    /**
     * Initialize creditmemo state based on requested parameters
     * @param Creditmemo $creditmemo
     * @param array $data
     * @return void
     */
    protected function initData($creditmemo, $data)
    {
        $creditmemo->setBaseShippingAmount(0)
            ->setShippingAmount(0)
            ->setBaseShippingDiscountTaxCompensationAmnt(0)
            ->setShippingDiscountTaxCompensationAmount(0)
            ->setBaseShippingInclTax(0)
            ->setShippingInclTax(0)
            ->setShippingTaxAmount(0)
            ->setBaseShippingTaxAmount(0);

        if (isset($data['shipping_amount'])) {
            $creditmemo->setBaseShippingAmount((double)$data['shipping_amount']);
        }
        if (isset($data['adjustment_positive'])) {
            $creditmemo->setAdjustmentPositive($data['adjustment_positive']);
        }
        if (isset($data['adjustment_negative'])) {
            $creditmemo->setAdjustmentNegative($data['adjustment_negative']);
        }

        $order = $this->orderFactory->create();
        $this->orderResource->load($order, $creditmemo->getOrderId());

        $ttc = $this->environmentManager->getEnvironment() !== Environment::US;
        $creditmemo->setTtc($ttc);

        $taxRate = $this->getTaxRate($order);
        $creditmemo->setAdjustmentTaxRate($taxRate);

        if (isset($data[ConfigHelper::FIELD_AJUSTMENT_SHIPPING_FEES])) {
            $this->setDataCreditmemo(
                $creditmemo,
                ConfigHelper::FIELD_AJUSTMENT_SHIPPING_FEES,
                $data
            );
        }
        if (isset($data[ConfigHelper::FIELD_AJUSTMENT_REPAIRING_FEES])) {
            $this->setDataCreditmemo(
                $creditmemo,
                ConfigHelper::FIELD_AJUSTMENT_REPAIRING_FEES,
                $data
            );
        }
        if (isset($data[ConfigHelper::FIELD_AJUSTMENT_CREDIT_NOTE])) {
            $this->setDataCreditmemo(
                $creditmemo,
                ConfigHelper::FIELD_AJUSTMENT_CREDIT_NOTE,
                $data
            );
        }
        if (isset($data[ConfigHelper::FIELD_AJUSTMENT_RETURN_CHARGE])) {
            $this->setDataCreditmemo(
                $creditmemo,
                ConfigHelper::FIELD_AJUSTMENT_RETURN_CHARGE,
                $data
            );
        }
    }

    /**
     * Calc Base Values
     * @param Creditmemo $creditmemo
     * @param string $field
     * @param array $data
     */
    protected function setDataCreditmemo($creditmemo, $field, $data)
    {
        $amount = $data[$field];
        if ($creditmemo->getBaseCurrencyCode() != $creditmemo->getOrderCurrencyCode()) {
            $creditmemo->setData('base_' . $field, $this->priceCurrency->convertAndRound($amount, $creditmemo->getStoreId(), $creditmemo->getBaseCurrencyCode()));
            $creditmemo->setData($field, $this->priceCurrency->round($amount));
        } else {
            $creditmemo->setData('base_' . $field, $this->priceCurrency->round($amount));
            $creditmemo->setData($field, $this->priceCurrency->round($amount)
            );
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return int
     */
    protected function getTaxRate($order)
    {
        $taxRate = 0;
        $found = false;

        foreach ($this->taxItem->getTaxItemsByOrderId($order->getId()) as $tax) {
            if ($tax['taxable_item_type'] === 'shipping') {
                $taxRate += $tax['tax_percent'];
                $found = true;
            }
        }

        if (!$found) {
            $sku = null;
            if ($this->environmentManager->getEnvironment() === Environment::US) {
                // call avalara to get tax amount
                $sku = $this->helperY2Sales->getY2VirtualReference(OrderInterface::SHIPPING_AMOUNT);
            }

            $taxRate = $this->helperSalesTickets->getTaxRate($order, $sku);
        }

        return $taxRate;
    }
}
