<?php
/**
 * Module Find It in Store.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Project_FindInStore',
    __DIR__
);
