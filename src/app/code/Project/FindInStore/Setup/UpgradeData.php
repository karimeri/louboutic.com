<?php

namespace Project\FindInStore\Setup;

use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Project\FindInStore\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * UpgradeData constructor.
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->updateFindInStoreAttribute();
        }

        $installer->endSetup();
    }

    private function updateFindInStoreAttribute()
    {
        $this->eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'allow_find_in_store',
            EavAttributeInterface::IS_VISIBLE_ON_FRONT,
            false
        );
    }
}
