<?php

namespace Project\Owebia\Model\AdvancedShipping;

use Owebia\AdvancedShipping\Model\Wrapper\RateResult as RateResultWrapper;

/**
 * Class Carrier
 *
 * @package Project\Owebia\Model\AdvancedShipping
 * @author Synolia <contact@synolia.com>
 */
class Carrier extends \Owebia\AdvancedShipping\Model\Carrier
{
    /**
     * @param string $methodId
     * @param \stdClass $method
     *
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Method
     */
    protected function createMethod($methodId, RateResultWrapper\Method $method)
    {
        if (\strpos($methodId, 'dhl') !== false) {
            $methodId = 'dhl_dhl';
        } elseif (\strpos($methodId, 'yamato') !== false) {
            $methodId = 'yamato';
        }

        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $rate */
        $rate = $this->rateMethodFactory->create();
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod($methodId);
        $title = $method->title;
        $rate->setMethodTitle($title ? $title : 'N/A');
        $description = $method->description ? $method->description : null;
        $rate->setMethodDescription($description);
        $rate->setCost($method->price);
        $rate->setPrice($method->price);

        return $rate;
    }
}
