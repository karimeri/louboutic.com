<?php

namespace Eu\Core\Setup;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManager;
use Project\Core\Setup\CoreInstallData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreRepository;

/**
 * Class UpgradeData
 *
 * @package   Eu\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class UpgradeData extends CoreInstallData implements UpgradeDataInterface
{
    const VERSIONS = [
        '1.0.1' => '101',
        '1.0.2' => '102',
        '1.0.3' => '103',
        '1.0.5' => '105',
        '1.0.6' => '106',
        '1.0.7' => '107',
        '1.0.8' => '108',
        '1.0.9' => '109',
        '1.0.10' => '1010',
        '1.0.11' => '1011',
        '1.0.12' => '1012',
        '1.0.13' => '1013',
        '1.0.14' => '1014',
        '1.0.15' => '1015',
        '1.0.16' => '1016',
        '1.0.17' => '1017',
        '1.0.18' => '1018',
        '1.0.19' => '1019',
        '1.0.20' => '1020',
        '1.0.21' => '1021',
        '1.0.22' => '1022',
        '1.0.23' => '1023'
    ];

    const WEBSITE_CODE_FR = 'fr';
    const WEBSITE_CODE_UK = 'uk';
    const WEBSITE_CODE_IT = 'it';
    const WEBSITE_CODE_DE = 'de';
    const WEBSITE_CODE_ES = 'es';
    const WEBSITE_CODE_CH = 'ch';
    const WEBSITE_CODE_NL = 'nl';
    const WEBSITE_CODE_LU = 'lu';
    const WEBSITE_CODE_BE = 'be';
    const WEBSITE_CODE_AT = 'at';
    const WEBSITE_CODE_IE = 'ie';
    const WEBSITE_CODE_PT = 'pt';
    const WEBSITE_CODE_MC = 'mc';
    const WEBSITE_CODE_GR = 'gr';

    const STORE_CODE_FR_FR = 'fr_fr';
    const STORE_CODE_UK_EN = 'uk_en';
    const STORE_CODE_IT_EN = 'it_en';
    const STORE_CODE_DE_EN = 'de_en';
    const STORE_CODE_ES_EN = 'es_en';
    const STORE_CODE_CH_EN = 'ch_en';
    const STORE_CODE_CH_FR = 'ch_fr';
    const STORE_CODE_NL_EN = 'nl_en';
    const STORE_CODE_LU_FR = 'lu_fr';
    const STORE_CODE_LU_EN = 'lu_en';
    const STORE_CODE_BE_FR = 'be_fr';
    const STORE_CODE_BE_EN = 'be_en';
    const STORE_CODE_AT_EN = 'at_en';
    const STORE_CODE_IE_EN = 'ie_en';
    const STORE_CODE_PT_EN = 'pt_en';
    const STORE_CODE_MC_FR = 'mc_fr';
    const STORE_CODE_MC_EN = 'mc_en';
    const STORE_CODE_GR_EN = 'gr_en';

    const MODULE_NAME = 'Eu_Core';

    /**
     * ConsoleOutput
     */
    protected $output;
    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;
    /**
     * @var \Synolia\Standard\Setup\ConfigSetup
     */
    protected $configSetup;
    /**
     * @var \Synolia\Standard\Setup\ConfigSetupFactory
     */
    protected $configSetupFactory;
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    /**
     * UpgradeData constructor.
     * @param DirectoryList      $directoryList
     * @param Csv                $csvProcessor
     * @param Reader             $moduleReader
     * @param StoreManager       $storeManager
     * @param WriterInterface    $writerInterface
     * @param ConsoleOutput      $output
     * @param ConfigSetupFactory $configSetupFactory
     * @param StoreRepository    $storeRepository
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $writerInterface,
        ConsoleOutput $output,
        ConfigSetupFactory $configSetupFactory,
        StoreRepository $storeRepository
    ) {
        parent::__construct($directoryList, $csvProcessor, $moduleReader, $storeManager, $writerInterface);

        $this->output             = $output;
        $this->configSetupFactory = $configSetupFactory;
        $this->objectManager      = ObjectManager::getInstance();
        $this->storeManager       = $storeManager;
        $this->storeRepository    = $storeRepository;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();
        // @codingStandardsIgnoreStart
        if (!getenv('PROJECT_ENV') || getenv('PROJECT_ENV') !== Environment::EU) {
            // @codingStandardsIgnoreEnd
            $this->output->writeln('Upgrade Core EU ignore');
            $setup->endSetup();

            return;
        }

        $this->output->writeln(""); // new line in console

        foreach ($this::VERSIONS as $version => $fileData) {
            if (version_compare($context->getVersion(), $version, '<')) {
                $this->output->writeln("Processing EU Core setup : $version");

                $currentSetup = $this->getObjectManager()->create('Eu\Core\Setup\Upgrade\Upgrade' . $fileData);
                $currentSetup->run($this);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * @return ConfigSetup
     */
    public function getConfigSetup()
    {
        return $this->configSetup;
    }

    /**
     * @return StoreManager
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * @param string $storeCode
     * @return int
     */
    public function getStoreId($storeCode)
    {
        return $this->getStoreManager()->getStore($storeCode)->getId();
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocale()
    {
        return [
            'fr_FR' => [
                $this->storeRepository->get(self::STORE_CODE_FR_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_CH_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_FR)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_FR)->getId()
            ],
            'en_GB' => [
                $this->storeRepository->get(self::STORE_CODE_CH_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_UK_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_DE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_ES_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_NL_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_LU_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_BE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_AT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_IE_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_PT_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_MC_EN)->getId(),
                $this->storeRepository->get(self::STORE_CODE_GR_EN)->getId()
            ]
        ];
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoresIndexedByLocaleAndDevise()
    {
        return [
            'fr_FR' => [
                'euro' => [
                    $this->storeRepository->get(self::STORE_CODE_FR_FR)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_LU_FR)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_BE_FR)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_MC_FR)->getId()
                ],
                'chf' => [
                    $this->storeRepository->get(self::STORE_CODE_CH_FR)->getId(),
                ]
            ],
            'en_GB' => [
                'euro' => [
                    $this->storeRepository->get(self::STORE_CODE_IT_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_DE_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_ES_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_NL_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_LU_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_BE_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_AT_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_IE_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_PT_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_MC_EN)->getId(),
                    $this->storeRepository->get(self::STORE_CODE_GR_EN)->getId()
                ],
                'chf' => [
                    $this->storeRepository->get(self::STORE_CODE_CH_EN)->getId(),
                ],
                'pound' => [
                    $this->storeRepository->get(self::STORE_CODE_UK_EN)->getId(),
                ],
            ]
        ];
    }
}
