<?php

namespace Eu\Core\Setup\RecurringData;

use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\WebsiteRepository;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class Upgrade31DhlToUps
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade31DhlToUps implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var \Magento\Store\Model\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    const MAPPING = [
        // standard
        'ups_11' => [
            'it' => 0,
            'uk' => 0,
            'ch' => 0,
            'fr' => 0,
            'mc' => 0,
            'de' => 0,
            'nl' => 0,
            'be' => 0,
            'es' => 0,
            'at' => 0,
            'ie' => 0,
            'pt' => 0,
            'gr' => 0,
            'lu' => 0,
        ],
        // express Saver
        'ups_65' => [
            'it' => 15,
            'uk' => 20,
            'ch' => 25,
            'fr' => 20,
            'mc' => 20,
            'de' => 20,
            'nl' => 20,
            'be' => 20,
            'es' => 20,
            'at' => 20,
            'ie' => 20,
            'pt' => 20,
            'gr' => 20,
            'lu' => 20,
        ],
        // express within 10:30
        'ups_07' => [
            'it' => 10,
            'uk' => 15,
            'ch' => 20,
            'fr' => 15,
            'mc' => 15,
            'de' => 15,
            'nl' => 15,
            'be' => 15,
            'es' => 15,
            'at' => 15,
            'ie' => 15,
            'pt' => 15,
            'gr' => 15,
            'lu' => 15,
        ],
        // saturday
        'ups_07_s' => [
            'it' => 20,
            'uk' => 30,
            'ch' => 40,
            'fr' => 35,
            'mc' => 35,
            'de' => 35,
            'nl' => 35,
            'be' => 35,
            'es' => 35,
            'at' => 35,
            'ie' => 35,
            'pt' => 35,
            'gr' => 35,
            'lu' => 35,
        ],
    ];

    const COUNTRIES = [
        'it',
        'uk',
        'ch',
        'fr',
        'mc',
        'de',
        'nl',
        'be',
        'es',
        'at',
        'ie',
        'pt',
        'gr',
        'lu',
    ];

    const TIMEZONE = [
        'it' => 'Europe/Rome',
        'uk' => 'Europe/London',
        'ch' => 'Europe/Zurich',
        'fr' => 'Europe/Paris',
        'mc' => 'Europe/Monaco',
        'de' => 'Europe/Berlin',
        'nl' => 'Europe/Amsterdam',
        'be' => 'Europe/Brussels',
        'es' => 'Europe/Madrid',
        'at' => 'Europe/Vienna',
        'ie' => 'Europe/Dublin',
        'pt' => 'Europe/Lisbon',
        'gr' => 'Europe/Athens',
        'lu' => 'Europe/Luxembourg',
    ];

    /**
     * Upgrade31DhlToUps constructor.
     *
     * @param ConfigSetup $configSetup
     * @param \Magento\Store\Model\WebsiteRepository $websiteRepository
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        WebsiteRepository $websiteRepository,
        EnvironmentManager $environmentManager
    ) {
        $this->configSetup = $configSetup;
        $this->websiteRepository = $websiteRepository;
        $this->environmentManager = $environmentManager;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function run(Upgrade $upgradeObject)
    {
        if ($this->environmentManager->getEnvironment() === Environment::EU) {
            foreach (self::COUNTRIES as $websiteCode) {
                $this->configSetup->saveConfig(
                    'carriers/owsh1/config',
                    $this->getContent($websiteCode),
                    ScopeInterface::SCOPE_WEBSITES,
                    $this->websiteRepository->get($websiteCode)->getId()
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Update owebia from dhl to ups';
    }

    /**
     * @param string $websiteCode
     *
     * @return string
     */
    protected function getContent($websiteCode)
    {
        return sprintf(
            'addMethod(\'ups_11\', [
    \'title\'  => "UPS Standard",
    \'price\'  => %s,
    \'enabled\' => true
]);

addMethod(\'ups_07\', [
    \'title\'  => "UPS Express within 10:30",
    \'price\'  => %s,
    \'enabled\' => true
]);

addMethod(\'ups_65\', [
    \'title\'  => "UPS Express Saver",
    \'price\'  => %s,
    \'enabled\' => true
]);

addMethod(\'ups_07_s\', [
    \'title\'  => "UPS Saturday",
    \'price\'  => %s,
    \'enabled\' => count(array_filter($request->all_items, function ($item) {
        return substr($item->product->getAttributeText(\'dangerous_goods\'), 0, 1) == 1;
    })) == 0
    &&
    array_reduce([1], function ($item) {
        $timestamp         = time();
        $timestampInversed = strtotime(date(\'Y-m-d H:i:s\') . \' \' . \'%s\');

        $offset = $timestampInversed - $timestamp;
        $offset = $offset <= 0 ? $offset : -$offset;

        $currentDate = date(\'Y-m-d H:i:s\', $timestamp + $offset);
        $thursday    = date(\'Y-m-d\', strtotime(\'thursday this week\')) . \' 15:00:00\';
        $friday      = date(\'Y-m-d\', strtotime(\'friday this week\')) . \' 14:00:00\';

        if ($currentDate >= $thursday && $currentDate <= $friday) {
            return true;
        }

        return false;
    })
]);',
            self::MAPPING['ups_11'][$websiteCode],
            self::MAPPING['ups_07'][$websiteCode],
            self::MAPPING['ups_65'][$websiteCode],
            self::MAPPING['ups_07_s'][$websiteCode],
            self::TIMEZONE[$websiteCode]
        );
    }
}
