<?php

namespace Eu\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Eu\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Cms\Model\PageRepository;

/**
 * Class Upgrade19
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade19 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Eu\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * @var \Magento\Cms\Model\PageRepository
     */
    protected $pageRepository;

    /**
     * Upgrade19 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Eu\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     * @param \Magento\Cms\Model\PageRepository $pageRepository
     */
    public function __construct(
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput,
        PageRepository $pageRepository
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsPageIdentifiers = [
            'bikini-questa-sera' => 'Bikini Questa Sera',
            'tornade-blonde'     => 'Tornade Blonde',
            'trouble-in-heaven'  => 'Trouble In Heaven'
        ];

        foreach ($cmsPageIdentifiers as $identifier => $title) {
            $this->removeOldPages($identifier);
            $this->addCmsPages($identifier, $title);
        }
    }

    /**
     * @param $identifier
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function removeOldPages($identifier)
    {
        foreach ($this->upgradeData->getStoresIndexedByLocale() as $locale => $stores) {
            $page = $this->cmsSetup->getPage($identifier . '.html', $stores);

            if ($page->getId()) {
                $this->consoleOutput->writeln('Removing page '.$identifier.' on '
                    .$locale.' stores ('.implode(',', $stores).') ');

                // @codingStandardsIgnoreLine
                $this->pageRepository->delete($page);
            }
        }
    }

    /**
     * @param $identifier
     * @param $title
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addCmsPages($identifier, $title)
    {
        foreach ($this->upgradeData->getStoresIndexedByLocaleAndDevise() as $locale => $devises) {
            foreach ($devises as $devise => $stores) {
                $cmsPageContent = $this->cmsSetup->getCmsPageContent(
                    $identifier,
                    'Eu_Core',
                    '',
                    '',
                    'misc/cms/pages/'.$locale.'/'.$devise
                );

                $cmsPage = [
                    'title'             => 'Christian Louboutin - ' . $title,
                    'page_layout'       => 'fullscreen',
                    'identifier'        => $identifier . '.html',
                    'content_heading'   => '',
                    'content'           => $cmsPageContent,
                    'is_active'         => 1,
                    'stores'            => $stores
                ];

                $this->consoleOutput->writeln('Saving page '.$identifier.' on '
                    .$locale.' stores ('.implode(',', $stores).') ');

                $this->cmsSetup->savePage($cmsPage);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Remove and Add CMS page bikini-questa-sera, tornade-blonde and trouble-in-heaven for EU';
    }
}
