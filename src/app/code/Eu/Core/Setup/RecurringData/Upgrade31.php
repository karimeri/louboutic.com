<?php

namespace Eu\Core\Setup\RecurringData;

use Magento\CheckoutAgreements\Model\ResourceModel\Agreement\Collection;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository;
use Synolia\Standard\Setup\CmsSetup;
use Eu\Core\Setup\UpgradeData;

/**
 * Class Upgrade31
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade31 implements UpgradeDataSetupInterface
{
    /**
     * @var \Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository
     */
    protected $agreementsRepository;

    /**
     * @var \Magento\CheckoutAgreements\Model\ResourceModel\Agreement\Collection
     */
    protected $agreementsCollection;

    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Eu\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * Upgrade31 constructor.
     * @param \Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository $agreementsRepository
     * @param Collection $agreementsCollection
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Eu\Core\Setup\UpgradeData $upgradeData
     */
    public function __construct(
        CheckoutAgreementsRepository $agreementsRepository,
        Collection $agreementsCollection,
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData
    ) {
        $this->agreementsRepository = $agreementsRepository;
        $this->agreementsCollection = $agreementsCollection;
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $storeUrl = "{{store url=''}}";
        $messages = [];

        foreach ($this->agreementsCollection as $agreement) {
            foreach ($this->upgradeData->getStoresIndexedByLocale() as $local => $stores) {
                foreach ($stores as $storeId) {
                    $agreementObject = $this->agreementsRepository->get($agreement->getAgreementId(), $storeId);

                    $checkboxText = $agreementObject->getCheckboxText();
                    if (strpos($checkboxText, $storeUrl) === false) {
                        continue;
                    }

                    $agreementObject->setCheckboxText(str_replace($storeUrl, '../', $checkboxText));

                    try {
                        //@codingStandardsIgnoreLine
                        $this->agreementsRepository->save($agreementObject, $storeId);
                    } catch (\Exception $e) {
                        $messages[] = $e->getMessage();
                        continue;
                    }
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Remove magento variables in Terms and conditions checkbox on checkout for EU';
    }
}
