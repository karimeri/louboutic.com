<?php
namespace Eu\Core\Setup\RecurringData;

use Magento\Eav\Model\Config;
use Magento\Rma\Model\Item;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Eav\Setup\EavSetup;

/**
 * Class Upgrade7
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade7 implements UpgradeDataSetupInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    protected $eavSetup;
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavAttribute;

    /**
     * Upgrade7 constructor.
     * @param \Magento\Eav\Setup\EavSetup $eavSetup
     * @param \Magento\Eav\Model\Config   $eavAttribute
     */
    public function __construct(
        EavSetup $eavSetup,
        Config $eavAttribute
    ) {
        $this->eavSetup = $eavSetup;
        $this->eavAttribute = $eavAttribute;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            Item::REASON => [
                'values' => [
                    'Too Big',
                    'Too Small',
                    'Style',
                    'Fit Issue',
                    'Not As Shown',
                    'Wrong Item Shipped',
                    'Late Delivery',
                    'Gift',
                    'Changed Mind',
                    'Quality Issue',
                    'Delivery Issue',
                    'None',
                    'Other',
                    'Too Large',
                ],
                'attribute_id' => $this->eavSetup->getAttributeId(Item::ENTITY, Item::REASON),
            ],
            Item::CONDITION => [
                'values' => [
                    'Box Unopened',
                    'Box Opened',
                    'Shoes Damaged',
                    'To be reshipped',
                ],
                'attribute_id' => $this->eavSetup->getAttributeId(Item::ENTITY, Item::CONDITION),
            ],
        ];

        foreach ($attributes as $attributeCode => $attributeValues) {
            $this->deleteAttributesOptions($attributeCode);
            $this->eavSetup->addAttributeOption($attributeValues);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Set options for reason and condition RMA items on EU';
    }

    /**
     * @param string $attributeCode
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function deleteAttributesOptions($attributeCode)
    {
        $attribute = $this->eavAttribute->getAttribute(Item::ENTITY, $attributeCode);

        $options = $attribute->getSource()->getAllOptions();

        foreach ($options as $option) {
            $options['value'][$option['value']] = true;
            $options['delete'][$option['value']] = true;
        }

        $this->eavSetup->addAttributeOption($options);
    }
}
