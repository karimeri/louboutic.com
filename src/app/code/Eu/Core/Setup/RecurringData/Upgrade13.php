<?php

namespace Eu\Core\Setup\RecurringData;

use Synolia\Cron\Model\Task;
use Synolia\Cron\Model\TaskRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade13
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade13 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Cron\Model\Task
     */
    protected $task;

    /**
     * @var \Synolia\Cron\Model\TaskRepository
     */
    protected $taskRepository;

    /**
     * Upgrade13 constructor.
     * @param \Synolia\Cron\Model\Task $task
     * @param \Synolia\Cron\Model\TaskRepository $taskRepository
     */
    public function __construct(
        Task $task,
        TaskRepository $taskRepository
    ) {
        $this->task = $task;
        $this->taskRepository = $taskRepository;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Ecg.Performance.Loop.ModelLSD
     */
    public function run(Upgrade $upgradeObject)
    {
        $data = [
            [
                'name' => 'y2_export_customer',
                'active' => 1,
                'frequency' => '30 19 * * *',
                'command' => 'synolia:sync:launch y2_export_customer',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'y2_export_address',
                'active' => 1,
                'frequency' => '30 19 * * *',
                'command' => 'synolia:sync:launch y2_export_address',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'y2_export_sales_tickets',
                'active' => 1,
                'frequency' => '30 19 * * *',
                'command' => 'synolia:sync:launch y2_export_sales_tickets',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ],
            [
                'name' => 'y2_export_payments',
                'active' => 1,
                'frequency' => '30 19 * * *',
                'command' => 'synolia:sync:launch y2_export_payments',
                'parameter' => '',
                'option' => '',
                'isolated' => 1
            ]
        ];

        foreach ($data as $task) {
            $taskModel = $this->task->setData($task);
            $this->taskRepository->save($taskModel);
        }
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Init platform cron for Y2';
    }
}
