<?php

namespace Eu\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Synolia\Slider\Setup\Eav\SliderSetup;

class Upgrade25 implements UpgradeDataSetupInterface
{

    /**
     * @var CmsSetup
     */
    protected $cmsSetup;
    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * Upgrade25 constructor.
     * @param CmsSetup $cmsSetup
     * @param SliderSetup $sliderSetup
     */
    public function __construct(CmsSetup $cmsSetup, SliderSetup $sliderSetup)
    {
        $this->cmsSetup    = $cmsSetup;
        $this->sliderSetup = $sliderSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->addMaterialsSliderFr();
        $this->addMaterialsSliderEn();
        $this->addEmbellishmentSliderEuFr();
        $this->addEmbellishmentSliderEn();
        $this->addAnimatedShoesSliderEuFr();
        $this->addAnimatedShoesSliderEuEn();
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addMaterialsSliderFr()
    {
        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-first',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-second',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );

        $sliders = [
            [
                'identifier'     => 'discover-soles-slider-fr',
                'title'          => 'DISCOVER - Soles slider - FR',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-soles-slide-1-fr',
                        'title'      => 'La semelle de gomme',
                        'content'    => $secondSlide,
                        'image_big'  => 'step3-sole-img2.png',
                    ],
                    [
                        'identifier' => 'discover-soles-slide-2-fr',
                        'title'      => 'La semelle rouge',
                        'content'    => $firstSlide,
                        'image_big'  => 'step3-sole-img1.png',
                    ],
                ],
            ],
            [
                'identifier'     => 'discover-materials-slider-fr',
                'title'          => 'DISCOVER - Materials slider - FR',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-materials-slide-1-fr',
                        'title'      => 'Cuir',
                        'image_big'  => 'materials1.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-2-fr',
                        'title'      => 'Cuir Vernis Imprimé',
                        'image_big'  => 'materials3.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-3-fr',
                        'title'      => 'Cuir Vernis',
                        'image_big'  => 'materials4.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-4-fr',
                        'title'      => 'Découpe Laser',
                        'image_big'  => 'materials6.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-5-fr',
                        'title'      => 'Denim',
                        'image_big'  => 'materials7.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-6-fr',
                        'title'      => 'Cuir Vernis Dégradé',
                        'image_big'  => 'materials8.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-7-fr',
                        'title'      => 'Tissu Moiré',
                        'image_big'  => 'materials9.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-8-fr',
                        'title'      => 'Tressé',
                        'image_big'  => 'materials10.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-9-fr',
                        'title'      => 'Veau Velours',
                        'image_big'  => 'materials11.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, false);
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addMaterialsSliderEn()
    {
        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-first',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-second',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );

        $sliders = [
            [
                'identifier'     => 'discover-soles-slider-en',
                'title'          => 'DISCOVER - Soles slider - EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-soles-slide-1-en',
                        'title'      => 'The Lug sole',
                        'content'    => $secondSlide,
                        'image_big'  => 'step3-sole-img2.png',
                    ],
                    [
                        'identifier' => 'discover-soles-slide-2-en',
                        'title'      => 'The Red sole',
                        'content'    => $firstSlide,
                        'image_big'  => 'step3-sole-img1.png',
                    ],
                ],
            ],
            [
                'identifier'     => 'discover-materials-slider-en',
                'title'          => 'DISCOVER - Materials slider - EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-materials-slide-1-en',
                        'title'      => 'Calf Leather',
                        'image_big'  => 'materials1.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-2-en',
                        'title'      => 'Printed Patent Leather',
                        'image_big'  => 'materials3.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-3-en',
                        'title'      => 'Patent Leather',
                        'image_big'  => 'materials4.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-4-en',
                        'title'      => 'Laser Cut',
                        'image_big'  => 'materials6.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-5-en',
                        'title'      => 'Denim',
                        'image_big'  => 'materials7.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-6-en',
                        'title'      => 'Patent Dégradé',
                        'image_big'  => 'materials8.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-7-en',
                        'title'      => 'Moiré',
                        'image_big'  => 'materials9.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-8-en',
                        'title'      => 'Tressé',
                        'image_big'  => 'materials10.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-9-en',
                        'title'      => 'Veau Velours',
                        'image_big'  => 'materials11.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, true);
    }

    /**
     * @param \Eu\Core\Setup\UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addAnimatedShoesSliderEuFr()
    {

        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-first',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-second',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );
        $thirdSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-third',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );
        $fourthSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fourth',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );
        $fifthSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fifth',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/fr_FR'
        );

        $sliders = [
            [
                'identifier'     => 'discover-animated-shoe-slider-fr',
                'title'          => 'DISCOVER - Animated Shoe Slider - FR',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'derby-fr',
                        'title'        => 'Derby',
                        'image_big'    => 'oxford-sprite-in.png',
                        'image_medium' => 'oxford-sprite-out.png',
                        'image_small'  => 'oxford.svg',
                        'content'      => $firstSlide,
                    ],
                    [
                        'identifier'   => 'loafer-fr',
                        'title'        => 'Mocassin',
                        'image_big'    => 'loafer-sprite-in.png',
                        'image_medium' => 'loafer-sprite-out.png',
                        'image_small'  => 'loafer.svg',
                        'content'      => $secondSlide,
                    ],
                    [
                        'identifier'   => 'oxford-fr',
                        'title'        => 'Richelieu',
                        'image_big'    => 'brogue-sprite-in.png',
                        'image_medium' => 'brogue-sprite-out.png',
                        'image_small'  => 'brogue.svg',
                        'content'      => $thirdSlide,
                    ],
                    [
                        'identifier'   => 'ankle-boot-fr',
                        'title'        => 'Bottine',
                        'image_big'    => 'boot-sprite-in.png',
                        'image_medium' => 'boot-sprite-out.png',
                        'image_small'  => 'boot.svg',
                        'content'      => $fourthSlide,
                    ],
                    [
                        'identifier'   => 'double-monk-strap-fr',
                        'title'        => 'Soulier à double boucle',
                        'image_big'    => 'monkStrap-sprite-in.png',
                        'image_medium' => 'monkStrap-sprite-out.png',
                        'image_small'  => 'monkstrap.svg',
                        'content'      => $fifthSlide,
                    ]
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }

    /**
     * @param \Eu\Core\Setup\UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addAnimatedShoesSliderEuEn()
    {

        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-first',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-second',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );
        $thirdSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-third',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );
        $fourthSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fourth',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );
        $fifthSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fifth',
            'Eu_Core',
            '',
            '',
            'misc/cms/blocks/en_GB'
        );

        $sliders = [
            [
                'identifier'     => 'discover-animated-shoe-slider-en',
                'title'          => 'DISCOVER - Animated Shoe Slider - EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'derby-en',
                        'title'        => 'Derby',
                        'image_big'    => 'oxford-sprite-in.png',
                        'image_medium' => 'oxford-sprite-out.png',
                        'image_small'  => 'oxford.svg',
                        'content'      => $firstSlide,
                    ],
                    [
                        'identifier'   => 'loafer-en',
                        'title'        => 'Loafer',
                        'image_big'    => 'loafer-sprite-in.png',
                        'image_medium' => 'loafer-sprite-out.png',
                        'image_small'  => 'loafer.svg',
                        'content'      => $secondSlide,
                    ],
                    [
                        'identifier'   => 'oxford-en',
                        'title'        => 'Oxford',
                        'image_big'    => 'brogue-sprite-in.png',
                        'image_medium' => 'brogue-sprite-out.png',
                        'image_small'  => 'brogue.svg',
                        'content'      => $thirdSlide,
                    ],
                    [
                        'identifier'   => 'ankle-boot-en',
                        'title'        => 'Ankle Boot',
                        'image_big'    => 'boot-sprite-in.png',
                        'image_medium' => 'boot-sprite-out.png',
                        'image_small'  => 'boot.svg',
                        'content'      => $fourthSlide,
                    ],
                    [
                        'identifier'   => 'double-monk-strap-en',
                        'title'        => 'Double Monk Strap',
                        'image_big'    => 'monkStrap-sprite-in.png',
                        'image_medium' => 'monkStrap-sprite-out.png',
                        'image_small'  => 'monkstrap.svg',
                        'content'      => $fifthSlide,
                    ]
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }

    /**
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addEmbellishmentSliderEn()
    {
        $slider = [
            [
                'identifier'     => 'discover-embellishment-slider-en',
                'title'          => 'DISCOVER - Embellishment slider - EN',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-embellishment-slide-1',
                        'title'      => 'Embroideries',
                        'image_big'  => 'embellishments1.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-2',
                        'title'      => 'Chains',
                        'image_big'  => 'embellishments3.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-3',
                        'title'      => 'Spikes Dégradé',
                        'image_big'  => 'embellishments4.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-4',
                        'title'      => 'Embellished Patch',
                        'image_big'  => 'embellishments5.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-5',
                        'title'      => 'Pik Pik',
                        'image_big'  => 'embellishments8.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-6',
                        'title'      => 'Embroideries',
                        'image_big'  => 'embellishments2.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-7',
                        'title'      => 'Strass',
                        'image_big'  => 'embellishments10.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-8',
                        'title'      => 'Spikes',
                        'image_big'  => 'embellishments9.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-9',
                        'title'      => 'Perforated Cap Toe',
                        'image_big'  => 'embellishments6.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($slider);
    }

    /**
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addEmbellishmentSliderEuFr()
    {
        $slider = [
            [
                'identifier'     => 'discover-embellishment-slider-fr',
                'title'          => 'DISCOVER - Embellishment slider - FR',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-embellishment-slide-1',
                        'title'      => 'Broderies',
                        'image_big'  => 'embellishments1.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-2',
                        'title'      => 'Chaînes',
                        'image_big'  => 'embellishments3.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-3',
                        'title'      => 'Dégradé De Spikes',
                        'image_big'  => 'embellishments4.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-4',
                        'title'      => 'Ecusson',
                        'image_big'  => 'embellishments5.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-5',
                        'title'      => 'Pik Pik',
                        'image_big'  => 'embellishments8.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-6',
                        'title'      => 'Broderies',
                        'image_big'  => 'embellishments2.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-7',
                        'title'      => 'Strass',
                        'image_big'  => 'embellishments10.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-8',
                        'title'      => 'Spikes',
                        'image_big'  => 'embellishments9.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-9',
                        'title'      => 'Fleuri',
                        'image_big'  => 'embellishments6.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($slider);
    }

    /**
     * Gets description of the setup
     * @return string
     */
    public function getDescription()
    {
        return "Add sliders discover";
    }
}
