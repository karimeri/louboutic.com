<?php

namespace Eu\Core\Setup\RecurringData;

use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade26
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade26 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $storeCode = [
        'uk_en',
        'it_en',
        'de_en',
        'es_en',
        'ch_en',
        'nl_en',
        'lu_en',
        'be_en',
        'at_en',
        'ie_en',
        'pt_en',
        'mc_en',
        'gr_en',
    ];

    /**
     * Upgrade26 constructor.
     * @param ConfigSetup $configSetup
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManagerInterface $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Upgrade $upgradeObject
     */
    public function run(Upgrade $upgradeObject)
    {
        foreach ($this->storeCode as $storeCode) {
            $this->configSetup->saveConfig(
                'synolia_retailer/general/twelve_format_hour',
                1,
                ScopeInterface::SCOPE_WEBSITES,
                $this->storeManager->getStore($storeCode)->getWebsiteId()
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Website in 12 hour format';
    }
}
