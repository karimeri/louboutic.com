<?php

namespace Eu\Core\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade12
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade12 implements UpgradeDataSetupInterface
{
    const TABLE_ORDER_ADDRESS = 'sales_order_address';
    const TABLE_QUOTE_ADDRESS = 'quote_address';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade12 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnToAdd = [
            [
                'table' => self::TABLE_ORDER_ADDRESS,
                'name' => 'contact_phone',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Contact phone'
                ]
            ],
            [
                'table' => self::TABLE_QUOTE_ADDRESS,
                'name' => 'contact_phone',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Contact phone'
                ]
            ],
        ];

        foreach ($columnToAdd as $column) {
            $this->abstractSetup->addColumn(
                $column['table'],
                $column['name'],
                $column['definition']
            );
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add custom attributes contact_phone for sales_order_address and quote_address';
    }
}
