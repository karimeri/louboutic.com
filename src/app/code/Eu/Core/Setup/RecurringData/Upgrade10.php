<?php

namespace Eu\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Eu\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade10
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade10 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Eu\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * @var \Synolia\Standard\Setup\Eav\ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade10 constructor.
     * @param \Eu\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     */
    public function __construct(
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput,
        ConfigSetup $configSetup
    ) {
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
        $this->configSetup = $configSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {

        $prefixConfigFR = 'M.;Mlle;Mme.';

        foreach ($this->upgradeData->getStoresIndexedByLocale() as $locale => $stores) {
            if ($locale === 'fr_FR') {
                foreach ($stores as $store) {
                    $this->consoleOutput->writeln('Saving configuration '.$prefixConfigFR.' on '
                        .$locale.' store '. $store);

                    $this->configSetup->saveConfig(
                        'customer/address/prefix_options',
                        $prefixConfigFR,
                        'stores',
                        $store
                    );
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Saving prefix configuration on fr_FR stores';
    }
}
