<?php

namespace Eu\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade3
 * @package Eu\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * Upgrade3 constructor.
     * @param ConfigSetup $configSetup
     * @param StoreManager $storeManager
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     */
    public function run(Upgrade $upgradeObject)
    {
        // @codingStandardsIgnoreStart
        $this->configSetup->saveConfig('synolia_retailer/map/map_latitude', '47.585823');
        $this->configSetup->saveConfig('synolia_retailer/map/map_longitude', '8.518022');
        $this->configSetup->saveConfig('synolia_retailer/map/map_zoom', '6');
        $this->configSetup->saveConfig('synolia_retailer/map/map_style', 'ultra_light_with_labels');
        $this->configSetup->saveConfig('synolia_retailer/map/map_unit', 'kilometers');
        $this->configSetup->saveConfig('synolia_retailer/map/map_distance', '3328');
        $this->configSetup->saveConfig('synolia_retailer/general/display_address', '1');
        $this->configSetup->saveConfig('synolia_retailer/general/display_contact_information', '1');
        $this->configSetup->saveConfig('synolia_retailer/map/map_filter', '{"_0":{"label":"Woman","value":"woman"},"_1":{"label":"Man","value":"man"},"_2":{"label":"Bag","value":"bag"},"_3":{"label":"Beauty","value":"beauty"}}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_all_retailer_title', 'Find Christian Louboutin Stores - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_all_retailer_description', 'Christian Louboutin Official Website {store} - Locate All Christian Louboutin Stores in the World');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_country_title', 'Find Christian Louboutin Stores in {country} - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_country_description', 'Discover our Christian Louboutin Stores in {country} and find all information about Opening Hours, Special Events and Products.');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_city_title', 'Find Christian Louboutin Stores in {city}, {country} - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_city_description', 'Discover our Christian Louboutin Stores in {city}, {country} and find all information about Opening Hours, Special Events and Products.');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_retailer_title', 'Find {retailer} Stores - Christian Louboutin {store}');
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_retailer_description', 'Discover our {retailer} and find all information about Opening Hours, Special Events and Products.');
        $this->configSetup->saveConfig('synolia_retailer/map/map_marker', 'default/point-map-small.png');


        $store = $this->storeManager->getStore('fr_fr');
        $storeId = $store->getId();

        $this->configSetup->saveConfig('synolia_retailer/seo/meta_retailer_title', 'Boutique {retailer} - Christian Louboutin {store}', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_retailer_description', 'Visitez {retailer} et trouvez ici les horaires d’ouverture et coordonnées.', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_all_retailer_title', 'Trouver les magasins - Christian Louboutin {store}', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_all_retailer_description', 'Christian Louboutin Site Officiel {store} - Trouvez votre magasin Christian Louboutin le plus proche.', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_country_title', 'Trouver les magasins {country} - Christian Louboutin {store}', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_country_description', 'Visitez nos boutiques {country} et découvrez les souliers, la maroquinerie et les accessoires. Trouvez ici les horaires d’ouverture et coordonnées.', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_city_title', 'Trouver les magasins {city}, {country} - Christian Louboutin {store}', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/seo/meta_city_description', 'Visitez nos boutiques {city}, {country} et découvrez les souliers, la maroquinerie et les accessoires. Trouvez ici les horaires d’ouverture et coordonnées.', 'stores', $storeId);
        $this->configSetup->saveConfig('synolia_retailer/map/map_filter', '{"_0":{"label":"Femme","value":"woman"},"_1":{"label":"Homme","value":"man"},"_2":{"label":"Sacs","value":"bag"},"_3":{"label":"Beauté","value":"beauty"}}', 'stores', $storeId);
        // @codingStandardsIgnoreEnd
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Retailer config EU';
    }
}
