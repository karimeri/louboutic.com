<?php

namespace Eu\Core\Setup;

use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Registry;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\StoreManager;
use Magento\Store\Model\StoreRepositoryFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\App\Config;
use Magento\Store\Model\WebsiteRepository;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\ConfigSetupFactory;
use Synolia\Standard\Setup\ShopSetupFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Project\Core\Setup\CoreInstallData;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class InstallData
 * @package   Eu\Core\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData extends CoreInstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \Synolia\Standard\Setup\ShopSetup
     */
    protected $shopSetup;

    /**
     * @var \Magento\Store\Model\StoreRepository
     */
    protected $storeRepository;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Website
     */
    protected $websiteResourceModel;

    /**
     * @var WebsiteFactory
     */
    protected $websiteFactory;

    /**
     * @var GroupFactory
     */
    protected $groupFactory;

    /**
     * @var Group
     */
    protected $groupResourceModel;

    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Store
     */
    protected $storeResourceModel;

    /**
     * InstallData constructor.
     * @param DirectoryList          $directoryList
     * @param Csv                    $csvProcessor
     * @param Reader                 $moduleReader
     * @param StoreManager           $storeManager
     * @param WriterInterface        $writerInterface
     * @param Config                 $config
     * @param ShopSetupFactory       $shopSetupFactory
     * @param StoreRepositoryFactory $storeRepositoryFactory
     * @param ConfigSetupFactory     $configSetupFactory
     * @param WebsiteRepository      $websiteRepository
     * @param Website                $websiteResourceModel
     * @param WebsiteFactory         $websiteFactory
     * @param GroupFactory           $groupFactory
     * @param Group                  $groupResourceModel
     * @param StoreFactory           $storeFactory
     * @param Store                  $storeResourceModel
     * @param Registry               $registry
     */
    public function __construct(
        DirectoryList $directoryList,
        Csv $csvProcessor,
        Reader $moduleReader,
        StoreManager $storeManager,
        WriterInterface $writerInterface,
        Config $config,
        ShopSetupFactory $shopSetupFactory,
        StoreRepositoryFactory $storeRepositoryFactory,
        ConfigSetupFactory $configSetupFactory,
        WebsiteRepository $websiteRepository,
        Website $websiteResourceModel,
        WebsiteFactory $websiteFactory,
        GroupFactory $groupFactory,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        Store $storeResourceModel,
        Registry $registry
    ) {
        parent::__construct($directoryList, $csvProcessor, $moduleReader, $storeManager, $writerInterface);

        $this->config               = $config;
        $this->shopSetup            = $shopSetupFactory->create();
        $this->storeRepository      = $storeRepositoryFactory->create();
        $this->configSetupFactory   = $configSetupFactory;
        $this->websiteRepository    = $websiteRepository;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->websiteFactory       = $websiteFactory;
        $this->groupFactory         = $groupFactory;
        $this->groupResourceModel   = $groupResourceModel;
        $this->storeFactory         = $storeFactory;
        $this->storeResourceModel   = $storeResourceModel;
        $this->registry             = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        // @codingStandardsIgnoreStart
        if (!getenv('PROJECT_ENV') || getenv('PROJECT_ENV') != Environment::EU) {
            echo 'Install Core EU ignore';
            // @codingStandardsIgnoreEnd
            $setup->endSetup();
            return;
        }

        $this->configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        $this->config->clean();
        $this->createWebsites();

        $setup->endSetup();
    }

    /**
     * Create website Time FR and Time Int EN
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function createWebsites()
    {
        $this->configSetup->saveConfig('general/locale/code', 'fr_FR');

        $this->registry->register('isSecureArea', true);

        $this->shopSetup->populateWebsites([
            UpgradeData::WEBSITE_CODE_FR => [
                'code' => UpgradeData::WEBSITE_CODE_FR,
                'name' => 'France',
                'is_default' => 1,
                'groups' => [
                    'France Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_FR,
                        'name' => 'France Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_FR_FR => [
                                'code' => UpgradeData::STORE_CODE_FR_FR,
                                'name' => 'Français',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_UK => [
                'code' => UpgradeData::WEBSITE_CODE_UK,
                'name' => 'United Kingdom',
                'groups' => [
                    'United Kingdom Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_UK,
                        'name' => 'United Kingdom Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_UK_EN => [
                                'code' => UpgradeData::STORE_CODE_UK_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_IT => [
                'code' => UpgradeData::WEBSITE_CODE_IT,
                'name' => 'Italy',
                'groups' => [
                    'Italy Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_IT,
                        'name' => 'Italy Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_IT_EN => [
                                'code' => UpgradeData::STORE_CODE_IT_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_DE => [
                'code' => UpgradeData::WEBSITE_CODE_DE,
                'name' => 'Germany',
                'groups' => [
                    'Germany Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_DE,
                        'name' => 'Germany Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_DE_EN => [
                                'code' => UpgradeData::STORE_CODE_DE_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_ES => [
                'code' => UpgradeData::WEBSITE_CODE_ES,
                'name' => 'Spain',
                'groups' => [
                    'Spain Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_ES,
                        'name' => 'Spain Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_ES_EN => [
                                'code' => UpgradeData::STORE_CODE_ES_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_CH => [
                'code' => UpgradeData::WEBSITE_CODE_CH,
                'name' => 'Switzerland',
                'groups' => [
                    'Switzerland Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_CH,
                        'name' => 'Switzerland Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_CH_EN => [
                                'code' => UpgradeData::STORE_CODE_CH_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_CH_FR => [
                                'code' => UpgradeData::STORE_CODE_CH_FR,
                                'name' => 'French',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_NL => [
                'code' => UpgradeData::WEBSITE_CODE_NL,
                'name' => 'Netherlands',
                'groups' => [
                    'Netherlands Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_NL,
                        'name' => 'Netherlands Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_NL_EN => [
                                'code' => UpgradeData::STORE_CODE_NL_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_LU => [
                'code' => UpgradeData::WEBSITE_CODE_LU,
                'name' => 'Luxembourg',
                'groups' => [
                    'Luxembourg Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_LU,
                        'name' => 'Luxembourg Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_LU_EN => [
                                'code' => UpgradeData::STORE_CODE_LU_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_LU_FR => [
                                'code' => UpgradeData::STORE_CODE_LU_FR,
                                'name' => 'French',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_BE => [
                'code' => UpgradeData::WEBSITE_CODE_BE,
                'name' => 'Belgium',
                'groups' => [
                    'Belgium Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_BE,
                        'name' => 'Belgium Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_BE_EN => [
                                'code' => UpgradeData::STORE_CODE_BE_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_BE_FR => [
                                'code' => UpgradeData::STORE_CODE_BE_FR,
                                'name' => 'French',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_AT => [
                'code' => UpgradeData::WEBSITE_CODE_AT,
                'name' => 'Austria',
                'groups' => [
                    'Austria Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_AT,
                        'name' => 'Austria Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_AT_EN => [
                                'code' => UpgradeData::STORE_CODE_AT_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_IE => [
                'code' => UpgradeData::WEBSITE_CODE_IE,
                'name' => 'Ireland',
                'groups' => [
                    'Ireland Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_IE,
                        'name' => 'Ireland Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_IE_EN => [
                                'code' => UpgradeData::STORE_CODE_IE_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_PT => [
                'code' => UpgradeData::WEBSITE_CODE_PT,
                'name' => 'Portugal',
                'groups' => [
                    'Portugal Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_PT,
                        'name' => 'Portugal Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_PT_EN => [
                                'code' => UpgradeData::STORE_CODE_PT_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_MC => [
                'code' => UpgradeData::WEBSITE_CODE_MC,
                'name' => 'Monaco',
                'groups' => [
                    'Monaco Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_MC,
                        'name' => 'Monaco Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_MC_EN => [
                                'code' => UpgradeData::STORE_CODE_MC_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ],
                            UpgradeData::STORE_CODE_MC_FR => [
                                'code' => UpgradeData::STORE_CODE_MC_FR,
                                'name' => 'French',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ],
            UpgradeData::WEBSITE_CODE_GR => [
                'code' => UpgradeData::WEBSITE_CODE_GR,
                'name' => 'Greece',
                'groups' => [
                    'Greece Group' => [
                        'code' => UpgradeData::WEBSITE_CODE_GR,
                        'name' => 'Greece Group',
                        'root_category_id' => 2,
                        'stores' => [
                            UpgradeData::STORE_CODE_GR_EN => [
                                'code' => UpgradeData::STORE_CODE_GR_EN,
                                'name' => 'English',
                                'is_active' => 1
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $store = $this->storeFactory->create();
        $this->storeResourceModel->load($store, 'default', 'code');
        $this->storeResourceModel->delete($store);

        $storeGroup = $this->groupFactory->create();
        $this->groupResourceModel->load($storeGroup, 'main_website_store', 'code');
        $this->groupResourceModel->delete($storeGroup);

        $website = $this->websiteFactory->create();
        $this->websiteResourceModel->load($website, 'base', 'code');
        $this->websiteResourceModel->delete($website);

        $idStoreFrFr = $this->storeRepository->get(UpgradeData::STORE_CODE_FR_FR)->getId();
        $idStoreUkEn = $this->storeRepository->get(UpgradeData::STORE_CODE_UK_EN)->getId();
        $idStoreItEn = $this->storeRepository->get(UpgradeData::STORE_CODE_IT_EN)->getId();
        $idStoreDeEn = $this->storeRepository->get(UpgradeData::STORE_CODE_DE_EN)->getId();
        $idStoreEsEn = $this->storeRepository->get(UpgradeData::STORE_CODE_ES_EN)->getId();
        $idStoreChEn = $this->storeRepository->get(UpgradeData::STORE_CODE_CH_EN)->getId();
        $idStoreChFr = $this->storeRepository->get(UpgradeData::STORE_CODE_CH_FR)->getId();
        $idStoreNlEn = $this->storeRepository->get(UpgradeData::STORE_CODE_NL_EN)->getId();
        $idStoreLuEn = $this->storeRepository->get(UpgradeData::STORE_CODE_LU_EN)->getId();
        $idStoreLuFr = $this->storeRepository->get(UpgradeData::STORE_CODE_LU_FR)->getId();
        $idStoreBeFr = $this->storeRepository->get(UpgradeData::STORE_CODE_BE_FR)->getId();
        $idStoreBeEn = $this->storeRepository->get(UpgradeData::STORE_CODE_BE_EN)->getId();
        $idStoreAtEn = $this->storeRepository->get(UpgradeData::STORE_CODE_AT_EN)->getId();
        $idStoreIeEn = $this->storeRepository->get(UpgradeData::STORE_CODE_IE_EN)->getId();
        $idStorePtEn = $this->storeRepository->get(UpgradeData::STORE_CODE_PT_EN)->getId();
        $idStoreMcEn = $this->storeRepository->get(UpgradeData::STORE_CODE_MC_EN)->getId();
        $idStoreMcFr = $this->storeRepository->get(UpgradeData::STORE_CODE_MC_FR)->getId();
        $idStoreGrEn = $this->storeRepository->get(UpgradeData::STORE_CODE_GR_EN)->getId();

        $stores = [
            $idStoreFrFr => 'fr_FR',
            $idStoreUkEn => 'en_GB',
            $idStoreItEn => 'en_GB',
            $idStoreDeEn => 'en_GB',
            $idStoreEsEn => 'en_GB',
            $idStoreChEn => 'en_GB',
            $idStoreChFr => 'fr_FR',
            $idStoreNlEn => 'en_GB',
            $idStoreLuEn => 'en_GB',
            $idStoreLuFr => 'fr_FR',
            $idStoreBeFr => 'fr_FR',
            $idStoreBeEn => 'en_GB',
            $idStoreAtEn => 'en_GB',
            $idStoreIeEn => 'en_GB',
            $idStorePtEn => 'en_GB',
            $idStoreMcEn => 'en_GB',
            $idStoreMcFr => 'fr_FR',
            $idStoreGrEn => 'en_GB'
        ];

        foreach ($stores as $id => $locale) {
            $this->configSetup->saveConfig('general/locale/code', $locale, ScopeInterface::SCOPE_STORES, $id);
        }
    }
}
