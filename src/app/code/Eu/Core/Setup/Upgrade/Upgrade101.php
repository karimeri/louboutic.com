<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;

/**
 * Class Upgrade101
 * @package   Eu\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade101
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $upgradeDataObject->getConfigSetup()->saveConfig('general/locale/timezone', 'Europe/Paris');
    }
}
