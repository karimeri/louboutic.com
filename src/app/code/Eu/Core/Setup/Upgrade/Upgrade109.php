<?php
namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Magento\Rma\Model\Rma;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Upgrade109
 * @package Eu\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade109
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $storeViewIdIt = $upgradeDataObject->getStoreId(UpgradeData::STORE_CODE_IT_EN);

        // Active RMA on storefront for all
        $upgradeDataObject->getConfigSetup()->saveConfig(Rma::XML_PATH_ENABLED, 1);

        // Disable RMA on storefront for Italy
        $upgradeDataObject->getConfigSetup()->saveConfig(
            Rma::XML_PATH_ENABLED,
            0,
            ScopeInterface::SCOPE_STORES,
            $storeViewIdIt
        );
    }
}
