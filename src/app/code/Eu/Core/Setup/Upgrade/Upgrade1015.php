<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Psr\Log\LoggerInterface;
use Synolia\Cron\Api\TaskRepositoryInterface;

/**
 * Class Upgrade1015
 * @package Eu\Core\Setup\Upgrade
 */
class Upgrade1015
{
    /**
     * @var TaskRepositoryInterface
     */
    protected $taskRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $tasks = [
        'linkshare_export_products',
        'linkshare_export_orders',
        'linkshare_export_canceled_orders'
    ];

    /**
     * Upgrade1015 constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository,
        LoggerInterface $logger
    ) {
        $this->taskRepository = $taskRepository;
        $this->logger = $logger;
    }

    /**
     * Remove Project_Linkshare tasks
     *
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        try {
            foreach ($this->tasks as $task) {
                $task = $this->taskRepository->getByName($task);
                if ($task) {
                    $this->taskRepository->deleteById($task->getTaskId());
                }
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}