<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Magento\Framework\App\State;
use Magento\Framework\File\Csv;
use Magento\Store\Model\ScopeInterface;
use Magento\Tax\Api\Data\TaxRuleInterface;
use Magento\Tax\Api\Data\TaxRuleInterfaceFactory;
use Magento\Tax\Model\Calculation\Rate;
use Magento\Tax\Model\Calculation\RateFactory;
use Magento\Tax\Model\Calculation\RateRepository;
use Magento\Tax\Api\TaxRuleRepositoryInterface;
use Magento\Tax\Api\Data\TaxClassInterfaceFactory;
use Magento\Tax\Api\TaxClassRepositoryInterface;
use Magento\Framework\Module\Dir\Reader;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Project\Core\Setup\TaxInstallData;

/**
 * Class Upgrade102
 * @package   Eu\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade102 extends TaxInstallData
{
    const MODULE_NAME = 'Eu_Core';
    const TAX_LOW_RATE_PATH = 'data/taxLowRates.csv';
    const TAX_RATE_PATH = 'data/taxRates.csv';
    const TAX_SHIPPING_RATE_PATH = 'data/taxShippingRates.csv';

    /**
     * @var RateFactory
     */
    protected $rateFactory;

    /**
     * @var RateRepository
     */
    protected $rateRepository;

    /**
     * @var TaxRuleRepositoryInterface
     */
    protected $ruleService;

    /**
     * @var TaxRuleInterfaceFactory
     */
    protected $taxRuleDataObjectFactory;

    /**
     * @var TaxClassInterfaceFactory
     */
    protected $taxClassDataObjectFactory;

    /**
     * @var TaxClassRepositoryInterface
     */
    protected $taxClassRepository;

    /**
     * @var array
     */
    protected $taxCalculationRateByCode = [];

    /**
     * @var array
     */
    protected $taxCalculationLowRateByCode = [];

    /**
     * @var array
     */
    protected $taxCalculationShippingRateByCode = [];

    /**
     * Upgrade102 constructor.
     * @param Csv                         $csvProcessor
     * @param Reader                      $moduleReader
     * @param RateFactory                 $rateFactory
     * @param RateRepository              $rateRepository
     * @param TaxRuleInterfaceFactory     $taxRuleDataObjectFactory
     * @param TaxRuleRepositoryInterface  $ruleService
     * @param TaxClassInterfaceFactory    $taxClassDataObjectFactory
     * @param TaxClassRepositoryInterface $taxClassRepository
     * @param State                       $state
     * @throws LocalizedException
     */
    public function __construct(
        Csv $csvProcessor,
        Reader $moduleReader,
        RateFactory $rateFactory,
        RateRepository $rateRepository,
        TaxRuleInterfaceFactory $taxRuleDataObjectFactory,
        TaxRuleRepositoryInterface $ruleService,
        TaxClassInterfaceFactory $taxClassDataObjectFactory,
        TaxClassRepositoryInterface $taxClassRepository,
        State $state
    ) {
        parent::__construct($csvProcessor, $moduleReader);

        $this->rateFactory               = $rateFactory;
        $this->rateRepository            = $rateRepository;
        $this->taxRuleDataObjectFactory  = $taxRuleDataObjectFactory;
        $this->ruleService               = $ruleService;
        $this->taxClassDataObjectFactory = $taxClassDataObjectFactory;
        $this->taxClassRepository        = $taxClassRepository;
        $this->state                     = $state;

        try {
            $this->state->setAreaCode('adminhtml');
        } catch (\Exception $e) {
            //Area code already set
        }
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $taxRate = $this->rateRepository->get(1);
        $this->rateRepository->delete($taxRate);
        $taxRate = $this->rateRepository->get(2);
        $this->rateRepository->delete($taxRate);

        $this->saveTaxCalculationRate();
        $this->saveTaxRule();
        $this->saveTaxConfiguration($upgradeDataObject);
    }

    /**
     * @throws LocalizedException
     */
    protected function saveTaxCalculationRate()
    {
        $taxCalculationLowRateData         = $this->getDataTaxFile(self::MODULE_NAME, self::TAX_LOW_RATE_PATH);
        $this->taxCalculationLowRateByCode = $this->saveTaxCalculationArray($taxCalculationLowRateData);

        $taxCalculationRateData         = $this->getDataTaxFile(self::MODULE_NAME, self::TAX_RATE_PATH);
        $this->taxCalculationRateByCode = $this->saveTaxCalculationArray($taxCalculationRateData);

        $taxCalculationShippingRateData         = $this->getDataTaxFile(self::MODULE_NAME, self::TAX_SHIPPING_RATE_PATH);
        $this->taxCalculationShippingRateByCode = $this->saveTaxCalculationArray($taxCalculationShippingRateData);
    }

    /**
     * @throws \Exception
     * @throws InputException
     * @throws LocalizedException
     */
    protected function saveTaxRule()
    {
        $taxClass          = $this->taxClassDataObjectFactory->create()
            ->setClassType('PRODUCT')
            ->setClassName('Low Rate Taxable goods');
        $taxClassLowRateId = $this->taxClassRepository->save($taxClass);

        $taxClass               = $this->taxClassDataObjectFactory->create()
            ->setClassType('PRODUCT')
            ->setClassName('Shipping');
        $taxClassShippingRateId = $this->taxClassRepository->save($taxClass);

        $taxRules = [
            'tax_good_rate' => [
                'setCode' => 'Retail Customer-Taxable Goods-Rate 1',
                'setTaxRateIds' => $this->taxCalculationRateByCode,
                'setCustomerTaxClassIds' => [3],
                'setProductTaxClassIds' => [2],
                'setPriority' => 1,
                'setPosition' => 1
            ],
            'tax_good_low_rate' => [
                'setCode' => 'Retail Customer-Taxable Goods-LowRate',
                'setTaxRateIds' => $this->taxCalculationLowRateByCode,
                'setCustomerTaxClassIds' => [3],
                'setProductTaxClassIds' => [$taxClassLowRateId],
                'setPriority' => 1,
                'setPosition' => 1
            ],
            'tax_good_shipping_rate' => [
                'setCode' => 'Shipping',
                'setTaxRateIds' => $this->taxCalculationShippingRateByCode,
                'setCustomerTaxClassIds' => [3],
                'setProductTaxClassIds' => [$taxClassShippingRateId],
                'setPriority' => 1,
                'setPosition' => 1
            ]
        ];

        foreach ($taxRules as $taxRule) {
            $taxRuleDataObject = $this->getTaxRuleDataObject();
            foreach ($taxRule as $method => $value) {
                $taxRuleDataObject->$method($value);
            }

            $this->ruleService->save($taxRuleDataObject);
        }
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    protected function saveTaxConfiguration(UpgradeData $upgradeDataObject)
    {
        $configData = [
            'tax/classes/shipping_tax_class' => 5,
            'tax/calculation/price_includes_tax' => 1,
            'tax/calculation/shipping_includes_tax' => 1,
            'tax/calculation/discount_tax' => 1,
            'tax/defaults/country' => 'FR',
            'tax/display/type' => 2,
            'tax/display/shipping' => 2,
            'tax/cart_display/price' => 2,
            'tax/cart_display/subtotal' => 2,
            'tax/cart_display/shipping' => 2,
            'tax/cart_display/gift_wrapping' => 2,
            'tax/cart_display/printed_card' => 2,
            'tax/sales_display/price' => 2,
            'tax/sales_display/subtotal' => 2,
            'tax/sales_display/shipping' => 2,
            'tax/sales_display/gift_wrapping' => 2,
            'tax/sales_display/printed_card' => 2,
            'tax/sales_display/grand_total' => 2
        ];

        foreach ($configData as $path => $value) {
            $upgradeDataObject->getConfigSetup()->saveConfig($path, $value);
        }
    }

    /**
     * @param array $taxCalculationArray
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveTaxCalculationArray(array $rawData)
    {
        $taxCalculationArray = $this->formatRawData($rawData);

        $taxCalculationRateByCode = [];
        foreach ($taxCalculationArray as $tax) {
            $rate = $this->getRateCalculationModel();
            $rate->addData($tax);
            $taxRate                    = $this->rateRepository->save($rate);
            $taxCalculationRateByCode[] = $taxRate->getId();
        }

        return $taxCalculationRateByCode;
    }

    /**
     * @return Rate
     */
    protected function getRateCalculationModel()
    {
        return $this->rateFactory->create();
    }

    /**
     * @return TaxRuleInterface
     */
    protected function getTaxRuleDataObject()
    {
        return $this->taxRuleDataObjectFactory->create();
    }
}
