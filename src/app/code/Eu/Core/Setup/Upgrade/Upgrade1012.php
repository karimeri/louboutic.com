<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;

/**
 * Class Upgrade1012
 *
 * @package Eu\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade1012
{
    const COUNTRY_AND_CURRENCY_PATH = 'data/countryAndCurrency.csv';

    /**
     * @param UpgradeData $upgradeDataObject
     *
     * @throws \Exception
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $countryAndCurrencyData = $upgradeDataObject->getDataFile(
            UpgradeData::MODULE_NAME,
            self::COUNTRY_AND_CURRENCY_PATH
        );
        $upgradeDataObject->saveConfig($countryAndCurrencyData);
    }
}
