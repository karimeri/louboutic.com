<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Upgrade1013
 * @package Eu\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1013
{

    const REGION_STATE_REQUIRED_PATH = 'general/region/state_required';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $config;

    /**
     * Upgrade1013 constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $config
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->config = $config;
    }

    /**
     * Runs setup
     * @param UpgradeData $upgradeObject
     * @return void
     */
    public function run(UpgradeData $upgradeObject)
    {
        $newStatesArray = [
            UpgradeData::WEBSITE_CODE_FR,
            'GB',//UpgradeData::WEBSITE_CODE_UK
            UpgradeData::WEBSITE_CODE_IT,
            UpgradeData::WEBSITE_CODE_DE,
            UpgradeData::WEBSITE_CODE_ES,
            UpgradeData::WEBSITE_CODE_CH,
            UpgradeData::WEBSITE_CODE_NL,
            UpgradeData::WEBSITE_CODE_LU,
            UpgradeData::WEBSITE_CODE_BE,
            UpgradeData::WEBSITE_CODE_AT,
            UpgradeData::WEBSITE_CODE_IE,
            UpgradeData::WEBSITE_CODE_PT,
            UpgradeData::WEBSITE_CODE_MC,
            UpgradeData::WEBSITE_CODE_GR,
        ];
        $newStatesArray = \array_map('strtoupper', $newStatesArray);

        $requiredStates = $this->scopeConfig->getValue(
            self::REGION_STATE_REQUIRED_PATH,
            ScopeInterface::SCOPE_STORE
        );
        $requiredStatesArray = \explode(',', $requiredStates);

        //Merge the new and the existing arrays of states
        $newRequiredStatesArray = \array_unique(\array_merge($requiredStatesArray, $newStatesArray), SORT_REGULAR);

        $upgradeObject->getConfigSetup()->saveConfig(
            self::REGION_STATE_REQUIRED_PATH,
            \implode(',', $newRequiredStatesArray)
        );
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Make region required for EU countries';
    }
}
