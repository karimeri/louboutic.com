<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Exception\NoSuchEntityException;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade108
 * @package Eu\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade108
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade108 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @throws NoSuchEntityException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $cmsBlockIdentifiers = [
            'fit-suggestions-runs-full-larger',
            'fit-suggestions-runs-full-small',
            'fit-suggestions-runs-half-large',
            'fit-suggestions-runs-half-small',
            'fit-suggestions-true-to-size',
            'fit-suggestions-whole-only'
        ];

        foreach ($cmsBlockIdentifiers as $cmsBlockIdentifier) {
            foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
                $cmsBlockContent = $this->cmsSetup->getCmsBlockContent(
                    $cmsBlockIdentifier,
                    'Eu_Core',
                    '',
                    '',
                    'misc/cms/blocks/'.$locale
                );

                $cmsBlock = [
                    'title'      => 'PRODUCT > '.ucfirst(str_replace('-', ' ', $cmsBlockIdentifier)),
                    'identifier' => $cmsBlockIdentifier,
                    'content'    => $cmsBlockContent,
                    'is_active'  => 1,
                    'store_id'   => $stores,
                    'stores'     => $stores
                ];

                $this->consoleOutput->writeln('Saving block '.$cmsBlockIdentifier.' on '
                    .$locale.' stores ('.implode(',', $stores).') ');

                $this->cmsSetup->saveBlock($cmsBlock);
            }
        }
    }
}
