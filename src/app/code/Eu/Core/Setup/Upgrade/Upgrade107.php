<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade107
 * @package Eu\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade107
{
    const XML_RECAPTCHA_BASE_PATH = 'synolia_recaptcha/general/';

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade107 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'enabled',
            '1'
        );
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'public_key',
            '6Ld_9E0UAAAAAMzxRkpc3JQE5u2Av_EyjyLHMDZd'
        );
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'private_key',
            '6Ld_9E0UAAAAADksv80P5EOAjxoMl9uRw6xbpxBy'
        );
    }
}
