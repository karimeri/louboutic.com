<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Exception\NoSuchEntityException;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade1019
 * @package Eu\Core\Setup\Upgrade
 */
class Upgrade1019
{
    /**
     * @var CmsSetup
     */
    protected $cmsSetup;

    /**
     * ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade1010 constructor.
     * @param CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     * @throws NoSuchEntityException
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $cmsBlockIdentifier = 'bracelet-sizes-table-men';

        foreach ($upgradeDataObject->getStoresIndexedByLocale() as $locale => $stores) {
            $cmsBlockContent = $this->cmsSetup->getCmsBlockContent(
                $cmsBlockIdentifier,
                'Eu_Core',
                '',
                '',
                'misc/cms/blocks/'.$locale
            );

            $cmsBlock = [
                'title'      => 'PRODUCT > Bracelets sizes table men',
                'identifier' => $cmsBlockIdentifier,
                'content'    => $cmsBlockContent,
                'is_active'  => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->consoleOutput->writeln('Saving block '.$cmsBlockIdentifier.' on '
                .$locale.' stores ('.implode(',', $stores).') ');

            $this->cmsSetup->saveBlock($cmsBlock);
        }
    }
}
