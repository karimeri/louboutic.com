<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade1011
 * @package Eu\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1011
{
    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade1011 constructor.
     *
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    public function run()
    {
        $attribute = [
            'type' => 'customer_address',
            'code' => 'company',
            'data' => [
                'is_visible' => false
            ],
        ];

        if ($this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }
}
