<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;

/**
 * Class Upgrade105
 * @package   Eu\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade105
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Hide company on checkout address form on EU
        $upgradeDataObject->getConfigSetup()->saveConfig('customer/address/company_show', null);
    }
}
