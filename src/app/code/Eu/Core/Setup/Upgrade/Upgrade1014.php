<?php

namespace Eu\Core\Setup\Upgrade;

use Eu\Core\Setup\UpgradeData;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Store\Model\ScopeInterface;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade1014
 * @package Eu\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1014
{

    const REGION_STATE_REQUIRED_PATH = 'general/region/state_required';
    const REGION_DISPLAY_ALL_PATH = 'general/region/display_all';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    protected $config;

    /**
     * @var ConfigInterface
     */
    protected $resourceConfig;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade1014 constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config
     * @param ConfigInterface $resourceConfig
     * @param EavSetup $eavSetup
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $config,
        ConfigInterface $resourceConfig,
        EavSetup $eavSetup
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->config = $config;
        $this->resourceConfig = $resourceConfig;
        $this->eavSetup = $eavSetup;
    }

    /**
     * Runs setup
     * @param UpgradeData $upgradeObject
     * @return void
     */
    public function run(UpgradeData $upgradeObject)
    {
        //Update require states config
        if (!empty($this->getRequiredStates())) {
            $upgradeObject->getConfigSetup()->saveConfig(
                self::REGION_STATE_REQUIRED_PATH,
                $this->getRequiredStates()
            );
        } else {
            $this->resourceConfig->deleteConfig(
                self::REGION_STATE_REQUIRED_PATH,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                \Magento\Store\Model\Store::DEFAULT_STORE_ID
            );
        }

        //Hide region field on checkout form
        $upgradeObject->getConfigSetup()->saveConfig(
            self::REGION_DISPLAY_ALL_PATH,
            0
        );
    }

    /**
     * Remove EU states from require state config
     * @return array|string
     */
    public function getRequiredStates()
    {
        $statesArray = [
            UpgradeData::WEBSITE_CODE_FR,
            'GB',//UpgradeData::WEBSITE_CODE_UK
            UpgradeData::WEBSITE_CODE_IT,
            UpgradeData::WEBSITE_CODE_DE,
            UpgradeData::WEBSITE_CODE_ES,
            UpgradeData::WEBSITE_CODE_CH,
            UpgradeData::WEBSITE_CODE_NL,
            UpgradeData::WEBSITE_CODE_LU,
            UpgradeData::WEBSITE_CODE_BE,
            UpgradeData::WEBSITE_CODE_AT,
            UpgradeData::WEBSITE_CODE_IE,
            UpgradeData::WEBSITE_CODE_PT,
            UpgradeData::WEBSITE_CODE_MC,
            UpgradeData::WEBSITE_CODE_GR,
        ];
        $removedStatesArray = \array_map('strtoupper', $statesArray);

        $requiredStates = $this->scopeConfig->getValue(
            self::REGION_STATE_REQUIRED_PATH,
            ScopeInterface::SCOPE_STORE
        );
        $requiredStatesArray = \explode(',', $requiredStates);

        $result = \array_diff($requiredStatesArray, $removedStatesArray);

        if ($result) {
            return implode(',', $result);
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Make region not required for EU countries (revert upgrade 1013)';
    }
}
