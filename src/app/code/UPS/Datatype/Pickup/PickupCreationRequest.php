<?php

namespace UPS\Datatype\Pickup;

/**
 * Class PickupCreationRequest
 *
 * @package UPS\Datatype\Pickup
 * @author Synolia <contact@synolia.com>
 */
class PickupCreationRequest
{
    /**
     * @var string
     */
    protected $RatePickupIndicator = 'N';

    /**
     * @var \UPS\Datatype\Pickup\PickupCreationRequest\PickupDateInfo
     */
    protected $PickupDateInfo;

    /**
     * @var \UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress
     */
    protected $PickupAddress;

    /**
     * @var string
     */
    protected $AlternateAddressIndicator = 'Y';

    /**
     * @var \UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece
     */
    protected $PickupPiece;

    /**
     * @var \UPS\Datatype\Pickup\PickupCreationRequest\TotalWeight
     */
    protected $TotalWeight;

    /**
     * @var string
     */
    protected $OverweightIndicator = 'N';

    /**
     * @var string
     */
    protected $PaymentMethod = '00';

    /**
     * @var string
     */
    protected $SpecialInstruction = '';

    /**
     * @var string
     */
    protected $ReferenceNumber = '';

    /**
     * @var \UPS\Datatype\Pickup\PickupCreationRequest\Notification
     */
    protected $Notification;

    /**
     * @return string
     */
    public function getRatePickupIndicator()
    {
        return $this->RatePickupIndicator;
    }

    /**
     * @param string $ratePickupIndicator
     */
    public function setRatePickupIndicator($ratePickupIndicator)
    {
        $this->RatePickupIndicator = $ratePickupIndicator;
    }

    /**
     * @return \UPS\Datatype\Pickup\PickupCreationRequest\PickupDateInfo
     */
    public function getPickupDateInfo()
    {
        return $this->PickupDateInfo;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\PickupDateInfo $pickupDateInfo
     */
    public function setPickupDateInfo($pickupDateInfo)
    {
        $this->PickupDateInfo = $pickupDateInfo;
    }

    /**
     * @return \UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress
     */
    public function getPickupAddress()
    {
        return $this->PickupAddress;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress $pickupAddress
     */
    public function setPickupAddress($pickupAddress)
    {
        $this->PickupAddress = $pickupAddress;
    }

    /**
     * @return string
     */
    public function getAlternateAddressIndicator()
    {
        return $this->AlternateAddressIndicator;
    }

    /**
     * @param string $alternateAddressIndicator
     */
    public function setAlternateAddressIndicator($alternateAddressIndicator)
    {
        $this->AlternateAddressIndicator = $alternateAddressIndicator;
    }

    /**
     * @return \UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece
     */
    public function getPickupPiece()
    {
        return $this->PickupPiece;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece $pickupPiece
     */
    public function setPickupPiece($pickupPiece)
    {
        $this->PickupPiece = $pickupPiece;
    }

    /**
     * @return \UPS\Datatype\Pickup\PickupCreationRequest\TotalWeight
     */
    public function getTotalWeight()
    {
        return $this->TotalWeight;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\TotalWeight $totalWeight
     */
    public function setTotalWeight($totalWeight)
    {
        $this->TotalWeight = $totalWeight;
    }

    /**
     * @return string
     */
    public function getOverweightIndicator()
    {
        return $this->OverweightIndicator;
    }

    /**
     * @param string $overweightIndicator
     */
    public function setOverweightIndicator($overweightIndicator)
    {
        $this->OverweightIndicator = $overweightIndicator;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->PaymentMethod;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->PaymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getSpecialInstruction()
    {
        return $this->SpecialInstruction;
    }

    /**
     * @param string $specialInstruction
     */
    public function setSpecialInstruction($specialInstruction)
    {
        $this->SpecialInstruction = $specialInstruction;
    }

    /**
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->ReferenceNumber;
    }

    /**
     * @param string $referenceNumber
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->ReferenceNumber = $referenceNumber;
    }

    /**
     * @return \UPS\Datatype\Pickup\PickupCreationRequest\Notification
     */
    public function getNotification()
    {
        return $this->Notification;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\Notification $notification
     */
    public function setNotification($notification)
    {
        $this->Notification = $notification;
    }

    public function toArray()
    {
        return [
            "PickupCreationRequest" => [
                "RatePickupIndicator" => $this->getRatePickupIndicator(),
                "PickupDateInfo" => $this->getPickupDateInfo()->toArray(),
                "PickupAddress" => $this->getPickupAddress()->toArray(),
                "AlternateAddressIndicator" => $this->getAlternateAddressIndicator(),
                "PickupPiece" => $this->getPickupPiece()->toArray(),
                "TotalWeight" => $this->getTotalWeight()->toArray(),
                "OverweightIndicator" => $this->getOverweightIndicator(),
                "PaymentMethod" => $this->getPaymentMethod(),
                "SpecialInstruction" => $this->getSpecialInstruction(),
                "ReferenceNumber" => $this->getReferenceNumber(),
                "Notification" => $this->getNotification()->toArray()
            ]
        ];
    }
}
