<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest;

/**
 * Class PickupDateInfo
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest
 * @author Synolia <contact@synolia.com>
 */
class PickupDateInfo
{
    /**
     * @var string
     */
    protected $CloseTime = '1700';

    /**
     * @var string
     */
    protected $ReadyTime = '0900';

    /**
     * @var string
     */
    protected $PickupDate;

    /**
     * @return string
     */
    public function getCloseTime()
    {
        return $this->CloseTime;
    }

    /**
     * @param string $closeTime
     */
    public function setCloseTime($closeTime)
    {
        $this->CloseTime = $closeTime;
    }

    /**
     * @return string
     */
    public function getReadyTime()
    {
        return $this->ReadyTime;
    }

    /**
     * @param string $readyTime
     */
    public function setReadyTime($readyTime)
    {
        $this->ReadyTime = $readyTime;
    }

    /**
     * @return string
     */
    public function getPickupDate()
    {
        return $this->PickupDate;
    }

    /**
     * @param string $pickupDate
     */
    public function setPickupDate($pickupDate)
    {
        $this->PickupDate = str_replace('-', '', $pickupDate);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "CloseTime" => $this->getCloseTime(),
            "ReadyTime" => $this->getReadyTime(),
            "PickupDate" => $this->getPickupDate()
        ];
    }
}
