<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece;

/**
 * Class Item
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece
 * @author Synolia <contact@synolia.com>
 */
class Item
{
    /**
     * @var string
     */
    protected $ServiceCode;

    /**
     * @var string
     */
    protected $Quantity;

    /**
     * @var string
     */
    protected $DestinationCountryCode;

    /**
     * @var string
     */
    protected $ContainerCode = '01';

    /**
     * @return string
     */
    public function getServiceCode()
    {
        return $this->ServiceCode;
    }

    /**
     * @param string $serviceCode
     */
    public function setServiceCode($serviceCode)
    {
        $this->ServiceCode = $serviceCode;
    }

    /**
     * @return string
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }

    /**
     * @param string $quantity
     */
    public function setQuantity($quantity)
    {
        $this->Quantity = (int)$quantity;
    }

    /**
     * @return string
     */
    public function getDestinationCountryCode()
    {
        return $this->DestinationCountryCode;
    }

    /**
     * @param string $destinationCountryCode
     */
    public function setDestinationCountryCode($destinationCountryCode)
    {
        $this->DestinationCountryCode = $destinationCountryCode;
    }

    /**
     * @return string
     */
    public function getContainerCode()
    {
        return $this->ContainerCode;
    }

    /**
     * @param string $containerCode
     */
    public function setContainerCode($containerCode)
    {
        $this->ContainerCode = $containerCode;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "ServiceCode" => $this->getServiceCode(),
            "Quantity" => (string)$this->getQuantity(),
            "DestinationCountryCode" => $this->getDestinationCountryCode(),
            "ContainerCode" => $this->getContainerCode()
        ];
    }
}
