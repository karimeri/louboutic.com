<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest;

/**
 * Class TotalWeight
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest
 * @author Synolia <contact@synolia.com>
 */
class TotalWeight
{
    /**
     * @var string
     */
    protected $Weight;

    /**
     * @var string
     */
    protected $UnitOfMeasurement;

    /**
     * @return string
     */
    public function getWeight()
    {
        return $this->Weight;
    }

    /**
     * @param string $weight
     */
    public function setWeight($weight)
    {
        $this->Weight = $weight;
    }

    /**
     * @return string
     */
    public function getUnitOfMeasurement()
    {
        return $this->UnitOfMeasurement;
    }

    /**
     * @param string $unitOfMeasurement
     */
    public function setUnitOfMeasurement($unitOfMeasurement)
    {
        $this->UnitOfMeasurement = $unitOfMeasurement;
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "Weight" => (string)$this->getWeight(),
            "UnitOfMeasurement" => $this->getUnitOfMeasurement()
        ];
    }
}
