<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest;

/**
 * Class Notification
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest
 * @author Synolia <contact@synolia.com>
 */
class Notification
{
    /**
     * @var string
     */
    protected $ConfirmationEmailAddress;

    /**
     * @var string
     */
    protected $UndeliverableEmailAddress;

    /**
     * @return string
     */
    public function getConfirmationEmailAddress()
    {
        return $this->ConfirmationEmailAddress;
    }

    /**
     * @param string $confirmationEmailAddress
     */
    public function setConfirmationEmailAddress($confirmationEmailAddress)
    {
        $this->ConfirmationEmailAddress = $confirmationEmailAddress;
    }

    /**
     * @return string
     */
    public function getUndeliverableEmailAddress()
    {
        return $this->UndeliverableEmailAddress;
    }

    /**
     * @param string $undeliverableEmailAddress
     */
    public function setUndeliverableEmailAddress($undeliverableEmailAddress)
    {
        $this->UndeliverableEmailAddress = $undeliverableEmailAddress;
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "ConfirmationEmailAddress" => $this->getConfirmationEmailAddress(),
            "UndeliverableEmailAddress" => $this->getUndeliverableEmailAddress()
        ];
    }
}
