<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest;

/**
 * Class PickupAddress
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest
 * @author Synolia <contact@synolia.com>
 */
class PickupAddress
{
    /**
     * @var string
     */
    protected $CompanyName = '';

    /**
     * @var string
     */
    protected $ContactName = '';

    /**
     * @var string
     */
    protected $AddressLine;

    /**
     * @var string
     */
    protected $Room = '';

    /**
     * @var string
     */
    protected $Floor = '';

    /**
     * @var string
     */
    protected $City;

    /**
     * @var string
     */
    protected $StateProvince;

    /**
     * @var string
     */
    protected $Urbanization = '';

    /**
     * @var string
     */
    protected $PostalCode;

    /**
     * @var string
     */
    protected $CountryCode;

    /**
     * @var string
     */
    protected $ResidentialIndicator = 'Y';

    /**
     * @var string
     */
    protected $PickupPoint = '';

    /**
     * @var \UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress\Phone
     */
    protected $phone;

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->CompanyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->CompanyName = $companyName;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->ContactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName($contactName)
    {
        $this->ContactName = $contactName;
    }

    /**
     * @return string
     */
    public function getAddressLine()
    {
        return $this->AddressLine;
    }

    /**
     * @param string $pickupDate
     */
    public function setAddressLine($pickupDate)
    {
        $this->AddressLine = $pickupDate;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->Room;
    }

    /**
     * @param string $room
     */
    public function setRoom($room)
    {
        $this->Room = $room;
    }

    /**
     * @return string
     */
    public function getFloor()
    {
        return $this->Floor;
    }

    /**
     * @param string $floor
     */
    public function setFloor($floor)
    {
        $this->Floor = $floor;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->City = $city;
    }

    /**
     * @return string
     */
    public function getStateProvince()
    {
        return $this->StateProvince;
    }

    /**
     * @param string $stateProvince
     */
    public function setStateProvince($stateProvince)
    {
        $this->StateProvince = $stateProvince;
    }

    /**
     * @return string
     */
    public function getUrbanization()
    {
        return $this->Urbanization;
    }

    /**
     * @param string $urbanization
     */
    public function setUrbanization($urbanization)
    {
        $this->Urbanization = $urbanization;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->PostalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->PostalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->CountryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->CountryCode = $countryCode;
    }

    /**
     * @return string
     */
    public function getResidentialIndicator()
    {
        return $this->ResidentialIndicator;
    }

    /**
     * @param string $residentialIndicator
     */
    public function setResidentialIndicator($residentialIndicator)
    {
        $this->ResidentialIndicator = $residentialIndicator;
    }

    /**
     * @return string
     */
    public function getPickupPoint()
    {
        return $this->PickupPoint;
    }

    /**
     * @param string $pickupPoint
     */
    public function setPickupPoint($pickupPoint)
    {
        $this->PickupPoint = $pickupPoint;
    }

    /**
     * @return \UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress\Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress\Phone $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "CompanyName" => $this->getCompanyName(),
            "ContactName" => $this->getContactName(),
            "AddressLine" => $this->getAddressLine(),
            "Room" => $this->getRoom(),
            "Floor" => $this->getFloor(),
            "City" => $this->getCity(),
            "StateProvince" => $this->getStateProvince(),
            "Urbanization" => $this->getUrbanization(),
            "PostalCode" => $this->getPostalCode(),
            "CountryCode" => $this->getCountryCode(),
            "ResidentialIndicator" => $this->getResidentialIndicator(),
            "PickupPoint" => $this->getPickupPoint(),
            "Phone" => $this->getPhone()->toArray()
        ];
    }
}
