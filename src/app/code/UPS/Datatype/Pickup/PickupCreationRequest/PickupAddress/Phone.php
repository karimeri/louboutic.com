<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress;

/**
 * Class Phone
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest\PickupAddress
 * @author Synolia <contact@synolia.com>
 */
class Phone
{
    /**
     * @var string
     */
    protected $Number;

    /**
     * @var string
     */
    protected $Extension = '';

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->Number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->Number = $number;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->Extension;
    }

    /**
     * @param string $number
     */
    public function setExtension($number)
    {
        $this->Extension = $number;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            "Number" => $this->getNumber(),
            "Extension" => $this->getExtension()
        ];
    }
}
