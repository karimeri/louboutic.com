<?php

namespace UPS\Datatype\Pickup\PickupCreationRequest;

/**
 * Class PickupPiece
 *
 * @package UPS\Datatype\Pickup\PickupCreationRequest
 * @author Synolia <contact@synolia.com>
 */
class PickupPiece
{
    /**
     * @var array
     */
    protected $Items = [];

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->Items;
    }

    /**
     * @param \UPS\Datatype\Pickup\PickupCreationRequest\PickupPiece\Item $item
     */
    public function addItem($item)
    {
        $this->Items[] = $item;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->Items = $items;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [];
        foreach ($this->getItems() as $item) {
            $array[] = $item->toArray();
        }

        return $array;
    }
}
