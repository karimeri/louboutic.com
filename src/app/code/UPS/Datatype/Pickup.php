<?php

namespace UPS\Datatype\PickupCreationRequest;

/**
 * Class Pickup
 *
 * @package UPS\Datatype
 * @author Synolia <contact@synolia.com>
 */
class Pickup
{
    /**
     * @var \UPS\Datatype\PickupCreationRequest
     */
    protected $PickupCreationRequest;

    /**
     * @return \UPS\Datatype\PickupCreationRequest
     */
    public function getPickupCreationRequest()
    {
        return $this->PickupCreationRequest;
    }

    /**
     * @param \UPS\Datatype\PickupCreationRequest $pickupCreationRequest
     */
    public function setPickupCreationRequest($pickupCreationRequest)
    {
        $this->PickupCreationRequest = $pickupCreationRequest;
    }
}
