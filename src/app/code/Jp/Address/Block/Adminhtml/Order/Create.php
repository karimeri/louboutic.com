<?php

namespace Jp\Address\Block\Adminhtml\Order;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Sales\Block\Adminhtml\Order\Create as BaseCreate;
use Jp\Address\Helper\Data;

/**
 * Class Create
 *
 * @package Jp\Address\Block\Adminhtml\Order
 * @author Synolia <contact@synolia.com>
 */
class Create extends BaseCreate
{
    /**
     * @var \Jp\Address\Helper\Data
     */
    protected $helper;

    /**
     * Create constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Jp\Address\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $sessionQuote, $data);
    }

    /**
     * Constructor
     *
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    //phpcs:ignore
    protected function _construct()
    {
        parent::_construct();
        if ($this->helper->isJapan()) {
            $this->buttonList->update('save', 'onclick', 'order.checkAddressBeforeSubmit()');
        } else {
            $this->buttonList->update('save', 'onclick', 'order.submit()');
        }
    }
}
