<?php

namespace Jp\Address\Model;

use Magento\Framework\UrlInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Jp\Address\Helper\Data;

/**
 * Class AdditionalConfigProvider
 * @package Jp\Address\Model
 * @author Synolia <contact@synolia.com>
 */
class AdditionalConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Jp\Address\Helper\Data
     */
    protected $helper;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * AdditionalConfigProvider constructor.
     * @param \Jp\Address\Helper\Data $helper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     */
    public function __construct(
        Data $helper,
        UrlInterface $urlBuilder
    ) {
        $this->helper = $helper;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $output['isJapan'] = $this->helper->isJapan();

        $baseUrl = $this->urlBuilder->getBaseUrl();
        $output['japanAddressApi'] = $baseUrl . 'address/api/getfulladdress/postal_code/%s/language/jp';

        return $output;
    }
}
