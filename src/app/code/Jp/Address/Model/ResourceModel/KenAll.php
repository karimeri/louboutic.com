<?php

namespace Jp\Address\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

use Jp\Address\Model\KenAll as Model;

/**
 * Class Data
 *
 * @package Jp\Address\Model\ResourceModel
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD)
 */
class KenAll extends AbstractDb
{
    // @codingStandardsIgnoreLine
    protected function _construct()
    {
        $this->_init(Model::TABLE_NAME, 'id');
    }
}
