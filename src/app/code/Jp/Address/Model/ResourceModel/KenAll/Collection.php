<?php

namespace Jp\Address\Model\ResourceModel\KenAll;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

use Jp\Address\Model\KenAll;
use Jp\Address\Model\ResourceModel\KenAll as ResourceModel;

/**
 * Class Collection
 *
 * @package Jp\Address\Model\ResourceModel\KenAll
 * @author  Synolia <contact@synolia.com>
 * @SuppressWarnings(PHPMD)
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource collection
     *
     * @return void
     */
    // @codingStandardsIgnoreLine
    protected function _construct()
    {
        $this->_init(KenAll::class, ResourceModel::class);
    }
}
