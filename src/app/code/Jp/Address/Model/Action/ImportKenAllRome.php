<?php

namespace Jp\Address\Model\Action;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

use Jp\Address\Model\Repository\KenAllRepository;

/**
 * Class ImportKenAllRome
 *
 * @package Jp\Address\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class ImportKenAllRome implements ActionInterface
{
    /**
     * @var KenAllRepository
     */
    protected $kenAllRepository;

    /**
     * Import constructor.
     *
     * @param KenAllRepository $kenAllRepository
     */
    public function __construct(
        KenAllRepository $kenAllRepository
    ) {
        $this->kenAllRepository = $kenAllRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $this->output = $consoleOutput;

        $kenAll = [];

        if (isset($data[0]['content'])) {
            foreach ($data[0]['content'] as $row) {
                $kenAll[] = [
                    'postal_code' => $row['2'],
                    'prefecture' => $row['5'],
                    'city' => $row['4'],
                    'address' => $row['3'],
                    'language' => 'EN',
                ];
            }

            $this->kenAllRepository->insertData($kenAll);
        }


        return $data;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function decodeValue($value)
    {
        return str_replace('-', ' ', $value);
    }
}
