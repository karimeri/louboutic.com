<?php

namespace Jp\Address\Model\Action;

use Synolia\Sync\Console\ConsoleOutput;
use Synolia\Sync\Model\Action\ActionInterface;

use Jp\Address\Model\Repository\KenAllRepository;

/**
 * Class ImportKenAll
 *
 * @package Jp\Address\Model\Action
 * @author  Synolia <contact@synolia.com>
 */
class ImportKenAll implements ActionInterface
{
    /**
     * @var KenAllRepository
     */
    protected $kenAllRepository;

    /**
     * Import constructor.
     *
     * @param KenAllRepository $kenAllRepository
     */
    public function __construct(
        KenAllRepository $kenAllRepository
    ) {
        $this->kenAllRepository = $kenAllRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute(
        array $params,
        array $data,
        $flowCode,
        ConsoleOutput $consoleOutput
    ) {
        $this->output = $consoleOutput;

        $kenAll = [];
        $region = [];
        $processedRegion = [];

        if (isset($data[0]['content'])) {
            foreach ($data[0]['content'] as $row) {
                $kenAll[] = [
                    'postal_code' => $row['3'],
                    'prefecture' => $row['7'],
                    'city' => $row['8'],
                    'address' => $row['9'],
                    'language' => 'JP',
                ];

                if (!isset($processedRegion[$row['7']])) {
                    $processedRegion[$row['7']] = 1;

                    $region[] = [
                        'country_id' => 'JP',
                        'code' => sprintf('JP_%s', $row['7']),
                        'default_name' => $row['7'],
                    ];
                }
            }

            $this->kenAllRepository->insertData($kenAll);
            $this->kenAllRepository->insertRegions($region);
        }

        return $data;
    }
}
