<?php

namespace Jp\Address\Model\Action;

use Jp\Address\Model\Repository\KenAllRepository;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\App\State;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Project\Erp\Helper\Config\Columbus\Customer as HelperConfig;
use Project\Erp\Helper\ErpCustomerId as Helper;
use Project\Wms\Helper\Config as WmsHelper;

/**
 * Class GetColumbusExportCustomerData
 * @package Jp\Address\Model\Action
 * @author Synolia <contact@synolia.com>
 */
class GetColumbusExportCustomerData extends \Project\Erp\Model\Action\GetColumbusExportCustomerData
{
    /**
     * @var \Jp\Address\Model\Repository\KenAllRepository
     */
    protected $kenAllRepository;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * GetColumbusExportCustomerData constructor.
     *
     * @param \Project\Erp\Helper\ErpCustomerId $helper
     * @param \Project\Wms\Helper\Config $wmsHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Project\Erp\Helper\Config\Columbus\Customer $helperConfig
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Magento\Framework\App\State $state
     * @param \Jp\Address\Model\Repository\KenAllRepository $kenAllRepository
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        Helper $helper,
        WmsHelper $wmsHelper,
        TimezoneInterface $timezone,
        HelperConfig $helperConfig,
        CustomerRegistry $customerRegistry,
        State $state,
        KenAllRepository $kenAllRepository,
        EnvironmentManager $environmentManager
    ) {
        parent::__construct(
            $helper,
            $wmsHelper,
            $timezone,
            $helperConfig,
            $customerRegistry,
            $state
        );

        $this->kenAllRepository = $kenAllRepository;
        $this->environmentManager = $environmentManager;
    }

    /**
     * @inheritdoc
     */
    protected function getCustomerLine($order, $customer)
    {
        $line = parent::getCustomerLine($order, $customer);

        if ($this->environmentManager->getEnvironment() === Environment::JP) {
            try {
                /** @var \Jp\Address\Model\KenAll $jpAddress */
                $jpAddress = $this->kenAllRepository->find($order->getData('japanese_address_id'));
                $jpAddressEn = $this->kenAllRepository->findOneBy(
                    ['postal_code' => $jpAddress->getPostalCode()]
                );

                // Address Line 1
                $line[12] = $jpAddressEn->getAddress();
                // Address Line 2
                $line[13] = '';
                // Address Line 3
                $line[14] = '';
                // Address Line 4
                $line[15] = $jpAddressEn->getPrefecture();
                // City
                $line[16] = $jpAddressEn->getCity();
                // Postal Code
                $line[17] = $jpAddressEn->getPostalCode();
                // City
                $line[18] = $jpAddressEn->getCity();
            } catch (\Exception $e) {
                // custom address => do nothing
            }
        }

        return $line;
    }
}
