<?php

namespace Jp\Address\Model;

use Magento\Customer\Model\Customer\DataProvider as BaseDataProvider;
use Magento\Framework\UrlInterface;
use Project\Core\Manager\EnvironmentManager;
use Magento\Customer\Model\FileProcessorFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Eav\Model\Config;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool;
use Magento\Ui\DataProvider\EavValidationRules;

/**
 * Class DataProvider
 *
 * @package Jp\Address\Model\Slide
 * @author Synolia <contact@synolia.com>
 */
class DataProvider extends BaseDataProvider
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Jp\Address\Helper\Data
     */
    protected $helper;

    /**
     * Constructor
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param EavValidationRules $eavValidationRules
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param Config $eavConfig
     * @param FilterPool $filterPool
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Jp\Address\Helper\Data $helper
     * @param FileProcessorFactory $fileProcessorFactory
     * @param array $meta
     * @param array $data
     * @param ContextInterface $context
     * @param bool $allowToShowHiddenAttributes
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        EavValidationRules $eavValidationRules,
        CustomerCollectionFactory $customerCollectionFactory,
        Config $eavConfig,
        FilterPool $filterPool,
        UrlInterface $urlBuilder,
        \Jp\Address\Helper\Data $helper,
        FileProcessorFactory $fileProcessorFactory = null,
        array $meta = [],
        array $data = [],
        ContextInterface $context = null,
        $allowToShowHiddenAttributes = true
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $eavValidationRules,
            $customerCollectionFactory,
            $eavConfig,
            $filterPool,
            $fileProcessorFactory,
            $meta,
            $data,
            $context,
            $allowToShowHiddenAttributes
        );

        $this->urlBuilder = $urlBuilder;
        $this->helper = $helper;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        parent::getData();

        $this->loadedData['isJapan'] = $this->helper->isJapan();

        $baseUrl = $this->urlBuilder->getBaseUrl();
        $this->loadedData['japanAddressApi'] = $baseUrl . 'address/api/getfulladdress/postal_code/%s/language/jp';

        foreach ($this->loadedData as &$customer) {
            if (isset($customer['customer'])) {
                $customer['customer'] = array_merge(
                    $customer['customer'],
                    [
                        'isJapan' => $this->loadedData['isJapan'],
                        'japanAddressApi' => $this->loadedData['japanAddressApi'],
                    ]
                );
            }
        }
        unset($customer);

        return $this->loadedData;
    }
}
