<?php

namespace Jp\Address\Model\Repository;

use Jp\Address\Model\ResourceModel\KenAll as KenAllResourceModel;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Exception\NoSuchEntityException;

use Synolia\Standard\Exception\NestedConnectionException;
use Synolia\Standard\Model\Repository\AbstractRepository;

use Jp\Address\Model\KenAll;
use Jp\Address\Model\KenAllFactory;
use Jp\Address\Model\ResourceModel\KenAll\Collection;
use Jp\Address\Model\ResourceModel\KenAll\CollectionFactory;

/**
 * Class KenAllRepository
 *
 * @package Jp\Address\Model\Repository
 * @author  Synolia <contact@synolia.com>
 */
class KenAllRepository extends AbstractRepository
{
    const ID_FIELD_NAME = 'id';

    /**
     * @var KenAllFactory
     */
    protected $factory;

    /**
     * @var CollectionAllFactory
     */
    protected $collectionFactory;

    /**
     * @var KenAllResourceModel
     */
    protected $kenAllResourceModel;

    /**
     * KenAllRepository constructor.
     *
     * @param Context             $context
     * @param KenAllFactory       $factory
     * @param CollectionFactory   $collectionFactory
     * @param KenAllResourceModel $kenAllResourceModel
     * @param string|null         $connectionName
     */
    public function __construct(
        Context $context,
        KenAllFactory $factory,
        CollectionFactory $collectionFactory,
        KenAllResourceModel $kenAllResourceModel,
        $connectionName = null
    ) {
        $this->factory             = $factory;
        $this->collectionFactory   = $collectionFactory;
        $this->kenAllResourceModel = $kenAllResourceModel;

        parent::__construct($context, $connectionName);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    protected function getIdFieldName()
    {
        return $this->kenAllResourceModel->getIdFieldName();
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    protected function getTableName()
    {
        return $this->kenAllResourceModel->getMainTable();
    }

    /**
     * @param array $params
     *
     * @return KenAll|null
     * @throws NestedConnectionException
     * @throws NoSuchEntityException
     */
    public function findOneBy($params)
    {
        $data = parent::findOneBy($params);

        if (!isset($data['id'])) {
            throw new NoSuchEntityException(__('No Ken All found with these parameters.'));
        }

        $kenAll = $this->factory->create();
        $kenAll->setData($data);

        return $kenAll;
    }

    /**
     * @return Model
     */
    public function create()
    {
        return $this->factory->create();
    }

    /**
     * @param \Magento\Framework\DataObject $kenAll
     *
     * @return AbstractRepository
     */
    public function save($kenAll)
    {
        return parent::persistToDb($kenAll);
    }

    /**
     * @return array
     */
    public function getPostalCodes()
    {
        /** @var Collection $kenAll */
        $collection = $this->collectionFactory->create();

        // @codingStandardsIgnoreLine
        $collection->addFieldToSelect(KenAll::POSTAL_CODE)->distinct(true);

        return $collection->toArray();
    }

    /**
     * @param string      $postalCode
     * @param string|null $language
     *
     * @return array
     */
    public function getPrefectures($postalCode, $language = null)
    {
        /** @var Collection $kenAll */
        $collection = $this->collectionFactory->create();

        $collection->addFieldToSelect(KenAll::PREFECTURE)->addFieldToFilter(KenAll::POSTAL_CODE, $postalCode);

        if ($language) {
            $collection->addFieldToFilter(KenAll::LANGUAGE, $language);
        }

        // @codingStandardsIgnoreLine
        $collection->distinct(true);

        return $collection->toArray();
    }

    /**
     * @param string      $postalCode
     * @param string      $prefecture
     * @param string|null $language
     *
     * @return array
     */
    public function getCities($postalCode, $prefecture, $language = null)
    {
        /** @var Collection $kenAll */
        $collection = $this->collectionFactory->create();

        $collection->addFieldToSelect(KenAll::CITY)
            ->addFieldToFilter(KenAll::POSTAL_CODE, $postalCode)
            ->addFieldToFilter(KenAll::PREFECTURE, $prefecture);

        if ($language) {
            $collection->addFieldToFilter(KenAll::LANGUAGE, $language);
        }

        // @codingStandardsIgnoreLine
        $collection->distinct(true);

        return $collection->toArray();
    }

    /**
     * @param string      $postalCode
     * @param string      $prefecture
     * @param string      $city
     * @param string|null $language
     *
     * @return array
     */
    public function getAddresses($postalCode, $prefecture, $city, $language = null)
    {
        /** @var Collection $kenAll */
        $collection = $this->collectionFactory->create();

        $collection->addFieldToSelect(KenAll::ADDRESS)
            ->addFieldToFilter(KenAll::POSTAL_CODE, $postalCode)
            ->addFieldToFilter(KenAll::PREFECTURE, $prefecture)
            ->addFieldToFilter(KenAll::CITY, $city);

        if ($language) {
            $collection->addFieldToFilter(KenAll::LANGUAGE, $language);
        }

        // @codingStandardsIgnoreLine
        $collection->distinct(true);

        return $collection->toArray();
    }

    /**
     * @param string|null $postalCode
     * @param string|null $prefecture
     * @param string|null $city
     * @param string|null $language
     *
     * @return array
     */
    public function getFullAddress($postalCode, $prefecture = null, $city = null, $language = null)
    {
        /** @var Collection $kenAll */
        $collection = $this->collectionFactory->create();

        $collection->addFieldToSelect(KenAll::PREFECTURE)
            ->addFieldToSelect(KenAll::CITY)
            ->addFieldToSelect(KenAll::ADDRESS)
            ->addFieldToFilter(KenAll::POSTAL_CODE, $postalCode);

        if ($prefecture) {
            $collection->addFieldToFilter(KenAll::PREFECTURE, $prefecture);
        }

        if ($city) {
            $collection->addFieldToFilter(KenAll::CITY, $city);
        }

        if ($language) {
            $collection->addFieldToFilter(KenAll::LANGUAGE, $language);
        }

        // @codingStandardsIgnoreLine
        $collection->distinct(true);

        return $collection->toArray();
    }

    /**
     * @param array $data
     */
    public function insertData($data)
    {
        $this->kenAllResourceModel->getConnection()->insertOnDuplicate(
            KenAll::TABLE_NAME,
            $data,
            [
                'postal_code',
                'prefecture',
                'city',
                'address',
                'language',
            ]
        );
    }

    /**
     * @param array $data
     *
     */
    public function insertRegions($data)
    {
        $connection = $this->kenAllResourceModel->getConnection();

        foreach ($data as $regionData) {
            $select = $connection->select()
                ->from(
                    'directory_country_region',
                    ['region_id']
                )
                ->where('default_name = ?', $regionData['default_name'])
                ->where('country_id = ?', $regionData['country_id']);

            try  {
                if (!$connection->query($select)->fetch()) {
                    $this->kenAllResourceModel->getConnection()->insertOnDuplicate(
                        'directory_country_region',
                        $regionData,
                        [
                            'country_id',
                            'code',
                            'default_name',
                        ]
                    );
                    $regionId = $connection->lastInsertId('directory_country_region');

                    $this->kenAllResourceModel->getConnection()->insertOnDuplicate(
                        'directory_country_region_name',
                        [
                            'locale' => 'jp_JA',
                            'region_id' => $regionId,
                            'name' => $regionData['default_name']
                        ]
                    );

                }
            } catch (\Exception $e) {
                // do nothing
            }
        }
    }

    /**
     * @return mixed
     * @throws \Zend_Db_Statement_Exception
     */
    public function getNextRegionId()
    {
        $select = $this->kenAllResourceModel->getConnection()->select()
            ->from(
                'directory_country_region',
                ['region_id']
            )
            ->order('region_id DESC');

        //phpcs:ignore
        return $this->kenAllResourceModel->getConnection()->query($select)->fetch(\PDO::FETCH_COLUMN) + 1;
    }
}
