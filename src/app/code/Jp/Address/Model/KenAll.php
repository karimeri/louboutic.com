<?php

namespace Jp\Address\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class KenAll
 *
 * @package Jp\Address\Model
 * @author  Synolia <contact@synolia.com>
 */
class KenAll extends AbstractModel
{
    const TABLE_NAME  = 'japanese_address';
    const ID          = 'id';
    const POSTAL_CODE = 'postal_code';
    const PREFECTURE  = 'prefecture';
    const CITY        = 'city';
    const ADDRESS     = 'address';
    const LANGUAGE    = 'language';

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->getData(self::POSTAL_CODE);
    }

    /**
     * @return string
     */
    public function getPrefecture()
    {
        return $this->getData(self::PREFECTURE);
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->getData(self::LANGUAGE);
    }
}
