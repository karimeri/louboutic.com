<?php

namespace Jp\Address\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class Data
 *
 * @package Jp\CashOnDelivery\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Data extends AbstractHelper
{
    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * Config constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     */
    public function __construct(
        Context $context,
        EnvironmentManager $environmentManager
    ) {
        $this->environmentManager = $environmentManager;

        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function isJapan()
    {
        return $this->environmentManager->getEnvironment() === Environment::JP;
    }
}
