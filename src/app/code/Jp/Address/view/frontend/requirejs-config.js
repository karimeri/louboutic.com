var config = {
    map: {
        '*': {
            postcodeError: 'Jp_Address/js/postcode-error'
        }
    },
    config: {
        mixins: {
            'Magento_Ui/js/form/element/post-code': {
                'Jp_Address/js/form/element/post-code-mixin': true
            }
        }
    }
};
