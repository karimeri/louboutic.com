require([
    'jquery',
    'mage/template',
    'mage/translate',
    'jquery/ui'
], function(
    $,
    mageTemplate,
    $t
) {
    'use strict';

    $.widget('project.postcodeError', {

        options: {
            jpPostcode1: '#jppostcode1',
            jpPostcode2: '#jppostcode2',
            jpPostcodeError: '#jppostcode-error',
            errorClass: 'mage-error',
            fieldError: '.field-error'
        },

        $jpPostcode1: {},
        $jpPostcode2: {},
        $jpPostcodeError: {},

        _initVars: function() {
            this.$jpPostcode1 = $(this.options.jpPostcode1);
            this.$jpPostcode2 = $(this.options.jpPostcode2);
            this.$jpPostcodeError = $(this.options.jpPostcodeError);
        },

        /**
         * Add post code error
         */
        add: function () {
            var html = '<div for="jppostcode2" ' +
                '            generated="true" ' +
                '            class="mage-error" ' +
                '            id="jppostcode-error"><%- data.message %></div>',
                template = mageTemplate(html),
                data = {
                    message: $t('Enter the zip code')
                },
                message = template({data: data});

            this._initVars();

            this.$jpPostcode1.addClass(this.options.errorClass);
            this.$jpPostcode2.addClass(this.options.errorClass)
                .after(message);
        },

        /**
         * Remove post code error
         */
        remove: function () {
            this._initVars();

            var $field = this.$jpPostcode1.closest('.field');

            this.$jpPostcode1.removeClass(this.options.errorClass);
            this.$jpPostcode2.removeClass(this.options.errorClass);
            this.$jpPostcodeError.remove();

            if($field.hasClass('_error')) {
                $field.removeClass('_error');
                $field.find(this.options.fieldError).remove();
            }
        }
    });

    return $.project.postcodeError;

});


