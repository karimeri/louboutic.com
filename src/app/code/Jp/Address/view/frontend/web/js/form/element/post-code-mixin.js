define([
    'jquery',
    'Magento_Ui/js/model/messageList',
    'mage/translate',
    'Magento_Ui/js/modal/alert',
    'postcodeError'
], function (
    $,
    messageList,
    $t,
    alert
) {
    'use strict';

    var mixin = {
        jpPostcode1: '#jppostcode1',
        jpPostcode2: '#jppostcode2',

        $jpPostcode1: {},
        $jpPostcode2: {},

        initVars: function() {
            this.$jpPostcode1 = $(this.jpPostcode1);
            this.$jpPostcode2 = $(this.jpPostcode2);
        },

        /**
         * @returns bool
         */
        isJapan: function () {
            return window.checkoutConfig.isJapan;
        },

        /**
         *
         * @param postcode
         * @returns {*}
         */
        getAddressApi: function (postcode) {
            return window.checkoutConfig.japanAddressApi.replace('%s', postcode);
        },

        autocompleteAddress: function () {
            this.initVars();

            var $jpPostcode1 = this.$jpPostcode1.val(),
                $jpPostcode2 = this.$jpPostcode2.val();

            if ($jpPostcode1 && $jpPostcode2) {
                this.ajaxCallback($jpPostcode1, $jpPostcode2);
            }
            else {
                $('body').postcodeError().postcodeError('add');
            }
        },

        ajaxCallback: function(jppostcode1, jppostcode2) {
            var postcode = jppostcode1 + jppostcode2;

            $('body').postcodeError().postcodeError('remove');

            $.ajax({
                showLoader: true,
                url: this.getAddressApi(postcode),
                type: 'GET'
            }).done(this.fillAddress.bind(this, postcode));
        },

        fillAddress: function(postcode, data) {
            if (data.items.length === 1) {
                var prefecture = data.items[0]['prefecture'];

                $('input[name*="street[0]"]').val(data.items[0]['address']).keyup();
                $('input[name*="city"]').val(data.items[0]['city']).keyup();
                $('input[name*="region"]').val(prefecture).keyup();
                $('select[name*="region_id"] option[data-title='+ prefecture +']').prop('selected', true).change();
                $('input[name*="postcode"]').val(postcode).change();
            } else {
                alert({
                    content: $t('No address is matching the zip code.')
                });

                $('input[name*="postcode"]').show();

                return false;
            }

            return true;
        }
    };

    return function (target) { // target == Result that Magento_Ui/view/base/web/js/form/element/post-code returns.
        return target.extend(mixin);
    };
});
