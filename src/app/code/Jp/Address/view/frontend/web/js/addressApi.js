define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/modal/alert',
    'jquery/ui',
    'postcodeError'
], function (
    $,
    $t,
    alert
) {
    'use strict';

    function main(config) {
        $(document).on('click', '.jp-address-fill', function (event) {
            var $jpPostcode1 = $('#jppostcode1'),
                $jpPostcode2 = $('#jppostcode2'),
                jpPostcode1Value1 = $jpPostcode1.val(),
                jpPostcode1Value2 = $jpPostcode2.val(),
                postcode = jpPostcode1Value1 + jpPostcode1Value2,
                url = config.baseUrl + 'address/api/getfulladdress/postal_code/' + postcode + '/language/jp',
                postcodeErrorFunction = $('body').postcodeError();

            function fillAddress(data) {
                if (data.items.length === 1) {
                    var prefecture = data.items[0]['prefecture'];

                    $('#street_1').val(data.items[0]['address']);
                    $('#city').val(data.items[0]['city']);
                    $('#region').val(prefecture);
                    $('#region_id option:contains('+ prefecture +')').attr('selected', true);
                    $('#zip').val(postcode);
                } else {
                    alert({
                        content: $t('No address is matching the zip code.')
                    });

                    return false;
                }

                return true;
            }

            event.preventDefault();

            if (!jpPostcode1Value1 || !jpPostcode1Value2) {
                postcodeErrorFunction.postcodeError('add');
                return;
            }

            /* Remove error message */
            postcodeErrorFunction.postcodeError('remove');

            $.ajax({
                showLoader: true,
                url: url,
                type: 'GET'
            }).done(fillAddress.bind(this));
        });
    }

    return main;
});
