<?php

namespace Jp\Address\Setup\Upgrade;

use Jp\Address\Model\KenAll;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class SchemaUpgrade102
 * @package Jp\Address\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class SchemaUpgrade102
{
    const TABLE_REGION = 'directory_country_region';
    const TABLE_REGION_NAME = 'directory_country_region_name';

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public static function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($setup->getConnection()->isTableExists($setup->getTable(KenAll::TABLE_NAME))) {
            try {
                // clear table
                $setup->getConnection()->delete(KenAll::TABLE_NAME);
                //add unique index for japanese_address table
                $setup->getConnection()->addIndex(
                    $setup->getTable(KenAll::TABLE_NAME),
                    $setup->getIdxName(
                        $setup->getTable(KenAll::TABLE_NAME),
                        ['postal_code', 'prefecture', 'city', 'address', 'language'],
                        AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['postal_code', 'prefecture', 'city', 'address', 'language'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                );
            } catch (\Exception $e) {
                //do nothing
            }
        }
    }
}
