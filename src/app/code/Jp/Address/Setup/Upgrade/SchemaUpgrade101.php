<?php

namespace Jp\Address\Setup\Upgrade;

use Jp\Address\Model\KenAll;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class SchemaUpgrade101
 * @package Jp\Address\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class SchemaUpgrade101
{
    const TABLE_ORDER = 'sales_order';

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public static function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $columnExists = $setup->getConnection()->tableColumnExists(
            $setup->getTable(self::TABLE_ORDER),
            'japanese_address_id'
        );

        if (!$columnExists) {
            $setup->getConnection()->addColumn(
                $setup->getTable(self::TABLE_ORDER),
                'japanese_address_id',
                [
                    'type' => Table::TYPE_INTEGER,
                    'unsigned' => true,
                    'nullable' => true,
                    'comment' => 'Japanese Address',
                ]
            );
            $setup->getConnection()->addForeignKey(
                $setup->getFkName(self::TABLE_ORDER, 'entity_id', KenAll::TABLE_NAME, 'entity_id'),
                self::TABLE_ORDER,
                'japanese_address_id',
                $setup->getTable(KenAll::TABLE_NAME),
                'id',
                Table::ACTION_CASCADE
            );
        }
    }
}
