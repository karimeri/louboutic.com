<?php

namespace Jp\Address\Setup;

use Jp\Address\Setup\Upgrade\SchemaUpgrade101;
use Jp\Address\Setup\Upgrade\SchemaUpgrade102;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Class UpgradeSchema
 * @package Jp\Address\Setup
 * @author Synolia <contact@synolia.com>
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            SchemaUpgrade101::upgrade($setup, $context);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            SchemaUpgrade102::upgrade($setup, $context);
        }
    }
}
