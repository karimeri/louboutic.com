<?php

namespace Jp\Address\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Jp\Address\Model\KenAll;
use Project\Core\Model\Environment;
use Project\Core\Manager\EnvironmentManager;

/**
 * Class InstallData
 * @package   Jp\DeliveryDate\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var ConsoleOutput
     */
    protected $consoleOutput;

    public function __construct(
        EnvironmentManager $environmentManager,
        ConsoleOutput $consoleOutput
    ) {
        $this->environmentManager = $environmentManager;
        $this->consoleOutput      = $consoleOutput;
    }

    /**
     * {@inheritdoc}
     * @throws \Zend_Db_Exception
     * @throws \RuntimeException
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if ($this->environmentManager->getEnvironment() !== Environment::JP) {
            $this->consoleOutput->writeln('Install Address JP ignore');
            $setup->endSetup();

            return;
        }

        if (!$setup->getConnection()->isTableExists($setup->getTable(KenAll::TABLE_NAME))) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable(KenAll::TABLE_NAME)
            )->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )->addColumn(
                'postal_code',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false],
                'Postal Code'
            )->addColumn(
                'prefecture',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Prefecture'
            )->addColumn(
                'city',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'City'
            )->addColumn(
                'address',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Address'
            )->addColumn(
                'language',
                Table::TYPE_TEXT,
                2,
                ['nullable' => false],
                'Language'
            )
            ;

            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }
}
