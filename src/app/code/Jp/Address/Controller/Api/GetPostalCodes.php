<?php

namespace Jp\Address\Controller\Api;

/**
 * Class GetPostalCodes
 *
 * @package Jp\Address\Controller\Api
 * @author  Synolia <contact@synolia.com>
 */
class GetPostalCodes extends AbstractAction
{
    /**
     * @return string
     */
    public function execute()
    {
        $postalCodes = $this->repository->getPostalCodes();

        $result = $this->resultJsonFactory->create();
        $result->setJsonData(json_encode($postalCodes, JSON_UNESCAPED_UNICODE));

        return $result;
    }
}
