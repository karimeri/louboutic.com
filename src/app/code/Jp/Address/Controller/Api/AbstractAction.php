<?php

namespace Jp\Address\Controller\Api;

use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

use Jp\Address\Model\Repository\KenAllRepository as Repository;

/**
 * Class AbstractAction
 *
 * @package Jp\Address\Controller\Api
 * @author  Synolia <contact@synolia.com>
 */
abstract class AbstractAction extends Action
{
    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Http
     */
    protected $request;

    /**
     * AbstractAction constructor.
     *
     * @param Context     $context
     * @param Http        $request
     * @param Repository  $repository
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        Http $request,
        Repository $repository,
        JsonFactory $resultJsonFactory
    ) {
        $this->repository        = $repository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->request           = $request;

        parent::__construct($context);
    }
}
