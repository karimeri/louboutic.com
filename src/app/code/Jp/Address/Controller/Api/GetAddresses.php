<?php

namespace Jp\Address\Controller\Api;

use Jp\Address\Model\KenAll;

/**
 * Class GetAddresses
 *
 * @package Jp\Address\Controller\Api
 * @author  Synolia <contact@synolia.com>
 */
class GetAddresses extends AbstractAction
{
    /**
     * @return string
     */
    public function execute()
    {
        $postalCode = $this->request->getParam(KenAll::POSTAL_CODE);
        $prefecture = $this->request->getParam(KenAll::PREFECTURE);
        $city       = $this->request->getParam(KenAll::CITY);
        $language   = $this->request->getParam(KenAll::LANGUAGE);

        if ($postalCode && $prefecture && $city) {
            $addresses = $this->repository->getAddresses($postalCode, $prefecture, $city, $language);
        } else {
            $addresses = [];
        }

        $result = $this->resultJsonFactory->create();
        $result->setJsonData(json_encode($addresses, JSON_UNESCAPED_UNICODE));

        return $result;
    }
}
