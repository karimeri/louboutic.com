<?php

namespace Jp\Address\Controller\Api;

use Jp\Address\Model\KenAll;

/**
 * Class GetPrefectures
 *
 * @package Jp\Address\Controller\Api
 * @author  Synolia <contact@synolia.com>
 */
class GetPrefectures extends AbstractAction
{
    /**
     * @return string
     */
    public function execute()
    {
        $postalCode = $this->request->getParam(KenAll::POSTAL_CODE);
        $language   = $this->request->getParam(KenAll::LANGUAGE);

        if ($postalCode) {
            $prefectures = $this->repository->getPrefectures($postalCode, $language);
        } else {
            $prefectures = [];
        }

        $result = $this->resultJsonFactory->create();
        $result->setJsonData(json_encode($prefectures, JSON_UNESCAPED_UNICODE));

        return $result;
    }
}
