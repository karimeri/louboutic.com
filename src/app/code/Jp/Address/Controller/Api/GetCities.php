<?php

namespace Jp\Address\Controller\Api;

use Jp\Address\Model\KenAll;

/**
 * Class GetCities
 *
 * @package Jp\Address\Controller\Api
 * @author  Synolia <contact@synolia.com>
 */
class GetCities extends AbstractAction
{
    /**
     * @return string
     */
    public function execute()
    {
        $postalCode = $this->request->getParam(KenAll::POSTAL_CODE);
        $prefecture = $this->request->getParam(KenAll::PREFECTURE);
        $language   = $this->request->getParam(KenAll::LANGUAGE);

        $cities = [];
        if ($postalCode && $prefecture) {
            $cities = $this->repository->getCities($postalCode, $prefecture, $language);
        }

        $result = $this->resultJsonFactory->create();
        $result->setJsonData(json_encode($cities, JSON_UNESCAPED_UNICODE));

        return $result;
    }
}
