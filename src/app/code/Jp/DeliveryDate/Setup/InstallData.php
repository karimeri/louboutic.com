<?php

namespace Jp\DeliveryDate\Setup;

use Magento\Framework\App\Config;
use Magento\Framework\DB\Ddl\Table;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Store\Model\StoreRepositoryFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

use Synolia\Standard\Setup\ShopSetupFactory;
use Synolia\Standard\Setup\ConfigSetupFactory;

/**
 * Class InstallData
 * @package   Jp\DeliveryDate\Setup
 * @author    Synolia <contact@synolia.com>
 */
class InstallData implements InstallDataInterface
{
    const SCOPE_TYPE_STORE = 'store';
    const STORE_CODE_JP_JA = 'jp_ja';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * InstallData constructor.
     *
     * @param Config             $config
     * @param ConfigSetupFactory $configSetupFactory
     */
    public function __construct(
        Config $config,
        ConfigSetupFactory $configSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory
    ) {
        $this->config             = $config;
        $this->configSetupFactory = $configSetupFactory;
        $this->salesSetupFactory  = $salesSetupFactory;
        $this->quoteSetupFactory  = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        // @codingStandardsIgnoreStarts
        if (!(getenv('PROJECT_ENV')) || getenv('PROJECT_ENV') != 'jp') {
            echo 'Install Core JP ignore';
            // @codingStandardsIgnoreEnd
            $setup->endSetup();

            return;
        }

        $configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        // cashondelivery general settings
        $configSetup->saveConfig('shipping/japan/active', 1);
        $configSetup->saveConfig('shipping/japan/empty_label', '指定なし');
        $configSetup->saveConfig('shipping/japan/days_gap', '5');
        $configSetup->saveConfig('shipping/japan/days_number', '7');
        // phpcs:ignore
        $configSetup->saveConfig('shipping/japan/schedule', '{"_1521118199562_562":{"schedule":"Matin"},"_1521118207147_147":{"schedule":"14h-16h"},"_1521118209861_861":{"schedule":"16h-18h"},"_1521118210765_765":{"schedule":"18h-20h"},"_1521118211427_427":{"schedule":"19h-21h"}}');

        $this->addQuoteFields($setup);
        $this->addOrderFields($setup);

        $this->config->clean();

        $setup->endSetup();
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function addQuoteFields($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'japan_shipping_day',
            [
                'type' => Table::TYPE_DATE,
                'nullable' => true,
                'comment' => 'Japan Shipping Day',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'japan_shipping_hour',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Japan Shipping Hour',
            ]
        );
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function addOrderFields($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'japan_shipping_day',
            [
                'type' => Table::TYPE_DATE,
                'nullable' => true,
                'comment' => 'Japan Shipping Day',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'japan_shipping_hour',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Japan Shipping Hour',
            ]
        );
    }
}
