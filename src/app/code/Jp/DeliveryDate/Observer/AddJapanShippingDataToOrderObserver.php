<?php

namespace Jp\DeliveryDate\Observer;

use Colissimo\Shipping\Model\Pickup;
use Colissimo\Shipping\Model\Address;
use Jp\DeliveryDate\Helper\Config;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Quote\Api\Data\AddressExtensionFactory;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;

/**
 * Class AddJapanShippingDataToOrderObserver
 *
 * @package Jp\DeliveryDate\Observer
 * @author  Synolia <contact@synolia.com>
 */
class AddJapanShippingDataToOrderObserver implements ObserverInterface
{
    /**
     * @var Config $helper
     */
    protected $helper;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Jp\Address\Model\Repository\KenAllRepository
     */
    protected $kenAllRepository;

    /**
     * @param Config $helper
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Jp\Address\Model\Repository\KenAllRepository $kenAllRepository
     */
    public function __construct(
        Config $helper,
        EnvironmentManager $environmentManager,
        \Jp\Address\Model\Repository\KenAllRepository $kenAllRepository
    ) {
        $this->helper = $helper;
        $this->environmentManager = $environmentManager;
        $this->kenAllRepository = $kenAllRepository;
    }

    /**
     * Add data to order address
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        if ($this->environmentManager->getEnvironment() === Environment::JP) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $observer->getEvent()->getOrder();

            /** @var \Magento\Quote\Model\Quote $quote */
            $quote = $observer->getEvent()->getQuote();

            try {
                $kenAll = $this->kenAllRepository->findOneBy(
                    ['postal_code' => $quote->getShippingAddress()->getPostcode()]
                );

                $order->setJapaneseAddressId($kenAll->getId());
            } catch (\Exception $e) {
                // custom address => do nothing
            }

            if ($this->helper->isActive()) {
                $order->setJapanShippingDay($quote->getJapanShippingDay());
                $order->setJapanShippingHour($quote->getJapanShippingHour());
            }
        }

        return $this;
    }
}
