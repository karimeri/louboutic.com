var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Jp_DeliveryDate/js/view/shipping-mixin': true
            },
            'CyberSource_Address/js/view/cybersource-shipping': {
                'Jp_DeliveryDate/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/model/quote': {
                'Jp_DeliveryDate/js/model/quote-mixin': true
            }
        }
    }
};
