define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-save-processor',
    'Jp_DeliveryDate/js/model/shipping-save-processor/customer-address'
], function (
    Component,
    shippingSaveProcessor,
    customerAddressShippingSaveProcessor
) {
    'use strict';

    shippingSaveProcessor.registerProcessor('customer-address', customerAddressShippingSaveProcessor);

    /** Add view logic here if needed */
    return Component.extend({});
});
