define([
    'jquery',
], function (
    $
) {
    'use strict';

    var mixin = {
        day: '',
        hour: '',

        setDaySchedule: function(day) {
            this.day = day;
        },

        setHourSchedule: function (hour) {
            this.hour = hour;
        },

        getDaySchedule: function() {
            return this.day;
        },

        getHourSchedule: function () {
            return this.hour;
        }
    };

    return function (target) { // target == Result that Magento_Checkout/js/model/quote returns.
        return $.extend(true, {}, target, mixin);
    };
});

