define([
    'jquery',
    'Magento_Checkout/js/model/quote',
], function (
    $,
    quote
) {
    'use strict';

    var mixin = {
        japanShippingDaysSchedule: window.checkoutConfig.japan_shipping_days_schedule,
        japanShippingHoursSchedule: window.checkoutConfig.japan_shipping_hours_schedule,

        /**
         *
         * @returns bool
         */
        isJapanDeliveryActive: function () {
            return window.checkoutConfig.japan_shipping_active;
        },

        onScheduleDayChange: function (shippingMethod, event) {
            var value = event.currentTarget.options[event.currentTarget.selectedIndex].value;
            quote.setDaySchedule(value);
        },

        onScheduleHourChange: function (shippingMethod, event) {
            var value = event.currentTarget.options[event.currentTarget.selectedIndex].value;
            quote.setHourSchedule(value);
        }
    };

    return function (target) { // target == Result that Magento_Checkout/view/frontend/web/js/view/shipping returns.
        return target.extend(mixin);
    };
});
