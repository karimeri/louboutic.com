define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-save-processor',
    'Jp_DeliveryDate/js/model/shipping-save-processor/default'
], function (
    Component,
    shippingSaveProcessor,
    defaultShippingSaveProcessor
) {
    'use strict';

    shippingSaveProcessor.registerProcessor('default', defaultShippingSaveProcessor);

    /** Add view logic here if needed */
    return Component.extend({});
});
