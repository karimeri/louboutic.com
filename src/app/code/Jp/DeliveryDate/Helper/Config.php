<?php

namespace Jp\DeliveryDate\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class Config
 *
 * @package Jp\DeliveryDate\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_ACTIVE      = 'shipping/japan/active';
    const XML_PATH_DAYS_GAP    = 'shipping/japan/days_gap';
    const XML_PATH_DAYS_NUMBER = 'shipping/japan/days_number';
    const XML_PATH_SCHEDULE    = 'shipping/japan/schedule';
    const XML_PATH_EMPTY_LABEL = 'shipping/japan/empty_label';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * Config constructor.
     *
     * @param Context             $context
     * @param SerializerInterface $serializer
     * @param TimezoneInterface   $timezone
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer,
        TimezoneInterface $timezone
    ) {
        $this->serializer = $serializer;
        $this->timezone   = $timezone;

        parent::__construct($context);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool
     */
    public function isActive($scopeCode = null)
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_ACTIVE, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getDaysGap($scopeCode = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_DAYS_GAP, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getDaysNumber($scopeCode = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_DAYS_NUMBER, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return string|null
     */
    public function getEmptyLabel($scopeCode = null)
    {
        return $this->scopeConfig->getValue(self::XML_PATH_EMPTY_LABEL, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param bool $withEmptyValue
     *
     * @return array|bool
     * @throws \InvalidArgumentException
     */
    public function getHoursSchedule($withEmptyValue = false)
    {
        $schedule = $this->scopeConfig->getValue(self::XML_PATH_SCHEDULE);

        if ($schedule) {
            $schedule = array_values($this->serializer->unserialize($schedule));

            foreach ($schedule as &$entry) {
                $entry['schedule'] = __($entry['schedule']);
            }

            if ($withEmptyValue) {
                $emptyLabel = ['schedule' => $this->getEmptyLabel()];
                array_unshift($schedule, $emptyLabel);
            }

            return $schedule;
        }


        return $schedule;
    }

    /**
     * @param bool $withEmptyValue
     *
     * @return array|bool
     */
    public function getDaysSchedule($withEmptyValue = false)
    {
        $daysGap    = $this->getDaysGap();
        $daysNumber = $this->getDaysNumber();
        $date       = $this->timezone->date()->format('Y-m-d');
        $dates      = [];

        $dateGapped = date('Y-m-d', strtotime(sprintf('%s +%s day', $date, $daysGap)));
        $displayDate = sprintf(
            '%s (%s)',
            date('Y/m/d', strtotime($dateGapped)),
            __(date('l', strtotime($dateGapped)))
        );
        $dates[0]   = [
            'date'        => $dateGapped,
            'displayDate' => $displayDate,
        ];

        for ($i = 1; $i < $daysNumber; $i++) {
            $dateDay = date('Y-m-d', strtotime(sprintf('%s +%s day', $dateGapped, $i)));
            $displayDate = sprintf(
                '%s (%s)',
                date('Y/m/d', strtotime($dateDay)),
                __(date('l', strtotime($dateDay)))
            );
            $dates[] = [
                'date'        => $dateDay,
                'displayDate' => $displayDate,
            ];
        }

        if ($withEmptyValue) {
            $unshift = [
                'date'        => $this->getEmptyLabel(),
                'displayDate' => $this->getEmptyLabel(),
            ];
            array_unshift($dates, $unshift);
        }

        return $dates;
    }
}
