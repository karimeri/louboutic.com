<?php

namespace Jp\DeliveryDate\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class ScheduleArray
 *
 * @package Jp\DeliveryDate\Block\Adminhtml\System\Config\Form\Field
 * @author Synolia <contact@synolia.com>
 */
class ScheduleArray extends AbstractFieldArray
{
    /**
     * Initialise form fields
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct() //@codingStandardsIgnoreLine
    {
        $this->addColumn('schedule', ['label' => __('Schedule')]);

        $this->_addAfter       = false;
        $this->_addButtonLabel = __('Add');
    }
}
