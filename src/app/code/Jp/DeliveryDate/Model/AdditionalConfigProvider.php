<?php

namespace Jp\DeliveryDate\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

use Jp\DeliveryDate\Helper\Config;

/**
 * Class AdditionalConfigProvider
 *
 * @package Jp\DeliveryDate\Model
 * @author  Synolia <contact@synolia.com>
 */
class AdditionalConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Config
     */
    protected $helper;

    /**
     * Config constructor.
     *
     * @param Config $helper
     */
    public function __construct(
        Config $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $output['japan_shipping_active'] = $this->helper->isActive();

        if ($output['japan_shipping_active']) {
            $output['japan_shipping_days_schedule']  = $this->helper->getDaysSchedule();
            $output['japan_shipping_hours_schedule'] = $this->helper->getHoursSchedule();
        }

        return $output;
    }
}
