<?php

namespace Jp\CashOnDelivery\Model\System\Config;

use Magento\Config\Model\Config\Backend\Serialized\ArraySerialized;

/**
 * Class PriceRulesArray
 *
 * @package Jp\CashOnDelivery\Model\System\Config
 * @author  Synolia <contact@synolia.com>
 */
class PriceRulesArray extends ArraySerialized
{
    /**
     * @return ArraySerialized
     */
    public function beforeSave()
    {
        // For value validations
        $exceptions = $this->getValue();

        // Validations
        $this->setValue($exceptions);

        return parent::beforeSave();
    }
}
