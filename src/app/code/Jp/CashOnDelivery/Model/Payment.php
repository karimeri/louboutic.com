<?php

namespace Jp\CashOnDelivery\Model;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Payment\Model\Method\AbstractMethod;

/**
 * Class Payment
 *
 * @package Jp\CashOnDelivery\Model
 * @author  Synolia <contact@synolia.com>
 */
class Payment extends AbstractMethod
{
    const CODE                     = 'cashondelivery';
    const XML_PATH_EXCLUDE_REGIONS = 'payment/cashondelivery/exclude_regions';

    /**
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var string
     */
    protected $_formBlockType = 'Magento\OfflinePayments\Block\Form\Checkmo';

    /**
     * @var string
     */
    protected $_infoBlockType = 'Jp\CashOnDelivery\Block\Info\CashOnDelivery';

    /**
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * @param CartInterface|null $quote
     *
     * @return bool
     */
    public function isAvailable(CartInterface $quote = null)
    {
        if (!parent::isAvailable($quote)) {
            return false;
        }

        if ($quote->getItemVirtualQty() > 0) {
            return false;
        }

        return true;
    }
}
