<?php

namespace Jp\CashOnDelivery\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

use Jp\CashOnDelivery\Helper\Config;

/**
 * Class AdditionalConfigProvider
 * @author Synolia <contact@synolia.com>
 */
class AdditionalConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Config
     */
    protected $helper;

    /**
     * Config constructor.
     *
     * @param Config $helper
     */
    public function __construct(
        Config $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $output['priceRules']            = $this->helper->getPriceRules();
        $output['unavailableCODMessage'] = $this->helper->getUnavailableMessage();

        return $output;
    }
}
