<?php

namespace Jp\CashOnDelivery\Model\Sales\Pdf;

use Magento\Sales\Model\Order\Pdf\Total\DefaultTotal;

/**
 * Class Amount
 *
 * @package Jp\CashOnDelivery\Model\Sales\Pdf
 * @author  Synolia <contact@synolia.com>
 */
class Amount extends DefaultTotal
{
    /**
     * Get Total amount from source
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->getOrder()->getCodAmount();
    }
}
