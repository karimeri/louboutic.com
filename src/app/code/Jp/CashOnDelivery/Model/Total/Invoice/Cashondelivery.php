<?php

namespace Jp\CashOnDelivery\Model\Total\Invoice;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

use Jp\CashOnDelivery\Model\Payment;

/**
 * Class Cashondelivery
 *
 * @package Jp\CashOnDelivery\Model\Total\Invoice
 * @author  Synolia <contact@synolia.com>
 */
class Cashondelivery extends AbstractTotal
{
    public function collect(Invoice $invoice)
    {
        $order = $invoice->getOrder();

        $invoice->setCodAmount($order->getCodAmount());

        if ($this->_canApplyTotal($order)) {
            $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getCodAmount());
        }

        return $this;
    }

    /**
     * Return true if can apply totals
     *
     * @param Order $order
     *
     * @return bool
     */
    protected function _canApplyTotal(Order $order)
    {
        return ($order->getPayment()->getMethod() == Payment::CODE);
    }
}
