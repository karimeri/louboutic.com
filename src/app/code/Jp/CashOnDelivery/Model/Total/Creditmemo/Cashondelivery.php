<?php

namespace Jp\CashOnDelivery\Model\Total\Creditmemo;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

use Jp\CashOnDelivery\Model\Payment;

/**
 * Class Cashondelivery
 *
 * @package Jp\CashOnDelivery\Model\Total\Creditmemo
 * @author  Synolia <contact@synolia.com>
 */
class Cashondelivery extends AbstractTotal
{
    public function collect(Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();

        $creditmemo->setCodAmount($order->getCodAmount());

        if ($this->_canApplyTotal($order)) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getCodAmount());
        }

        return $this;
    }

    /**
     * Return true if can apply totals
     *
     * @param Order $order
     *
     * @return bool
     */
    protected function _canApplyTotal(Order $order)
    {
        return ($order->getPayment()->getMethod() == Payment::CODE);
    }
}
