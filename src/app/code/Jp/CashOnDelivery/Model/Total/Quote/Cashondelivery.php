<?php

namespace Jp\CashOnDelivery\Model\Total\Quote;

use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

use Jp\CashOnDelivery\Model\Payment;
use Jp\CashOnDelivery\Helper\Config;

/**
 * Class Cashondelivery
 * @package Jp\CashOnDelivery\Model\Total\Quote
 * @author  Synolia <contact@synolia.com>
 */
class Cashondelivery extends AbstractTotal
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrencyInterface;

    /**
     * @var Config
     */
    private $helper;

    public function __construct(
        PriceCurrencyInterface $priceCurrencyInterface,
        Config $helper
    ) {
        $this->priceCurrencyInterface = $priceCurrencyInterface;
        $this->helper = $helper;
        $this->setCode(Payment::CODE);
    }

    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        if ($shippingAssignment->getShipping()->getAddress()->getAddressType() != Address::TYPE_SHIPPING
            || $quote->isVirtual()
        ) {
            return $this;
        }

        $amount = $this->helper->getFeeByTotal($quote->getGrandTotal());

        if ($amount === null) {
            return $this;
        }

        if ($this->_canApplyTotal($quote)) {
            $total->setBaseTotalAmount(Payment::CODE, $amount);
            $total->setTotalAmount(Payment::CODE, $amount);

            $total->setCodAmount($amount);

            $total->setBaseGrandTotal($total->getBaseGrandTotal() + $amount);
            $total->setGrandTotal($total->getGrandTotal() + $amount);
        }

        $quote->setCodAmount($amount);

        return $this;
    }

    public function fetch(Quote $quote, Total $total)
    {
        if ($this->_canApplyTotal($quote)) {
            return [
                'code' => $this->getCode(),
                'title' => __('Cash On Delivery'),
                'value' => $total->getCodAmount(),
            ];
        }

        return null;
    }

    public function getLabel()
    {
        return __('Cash On Delivery');
    }

    /**
     * Return true if can apply totals
     *
     * @param Quote $quote
     *
     * @return bool
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    //phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    protected function _canApplyTotal(Quote $quote)
    {
        if (!$quote->getId()) {
            return false;
        }

        return ($quote->getPayment()->getMethod() == Payment::CODE);
    }
}
