<?php

namespace Jp\CashOnDelivery\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Config
 *
 * @package Jp\CashOnDelivery\Helper
 * @author  Synolia <contact@synolia.com>
 */
class Config extends AbstractHelper
{
    const XML_PATH_PRICE_RULES         = 'payment/cashondelivery/price_rules';
    const XML_PATH_UNAVAILABLE_MESSAGE = 'payment/cashondelivery/unavailable_message';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Config constructor.
     *
     * @param Context             $context
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context $context,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;

        parent::__construct($context);
    }

    /**
     * @param             $configName
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getConfig($configName, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($configName, ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|array
     * @throws \InvalidArgumentException
     */
    public function getPriceRules($scopeCode = null)
    {
        $priceRules = $this->getConfig(self::XML_PATH_PRICE_RULES, $scopeCode);

        if ($priceRules) {
            return $this->serializer->unserialize($priceRules);
        }

        return $priceRules;
    }

    /**
     * @param float       $total
     * @param string|null $scopeCode
     *
     * @return float|null
     */
    public function getFeeByTotal($total, $scopeCode = null)
    {
        $priceRules = $this->getPriceRules($scopeCode);

        if ($priceRules) {
            foreach ($this->getPriceRules($scopeCode) as $rule) {
                if ($total >= $rule['minimum_price'] && $total <= $rule['maximum_price']) {
                    return $rule['fee'];
                }
            }
        }

        return null;
    }

    /**
     * @param string|null $scopeCode
     *
     * @return bool|string
     */
    public function getUnavailableMessage($scopeCode = null)
    {
        return $this->getConfig(self::XML_PATH_UNAVAILABLE_MESSAGE, $scopeCode);
    }
}
