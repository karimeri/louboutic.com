<?php

namespace Jp\CashOnDelivery\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

use Jp\CashOnDelivery\Model\Payment;

/**
 * Class SalesModelServiceQuoteSubmitBefore
 *
 * @package Jp\CashOnDelivery\Observer
 * @author  Synolia <contact@synolia.com>
 */
class SalesModelServiceQuoteSubmitBefore implements ObserverInterface
{
    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();

        if ($order->getPayment()->getMethod() === Payment::CODE) {
            $order->setCodAmount($quote->getCodAmount());
            $order->setShippingMethod('cashondelivery_cashondelivery');
            $order->setShippingDescription('Cash On Delivery');
        }
    }
}
