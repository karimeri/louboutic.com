<?php

namespace Jp\CashOnDelivery\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Rma\Block\Adminhtml\Rma\Edit\Tab\Items\Grid\Column\Renderer\Textselect;

/**
 * Class PriceRulesArray
 * @package Jp\CashOnDelivery\Block\Adminhtml\System\Config
 * @author  Synolia <contact@synolia.com>
 */
class PriceRulesArray extends AbstractFieldArray
{
    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // phpcs:ignore
    protected function _prepareToRender()
    {
        $this->addColumn('label', [
            'label' => __('Label'),
            'renderer' => Textselect::class,
            'size' => 20,
        ]);
        $this->addColumn('minimum_price', [
            'label' => __('Minimum Price'),
            'renderer' => Textselect::class,
            'size' => 10,
        ]);
        $this->addColumn('maximum_price', [
            'label' => __('Maximum Price'),
            'renderer' => Textselect::class,
            'size' => 10,
        ]);
        $this->addColumn('fee', [
            'label' => __('Fee'),
            'renderer' => Textselect::class,
            'size' => 4,
        ]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}
