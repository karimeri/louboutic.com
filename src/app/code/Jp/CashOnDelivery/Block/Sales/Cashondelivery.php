<?php

namespace Jp\CashOnDelivery\Block\Sales;

use Magento\Framework\DataObject;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;

use Jp\CashOnDelivery\Model\Payment;

/**
 * Class Cashondelivery
 *
 * @package Jp\CashOnDelivery\Block\Sales
 * @author  Synolia <contact@synolia.com>
 */
class Cashondelivery extends Template
{
    /**
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $source = $parent->getSource();

        $payment = $this->getPayment($source);
        if ($payment && ($payment->getMethod() == Payment::CODE)) {
            $fee = new DataObject(
                [
                    'code' => Payment::CODE,
                    'strong' => false,
                    'value' => $this->getCodAmount($source),
                    'label' => __('Cash on delivery'),
                ]
            );

            $parent->addTotalBefore($fee, 'grand_total');
        }

        return $this;
    }

    /**
     * @param $source
     *
     * @return OrderPaymentInterface|null
     */
    protected function getPayment($source)
    {
        if ($source instanceof InvoiceInterface) {
            return $source->getOrder()->getPayment();
        }

        if ($source instanceof OrderInterface) {
            return $source->getPayment();
        }

        if ($source instanceof CreditMemoInterface) {
            return $source->getOrder()->getPayment();
        }

        return null;
    }

    /**
     * @param $source
     *
     * @return OrderPaymentInterface|null
     */
    protected function getCodAmount($source)
    {
        if ($source instanceof InvoiceInterface) {
            return $source->getOrder()->getCodAmount();
        }

        if ($source instanceof OrderInterface) {
            return $source->getCodAmount();
        }

        if ($source instanceof CreditMemoInterface) {
            return $source->getOrder()->getCodAmount();
        }

        return null;
    }
}
