<?php

namespace Jp\CashOnDelivery\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Project\Core\Manager\EnvironmentManager;
use Project\Core\Model\Environment;
use Synolia\Standard\Setup\Upgrade;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class RecurringData
 *
 * @package Jp\CashOnDelivery\Setup
 * @author Synolia <contact@synolia.com>
 */
class RecurringData implements \Magento\Framework\Setup\InstallDataInterface
{
    /**
     * @var Upgrade
     */
    protected $synoliaSetup;

    /**
     * @var \Project\Core\Manager\EnvironmentManager
     */
    protected $environmentManager;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * RecurringData constructor.
     *
     * @param Upgrade $synoliaSetup
     * @param \Project\Core\Manager\EnvironmentManager $environmentManager
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     */
    public function __construct(
        Upgrade $synoliaSetup,
        EnvironmentManager $environmentManager,
        ConsoleOutput $consoleOutput
    ) {
        $this->synoliaSetup = $synoliaSetup;
        $this->environmentManager = $environmentManager;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($this->environmentManager->getEnvironment() !== Environment::JP) {
            $this->consoleOutput->writeln('Recurring Data CashOnDelivery JP ignore');
            $setup->endSetup();

            return;
        }

        $this->synoliaSetup->runUpgrade($this, 'RecurringData');
        $setup->endSetup();
    }
}
