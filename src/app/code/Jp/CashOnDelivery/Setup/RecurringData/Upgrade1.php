<?php

namespace Jp\CashOnDelivery\Setup\RecurringData;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\WebsiteRepository;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;
use Magento\Store\Model\StoreManager;

/**
 * Class Upgrade1
 * @package Project\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade1 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @var \Magento\Store\Model\WebsiteRepository
     */
    protected $websiteRepository;

    /**
     * Upgrade1 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\ConfigSetup $configSetup
     * @param \Magento\Store\Model\StoreManager $storeManager
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Store\Model\WebsiteRepository $websiteRepository
     */
    public function __construct(
        ConfigSetup $configSetup,
        StoreManager $storeManager,
        ModuleDataSetupInterface $setup,
        WebsiteRepository $websiteRepository
    ) {
        $this->configSetup = $configSetup;
        $this->storeManager = $storeManager;
        $this->setup = $setup;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig(
            'payment/cashondelivery/price_rules',
            '{
"_1517575465228_228":{"label":"From 0 to 10000","minimum_price":"0","maximum_price":"10000","fee":"324"},
"_1517578442857_857":{"label":"From 10001 to 30000","minimum_price":"10000.01","maximum_price":"30000","fee":"432"},
"_1517578466150_150":{"label":"From 30001 to 100000","minimum_price":"30000.01","maximum_price":"100000","fee":"648"},
"_1517578477668_668":{"label":"From 100001 to 300000","minimum_price":"100000.01","maximum_price":"298950","fee":"1080"}
}');

        $this->configSetup->saveConfig(
            'carriers/owsh1/config',
            'addMethod(\'yamato1\', [
    \'title\'  => "配送料(税込)",
    \'price\'  => 850,
    \'enabled\' => $quote->grand_total < 15000
]);

addMethod(\'yamato2\', [
    \'title\'  => "配送料(税込)",
    \'price\'  => 0,
    \'enabled\' => $quote->grand_total >= 15000
]);',
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
            $this->websiteRepository->get('jp')->getId()
        );
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getDescription()
    {
        return 'Add new state for cash on delivery';
    }
}
