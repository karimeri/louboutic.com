<?php

namespace Jp\CashOnDelivery\Setup;

use Magento\Framework\App\Config;
use Magento\Sales\Model\Order;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Store\Model\StoreRepositoryFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Synolia\Standard\Setup\ShopSetupFactory;
use Synolia\Standard\Setup\ConfigSetupFactory;

/**
 * Class InstallData
 * @package   Jp\CashOnDelivery\Setup
 * @author    Synolia <contact@synolia.com>
 * @copyright Open Software License v. 3.0 (https://opensource.org/licenses/OSL-3.0)
 */
class InstallData implements InstallDataInterface
{
    const SCOPE_TYPE_STORE = 'store';
    const STORE_CODE_JP_JA = 'jp_ja';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var ConfigSetupFactory
     */
    protected $configSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * InstallData constructor.
     *
     * @param Config             $config
     * @param ConfigSetupFactory $configSetupFactory
     */
    public function __construct(
        Config $config,
        ConfigSetupFactory $configSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory
    ) {
        $this->config             = $config;
        $this->configSetupFactory = $configSetupFactory;
        $this->salesSetupFactory  = $salesSetupFactory;
        $this->quoteSetupFactory  = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        // @codingStandardsIgnoreStarts
        if (!(getenv('PROJECT_ENV')) || getenv('PROJECT_ENV') != 'jp') {
            // @codingStandardsIgnoreEnd
            //phpcs:ignore Ecg.Security.LanguageConstruct.DirectOutput
            echo 'Install Core JP ignore';
            $setup->endSetup();

            return;
        }

        $configSetup = $this->configSetupFactory->create(['setup' => $setup]);

        // cashondelivery general settings
        $configSetup->saveConfig('payment/cashondelivery/active', 1);
        $configSetup->saveConfig('payment/cashondelivery/title', '代金引換払い');
        //phpcs:ignore Generic.Files.LineLength.TooLong
        $configSetup->saveConfig('payment/cashondelivery/price_rules', '{"_1517575465228_228":{"label":"From 0 to 10000","minimum_price":"0","maximum_price":"10000","fee":"315"},"_1517578442857_857":{"label":"From 10001 to 30000","minimum_price":"10000.01","maximum_price":"30000","fee":"420"},"_1517578466150_150":{"label":"From 30001 to 100000","minimum_price":"30000.01","maximum_price":"100000","fee":"630"},"_1517578477668_668":{"label":"From 100001 to 300000","minimum_price":"100000.01","maximum_price":"298950","fee":"1050"}}');
        //phpcs:ignore Generic.Files.LineLength.TooLong
        $configSetup->saveConfig('payment/cashondelivery/unavailable_message', 'ご注文者とお届け先の住所が異なる場合や注文合計金額が￥300,000（税込）以上の場合、代金引換払いはご利用になれません。');
        $configSetup->saveConfig('payment/cashondelivery/order_status', Order::STATE_PROCESSING);

        $this->config->clean();

        $this->setupSalesTables($setup);

        $setup->endSetup();
    }

    protected function setupSalesTables(ModuleDataSetupInterface $setup)
    {
        $attributes = [
            'cod_amount' => 'Cash On Delivery Amount',
        ];

        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        foreach ($attributes as $attributeCode => $attributeLabel) {
            $salesSetup->addAttribute('order', $attributeCode, ['type' => 'decimal']);
            $salesSetup->addAttribute('order_address', $attributeCode, ['type' => 'decimal']);
        }

        $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
        foreach ($attributes as $attributeCode => $attributeLabel) {
            $quoteSetup->addAttribute('quote', $attributeCode, ['type' => 'decimal']);
            $quoteSetup->addAttribute('quote_address', $attributeCode, ['type' => 'decimal']);
        }
    }
}
