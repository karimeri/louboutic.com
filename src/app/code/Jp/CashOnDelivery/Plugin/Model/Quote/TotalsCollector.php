<?php

namespace Jp\CashOnDelivery\Plugin\Model\Quote;

use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\TotalsCollector as MagentoTotalsCollector;

class TotalsCollector
{
    /**
     * Reset quote COD amount
     *
     * @param MagentoTotalsCollector $subject
     * @param Quote                  $quote
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeCollect(
        MagentoTotalsCollector $subject,
        Quote $quote
    ) {
        $quote->setCodAmount(0);
    }
}
