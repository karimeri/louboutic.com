define(
    [
        'ko'
    ],
    function (ko) {
        'use strict';

        return {
            canShowCashOnDelivery: ko.observable(false)
        };
    }
);
