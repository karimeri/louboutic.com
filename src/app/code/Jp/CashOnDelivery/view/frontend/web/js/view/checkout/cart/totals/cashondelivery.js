define(
    [
        'ko',
        'Jp_CashOnDelivery/js/view/checkout/summary/cashondelivery',
        'Jp_CashOnDelivery/js/view/checkout/cashondelivery'
    ],
    function (ko, Component, cashondelivery) {
        'use strict';

        return Component.extend({
            isDisplayed: function () {
                return this.isFullMode();
            }
        });
    }
);
