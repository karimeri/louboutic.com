define([
    'ko',
    'jquery',
    'mage/storage',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Checkout/js/model/url-builder',
    'mage/url',
    'Magento_Checkout/js/model/full-screen-loader',
    'Jp_CashOnDelivery/js/view/checkout/cashondelivery',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/totals'
], function (ko, $, storage, Component, quote, getTotalsAction, urlBuilder, mageUrlBuilder, fullScreenLoader, cashondelivery, customer, totals) {
    'use strict';

    var mixin = {
        defaults: {
            template: 'Jp_CashOnDelivery/payment/cashondelivery'
        },
        lastDetectedMethod: null,
        extraFeeText: ko.observable(''),
        refreshMethod: function () {
            var serviceUrl;

            var paymentData = quote.paymentMethod();

            // We have to make sure we don't send title to the backend,
            // otherwise it will fail. Perhaps there is better way to do this.
            paymentData = JSON.parse(JSON.stringify(paymentData));
            delete paymentData['title'];

            fullScreenLoader.startLoader();

            if (customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/carts/mine/selected-payment-method', {});
            } else {
                serviceUrl = urlBuilder.createUrl('/guest-carts/:cartId/selected-payment-method', {
                    cartId: quote.getQuoteId()
                });
            }

            var payload = {
                cartId: quote.getQuoteId(),
                method: paymentData
            };

            return storage.put(
                serviceUrl,
                JSON.stringify(payload)
            ).done(function () {
                cashondelivery.canShowCashOnDelivery(quote.paymentMethod().method == 'cashondelivery');
                getTotalsAction([]);
                fullScreenLoader.stopLoader();
            });
        },
        initObservable: function () {
            this._super();
            var me = this;

            quote.paymentMethod.subscribe(function () {
                if (quote.paymentMethod().method != me.lastDetectedMethod) {
                    if (
                        (quote.paymentMethod().method == 'cashondelivery') ||
                        (me.lastDetectedMethod == 'cashondelivery') ||
                        (totals.getSegment('cashondelivery') && (me.lastDetectedMethod === null))
                    ) {
                        me.refreshMethod();
                    }

                    me.lastDetectedMethod = quote.paymentMethod().method;
                }
            });

            return this;
        },

        /**
         * Returns quote's subtotal_with_discount value
         *
         * @return {*}
         */
        getGrandTotal: function () {
            return quote.getTotals()().subtotal_incl_tax;
        },

        isAvailable: function () {
            if (quote.billingAddress() && quote.billingAddress().customerAddressId === quote.shippingAddress().customerAddressId) {
                if (this.getFeeByQuoteAmount()) {
                    return true;
                }
            }

            return false;
        },

        /**
         * Returns unavailable message from config
         *
         * @returns {*}
         */
        getUnavailableMessage: function () {
            return window.checkoutConfig.unavailableCODMessage;
        },

        /**
         * Returns the fee from the config depending on the amount
         *
         * @returns {*}
         */
        getFeeByQuoteAmount: function () {
            var amount = this.getGrandTotal();
            var priceRules = window.checkoutConfig.priceRules;

            for (var rule in priceRules) {
                if (amount >= priceRules[rule].minimum_price && amount <= priceRules[rule].maximum_price) {
                    return priceRules[rule]['fee'];
                }
            }

            return false;
        }
    };

    return function (target) { // target == Result that Magento_OfflinePayments/js/view/payment/method-renderer/cashondelivery-method returns.
        return target.extend(mixin);
    };
});
