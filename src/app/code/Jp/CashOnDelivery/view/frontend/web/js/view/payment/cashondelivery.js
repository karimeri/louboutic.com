define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'cashondelivery',
                component: 'Jp_CashOnDelivery/js/view/payment/method-renderer/cashondelivery'
            }
        );
        
        return Component.extend({});
    }
);
