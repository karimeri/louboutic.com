<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;

/**
 * Class Upgrade106
 *
 * @package Jp\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade106
{
    const COUNTRY_AND_CURRENCY_PATH = 'data/countryAndCurrency.csv';

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $countryAndCurrencyData = $upgradeDataObject->getDataFile(UpgradeData::MODULE_NAME, self::COUNTRY_AND_CURRENCY_PATH);
        $upgradeDataObject->saveConfig($countryAndCurrencyData);
    }
}
