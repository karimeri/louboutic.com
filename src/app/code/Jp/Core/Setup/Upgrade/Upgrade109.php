<?php
namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;
use Magento\Rma\Model\Rma;

/**
 * Class Upgrade109
 * @package Jp\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade109
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        // Disable RMA on storefront for all
        $upgradeDataObject->getConfigSetup()->saveConfig(Rma::XML_PATH_ENABLED, 0);
    }
}
