<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade107
 * @package Jp\Core\Setup\Upgrade
 * @author Synolia <contact@synolia.com>
 */
class Upgrade107
{
    const XML_RECAPTCHA_BASE_PATH = 'synolia_recaptcha/general/';

    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade107 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'enabled',
            '1'
        );
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'public_key',
            '6Lc8800UAAAAACL70MG4mabd4l8Ah6IOd3S0w7hy'
        );
        $this->configSetup->saveConfig(
            $this::XML_RECAPTCHA_BASE_PATH . 'private_key',
            '6Lc8800UAAAAANdKXyJOUhgUfRiWkh8V4O_Ro6xO'
        );
    }
}
