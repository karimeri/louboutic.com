<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;

/**
 * Class Upgrade101
 * @package   Jp\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade101
{
    /**
     * @param $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $upgradeDataObject->getConfigSetup()->saveConfig('general/locale/timezone', 'Asia/Tokyo');
    }
}
