<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;

/**
 * Class Upgrade104
 * @package   Jp\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade104
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Veritrans config
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/veritrans_cc/title', 'クレジットカード');
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/veritrans_cc/active', 1);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/veritrans_cc/use_3dsecure', 1);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/veritrans_cc/service_option_type', 'mpi-complete');

        //Disable adyen and cybersource on JP
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_abstract/debug', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_abstract/paypal_capture_mode', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_apple_pay/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_boleto/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_cc/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_cc/enable_installments', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_hpp/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_oneclick/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_pay_by_mail/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_pos/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/adyen_sepa/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/chcybersource/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/chcybersource/enable_cvv', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/chcybersource/enable_dm_cron', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/chcybersource/fingerprint_enabled', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/cybersource/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/cybersourceecheck/active', 0);
        $upgradeDataObject->getConfigSetup()->saveConfig('payment/cybersourcepaypal/active', 0);
    }
}
