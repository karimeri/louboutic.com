<?php

namespace Jp\Core\Setup\Upgrade;

use Magento\Eav\Model\AttributeRepository;
use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade1013
 * @package Jp\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade1013
{
    /**
     * @var CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * Upgrade1013 constructor.
     *
     * @param \Synolia\Standard\Setup\Eav\CustomerAddressSetup $customerAddressSetup
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     * @param \Magento\Eav\Model\AttributeRepository $attributeRepository
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup,
        AttributeRepository $attributeRepository
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup = $eavSetup;
        $this->attributeRepository = $attributeRepository;
    }


    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function run()
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'firstname_en',
                'entity_type_id' => 2,
                'data' => [
                    'backend_type' => 'varchar',
                    'frontend_label' => 'Firstname EN',
                    'input' => 'text',
                    'required' => true,
                    'visible' => true,
                    'is_user_defined' => true,
                    'is_system' => 0,
                    'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                    'sort_order' => 53,
                    'used_in_forms' => [
                        'adminhtml_customer_address',
                        'customer_address_edit',
                        'customer_register_address'
                    ],
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'lastname_en',
                'entity_type_id' => 2,
                'data' => [
                    'backend_type' => 'varchar',
                    'frontend_label' => 'Lastname EN',
                    'input' => 'text',
                    'required' => true,
                    'visible' => true,
                    'is_user_defined' => true,
                    'is_system' => 0,
                    'validate_rules' => '{"max_text_length":"35","input_validation":"length"}',
                    'sort_order' => 55,
                    'used_in_forms' => [
                        'adminhtml_customer_address',
                        'customer_address_edit',
                        'customer_register_address'
                    ],
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'company',
                'data' => [
                    'is_visible' => false,
                ],
            ],
        ];

        foreach ($attributes as $attribute) {
            if ($attribute['code'] !== 'company'
            && $this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->removeAttribute($attribute['type'], $attribute['code']);
            }

            if (!$this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->addAttribute($attribute['entity_type_id'], $attribute['code'], []);
            }

            $eavAttribute = $this->attributeRepository->get($attribute['type'], $attribute['code']);

            foreach ($attribute['data'] as $field => $value) {
                $eavAttribute->setData($field, $value);
            }

            // phpcs:ignore
            $eavAttribute->save();
        }
    }
}
