<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;
use Project\Erp\Helper\Config;

/**
 * Class Upgrade103
 *
 * @package Jp\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade103
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $upgradeDataObject->getConfigSetup()->saveConfig(Config::XML_PATH_ERP_TO_USE, Config::VALUE_COLUMBUS);
    }
}
