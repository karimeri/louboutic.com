<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;

/**
 * Class Upgrade105
 * @package   Jp\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade105
{
    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        //Hide company on checkout address form on JP
        $upgradeDataObject->getConfigSetup()->saveConfig('customer/address/company_show', null);

        // Prefix options on checkout address form on JP
        $upgradeDataObject->getConfigSetup()->saveConfig('customer/address/prefix_options', 'Mr.;Mrs.');

        //Hide tooltips on checkout address form on JP
        $upgradeDataObject->getConfigSetup()->saveConfig('synolia_standard_design/checkout/display_tooltips', 0);
    }
}
