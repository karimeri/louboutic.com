<?php

namespace Jp\Core\Setup\Upgrade;

use Jp\Core\Setup\UpgradeData;
use Magento\Framework\App\State;
use Magento\Store\Model\ScopeInterface;
use Magento\Tax\Api\Data\TaxRuleInterface;
use Magento\Tax\Api\Data\TaxRuleInterfaceFactory;
use Magento\Tax\Model\Calculation\RateFactory;
use Magento\Tax\Model\Calculation\Rate;
use Magento\Tax\Model\Calculation\RateRepository;
use Magento\Tax\Api\TaxRuleRepositoryInterface;
use Magento\Tax\Api\Data\TaxClassInterfaceFactory;
use Magento\Tax\Api\TaxClassRepositoryInterface;

/**
 * Class Upgrade102
 * @package   Jp\Core\Setup\Upgrade
 * @author    Synolia <contact@synolia.com>
 */
class Upgrade102
{
    /**
     * @var RateFactory
     */
    protected $rateFactory;

    /**
     * @var RateRepository
     */
    protected $rateRepository;

    /**
     * @var TaxRuleInterfaceFactory
     */
    protected $taxRuleDataObjectFactory;

    /**
     * @var TaxClassInterfaceFactory
     */
    protected $taxClassDataObjectFactory;

    /**
     * @var TaxClassRepositoryInterface
     */
    protected $taxClassRepository;

    /**
     * @var TaxRuleRepositoryInterface
     */
    protected $ruleService;

    /**
     * @var array
     */
    protected $taxCalculationRateProductByCode = [];

    /**
     * @var array
     */
    protected $taxCalculationRateShippingByCode = [];

    /**
     * Upgrade102 constructor.
     * @param RateFactory                 $rateFactory
     * @param RateRepository              $rateRepository
     * @param TaxRuleInterfaceFactory     $taxRuleDataObjectFactory
     * @param TaxRuleRepositoryInterface  $ruleService
     * @param TaxClassInterfaceFactory    $taxClassDataObjectFactory
     * @param TaxClassRepositoryInterface $taxClassRepository
     */
    public function __construct(
        RateFactory $rateFactory,
        RateRepository $rateRepository,
        TaxRuleInterfaceFactory $taxRuleDataObjectFactory,
        TaxRuleRepositoryInterface $ruleService,
        TaxClassInterfaceFactory $taxClassDataObjectFactory,
        TaxClassRepositoryInterface $taxClassRepository,
        State $state
    ) {
        $this->rateFactory               = $rateFactory;
        $this->rateRepository            = $rateRepository;
        $this->taxRuleDataObjectFactory  = $taxRuleDataObjectFactory;
        $this->ruleService               = $ruleService;
        $this->taxClassDataObjectFactory = $taxClassDataObjectFactory;
        $this->taxClassRepository        = $taxClassRepository;
        $this->state                     = $state;

        try {
            $this->state->setAreaCode('adminhtml');
        } catch (\Exception $e) {
            //Area code already set
        }
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    public function run(UpgradeData $upgradeDataObject)
    {
        $taxRate = $this->rateRepository->get(1);
        $this->rateRepository->delete($taxRate);
        $taxRate = $this->rateRepository->get(2);
        $this->rateRepository->delete($taxRate);

        $this->saveTaxCalculationRate();
        $this->saveTaxRule();
        $this->saveTaxConfiguration($upgradeDataObject);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveTaxCalculationRate()
    {
        $taxCalculationProductData = [
            'Japan_Products' => [
                'code' => 'Japan_Products',
                'tax_country_id' => 'JP',
                'tax_region_id' => 0,
                'tax_postcode' => '*',
                'rate' => 8.00,
            ]
        ];

        $this->taxCalculationRateProductByCode = $this->saveTaxCalculationArray($taxCalculationProductData);

        $taxCalculationShippingData = [
            'JP_Shipping_Rate' => [
                'code' => 'JP_Shipping_Rate',
                'tax_country_id' => 'JP',
                'tax_region_id' => 0,
                'tax_postcode' => '*',
                'rate' => 8.00,
            ]
        ];

        $this->taxCalculationRateShippingByCode = $this->saveTaxCalculationArray($taxCalculationShippingData);
    }

    /**
     * @param array $taxCalculationArray
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveTaxCalculationArray(array $taxCalculationArray)
    {
        $taxCalculationRateByCode = [];
        foreach ($taxCalculationArray as $tax) {
            $rate = $this->getRateCalculationModel();
            $rate->addData($tax);
            $taxRate                    = $this->rateRepository->save($rate);
            $taxCalculationRateByCode[] = $taxRate->getId();
        }

        return $taxCalculationRateByCode;
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveTaxRule()
    {
        $taxClass               = $this->taxClassDataObjectFactory->create()
            ->setClassType('PRODUCT')
            ->setClassName('Shipping');
        $taxClassShippingRateId = $this->taxClassRepository->save($taxClass);

        $taxRules = [
            'tax_product' => [
                'setCode' => 'Japan Products',
                'setTaxRateIds' => $this->taxCalculationRateProductByCode,
                'setCustomerTaxClassIds' => [3],
                'setProductTaxClassIds' => [2],
                'setPriority' => 1,
                'setPosition' => 1
            ],
            'tax_shipping' => [
                'setCode' => 'Japan Shipment',
                'setTaxRateIds' => $this->taxCalculationRateShippingByCode,
                'setCustomerTaxClassIds' => [3],
                'setProductTaxClassIds' => [$taxClassShippingRateId],
                'setPriority' => 1,
                'setPosition' => 1
            ]
        ];

        foreach ($taxRules as $taxRule) {
            $taxRuleDataObject = $this->getTaxRuleDataObject();
            foreach ($taxRule as $method => $value) {
                $taxRuleDataObject->$method($value);
            }

            $this->ruleService->save($taxRuleDataObject);
        }
    }

    /**
     * @param UpgradeData $upgradeDataObject
     */
    protected function saveTaxConfiguration(UpgradeData $upgradeDataObject)
    {
        $configData = [
            'tax/classes/shipping_tax_class' => 4,
            'tax/calculation/price_includes_tax' => 1,
            'tax/calculation/shipping_includes_tax' => 1,
            'tax/calculation/discount_tax' => 1,
            'tax/defaults/country' => 'JP',
            'tax/display/type' => 2,
            'tax/display/shipping' => 2,
            'tax/cart_display/price' => 2,
            'tax/cart_display/subtotal' => 2,
            'tax/cart_display/shipping' => 2,
            'tax/cart_display/gift_wrapping' => 2,
            'tax/cart_display/printed_card' => 2,
            'tax/sales_display/price' => 2,
            'tax/sales_display/subtotal' => 2,
            'tax/sales_display/shipping' => 2,
            'tax/sales_display/gift_wrapping' => 2,
            'tax/sales_display/printed_card' => 2,
            'tax/sales_display/grand_total' => 2
        ];

        foreach ($configData as $path => $value) {
            $upgradeDataObject->getConfigSetup()->saveConfig($path, $value);
        }
    }

    /**
     * @return TaxRuleInterface
     */
    protected function getTaxRuleDataObject()
    {
        return $this->taxRuleDataObjectFactory->create();
    }

    /**
     * @return Rate
     */
    protected function getRateCalculationModel()
    {
        return $this->rateFactory->create();
    }
}
