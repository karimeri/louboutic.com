<?php

namespace Jp\Core\Setup\Upgrade;

use Synolia\Standard\Setup\Eav\CustomerAddressSetup;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade1011
 * @package Jp\Core\Setup\Upgrade
 * @author  Synolia <contact@synolia.com>
 */
class Upgrade1011
{
    /**
     * @var CustomerAddressSetup
     */
    protected $customerAddressSetup;

    /**
     * @var EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade1011 constructor.
     *
     * @param CustomerAddressSetup $customerAddressSetup
     * @param EavSetup             $eavSetup
     */
    public function __construct(
        CustomerAddressSetup $customerAddressSetup,
        EavSetup $eavSetup
    ) {
        $this->customerAddressSetup = $customerAddressSetup;
        $this->eavSetup             = $eavSetup;
    }


    public function run()
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'firstname_en',
                'entity_type_id' => 2,
                'data' => [
                    'backend_type'       => 'static',
                    'frontend_label'     => 'Firstname EN',
                    'input'              => 'text',
                    'required'           => true,
                    'visible'            => true,
                    'is_user_defined'    => true,
                    'is_system'          => 0,
                    'validate_rules'     => '{"max_text_length":"35","input_validation":"length"}',
                    'sort_order'         => 53,
                    'used_in_forms'      => ['customer_register_address', 'customer_address_edit'],
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'lastname_en',
                'entity_type_id' => 2,
                'data' => [
                    'backend_type'       => 'static',
                    'frontend_label'     => 'Lastname EN',
                    'input'              => 'text',
                    'required'           => true,
                    'visible'            => true,
                    'is_user_defined'    => true,
                    'is_system'          => 0,
                    'validate_rules'     => '{"max_text_length":"35","input_validation":"length"}',
                    'sort_order'         => 55,
                    'used_in_forms'      => ['customer_register_address', 'customer_address_edit'],
                ],
            ],
            [
                'type' => 'customer_address',
                'code' => 'company',
                'data' => [
                    'is_visible' => false,
                ],
            ],
        ];

        foreach ($attributes as $attribute) {
            if (!$this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                $this->eavSetup->addAttribute($attribute['entity_type_id'], $attribute['code'], []);
            }

            foreach ($attribute['data'] as $field => $value) {
                $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
            }
        }
    }
}
