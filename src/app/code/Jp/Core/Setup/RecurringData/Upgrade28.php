<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Jp\Core\Setup\UpgradeData;
use Synolia\Slider\Setup\Eav\SliderSetup;
use Symfony\Component\Console\Output\ConsoleOutput;

class Upgrade28 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var SliderSetup
     */
    protected $sliderSetup;

    /**
     * @var \Jp\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade28 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Jp\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     * @param \Synolia\Slider\Setup\Eav\SliderSetup $sliderSetup
     */
    public function __construct(
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput,
        SliderSetup $sliderSetup
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
        $this->sliderSetup = $sliderSetup;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->addMaterialsSliderJp();
        $this->addEmbellishmentSliderJp();
        $this->addAnimatedShoesSliderJp();

        $cmsPageIdentifier = 'men-city-shoes';

        $cmsPageContent = $this->cmsSetup->getCmsPageContent(
            $cmsPageIdentifier,
            'Jp_Core'
        );

        $cmsPage = [
            'title'             => 'Christian Louboutin - Men City Shoes',
            'page_layout'       => 'fullscreen',
            'identifier'        => 'discover-men-city-shoes.html',
            'content_heading'   => '',
            'content'           => $cmsPageContent,
            'is_active'         => 1,
            'stores'            => [0]
        ];

        $this->cmsSetup->savePage($cmsPage);
    }

    /**
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addMaterialsSliderJp()
    {
        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-first',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-soles-slide-second',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );

        $sliders = [
            [
                'identifier'     => 'discover-soles-slider-jp',
                'title'          => 'DISCOVER - Soles slider - JP',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-soles-slide-1-jp',
                        'title'      => 'ラグソール',
                        'content'    => $secondSlide,
                        'image_big'  => 'step3-sole-img2.png',
                    ],
                    [
                        'identifier' => 'discover-soles-slide-2-jp',
                        'title'      => 'レッドソール',
                        'content'    => $firstSlide,
                        'image_big'  => 'step3-sole-img1.png',
                    ],
                ],
            ],
            [
                'identifier'     => 'discover-materials-slider-jp',
                'title'          => 'DISCOVER - Materials slider - JP',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-materials-slide-1-jp',
                        'title'      => 'カーフスキン',
                        'image_big'  => 'materials1.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-2-jp',
                        'title'      => 'プリンティッドパテントレザー',
                        'image_big'  => 'materials3.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-3-jp',
                        'title'      => 'パテントレザー',
                        'image_big'  => 'materials4.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-4-jp',
                        'title'      => 'レーザーカット',
                        'image_big'  => 'materials6.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-5-jp',
                        'title'      => 'デニム',
                        'image_big'  => 'materials7.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-6-jp',
                        'title'      => 'パテントデグラデ',
                        'image_big'  => 'materials8.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-7-jp',
                        'title'      => 'モアレ',
                        'image_big'  => 'materials9.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-8-jp',
                        'title'      => 'トレッセ',
                        'image_big'  => 'materials10.jpg',
                    ],
                    [
                        'identifier' => 'discover-materials-slide-9-jp',
                        'title'      => 'ベルベット',
                        'image_big'  => 'materials11.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders, true);
    }

    /**
     * @param \Eu\Core\Setup\UpgradeData $upgradeDataObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addAnimatedShoesSliderJp()
    {

        $firstSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-first',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );
        $secondSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-second',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );
        $thirdSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-third',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );
        $fourthSlide = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fourth',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );
        $fifthSlide  = $this->cmsSetup->getCmsBlockContent(
            'discover-animated-slide-fifth',
            'Jp_Core',
            '',
            '',
            'misc/cms/blocks/jp_ja'
        );

        $sliders = [
            [
                'identifier'     => 'discover-animated-shoe-slider-jp',
                'title'          => 'DISCOVER - Animated Shoe Slider - JP',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier'   => 'derby-jp',
                        'title'        => 'ダービー',
                        'image_big'    => 'oxford-sprite-in.png',
                        'image_medium' => 'oxford-sprite-out.png',
                        'image_small'  => 'oxford.svg',
                        'content'      => $firstSlide,
                    ],
                    [
                        'identifier'   => 'loafer-jp',
                        'title'        => 'ローファー',
                        'image_big'    => 'loafer-sprite-in.png',
                        'image_medium' => 'loafer-sprite-out.png',
                        'image_small'  => 'loafer.svg',
                        'content'      => $secondSlide,
                    ],
                    [
                        'identifier'   => 'oxford-jp',
                        'title'        => 'ブローグ',
                        'image_big'    => 'brogue-sprite-in.png',
                        'image_medium' => 'brogue-sprite-out.png',
                        'image_small'  => 'brogue.svg',
                        'content'      => $thirdSlide,
                    ],
                    [
                        'identifier'   => 'ankle-boot-jp',
                        'title'        => 'アンクルブーツ',
                        'image_big'    => 'boot-sprite-in.png',
                        'image_medium' => 'boot-sprite-out.png',
                        'image_small'  => 'boot.svg',
                        'content'      => $fourthSlide,
                    ],
                    [
                        'identifier'   => 'double-monk-strap-jp',
                        'title'        => 'ダブル モンク ストラップ',
                        'image_big'    => 'monkStrap-sprite-in.png',
                        'image_medium' => 'monkStrap-sprite-out.png',
                        'image_small'  => 'monkstrap.svg',
                        'content'      => $fifthSlide,
                    ]
                ]
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($sliders);
    }


    /**
     * @throws \Synolia\Slider\Exception\SlideNotFoundException
     */
    public function addEmbellishmentSliderJp()
    {
        $slider = [
            [
                'identifier'     => 'discover-embellishment-slider-jp',
                'title'          => 'DISCOVER - Embellishment slider - JP',
                'autoplay'       => 0,
                'autoplay_speed' => 0,
                'infinite'       => 0,
                'slides'         => [
                    [
                        'identifier' => 'discover-embellishment-slide-1',
                        'title'      => 'エンブロイダリーズ',
                        'image_big'  => 'embellishments1.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-2',
                        'title'      => 'チェーン',
                        'image_big'  => 'embellishments3.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-3',
                        'title'      => 'デグラスパイク',
                        'image_big'  => 'embellishments4.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-4',
                        'title'      => 'エンベリッシュド　パッチ',
                        'image_big'  => 'embellishments5.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-5',
                        'title'      => 'ピックピック',
                        'image_big'  => 'embellishments8.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-6',
                        'title'      => 'エンブロイダリーズ',
                        'image_big'  => 'embellishments2.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-7',
                        'title'      => 'ストラス',
                        'image_big'  => 'embellishments10.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-8',
                        'title'      => 'スパイク',
                        'image_big'  => 'embellishments9.jpg',
                    ],
                    [
                        'identifier' => 'discover-embellishment-slide-9',
                        'title'      => 'パーフォレーション',
                        'image_big'  => 'embellishments6.jpg',
                    ],
                ],
            ]
        ];

        $this->sliderSetup->saveMultipleSlider($slider, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS page men-city-shoes for Jp';
    }
}
