<?php

namespace Jp\Core\Setup\RecurringData;

use Magento\Framework\DB\Ddl\Table;
use Synolia\Standard\Setup\AbstractSetup;
use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;

/**
 * Class Upgrade18
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade18 implements UpgradeDataSetupInterface
{
    const TABLE_ORDER_ADDRESS = 'sales_order_address';
    const TABLE_QUOTE_ADDRESS = 'quote_address';

    /**
     * @var AbstractSetup
     */
    protected $abstractSetup;

    /**
     * Upgrade18 constructor.
     * @param \Synolia\Standard\Setup\AbstractSetup $abstractSetup
     */
    public function __construct(
        AbstractSetup $abstractSetup
    ) {
        $this->abstractSetup = $abstractSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $columnToAdd = [
            [
                'table' => self::TABLE_ORDER_ADDRESS,
                'name' => 'firstname_en',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Firstname EN'
                ]
            ],
            [
                'table' => self::TABLE_ORDER_ADDRESS,
                'name' => 'lastname_en',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Lastname EN'
                ]
            ],
            [
                'table' => self::TABLE_QUOTE_ADDRESS,
                'name' => 'firstname_en',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Firstname EN'
                ]
            ],
            [
                'table' => self::TABLE_QUOTE_ADDRESS,
                'name' => 'lastname_en',
                'definition' => [
                    'nullable' => true,
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Lastname EN'
                ]
            ]
        ];

        foreach ($columnToAdd as $column) {
            $this->abstractSetup->addColumn(
                $column['table'],
                $column['name'],
                $column['definition']
            );
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Add custom attributes firstname_en lastname_en for sales_order_address and quote_address';
    }
}
