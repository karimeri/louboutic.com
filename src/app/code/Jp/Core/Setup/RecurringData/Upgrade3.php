<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade3
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade3 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade3 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('louboutin_wms/retailer_import/isMaster', '0');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Retailer import config Jp';
    }
}
