<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;
use Jp\Core\Setup\UpgradeData;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class Upgrade33
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade33 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * @var \Jp\Core\Setup\UpgradeData
     */
    protected $upgradeData;

    /**
     * @var \Symfony\Component\Console\Output\ConsoleOutput
     */
    protected $consoleOutput;

    /**
     * Upgrade33 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     * @param \Jp\Core\Setup\UpgradeData $upgradeData
     * @param \Symfony\Component\Console\Output\ConsoleOutput $consoleOutput
     */
    public function __construct(
        CmsSetup $cmsSetup,
        UpgradeData $upgradeData,
        ConsoleOutput $consoleOutput
    ) {
        $this->cmsSetup = $cmsSetup;
        $this->upgradeData = $upgradeData;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {

        $cmsBlockIdentifier = 'newsletter-infos-bottom-footer';

        foreach ($this->upgradeData->getStores() as $locale => $stores) {
            $cmsBlockContent = $this->cmsSetup->getCmsBlockContent(
                $cmsBlockIdentifier,
                'Jp_Core',
                '',
                '',
                'misc/cms/blocks/'.$locale
            );

            $cmsBlock = [
                'title' => 'FOOTER - newsletter-infos',
                'identifier' => $cmsBlockIdentifier,
                'content' => $cmsBlockContent,
                'is_active' => 1,
                'store_id'   => $stores,
                'stores'     => $stores
            ];

            $this->consoleOutput->writeln('Saving block '.$cmsBlockIdentifier.' on '
                                          .$locale.' stores ('.implode(',', $stores).') ');

            $this->cmsSetup->saveBlock($cmsBlock);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS Block account-create-infos for JP';
    }
}
