<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade21
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade21 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade21 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsPageIdentifier = 'bikini-questa-sera';

        $cmsPageContent = $this->cmsSetup->getCmsPageContent(
            $cmsPageIdentifier,
            'Jp_Core'
        );

        $cmsPage = [
            'title'             => 'Christian Louboutin - Bikini Questa Sera',
            'page_layout'       => 'fullscreen',
            'identifier'        => 'bikini-questa-sera.html',
            'content_heading'   => '',
            'content'           => $cmsPageContent,
            'is_active'         => 1,
            'stores'            => [0]
        ];


        $this->cmsSetup->savePage($cmsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS page bikini-questa-sera for JP';
    }
}
