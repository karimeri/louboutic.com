<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade22
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade22 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade22 constructor.
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        CmsSetup $cmsSetup
    ) {
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * @param \Synolia\Standard\Setup\Upgrade $upgradeObject
     * @throws \Magento\Framework\Exception\FileSystemException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function run(Upgrade $upgradeObject)
    {
        $cmsPageIdentifier = 'tornade-blonde';

        $cmsPageContent = $this->cmsSetup->getCmsPageContent(
            $cmsPageIdentifier,
            'Jp_Core'
        );

        $cmsPage = [
            'title'             => 'Christian Louboutin - Tornade Blonde',
            'page_layout'       => 'fullscreen',
            'identifier'        => 'tornade-blonde.html',
            'content_heading'   => '',
            'content'           => $cmsPageContent,
            'is_active'         => 1,
            'stores'            => [0]
        ];


        $this->cmsSetup->savePage($cmsPage);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Add CMS page tornade-blonde for JP';
    }
}
