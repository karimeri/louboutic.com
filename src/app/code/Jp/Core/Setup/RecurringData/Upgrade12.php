<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\EavSetup;
use Synolia\Standard\Setup\CmsSetup;

/**
 * Class Upgrade12
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade12 implements UpgradeDataSetupInterface
{
    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * @var \Synolia\Standard\Setup\CmsSetup
     */
    protected $cmsSetup;

    /**
     * Upgrade12 constructor.
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     * @param \Synolia\Standard\Setup\CmsSetup $cmsSetup
     */
    public function __construct(
        EavSetup $eavSetup,
        CmsSetup $cmsSetup
    ) {
        $this->eavSetup = $eavSetup;
        $this->cmsSetup = $cmsSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $attributes = [
            [
                'type' => 'customer_address',
                'code' => 'firstname',
                'data' => [
                    'sort_order' => 40,
                ]
            ],
            [
                'type' => 'customer_address',
                'code' => 'lastname',
                'data' => [
                    'sort_order' => 20,
                ]
            ],
            [
                'type' => 'customer_address',
                'code' => 'firstname_en',
                'data' => [
                    'sort_order' => 55,
                ]
            ],
            [
                'type' => 'customer_address',
                'code' => 'lastname_en',
                'data' => [
                    'sort_order' => 53,
                ]
            ]
        ];

        foreach ($attributes as $attribute) {
            foreach ($attribute['data'] as $field => $value) {
                if ($this->eavSetup->getAttribute($attribute['type'], $attribute['code'], 'attribute_id')) {
                    $this->eavSetup->updateAttribute($attribute['type'], $attribute['code'], $field, $value);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Set sort order of customer address fields for JP';
    }
}
