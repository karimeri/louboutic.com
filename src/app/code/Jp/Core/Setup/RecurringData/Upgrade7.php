<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Synolia\Standard\Setup\Eav\ConfigSetup;

/**
 * Class Upgrade7
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade7 implements UpgradeDataSetupInterface
{
    /**
     * @var ConfigSetup
     */
    protected $configSetup;

    /**
     * Upgrade7 constructor.
     * @param ConfigSetup $configSetup
     */
    public function __construct(
        ConfigSetup $configSetup
    ) {
        $this->configSetup = $configSetup;
    }

    /**
     * {@inheritdoc}
     * phpcs:disable Generic.Files.LineLength
     */
    public function run(Upgrade $upgradeObject)
    {
        $this->configSetup->saveConfig('synolia_retailer/map/map_marker', 'default/point-map-small.png');
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Retailer marker config JP';
    }
}
