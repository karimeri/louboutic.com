<?php

namespace Jp\Core\Setup\RecurringData;

use Synolia\Standard\Setup\Upgrade;
use Synolia\Standard\Setup\Upgrade\UpgradeDataSetupInterface;
use Magento\Eav\Model\Config;
use Synolia\Standard\Setup\Eav\EavSetup;

/**
 * Class Upgrade20
 * @package Jp\Core\Setup\RecurringData
 * @author Synolia <contact@synolia.com>
 */
class Upgrade20 implements UpgradeDataSetupInterface
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @var \Synolia\Standard\Setup\Eav\EavSetup
     */
    protected $eavSetup;

    /**
     * Upgrade20 constructor.
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Synolia\Standard\Setup\Eav\EavSetup $eavSetup
     */
    public function __construct(
        Config $eavConfig,
        EavSetup $eavSetup
    ) {
        $this->eavConfig = $eavConfig;
        $this->eavSetup = $eavSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function run(Upgrade $upgradeObject)
    {
        $genderAttribute = $this->eavConfig->getAttribute('customer', 'gender');
        $options = $genderAttribute->getSource()->getAllOptions();

        foreach ($options as $option) {
            if ($option['label'] === "Not Specified") {
                $options['delete'][$option['value']] = true;
                $options['value'][$option['value']] = true;
            }
        }

        $this->eavSetup->addAttributeOption($options);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Remove Not Specified option for gender attribute for JP';
    }
}
