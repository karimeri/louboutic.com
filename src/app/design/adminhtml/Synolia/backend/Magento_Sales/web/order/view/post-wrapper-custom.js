define([
    'jquery',
    'Magento_Ui/js/modal/confirm',
    'mage/translate'
], function ($, confirm) {
    'use strict';

    var $cancelButtons = $('#order-view-cancel-button, #order_cancel_offline');

    /**
     * @param {String} url
     * @returns {Object}
     */
    function getForm(url) {
        return $('<form>', {
            'action': url,
            'method': 'POST'
        }).append($('<input>', {
            'name': 'form_key',
            'value': window.FORM_KEY,
            'type': 'hidden'
        }));
    }

    $cancelButtons.click(function () {

        var msg = $.mage.__('Are you sure you want to cancel this order?'),
            url = $('#order-view-cancel-button').data('url');


        confirm({
            'content': msg,
            'actions': {

                /**
                 * 'Confirm' action handler.
                 */
                confirm: function () {
                    $cancelButtons.attr('disabled', true);
                    getForm(url).appendTo('body').submit();
                }
            }
        });

        return false;
    });

    $('#order-view-hold-button').click(function () {
        var url = $('#order-view-hold-button').data('url');

        getForm(url).appendTo('body').submit();
    });

    $('#order-view-unhold-button').click(function () {
        var url = $('#order-view-unhold-button').data('url');

        getForm(url).appendTo('body').submit();
    });

});
