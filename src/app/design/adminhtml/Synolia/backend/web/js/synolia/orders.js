require([
    'jquery',
    'jquery/ui',
    'Magento_Ui/js/grid/listing'
], function($) {
    'use strict';

    function colorizeRow(container, color) {
        container.parents('tr').css({'background-color':color});
        container.siblings('td').css({'background-color':'inherit'});
        //overriding parent's cell color for resolving bug on target
        container.parents('td').css({'background-color':'white'});
    }

    $(document).on('afterHideLoader', function() {
        //Reinitiate rows background-color
        $("table.data-grid tr").css({'background':'inherit'});

        //Colorize row with status ; Label generation error
        var $generationError = $('.status > div:contains("Label generation error")');
        $generationError.each(function(){
            colorizeRow( $(this).parents('.status'), 'orange');
        });

        //Colorize row with isFraud ; Yes
        var $yes = $('.fraud > div:contains("Yes")');
        $yes.each(function(){
            colorizeRow( $(this).parents('.fraud'),'red');
        });
    });
});