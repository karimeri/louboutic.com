define([
    'ko',
    'underscore',
    'Magento_Ui/js/lib/spinner',
    'rjsResolver',
    'uiLayout',
    'uiCollection',
    'jquery'
], function (ko, _, loader, resolver, layout, Collection, $) {
    'use strict';

    var mixin = {
        /**
         * Hides loader.
         */
        hideLoader: function () {
            this._super();
            $(document).trigger('afterHideLoader');
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
