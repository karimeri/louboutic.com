var config = {

    config: {
        mixins: {
            'Magento_Ui/js/grid/listing': {
                'js/synolia/grid/listing-mixin': true
            }
        }
    },

    deps: [
        'js/synolia/orders'
    ]
};