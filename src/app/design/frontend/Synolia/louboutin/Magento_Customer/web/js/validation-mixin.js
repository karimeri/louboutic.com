define([
    'jquery'
], function ($) {
    "use strict";

    return function () {
        $.validator.addMethod(
            'phone-eu',
            function (value) {
                return /^(?:\+[0-9]{0,3}|\([0-9]{0,3}\)|0)\s{0,1}[0-9]{6,14}$/.test(value)
            },
            $.mage.__('Please insert a valid phone number starting with the international country code. For example : +44630124563 for the United Kingdom')
        );
    }
});
