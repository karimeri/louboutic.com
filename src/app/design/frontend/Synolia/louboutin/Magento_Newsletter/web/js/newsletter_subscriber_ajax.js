require.config({
    deps: [
        'jquery'
    ],
    callback: function ($) {
        var form = $('form.subscribe');

        form.submit(function(e) {
            if(form.validation('isValid')){
                var email = $("#newsletter").val();
                var url = form.attr('action');
                var loadingMessage = $('#loading-message');

                if(loadingMessage.length == 0) {
                    form.find('.email-wrapper').append('<div id="loading-message" style="display:none;padding-top:10px;">&nbsp;</div>');
                    var loadingMessage = $('#loading-message');
                }

                e.preventDefault();
                try{
                    loadingMessage.html($.mage.__('Submitting...')).show();
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        type: 'POST',
                        data: {email: email},
                        success: function (data){
                            if(data.status != "ERROR"){
                                $('#newsletter').val('');
                            }
                            loadingMessage.html(data.msg);
                            if(data.status != "ERROR"){
                                $('#loading-message').addClass('success');
                                window.dataLayer.push({
                                    'event': 'gtm.ext.event',
                                    'eventTmp': 'newsletterSubscription',
                                    'eventCategory': 'newsletterSubscription',
                                    'eventAction': 'stayConnected',
                                    'eventLabel': document.URL
                                });
                            } else {
                                $('#loading-message').addClass('error');
                            }
                        },
                        complete: function(){
                            setTimeout(function(){
                                loadingMessage.hide();
                                loadingMessage.removeClass('error').removeClass('success');
                            },5000);
                        }
                    });
                } catch (e){
                    loadingMessage.html(e.message);
                }
            }
            return false;
        });
    }
})