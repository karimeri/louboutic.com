define([
    'jquery',
    'jquery/ui',
    'mage/translate',
    'mage/url',
    'storeSwitcherPopup',
    'Magento_Ui/js/modal/modal',
    'text!storeswitcher/template/modal/modal-storeswitcher-redirect.html',
    'Synolia_StoreSwitcher/js/ajax',
    'domReady!'
], function (
    $,
    ui,
    $t,
    url,
    storeSwitcherPopup,
    modal,
    modalRedirectTemplate
) {

    $.widget('project.storeswitcherajax', $.synolia.storeswitcherajax, {
        options: {
            modalRedirectTemplate: modalRedirectTemplate,
            modalRedirectContainer: 'store-switcher-modal-redirect',
            cookieName: 'store_switcher_country_choice'
        },

        _ajaxResponseActions: function(response) {
            $(document).trigger('store-switcher-ajax-after');

            if (response.action === 'redirect' && response.redirect_url !== null) {
                window.location.href = response.redirect_url;
                return;
            }

            this.ajaxResponse = response;
            this._initializeCountryPopup();

            if (response.action === 'openPopup' && response.jsonConfig !== null) {
                if (this._getCookies(this.options.cookieName)==='') { // user has made no choice
                    this._showRedirectPopup();
                }
            }
        },

        /* Initial module popup */
        _initializeCountryPopup: function() {
            var modalContainerClass = '.'+this.options.modalContainer;

            $('<div>', {
                class: this.options.modalContainer
            }).appendTo('body');

            $(modalContainerClass).modalStoreswitcher(_.extend(
                this.options.modalStoreSwitcherOptions,
                this.ajaxResponse
            ));
        },

        /* Show popup for the user to choose between */
        _showRedirectPopup: function() {
            if (!this.ajaxResponse.suggestedCountry ||
                !this.ajaxResponse.currentCountry ||
                !this.ajaxResponse.suggestedUrl
            ) {
                return false;
            }

            var modalContainerClass = '.'+this.options.modalRedirectContainer;

            $('<div>', {
                class: this.options.modalRedirectContainer
            }).appendTo('body');

            var redirectModal = $(modalContainerClass).modal({
                modalClass: this.options.modalRedirectContainer,
                popupTpl: this.options.modalRedirectTemplate,
                mediaImg: url.buildPub('Synolia_StoreSwitcher/images/compass.png'),
                intro: $t('It appears you are located in %1. How would you like to proceed ?')
                    .replace('%1', this.ajaxResponse.suggestedCountry),
                suggestedCountryPhrase: $t('Visit %1 site')
                    .replace('%1', this.ajaxResponse.suggestedCountry),
                suggestedUrl: this.ajaxResponse.suggestedUrl,
                currentCountryPhrase: $t('Continue browsing %1 site')
                    .replace('%1', this.ajaxResponse.currentCountry),
                currentUrl: window.location.href
            });

            redirectModal.modal('openModal');

            this._bindRedirectPopupEvents();
        },

        _bindRedirectPopupEvents: function() {
            var modalContainerClass = '.'+this.options.modalRedirectContainer;

            $(modalContainerClass).on('click', '.suggested-url', this._redirectToSuggestedWebsite.bind(this));
            $(modalContainerClass).on('click', '.current-url', this._stayOnCurrentWebsite.bind(this));
        },

        _redirectToSuggestedWebsite: function(event) {
            this._createCookie(this.options.cookieName, this.ajaxResponse.suggestedCountry);
            return true;
        },

        _stayOnCurrentWebsite: function(event) {
            this._createCookie(this.options.cookieName, this.ajaxResponse.currentCountry);
            return true;
        },


        /* Cookie methods s*/
        _createCookie: function (name, value, days) {
            var expires = '';

            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = '; expires=' + date.toGMTString();
            }

            document.cookie = name + '=' + value + expires + '; path=/;';
        },

        _getCookies: function (cname) {
            var name = cname + '=';
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return '';
        }
    });

    return $.project.storeswitcherajax;
});