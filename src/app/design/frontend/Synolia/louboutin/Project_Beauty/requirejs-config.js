var config = {
    map: {
        '*': {
            wheelSlider   : 'Project_Beauty/js/wheel-slider',
            videoLips     : 'Project_Beauty/js/video-lips',
            discoverShoes : 'Project_Beauty/js/discover-shoes',
            discoverNails : 'Project_Beauty/js/nails/discover-nails',
            nailsCover    : 'Project_Beauty/js/nails/nails-cover',
            nailsWheel    : 'Project_Beauty/js/nails/nails-wheel',
            nailsRed      : 'Project_Beauty/js/nails/nails-red',
            discoverRed   : 'Project_Beauty/js/discover-red',
            imageRotation : 'Project_Beauty/js/image-rotation'
        }
    }
};
