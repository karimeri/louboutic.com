define([
    'jquery',
    'TweenLite',
    'TimelineLite'
], function($) {
    'use strict';

    /**
     * Parallax effect
     */
    $.widget('synolia.nailsRed', {

        options: {
            abstract: null,
            imageSelector: '.image-holder'
        },

        /**
         * @private
         */
        _create: function () {
            this._initElements();

            _.bindAll(this, '_setRedParallax');
            this.abstract.$window.on('scroll', _.throttle(this._setRedParallax, 12));
        },

        /**
         * @private
         */
        _initElements: function () {
            this.abstract = this.options.abstract;
            this.$image   = this.element.find(this.options.imageSelector);
        },

        /**
         * @private
         */
        _setRedParallax: function () {
            if (this.abstract.$window.width() <= this.abstract.options.desktopBreakpoint){
                return;
            }

            // eslint-disable-next-line vars-on-top
            var scrollTop       = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
                position        = scrollTop + this.abstract.options.headerHeight.desktop,
                heightElement   = this.abstract.$red.height(),
                parallaxHeight  = heightElement / 2,
                redTop          = this.abstract.$red.offset().top,
                redOffsetBottom = redTop + heightElement,
                delta;

            this.easedDelta = 0;

            if (position + heightElement >= redTop && position <= redOffsetBottom) {
                // eslint-disable-next-line no-extra-parens
                delta = ((1 - (redOffsetBottom - position) / (heightElement * 2)) * parallaxHeight);
                this.easedDelta += (delta - this.easedDelta) * 0.8;

                this.$image.css({
                    transform: 'translate3d(0, ' + -this.easedDelta + 'px, 0)'
                });
            }
        }
    });

    return $.synolia.nailsWheel;
});