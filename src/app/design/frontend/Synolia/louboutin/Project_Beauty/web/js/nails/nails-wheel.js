/*global TweenLite*/
/*global TimelineLite*/
define([
    'jquery',
    'matchMedia',
    'TweenLite-css',
    'TimelineLite',
    'synolia_slider_init'
], function($, mediaCheck) {
    'use strict';

    /**
     * Nails wheel with menu
     * Slider on mobile
     * Step 1 : Complete wheel
     * Step 2 : Products from one category
     */
    $.widget('synolia.nailsWheel', {

        options: {
            abstract: null,
            class: {
                wheelWrapper: 'wheel-wrapper',
                menuActive: 'active',
                step2: 'step-2',
            },
            selectors: {
                mainWrapper: '#category-selector',
                info: '.info',
                infoBottle: '.bottle',
                infoCategory: '.category',
                categoriesMenuElements: '.categories-menu li',
                backButton: '.back-btn'
            },
            data: {
                productName: 'name',
                productCategory: 'category-human-readable',
                productCategoryKey: 'category-beauty'
            },
            timer: 0.3,
            sliderOptions: {
                mobileFirst: true,
                variableWidth: true,
                centerMode: true,
                prevArrow: '.info .prev',
                nextArrow: '.info .next',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: 'unslick'
                    }
                ]
            }
        },

        categoryBottlesInfoVisible: false,
        animate: false, // True when a category is displaying

        /**
         * @private
         */
        _create: function () {
            this._initElements()
                ._initCategoriesEvent()
                ._initSlider()
                ._initResponsive();
        },

        _initResponsive: function () {
            mediaCheck({
                media: '(max-width: ' + this.abstract.options.desktopBreakpoint + 'px)',
                entry: function () {
                    this._setMobile();
                }.bind(this),
                exit: function () {
                    this._setDesktop();
                }.bind(this)
            });
        },

        /**
         * @private
         */
        _setMobile: function () {
            var slick = this.$slider.slick('getSlick');

            slick.resize();
            slick.slickGoTo(slick.slickCurrentSlide(), true);
            this.$info.attr('style', '');
            this.$productElements.off('mouseenter mouseleave click');
        },

        /**
         * @private
         */
        _setDesktop: function () {
            this.$infoBottle.text('');
            this.$infoCategory.text('');
            this.$info.find('a').remove();
            this._initWheelEvents();
        },

        /**
         * @returns {synolia.nailsWheel}
         * @private
         */
        _initElements: function () {
            // Abstract
            this.abstract = this.options.abstract;

            // Wrapper
            this.$mainWrapper  = $(this.options.selectors.mainWrapper);
            this.$wheelWrapper = this.$mainWrapper.find('.' + this.options.class.wheelWrapper);

            // Products
            this.$productElements = this.$wheelWrapper.find('li');
            this.$info            = this.$mainWrapper.find(this.options.selectors.info);
            this.$infoBottle      = this.$info.find(this.options.selectors.infoBottle);
            this.$infoCategory    = this.$info.find(this.options.selectors.infoCategory);

            // Categories
            this.$categoriesMenuElements = this.$mainWrapper.find(this.options.selectors.categoriesMenuElements);
            this.$backButton             = this.$mainWrapper.find(this.options.selectors.backButton);

            // Slider
            this.$slider = this.$wheelWrapper.children('ul');

            // Add reverse method to jQuery
            jQuery.fn.reverse = [].reverse;

            return this;
        },

        /**
         * @returns {synolia.nailsWheel}
         * @private
         */
        _initWheelEvents: function () {
            _.bindAll(this, '_updateProductInfo', '_hideProductInfo', '_clickProductEvent');
            this.$productElements
                .on('mouseenter', this._updateProductInfo)
                .on('mouseleave', this._hideProductInfo)
                .on('click', this._clickProductEvent);

            return this;
        },

        /**
         * Update and show product info on hover
         * @param event
         * @private
         */
        _updateProductInfo: function (event) {
            var $element,
                currentCategory,
                liSelector;

            if (this.animate) {
                return;
            }

            $element        = $(event.currentTarget);
            currentCategory = $element.data('category-beauty');

            this.$infoBottle.text($element.data(this.options.data.productName));
            this.$infoCategory.text($element.data(this.options.data.productCategory));

            this.$categoriesMenuElements.removeClass(this.options.class.menuActive);
            this.$categoriesMenuElements.parent().find('li[data-category-beauty="' + currentCategory + '"]')
                .addClass(this.options.class.menuActive);

            // Stop the hiding when information for another product needs to be displayed
            if (this.hideProductInfoTimeout) {
                clearTimeout(this.hideProductInfoTimeout);
            }

            // We want this effect only on step 1 when this.$info is hidden
            if (!this.categoryBottlesInfoVisible && !this.$wheelWrapper.hasClass(this.options.class.step2)) {
                this.categoryBottlesInfoVisible = true;
                TweenLite.fromTo(this.$info, this.options.timer, {
                    opacity: 0,
                    y: 10
                }, {
                    opacity: 1,
                    y: 0
                });
            }

            // Show products from the same category
            liSelector = 'li[data-' + this.options.data.productCategoryKey + '="' + currentCategory + '"]';
            TweenLite.to(this.$wheelWrapper.find(liSelector), this.options.timer, {
                opacity: 1
            });

            // Hide other products
            liSelector = 'li:not([data-' + this.options.data.productCategoryKey + '="' + currentCategory + '"])';
            TweenLite.to(this.$wheelWrapper.find(liSelector), this.options.timer, {
                opacity: 0.2
            });
        },

        /**
         * Hide product info with animation and delay
         * @private
         */
        _hideProductInfo: function () {
            if (this.animate) {
                return;
            }

            this.hideProductInfoTimeout = setTimeout(function () {
                // Only on Step 1
                if (this.categoryBottlesInfoVisible && !this.$wheelWrapper.hasClass(this.options.class.step2)) {
                    TweenLite.to(this.$info, this.options.timer, {
                        opacity: 0,
                        y: 10,
                        onComplete: function() {
                            this.categoryBottlesInfoVisible = false;
                        }.bind(this)
                    });

                    TweenLite.to(this.$wheelWrapper.find('li'), this.options.timer, {
                        opacity: 1
                    });

                    this.$categoriesMenuElements.removeClass(this.options.class.menuActive);
                } else { // On step 2, we reset this var
                    this.categoryBottlesInfoVisible = false;
                }
            }.bind(this), 500);
        },

        /**
         * @param event
         * @private
         */
        _clickProductEvent: function (event) {
            var $currentTarget = $(event.currentTarget),
                $categoryElement;

            // Step 2 - Redirection to product page
            if (this.$wheelWrapper.hasClass(this.options.class.step2)) {
                window.location.href = $currentTarget.find('a').attr('href');
            } else { // Step 1 - Display products with the same category
                $categoryElement = this.$categoriesMenuElements
                    .parent()
                    .find(
                        'li[data-category-beauty="' + $currentTarget.data('category-beauty') + '"]'
                    );

                $categoryElement.addClass(this.options.class.menuActive);
                this._displayProductsCategory($categoryElement);
            }
        },

        /**
         * @returns {synolia.nailsWheel}
         * @private
         */
        _initCategoriesEvent: function () {
            _.bindAll(this, '_clickMenuEvent', '_clickBackButtonEvent');
            this.$categoriesMenuElements.on('click', this._clickMenuEvent);
            this.$backButton.on('click', this._clickBackButtonEvent);

            return this;
        },

        /**
         * @param event
         * @private
         */
        _clickMenuEvent: function (event) {
            this._displayProductsCategory($(event.currentTarget));
        },

        /**
         * Display products from one category
         * @param $categoryElement
         * @private
         */
        _displayProductsCategory: function ($categoryElement) {
            var timeline           = new TimelineLite(),
                productCategoryKey = $categoryElement.data(this.options.data.productCategoryKey),
                $currentProducts   = this.$wheelWrapper.find(
                    'li[data-' + this.options.data.productCategoryKey + '="' + productCategoryKey + '"]'
                ),
                $firstProduct      = $($currentProducts[0]),
                $otherProducts;

            this.animate = true;

            // Menu
            this.$categoriesMenuElements.removeClass(this.options.class.menuActive);
            $categoryElement.addClass(this.options.class.menuActive);

            // Start of effects
            timeline.to(this.$info, this.options.timer, {
                opacity: 0,
                y: 10,
                onComplete: function () {
                    this.$infoBottle.text('');
                    this.$infoCategory.text($firstProduct.data(this.options.data.productCategory));
                }.bind(this)
            });

            if (!this.$wheelWrapper.hasClass(this.options.class.step2)) { // Step 1
                timeline.staggerTo(this.$productElements, this.options.timer, {
                    opacity: 0
                }, 0.03);
            } else { // Step 2
                $otherProducts = this.$wheelWrapper.find(
                    'li[data-' + this.options.data.productCategoryKey + '="' + this.productCategoryKey + '"]'
                );
                timeline.staggerTo($otherProducts, this.options.timer, {
                    opacity: 0
                }, 0.03);
            }

            timeline.append(function() {
                this.$productElements.css({
                    display: 'none',
                    opacity: 1
                });

                $currentProducts.css({
                    display: 'block',
                    opacity: 0
                });

                this.$wheelWrapper
                    .removeClass()
                    .addClass(
                        this.options.class.wheelWrapper + ' ' + this.options.class.step2 + ' ' + productCategoryKey
                    );

                this.productCategoryKey = productCategoryKey;
            }.bind(this));

            timeline.staggerTo($currentProducts, this.options.timer, {
                opacity: 1
            }, 0.03, '+=.3');

            timeline.to(this.$info, this.options.timer, {
                opacity: 1,
                y: 0
            });

            timeline.to(this.$backButton , this.options.timer, {
                display: 'block',
                opacity: 1,
                bottom: 40,
                onComplete: function () {
                    this.animate = false;
                }.bind(this)
            });
        },

        /**
         * Go to step 1 with animation
         * @private
         */
        _clickBackButtonEvent: function () {
            var timeline   = new TimelineLite(),
                liSelector = 'li[data-' + this.options.data.productCategoryKey + '="' + this.productCategoryKey + '"]';

            TweenLite.to(this.$info, this.options.timer, {
                opacity: 0,
                y: 10
            });

            timeline.to(this.$backButton, this.options.timer, {
                display: 'none',
                opacity: 0,
                bottom: 20
            });

            timeline.staggerTo( this.$wheelWrapper.find(liSelector).reverse(), this.options.timer, {
                opacity: 0
            }, 0.03);

            timeline.append(function() {
                this.$productElements.css({
                    display: 'block',
                    opacity: 0
                });

                this.$wheelWrapper
                    .removeClass()
                    .addClass(this.options.class.wheelWrapper);
            }.bind(this));

            timeline.staggerTo(this.$productElements, this.options.timer, {
                opacity: 1
            }, 0.03);

            this.$categoriesMenuElements.removeClass(this.options.class.menuActive);
        },

        /**
         * @returns {synolia.nailsWheel}
         * @private
         */
        _initSlider: function () {
            _.bindAll(this, '_setSlideWidth', '_setProductInfoSlider');

            this.$slider
                .on('init breakpoint', this._setSlideWidth)
                .on('afterChange', this._setProductInfoSlider);
            this.abstract.$window.on('resize orientationchange', _.throttle(this._setSlideWidth, 100));

            this.$slider.slick(this.options.sliderOptions);

            return this;
        },

        /**
         * @param event
         * @param slick
         * @param currentSlide
         * @private
         */
        _setProductInfoSlider: function (event, slick, currentSlide) {
            var $currentSlide = $(slick.$slides[currentSlide]);

            this.$infoBottle.text($currentSlide.data(this.options.data.productName));
            this.$infoCategory.text($currentSlide.data(this.options.data.productCategory));
            this.$info.find('a').remove();
            this.$info.append($currentSlide.find('a').clone());
        },

        /**
         * @private
         */
        _setSlideWidth: function () {
            this.$slider.find('.slick-slide').css({
                'width': this.abstract.$window.width() / 2
            });
        }
    });

    return $.synolia.nailsWheel;
});
