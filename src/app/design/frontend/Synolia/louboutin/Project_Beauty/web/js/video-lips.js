define([
    'jquery',
    'matchMedia',
    'video',
    'jquery/ui'
], function ($, mediaCheck) {
    'use strict';

    $.widget('synolia.videoLips', $.synolia.video, {

        options: {
            mqDesktop: '(min-width: 770px)',
            lips: {
                arrow: '.arrow',
                videoText: '.video-text'
            },
            slider : '.wheel-slider'
        },

        $lipsArrow: {},
        $lipsVideoText: {},
        $wheelSlider: {},
        isFirstWheel: true,

        _init: function() {
            this._initLipsVars();
            this._super();
        },

        _initBindings: function() {
            this._super();

            this.$lipsArrow.on('click', this.scrollToWheel.bind(this));
        },

        _initMediaCheck: function() {
            mediaCheck({
                media: this.options.mqDesktop,
                entry: function () {
                    this._initLipsVideo();
                }.bind(this),
                exit: function () {
                    this._initMobile();
                }.bind(this)
            });
        },

        /**
         *  Init lips vars
         */
        _initLipsVars: function() {
            this.$lipsArrow     = this.element.find(this.options.lips.arrow);
            this.$lipsVideoText = this.element.find(this.options.lips.videoText);
            this.$wheelSlider   = $(this.options.slider);
        },

        /**
         * Init lips video
         */
        _initLipsVideo: function() {
            this.element.removeClass('play')
                .addClass(this.options.mute);

            /* Video not played infinite */
            this.changeAttribute(this.options.video.attribute.loop, false);

            this.$video.show();

            /* On window scroll video is launched */
            this.element.on('wheel', this.lipsLaunchVideo.bind(this));
        },

        /**
         *  Hide arrow and video text
         */
        hideText: function() {
            this.$lipsArrow.addClass('hidden');
            this.$lipsVideoText.removeClass('visible')
                               .addClass('hidden');
        },

        scrollToWheel: function() {
            var padding = 30;
            $('html, body').animate({
                scrollTop: this.$wheelSlider.offset().top - padding
            }, 2000);
        },

        /**
         * Launch video for discover lips page
         * @param e
         */
        lipsLaunchVideo: function(e) {
            /* Prevent scrolling page */
            e.preventDefault();

            if(this.isFirstWheel) {
                this.hideText();
                this.playVideo();
                this._initVideoBindings();
            }

            this.isFirstWheel = false;
        },

        /**
         * Init video bindings
         * @private
         */
        _initVideoBindings: function() {
            _.bindAll(this, 'scrollOnTime', 'hideLipsVideo');

            this.$video.on('timeupdate', this.scrollOnTime);
            this.$video.on('ended', this.hideLipsVideo);
        },

        /**
         * Scroll to next element on video time update
         */
        scrollOnTime: function() {
            var timeLimit = this.$video.get(0).duration - 3,
                padding = 110;

            if (this.$video.get(0).currentTime > timeLimit) {
                $('html, body').animate({
                    scrollTop: this.$wheelSlider.offset().top - padding
                }, 2000);

                this.$video.off('timeupdate');
            }
        },

        /**
         *  Hide video and show text
         */
        hideLipsVideo: function() {
            this.element.off('wheel');
            this.$video.hide();
            this.$lipsVideoText.removeClass('hidden')
                               .addClass('visible');
            this.$lipsVideoText.find('img').show();
        }

    });

    return $.synolia.wheelSlider;
});