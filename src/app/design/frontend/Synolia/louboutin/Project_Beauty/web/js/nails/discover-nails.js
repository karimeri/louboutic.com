define([
    'jquery',
    'nailsCover',
    'nailsWheel',
    'nailsRed',
    'pageLoader'
], function($) {
    'use strict';

    /**
     * Main widget for nails page
     * Working like abstract class
     */
    $.widget('synolia.discoverNails', {

        options: {
            desktopBreakpoint: 1024,
            headerHeight: {
                mobile: 50,
                desktop: 90
            },
            selectors: {
                cover: '#cover',
                categorySelector: '#category-selector',
                red: '#red-louboutin',
                window: window
            }
        },

        /**
         * @private
         */
        _create: function () {
            $(document).pageLoader();

            this._initElements()
                ._initElementHeight();

            this.element.nailsCover({abstract: this});
            this.element.nailsWheel({abstract: this});
            this.element.nailsRed({abstract: this});
            this.element.show();
        },

        /**
         * @returns {synolia.discoverNails}
         * @private
         */
        _initElements: function () {
            this.$cover            = this.element.find(this.options.selectors.cover);
            this.$categorySelector = this.element.find(this.options.selectors.categorySelector);
            this.$red              = this.element.find(this.options.selectors.red);
            this.$window           = $(this.options.selectors.window);

            return this;
        },

        /**
         * @private
         */
        _initElementHeight: function () {
            this._setElementHeight();

            _.bindAll(this, '_setElementHeight');
            this.$window.on('resize', _.throttle(this._setElementHeight, 100));
        },

        /**
         * @returns {synolia.discoverNails}
         * @private
         */
        _setElementHeight: function () {
            var viewportHeight = window.innerHeight ? window.innerHeight : document.viewport.getHeight(),
                elementHeight;

            this.headerHeight = this.options.headerHeight.mobile;
            this.mobile       = true;

            if (this.$window.width() > this.options.desktopBreakpoint) {
                this.mobile       = false;
                this.headerHeight = this.options.headerHeight.desktop;
            }

            elementHeight = viewportHeight - this.headerHeight;

            this.$cover.css({'height': elementHeight});
            this.$red.css({'height': elementHeight});

            if (this.mobile) {
                this.$categorySelector.css({'height': ''});
            } else {
                this.$categorySelector.css({'height': elementHeight});
            }

            return this;
        }
    });

    return $.synolia.discoverNails;
});