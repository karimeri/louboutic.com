define([
    'jquery',
    'matchMedia',
    'underscore',
    'jquery/ui'
], function ($, mediaCheck) {
    'use strict';

    $.widget('synolia.imageRotation', {

        options: {
            mqDesktop: '(min-width: 1025px)',
            section: '.shoe-text-block',
            image: '.image-wrapper .image',
            activeImage: '.shoe-text-block .image.active',
            nbImage: 82
        },

        $activeImage: undefined,
        $prevImage: undefined,
        $nextImage: undefined,
        currentIndex: 0,

        _init: function() {
            this._initVars();
            this._initMediaCheck();
        },

        _initVars: function() {
            this.$activeImage = this.element.find(this.options.activeImage);
        },

        _initMediaCheck: function() {
            mediaCheck({
                media: this.options.mqDesktop,
                entry: function () {
                    // no throttle or debounced method because the event must be realized immediately after the scroll
                    $(window).on('scroll wheel', this.eventScroll.bind(this));
                }.bind(this),
                exit: function () {
                    $(window).off('scroll wheel');
                }
            });
        },

        eventScroll: function(e) {
            var shoeBlockHeight       = $(this.options.section).height(),
                windowHeight          = $(window).height(),
                maxScrollTop          = parseInt(shoeBlockHeight - windowHeight, 10),
                nbPixelsByImage       = maxScrollTop / this.options.nbImage,
                currentScrollPosition = $(e.currentTarget).scrollTop(),
                imageIndexToActivate  = Math.ceil(currentScrollPosition / nbPixelsByImage) - 1;

            if (imageIndexToActivate < 0) {
                imageIndexToActivate = 0;
            } else if (imageIndexToActivate > this.options.nbImage - 1) {
                imageIndexToActivate = this.options.nbImage - 1;
            }

            this.goToImage(imageIndexToActivate);
        },

        goToImage: function(imageIndex) {
            var activeImageIndex = this.$activeImage.data('index'),
                index,
                i = 0;

            if (imageIndex > activeImageIndex) {
                for (index = activeImageIndex; index < imageIndex + 1; index++) {
                    this.changeActiveImage(i, index, 'down');
                    i++;
                }
            } else {
                for (index = activeImageIndex; index > imageIndex - 1; index--) {
                    this.changeActiveImage(i, index, 'up');
                    i++;
                }
            }
        },

        changeActiveImage: function(i, index, direction) {
            var delay,
                animationTime = 50;

            delay = i === 0 ? 0 : animationTime * i;


            if (direction === 'down') {
                setTimeout(function () {
                    if(this.$activeImage.data('index') < index) {
                        $(this.options.image + '[data-index=' + index + ']').addClass('active');
                        this.$activeImage = $(this.options.image + '[data-index=' + index +']');
                    }
                }.bind(this), delay);
            }

            if(direction === 'up') {
                setTimeout(function () {
                    if(this.$activeImage.data('index') > index) {
                        this.$activeImage.removeClass('active');
                        this.$activeImage = $(this.options.image + '[data-index=' + index +']');
                    }
                }.bind(this), delay);
            }
        }
    });

    return $.synolia.imageRotation;
});