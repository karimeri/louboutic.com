define([
    'jquery',
    'jquery/ui'
], function ($) {
    'use strict';

    $.widget('synolia.wheelSlider', $.mage.collapsible, {

        options: {
            svg: {
                class: '.wheel-colors svg',
                colors: '.all-colors',
                path: '.wheel-color'
            },
            product: {
                class: '.product-selector .product',
                image: {
                    class: '.image .lipstick',
                    width: 80
                },
                color: '.color-name',
                category: '.category',
                arrow: {
                    class: '.arrow',
                    left: 'arrow-left',
                    right: 'arrow-right'
                }
            },
            animation: {
                easing: 'easeOutCubic',
                duration: 200
            },
            rotationByPath: 8.55,
            firstRotate: 5
        },

        $svg: {},
        $colors: {},
        $path: {},
        $product: {},
        colorsCount: undefined,
        currentIndex: undefined,
        currentRotation: undefined,

        _init: function() {
            this._initVars();
            this._initBindings();
            this._initProduct();
        },

        _initVars: function() {
            this.$svg        = this.element.find(this.options.svg.class);
            this.$colors     = this.element.find(this.options.svg.colors);
            this.$path       = this.$colors.find(this.options.svg.path);
            this.$product    = this.element.find(this.options.product.class);
            this.colorsCount = this.$colors.find('g path').length;
        },

        _initBindings: function() {
            this.$path.on('click', this.replaceProduct.bind(this));
            this.$product.find(this.options.product.arrow.class).on('click', this.clickArrow.bind(this));
        },

        /**
         * _initProduct : At the beginning we show product with index = 0
         * @private
         */
        _initProduct: function() {
            var $firstPath  = this.element.find(this.options.svg.path + '[data-index="0"]');

            this.replaceProduct($firstPath);
        },

        /**
         * replaceProduct : To replace product informations at circle center (color and catgory name)
         * @param path
         */
        replaceProduct: function(path) {
            var index, color, category, productUrl, categoryUrl, productLink, categoryLink, imageLink;

            if (path.target) { // if path is an event, path is replaced by current target object
                path = $(path.currentTarget);
            }

            index        = path.data('index'),
            color        = path.data('color-name'),
            category     = path.data('category-name'),
            productUrl   = path.data('product-url'),
            categoryUrl  = path.data('category-url'),
            productLink  = '<a href="' + productUrl +'" target="_blank">' + color + '</a>',
            categoryLink = '<a href="' + categoryUrl +'" target="_blank">' + category + '</a>',
            imageLink    = '<a href="' + productUrl +'" target="_blank"/>';

            this.rotateWheelTo(index);

            this.$product.find(this.options.product.color).html(productLink);
            this.$product.find(this.options.product.category).html(categoryLink);
            this.$product.find(this.options.product.image.class).html(imageLink);

            this._initBindingClick();

            this.currentIndex = index;
        },

        /**
         * _initBindingClick : Once links created we bind click event
         * @private
         */
        _initBindingClick: function() {
            this.$product.find('a').on('click', this.followLink.bind(this));
        },

        /**
         * followLink : Open target in new window
         * @param event
         */
        followLink: function(event) {
            window.open($(event.currentTarget).attr('href'), '_blank');
        },

        /**
         * clickArrow : Event handler for arrow click
         * @param event
         */
        clickArrow: function(event) {
            var newIndex, $newPath;

            if ($(event.currentTarget).hasClass(this.options.product.arrow.left)) {
                if (this.currentIndex > 0) {
                    newIndex = this.currentIndex - 1;
                } else {
                    newIndex = this.colorsCount - 1;
                }
            }

            if ($(event.currentTarget).hasClass(this.options.product.arrow.right)) {
                if (this.currentIndex < this.colorsCount - 1) {
                    newIndex = this.currentIndex + 1;
                } else {
                    newIndex = 0;
                }
            }

            $newPath = this.element.find(this.options.svg.path + '[data-index="'+ newIndex +'"]');

            this.replaceProduct($newPath);
        },

        /**
         * rotateWheelTo : Rotate the wheel to the given path index
         * @param newIndex
         */
        rotateWheelTo: function(newIndex) {
            var newRotate = -(newIndex * this.options.rotationByPath + this.options.firstRotate);

            this.$svg.animate({ textIndent: 0 }, {
                step: function() {
                    $(this).css({
                        'transform': 'translateX(-50%) rotate(' + newRotate + 'deg)'
                    });
                },
                duration: this.options.animation.duration,
                easing: this.options.animation.easing,
                complete: function() {
                    this.changeImage(newIndex);
                }.bind(this)
            });
        },

        /**
         * changeImage: Put product image corresponding to the given index at circle center
         * @param newIndex
         */
        changeImage: function(newIndex) {
            var backgroundX  = -(this.options.product.image.width * newIndex), // eslint-disable-line vars-on-top
                backgroundP  = '' + backgroundX + 'px 0';

            this.$product.find(this.options.product.image.class).animate({
                'opacity': 1
            }, {
                step: function() {
                    $(this).css({
                        'background-position': backgroundP
                    });
                },
                duration: this.options.animation.duration
            }, this.options.animation.easing);
        }
    });

    return $.synolia.wheelSlider;
});