define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'imageRotation'
], function ($) {
    'use strict';

    $.widget('synolia.discoverRed', {

        options: {
            discoverContent: '.discover-pages.discover-red'
        },

        _init: function() {
            this.element.find(this.options.discoverContent).imageRotation();
        }
    });

    return $.synolia.discoverRed;
});