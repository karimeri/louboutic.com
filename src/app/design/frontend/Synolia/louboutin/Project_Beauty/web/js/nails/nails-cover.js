define([
    'jquery',
    'mage/url',
    'mage/cookies'
], function($, url) {
    'use strict';

    /**
     * Parallax effect
     */
    $.widget('synolia.nailsCover', {

        options: {
            abstract: null,
            srcImages: 'cms/page/discover-nails/',
            selectors: {
                timer: '.time',
                country: '.country',
                top: '.page-wrapper > .top-container'
            },
            cookieName: 'country'
        },

        /**
         * @private
         */
        _create: function () {
            this._initElements()
                ._initTimer()
                ._initCover()
                ._initCountry();
        },

        /**
         * @returns {synolia.nailsCover}
         * @private
         */
        _initElements: function () {
            this.abstract = this.options.abstract;
            this.$cover   = this.abstract.$cover;
            this.$cover1  = this.$cover.children('.layer-1');
            this.$cover2  = this.$cover.children('.layer-2');
            this.$cover3  = this.$cover.children('.layer-3');
            this.$cover4  = this.$cover.children('.layer-4');
            this.$time    = this.element.find(this.options.selectors.timer);
            this.$country = this.element.find(this.options.selectors.country);
            this.$top     = $(this.options.selectors.top);

            return this;
        },

        /**
         * @returns {synolia.nailsCover}
         * @private
         */
        _initCover: function () {
            this._setImages();

            _.bindAll(this, '_setCoverParallax');
            this.abstract.$window.on('scroll', _.throttle(this._setCoverParallax, 12));

            return this;
        },

        /**
         * @private
         */
        _setCoverParallax: function () {
            var scrollTop      = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
                position       = scrollTop + this.abstract.headerHeight + this.$top.height(),
                coverOffsetTop = this.$cover.offset().top;

            if (position >= coverOffsetTop && !this.abstract.mobile) {
                this.$cover1.css({'transform': 'translate3d(0,' + scrollTop / 1.5 + 'px, 0)'});
                this.$cover2.css({'transform': 'translate3d(0,' + scrollTop / 2 + 'px, 0)'});
                this.$cover3.css({'transform': 'translate3d(0,' + scrollTop / 3 + 'px, 0)'});
                this.$cover4.css({'transform': 'translate3d(0,' + scrollTop / 4 + 'px, 0)'});
            }
        },

        /**
         * @returns {synolia.nailsCover}
         * @private
         */
        _initTimer: function () {
            setInterval(function(){
                var d = new Date();

                this.$time.html(d.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'}));
            }.bind(this),300);

            return this;
        },

        /**
         * @returns {synolia.nailsCover}
         * @private
         */
        _setImages: function () {
            var date         = new Date(),
                hours        = parseInt(date.getHours(), 10),
                timeOfTheDay = '',
                srcImages    = this.options.srcImages;

            if (hours >= 4 && hours < 8) {
                timeOfTheDay = 'morning';
            } else if (hours >= 8 && hours < 15) {
                timeOfTheDay = 'day';
            } else if (hours >= 15 && hours < 19) {
                timeOfTheDay = 'evening';
            } else {
                timeOfTheDay = 'night';
            }

            this.$cover.removeClass('morning day evening night').addClass(timeOfTheDay);
            this.$cover1.css({'background-image': 'url(' + url.buildMedia(srcImages + timeOfTheDay) + '_1.jpg)'});
            this.$cover2.css({'background-image': 'url(' + url.buildMedia(srcImages + timeOfTheDay) + '_2.png)'});
            this.$cover3.css({'background-image': 'url(' + url.buildMedia(srcImages + timeOfTheDay) + '_3.png)'});
            this.$cover4.css({'background-image': 'url(' + url.buildMedia(srcImages + timeOfTheDay) + '_4.png)'});

            return this;
        },

        /**
         * @returns {synolia.nailsCover}
         * @private
         */
        _initCountry: function () {
            _.bindAll(this, '_setCountry');
            $(document).on('store-switcher-ajax-after', this._setCountry);

            return this;
        },

        /**
         * @private
         */
        _setCountry: function () {
            var country = $.mage.cookies.get(this.options.cookieName);

            if (country) {
                this.$country.text(country.replace('+', ' '));
            }
        }
    });

    return $.synolia.nailsCover;
});