define([
    'jquery',
    'underscore',
    'synolia_slider_init'
], function($, _) {
    'use strict';

    $.widget('synolia.discoverShoes', {

        options: {
            currentShoeType: '',
            currentShoeTitle: '',
            currentShoeLink: '',
            slidingClass: 'sliding',
            siblings: '.info, h3',
            selectors: {
                currentSlide: '.slider-animated-shoe .slide.slick-active',
                html: 'html',
                allShoes: '.animated-shoe',
                slide: '.animated-shoe .slide',
                slider: '.slider-animated-shoe',
                animatedShoe: '.animated-shoe',
                shoeTitle: '.shoe-type > h3',
                shoeLink: '.shoe-type .info a',
                shoeDesc: '.shoe-type .info p',
                shoeThumbs: '.shoe-types-thumb',
                container: '.discover-shoes',
                pictosSlider: '.pictos-slider',
                list: '.pictos-slider > li'
            }
        },
        $currentShoe: '',
        $currentLi: '',
        $nextShoe: '',
        $sliding: '',
        $els: {},

        _initElements: function (selectors, target) {
            $.each(selectors, function (index, selector) {
                target[index] = $(selector);
            });

            return this;
        },

        _create: function() {
            this._initElements(this.options.selectors, this.$els);
            this._initBindings();
            this.$currentShoe = this.$els.currentSlide.find(this.$els.animatedShoe);
            this.$currentLi = this.$els.pictosSlider.find('> li:first-child').addClass('active');
            this.$els.html.css({
                'scroll-behavior' : 'smooth'
            });
        },

        _initBindings: function () {
            _.bindAll(this, '_addSliding', '_removeSliding', '_slideShoe', '_initElements');
            this.element.on('swipe', this._addSliding);
            this.element.on('afterChange', this._removeSliding);
            this.$els.list.on('click', this._slideShoe);
        },

        // Prepare the animation between two slides of shoes
        _slideShoe: function (event) {
            var $nextLi = $(event.currentTarget),
                $siblings = '';
            if(!$($nextLi).hasClass('active')){
                this.$nextShoe = this.$els.slider.find($nextLi.data('slide'));
                $siblings = this.$nextShoe.siblings(this.options.siblings);
                $siblings.addClass('fadein');
                this._shoeOut(this.$currentShoe, $nextLi);
            }
        },

        // Animate the first shoe to step out
        _shoeOut: function (elem, nextLi) {
            var $nextSlide = this.$els.slider.find(nextLi.data('slide')).parent('.slick-slide'),
                $siblings = elem.siblings(this.options.siblings);

            this.$els.allShoes.each( function(){
                $(this).css({
                    'background-image': 'url("' + $(this).data('in') + '")'
                });
            });

            elem.css({
                'background-image': 'url("'+elem.data('out')+'")',
                'visibility': 'visible'
            });

            $siblings.addClass('fadeout');

            for(var i = 100; i >= 0; i = i-20) {
                if (i === 20) {
                    elem.animate({
                        'background-position-x': i + '%',
                        'opacity': 0
                    }, 0, 'linear')
                    .animate({
                        'visibility': 'visible'
                    },
                    {
                        complete: function () {
                            this.$els.slider.slick('slickGoTo', nextLi.index(this.$els.slider.slick.$slides), true);
                            $siblings.removeClass('fadeout');
                            this._shoeIn(this.$nextShoe);
                        }.bind(this)
                    }, 80, 'ease');
                } else {
                    elem.animate({
                        'background-position-x': i + '%',
                        'opacity': i/100
                    }, 0, 'linear')
                        .animate({
                            'visibility': 'visible'
                    }, 60, 'linear');
                }
            }

            this.$els.currentSlide.removeClass('slick-current slick-active');
            this.$els.list.removeClass('active');
            nextLi.addClass('active');
            $nextSlide.addClass('slick-current slick-active');
        },


        //Animate the next shoe to step in
        _shoeIn: function (elem) {
            elem.siblings('.info, h3').removeClass('fadein');

            for(var i = 20; i <= 100; i = i+20) {
                elem.animate({
                    'background-position-x': i+'%',
                    'opacity': i/100,
                    'visibility': 'visible'
                }, 0).animate({
                    'background-position-x': i+'%'
                }, 80);
            }

            this.$currentShoe = elem;
        },

        _addSliding: function () {
            this.element.addClass(this.options.slidingClass);
        },

        _removeSliding: function () {
            this.element.removeClass(this.options.slidingClass);
        }

    });

    return $.synolia.discoverShoes;

});