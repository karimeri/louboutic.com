define([
    'jquery',
    'underscore',
    './select'
], function (
    $,
    _,
    Select
) {
    return Select.extend({
        radioReady: function () {
            $('body').trigger('radio-ready');
        },
        _onChange: function (event) {
            if (event.value) {
                this.value(event.value);
            }
        }
    });
});