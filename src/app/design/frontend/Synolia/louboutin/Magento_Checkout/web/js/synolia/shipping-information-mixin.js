define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote'
], function ($, Component, quote) {
    'use strict';

    var mixin = {
        getShippingMethodTitle: function () {
            var shippingMethod = quote.shippingMethod();

            return shippingMethod ? shippingMethod['carrier_title'] : '';
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});