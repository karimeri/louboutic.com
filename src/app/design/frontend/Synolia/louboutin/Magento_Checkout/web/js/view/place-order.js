define([
    'jquery',
    'uiComponent',
    'Magento_Ui/js/modal/alert',
    'mage/translate'
], function(
    $,
    Component,
    alert,
    $t
) {
    'use strict';

    return Component.extend({
        initialize: function () {
            $(function() {
                $('body').on('click', '#place-order-trigger', this.onClick);
            }.bind(this));

            this._super();
        },

        onClick: function() {
            var activePaymentDiv = $('.payment-method._active'),
                activePaymentMethod = activePaymentDiv.find('input.radio').is(':checked');

            if (activePaymentMethod === false) {
                alert({
                    title: $t('Payment method'),
                    content: $t('Please check one payment method before continue')
                });
            }

            activePaymentDiv.find('.action.primary.checkout').trigger('click');
        }

    });
}
);