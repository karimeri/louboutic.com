define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/modal/confirm',
    'jquery/ui',
    'mage/decorate',
    'mage/collapsible',
    'mage/cookies',
    'Magento_Checkout/js/sidebar'
], function ($, authenticationPopup, customerData, alert, confirm) {
    'use strict';

    $.widget('synolia.sidebar', $.mage.sidebar, {
        /**
         * disable calcHeight action
         */
        _calcHeight: function () {}
    });

    return $.synolia.sidebar;
});
