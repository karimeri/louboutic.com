define([
    'jquery',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'mage/translate'
], function ($, Component, quote, $t) {
    'use strict';

    var mixin = {
        getShippingMethodTitle: function () {
            var shippingMethod;

            if (!this.isCalculated()) {
                return '';
            }
            shippingMethod = quote.shippingMethod();

            return shippingMethod ? shippingMethod['method_title'] : '';
        },

        /**
         * @return {*}
         */
        getValue: function () {
            var price;

            if (!this.isCalculated()) {
                return this.notCalculatedMessage;
            }
            price = this.totals()['shipping_amount'];

            if (parseInt(price, 10) === 0) {
                return $t('Free shipping');
            }

            return this.getFormattedPrice(price);
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});