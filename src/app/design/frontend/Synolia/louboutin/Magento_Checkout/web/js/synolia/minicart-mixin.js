define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'jquery',
    'ko',
    'underscore',
    'sidebar',
    'mage/translate'
], function (Component, customerData, $, ko, _) {

   var miniCart = $('[data-block=\'minicart\']');
   var mixin = {
       minicartIconSelector: '.action.showcart',
       minicartMobileSelector: '[data-block="minicart-mobile"]',
       minicartStateElementSelector: 'body',



       initSideBar : function () {
           this._super();
           miniCart.sidebar({
               'targetElement': 'div.block.block-minicart',
               'url': {
                   'checkout': window.checkout.checkoutUrl,
                   'update': window.checkout.updateItemQtyUrl,
                   'remove': window.checkout.removeItemUrl,
                   'loginUrl': window.checkout.customerLoginUrl,
                   'isRedirectRequired': window.checkout.isRedirectRequired
               },
               'button': {
                   'checkout': '#top-cart-btn-checkout',
                   'remove': '.minicart-items a.action.delete',
                   'close': '#btn-minicart-close'
               },
               'showcart': {
                   'parent': 'span.counter',
                   'qty': 'span.counter-number',
                   'label': 'span.counter-label'
               },
               'minicart': {
                   'list': '#mini-cart',
                   'content': '#minicart-content-wrapper',
                   'qty': 'div.items-total',
                   'subtotal': 'div.subtotal span.price',
                   'maxItemsVisible': window.checkout.minicartMaxItemsVisible
               },
               'item': {
                   'qty': ':input.cart-item-qty',
                   'button': ':button.update-cart-item'
               },
               'confirmMessage': $.mage.__('Are you sure you would like to remove this item from the shopping cart?')
           });
       },



       initialize: function () {
           this._super();
           this.initSideBar();
           this._bindDropdownEvents();
       },

       _bindDropdownEvents: function() {
           $(this.minicartMobileSelector).on('dropdowndialogopen', function () {
               $(this.minicartStateElementSelector).addClass('minicart-open');
               $('html, body').scrollTop(0);
           }.bind(this));
           $(this.minicartMobileSelector).on('dropdowndialogclose', function () {
               $(this.minicartStateElementSelector).removeClass('minicart-open');
           }.bind(this));
       },

       update: function () {
           this._super();

           var nbItemsInCart = this.getCartLineItemsCount();

           if (nbItemsInCart>0) {
               $(this.minicartIconSelector).removeClass('empty');
           } else {
               $(this.minicartIconSelector).addClass('empty');
           }
       },

       redirectToCartAction: function() {
           window.location.href = window.checkout.shoppingCartUrl;
       }

   };

    return function (target) {
        return target.extend(mixin);
    };
});