var config = {
    map: {
        '*': {
            sidebar: 'Magento_Checkout/js/extend/magento/sidebar'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/minicart': {
                'Magento_Checkout/js/synolia/minicart-mixin': true
            },
            'Magento_Checkout/js/view/summary/shipping': {
                'Magento_Checkout/js/synolia/summary/shipping-mixin': true
            },
            'Magento_Checkout/js/view/shipping-information': {
                'Magento_Checkout/js/synolia/shipping-information-mixin': true
            },
            'Magento_Checkout/js/sidebar': {
                'Magento_Checkout/js/synolia/sidebar-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Magento_Checkout/js/synolia/shipping-mixin': true
            },
            'Magento_Checkout/js/view/billing-address': {
                'Magento_Checkout/js/synolia/billing-address-mixin': true
            }
        }
    }
};
