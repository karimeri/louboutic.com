var config = {
    map: {
        '*': {
            video:      'js/synolia/video',
            animations: 'js/synolia/animations',
            menu:       'js/extend/magento/menu',
            infoBox:    'js/synolia/info-box',
            parallax:   'js/synolia/parallax',
            pageLoader: 'js/synolia/page-loader'
        }
    },

    paths: {
        'validation-rules': 'js/synolia/validation-rules',
        'skrollr': 'js/lib/skrollr.min',
        'TweenLite': 'js/lib/GSAP/TweenLite.min',
        'TimelineLite': 'js/lib/GSAP/TimelineLite.min',
        'TweenLite-css': 'js/lib/GSAP/plugins/CSSPlugin.min',
        'jquery/jquery-migrate': 'js/lib/web/jquery-migrate'
    },

    deps: [
        'js/synolia/responsive',
        'js/synolia/theme'
    ],

    shim: {
        'mage/validation': {
            'deps': [
                'validation-rules'
            ]
        },
        'TweenLite-css': {
            'deps': [
                'TweenLite'
            ]
        }
    },

    config: {
        mixins: {
            'mage/validation': {
                'js/extend/magento/validation': true
            },
            'mageplaza/core/bootstrap': {
                'js/extend/bootstrap.min': true
            }
        }
    }
};
