/*jshint browser:true jquery:true*/
define([
    'jquery',
    'jquery/ui',
    'mage/template',
    'mage/translate',
    'priceBox',
    'Magento_ConfigurableProduct/js/configurable'
], function ($, ui, mageTemplate, $t) {
    'use strict';

    $.widget('synolia.configurable', $.mage.configurable, {

        options: {
            inputHiddenSelector: '.super-attribute-hidden',
            formSelector: '.product-add-form',
            addToCartBoxSelector: '.box-tocart',
            addToCartButtonSelector: '.action.tocart',
            abstractAlertStockSelector: '.alert.stock .action.abstract',
            clonedAlertStockSelector: '.product-info-main .alert.stock .action.cloned'
        },

        optionSelected: undefined,

        _create: function () {
            // Initial setting of various option values
            this._initializeOptions();

            // Override defaults with URL query parameters and/or inputs values
            this._overrideDefaults();

            // Fill state
            this._fillState();

            // Setup child and prev/next settings
            this._setChildSettings();

            // Setup/configure values to inputs
            this._configureForValues();

            // Change events to check select reloads
            this._setupChangeEvents();

            // Not display bracelets size if TU
            this._sizeDisplayNone(this.element);

            $(this.element).trigger('configurable.initialized');
        },

        _initializeOptions: function () {
            var options = this.options,
                gallery = $(options.mediaGallerySelector),
                priceBoxOptions = $(this.options.priceHolderSelector).priceBox().priceBox('option').priceConfig || null;

            if (priceBoxOptions && priceBoxOptions.optionTemplate) {
                options.optionTemplate = priceBoxOptions.optionTemplate;
            }

            if (priceBoxOptions && priceBoxOptions.priceFormat) {
                options.priceFormat = priceBoxOptions.priceFormat;
            }
            options.optionTemplate = mageTemplate(options.optionTemplate);
            options.tierPriceTemplate = $(this.options.tierPriceTemplateSelector).html();

            options.settings = options.spConfig.containerId ?
                $(options.spConfig.containerId).find(options.superSelector) :
                $(options.superSelector);

            options.values = options.spConfig.defaultValues || {};
            options.parentImage = $('[data-role=base-image-container] img').attr('src');

            this.inputSimpleProduct = this.element.find(options.selectSimpleProduct);

            gallery.data('gallery') ?
                this._onGalleryLoaded(gallery) :
                gallery.on('gallery:loaded', this._onGalleryLoaded.bind(this, gallery));

        },

        /**
         * Set up .on('change') events for each option element to configure the option.
         * @private
         */
        _setupChangeEvents: function () {
            $.each(this.options.settings, $.proxy(function (index, element) {
                $(element).find('li').on('click', this, this._configure.bind(element));
            }, this));
        },

        _fillSelect: function (element) {
            var attributeId = element.id.replace(/[a-z]*/, ''),
                options = this._getAttributeOptions(attributeId),
                prevConfig,
                allowedProducts,
                allowedProductsByOption,
                allowedProductsAll,
                i,
                j,
                allowedOptions = [],
                indexKey;

            prevConfig = false;

            if (element.prevSetting) {
                prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
            }

            if (options) {
                for (indexKey in this.options.spConfig.index) {
                    /* eslint-disable max-depth */
                    if (this.options.spConfig.index.hasOwnProperty(indexKey)) {
                        allowedOptions = allowedOptions.concat(_.values(this.options.spConfig.index[indexKey]));
                    }
                }

                if (prevConfig) {
                    allowedProductsByOption = {};
                    allowedProductsAll = [];

                    for (i = 0; i < options.length; i++) {
                        /* eslint-disable max-depth */
                        for (j = 0; j < options[i].products.length; j++) {
                            // prevConfig.config can be undefined
                            if (prevConfig.config &&
                                prevConfig.config.allowedProducts &&
                                prevConfig.config.allowedProducts.indexOf(options[i].products[j]) > -1) {
                                if (!allowedProductsByOption[i]) {
                                    allowedProductsByOption[i] = [];
                                }
                                allowedProductsByOption[i].push(options[i].products[j]);
                                allowedProductsAll.push(options[i].products[j]);
                            }
                        }
                    }
                }

                for (i = 0; i < options.length; i++) {
                    if (prevConfig && typeof allowedProductsByOption[i] === 'undefined') {
                        continue; //jscs:ignore disallowKeywords
                    }

                    allowedProducts = prevConfig ? allowedProductsByOption[i] : options[i].products.slice(0);

                    if (allowedProducts.length > 0 || _.include(allowedOptions, options[i].id)) {
                        options[i].allowedProducts = allowedProducts;

                        var $optionElement = $('<li>', {
                            text: this._getOptionLabel(options[i])
                        }).data('value', options[i].id);

                        $(element).append($optionElement);

                        this._setStockStatus(element, $optionElement, options[i]);

                        if (typeof options[i].price !== 'undefined') {
                            $optionElement.get(0).setAttribute('price', options[i].price);
                        }

                        if (allowedProducts.length === 0) {
                            $optionElement.get(0).disabled = true;
                        }

                        $optionElement.get(0).config = options[i];
                    }

                    /* eslint-enable max-depth */
                }
            }
        },

        _clearSelect: function (element) {
            element.html('');
        },


        /**
         * Get product various prices
         * @returns {{}}
         * @private
         */
        _getPrices: function () {
            var prices = {},
                elements = _.toArray(this.options.settings),
                hasProductPrice = false;

            _.each(elements, function (element) {
                var selected = $(element).find('li').eq(element.selectedIndex),
                    config = selected && selected.config,
                    priceValue = {};

                if (config && config.allowedProducts.length === 1 && !hasProductPrice) {
                    priceValue = this._calculatePrice(config);
                    hasProductPrice = true;
                }

                prices[element.attributeId] = priceValue;
            }, this);

            return prices;
        },

        _configure: function (event) {
            event.data._setValueOnSelect(this, event.currentTarget);
            event.data._configureElement(this);
            event.data._changeOptionState();
        },

        _setValueOnSelect: function(select, optionElement) {
            this.optionSelected = optionElement;
            $(select).data('value', $(optionElement).data('value'));
        },

        _configureElement: function (element) {
            this.simpleProduct = this._getSimpleProductId(element);

            if ($(element).data('value')) {
                $(this.options.inputHiddenSelector).val($(element).data('value'));

                this.options.state[element.config.id] = $(element).data('value');

                if (element.nextSetting) {
                    element.nextSetting.disabled = false;
                    this._fillSelect(element.nextSetting);
                    this._resetChildren(element.nextSetting);
                } else {
                    this.inputSimpleProduct.val(this.optionSelected.config.allowedProducts[0]);
                }
            } else {
                this._resetChildren(element);
            }

            this._reloadPrice();
            this._displayRegularPriceBlock(this.simpleProduct);
            this._displayTierPriceBlock(this.simpleProduct);
            this._changeProductImage();


            this._changeAvailability(element); // change availability status & add to cart visibility
        },

        _getSimpleProductId: function (element) {
            // TODO: Rewrite algorithm. It should return ID of
            //        simple product based on selected options.
            var allOptions = element.config.options,
                value = $(element).data('value'),
                config;

            config = _.filter(allOptions, function (option) {
                return option.id === value;
            });
            config = _.first(config);

            return _.isEmpty(config) ?
                undefined :
                _.first(config.allowedProducts);

        },

        _setStockStatus: function($element, $optionElement, elementData) {
            var productId = elementData.products[0];

            var stockInfo = this.options.spConfig.stock[productId];

            if (stockInfo.saleable) {
                $optionElement.addClass('in-stock');
            } else if (stockInfo.is_preorder){
                $optionElement.addClass('in-preorder');
            } else {
                $optionElement.addClass('out-of-stock');
            }
        },

        _changeOptionState: function() {
            $(this.optionSelected).siblings().removeClass('selected');
            $(this.optionSelected).addClass('selected');
        },

        _changeAvailability: function(element) {
            var spConfig  = this.options.spConfig;
            var productId = this.simpleProduct;
            var inputPreorder = $(this.options.settings).parents(this.options.formSelector).first()
                                                        .find('input#is_preorder');

            var form = $(this.options.settings).parents(this.options.formSelector).first(),
                $addToCartBoxSelector = form.find(this.options.addToCartBoxSelector),
                $preorderMessage = $addToCartBoxSelector.find('.preorder-message');

            if ($preorderMessage) {
                $preorderMessage.remove();
            }

            if (spConfig.stock && spConfig.stock[productId] !== undefined) {
                // we can access stock data for this product
                var stockInfo = spConfig.stock[productId],
                    text = $t(stockInfo.label);

                if(stockInfo.saleable) {
                    inputPreorder.val(0);
                    this._showAddToCart(element, productId, stockInfo);
                } else if (stockInfo.is_preorder) {
                    inputPreorder.val(1);
                    this._addPreOrderMessage(stockInfo.preorder_txt);
                    this._showAddToCart(element, productId, stockInfo, text, stockInfo.is_preorder);
                } else {
                    inputPreorder.val(0);
                    this._hideAddToCart(element, productId, stockInfo);
                }
            }
        },

        _showAddToCart: function(element, productId, stockInfo, text, isPreorder) {
            var form = $(this.options.settings).parents(this.options.formSelector).first(),
                $addToCartButton = form.find(this.options.addToCartButtonSelector);

            if(text === undefined) {
                text = $t('Add to Cart');
            }

            if(isPreorder !== undefined) {
                $addToCartButton.addClass('preorder');
            } else {
                $addToCartButton.removeClass('preorder');
            }

            $addToCartButton.removeClass('no-display')
                .removeClass('disabled')
                .find('span')
                .text(text);

            // Stock alert link
            $(this.options.clonedAlertStockSelector).remove();
        },

        _hideAddToCart: function(element, productId, stockInfo) {
            var form = $(this.options.settings).parents(this.options.formSelector).first(),
                $addToCartButton = form.find(this.options.addToCartButtonSelector);

            if (stockInfo.is_back_in_stock) {
               // disable add to cart
                $addToCartButton.addClass('no-display');

                // Show stock alert link
                $(this.options.clonedAlertStockSelector).remove();
                var alertStockAbstract = $(this.options.abstractAlertStockSelector);

                var alertLink = alertStockAbstract.clone();
                alertLink.attr('href', stockInfo.alert_stock)
                    .removeClass('abstract no-display')
                    .addClass('cloned product-'+productId);

                alertStockAbstract.after(alertLink);
            } else if (!stockInfo.is_back_in_stock && stockInfo.label) {
                // Remove stock alert link
                $(this.options.clonedAlertStockSelector).remove();

                // Disable add to cart and change label
                $addToCartButton.removeClass('no-display')
                    .addClass('disabled')
                    .find('span')
                    .text($t('Sold Out'));
            }
        },

        _addPreOrderMessage: function(message) {
            var form = $(this.options.settings).parents(this.options.formSelector).first(),
                $addToCartBoxSelector = form.find(this.options.addToCartBoxSelector),
                $preOrderMessageBox = $('<div>', {'class': 'preorder-message'});

            if(message) {
                $preOrderMessageBox.append($t(message));
                $addToCartBoxSelector.prepend($preOrderMessageBox);
            }
        },

        _sizeDisplayNone: function(element) {
            if ($('ul.super-attribute-select li').length == 1 && $('ul.super-attribute-select li').text() == 'TU') {
                $('.product-options-wrapper').hide();
                $('ul.super-attribute-select li').first().addClass("selected").click()
            }
        }
    });

    return $.synolia.configurable;
});
