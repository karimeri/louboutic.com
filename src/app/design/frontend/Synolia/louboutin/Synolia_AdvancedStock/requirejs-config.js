var config = {
    map: {
        '*': {
            configurable: 'Synolia_AdvancedStock/js/configurable'
        }
    },

    config: {
        mixins: {
            'Magento_ConfigurableProduct/js/configurable': {
                'Synolia_AdvancedStock/js/configurable-mixin': false
            }
        }
    }
};