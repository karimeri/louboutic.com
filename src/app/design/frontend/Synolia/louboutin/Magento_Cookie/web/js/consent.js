require([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';

    $('#cookie_consent').click(function() {
        cookieLabo.init('cookieChoose');
    });
});
