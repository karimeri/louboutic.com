define([
    'jquery',
    'jquery/ui'
], function($) {
    $.widget('synolia.clearInput', {
        options: {
            resetElement: '#search_mobile'
        },

        _create: function() {
            this._bind();
        },

        _bind: function() {
            $(this.element).on('click', function() {
                var $resetElement = $(this.options.resetElement);

                if ($resetElement.length) {
                    $resetElement.val('');
                    $resetElement.focus();
                }
            }.bind(this));
        }

    });

    return $.synolia.clearInput;
});