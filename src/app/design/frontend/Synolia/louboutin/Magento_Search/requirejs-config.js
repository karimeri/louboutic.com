var config = {
    map: {
        '*': {
            clearInput: 'Magento_Search/js/clear-input'
        }
    },

    deps: [
        'Magento_Search/js/clear-input'
    ]
};