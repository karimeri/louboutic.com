# Theme d'initialisation front projet "customtheme"

## But

Le but de ce theme est d'initialiser rapidement une base de travail pour les développeurs frontend.

Ce thème intègre un certain nombre d'éléments front sur lesquels Synolia a régulièrement à travailler.
Il hérite du thème standard Magento Luma (Magento/luma) qui lui-même hérite du thème standard Magento Blank (Magento/blank). Tous les éléments de ce thème sont personnalisables / supprimables selon les besoins du projet.


## Utilisation du theme

1) Déposer le theme dans app/design/frontend/customtheme

2) Renommer le theme selon le projet (au quel cas, penser à modifier le registration.php et le theme.xml)

3) Creer 2 blocs CMS email-header et email-footer pour gérer le header et footer des mails

4) Comparer le fichier du theme mage/__validation.js et celui du standard lib/web/mage/validation.js et renommer __validation.js en validation.js si les patchs n'ont pas été apportés par Magento

5) Si besoin, inclure fontAwesome en décommentant la ligne dans _lib.less 

6) Inclure les icones dans la font ProjectIcons. Il faut importer le fichier ProjectIcons.json sur https://icomoon.io/app. 
Pour voir les icones présentes dans la font : /pub/static/frontend/Synolia/louboutin/en_US/fonts/Loubicons/icons-reference.html


7) Utiliser le module synolia_standard_design

````
"require": {
    "synolia/mto2-synolia_standard_design": "1.0.1"
}
````

8) Utiliser le module synolia_admin

````
"require": {
    "synolia/mto2-synolia_admin": "1.*"
}
````

9) Adapter le theme selon les besoins projet


## Règles à suivre pour toute évolution

- Tous les éléments chartés doivent être présent sur la page de test située dans web/css/docs/index.html
URL de test : [/static/frontend/Synolia/customtheme/en_US/css/docs/synolia.html](Link URL) OU /pub/static/frontend/Synolia/customtheme/en_US/css/docs/synolia.html (suivant le baseDir)

- Toutes les librairies ajoutées doivent être documentées dans le dossier docs (au minimum un fichier README.md avec un lien vers la doc officielle, au mieux une documentation complète)

- Tous les nouveaux éléments doivent être dans des dossiers synolia (pour éviter toute réécriture involontaire des fichiers du standard)

- Ne pas créer de fichier web/css/source/_variables.less car ce fichier viendrait réécrite celui de Luma. Respecher la règle suivante :

    1) Override de variable existant deja dans Magento/blank ou Magento/luma : dans _theme.less

    2) Nouvelle variable du thème : dans un des fichiers du dossier web/css/source/lib/variables/synolia/

- Respecter, le plus possible, la catégorisation des éléments chartés (ex: les variables des différents types de boutons doivent se trouver dans web/css/source/lib/variables/synolia/_buttons.less)

- Réécrire le moins possible les éléments du standard et en cas de réécriture :

    - Préfixer le fichier par un double inderscore (__) pour qu'il soit vérifié a chaque installation du thème
    
    - Indiquer la réécriture dans ce fichier (section "Qu'y-a-t-il dans ce thème")



## Qu'y a-t-il dans ce thème (TODO)

- Logo du thème **(100%)**

- Ajout de 2 google fonts custom (Roboto, Lobster). **(100%)**

La font Roboto est déclinée selon 3 types de rendu (regular, bold, thin), la Lobster n'existe qu'en un type de rendu : Regular.

La font Roboto Regular est utilisée par défaut, la lobster est une police liée qui n'est jamais utiliée.


- Ajout d'une font custom pour la gestion des icones (générer via icon-moon) **(100%)**

- Ajout d'une mécanique de gestion des sprites **(10%)**

- Customisation des liens (avec effets de survols) **(100%)**

- Customisation de 3 types de boutons avec leurs effets de hover **(10%)**

- Customisation des inputs de type text et équivalents (ex: email, number, ...) **(10%)**

- Customisation des selects **(20%)**

- Customisation des boutons radios et checkbox **(20%)**

- Customisation des tables **(0%)**

- Customisation du breadcrumb **(0%)**

- Customisation des messages (success, error, notice) **(0%)**

- Customisation des onglets (tabs) **(0%)**

- Customisation des accordéons (accordion) **(0%)**

- Customisation des notes (ratings) **(50%)**

- Customisation des liste déroulantes (dropdowns) **(0%)**

- Customisation de la pagination **(0%)**

- **Surcharge** de la modal magento pour ajouter des événement **(100%)**



- **Surcharge** des templates standards Magento header / footer pret à gérer header et footer sous forme de blocs CMS **(100%)**

- **Surcharge** de la feuille de style _email-extend.less qui sera utilisé pour la customisation des styles des mails **(100%)**

- **Surcharge** de mage/url de Magento pour pouvoir fabriquer des URLS vers des controllers de module et également vers les dossiers media, pub, etc... **(100%)**

- **Surcharge** de mage/menu de Magento pour pouvoir utiliser le widget de navigation afin d'avoir un nombre limite de catégorie à afficher (overflowItems) **(0%)**







## Snippets

Pour changer rapidement de theme insérez ces lignes dans le di.xml de votre module projet

````
<type name="Magento\Theme\Model\View\Design">
    <arguments>
        <argument name="themes" xsi:type="array">
            <!--<item name="frontend" xsi:type="string">Magento/luma</item>-->
            <item name="frontend" xsi:type="string">Synolia/customtheme</item>
        </argument>
    </arguments>
</type>
````

La table core_config_data ne doit faire référence à aucun thème

````
DELETE FROM `core_config_data` WHERE `path` = 'design/theme/theme_id'
````

La table design_config_grid_flat ne doit faire référence à aucun thème

````
UPDATE `design_config_grid_flat` SET `theme_theme_id` = '';
````

## Problèmes connus

* Lors de l'installation du thème si le message "/styles-m.css' wasn't found (404)" (ou équivalent) apparait, merci d'exécuter les commandes suivante :

````
rm -rf var/view_preprocessed/*
rm -rf pub/static/frontend/Synolia/customtheme/*
php bin/magento dev:source-theme:deploy --theme Synolia/customtheme --locale fr_FR
````


* Problème de traductions JS en plusieurs parties (concaténation) dans le validation.js notamment que Magento ne trouve pas
lors de son parsing de fichiers et n'arrive donc pas a ajouter au js-translations.json.

Problème relevé ICI : \lib\web\mage\validation.js:549

Issue : https://github.com/magento/magento2/issues/5820

Seul correctif possible : réécriture  et suppression de la concaténation


* Problème de traduction des titres de pages quand déclarés dans des XML

Problème relevé dans les fichiers suivants notamment :

- Confirmation de commande : vendor/magento/module-checkout/view/frontend/layout/checkout_onepage_success.xml:10
- Page de contact : vendor/magento/module-contact/view/frontend/layout/contact_index_index.xml:10
- Recherche avancée : vendor/magento/module-catalog-search/view/frontend/layout/catalogsearch_advanced_index.xml:10
- Page mot de passe oublié : vendor/magento/module-customer/view/frontend/layout/customer_account_forgotpassword.xml:10

Thread principal : https://github.com/magento/magento2/issues/2951

**Ce problème est corrigé dans le module Synolia_Standard** (réécriture de la classe Magento\Framework\View\Page\Config\Renderer)


* Le bloc Magento\CatalogSearch\Block\SearchResult\ListProduct n'existe pas, c'est un virtualType


* A l'installation le logo ne change pas et toute autre modification des layouts du thème n'impactent pas le front.

Vérifier la table "theme" dans la BDD, si le type du thème est 1, Magento considère le thème comme "virtuel" et ignore les layouts
 
 Cf stack overflow : https://magento.stackexchange.com/questions/134523/magento-2-custom-theme-layout-xml-and-css-not-working
 
 
