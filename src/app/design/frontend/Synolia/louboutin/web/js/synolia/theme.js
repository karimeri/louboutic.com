require([
    'jquery',
    'jquery/ui',
    'video',
    'animations',
    'videoLips',
    'parallax',
    'pageLoader',
    'discoverRed'
], function($) {
    'use strict';

    var $body = $('body');

    $body.animations();

    $('.video-container').video();
    $('.video-container-lips').videoLips();

    $('[data-parallax="true"]').parallax();

    $('.discover-pages').pageLoader();

    $('.cms-discover-rouge-html').discoverRed();

    $(".top-links a").click(function(e) {
        e.preventDefault();
        var _hrefId = $(this).attr("href");
        $('html,body').animate({scrollTop: $(_hrefId).offset().top - 100},'slow');
    });
});
