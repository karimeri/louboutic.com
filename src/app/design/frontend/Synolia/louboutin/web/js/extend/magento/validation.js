define(['jquery'], function($) {
    return function (target) {
        $.widget("mage.validation", target, {
            options: {
                errorPlacement: function (error, element) {
                    var errorPlacement = element,
                        fieldWrapper;

                    // logic for date-picker error placement
                    if (element.hasClass('_has-datepicker')) {
                        errorPlacement = element.siblings('button');
                    }
                    //logic for checkboxes/radio
                    if (element.is(':checkbox') || element.is(':radio')) {
                        errorPlacement = element.parents('.control').children().last();

                        //fallback if group does not have .control parent
                        if (!errorPlacement.length) {
                            errorPlacement = element.siblings('label').last();
                        }
                    }
                    // logic for field wrapper
                    fieldWrapper = element.closest('.addon');

                    if (fieldWrapper.length) {
                        errorPlacement = fieldWrapper.after(error);
                    }
                    //logic for control with tooltip
                    if (element.siblings('.tooltip').length) {
                        errorPlacement = element.siblings('.tooltip');
                    }
                    errorPlacement.after(error);
                }
            }
        });

        return $.mage.validation;
    };
});