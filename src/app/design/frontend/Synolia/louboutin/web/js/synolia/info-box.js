define([
    'jquery',
    'jquery/ui',
    'mage/cookies'
], function($, ui) {
    'use strict';

    $.widget('synolia.infoBox', {

        options: {
            closeSelector: '.close',
            cookieName: 'info_box',
            cookieLifetime: 3600 * 24 * 365
        },

        _create: function() {
            var cookieValue = $.mage.cookies.get(this.options.cookieName);

            this.element.toggleClass('no-display', !!cookieValue);

            this._bindEvents();
        },

        _bindEvents: function() {
            this.element.find(this.options.closeSelector).on('click', this._close.bind(this));
        },

        _close: function() {
            $.mage.cookies.set(this.options.cookieName, '1', {
                lifetime: this.options.cookieLifetime
            });

            this.element.slideUp(400, function () {
                $(this).addClass('no-display');
            });
        }
    });

    return $.synolia.infoBox;
});
