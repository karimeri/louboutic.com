define([
    'jquery',
    'jquery/ui',
    'mage/menu',
    'underscore'
], function ($, ui, menu, _) {
    'use strict';

    $.widget('synolia.menu', $.mage.menu, {
        options: {
            navBarRight: '.navbar-right',
            navBarDesktopContainer: '.menu-area .navbar',
            navBarMobileContainer: '.menu-mobile-extra',
            mobileCatalogMenuContainer: '#store\\.menu',
            mobileBackLink: '.section-item-content .menu-back',
            mediaBreakPoint: 1024
        },
        autoComplete: {
            parentElement: '.menu-categories',
            algoliaSearchInput: '#algolia-searchbox input',
            autocompleteStateElement: 'body',
            transitionDuration: 400
        },

        searchTimeoutClose: [],
        searchTimeoutMenuEnter: [],

        toggle: function () {
            var html = $('html');

            if (window.menuMoving) {
                // lock : usefull for multiple menu widget instantiation
                return;
            }

            if (html.hasClass('nav-open')) {
                window.menuMoving = true;

                html.removeClass('nav-open');
                setTimeout(function () {
                    html.removeClass('nav-before-open');
                    window.menuMoving = false;
                }, this.options.hideDelay);
            } else {
                window.menuMoving = true;

                html.addClass('nav-before-open');
                setTimeout(function () {
                    html.addClass('nav-open');
                    window.menuMoving = false;
                }, this.options.showDelay);
            }
        },

        _toggleMobileMode: function() {
            $(this.options.navBarDesktopContainer)
                .find(this.options.navBarRight)
                .prependTo(this.options.navBarMobileContainer);

            $(this.element).off('mouseenter mouseleave');
            this._on({
                /**
                 * @param {jQuery.Event} event
                 */
                'click .ui-menu-item > a': function (event) {
                    var target, isOpen;
                    event.preventDefault();
                    target = $(event.target).closest('.ui-menu-item');
                    isOpen = target.find('> .ui-state-active').length;
                    if (!target.hasClass('parent') || isOpen || target.find('> a').attr('id') === 'cat-loub') { // has no child or is already opened
                        window.location.href = target.find('> a').attr('href');
                    }
                }
            });

            $(this.options.mobileBackLink).on('click', function(event) {
                event.stopPropagation(); // prevent menu interactions
                event.preventDefault();

                if (this.currentSubmenu && this.currentSubmenu.length) {
                    this.collapse();
                }
            }.bind(this));
        },

        _open: function(submenu) {

            if (window.innerWidth > this.options.mediaBreakPoint) {
                this._super(submenu);
                $(this.options.mobileCatalogMenuContainer).addClass('open');
            } else {
                if (!$(submenu).closest('.louboutin-world').length) {
                    this._super(submenu);
                    $(this.options.mobileCatalogMenuContainer).addClass('open');
                }
            }

            if ($(submenu).hasClass('level1')) {
                $(this.options.mobileCatalogMenuContainer).addClass('level1-open');
            } else {
                $(this.options.mobileCatalogMenuContainer).removeClass('level1-open');
            }

            this.currentSubmenu = submenu; // usefull for the back link and collapse css effect
        },

        _close: function(startMenu) {
            this._super(startMenu);

            if ($(this.active).hasClass('level0')) {
                $(this.options.mobileCatalogMenuContainer).removeClass('open level1-open');
            }
        },

        collapse: function(event) {
            this.currentSubmenu.css('left', '100%');
            if ($(this.active).hasClass('level2')) {
                $(this.options.mobileCatalogMenuContainer).removeClass('level1-open');
            }

            /* Making sure menu item selected is the li */
            var newItem = this.active &&
                this.active.parent().closest( "li.ui-menu-item", this.element );
            if ( newItem && newItem.length ) {
                this._close();
                this.focus( event, newItem );
            }

            this.currentSubmenu = newItem.closest(this.options.menus);
        },

        collapseAll: function(event, all) {
            this._super(event, all);
            $(this.options.mobileCatalogMenuContainer).removeClass('open level1-open');
        },

        select: function (event) {
            var ui;

            this.active = $(event.target).closest('.ui-menu-item');

            ui = {
                item: this.active
            };


            this._trigger('select', event, ui);
        },

        _toggleDesktopMode: function() {
            $(this.options.navBarMobileContainer)
                .find(this.options.navBarRight)
                .appendTo(this.options.navBarDesktopContainer);

            $('.ui-menu .level-top.ui-menu-item a').on('mouseover', function() {
                this.searchTimeoutClose.push(setTimeout(function() {
                    this._closeSearchAutocomplete();
                }.bind(this), 1000));
            }.bind(this));

            var categoryParent, html;

            $(document).on('mouseenter', this.autoComplete.parentElement, this._parentAutocompleteEnter.bind(this));

            this._on({
                /**
                 * Prevent focus from sticking to links inside menu after clicking
                 * them (focus should always stay on UL during navigation).
                 */
                'mousedown .ui-menu-item > a': function (event) {
                    event.preventDefault();
                },

                /**
                 * Prevent focus from sticking to links inside menu after clicking
                 * them (focus should always stay on UL during navigation).
                 */
                'click .ui-state-disabled > a': function (event) {
                    event.preventDefault();
                },

                /**
                 * @param {jQuer.Event} event
                 */
                'click .ui-menu-item:has(a)': function (event) {
                    var target = $(event.target).closest('.ui-menu-item');

                    if (!this.mouseHandled && target.not('.ui-state-disabled').length) {
                        this.select(event);

                        // Only set the mouseHandled flag if the event will bubble, see #9469.
                        if (!event.isPropagationStopped()) {
                            this.mouseHandled = true;
                        }

                        // Open submenu on click
                        if (target.has('.ui-menu').length) {
                            this.expand(event);
                        } else if (!this.element.is(':focus') &&
                            $(this.document[0].activeElement).closest('.ui-menu').length
                        ) {
                            // Redirect focus to the menu
                            this.element.trigger('focus', [true]);

                            // If the active item is on the top level, let it stay active.
                            // Otherwise, blur the active item since it is no longer visible.
                            if (this.active && this.active.parents('.ui-menu').length === 1) { //eslint-disable-line
                                clearTimeout(this.timer);
                            }
                        }
                    }
                },

                /**
                 * @param {jQuery.Event} event
                 */
                'mouseenter .ui-menu-item': function (event) {
                    if (this._isSearchDisplayed()) {
                        this.searchTimeoutMenuEnter.push(setTimeout(function() {
                            this.displayMenu(event);
                        }.bind(this), this.options.transitionDuration));
                    } else {
                        this.displayMenu(event);
                    }
                }.bind(this),

                /**
                 * @param {jQuery.Event} event
                 */
                'mouseleave': function (event) {
                    this.collapseAll(event, true);
                },

                /**
                 * Mouse leave.
                 */
                'mouseleave .ui-menu': 'collapseAll'
            });

            categoryParent = this.element.find('.all-category');
            html = $('html');

            categoryParent.remove();

            if (html.hasClass('nav-open')) {
                html.removeClass('nav-open');
                setTimeout(function () {
                    html.removeClass('nav-before-open');
                }, 300);
            }
        },

        _isSearchDisplayed: function() {
            return $(this.autoComplete.parentElement).find('[data-category="search"]').is(':visible');
        },

        _parentAutocompleteEnter: function () {
            if (this._isSearchDisplayed()) {
                $.each(this.searchTimeoutPanelWrapper, function(index, item) {
                    clearTimeout(item);
                    delete this.searchTimeoutPanelWrapper[index];
                }.bind(this));

                $.each(this.searchTimeoutClose, function(index, item) {
                    clearTimeout(item);
                    delete this.searchTimeoutMenuEnter[index];
                }.bind(this));
            }
        },

        displayMenu: function(event) {
            var target = $(event.currentTarget),
                submenu = this.options.menus,
                ulElement,
                ulElementWidth,
                width,
                targetPageX,
                rightBound;

            if (target.has(submenu)) {
                ulElement = target.find(submenu);
                ulElementWidth = ulElement.outerWidth(true);
                width = target.outerWidth() * 2;
                targetPageX = target.offset().left;
                rightBound = $(window).width();

                if (ulElementWidth + width + targetPageX > rightBound) {
                    ulElement.addClass('submenu-reverse');
                }

                if (targetPageX - ulElementWidth < 0) {
                    ulElement.removeClass('submenu-reverse');
                }
            }

            // Remove ui-state-active class from siblings of the newly focused menu item
            // to avoid a jump caused by adjacent elements both having a class with a border
            target.siblings().children('.ui-state-active').removeClass('ui-state-active');
            this.focus(event, target);
        },

        _closeSearchAutocomplete: function() {
            var $parentElement = $(this.autoComplete.parentElement),
                $subElements = $parentElement.find('[data-category]'),
                $autocompleteStateElement = $(this.autoComplete.autocompleteStateElement),
                $algoliaSearchInput = $(this.autoComplete.algoliaSearchInput);

            $subElements
                .addClass('hide')
                .css('opacity', 0);

            $parentElement.css({
                'height': 0
            });

            $parentElement.hide();

            // Empty results
            if ($algoliaSearchInput.length) {
                $algoliaSearchInput.val('');
                $algoliaSearchInput.get(0).dispatchEvent(new Event('input'));
            }

            $autocompleteStateElement.removeClass('autocomplete-update');
        }
    });

    return $.synolia.menu;
});
