require([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function($, modal) {

    $('<p/>', {
        'class': 'modal-content',
        'text': 'TEST'
    }).appendTo('body');

    $('.modal-content').modal().modal('openModal');
});