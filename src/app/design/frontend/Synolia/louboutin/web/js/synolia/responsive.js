define([
    'jquery',
    'matchMedia',
    'domReady!'
], function ($, mediaCheck) {
    'use strict';

    mediaCheck({
        media: '(min-width: 769px)',
        // Switch to Desktop Version
        entry: function () {
            //console.log('>=769px');
        },
        // Switch to Mobile Version
        exit: function () {
            //console.log('<769px');
        }
    });
});
