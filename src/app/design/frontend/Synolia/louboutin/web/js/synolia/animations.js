define([
    "jquery",
    "jquery/ui",
    "underscore"
], function($) {
    "use strict";

     $.widget('synolia.animations', {

        options: {
            scroll: {
                notVisibleClass: 'scroll-notvisible',
                visibleClass: 'scroll-visible'
            }
        },

        _init: function () {
            this._load();
            this._showBlock();
            this._initBindings();
        },

        _initBindings: function () {
            $(window).on('scroll', _.bind(_.throttle(this._showBlock, 100), this));
        },

         _load: function(event) {
             $('.left, .right').addClass('show');
         },

         _showBlock: function(event) {
             var windowHeight    = $(window).height(),
                 windowOffsetTop = $(window).scrollTop(),
                 notVisibleClass = this.options.scroll.notVisibleClass,
                 element         = '.' + notVisibleClass;

             $(element).each(function(index, item) {
                 var itemOffsetTop = $(item).offset().top,
                     resultTop     = itemOffsetTop - windowHeight;

                 if((Boolean(item.closest('.images-products-right'))
                     && item.classList.contains('image-first')
                     && window.matchMedia('(max-width: 767px)').matches)) {
                         $(item).removeClass(notVisibleClass).addClass(this.options.scroll.visibleClass);
                 }
                 if(windowOffsetTop >= resultTop) {
                         $(item).removeClass(notVisibleClass)
                         .addClass(this.options.scroll.visibleClass);
                 }
             }.bind(this));
         }
    });

    return $.synolia.animations;
});
