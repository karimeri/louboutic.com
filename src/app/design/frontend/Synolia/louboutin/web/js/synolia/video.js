define([
    'jquery',
    'matchMedia',
    'underscore',
    'jquery/ui'
], function($, mediaCheck, _) {
    'use strict';

    $.widget('synolia.video', {

        options: {
            mqDesktop: '(min-width: 1025px)',
            videoContainer: '.video-container',
            muteButton: '.mute-button',
            playButton: '.play-button',
            wait: 300,
            video: {
                attribute: {
                    mute: 'muted',
                    loop: 'loop'
                },
                class: {
                    playOnScroll: 'playOnScroll'
                }
            }
        },

        $muteButton: {},
        $playButton: {},
        $video: {},
        isVideoMute: false,

        _init: function () {
            this._initVars();
            this._initMediaCheck();
            this._initBindings();

            if (window.matchMedia("(max-width: 414px)").matches) {
                let video = this.$video.get(0);

                video.addEventListener('webkitendfullscreen', function (e) {
                    $(this).parent(".video-container").removeClass('muted play');
                    $(this).parent(".video-container").find('video').get(0).pause();
                });
            }

        },

        _initBindings: function() {
            if(this.$muteButton.length){
                this.$muteButton.off('click')
                    .on('click', _.bind(this.manageSound, this));
            }

            if (this.$video.hasClass(this.options.video.class.playOnScroll)) {
                $(window).on('scroll', _.bind(this.playOnScroll, this));
            }
        },

        _initVars: function() {
            this.$muteButton = this.element.find(this.options.muteButton);
            this.$playButton = this.element.find(this.options.playButton);
            this.$video      = this.element.find('video');
        },

        _initMediaCheck: function() {
            mediaCheck({
                media: this.options.mqDesktop,
                entry: function () {
                    this._initDesktop();
                }.bind(this),
                exit: function () {
                    this._initMobile();
                }.bind(this)
            });
        },

        _initDesktop: function () {
            this.element.removeClass('play')
                        .addClass(this.options.video.attribute.mute);

            /* Video is automatic played */
            if (!this.$video.hasClass(this.options.video.class.playOnScroll)) {
                this.launchVideo(true);
            }

            //Fix IOS bug with video tag, works only with attribute "controls"
            if (/iPhone|iPad|iPod/i.test(window.navigator.userAgent)) {
                $(this.$video.get(0)).attr('controls', 'controls');
            }

            return this;
        },

        _initMobile: function () {
            if(!this.element.hasClass('mobile-not-mute')) {
                this.element.removeClass('muted play');

                this.$video.get(0).pause();

                if(this.$playButton.length){
                    this.$playButton.on('click', _.bind(this.launchVideo, this, false));
                }
            } else {
                this._initDesktop();
            }

            return this;
        },

        manageSound: function() {
            var attribute = this.options.video.attribute.mute,
                isMute;

            isMute = this.$video.prop(attribute) === false || this.$video.prop(attribute) === 'undefined';

            this.element.toggleClass(attribute, isMute);
            this.changeAttribute(attribute, isMute);
        },

        playOnScroll: function() {
            if (this.$video.hasClass(this.options.video.class.playOnScroll)) {
                this.$video.removeClass(this.options.video.class.playOnScroll);
                this.launchVideo(true);
            }
        },

        launchVideo: function(mute) {
            var throttled = _.throttle(this.scrollAction, this.options.wait);

            this.playVideo();

            if(mute === false) {
                this.element.removeClass(this.options.video.attribute.mute);
                this.changeAttribute(this.options.video.attribute.mute, false);
            }

            $(window).on('scroll', throttled.bind(this));
        },

        playVideo: function() {
            this.element.addClass('play');

            this.$video.get(0).play();
        },

        changeAttribute: function(attribute, value) {
            this.$video.prop(attribute, value);
        },

        scrollAction: function() {
            var videoHeight = this.$video.height(),
                videoOffsetTop = this.$video.offset().top,
                scrollbarPosition = $(document).scrollTop();

            if(!this.element.hasClass(this.options.video.attribute.mute)) {
                if (scrollbarPosition > videoOffsetTop + videoHeight) {
                    if (window.matchMedia("(max-width: 1024px)").matches) {
                        this.element.removeClass('play');
                    }
                    this.changeAttribute(this.options.video.attribute.mute, true);
                    this.isVideoMute = true;
                    this.element.find('video').get(0).pause();
                }
                else {
                    if(this.isVideoMute === true && !( window.matchMedia("(max-width: 1024px)").matches )){
                        this.changeAttribute(this.options.video.attribute.mute, false);
                        this.isVideoMute = false;
                        this.element.find('video').get(0).play();
                    }
                }
            }
        }
    });

    return $.synolia.video;
});
