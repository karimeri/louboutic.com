// Synolia additionnal validation rules

// Tips : To see if rule is correctly set, enter : jQuery.validator.messages in any browser javascript console

define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($){

    /**
     * Collection of validation rules including rules from additional-methods.js
     * @type {Object}
     */
    var rules = {
        "validate-zip-fr": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^((2[A|B])|(?!97|98)[0-9]{2})[0-9]{3}$/i.test(v);
            },
            $.mage.__('Please enter a valid french postcode for mainland France and Corsica')
        ],
        "validate-zip-be": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^[1-9]{1}[0-9]{3}$/i.test(v);
            },
            $.mage.__('Please enter a valid belgian postcode')
        ],
        "validate-zip-nl": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$/i.test(v);
            },
            $.mage.__('Please enter a valid netherlands postcode')
        ],
        "validate-zip-lu": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^[1-9]{1}[0-9]{3}$/i.test(v);
            },
            $.mage.__('Please enter a valid luxembourg postcode')
        ],
        "validate-zip-de": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^[0-9][0-9]{4}$/i.test(v);
            },
            $.mage.__('Please enter a valid deutschland postcode')
        ],
        "validate-zip-at": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^[0-9][0-9]{3}$/i.test(v);
            },
            $.mage.__('Please enter a valid austria postcode')
        ],
        "validate-zip-gb": [
            function (v) {
                return $.mage.isEmptyNoTrim(v) || /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/i.test(v);
            },
            $.mage.__('Please enter a valid uk postcode')
        ]
    };

    $.each(rules, function (i, rule) {
        rule.unshift(i);
        $.validator.addMethod.apply($.validator, rule);
    });

});
