require(['mage/template'], function(mageTemplate) {

    var html = '<h1 class="title"><%- data.title %></div>';
    var template = mageTemplate(html);
    var data = {
        title: 'test titre'
    };

    var completedTemplate = template({data: data});

    console.log(completedTemplate);
});