define([
    'jquery',
    'jquery/ui',
    'domReady!'
], function ($) {
    'use strict';

    $.widget('synolia.pageLoader', {

        options: {
            loader: '.loader'
        },

        _init:function() {
            var $loader = this.element.find(this.options.loader);

            if($loader.length) {
                $loader.fadeOut('slow');
            }

        }
    });

    return $.synolia.loader;
});