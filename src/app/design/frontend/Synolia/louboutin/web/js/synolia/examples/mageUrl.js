require(['mage/url'], function(url) {

    console.log(url.build('module/controller/action'));
    console.log(url.build('catalogsearch/result', {'q': 'test'}));
    console.log(url.buildMedia('synolia/easycms/sample.png'));
    console.log(url.buildPub('Magento_Email/logo_email.png'));
});