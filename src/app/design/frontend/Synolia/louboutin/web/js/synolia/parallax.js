define([
    'jquery',
    'skrollr',
    'matchMedia',
    'jquery/ui',
    'domReady!'
], function ($, skrollr, mediaCheck) {
    'use strict';

    $.widget('synolia.parallax', {

        options: {
            mqDesktop: '(min-width: 770px)'
        },

        _create: function() {
            var breakpoint = this.element.data('parallax-breakpoint') || this.options.mqDesktop;

            mediaCheck({
                media: breakpoint,
                entry: function () {
                    this._initSkrollR();
                }.bind(this),
                exit: function () {
                    if (this.element.data('parallax-mobile') === false) {
                        this._destroySkrollR();
                    }
                }.bind(this)
            });
        },

        _initSkrollR: function() {
            skrollr.init({
                smoothScrolling: false,
                forceHeight: false,
                mobileCheck: function() {
                    return false;
                }
            });
        },

        _destroySkrollR: function() {
            skrollr.init().destroy();
        }
    });

    return $.synolia.parallax;
});