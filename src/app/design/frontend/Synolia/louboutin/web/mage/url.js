/* eslint-disable strict */
define([
    'jquery',
    'underscore'
], function ($, _) {
    var baseUrl = '';
    var mediaUrl = '';
    var pubUrl = '';

    return {
        /**
         * @param {String} path
         * @return {*}
         */
        build: function(path, args, override) {
            if (path.indexOf(baseUrl) === -1) {
                path = baseUrl + path;
            }

            if (!args) {
                return path;
            }
            if (typeof override !== "boolean") {
                override = true;
            }

            var splitAnchor = path.split('#');
            var anchor = splitAnchor.length > 1 ? '#' + splitAnchor[1] : '';
            path = path.replace(anchor, '');

            var splitUrl = path.split('?'),
                argsConfig = {
                    'inPath' : {},
                    'new' : {}
                },
                originalUrl = splitUrl[0],
                inJson = {};

            if (splitUrl.length > 1) {
                argsConfig.inPath = splitUrl[1];
                argsConfig.inPath = args.inPath.split('&');

                var temp,
                    key,
                    value;
                _.each(argsConfig.inPath, function(element) {
                    temp = element.split('=');
                    key = temp[0];
                    value = temp.length > 1 ? temp[1] : '';

                    inJson[key] = value;
                });
            }

            argsConfig.new = override ? _.extend(inJson, args) : _.extend(args, inJson);

            var query;
            query = _.size(argsConfig.new) ? '?' : '';
            query += $.param(argsConfig.new);

            return originalUrl + query + anchor;
        },


        setBaseUrl: function (url) {
            baseUrl = url;
        },

        setMediaUrl: function(url) {
            mediaUrl = url;
        },

        buildMedia: function(path) {
            if (path.indexOf(mediaUrl) != -1) {
                return path;
            }
            return mediaUrl + path;
        },

        setPubUrl: function(url) {
            pubUrl = url;
        },
        
        buildPub: function(path) {
            if (path.indexOf(pubUrl) != -1) {
                return path;
            }
            return pubUrl + '/' + path;
        }
    };
});
