define([
    'jquery',
    'jquery/ui',
    'jquery/jquery-storageapi',
    'mage/mage',
    'collapsible'
], function ($) {
    'use strict';

    $.widget('synolia.catalogFilters', $.mage.collapsible, {

        options: {
            scrollOnOpen: {
                active: true,
                element: 'html, body'
            }
        },

        activate: function () {
            if (this.options.scrollOnOpen.active && this.options.collateral.openedState) {
                $(this.options.scrollOnOpen.element).scrollTop(0);
            }

            this._super();
        },

        _create: function () {
            this.storage = $.localStorage;
            this.icons = false;

            if (typeof this.options.icons === 'string') {
                this.options.icons = $.parseJSON(this.options.icons);
            }

            this._processPanels();
            this._processState();
            this._refresh();

            if (this.options.icons.header && this.options.icons.activeHeader) {
                this._createIcons();
                this.icons = true;
            }

            this.element.on('dimensionsChanged', function (e) {
                if (e.target && e.target.classList.contains('active')) {
                    $(window).scrollTop(0);
                }
            }.bind(this));

            this._bind('click');
            this._trigger('created');
        },

        _bind: function(event) {
            this._super(event);

            $('.header .navbar').on('click', function() {
                this.deactivate();
            }.bind(this));
        }
    });

    return $.synolia.filters;
});
