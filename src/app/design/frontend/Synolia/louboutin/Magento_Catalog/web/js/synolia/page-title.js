define([
    'jquery',
    'jquery/ui',
    'domReady!'
], function ($) {
    'use strict';

    $.widget('synolia.pageTitle', {
        options: {
            bodyClassProductView: 'catalog-product-view',
            pageWrapperSelector: '.page-wrapper .column.main',
            titleWrapperSelector: '.page-title-wrapper.product',
            margin: 10
        },

        _create: function() {
            //Calculating padding top for parent div to fix long titles which cut product price and more
            //Only on product view page
            if ($('body').hasClass(this.options.bodyClassProductView)) {
                //Only on desktop because title position isn't absolute in mobile
                if (window.innerWidth > 1024) {
                    $(this.options.pageWrapperSelector).css(
                        'padding-top', ($(this.options.titleWrapperSelector).innerHeight() + this.options.margin) + 'px');
                }
            }
        }
    });

    return $.synolia.pageTitle;
});
