define([
    'jquery',
    'Amasty_PageSpeedOptimizer/js/jquery.lazy'
], function($, lazy){
    'use strict';

    $.widget('synolia.infiniteScrolling', {
        options: {
            toolbarSelector: '.toolbar-products',
            nextPageItemSelector: '.pages .pages-item-next',
            nextPageButtonSelector: 'a.next'
        },

        nextPageExists: false,

        $window: undefined,

        $mainToolbar: undefined,
        $toolbar: undefined,
        $nextPageItem: undefined,
        $nextPageButton: undefined,

        $productsItems: undefined,

        ajaxInProgress: false,

        _create: function() {
            _.bindAll(this, '_scroll');
            var $html = $('html');

            this.$window        = $(window);
            this.$productsItems = this._getProductItems($html);
            this.$mainToolbar   = $(this.options.toolbarSelector).last();


            if (!this.$mainToolbar.length) {
                return false;
            }

            this._setToolbarSelector($html);

            this._bindEvents();
        },

        _setToolbarSelector: function ($wrapper) {
            this.$toolbar       = $wrapper.find(this.options.toolbarSelector).last();
            this.$nextPageItem  = this.$toolbar.find(this.options.nextPageItemSelector);
            this.nextPageExists = this.$nextPageItem.length > 0;

            if (this.nextPageExists === true) {
                this.$nextPageButton = this.$nextPageItem.find(this.options.nextPageButtonSelector);
            }
        },

        _getProductItems: function($wrapper) {
            return $wrapper.find('.products-grid').children('.product-items');
        },

        _bindEvents: function () {
            this.$window.on('scroll', _.throttle(this._scroll, 12));
        },

        _scroll: function () {
            if (
                this.$window.scrollTop() + this.$window.height() > this.$mainToolbar.offset().top &&
                this.nextPageExists === true &&
                this.ajaxInProgress === false
            ) {
                this.ajaxInProgress = true;

                $('body').trigger('processStart');

                $.get({
                    url: this.$nextPageButton.attr('href'),
                    cache: true
                }, function (dataHtml) {
                    var $dataHtml      = $(dataHtml),
                        $productsItems = this._getProductItems($dataHtml);

                    this._setToolbarSelector($dataHtml);

                    this.$productsItems.append($productsItems.find('.item'));

                    $('.products-grid li').each(function () {
                        var $productImg = $(this).find('.product-item-photo').find('img');
                        var $productImgHover = $(this).find('.product-item-photo-hover').find('img');

                        $productImgHover.attr('src', $productImgHover.attr('data-amsrc'));

                        $productImg.attr('src', $productImg.attr('data-amsrc'));
                    });

                    this.ajaxInProgress = false;
                    $('body').trigger('processStop');
                }.bind(this));
            }
        }
    });

    return $.synolia.infiniteScrolling;
});