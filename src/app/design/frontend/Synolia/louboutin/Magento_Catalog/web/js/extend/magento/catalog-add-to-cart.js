define([
    'jquery',
    'mage/translate',
    'jquery/ui',
    'Magento_Catalog/js/catalog-add-to-cart'
], function($, $t) {
    'use strict';

    $.widget('synolia.catalogAddToCart', $.mage.catalogAddToCart, {

        options: {
            dropDownSelector: '[data-role="dropdownDialog"]'
        },

        _create: function () {
            this._super();

            this._bindAfterAddToCartActions();
        },

        _bindAfterAddToCartActions: function() {
            $(document).on('ajax:addToCart', this._displayMinicart.bind(this));
        },

        _displayMinicart: function() {

            $(this.options.minicartSelector).find(this.options.dropDownSelector)
                .dropdownDialog('open');

            $('html, body').animate({ scrollTop: 0 }, 400);
        },

        enableAddToCartButton: function (form) {
            var addToCartButtonTextAdded = this.options.addToCartButtonTextAdded || $t('Added'),
                self = this,
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);

            if($(this.options.addToCartButtonSelector).hasClass('preorder')) {
                this.options.addToCartButtonTextDefault = $t('Pre Order');
            }

            addToCartButton.find('span').text(addToCartButtonTextAdded);
            addToCartButton.attr('title', addToCartButtonTextAdded);

            setTimeout(function () {
                var addToCartButtonTextDefault = self.options.addToCartButtonTextDefault || $t('Add to Cart');

                addToCartButton.removeClass(self.options.addToCartButtonDisabledClass);
                addToCartButton.find('span').text(addToCartButtonTextDefault);
                addToCartButton.attr('title', addToCartButtonTextDefault);
            }, 1000);
        }
    });

    return $.synolia.catalogAddToCart;
});