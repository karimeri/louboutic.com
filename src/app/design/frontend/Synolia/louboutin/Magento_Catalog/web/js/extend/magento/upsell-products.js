define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'Magento_Catalog/js/upsell-products'
], function ($, mediaCheck) {
    'use strict';

    $.widget('synolia.upsellProducts', $.mage.upsellProducts, {

        options: {
            mediaBreakpoint: '(max-width: 1024px)',
            desktopContainer: '.product.media',
            mobileContainer: '.product-info-main'
        },

        _create: function() {
            mediaCheck({
                media: this.options.mediaBreakpoint,
                entry: $.proxy(function () {
                    this._toggleMobileMode();
                }, this),
                exit: $.proxy(function () {
                    this._toggleDesktopMode();
                }, this)
            });

            this._super();
        },

        _toggleMobileMode: function() {
            if (!$(this.element).parents(this.options.mobileContainer).length) {
                $(this.element).appendTo(this.options.mobileContainer);
            }
        },

        _toggleDesktopMode: function() {
            if (!$(this.element).parents(this.options.desktopContainer).length) {
                $(this.element).appendTo(this.options.desktopContainer);
            }
        }
    });

    return $.synolia.upsellProducts;
});
