var config = {
    map: {
        '*': {
            infiniteScrolling: 'Magento_Catalog/js/synolia/infinite-scrolling',
            catalogFilters:    'Magento_Catalog/js/synolia/filters',
            catalogAddToCart:  'Magento_Catalog/js/extend/magento/catalog-add-to-cart',
            upsellProducts:    'Magento_Catalog/js/extend/magento/upsell-products',
            pageTitle:         'Magento_Catalog/js/synolia/page-title'
        }
    }
};
