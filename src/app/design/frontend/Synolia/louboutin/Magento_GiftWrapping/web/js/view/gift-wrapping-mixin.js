define(function () {
    'use strict';

    var giftWrappingConfig = window.giftOptionsConfig ?
        window.giftOptionsConfig.giftWrapping :
        window.checkoutConfig.giftWrapping;

    var mixin = {
        isBeauty: function () {
            return giftWrappingConfig['isBeauty']
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
