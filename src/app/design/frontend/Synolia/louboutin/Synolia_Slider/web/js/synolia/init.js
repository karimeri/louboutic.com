define([
    'jquery',
    'slick',
    'owlCarousel',
    'jquery/ui'
], function($){
    "use strict";

    $.widget('synolia.slider', {

        _create: function() {
            if (!this.element.hasClass('init-slider')) { // explicit call to this widget
                if (this.options.sliderType === 'owlCarousel') {
                    this.element.owlCarousel(this.options);
                } else {
                    this.element.slick(this.options);
                }
            } else { // implicit call to this widget (CMS Widget init)
                this._initCmsSliders();
            }
        },

        _initCmsSliders: function() {
            _.each(window.sliders, function(sliderData){
                var sliderConfig,
                    $slider,
                    identifier,
                    customPaging;

                // We merge the config coming from the BO to the one coming from the template
                sliderConfig = _.filter(this.options.sliders, function(slider) {
                    return slider.identifier === sliderData.identifier;
                }, this);

                if (sliderConfig.length) {
                    sliderConfig = $.extend(true, {}, sliderConfig[0], sliderData);
                } else {
                    sliderConfig = sliderData;
                }

                $slider = $(sliderConfig.identifier);
                identifier = sliderConfig.identifier;

                if (!$slider.length) {
                    return;
                }

                if (sliderConfig.options.sliderType === 'owlCarousel') {
                    $slider.owlCarousel(sliderConfig.options);
                } else {
                    this._slickSliderInit($slider);

                    /**
                     * foreach event to handle, we bind a widget function to the event name
                     * this function is given the parameters described in slick documentation
                     * plus the identifier of the slider as the first argument
                     */
                    _.each(sliderConfig.events, function(handler, eventName) {
                        var fnHandler = this[handler];
                        if (typeof fnHandler === 'function') {
                            $slider.on(eventName, function() {
                                var args = Object.values(arguments);
                                args.unshift(identifier);
                                fnHandler.apply(
                                    this,
                                    args
                                );
                            }.bind(this));
                        }
                    }, this);



                    if (typeof sliderConfig.customPaging !== 'undefined' &&
                        typeof (customPaging = this[sliderConfig.customPaging]) === 'function') {
                        sliderConfig.options.customPaging = customPaging;
                    }

                    $slider.not('.slick-initialized').slick(sliderConfig.options);
                }
            }, this);
        },

        _slickSliderInit: function($slider) {
            $slider.on('init', function(event){
                $(event.currentTarget).find('img').css('visibility', 'visible');
            });
        },
    });

    return $.synolia.slider;
});