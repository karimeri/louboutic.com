define(
    [
        'jquery',
        'Magento_Customer/js/model/customer',
        'mage/url'
    ],
    function (
        $,
        customer,
        urlBuilder
    ) {
        'use strict';

        var mixin = {
            getTokens: function () {
                var urlTokens = urlBuilder.build('cybersource/index/gettokens'),
                    tokenListHtml = '';

                if (customer.isLoggedIn()) {
                    $.ajax({
                        url: urlTokens,
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            if (Object.keys(data).length > 0) {
                                tokenListHtml += '<input type="hidden" ' +
                                    'id="cybersource-token-new" ' +
                                    'name="token_type" value="cybersource-token-new"/>';
                                $('.payment-method-token-list').html(tokenListHtml);
                                $('#cybersource-cvv-form-validate').hide();
                                $('#cybersource_credit_card_form').show();
                            } else {
                                tokenListHtml += '<input type="hidden" ' +
                                                        'id="cybersource-token-new" ' +
                                                        'name="token_type" value="cybersource-token-new"/>';
                                $('.payment-method-token-list').html(tokenListHtml);
                                $('#cybersource_credit_card_form').show();
                                $('#cybersource-cvv-form-validate').hide();
                            }
                            $('.cybersource-button-action').removeClass('disabled');
                        }
                    });

                    return 'Payment token loading...';
                } else {
                    return '';
                }
            }
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);