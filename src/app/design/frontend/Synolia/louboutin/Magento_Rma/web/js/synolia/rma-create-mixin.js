define([
    'jquery'
], function (
    $
) {
    var mixin = {
        _showQuantity: function (type, index, qty) {
            var qtyReqBlock = $(this.options.qtyReqBlock + '_' + index),
                remQtyBlock = $(this.options.remQtyBlock + '_' + index),
                remQty = $(this.options.remQty + '_' + index);

            if (type === this.options.prodTypeBundle) {
                if (qtyReqBlock.length) {
                    qtyReqBlock.hide();
                }

                if (remQtyBlock.length) {
                    remQtyBlock.hide();
                }
            } else {
                if (qtyReqBlock.length) {
                    qtyReqBlock.show();
                }

                if (remQtyBlock.length) {
                    remQtyBlock.show();
                }

                if (remQty.length) {
                    remQty.text(qty);

                    var displayedQty = qty === '1' ? qty : '0';

                    $('[id="items:qty_requested' + index + '"]').val(displayedQty);
                    this._recountRemQty();
                }
            }
        }
    };

    return function (target) { // target == Result that Magento_Rma/rma-create returns.
        return $.widget('mage.rmaCreate', target, mixin);
    };
});