define([
    'jquery',
    'd3',
    'TimelineLite'
], function(
    $,
    d3,
    TimelineLite,
) {
    var BoltAnimation = function(){
        this.init();
        this.setTimeline();
    };

    BoltAnimation.prototype = {

        _create: function () {
            new TimelineLite();
        },

        init: function () {

            this.louboutinWorldContainer = document.querySelector('.louboutinworld-container');
            this.paths = this.louboutinWorldContainer.querySelectorAll('.bolt path');
            this.boltTop = this.paths[2];
            this.boltLeft = this.paths[1];
            this.boltRight = this.paths[0];
            this.tl;

        },

        setTimeline: function () {
            this.tl = new TimelineLite();

            //this.tl.pause();

            // this.tl.to(this.boltTop, 0.4, { css: { opacity: 0.9, scale: 1.05 }, ease: Elastic.easeOut.config(1, 0.3) }, 0)
            //     .to(this.boltRight, 0.9, { css: { x: '5px', y: '3px', scaleY: 1.06 }, ease: Elastic.easeOut.config(1.75, 0.1) }, 0.3)
            //     .to(this.boltLeft, 1.2, { css: { x: '-4px', y: '-3px', scaleY: 0.9 }, ease: Elastic.easeOut.config(2.5, 0.1) }, 0.5)
            //     .to(this.boltTop, 0.8, { css: { scaleY: 1.15 }, ease: Elastic.easeOut.config(2.5, 0.1) }, 0.7)
            //     .to(this.boltLeft, 0.8, { rotation: 2, ease: SteppedEase.config(4) }, 0.3)
            //     .to(this.boltRight, 0.6, { rotation: -2, ease: SteppedEase.config(4) }, 0.3);
            //
            // this.tl.play();
        }

    };

    return BoltAnimation;

});
