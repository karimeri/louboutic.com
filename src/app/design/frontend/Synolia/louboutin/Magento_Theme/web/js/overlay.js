define([
    'jquery',
    'mage/translate'
], function(
    $,
    $t
) {
    $.widget('synolia.overlay', {
        options: {
            hasCloseButton: true,
            triggerOpenSelector: undefined
        },


        /**
         * Constructor
         * @private
         */
        _create: function () {
            this._bindEvents();

            if (this.options.hasCloseButton === true) {
                this._createCloseButton();
            }
        },

        /**
         * Bind events
         * @private
         */
        _bindEvents: function() {
            _.bindAll(this, '_open', '_close');

            $(document).on('click', this.options.triggerOpenSelector, this._open);
            $(this.element).on('click', this._close);
            $(this.element).children().on('click', function(event) {
                event.stopPropagation();
            });
        },

        _createCloseButton: function () {
            var $button = $('<button/>')
                .attr('type', 'button')
                .addClass('close')
                .html('<span>' + $t('Close') + '</span>')
                .on('click', this._close);

            $(this.element).find('.overlay-content').prepend($button);
        },

        _open: function (event) {
            event.preventDefault();
            $(this.element).fadeIn();
        },

        _close: function () {
            $(this.element).fadeOut();
        }
    });

    return $.synolia.overlay;
});
