define([
    'jquery',
    'underscore',
    'matchMedia',
    'electricLogo',
    'boltAnimation'
], function(
    $,
    _,
    mediaCheck,
    ElectricLogo,
    BoltAnimation
) {
    $.widget('synolia.menuHeader', {
        options: {
            headerSelector: '.page-header',
            scrollNotifySelector: 'body',
            parentSubmenuSelector: '.menu-categories',
            transitionDuration: 400,
            autocompleteStateElement: 'body',
            autocompleteSearchInput: '#algolia-searchbox input',
            scrollActiveBreakpoint: 1024
        },

        currentScrollTop: 0,

        $helpNav:'',
        $helpNavParent:'',
        $helpBox:'',

        $headerElement: '',
        $scrollNotifyElement: '',

        currentMenuDisplayed: '',

        searchTimeoutPanelWrapper: undefined,

        /**
         * Constructor
         * @private
         */
        _create: function () {
            new ElectricLogo();
            new BoltAnimation();

            this.$helpNavParent = $('.header-help-wrapper');
            this.$helpNav       = this.$helpNavParent.find('.opt.help');
            this.$helpBox       = this.$helpNavParent.find('.dd-menu');

            this.$headerElement = $(this.options.headerSelector);
            this.$scrollNotifyElement = $(this.options.scrollNotifySelector);

            this._bindEvents();
        },

        /**
         * Bind events
         * @private
         */
        _bindEvents: function() {
            _.bindAll(this,
                '_onScroll',
                '_toggleHelpNav',
                '_toggleMenu',
                '_toggleMobileSearch',
                '_parentSubmenuEnter',
                '_showPanelWrapper',
                '_showPanel'
            );

            $(window).on('mousewheel DOMMouseScroll', _.throttle(this._onScroll, 12));

            this.$helpNav.on('mouseenter', this._toggleHelpNav);
            this.$helpNavParent.on('mouseleave', this._toggleHelpNav);

            _.each($(this.options.headerSelector + ' [data-toggle-menu]'), function(element) {
                $(element).on($(element).data('toggle-menu'), this._toggleMenu);
            }.bind(this));

            $(document).on('click', '.narrowscreen .search', this._toggleMobileSearch);

            $(document).on('mouseenter', this.options.parentSubmenuSelector, this._parentSubmenuEnter);

            mediaCheck({
                media: '(max-width: 1024px)',
                // Switch to Mobile Version
                entry: function () {
                    if (this._isSearchDisplayed()) {
                        this._closeMenu();
                    }
                }.bind(this)
            });
        },

        /**
         * On page scroll
         * @private
         */
        _onScroll: function () {
            $(window).scroll(function() {
                if(this.innerWidth > 1024) {
                    if ($(this).scrollTop() > 10){
                        $('.page-header').addClass('minimized');
                        $('body').addClass('menu-minimized');
                    }
                    else {
                        $('.page-header').removeClass('minimized');
                        $('body').removeClass('menu-minimized');
                    }
                }
            });
        },

        /**
         * Toggle help navigation
         * @private
         */
        _toggleHelpNav: function (){
            this.$helpNavParent.toggleClass('dd-open');
        },

        /**
         * Toggle categories menu
         * @param event
         * @private
         */
        _toggleMenu: function (event) {
            var category      = $(event.currentTarget).data('category'),
                $menuCategory = $(this.options.parentSubmenuSelector + ' [data-category="' + category + '"]');

            this.$headerElement.off('mouseleave');

            if (
                category === this._getCurrentMenu() &&
                $(this.options.parentSubmenuSelector).height() > 0 &&
                $(event.currentTarget).data('toggle-menu') === 'click'
            ) {
                this._closeMenu();

                return;
            } else if (category === this._getCurrentMenu()) {
                return;
            }

            if (this._isSearchDisplayed()) {
                this.searchTimeoutPanelWrapper = setTimeout(function() {
                    this._showPanel($menuCategory, $(event.currentTarget));
                }.bind(this), this.options.transitionDuration);
            } else {
                this._showPanel($menuCategory, $(event.currentTarget));
            }
        },

        /**
         * Close categories menu
         * @private
         */
        _closeMenu: function() {
            if (this._isSearchDisplayed()) {
                // Empty results
                var $algoliaSearchInput = $(this.options.autocompleteSearchInput);
                $algoliaSearchInput.val('');
                $algoliaSearchInput.get(0).dispatchEvent(new Event('input'));
                $(this.options.autocompleteStateElement).removeClass('autocomplete-update');
            }

            this._hideAllSubmenus();

            $(this.options.parentSubmenuSelector).css({
                'height': 0
            });

            _.delay(function(){
                $(this.options.parentSubmenuSelector).hide();
            }.bind(this), this.options.transitionDuration);

            this.currentMenuDisplayed = '';
        },

        /**
         * Hide every submenu panels
         * @private
         */
        _hideAllSubmenus: function() {
            $(this.options.parentSubmenuSelector + ' [data-category]')
                .addClass('hide')
                .css('opacity', 0);
        },

        /**
         * Show panel wrapper
         * @param $currentCategory
         * @param $element
         * @private
         */
        _showPanelWrapper: function($currentCategory, $element) {
            $(this.options.parentSubmenuSelector).show();

            $(this.options.parentSubmenuSelector).css({
                'height'          : $currentCategory.outerHeight(),
                'background-color': $element.data('background-color') || '#fff'
            });
        },

        /**
         * Show panel
         * @param $currentCategory
         * @param $element
         * @private
         */
        _showPanel: function ($currentCategory, $element) {
            var category = $element.data('category');

            this._hideAllSubmenus();
            this._showPanelWrapper($currentCategory, $element);

            this.currentMenuDisplayed = category;

            /* Show correct panel */
            _.delay(function() {
                $currentCategory.removeClass('hide').animate({'opacity': 1});

                if (category === 'search') {
                    $('#search').focus();
                }
            }.bind(this), this.options.transitionDuration);

            if ($element.data('toggle-menu') === 'mouseenter') {
                this.$headerElement.on('mouseleave', function () {
                    this._closeMenu();
                }.bind(this));
            }
        },

        /**
         * Toggle mobile search bar
         * @private
         */
        _toggleMobileSearch: function () {
            $('.mobile-search-panel').toggleClass('active');
            if ($(".mobile-search-panel").hasClass("active")) {
                $('#search_mobile').focus();
                $('#algolia-autocomplete-container').show();
            } else {
                $('#search_mobile').blur();
                $('#algolia-autocomplete-container').hide();
                // Empty results
                var algoliaSearchInput = $('#search_mobile');
                algoliaSearchInput.val('');
                algoliaSearchInput.get(0).dispatchEvent(new Event('input'));
            }
        },

        /**
         * On panel wrapper enter
         * @private
         */
        _parentSubmenuEnter: function () {
            if (this._isSearchDisplayed()) {
                clearTimeout(this.searchTimeoutPanelWrapper);
            }
        },

        _isSearchDisplayed: function() {
            return $(this.options.parentSubmenuSelector).find('[data-category="search"]').is(':visible');
        },

        _getCurrentMenu: function() {
            if (! $(this.options.parentSubmenuSelector).is(':visible')) {
                this.currentMenuDisplayed = '';
            }

            return this.currentMenuDisplayed;
        }
    });

    return $.synolia.menuHeader;
});
