define([
    'jquery',
    'd3'
], function(
    $,
    d3
) {
    var ElectricLogo = function(){
        // Params
        this.points = 12;
        this.minimumLifetime =  100;
        this.$svg = d3.selectAll('svg.logo-shape');

        this.color = '#ed1e24';

        this.distMedium = 50;
        this.logoRadius = 67.5;

        this.sparkRadius = 100;

        this.mouseAngle = 0;

        // Vars
        this.paths = [];
        this.timers = [];
        this.logo = document.querySelector('.logo-wrp > div');
        this.$logoContainer = document.querySelector('.logo-trigger');
        this.center = {x: 100, y: 100};
        this.maxRadius = 50;
        this.containerCenter = {left: 0, top: 0};

        this.containerCenter.left += this.center.x;
        this.containerCenter.top += this.center.y;
        this.zeroPoint = {x: 0, y:0};
        this.Geo = new Geo();

        // listeners
        this.mousemoveCallback = this.getMouseCoordinates.bind(this);
        this.$logoContainer.addEventListener('mouseenter', this.startTrackMouse.bind(this));
        this.$logoContainer.addEventListener('mouseleave', this.endTrackMouse.bind(this));

        this.init();
    };

    ElectricLogo.prototype = {
        init: function() {
            this.$circle = this.$svg.append('circle')
                .attr('cx', this.center.x)
                .attr('cy', this.center.y)
                .attr('r', this.logoRadius)
                .attr('fill', 'transparent')
                .attr('stroke', 'transparent')
                .attr('strokeWidth', 0)
                .attr('opacity', 1);

            var end = {x: this.center.x, y: this.center.y-this.logoRadius};
            var start = this.Geo.rotatePoint(end.x, end.y, -300, this.center);

            this.defaultStr = "M"+start.x+","+start.y+" A"+this.logoRadius+","+this.logoRadius+" 0 1,1 "+end.x+","+end.y+"";
            this.closePath = "A"+this.logoRadius+","+this.logoRadius+" 0 0,1 "+start.x+","+start.y;

            this.$path = this.$svg.append("path")
                .attr('d', this.defaultStr+this.closePath) //  L160,270 L140,255
                .attr('fill', 'transparent')
                .attr('strokeWidth', 2)
                .attr('stroke', 'transparent');
        },

        stopWobble: function() {
            if(this.wobbleTimer) {
                clearTimeout(this.wobbleTimer);
            }
            this.$circle
                .attr('opacity', 1);

            this.$path.transition()
                .duration(0)
                .attr('d', this.defaultStr+this.closePath);
        },

        iterateWobble: function(start) {
            var points = [];

            for(var i = 1; i <= 30; i++) {
                var angle = Math.round((115/30*i)-5+Math.random()*10);
                var height = (this.logoRadius) + Math.round((-1) + Math.random()*2)//*(this.getIntensity()/20);
                points.push(this.Geo.rotatePoint(this.center.x, this.center.y + height, -angle, this.center));
            }

            var str = this.defaultStr;
            for(var i = 0; i < points.length; i++) {
                str += " L"+points[i].x+","+points[i].y;
            }

            this.$path
                .attr('transform', 'rotate('+Math.round(this.mouseAngle+30)+','+this.center.x+','+this.center.y+')')

            if(start) {
                this.$path.transition()
                    .duration(0)
                    .attr('d', str);
            } else {
                this.$path.transition()
                    .attr('d', str)
                    .duration(80);
            }

            this.wobbleTimer = setTimeout(this.iterateWobble.bind(this), 80);
        },

        getPointsStr: function() {
            var pointsStr = '';
            for(var i = 0; i < this.points.length; i++) {
                var point = this.points[i];
                if(i > 0) pointsStr += ',';
                pointsStr += point.x + ',' + point.y;
            }

            return pointsStr;
        },

        getPoint: function(i) {
            if(this.timers[i]) {
                clearTimeout(this.timers[i]);
            }

            var lifetime = this.minimumLifetime+Math.round(Math.random()*this.minimumLifetime*3);

            if(lifetime >= lifetime*3.5) {
                lifetime = 1000;
            }

            if(this.paths[i]) {
                this.paths[i].remove();
            }

            var startAngle = Math.round(this.mouseAngle-(this.sparkRadius/2))
            var angle;

            angle = startAngle + ((this.sparkRadius/this.points) * i);
            angle += Math.round(-5+Math.random()*10);
            if(Math.round(Math.random()*10) == 10) {
                angle *= -1;
            }

            var width = 1+Math.round(6*Math.random());
            var height = this.logoRadius+Math.round(3+Math.round(this.distMedium*Math.random())/100*this.getIntensity()+2);

            var color = this.color;
            var top = height;
            var newTop = top-5;
            var baseTop = this.logoRadius-11;

            var p1 = this.Geo.rotatePoint(this.center.x + top, this.center.y, angle, this.center);
            var base1 = this.Geo.rotatePoint(this.center.x + baseTop, this.center.y, (angle)-width, this.center);
            var base2 = this.Geo.rotatePoint(this.center.x + baseTop, this.center.y, (angle)+width, this.center);


            var randMovement = Math.round(-10+Math.random()*20);
            var p2 = this.Geo.rotatePoint(this.center.x + newTop, this.center.y, angle+randMovement, this.center);
            var base12 = this.Geo.rotatePoint(this.center.x + baseTop, this.center.y, (angle)-width+randMovement, this.center);
            var base22 = this.Geo.rotatePoint(this.center.x + baseTop, this.center.y, (angle)+width+randMovement, this.center);
            var $shock = this.$svg.append("polygon")
                .attr('points', p1.x + ',' + p1.y + ',' + base1.x + ',' + base1.y + ',' + base2.x + ',' + base2.y)
                .attr('fill', color)
                .attr('strokeWidth', 0)
                .attr('stroke', 'transparent');

            this.paths[i] = $shock;
            $shock.transition()
                .attr('points', p2.x + ',' + p2.y + ',' + base12.x + ',' + base12.y + ',' + base22.x + ',' + base22.y)
                .duration(lifetime)

            this.timers[i] = setTimeout(this.getPoint.bind(this, i), lifetime);
        },

        startTrackMouse: function() {
            if (window.scrollY > 30) {
                return null;
            }

            $(document).on('mousemove', this.mousemoveCallback);
            this.iterateWobble(true);
            this.$circle
                .attr('opacity', 0);
            for(var i = 0; i<this.points; i++) {
                this.getPoint(i);
            }
        },

        endTrackMouse: function() {
            $(document).off('mousemove', this.mousemoveCallback);
            this.stopWobble();
            for(var i = 0; i<this.points; i++) {
                if (typeof this.paths[i] !== 'undefined') {
                    this.paths[i].remove();
                }
                clearTimeout(this.timers[i]);
            }
        },

        getMouseCoordinates: function(e) {
            var logoOffset = this.logo.getBoundingClientRect();
            var mouse = {
                x:Math.round(e.pageX - logoOffset.left) - this.center.x + 30,
                y: Math.round(e.pageY - logoOffset.top) - this.center.y + 30
            };

            this.mouseAngle = Math.round(this.Geo.getRelativeAngle(this.zeroPoint, mouse));
            this.mouseDistance = Math.round(this.Geo.getRelativeDistance(this.zeroPoint, mouse))
        },

        getIntensity: function() {
            if(!this.mouseDistance) {
                this.mouseDistance = this.maxRadius;
            }
            if(this.mouseDistance < this.logoRadius) {
                this.mouseDistance = this.logoRadius;
            }
            var intensity = (this.mouseDistance*-1+this.maxRadius);
            var percent = intensity/this.maxRadius*100;
            if(percent < 0) {
                percent = 0;
            }
            return percent;
        },

        setScale: function (scale) {
            this.logo.style.transform = "scale(" + scale + ")";
        }
    };


    var Geo = function() {};

    Geo.prototype = {
        rotatePoint: function(x, y, angleDeg, center) {
            var angleRad = angleDeg * Math.PI / 180;
            var xRot = center.x + Math.cos(angleRad) * (x - center.x) - Math.sin(angleRad) * (y - center.y);
            var yRot = center.y + Math.sin(angleRad) * (x - center.x) - Math.cos(angleRad) * (y - center.y);
            return {x: Math.round(xRot), y: Math.round(yRot), angle: angleDeg};
        },

        getRelativeAngle: function(p1, p2) {
            var deltaX = p2.x - p1.x;
            var deltaY = p2.y - p1.y;

            var angleDeg = Math.atan2(deltaY, deltaX) * 180 / Math.PI;

            return angleDeg;
        },

        getRelativeDistance: function(p1, p2) {
            var dist = Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
            return dist;
        }
    };


    return ElectricLogo;
});
