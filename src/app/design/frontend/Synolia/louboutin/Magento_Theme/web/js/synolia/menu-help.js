define([
    'jquery',
    'jquery/ui'
], function($, ui) {
    'use strict';

    $.widget('project.menuHelp', {
        options: {
            container: '.menu-help',
            actionElement: '> a',
            helpContent: '> .help-content'
        },
        $container: undefined,
        $actionElement: undefined,
        $help: undefined,

        _create: function () {
            this.$container =  $(this.options.container);
            this.$actionElement = this.$container.find(this.options.actionElement);
            this.$help =  this.$container.find(this.options.helpContent);

            this._bind();
        },

        _bind: function() {
            this.$actionElement.on('click', function(e) {
                e.preventDefault();

                if (this._isVisible()) {
                    this._hide();
                } else {
                    this._show();
                }
            }.bind(this));
        },

        _isVisible: function() {
            return !this.$help.hasClass('no-display');
        },

        _show: function() {
            this.$help.fadeIn(function() {
                this.$help.removeClass('no-display');
            }.bind(this));
        },

        _hide: function() {
            this.$help.fadeOut(function() {
                this.$help.addClass('no-display');
            }.bind(this));
        }
    });

    return $.project.menuHelp;
});