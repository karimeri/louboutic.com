var config = {
    map: {
        '*': {
            'menuHeader'  : 'Magento_Theme/js/menu-header',
            'd3'          : 'Magento_Theme/js/lib/d3.v4.min',
            'electricLogo': 'Magento_Theme/js/electric-logo',
            'overlay'     : 'Magento_Theme/js/overlay',
            'menuHelp'    : 'Magento_Theme/js/synolia/menu-help',
            'boltAnimation': 'Magento_Theme/js/bolt-animation'
        }
    }
};
