define([
    'jquery',
    'Magento_Checkout/js/view/shipping',
    'Magento_Checkout/js/view/payment',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',
    'Magento_GoogleTagManager/js/google-tag-manager'
], function ($, shipping, payment, quote, customerData) {
    'use strict';

    let checkoutIndex;

    /**
     * Dispatch checkout events to GA
     *
     * @param {Array} data - cart data
     * @param {String} stepIndex - step index
     * @param {String} stepDescription - step description
     *
     * @private
     */
    function notify(data, stepIndex, stepDescription) {
        let i = 0,
            globalDl,
            product,
            shippingOption,
            dlUpdate = {
                'event': 'gtm.ext.event',
                'eventTmp': 'virtualPageView',
                "eventCategory": "enhancedEcommerce",
                "eventAction": "checkout",
                "eventLabel": stepIndex,
                'ecommerce': {
                    'currencyCode': data.currencyCode,
                    'checkout': {
                        'actionField': {
                            'step': stepIndex,
                        },
                        'products': []
                    }
                }
            };

        shippingOption = getShippingMethodTitle();
        if (shippingOption !== null) {
            dlUpdate.ecommerce.checkout.actionField.shippingOption = shippingOption;
        }

        for (i; i < data.products.length; i++) {
            product = data.products[i];
            dlUpdate.ecommerce.checkout.products.push({
                'id': product.id,
                'name': product.name,
                'price': product.price,
                'priceHT': product.priceHT,
                'category': product.category,
                'quantity': product.quantity,
                'dimension1': product.dimension1,
                'dimension2': product.dimension2,
                'gender' : product.gender,

            });
        }

        let customer = customerData.get('customer');
        let userData = {
            userID: '',
            userCountry: '',
            userRegistrationStatus: 'not registered',
            userWishlistStatus: '',
            lastTransactionDate: '',
            lastSubscriptionDate: '',
            connectionStatus: 'not logged'
        };
        if (customer().id) {
            let wishlist = customerData.get('wishlist');
            userData.userID = customer().id;
            userData.userCountry = customer().userCountry;
            userData.userRegistrationStatus = 'registered';
            userData.userWishlistStatus = wishlist().count > 0 ? 'wishlistUser' : 'not wishlist user';
            userData.lastTransactionDate = customer().lastTransactionDate;
            userData.lastSubscriptionDate = customer().lastSubscriptionDate;
            userData.connectionStatus = 'logged';
        }

        globalDl = {...data.pageData};
        globalDl = {...globalDl, ...userData};
        if (parseInt(stepIndex) === 1) {
            window.dataLayer.push(globalDl);
        }
        window.dataLayer.push(dlUpdate);
        checkoutIndex = window.dataLayer.length - 1;
    }

    function getShippingMethodTitle() {
        let shippingMethod = quote.shippingMethod();
        if (!shippingMethod) {
            return null;
        }
        let shippingMethodTitle = shippingMethod['carrier_title'];
        if (typeof shippingMethod['method_title'] !== 'undefined') {
            shippingMethodTitle += ' - ' + shippingMethod['method_title'];
        }

        return shippingMethodTitle;
    }

    return function (data) {
        let events = {
                shipping: {
                    description: 'shipping',
                    index: '1'
                },
                payment: {
                    description: 'payment',
                    index: '2'
                }
            },

            shippingSubscription = shipping.prototype.visible.subscribe(function (value) {
                if (value) {
                    notify(data, events.shipping.index, events.shipping.description);
                    shippingSubscription.dispose();
                }
            }),

            paymentSubscription = payment.prototype.isVisible.subscribe(function (value) {
                if (value) {
                    window.dataLayer.splice(checkoutIndex, 1);
                    notify(data, events.payment.index, events.payment.description);
                    paymentSubscription.dispose();
                }
            });

        window.dataLayer ?
            notify(data, events.shipping.index, events.shipping.description) :
            $(document).on(
                'ga:inited',
                notify.bind(this, data, events.shipping.index, events.shipping.description)
            );
    };
});
