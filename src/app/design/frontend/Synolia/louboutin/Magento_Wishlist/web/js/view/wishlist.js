define([
    'uiComponent',
    'Magento_Customer/js/customer-data'
], function (Component, customerData) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Wishlist/header/wishlist',
            maxItems: 1,
            iterator: 1,
            showPrice: true
        },

        /** @inheritdoc */
        initialize: function (params) {
            this._super();

            this.customer = customerData.get('customer');
            this.wishlist = customerData.get('wishlist');
            this.wishlistUrl = params.wishlistUrl;
        },

        hasWishlistItems: function () {
            return this.wishlist().hasOwnProperty('items') && this.wishlist().items.length > 0;
        }
    });
});
