define([
    'jquery',
    'mage/url',
    'Synolia_Retailer/js/gmap'
], function($, urlBuilder) {
    'use strict';
    /* global google */
    /* global MarkerClusterer */

    $.widget('project.gmap', $.synolia.gmap, {
        _initMap: function() {
            var defaultPosition = {
                lat: this.options.defaultLatitude,
                lng: this.options.defaultLongitude
            };

            this.mapInstance = new google.maps.Map(this.element.get(0), {
                zoom: this.options.defaultZoom,
                minZoom: this.options.defaultZoom,
                center: defaultPosition,
                mapTypeControl: false,
                streetViewControl: false,
                gestureHandling: 'greedy'
            });

            this._initMapComponents();
        },

        _initAutcomplete: function() {
            var input = $(this.options.autocompleteInput).get(0),
                searchBox = new window.google.maps.places.SearchBox(input),
                map = this.mapInstance;

            /* Bias the SearchBox results towards current map's viewport. */
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            /* When use click on search result */
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                /* Place not found */
                if (places.length === 0) {
                    return;
                }

                var place = places[0];
                this.lastSearchedPlace = place;

                this._centerMapOnPosition(place.geometry.location, 13);

                if (this._getMarkersDataFromVisibleMarkers().length === 0) {
                    this._prepareNoResultsInfoWindow(places[0]);
                }
            }.bind(this));

            this.autocompleteInstance = searchBox;

            return this;
        },

        _bindMarkerEvents: function(marker) {
            window.google.maps.event.addListener(marker, 'click', function () {
                this.refresh_lock = true; // when lazyLoading is active, temporary prevent map from being refreshed

                var markerData = this._getMarkerDataFromMarkerObject(marker);

                this.element.trigger('click-marker', [markerData]); // notify that marker has been clicked

                this.mapInstance.setZoom(18);
                this.mapInstance.panTo(marker.position);
                this._scrollMarkerDataList();

                _.delay(function() {
                    this.refresh_lock = false;
                }.bind(this), this.options.lazyLoadingLockingTime);
            }.bind(this));
        },

        _filterMarkers: function(category, filtered) {
            var markers = this.markersArray,
                visibleMarkers,
                markerCategories = [],
                markerClusterImported = this._shouldUseCluster(),
                activeFilterIndex = -1;

            if (markerClusterImported) {
                this.clusterInstance.clearMarkers();
            }

            if (category === '') {
                this.activeFilters = [];
            } else {
                activeFilterIndex = this.activeFilters.indexOf(category);

                if (activeFilterIndex !== -1) {
                    // filter is already active : we remove it
                    this.activeFilters.splice(activeFilterIndex, 1);
                } else {
                    // filter is not active yet : we add it to active filters array
                    this.activeFilters.push(category);
                }
            }

            visibleMarkers = markers.filter(function(marker) {
                // it's possible that there is multiple categories affected for one marker
                markerCategories = marker.category.split(' ');

                if (this.activeFilters.length === 0) {
                    marker.setVisible(true);
                } else {
                    markerCategories.every(function(markerCategory) {
                        if (this.activeFilters.indexOf(markerCategory) !== -1) {
                            marker.setVisible(false);
                            return true;
                        }

                        marker.setVisible(true);
                        return false;
                    }.bind(this));
                }

                return marker.getVisible();
            }.bind(this));

            this._refreshInfoList();

            if (markerClusterImported) {
                this.clusterInstance.addMarkers(visibleMarkers);
            }

            if (filtered) {
                this._enableRemoveActiveFilterLink();
            } else {
                this._disableRemoveActiveFilterLink();
            }
        },

        /**
         * This method is rewritten to geolocate after marker initialization (we want to center map on closest marker)
         */
        _initMapComponents: function() {
            if (this.options.mapStyle !== 'default') {
                this._initStyles();
            }

            this._initAutcomplete();

            this._initMarkers();

            this._initClusters();
            this._initMapMovingEvents();
            this._initListEvents();
        },

        /**
         * This is rewritten to center map on closest marker (from geolocation)
         */
        _initGeolocation: function() {
            this._geolocation(this._centerMapOnClosestMarker);

            return this;
        },

        _initClusters: function() {
            if (!this._shouldUseCluster()) {
                return;
            }

            var clusterOptions = {
                gridSize: 65,
                maxZoom: 12,
                styles: [
                    {
                        url: urlBuilder.buildPub('Synolia_Retailer/images/cluster/m1.png'),
                        height: 16,
                        width: 16,
                        anchor: [0, 0],
                        textSize: 0.001
                    },
                    {
                        url: urlBuilder.buildPub('Synolia_Retailer/images/cluster/m1.png'),
                        height: 16,
                        width: 16,
                        anchor: [0, 0],
                        textSize: 0.001
                    },
                    {
                        url: urlBuilder.buildPub('Synolia_Retailer/images/cluster/m1.png'),
                        width: 16,
                        height: 16,
                        anchor: [0, 0],
                        textSize: 0.001
                    },
                    {
                        url: urlBuilder.buildPub('Synolia_Retailer/images/cluster/m1.png'),
                        width: 16,
                        height: 16,
                        anchor: [0, 0],
                        textSize: 0.001
                    },
                    {
                        url: urlBuilder.buildPub('Synolia_Retailer/images/cluster/m1.png'),
                        width: 16,
                        height: 16,
                        anchor: [0, 0],
                        textSize: 0.001
                    }
                ]
            };

            this.clusterInstance = new MarkerClusterer(this.mapInstance,
                this.markersArray,
                clusterOptions
            );

            return this;
        },

        _centerMapOnPositionAndSelectMarker: function(marker) {
            if (marker) {
                this._centerMapOnPosition(marker.position, 13);
            }
        },
    });

    return $.project.gmap;
});