define([
    'jquery',
    'ko',
    'retailerLocatorDataProvider',
    'mage/url'
], function ($, ko, dataProvider, urlBuilder) {
    'use strict';

    var mixin = {
        currentRetailerVisible: ko.observable(false),
        retailerPanelVisible: ko.observable(false),
        additionalInfo: dataProvider.additionalInfo,
        allStorePage: urlBuilder.build('storelocator/all-stores'),
        moreInfos: '.current-retailer .more-info',
        phoneClick: '#retailer-phone',
        mailClick: '#retailer-mail',
        addThisUrl: window.addThisUrl,

        hideCurrentStore: function() {
            this.currentRetailerVisible(false);
        },

        showFullDetails: function() {
            this.retailerPanelVisible(true);
            $('.current-retailer .more-info').hide();
        },

        hidePanel: function() {
            this.retailerPanelVisible(false);
            $('.current-retailer .more-info').show();
        },

        initMapWidget: function(element) {
            this.$map = $(element);
            this.mapOptions = window.retailerLocatorConfig;
            this.mapOptions.markersData = this.retailers();

            this.$map.gmap(this.mapOptions);

            this._bindMapEvents();

            this._bindAnalyticsEvent();

            this.widgetInitialized(true);
        },

        _bindAnalyticsEvent: function() {
            let self = this;

            $(document).on('click', this.moreInfos, function() {
                $('html, body, .retailer-panel').animate({ scrollTop: 0 }, 'slow');
                window.dataLayer.push({
                    'event': 'gtm.ext.event',
                    'eventTmp': 'MoreInfo',
                    'eventCategory': 'MoreInfo',
                    'eventAction':$('.current-retailer .name').text(),
                    'eventLabel': document.referrer
                });
            });

            $(document).on('click', this.phoneClick, function() {
                window.dataLayer.push({
                    'event': 'gtm.ext.event',
                    'eventTmp': 'storeLocatorClickPhone',
                    'eventCategory': 'storeLocatorClickPhone',
                    'eventAction': $('.current-retailer .name').text(),
                    'eventLabel': urlBuilder.build('store/' + self.currentRetailer().url_key)
                });
            });

            $(document).on('click', this.mailClick, function() {
                window.dataLayer.push({
                    'event': 'gtm.ext.event',
                    'eventTmp': 'storeLocatorClickEmail',
                    'eventCategory': 'storeLocatorClickEmail',
                    'eventAction': $('.current-retailer .name').text(),
                    'eventLabel': urlBuilder.build('store/' + self.currentRetailer().url_key)
                });
            });
        },

        _bindMapEvents: function() {
            this.$map.on('click-marker', function(event, retailerData) { // Plug on map event
                this.currentRetailer(retailerData);
                this.currentRetailerVisible(true);
            }.bind(this));
        }
    };

    return function (retailerLocator) {
        return retailerLocator.extend(mixin);
    };
});
