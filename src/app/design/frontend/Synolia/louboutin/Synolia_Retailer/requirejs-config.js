var config = {
    map: {
        '*': {
            gmap: 'Synolia_Retailer/js/extend/synolia/gmap'
        }
    },

    config: {
        mixins: {
            'Synolia_Retailer/js/view/retailer-locator': {
                'Synolia_Retailer/js/view/retailer-locator-mixin': true
            }
        }
    }
};