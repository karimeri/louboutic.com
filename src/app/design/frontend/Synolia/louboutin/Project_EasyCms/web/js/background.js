define([
    'jquery',
    'underscore'
], function ($, _) {
    'use strict';

    $.widget('synolia.background', {

        options: {
            images: '.image-background, .image-first, .image-second, .image-article'
        },

        _init: function () {
            this._bindEvents();
            this._srcImages();
        },

        _bindEvents: function () {
            _.bindAll(this, '_srcImages');
            $(window).on('scroll', _.throttle(this._srcImages, 100));
        },

        _srcImages: function () {
            var source = '';

            $(this.options.images).each(function () {

                if ($(this).css('opacity') !== '0') {
                    var img = $(this).find('img'),
                        dataAmsrc = img.attr('data-amsrc');

                    source = 'url('+img.attr('src')+')';
                    if (typeof dataAmsrc !== typeof undefined && dataAmsrc !== false) {
                        img.removeAttr('src');
                        source = 'url('+dataAmsrc+')';
                    }

                    $(this).css({
                        'background-image': source
                    });
                }
            });
        }
    });

    return $.synolia.background;
});
