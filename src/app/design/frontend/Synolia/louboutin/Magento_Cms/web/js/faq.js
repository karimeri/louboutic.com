
require([
'jquery',
'jquery/ui'
], function ($) {

    // Icons Select Section
    var $sectionLink = $('.section-link'),
        $contentFaq = $('#content-faq'),
        $section_li = $('.faq-section > ul > li'),
        $section = $('.faq-section');


    //Select Section FAQ
    $sectionLink.on('click', function() {
        $sectionLink.removeClass('active-section');
        $section.removeClass('active-section');
        $(this).addClass('active-section');
        var $selSection = $(this).attr('data-section-id');
        $('#' + $selSection + '').addClass('active-section');
        $('#' + $selSection + '').children('ul').children('li').removeClass('active');
        $('#' + $selSection + '').children('ul').children('li').first().addClass('active');
        $('#' + $selSection + '').children('div').children('div').removeClass('active');
        $('#' + $selSection + '').children('div').children('div').first().addClass('active');
    });

    $section_li.on('click', function(e) {
        e.preventDefault();
        $section_li.removeAttr("class");
        $(this).addClass('active');
        $('.tab-pane').removeClass('active');
        $($(this).children('a').attr('href')).addClass('active');
    });


    //Cross on chapter
    var $cross = $("#content-faq article h4");
    $cross.click(function(e) {
        e.preventDefault();
        $cross.not(this).addClass('add-before').removeClass('open');
        $(this).toggleClass('add-before open');
    });

    // Scroll to Tabs
    $("li.section-link").click(function() {
        var currentWidth = $(window).width();
        var xScroll = currentWidth < 767 ? 0 : 70;
        $('html, body').animate({
            scrollTop: $contentFaq.offset().top - xScroll
        }, 1000);
    });

    //Collapse others
    $('.btn').on('click', function() {
        $(this).parent().next().collapse('hide');
        $('.collapse').not(this).removeClass('in');
    });



        // COLLAPSE PUBLIC CLASS DEFINITION
        // ================================

        var Collapse = function (element, options) {
            this.$element      = $(element)
            this.options       = $.extend({}, Collapse.DEFAULTS, options)
            this.transitioning = null

            if (this.options.parent) this.$parent = $(this.options.parent)
            if (this.options.toggle) this.toggle()
        }

        Collapse.DEFAULTS = {
            toggle: true
        }

        Collapse.prototype.dimension = function () {
            var hasWidth = this.$element.hasClass('width')
            return hasWidth ? 'width' : 'height'
        }

        Collapse.prototype.show = function () {
            if (this.transitioning || this.$element.hasClass('in')) return

            var startEvent = $.Event('show.bs.collapse')
            this.$element.trigger(startEvent)
            if (startEvent.isDefaultPrevented()) return

            var actives = this.$parent && this.$parent.find('> .panel > .in')

if (actives && actives.length) {
    var hasData = actives.data('bs.collapse')
    if (hasData && hasData.transitioning) return
    actives.collapse('hide')
    hasData || actives.data('bs.collapse', null)
}

var dimension = this.dimension()

this.$element
.removeClass('collapse')
.addClass('collapsing')
[dimension](0)

this.transitioning = 1

var complete = function () {
    this.$element
        .removeClass('collapsing')
        .addClass('collapse in')
        [dimension]('auto')
    this.transitioning = 0
    this.$element.trigger('shown.bs.collapse')
}

if (!$.support.transition) return complete.call(this)

var scrollSize = $.camelCase(['scroll', dimension].join('-'))

this.$element
.one($.support.transition.end, $.proxy(complete, this))
.emulateTransitionEnd(350)
[dimension](this.$element[0][scrollSize])
}

Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element
    [dimension](this.$element[dimension]())
    [0].offsetHeight

    this.$element
    .addClass('collapsing')
    .removeClass('collapse')
    .removeClass('in')

    this.transitioning = 1

    var complete = function () {
    this.transitioning = 0
    this.$element
    .trigger('hidden.bs.collapse')
    .removeClass('collapsing')
    .addClass('collapse')
}

    if (!$.support.transition) return complete.call(this)

    this.$element
    [dimension](0)
    .one($.support.transition.end, $.proxy(complete, this))
    .emulateTransitionEnd(350)
}

Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
}


// COLLAPSE PLUGIN DEFINITION
// ==========================

var old = $.fn.collapse

$.fn.collapse = function (option) {
    return this.each(function () {
    var $this   = $(this)
    var data    = $this.data('bs.collapse')
    var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

    if (!data && options.toggle && option == 'show') option = !option
    if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
    if (typeof option == 'string') data[option]()
})
}

$.fn.collapse.Constructor = Collapse


// COLLAPSE NO CONFLICT
// ====================

$.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
}

function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
    'WebkitTransition' : 'webkitTransitionEnd',
    'MozTransition'    : 'transitionend',
    'OTransition'      : 'oTransitionEnd otransitionend',
    'transition'       : 'transitionend'
}

    for (var name in transEndEventNames) {
    if (el.style[name] !== undefined) {
    return { end: transEndEventNames[name] }
}
}

    return false // explicit for ie8 (  ._.)
}

// http://blog.alexmaccaw.com/css-transitions
$.fn.emulateTransitionEnd = function (duration) {
    var called = false, $el = this
    $(this).one($.support.transition.end, function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
}

$(function () {
    $.support.transition = transitionEnd()
})


// COLLAPSE DATA-API
// =================

$(document).on('click.bs.collapse.data-api', '[data-toggle=collapse]', function (e) {
    var $this   = $(this), href
    var target  = $this.attr('data-target')
    || e.preventDefault()
    || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
    var $target = $(target)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()
    var parent  = $this.attr('data-parent')
    var $parent = parent && $(parent)

    if (!data || !data.transitioning) {
    if ($parent) $parent.find('[data-toggle=collapse][data-parent="' + parent + '"]').not($this).addClass('collapsed')
    $this[$target.hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
}

    $target.collapse(option)
})
});

