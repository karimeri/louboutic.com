<?php
return [
    'session' => [
        'save' => 'redis',
        'redis' => [
            'host' => '!REDIS_SESSION_ENDPOINT!',
            'port' => '6379',
            'password' => '!REDIS_SESSION_PASSWORD!',
            'database' => '2',
            'timeout' => '2.5',
            'persistent_identifier' => '',
            'compression_threshold' => '2048',
            'compression_library' => 'gzip',
            'log_level' => '3',
            'max_concurrency' => '6',
            'break_after_frontend' => '5',
            'break_after_adminhtml' => '30',
            'first_lifetime' => '600',
            'bot_first_lifetime' => '60',
            'bot_lifetime' => '7200',
            'disable_locking' => 1,
            'min_lifetime' => '60',
            'max_lifetime' => '2592000'
        ]
    ],
    'backend' => [
        'frontName' => 'synadmin'
    ],
    'db' => [
        'connection' => [
            'default' => [
                'host' => '!MYSQL_ENDPOINT!',
                'dbname' => '!MYSQL_DATABASE!',
                'username' => '!MYSQL_USER!',
                'password' => '!MYSQL_PASSWORD!',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1'
            ],
            'indexer' => [
                'host' => '!MYSQL_ENDPOINT!',
                'dbname' => '!MYSQL_DATABASE!',
                'username' => '!MYSQL_USER!',
                'password' => '!MYSQL_PASSWORD!',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1'
            ]
        ],/*
        'slave_connection' => [
            'default' => [
                'host' => '!MYSQL_ENDPOINT_SLAVE!',
                'dbname' => '!MYSQL_DATABASE_SLAVE!',
                'username' => '!MYSQL_USER_SLAVE!',
                'password' => '!MYSQL_PASSWORD_SLAVE!',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1'
            ],
            'indexer' => [
                'host' => '!MYSQL_ENDPOINT_SLAVE!',
                'dbname' => '!MYSQL_DATABASE_SLAVE!',
                'username' => '!MYSQL_USER_SLAVE!',
                'password' => '!MYSQL_PASSWORD_SLAVE!',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;',
                'active' => '1'
            ]
        ],*/
        'table_prefix' => ''
    ],
    'crypt' => [
        'key' => '01c18a108819043cbcf98044ca620e39'
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => '!MAGENTO_RUN_MODE!',
    'cache' => [
        'frontend' => [
            'default' => [
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => [
                    'server' => '!REDIS_APP_ENDPOINT!',
                    'database' => 0,
                    'port' => '6379',
            		'password' => '!REDIS_APP_PASSWORD!',
                ]
            ],
            'page_cache' => [
                'backend' => 'Cm_Cache_Backend_Redis',
                'backend_options' => [
                    'server' => '!REDIS_APP_ENDPOINT!',
                    'database' => 1,
                    'port' => '6379',
            		'password' => '!REDIS_APP_PASSWORD!',
                ]
            ]
        ]
    ],
    'install' => [
        'date' => 'Thu, 21 Dec 2017 10:03:29 +0000'
    ],
];
