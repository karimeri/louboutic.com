<?php
/*return [
    'scopes' => [
        'websites' => [
            'admin' => [
                'website_id' => '0',
                'code' => 'admin',
                'name' => 'Admin',
                'sort_order' => '0',
                'default_group_id' => '0',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'fr' => [
                'website_id' => '6',
                'code' => 'fr',
                'name' => 'France',
                'sort_order' => '0',
                'default_group_id' => '6',
                'is_default' => '1',
                'restrict_sale' => '0'
            ],
            'uk' => [
                'website_id' => '9',
                'code' => 'uk',
                'name' => 'United Kingdom',
                'sort_order' => '0',
                'default_group_id' => '9',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'it' => [
                'website_id' => '12',
                'code' => 'it',
                'name' => 'Italy',
                'sort_order' => '0',
                'default_group_id' => '12',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'de' => [
                'website_id' => '15',
                'code' => 'de',
                'name' => 'Germany',
                'sort_order' => '0',
                'default_group_id' => '15',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'es' => [
                'website_id' => '18',
                'code' => 'es',
                'name' => 'Spain',
                'sort_order' => '0',
                'default_group_id' => '18',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'ch' => [
                'website_id' => '21',
                'code' => 'ch',
                'name' => 'Switzerland',
                'sort_order' => '0',
                'default_group_id' => '21',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'nl' => [
                'website_id' => '24',
                'code' => 'nl',
                'name' => 'Netherlands',
                'sort_order' => '0',
                'default_group_id' => '24',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'lu' => [
                'website_id' => '27',
                'code' => 'lu',
                'name' => 'Luxembourg',
                'sort_order' => '0',
                'default_group_id' => '27',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'be' => [
                'website_id' => '30',
                'code' => 'be',
                'name' => 'Belgium',
                'sort_order' => '0',
                'default_group_id' => '30',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'at' => [
                'website_id' => '33',
                'code' => 'at',
                'name' => 'Austria',
                'sort_order' => '0',
                'default_group_id' => '33',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'ie' => [
                'website_id' => '36',
                'code' => 'ie',
                'name' => 'Ireland',
                'sort_order' => '0',
                'default_group_id' => '36',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'pt' => [
                'website_id' => '39',
                'code' => 'pt',
                'name' => 'Portugal',
                'sort_order' => '0',
                'default_group_id' => '39',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'mc' => [
                'website_id' => '42',
                'code' => 'mc',
                'name' => 'Monaco',
                'sort_order' => '0',
                'default_group_id' => '42',
                'is_default' => '0',
                'restrict_sale' => '0'
            ],
            'gr' => [
                'website_id' => '45',
                'code' => 'gr',
                'name' => 'Greece',
                'sort_order' => '0',
                'default_group_id' => '45',
                'is_default' => '0',
                'restrict_sale' => '0'
            ]
        ],
        'groups' => [
            0 => [
                'group_id' => '0',
                'website_id' => '0',
                'code' => 'default',
                'name' => 'Default',
                'root_category_id' => '0',
                'default_store_id' => '0'
            ],
            6 => [
                'group_id' => '6',
                'website_id' => '6',
                'code' => 'fr',
                'name' => 'France Group',
                'root_category_id' => '2',
                'default_store_id' => '6'
            ],
            9 => [
                'group_id' => '9',
                'website_id' => '9',
                'code' => 'uk',
                'name' => 'United Kingdom Group',
                'root_category_id' => '2',
                'default_store_id' => '9'
            ],
            12 => [
                'group_id' => '12',
                'website_id' => '12',
                'code' => 'it',
                'name' => 'Italy Group',
                'root_category_id' => '2',
                'default_store_id' => '12'
            ],
            15 => [
                'group_id' => '15',
                'website_id' => '15',
                'code' => 'de',
                'name' => 'Germany Group',
                'root_category_id' => '2',
                'default_store_id' => '15'
            ],
            18 => [
                'group_id' => '18',
                'website_id' => '18',
                'code' => 'es',
                'name' => 'Spain Group',
                'root_category_id' => '2',
                'default_store_id' => '18'
            ],
            21 => [
                'group_id' => '21',
                'website_id' => '21',
                'code' => 'ch',
                'name' => 'Switzerland Group',
                'root_category_id' => '2',
                'default_store_id' => '21'
            ],
            24 => [
                'group_id' => '24',
                'website_id' => '24',
                'code' => 'nl',
                'name' => 'Netherlands Group',
                'root_category_id' => '2',
                'default_store_id' => '27'
            ],
            27 => [
                'group_id' => '27',
                'website_id' => '27',
                'code' => 'lu',
                'name' => 'Luxembourg Group',
                'root_category_id' => '2',
                'default_store_id' => '30'
            ],
            30 => [
                'group_id' => '30',
                'website_id' => '30',
                'code' => 'be',
                'name' => 'Belgium Group',
                'root_category_id' => '2',
                'default_store_id' => '36'
            ],
            33 => [
                'group_id' => '33',
                'website_id' => '33',
                'code' => 'at',
                'name' => 'Austria Group',
                'root_category_id' => '2',
                'default_store_id' => '42'
            ],
            36 => [
                'group_id' => '36',
                'website_id' => '36',
                'code' => 'ie',
                'name' => 'Ireland Group',
                'root_category_id' => '2',
                'default_store_id' => '45'
            ],
            39 => [
                'group_id' => '39',
                'website_id' => '39',
                'code' => 'pt',
                'name' => 'Portugal Group',
                'root_category_id' => '2',
                'default_store_id' => '48'
            ],
            42 => [
                'group_id' => '42',
                'website_id' => '42',
                'code' => 'mc',
                'name' => 'Monaco Group',
                'root_category_id' => '2',
                'default_store_id' => '51'
            ],
            45 => [
                'group_id' => '45',
                'website_id' => '45',
                'code' => 'gr',
                'name' => 'Greece Group',
                'root_category_id' => '2',
                'default_store_id' => '57'
            ]
        ],
        'stores' => [
            'admin' => [
                'store_id' => '0',
                'code' => 'admin',
                'website_id' => '0',
                'group_id' => '0',
                'name' => 'Admin',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'fr_fr' => [
                'store_id' => '6',
                'code' => 'fr_fr',
                'website_id' => '6',
                'group_id' => '6',
                'name' => 'Fran�ais',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'uk_en' => [
                'store_id' => '9',
                'code' => 'uk_en',
                'website_id' => '9',
                'group_id' => '9',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'it_en' => [
                'store_id' => '12',
                'code' => 'it_en',
                'website_id' => '12',
                'group_id' => '12',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'de_en' => [
                'store_id' => '15',
                'code' => 'de_en',
                'website_id' => '15',
                'group_id' => '15',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'es_en' => [
                'store_id' => '18',
                'code' => 'es_en',
                'website_id' => '18',
                'group_id' => '18',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'ch_en' => [
                'store_id' => '21',
                'code' => 'ch_en',
                'website_id' => '21',
                'group_id' => '21',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'ch_fr' => [
                'store_id' => '24',
                'code' => 'ch_fr',
                'website_id' => '21',
                'group_id' => '21',
                'name' => 'French',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'nl_en' => [
                'store_id' => '27',
                'code' => 'nl_en',
                'website_id' => '24',
                'group_id' => '24',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'lu_en' => [
                'store_id' => '30',
                'code' => 'lu_en',
                'website_id' => '27',
                'group_id' => '27',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'lu_fr' => [
                'store_id' => '33',
                'code' => 'lu_fr',
                'website_id' => '27',
                'group_id' => '27',
                'name' => 'French',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'be_en' => [
                'store_id' => '36',
                'code' => 'be_en',
                'website_id' => '30',
                'group_id' => '30',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'be_fr' => [
                'store_id' => '39',
                'code' => 'be_fr',
                'website_id' => '30',
                'group_id' => '30',
                'name' => 'French',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'at_en' => [
                'store_id' => '42',
                'code' => 'at_en',
                'website_id' => '33',
                'group_id' => '33',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'ie_en' => [
                'store_id' => '45',
                'code' => 'ie_en',
                'website_id' => '36',
                'group_id' => '36',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'pt_en' => [
                'store_id' => '48',
                'code' => 'pt_en',
                'website_id' => '39',
                'group_id' => '39',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'mc_en' => [
                'store_id' => '51',
                'code' => 'mc_en',
                'website_id' => '42',
                'group_id' => '42',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'mc_fr' => [
                'store_id' => '54',
                'code' => 'mc_fr',
                'website_id' => '42',
                'group_id' => '42',
                'name' => 'French',
                'sort_order' => '0',
                'is_active' => '1'
            ],
            'gr_en' => [
                'store_id' => '57',
                'code' => 'gr_en',
                'website_id' => '45',
                'group_id' => '45',
                'name' => 'English',
                'sort_order' => '0',
                'is_active' => '1'
            ]
        ]
    ],
    'system' => [
        'stores' => [
            'fr_fr' => [
                'general' => [
                    'locale' => [
                        'code' => 'fr_FR'
                    ]
                ]
            ],
            'uk_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'it_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'de_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'es_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'ch_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'ch_fr' => [
                'general' => [
                    'locale' => [
                        'code' => 'fr_FR'
                    ]
                ]
            ],
            'nl_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'lu_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'lu_fr' => [
                'general' => [
                    'locale' => [
                        'code' => 'fr_FR'
                    ]
                ]
            ],
            'be_fr' => [
                'general' => [
                    'locale' => [
                        'code' => 'fr_FR'
                    ]
                ]
            ],
            'be_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'at_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'ie_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'pt_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'mc_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ],
            'mc_fr' => [
                'general' => [
                    'locale' => [
                        'code' => 'fr_FR'
                    ]
                ]
            ],
            'gr_en' => [
                'general' => [
                    'locale' => [
                        'code' => 'en_GB'
                    ]
                ]
            ]
        ],
        'default' => [
            'dev' => [
                'js' => [
                    'minify_files' => '1'
                ],
                'css' => [
                    'minify_files' => '1'
                ]
            ],
            'persistent' => [
                'options' => [
                    'enabled' => '1'
                ]
            ]
        ]
    ],
    'modules' => [
        'Magento_AdminAnalytics' => 0,
        'Magento_Store' => 1,
        'Magento_AdminGwsConfigurableProduct' => 1,
        'Magento_AdminGwsStaging' => 1,
        'Magento_Directory' => 1,
        'Magento_AdobeIms' => 0,
        'Magento_AdobeImsApi' => 0,
        'Magento_AdobeStockAdminUi' => 0,
        'Magento_MediaGallery' => 1,
        'Magento_AdobeStockAssetApi' => 0,
        'Magento_AdobeStockClient' => 0,
        'Magento_AdobeStockClientApi' => 0,
        'Magento_AdobeStockImage' => 0,
        'Magento_AdobeStockImageAdminUi' => 0,
        'Magento_AdobeStockImageApi' => 0,
        'Magento_Theme' => 1,
        'Magento_Eav' => 1,
        'Magento_AdvancedPricingImportExport' => 1,
        'Magento_Rule' => 1,
        'Magento_Customer' => 1,
        'Magento_Backend' => 1,
        'Magento_Amqp' => 1,
        'Magento_AmqpStore' => 1,
        'Magento_Config' => 1,
        'Magento_User' => 1,
        'Magento_Authorization' => 1,
        'Magento_AdminNotification' => 1,
        'Magento_Indexer' => 1,
        'Magento_Variable' => 1,
        'Magento_AuthorizenetGraphQl' => 0,
        'Magento_Cms' => 1,
        'Magento_Backup' => 1,
        'Magento_Catalog' => 1,
        'Magento_Quote' => 1,
        'Magento_SalesSequence' => 1,
        'Magento_Payment' => 1,
        'Magento_CatalogInventory' => 1,
        'Magento_BraintreeGraphQl' => 0,
        'Magento_Bundle' => 1,
        'Magento_GraphQl' => 1,
        'Magento_BundleImportExport' => 1,
        'Magento_BundleImportExportStaging' => 1,
        'Magento_Sales' => 1,
        'Magento_CacheInvalidate' => 1,
        'Magento_MediaStorage' => 1,
        'Magento_Checkout' => 1,
        'Magento_AdvancedCatalog' => 1,
        'Magento_Security' => 1,
        'Magento_CmsGraphQl' => 1,
        'Magento_EavGraphQl' => 1,
        'Magento_Search' => 1,
        'Magento_StoreGraphQl' => 1,
        'Magento_CatalogImportExport' => 1,
        'Magento_CatalogImportExportStaging' => 1,
        'Magento_Authorizenet' => 0,
        'Magento_CatalogInventoryGraphQl' => 1,
        'Magento_CatalogRule' => 1,
        'Magento_CatalogPageBuilderAnalytics' => 1,
        'Magento_CatalogPageBuilderAnalyticsStaging' => 1,
        'Magento_CatalogUrlRewrite' => 1,
        'Magento_Widget' => 1,
        'Magento_Msrp' => 1,
        'Magento_CatalogRuleGraphQl' => 1,
        'Magento_SalesRule' => 1,
        'Magento_CustomerCustomAttributes' => 1,
        'Magento_Downloadable' => 1,
        'Magento_Ui' => 1,
        'Magento_GiftCard' => 1,
        'Magento_Captcha' => 1,
        'Magento_CatalogGraphQl' => 1,
        'Magento_CatalogEvent' => 1,
        'Magento_CustomerSegment' => 1,
        'Magento_CardinalCommerce' => 0,
        'Magento_CheckoutAddressSearch' => 1,
        'Magento_Wishlist' => 1,
        'Magento_CheckoutAgreements' => 1,
        'Magento_CheckoutAgreementsGraphQl' => 1,
        'Magento_Staging' => 1,
        'Magento_AdvancedCheckout' => 1,
        'Magento_CatalogCmsGraphQl' => 1,
        'Magento_CmsPageBuilderAnalytics' => 1,
        'Magento_CmsPageBuilderAnalyticsStaging' => 1,
        'Magento_VersionsCms' => 1,
        'Magento_CmsUrlRewrite' => 1,
        'Magento_CmsUrlRewriteGraphQl' => 1,
        'Magento_ComposerRootUpdatePlugin' => 0,
        'Magento_Integration' => 1,
        'Magento_ConfigurableProduct' => 1,
        'Magento_CatalogRuleConfigurable' => 1,
        'Magento_QuoteGraphQl' => 1,
        'Magento_ConfigurableProductSales' => 1,
        'Magento_PageCache' => 1,
        'Magento_Contact' => 1,
        'Magento_Cookie' => 1,
        'Magento_Cron' => 1,
        'Magento_Csp' => 0,
        'Magento_CurrencySymbol' => 1,
        'Magento_CustomAttributeManagement' => 1,
        'Magento_AdvancedRule' => 1,
        'Magento_Analytics' => 1,
        'Magento_CustomerBalance' => 1,
        'Magento_CustomerBalanceGraphQl' => 1,
        'Magento_CatalogWidget' => 1,
        'Magento_DownloadableGraphQl' => 1,
        'Magento_CustomerFinance' => 1,
        'Magento_CustomerGraphQl' => 1,
        'Magento_CustomerImportExport' => 1,
        'Magento_ProductAlert' => 1,
        'Magento_Vault' => 1,
        'Magento_Deploy' => 1,
        'Magento_Developer' => 1,
        'Magento_Dhl' => 0,
        'Magento_AdvancedSearch' => 1,
        'Magento_DirectoryGraphQl' => 1,
        'Magento_CatalogSearch' => 1,
        'Magento_CustomerDownloadableGraphQl' => 1,
        'Magento_ImportExport' => 1,
        'Magento_TargetRule' => 1,
        'Magento_AuthorizenetAcceptjs' => 0,
        'Magento_CatalogCustomerGraphQl' => 1,
        'Magento_Elasticsearch' => 1,
        'Magento_Elasticsearch6' => 1,
        'Magento_Elasticsearch7' => 1,
        'Magento_WebsiteRestriction' => 1,
        'Magento_Email' => 1,
        'Magento_EncryptionKey' => 1,
        'Magento_Enterprise' => 1,
        'Magento_Eway' => 1,
        'Magento_Fedex' => 0,
        'Magento_Tax' => 1,
        'Magento_GiftCardAccount' => 1,
        'Magento_GiftCardAccountGraphQl' => 1,
        'Magento_GiftCardGraphQl' => 1,
        'Magento_GiftCardImportExport' => 1,
        'Magento_VisualMerchandiser' => 1,
        'Magento_GiftMessage' => 1,
        'Magento_GiftMessageStaging' => 1,
        'Magento_UrlRewrite' => 1,
        'Magento_GiftWrapping' => 1,
        'Magento_GiftWrappingStaging' => 1,
        'Magento_GoogleAdwords' => 1,
        'Magento_GoogleAnalytics' => 1,
        'Magento_GoogleOptimizer' => 1,
        'Magento_GoogleOptimizerStaging' => 1,
        'Magento_GoogleShoppingAds' => 0,
        'Magento_Banner' => 1,
        'Magento_BundleGraphQl' => 1,
        'Magento_GraphQlCache' => 1,
        'Magento_GroupedProduct' => 1,
        'Magento_GroupedImportExport' => 1,
        'Magento_GroupedCatalogInventory' => 1,
        'Magento_GroupedProductGraphQl' => 1,
        'Magento_GroupedProductStaging' => 1,
        'Magento_DownloadableImportExport' => 1,
        'Magento_Paypal' => 1,
        'Magento_InstantPurchase' => 1,
        'Magento_CatalogAnalytics' => 1,
        'Magento_Inventory' => 0,
        'Magento_InventoryAdminUi' => 0,
        'Magento_InventoryAdvancedCheckout' => 0,
        'Magento_InventoryApi' => 0,
        'Magento_InventoryBundleProduct' => 0,
        'Magento_InventoryBundleProductAdminUi' => 0,
        'Magento_InventoryCatalog' => 0,
        'Magento_InventorySales' => 0,
        'Magento_InventoryCatalogAdminUi' => 0,
        'Magento_InventoryCatalogApi' => 0,
        'Magento_InventoryCatalogSearch' => 0,
        'Magento_InventoryConfigurableProduct' => 0,
        'Magento_InventoryConfigurableProductAdminUi' => 0,
        'Magento_InventoryConfigurableProductIndexer' => 0,
        'Magento_InventoryConfiguration' => 0,
        'Magento_InventoryConfigurationApi' => 0,
        'Magento_InventoryDistanceBasedSourceSelection' => 0,
        'Magento_InventoryDistanceBasedSourceSelectionAdminUi' => 0,
        'Magento_InventoryDistanceBasedSourceSelectionApi' => 0,
        'Magento_InventoryElasticsearch' => 0,
        'Magento_InventoryExportStockApi' => 0,
        'Magento_InventoryIndexer' => 0,
        'Magento_InventorySalesApi' => 0,
        'Magento_InventoryGroupedProduct' => 0,
        'Magento_InventoryGroupedProductAdminUi' => 0,
        'Magento_InventoryGroupedProductIndexer' => 0,
        'Magento_InventoryImportExport' => 0,
        'Magento_InventoryCache' => 0,
        'Magento_InventoryLowQuantityNotification' => 0,
        'Magento_Reports' => 1,
        'Magento_InventoryLowQuantityNotificationApi' => 0,
        'Magento_InventoryMultiDimensionalIndexerApi' => 0,
        'Magento_InventoryProductAlert' => 0,
        'Magento_InventoryRequisitionList' => 0,
        'Magento_InventoryReservations' => 0,
        'Magento_InventoryReservationCli' => 0,
        'Magento_InventoryReservationsApi' => 0,
        'Magento_InventoryExportStock' => 0,
        'Magento_InventorySalesAdminUi' => 0,
        'Magento_InventoryGraphQl' => 0,
        'Magento_InventorySalesFrontendUi' => 0,
        'Magento_InventorySetupFixtureGenerator' => 0,
        'Magento_InventoryShipping' => 0,
        'Magento_Shipping' => 1,
        'Magento_InventorySourceDeductionApi' => 0,
        'Magento_InventorySourceSelection' => 0,
        'Magento_InventorySourceSelectionApi' => 0,
        'Magento_Invitation' => 1,
        'Magento_LayeredNavigation' => 1,
        'Magento_LayeredNavigationStaging' => 1,
        'Magento_Logging' => 1,
        'Magento_Marketplace' => 0,
        'Magento_AdobeStockAsset' => 0,
        'Magento_MediaGalleryApi' => 1,
        'Magento_CatalogPermissions' => 1,
        'Magento_MessageQueue' => 1,
        'Magento_ConfigurableImportExport' => 1,
        'Magento_MsrpConfigurableProduct' => 1,
        'Magento_MsrpGroupedProduct' => 1,
        'Magento_MsrpStaging' => 1,
        'Magento_MultipleWishlist' => 1,
        'Magento_Multishipping' => 1,
        'Magento_MysqlMq' => 1,
        'Magento_NewRelicReporting' => 0,
        'Magento_Newsletter' => 1,
        'Magento_OfflinePayments' => 1,
        'Magento_OfflineShipping' => 1,
        'Magento_BannerCustomerSegment' => 1,
        'Magento_PageBuilder' => 1,
        'Magento_Weee' => 0,
        'Magento_AdminGws' => 1,
        'Magento_PaymentStaging' => 1,
        'Magento_Braintree' => 0,
        'Magento_PaypalCaptcha' => 0,
        'Magento_PaypalGraphQl' => 0,
        'MSP_ReCaptcha' => 1,
        'Magento_Persistent' => 1,
        'Magento_PersistentHistory' => 1,
        'Magento_PricePermissions' => 1,
        'Magento_GiftRegistry' => 1,
        'Magento_ProductVideo' => 1,
        'Magento_CatalogStaging' => 1,
        'Magento_PromotionPermissions' => 1,
        'Magento_BannerPageBuilderAnalytics' => 1,
        'Magento_QuoteAnalytics' => 1,
        'Magento_ConfigurableProductGraphQl' => 1,
        'Magento_RelatedProductGraphQl' => 1,
        'Magento_ReleaseNotification' => 1,
        'Magento_Reminder' => 1,
        'Magento_InventoryLowQuantityNotificationAdminUi' => 0,
        'Magento_RequireJs' => 1,
        'Magento_ResourceConnections' => 1,
        'Magento_Review' => 1,
        'Magento_ReviewAnalytics' => 1,
        'Magento_ReviewStaging' => 1,
        'Magento_Reward' => 1,
        'Magento_RewardGraphQl' => 1,
        'Magento_AdvancedSalesRule' => 1,
        'Magento_Rma' => 1,
        'Magento_RmaGraphQl' => 1,
        'Magento_RmaStaging' => 1,
        'Magento_Robots' => 1,
        'Magento_Rss' => 1,
        'Magento_SalesRuleStaging' => 1,
        'Magento_BannerPageBuilder' => 1,
        'Magento_SalesAnalytics' => 1,
        'Magento_Signifyd' => 0,
        'Magento_SalesGraphQl' => 1,
        'Magento_SalesInventory' => 1,
        'Magento_CatalogRuleStaging' => 1,
        'Magento_RewardStaging' => 1,
        'Magento_GoogleTagManager' => 1,
        'Magento_SampleData' => 0,
        'Magento_ScalableCheckout' => 0,
        'Magento_ScalableInventory' => 0,
        'Magento_ScalableOms' => 1,
        'Magento_ScheduledImportExport' => 1,
        'Magento_DownloadableStaging' => 1,
        'Magento_SearchStaging' => 1,
        'Magento_CustomerAnalytics' => 1,
        'Magento_SendFriend' => 1,
        'Magento_SendFriendGraphQl' => 1,
        'Magento_InventoryShippingAdminUi' => 0,
        'Magento_SalesArchive' => 1,
        'Magento_Sitemap' => 1,
        'Magento_StagingGraphQl' => 1,
        'Magento_CatalogStagingGraphQl' => 1,
        'Magento_StagingPageBuilder' => 1,
        'Magento_CheckoutAddressSearchGiftRegistry' => 1,
        'Magento_UrlRewriteGraphQl' => 1,
        'Magento_Support' => 1,
        'Magento_Webapi' => 1,
        'Magento_SwaggerWebapi' => 0,
        'Magento_SwaggerWebapiAsync' => 0,
        'Magento_Swatches' => 1,
        'Magento_SwatchesGraphQl' => 1,
        'Magento_SwatchesLayeredNavigation' => 1,
        'Magento_CatalogInventoryStaging' => 1,
        'Magento_GiftCardStaging' => 1,
        'Magento_TaxGraphQl' => 1,
        'Magento_TaxImportExport' => 1,
        'Magento_AuthorizenetCardinal' => 0,
        'Magento_ThemeGraphQl' => 1,
        'Magento_Tinymce3' => 1,
        'Magento_Tinymce3Banner' => 1,
        'Magento_Translation' => 1,
        'Magento_CheckoutStaging' => 1,
        'Magento_Ups' => 1,
        'Magento_CatalogUrlRewriteStaging' => 1,
        'Magento_CatalogUrlRewriteGraphQl' => 1,
        'Magento_AsynchronousOperations' => 1,
        'Magento_Usps' => 0,
        'Magento_ElasticsearchCatalogPermissions' => 1,
        'Magento_Cybersource' => 0,
        'Magento_VaultGraphQl' => 1,
        'Magento_Version' => 0,
        'Magento_CmsStaging' => 1,
        'Magento_VersionsCmsUrlRewrite' => 1,
        'Magento_ConfigurableProductStaging' => 1,
        'Magento_Swagger' => 0,
        'Magento_WebapiAsync' => 0,
        'Magento_WebapiSecurity' => 1,
        'Magento_BundleStaging' => 1,
        'Magento_CatalogStagingPageBuilder' => 1,
        'Magento_WeeeGraphQl' => 0,
        'Magento_WeeeStaging' => 0,
        'Magento_PageBuilderAnalytics' => 1,
        'Magento_ProductVideoStaging' => 1,
        'Magento_WishlistAnalytics' => 1,
        'Magento_WishlistGraphQl' => 1,
        'Magento_Worldpay' => 1,
        'Adyen_Payment' => 1,
        'Algolia_AlgoliaSearch' => 1,
        'Amasty_Base' => 1,
        'Amasty_CronScheduleList' => 1,
        'Amasty_CronScheduler' => 1,
        'Amasty_Geoip' => 1,
        'Amasty_GeoipRedirect' => 1,
        'Amasty_PageSpeedOptimizer' => 1,
        'Amasty_SecurityAuth' => 1,
        'MageHost_PerformanceDashboard' => 0,
        'Amazon_Core' => 1,
        'Amazon_Login' => 1,
        'Amazon_Payment' => 1,
        'Bss_CustomProductAttributeExport' => 0,
        'CyberSource_AccountUpdater' => 0,
        'CyberSource_Core' => 0,
        'CyberSource_ApplePay' => 0,
        'CyberSource_Atp' => 0,
        'CyberSource_BankTransfer' => 0,
        'CyberSource_SecureAcceptance' => 0,
        'CyberSource_ECheck' => 0,
        'CyberSource_KlarnaFinancial' => 0,
        'CyberSource_VisaCheckout' => 0,
        'CyberSource_Address' => 0,
        'CyberSource_Tax' => 0,
        'CyberSource_ThreeDSecure' => 0,
        'CyberSource_PayPal' => 0,
        'Dotdigitalgroup_Email' => 0,
        'Dotdigitalgroup_Chat' => 0,
        'Dotdigitalgroup_Enterprise' => 0,
        'Eu_Checkout' => 1,
        'Synolia_AdvancedMenu' => 1,
        'Fastly_Cdn' => 0,
        'Project_Core' => 1,
        'Jp_Address' => 1,
        'Jp_CashOnDelivery' => 1,
        'Jp_Core' => 1,
        'Jp_DeliveryDate' => 1,
        'Klarna_Core' => 0,
        'Klarna_Ordermanagement' => 0,
        'Klarna_Kp' => 0,
        'Louboutin_Cdn' => 1,
        'Mirasvit_Feed' => 1,
        'Louboutin_ImportMerchandising' => 1,
        'Louboutin_Newsletter' => 1,
        'Louboutin_Sales' => 1,
        'Louboutin_Security' => 1,
        'Louboutin_Varnish' => 1,
        'Louboutin_VisualMerchandiser' => 1,
        'Magento_PaypalReCaptcha' => 0,
        'MSP_TwoFactorAuth' => 1,
        'MageWorx_SeoAll' => 1,
        'MageWorx_Info' => 1,
        'MageWorx_HtmlSitemap' => 1,
        'MageWorx_SeoBase' => 1,
        'MageWorx_SeoBreadcrumbs' => 1,
        'MageWorx_SeoCategoryGrid' => 1,
        'MageWorx_SeoCrossLinks' => 1,
        'MageWorx_SeoExtended' => 1,
        'MageWorx_SeoMarkup' => 1,
        'MageWorx_SeoRedirects' => 1,
        'MageWorx_SeoReports' => 1,
        'MageWorx_SeoUrls' => 1,
        'MageWorx_SeoXTemplates' => 1,
        'MageWorx_XmlSitemap' => 1,
        'Mageplaza_Core' => 1,
        'Mirasvit_Core' => 1,
        'Louboutin_Feed' => 1,
        'Mirasvit_Report' => 1,
        'Owebia_SharedPhpConfig' => 1,
        'Owebia_AdvancedShipping' => 1,
        'Pimgento_Entities' => 1,
        'Pimgento_Import' => 1,
        'Pimgento_Family' => 1,
        'Pimgento_Attribute' => 1,
        'Pimgento_Category' => 1,
        'Pimgento_Log' => 1,
        'Pimgento_Option' => 1,
        'Pimgento_Product' => 1,
        'Pimgento_Upgrade' => 1,
        'Pimgento_Variant' => 1,
        'Pimgento_VariantFamily' => 1,
        'PluginCompany_LicenseManager' => 1,
        'PluginCompany_CmsRevisions' => 1,
        'Project_Addthis' => 1,
        'Project_Adyen' => 1,
        'Project_AlgoliaSearch' => 1,
        'Project_AmastyAcart' => 1,
        'Project_Erp' => 1,
        'Synolia_AdvancedStock' => 1,
        'Project_Backend' => 1,
        'Project_Beauty' => 1,
        'Project_BlueFoot' => 1,
        'Project_Catalog' => 1,
        'Project_CatalogSearch' => 0,
        'Project_CatalogWidget' => 1,
        'Project_CbSecurity' => 1,
        'Project_Checkout' => 1,
        'Project_Cms' => 1,
        'Project_ConfigurableProduct' => 1,
        'Synolia_StoreSwitcher' => 1,
        'Project_Cookie' => 1,
        'Eu_Core' => 1,
        'Project_Csp' => 1,
        'Synolia_CustomFilter' => 1,
        'Project_CustomProductExport' => 1,
        'Project_Customer' => 1,
        'Project_CyberSource' => 0,
        'Project_Developer' => 1,
        'Project_Directory' => 1,
        'Project_EasyCms' => 1,
        'Project_EmailSent' => 1,
        'Project_Avalara' => 1,
        'Project_ExchangeOffline' => 1,
        'Project_FindInStore' => 1,
        'Project_GiftMessage' => 1,
        'Project_GiftWrapping' => 1,
        'Project_MageWorxSeoMarkup' => 1,
        'Project_MigrateData' => 1,
        'Project_NewsletterPopup' => 1,
        'Project_Owebia' => 1,
        'Project_PageCache' => 1,
        'Project_Pdf' => 1,
        'Project_Permissions' => 1,
        'Project_Persistent' => 1,
        'Project_Pimgento' => 1,
        'Synolia_MultiStock' => 1,
        'Project_ProductAlert' => 1,
        'Project_ProductFile' => 1,
        'Project_ReCaptcha' => 1,
        'Synolia_RestrictSale' => 1,
        'Synolia_Retailer' => 1,
        'Project_Rma' => 1,
        'Synolia_OrderArchive' => 1,
        'Synolia_SalesCustomSequence' => 1,
        'Project_SalesForce' => 1,
        'Project_SampleData' => 1,
        'Project_Security' => 1,
        'Project_Share' => 1,
        'Project_StoreSwitcher' => 1,
        'Synolia_Standard' => 1,
        'Project_Tax' => 1,
        'Project_Ups' => 1,
        'Project_UrlRewrite' => 1,
        'Project_User' => 1,
        'Synolia_ImportExport' => 1,
        'WeltPixel_Backend' => 1,
        'Project_Retailer' => 1,
        'Project_Wishlist' => 1,
        'Project_Wms' => 1,
        'Rollback_Export' => 1,
        'Synolia_Admin' => 1,
        'Hk_Core' => 1,
        'Project_BackInStock' => 1,
        'Synolia_Tax' => 1,
        'Synolia_ClassLoader' => 1,
        'Synolia_Cron' => 1,
        'Project_CustomFilter' => 1,
        'Synolia_CustomMenu' => 1,
        'Synolia_DutyCalculator' => 1,
        'Synolia_EasyCms' => 1,
        'Synolia_GeoIp' => 1,
        'Project_VisualMerchandiser' => 1,
        'Synolia_Indexer' => 1,
        'Synolia_Logger' => 1,
        'Project_PreOrder' => 1,
        'Project_Sales' => 1,
        'Synolia_ReCaptcha' => 1,
        'Synolia_Redirects' => 1,
        'Synolia_Reset' => 1,
        'Project_RestrictSale' => 1,
        'WeltPixel_GoogleTagManager' => 1,
        'Project_SalesCustomSequence' => 1,
        'Synolia_Slider' => 1,
        'Project_SynoliaSlider' => 1,
        'Synolia_StandardDesign' => 1,
        'Project_Contact' => 1,
        'Synolia_Sync' => 1,
        'Synolia_Avalara' => 1,
        'Temando_ShippingRemover' => 1,
        'Us_Checkout' => 1,
        'Us_Core' => 1,
        'Us_LiveChat' => 1,
        'Us_MultiStock' => 1,
        'Us_ProhibitedProduct' => 1,
        'Veriteworks_Base' => 1,
        'Veriteworks_CookieFix' => 1,
        'Veriteworks_Veritrans' => 1,
        'Vertex_Tax' => 0,
        'Vertex_AddressValidation' => 0,
        'Project_WeltPixelBackend' => 1,
        'Project_WeltPixelGoogleTagManager' => 1,
        'Yotpo_Yotpo' => 0
    ]
];*/

