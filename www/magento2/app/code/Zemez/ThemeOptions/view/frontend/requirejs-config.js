var config = {
    map: {
        '*': {}
    },
    paths: {
        "productListingGallery":    "Zemez_ThemeOptions/js/product-listing-gallery",
        "switchImage":              "Zemez_ThemeOptions/js/switch-image",
        "stickUp":                  "Zemez_ThemeOptions/js/stickUp",
        "themeOptions":             "Zemez_ThemeOptions/js/theme-options"
    },
    shim: {
        "stickUp":  ["jquery"]
    },
};