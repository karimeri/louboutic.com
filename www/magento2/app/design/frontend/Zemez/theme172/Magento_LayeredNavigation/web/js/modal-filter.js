/**
 * Copyright © Zemez, All rights reserved.
 * See COPYING.txt for license details.
 */

require([
    'jquery',
    'matchMedia',
    'Magento_Ui/js/modal/modal'
], function($, mediaCheck) {
    'use strict';

    function modalAside() {
        mediaCheck({
            media: '(max-width: 1023px)',
            entry: function() {
                var filterBlock = $('#layered-filter-block'),
                    filterToggle = $('.filter-toggle');
                if (filterBlock.length) {
                    filterToggle.show();
                } else {
                    filterToggle.hide();
                }
                $(document).on('click', '.filter-toggle', function() {
                    $('.filter-toggle').toggleClass('active');
                    $('#layered-filter-block').toggleClass('active');
                });
                $(document).ajaxComplete(function() {
                    $('.filter-toggle').removeClass('active');
                    $('#layered-filter-block').removeClass('active');
                });
            },
            exit: function() {
                $('.filter-toggle').hide();
            }
        });
    }

    modalAside();
});
