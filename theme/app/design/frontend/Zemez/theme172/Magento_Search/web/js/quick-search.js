/**
 * Copyright © Zemez, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery'
], function ($) {
    'use strict';

    var searchMixin = {
        options: {
            isExpandable: true
        },
        _create: function() {
            this.responseList = {
                indexList: null,
                selected: null
            };
            this.autoComplete = $(this.options.destinationSelector);
            this.searchForm = $(this.options.formSelector);
            this.submitBtn = this.searchForm.find(this.options.submitBtn)[0];
            this.searchLabel = this.searchForm.find(this.options.searchLabel);
            this.isExpandable = this.options.isExpandable;
            _.bindAll(this, '_onKeyDown', '_onPropertyChange', '_onSubmit');
            this.submitBtn.disabled = true;
            this.element.attr('autocomplete', this.options.autocomplete);
            if (this.isExpandable) {
                this._expandableSearch();
            } else {
                mediaCheck({
                    media: '(max-width: 768px)',
                    entry: function() {
                        this._expandableSearch();
                        this.element.on('focus', this.setActiveState.bind(this, true));
                    }.bind(this)
                });
            }
            this.element.on('blur', $.proxy(function () {
                this.autoComplete.hide();
                this._updateAriaHasPopup(false);
            }, this));
            this.element.on('keydown', this._onKeyDown);
            // Prevent spamming the server with requests by waiting till the user has stopped typing for period of time
            this.element.on('input propertychange', _.debounce(this._onPropertyChange, this.options.suggestionDelay));
            this.searchForm.on('submit', $.proxy(function(e) {
                this._onSubmit(e);
                this._updateAriaHasPopup(false);
            }, this));
        },
        _expandableSearch: function() {
            this.searchLabel.on('click', function(e) {
                if (this.isActive()) {
                    e.preventDefault();
                    this._hideSearch();
                } else {
                    this.setActiveState(true);
                    setTimeout($.proxy(function () {
                        this.element.focus().select();
                    }, this), 250);
                }
            }.bind(this));
            $('body').on('click.outsideSearch', function(event) {
                if (this.isActive() && !$(event.target).closest('#search_mini_form').length && !this.element.val()) {
                    this._hideSearch();
                }
            }.bind(this));
        },
        _hideSearch: function() {
            this.setActiveState(false);
            this.element.val('');
        }
    };

    return function (targetWidget) {
        $.widget('mage.quickSearch', targetWidget, searchMixin);
        return $.mage.quickSearch;
    };
});
