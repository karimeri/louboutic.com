/**
 * Copyright © Zemez, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    config: {
        mixins: {
            'Magento_Search/js/form-mini': {
                'Magento_Search/js/quick-search': true
            }
        }
    }
};
