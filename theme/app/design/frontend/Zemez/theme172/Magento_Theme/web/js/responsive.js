/**
 * Copyright © Zemez, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'matchMedia',
    'mage/tabs',
    'domReady!',
    'dropdownDialog'
], function($, mediaCheck) {
    'use strict';
    mediaCheck({
        media: '(min-width: 768px)',
        /**
         * Switch to Desktop Version.
         */
        entry: function() {
            // Mobile customer links
            var headerCustomerLinks = $('.customer-links');
            if (headerCustomerLinks.hasClass('ui-dialog-content')) {
                headerCustomerLinks.dropdownDialog("destroy");
            }

            // Magento blank theme responsive.js
            var galleryElement;
            (function() {
                var productInfoMain = $('.product-info-main'),
                    productInfoAdditional = $('#product-info-additional');
                if (productInfoAdditional.length) {
                    productInfoAdditional.addClass('hidden');
                    productInfoMain.removeClass('responsive');
                }
            })();
            galleryElement = $('[data-role=media-gallery]');
            if (galleryElement.length && galleryElement.data('mageZoom')) {
                galleryElement.zoom('enable');
            }
            if (galleryElement.length && galleryElement.data('mageGallery')) {
                galleryElement.gallery('option', 'disableLinks', true);
                galleryElement.gallery('option', 'showNav', false);
                galleryElement.gallery('option', 'showThumbs', true);
            }

            // Footer
            var footerColTitle = $('.footer-col h4');
            footerColTitle.off('click');
            footerColTitle.next('.footer-col-content').removeAttr('style');
        },
        /**
         * Switch to Mobile Version.
         */
        exit: function() {
            // Mobile customer links
            $(".customer-links").dropdownDialog({
                appendTo: ".customer-wrap",
                triggerTarget: ".customer-toggle",
                closeOnMouseLeave: false,
                closeOnClickOutside: true,
                parentClass: "active",
                triggerClass: "active"
            });

            // Magento blank theme responsive.js
            var galleryElement;
            $('.action.toggle.checkout.progress').on('click.gotoCheckoutProgress', function() {
                var myWrapper = '#checkout-progress-wrapper';
                scrollTo(myWrapper + ' .title');
                $(myWrapper + ' .title').addClass('active');
                $(myWrapper + ' .content').show();
            });
            $('body').on('click.checkoutProgress', '#checkout-progress-wrapper .title', function() {
                $(this).toggleClass('active');
                $('#checkout-progress-wrapper .content').toggle();
            });
            galleryElement = $('[data-role=media-gallery]');
            setTimeout(function() {
                if (galleryElement.length && galleryElement.data('mageZoom')) {
                    galleryElement.zoom('disable');
                }
                if (galleryElement.length && galleryElement.data('mageGallery')) {
                    galleryElement.gallery('option', 'disableLinks', false);
                    galleryElement.gallery('option', 'showNav', true);
                    galleryElement.gallery('option', 'showThumbs', false);
                }
            }, 2000);

            // Footer
            $('.footer-col h4').click(function() {
                $(this).toggleClass('active');
                $(this).next('.footer-col-content').slideToggle();
            });
        }
    });
});
