/**
 * Copyright © Zemez, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'owlcarousel'
], function($) {
    "use strict";
    $.widget('Zemez.carouselInit', {
        options: {
            nav: true,
            navText: false,
            dots: false,
            autoplay: false,
            loop: false,
            items: 1,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1024: {
                    items: 4
                }
            }
        },
        _create: function() {
            var $owl = this.element.owlCarousel(this.options);
            $('body').on('rtlEnabled', function() {
                $owl.data('owl.carousel').options.rtl = true;
                $owl.trigger('refresh.owl.carousel');
            });
        }
    });
    return $.Zemez.carouselInit;
});
