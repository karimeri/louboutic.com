define([
    'jquery',
    'underscore',
    'stickUp'
], function($, _) {
    'use strict';
    $.widget('Zemez.themeOptions', {
        options: {
            isStickyMenu: false,
            isToTopButton: false,
            stickUpSelector: '.nav-sections',
            stickParams: {}
        },
        _create: function() {
            this._stickUp();
            this._toTop();
        },
        _stickUp: function() {
            var stickUpSelector = $(this.options.stickUpSelector);
            if (this.options.isStickyMenu && stickUpSelector.length) {
                stickUpSelector.stickUp(this.options.stickParams);
            }
        },
        _toTop: function() {
            if (this.options.isToTopButton) {
                $(window).scroll(function() {
                    if ($(this).scrollTop() > 400) {
                        $('.scrollToTop').stop(true).fadeIn();
                    } else {
                        $('.scrollToTop').stop(true).fadeOut();
                    }
                });
                $('.scrollToTop').click(function() {
                    $('html, body').stop(true).animate({scrollTop: 0}, 800);
                    return false;
                });
            }
        }
    });
    return $.Zemez.themeOptions;
});
