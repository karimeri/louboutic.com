/**
 * Copyright © Zemez, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!',
    'carouselInit'
], function($, keyboardHandler) {
    'use strict';
    _messageClear();
    _checkoutCollapsible();
    _testimonialsCarousel();
    _faqAccordion();

    $('.panel.header > .header.links').clone().appendTo('#store\\.links');
    keyboardHandler.apply();

    function _messageClear() {
        $(document).on('click', '.page.messages .message', function() {
            $(this).hide();
        });
    }

    function _checkoutCollapsible() {
        if ($('body').hasClass('checkout-cart-index')) {
            if ($('#co-shipping-method-form .fieldset.rates').length > 0 &&
                $('#co-shipping-method-form .fieldset.rates :checked').length === 0
            ) {
                $('#block-shipping').on('collapsiblecreate', function() {
                    $('#block-shipping').collapsible('forceActivate');
                });
            }
        }
    }

    function _testimonialsCarousel() {
        $('.owl-testimonials').carouselInit({
            nav: false,
            dots: true,
            autoplay: false,
            responsive: {
                480: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1024: {
                    items: 1
                }
            }
        });
    }

    function _faqAccordion() {
        $("#faq-accordion .accordion-trigger").click(function() {
            var _accTrigger = $(this);
            var _accBlock = $(_accTrigger).parent(".accordion-block");
            var _accContent = $(_accBlock).find(".accordion-content");
            if ($(_accTrigger).hasClass("close")) {
                $(this).removeClass("close").addClass("open");
                $(_accBlock).removeClass("close").addClass("open");
                $(_accContent).slideDown();
            } else {
                $(this).removeClass("open").addClass("close");
                $(_accBlock).removeClass("open").addClass("close");
                $(_accContent).slideUp();
            }
        });
    }
});
