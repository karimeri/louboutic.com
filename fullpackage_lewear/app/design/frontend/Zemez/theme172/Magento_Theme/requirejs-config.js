/**
 * Copyright © Zemez, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        "carouselInit":     'Magento_Theme/js/carouselInit',
        "owlcarousel":      'Magento_Theme/js/owl.carousel.min'
    },
    shim: {
        "owlcarousel":      ["jquery"]
    }
};
